cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        bgVolume: 1.0,           // 背景音量
        sfxVolume: 1.0,           // 音效音量

        deskVolume: 1.0,         //   房间 房间音量

        bgAudioID: -1,            //   背景 音乐  id

        bgAudioIndex: 1           //背景音乐选择
    },

    // use this for initialization
    init: function () {
        var bindex = cc.sys.localStorage.getItem("bgAudioIndex");
        if (bindex != null) {
            this.bgAudioIndex = parseFloat(bindex);
        }

        var t = cc.sys.localStorage.getItem("sfxVolume");
        if (t != null) {
            this.sfxVolume = parseFloat(t);
        }

        var bg = cc.sys.localStorage.getItem("bgVolume");

        if (bg != null) {
            this.bgVolume = parseFloat(bg);
        }

        cc.game.on(cc.game.EVENT_HIDE, function () {
            cc.log("网页端 TRUE-----关闭音乐");
            cc.audioEngine.pauseAll();
        });

        cc.game.on(cc.game.EVENT_SHOW, function () {
            cc.log("网页端E TRUE-----开启音乐");

            cc.audioEngine.resumeAll();
        });
        cc.eventManager.addCustomListener(cc.game.EVENT_HIDE, function () {

            cc.log("手机端 TRUE-----关闭音乐");

            cc.audioEngine.pauseAll();
        });

        cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function () {
            cc.log("手机端  TRUE-----开启音乐");

            cc.audioEngine.resumeAll();
        });
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {


    // },

    getBgVolume: function () {
        return this.bgVolume;
    },
    getSfxVolume: function () {
        return this.sfxVolume;
    },
    getBgIndex: function () {
        return this.bgAudioIndex;
    },
    getUrl: function (url) {
        return cc.url.raw("resources/sounds/" + url);
    },

    playBGM: function () {
        var bgIndex = cc.mile.audio.getBgIndex();
        var musicname = "game_bgm.mp3";
        if (bgIndex == 2) {
            musicname = "bgMain.mp3";
        } else if (bgIndex == 3) {
            musicname = "bgFight.mp3";
        }
        var audioUrl = this.getUrl(musicname);
        if (this.bgAudioID >= 0) {
            cc.audioEngine.stop(this.bgAudioID);
        }
        if (this.bgVolume > 0) {
            this.bgAudioID = cc.audioEngine.play(audioUrl, true, this.bgVolume * 0.5);
        }
    },

    playSFX: function (url) {
        var audioUrl = this.getUrl(url);
        if (this.sfxVolume > 0) {
            var audioId = cc.audioEngine.play(audioUrl, false, this.sfxVolume * 0.5);
        }
    },

    setSFXVolume: function (v) {
        cc.sys.localStorage.setItem("sfxVolume", v);
        this.sfxVolume = v;
    },
    setBgVolume: function (v) {
        cc.sys.localStorage.setItem("bgVolume", v);
        this.bgVolume = v;
    },
    setBgIndex: function (v) {
        cc.sys.localStorage.setItem("bgAudioIndex", v);
        this.bgAudioIndex = v;
    },
    getState: function () {
        return cc.audioEngine.getState(this.bgAudioID);
    },
    setBGMVolume: function (v, force) {
        if (this.bgAudioID >= 0) {
            if (v > 0 && cc.audioEngine.getState(this.bgAudioID) === cc.audioEngine.AudioState.PAUSED) {
                cc.audioEngine.resume(this.bgAudioID);
            } else if (v == 0) {
                cc.audioEngine.pause(this.bgAudioID);
            }
        }
        if (this.bgVolume != v || force) {
            cc.sys.localStorage.setItem("bgVolume", v);
            this.bgmVolume = v;
            cc.audioEngine.setVolume(this.bgAudioID, v);
        }
    },

    stopAll: function () {
        cc.audioEngine.stopAll();
    },
    pauseAll: function () {
        cc.audioEngine.pauseAll();
    },

    resumeAll: function () {
        cc.audioEngine.resumeAll();
    }
});
