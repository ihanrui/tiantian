cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        userLeft: {
            default: null,
            type: cc.Node
        },
        userTop: {
            default: null,
            type: cc.Node
        },
        userRight: {
            default: null,
            type: cc.Node
        },
        userSelf: {
            default: null,
            type: cc.Node
        },
        selfPosition: null,
        sourcePosition: null,
        targetPosition: null,
        targetUserid: null,
    },

    // use this for initialization
    onLoad: function onLoad() {
        this.animationData = {};
    },

    initSelfPosition: function (pos) {
        this.selfPosition = pos;
        this.sourcePosition = pos;
    },

    setSourcePosition: function (pos) {
        this.sourcePosition = pos;
    },
    setTargetPosition: function (pos) {
        this.targetPosition = pos;
    },
    setChoicePosition: function (pos) {
        debugger
        this.choicePosition = pos;
    },
    getChoicePosition: function () {
        return this.choicePosition;
    },

    setTargetUserid: function (userid) {
        this.targetUserid = userid;
    },
    init: function (data) {
        this.targetPosition.x = 0;
        this.targetPosition.y = 0;
        if (data == "userRight") {
            this.targetPosition.x = this.userRight.x;
            this.targetPosition.y = this.userRight.y;
        } else if (data == "userTop") {

            this.targetPosition.x = this.userTop.x;
            this.targetPosition.y = this.userTop.y;
        } else if (data == "userLeft") {

            this.targetPosition.x = this.userLeft.x;
            this.targetPosition.y = this.userLeft.y;
        }
    },
    getCenterX: function () {
        return cc.view.getFrameSize().height / 2;
    },
    getCenterY: function () {
        return cc.view.getFrameSize().width / 2;
    },

    sendPlayAnimation: function (fromId,data) {
        debugger
        cc.log('发送表情sendPlayAnimation:['+fromId+']---data:['+data);
        var sendSuccess = false;
        if (data == "bomb") {
            //丢炸弹
            sendSuccess = this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x,
                targetY: this.targetPosition.y,
                audioName: "mazi/M_4.mp3",
                images: ["01.png", "02.png", "03.png", "11.png", "13.png", "15.png", "17.png", "19.png", "23.png", "25.png", "29.png", "31.png", "33.png"],
                targetPath: "resources/images/roomchat/4",
                targetImage: "resources/images/roomchat/bomb.png"
            });
        }
        else if (data == "flower") {
            //花
            sendSuccess =this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x - 10,
                targetY: this.targetPosition.y - 15,
                audioName: "mazi/M_1.mp3",
                images: ["11.png", "12.png", "13.png", "14.png", "15.png", "16.png", "17.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png", "25.png", "26.png", "27.png", "28.png", "29.png", "30.png", "31.png"],
                targetPath: "resources/images/roomchat/1",
                targetImage: "resources/images/roomchat/flower.png"
            });
        }
        else if (data == "cheers") {
            //大拇指
            sendSuccess = this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x - 20,
                targetY: this.targetPosition.y - 25,
                audioName: "mazi/M_2.mp3",
                images: ["01.png", "02.png", "03.png", "04.png", "05.png", "06.png", "07.png", "08.png", "09.png", "10.png", "11.png", "12.png", "13.png", "14.png", "15.png", "16.png"],
                targetPath: "resources/images/roomchat/2",
                targetImage: "resources/images/roomchat/cheers.png"
            });
        }
        else if (data == "kiss") {
            //飞吻
            sendSuccess = this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x - 15,
                targetY: this.targetPosition.y - 15,
                audioName: "mazi/M_3.mp3",
                images: ["01.png", "03.png", "07.png", "08.png", "09.png", "11.png", "13.png", "15.png", "17.png", "19.png", "25.png", "27.png", "44.png", "46.png", "48.png", "52.png", "54.png"],
                targetPath: "resources/images/roomchat/3",
                targetImage: "resources/images/roomchat/kiss.png"
            });
        }
        else if (data == "egg") {
            //丢鸡蛋
            var targetX = this.targetPosition.x - 10;
            sendSuccess = this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x - 15,
                targetY: this.targetPosition.y - 40,
                audioName: "mazi/M_5.mp3",
                images: ["01.png", "02.png", "03.png", "04.png", "05.png", "11.png", "13.png", "17.png", "19.png", "23.png", "29.png", "68.png", "73.png", "78.png", "81.png", "85.png"],
                targetPath: "resources/images/roomchat/5",
                targetImage: "resources/images/roomchat/egg.png"
            });
        }
        else if (data == "shoe") {
            //丢拖鞋
            sendSuccess = this.sendImageAnimation({
                fromId: fromId,
                startX: this.sourcePosition.x,
                startY: this.sourcePosition.y,
                targetX: this.targetPosition.x - 20,
                targetY: this.targetPosition.y - 25,
                audioName: "mazi/M_6.mp3",
                images: ["10.png", "11.png", "12.png", "13.png", "14.png", "15.png", "16.png", "17.png", "18.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png", "25.png", "26.png"],
                targetPath: "resources/images/roomchat/6",
                targetImage: "resources/images/roomchat/shoe.png"
            });
        }

        return sendSuccess;
    },

    sendImageAnimation: function (params) {
        cc.log('发送表情信息:'+JSON.stringify(params));
        if(this.animationData[params.fromId] && this.animationData[params.fromId].animationNode){
            var gameUtils = require("WangdaGameUtils");
            gameUtils.showCenterToast('老板先歇一下，喝口茶');
            return false;
        }else {

            this.animationData[params.fromId] = {
                startX : params.startX,
                startY : params.startY,
                targetX : params.targetX,
                targetY : params.targetY,
                images : params.images,
                targetImage : params.targetImage,
                targetPath : params.targetPath,
                audioName : params.audioName
            }

            this.startSend(params.fromId);
        }
        return true;
    },
    startSend: function (fromId) {

        this.animationData[fromId].animationNode = new cc.Node();
        var sprite = this.animationData[fromId].animationNode.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw(this.animationData[fromId].targetImage));
        this.animationData[fromId].animationNode.setPosition(this.animationData[fromId].startX, this.animationData[fromId].startY);
        this.animationData[fromId].animationNode.parent = cc.find("Canvas");
        // cc.director.getScene().addChild(this.animationNode);

        //完成后删除莫牌对象
        var finished = cc.callFunc(function () {
            this.startAnimation(fromId);
        }, this);

        // 创建一个移动动作
        var action = cc.moveTo(0.5, this.animationData[fromId].targetX, this.animationData[fromId].targetY);
        // 执行动作
        this.animationData[fromId].animationNode.runAction(cc.sequence(action, finished));
    },

    startAnimation: function (fromId) {
        var animation = this.animationData[fromId].animationNode.addComponent(cc.Animation);
        // frames 这是一个 SpriteFrame 的数组.
        var frames = this.getImageSpriteFrame(fromId);
        var clip = cc.AnimationClip.createWithSpriteFrames(frames, 2.5);
        clip.name = ""+fromId;

        /* 添加帧事件
        clip.events.push({
            frame: 1,               // 准确的时间，以秒为单位。这里表示将在动画播放到 1s 时触发事件
            func: "frameEvent",     // 回调函数名称
            params: [1, "hello"]    // 回调参数
        });
        */


        animation.addClip(clip);
        cc.mile.audio.playSFX(this.animationData[fromId].audioName);
        animation.on('finished', this.onFinished,this);
        animation.play(""+fromId);
    },
    onFinished: function (animation) {

        var animationName = animation.currentTarget.name;
        if(this.animationData[animationName]
            && this.animationData[animationName].animationNode){
            this.animationData[animationName].animationNode.destroy();
            this.animationData[animationName].animationNode = null;
        }

    },
    getImageSpriteFrame: function (fromId) {
        var lstFrames = [];
        for (var i = 0; i < this.animationData[fromId].images.length; i++) {
            lstFrames.push(new cc.SpriteFrame(cc.url.raw(this.animationData[fromId].targetPath + "/" + this.animationData[fromId].images[i])));
        }
        return lstFrames;
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
