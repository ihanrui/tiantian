var customManifestStr = JSON.stringify({
    "packageUrl": "https://tiantian.chnmile.com/hotfix/",
    "remoteManifestUrl": "https://tiantian.chnmile.com/hotfix/project.manifest",
    "remoteVersionUrl": "https://tiantian.chnmile.com/hotfix/version.manifest",
    "version": "1.2.8",
    "assets": {},
    "searchPaths": []
});

var gameUtils = require("WangdaGameUtils");

cc.Class({
    extends: cc.Component,

    properties: {

        manifestUrl: cc.RawAsset,
        updateUI: {
            default: null,
            type: cc.Node
        },
        loginContainerUI: {
            default: null,
            type: cc.Node
        },
        hotFixProInfoLabel: {
            default: null,
            type: cc.Label
        },
        buttonGroupWechat:{
            default:null,
            type: cc.Node
        },
        buttonGroupAll:{
            default:null,
            type: cc.Node
        },

        byteProgress: {
            default: null,
            type: cc.ProgressBar
        },
        byteProgressBar: {
            default: null,
            type: cc.Node
        },
        byteProgressNumNode: {
            default: null,
            type: cc.Node
        },
        byteProgressNumLabel: {
            default: null,
            type: cc.Label
        },
        backgroundImage: {
            default: null,
            type: cc.Node
        },

        updateVersionContainerNode: {
            default: null,
            type: cc.Node
        },
        updateVersionLabel: {
            default: null,
            type: cc.Label
        },

        _updating: false,
        _canRetry: false,
        _storagePath: ''
    },

    checkCb: function (event) {
        cc.log('Code: ' + event.getEventCode());
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                this.hotFixProInfoLabel.string = "没有找到本地的版本文件，更新失败.";
                this.showHotFixView(false);
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:

                this.hotFixProInfoLabel.string = "下载服务器版本文件失败，请重试.";
                this.showHotFixView(false);
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                this.hotFixProInfoLabel.string = "已经是最新版本";

                this.showHotFixView(false);

                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                this.hotFixProInfoLabel.string = '检查到新版本';
                //this.panel.checkBtn.active = false;
                this.backgroundImage.active = false;

                this.showHotFixView(true);
                this.hotUpdate();
                break;
            default:
                return;
        }
        cc.log('checkCbfinish--Code: ' + event.getEventCode());

        cc.eventManager.removeListener(this._checkListener);
        this._checkListener = null;
        this._updating = false;
    },

    updateCb: function (event) {
        var needRestart = false;
        var failed = false;
        cc.log("更新回调：" + event.getEventCode() + "----" + event.getMessage());
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                this.hotFixProInfoLabel.string = 'No local manifest file found, hot update skipped.';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                //this.byteProgress.progress = event.getPercent();
                //this.fileProgress.progress = event.getPercentByFile();

                //this.fileProInfoLabel.string = event.getDownloadedFiles() + ' / ' + event.getTotalFiles();
                //this.byteProInfoLabel.string = event.getDownloadedBytes() + ' / ' + event.getTotalBytes();


                this.updateProgressBar(event.getDownloadedFiles(), event.getTotalFiles());

                //var msg = event.getMessage();
                //if (msg) {
                //  this.hotFixProInfoLabel.string = 'Updated file: ' + msg;
                // cc.log(event.getPercent()/100 + '% : ' + msg);
                //}
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                this.hotFixProInfoLabel.string = '热更新失败';
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                this.hotFixProInfoLabel.string = '已经是最新的版本';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                this.hotFixProInfoLabel.string = '更新完成！';
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                this.hotFixProInfoLabel.string = '更新失败，请重新打开游戏 ';
                //this.panel.retryBtn.active = true;
                this._updating = false;
                this._canRetry = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                cc.log('Asset update error: ' + event.getAssetId() + ', ' + event.getMessage());
                this.hotFixProInfoLabel.string = '更新失败，请重新打开游戏 ';
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                this.hotFixProInfoLabel.string = event.getMessage();
                break;
            default:
                break;
        }

        if (failed) {
            cc.eventManager.removeListener(this._updateListener);
            this._updateListener = null;
            this._updating = false;
        }

        if (needRestart) {
            cc.eventManager.removeListener(this._updateListener);
            this._updateListener = null;
            // Prepend the manifest's search path
            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = this._am.getLocalManifest().getSearchPaths();
            console.log(JSON.stringify(newPaths));
            Array.prototype.unshift(searchPaths, newPaths);
            // This value will be retrieved and appended to the default search path during game startup,
            // please refer to samples/js-tests/main.js for detailed usage.
            // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
            cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);

            cc.audioEngine.stopAll();
            cc.game.restart();
        }
    },
    updateProgressBar: function (finishCount, fileCount) {
        cc.log("更新信息finishCount：【" + finishCount + "】----fileCount：【" + fileCount + "】");
        var num = (finishCount / fileCount);
        var beforeWidth = this.byteProgressBar.width;
        this.byteProgress.progress = num;
        var speed = this.byteProgressBar.width - beforeWidth;
        this.byteProgressNumNode.setPosition(this.byteProgressNumNode.position.x + speed, this.byteProgressNumNode.position.y);
        this.byteProgressNumLabel.string = parseInt(num * 100) + "%";


    },

    loadCustomManifest: function () {
        if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
            var manifest = new jsb.Manifest(customManifestStr, this._storagePath);
            this._am.loadLocalManifest(manifest, this._storagePath);
            this.hotFixProInfoLabel.string = 'Using custom manifest';
        }
    },

    retry: function () {
        if (!this._updating && this._canRetry) {
            //this.panel.retryBtn.active = false;
            this._canRetry = false;

            this.hotFixProInfoLabel.string = 'Retry failed Assets...';
            this._am.downloadFailedAssets();
        }
    },

    checkUpdate: function () {
        cc.log('检查新版本--begin');
        if (this._updating) {
            this.hotFixProInfoLabel.string = '正在更新，请勿关闭游戏！';
            return;
        }
        if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
            cc.log('--loadLocalManifest--begin：' + this.manifestUrl);
            this._am.loadLocalManifest(this.manifestUrl);
        }
        if (!this._am.getLocalManifest() || !this._am.getLocalManifest().isLoaded()) {
            this.hotFixProInfoLabel.string = '获取更新文件失败 ...';
            return;
        }
        this._checkListener = new jsb.EventListenerAssetsManager(this._am, this.checkCb.bind(this));
        cc.eventManager.addListener(this._checkListener, 1);

        this._am.checkUpdate();

    },

    hotUpdate: function () {
        cc.log(this._am + '------hotUpdate-------' + this._updating);
        if (this._am && !this._updating) {

            this._updating = true;

            cc.log(this._am.getState() + '------hotUpdate-----begin--' + jsb.AssetsManager.State.UNINITED);
            this._updateListener = new jsb.EventListenerAssetsManager(this._am, this.updateCb.bind(this));
            cc.eventManager.addListener(this._updateListener, 1);

            if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
                this._am.loadLocalManifest(this.manifestUrl);
            }

            this._failCount = 0;
            this._am.update();

            this._updating = true;
        }
    },

    showHotFixView: function (status) {

        this.updateUI.active = status;
        this.loginContainerUI.active = !status;
        if (!status) {
            this.autoLogin();
        }
    },

    autoLogin: function () {
        var object = cc.find("Canvas/script/common");
        if (object) {
            var commonScript = object.getComponent('common');
            commonScript.autoLogin();
        }

    },
    // use this for initialization
    onLoad: function () {
        // Hot update is only available in Native build
        if (!cc.sys.isNative) {
            /*var self = this;
            var filecount = 57;
            var fileFinished= 0;
            this.runtimer = function () {
                fileFinished++;
                self.updateProgressBar(fileFinished,filecount);
                if (filecount == fileFinished) {
                    self.unscheduleAllCaldeslbacks();
                    this.showHotFixView(false);
                }
            }
            self.schedule(this.runtimer, 0.05);*/

            this.showHotFixView(false);
            //this.getVersionConfig();
            return;
        }
        this._storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'blackjack-remote-asset');
        cc.log('Storage path for remote asset : ' + this._storagePath);

        // Setup your own version compare handler, versionA and B is versions in string
        // if the return value greater than 0, versionA is greater than B,
        // if the return value equals 0, versionA equals to B,
        // if the return value smaller than 0, versionA is smaller than B.
        this.versionCompareHandle = function (versionA, versionB) {
            cc.log("JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
            var vA = versionA.split('.');
            var vB = versionB.split('.');
            for (var i = 0; i < vA.length; ++i) {
                var a = parseInt(vA[i]);
                var b = parseInt(vB[i] || 0);
                if (a === b) {
                    continue;
                }
                else {
                    return a - b;
                }
            }
            if (vB.length > vA.length) {
                return -1;
            }
            else {
                return 0;
            }
        };

        // Init with empty manifest url for testing custom manifest
        this._am = new jsb.AssetsManager('', this._storagePath, this.versionCompareHandle);
        if (!cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) {
            this._am.retain();
        }

        var infoLabel = this.hotFixProInfoLabel;
        // Setup the verification callback, but we don't have md5 check function yet, so only print some message
        // Return true if the verification passed, otherwise return false
        this._am.setVerifyCallback(function (path, asset) {
            // When asset is compressed, we don't need to check its md5, because zip file have been deleted.
            var compressed = asset.compressed;
            // Retrieve the correct md5 value.
            var expectedMD5 = asset.md5;
            // asset.path is relative path and path is absolute.
            var relativePath = asset.path;
            // The size of asset file, but this value could be absent.
            var size = asset.size;
            if (compressed) {
                //infoLabel.string = "Verification passed : " + relativePath;
                return true;
            }
            else {
                //infoLabel.string = "Verification passed : " + relativePath + ' (' + expectedMD5 + ')';
                return true;
            }
        });

        //this.hotFixProInfoLabel.string = 'Hot update is ready, please check or directly update.';

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            // Some Android device may slow down the download process when concurrent tasks is too much.
            // The value may not be accurate, please do more test and find what's most suitable for your game.
            this._am.setMaxConcurrentTask(2);
            //this.hotFixProInfoLabel.string = "Max concurrent tasks count have been limited to 2";
        }


        this.getVersionConfig();

    },

    getVersionConfig: function () {
        //通过ID获取 玩家信息
        var params = {
            code: 'versionConfig',
        };
        cc.mile.http.httpPost("/hall/getJsonstr", params,
            this.getVersionSuccess, this.getVersionFailure, this);
    },
    getVersionSuccess: function (result, object) {
        //cc.log('获取版本更新信息：'+ JSON.stringify(this.updateVerionData));
        cc.log('获取版本更新信息：' + JSON.stringify(result));
        var data = gameUtils.jsonParse(result);
        if (data != null) {
            object.showVersionUpdate(data);
        } else {
            object.getSystemConfig();
        }
    },
    getVersionFailure: function (result, object) {

        cc.log('---服务器信息获取失败---');

        object.showCenterToast("服务器信息获取失败");
    },
    showVersionUpdate: function (data) {

        cc.log('获取版本更新服务器信息：' + JSON.stringify(data));
        cc.log('获取版本更新本地信息：' + JSON.stringify(customManifestStr));
        var localObject = JSON.parse(customManifestStr);

        cc.log('version：' + localObject.version);
        cc.log('versionCode：' + data.versionCode);

        if (data.mustUpdate && localObject.version != data.versionCode) {// 且版本不一样
            this.updateVersionContainerNode.active = true;
            this.updateVerionData = data;
            this.updateVersionLabel.stropenURLing = data.versionInfo;
        } else {
            this.getSystemConfig();
        }
    },
    onVersionUpdateClick: function () {
        cc.log('获取版本更新信息：' + JSON.stringify(this.updateVerionData));

        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "openURL", "(Ljava/lang/String;)V", this.updateVerionData.android.updateUrl);

        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppController",
                "openURLScheme:", this.updateVerionData.ios.updateUrl);
        }
    },

    getSystemConfig: function () {
        //通过ID获取 玩家信息
        var params = {
            code: 'systemConfig',
        };
        cc.mile.http.httpPost("/hall/getJsonstr", params,
            this.getSystemConfigSuccess, this.getVersionFailure, this);
    },
    getSystemConfigSuccess: function (result, object) {
        cc.log('获取系统配置信息：' + JSON.stringify(result));
        var data = gameUtils.jsonParse(result);
        object.initSystemConfig(data);

    },
    initSystemConfig: function (data) {
        if (data && data.appid) {

            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                    "initWeXin", "(Ljava/lang/String;)V", data.appid);

            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
                jsb.reflection.callStaticMethod("AppController",
                    "initWeXin:", data.appid);
            }
        }
        if(data && data.userSeeCards){
            cc.mile.userSeeCards = data.userSeeCards;
        }
        if(data && data.allLogin){
            this.buttonGroupAll.active = true;
            this.buttonGroupWechat.active = false;
        }else {
            this.buttonGroupAll.active = false;
            this.buttonGroupWechat.active = true;
        }
        if(data && data.appRecharge){
            cc.mile.appRecharge = data.appRecharge
        }

        this.checkUpdate();

    },
    getSystemConfignFailure: function (result, object) {
        object.showCenterToast("服务器信息获取失败");
    },
    onDestroy: function () {
        if (this._updateListener) {
            cc.eventManager.removeListener(this._updateListener);
            this._updateListener = null;
        }
        if (this._am && !cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) {
            this._am.release();
        }
    }
});
