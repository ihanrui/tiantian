var beiMiCommon = require("MileCommon");
var voiceOnLineSelf = null;

cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...

        recording_btn: {
            default: null,
            type: cc.Node
        },
        recording_animation: {
            default: null,
            type: cc.Animation
        },
        recording_container: {
            default: null,
            type: cc.Node
        },
        recording_cancel: {
            default: null,
            type: cc.Node
        },
    },
    static: {

    },
    // use this for initialization
    onLoad: function () {
        voiceOnLineSelf = this;
        cc.gvoiceUploadCallbackJS = this.gvoiceUploadCallbackJS;
        this._registerEvent();
    },

    _registerEvent: function () {

        this.recording_btn.on(cc.Node.EventType.TOUCH_START, function (e) {
            console.log('poker TOUCH_START:' + e.getLocationX() + "---" + e.getLocationY());
            this.beginSoundRecording();
            this.beginRecordingPositionY = e.getLocationY();
            this.beginRecordingPositionX = e.getLocationX();
            e.stopPropagation();
        }, this);
        this.recording_btn.on(cc.Node.EventType.TOUCH_MOVE, function (e) {
            console.log('poker TOUCH_MOVE:' + e.getLocationX() + "---" + e.getLocationY());
            if (this.sound_recording_status) {
                var positionY = e.getLocationY();
                var positionX = e.getLocationX();
                if (positionY - this.beginRecordingPositionY > 50) {
                    this.endSoundRecording();
                } else if (positionY - this.beginRecordingPositionY > 30) {
                    this.cancelSoundRecording();
                } else if (positionX - this.beginRecordingPositionX > 20
                    || positionX - this.beginRecordingPositionX < -20) {
                    this.endSoundRecording();
                }
            }
            e.stopPropagation();
        }, this);
        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）
        this.recording_btn.on(cc.Node.EventType.TOUCH_END, function (e) {
            console.log('poker TOUCH_END');
            this.endSoundRecording();
            e.stopPropagation();

        }, this);

    },

    beginSoundRecording: function () {
        if (this.sound_recording_status != 3) {

            cc.audioEngine.pauseAll();

            this.sound_recording_status = 1;
            this.recording_container.active = true;
            this.recording_animation.play();
            this.onVoiceChatStart();

        } else {
            if (this.sound_recording_check_count > 0) {
                this.sound_recording_check_count--;
                this.sound_recording_status = 1;
            }
            cc.audioEngine.resumeAll();
            this.showCenterToast("语音发送中，请稍等！");

        }
    },
    endSoundRecording: function () {
        if (this.recording_container.active
            || this.recording_cancel.active) {
            this.recording_animation.stop();
            this.recording_container.active = false;
            this.recording_cancel.active = false;
            this.onVoiceChatStop();
        }

        /*var pokers = this.getCommon("MaziPoker");


        pokers.clearSelectedCards();
        var currentTipCards = [66,23];
        for(var i = 0 ;i < currentTipCards.length ; i++){
            pokers.addSelectCard(currentTipCards[i]);
        }
        pokers.doSelectCard();
*/
    },
    cancelSoundRecording: function () {
        this.sound_recording_status = 2;
        this.recording_container.active = false;
        this.recording_animation.stop();
        this.recording_cancel.active = true;
    },
    onVoiceChatStart: function () {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "startVoice", "()V");
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppController", "startRecord");
        }
    },
    onVoiceChatStop: function () {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "stopVoice", "(I)V", this.sound_recording_status);
            var isOpenVoice = jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "isOpenVoice", "()Z");
            if (!isOpenVoice) {
                this.showCenterToast('发送失败，请开启录音话筒权限!');
                return;
            }
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

            jsb.reflection.callStaticMethod("AppController",
                "stopVoice:", this.sound_recording_status);
            var isOpenVoice = jsb.reflection.callStaticMethod("AppController",
                "isOpenVoice");
            if (!isOpenVoice) {
                this.showCenterToast('发送失败，请开启录音话筒权限!');
                return;
            }
        }

        if (this.sound_recording_status == 1) {
            this.sound_recording_status = 3; // 发送中
            this.sound_recording_check_count = 1; // 发送中

        }
    },
    gvoiceUploadCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {

            voiceOnLineSelf.sendMsgByUploadFileId(data.fileID);
        } else {
            if (data && data.callbackMsg) {
                voiceOnLineSelf.showCenterToast(data.callbackMsg);
            }
        }
        voiceOnLineSelf.sound_recording_status = 1;

    },

    sendMsgByUploadFileId: function (uploadFileId) {
        var msg = {
            source: cc.mile.user.id,
            target: '',
            msgtype: '1',
            sendtype: '0',
            msgid: uploadFileId,
            msgname: '',
            msgcontent: '',
            mem: '',
            token : cc.mile.authorization
        }

        this.emitSocketRequest("sendmsg", JSON.stringify(msg));

    },


    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
