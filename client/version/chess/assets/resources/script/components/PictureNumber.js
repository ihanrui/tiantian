/**
 * 网达棋牌  数字
 */
var PictureNumber = cc.Class({
    extends: cc.Component,
    properties: {
        parentNode:{
            default: null,
            type: cc.Node
        },
        m_Number: null,
        m_NumberTexture: null,
        x: 0,
        y: 0,
        splitSize: 12
    },
    /**
     *
     * @param paramNumber   需要转换的数字
     * @param paramTexture  图片的texture
     * @param splitSize  图片等分数量
     * @param positionX  开始的X 坐标
     * @param positionY   开始的Y 坐标
     * @returns {*|void}
     */
    buildNumber: function (buildParams) {

        this.setNumber(buildParams.paramNumber);
        this.setNumberTexture(buildParams.paramTexture);
        this.x = buildParams.positionX;
        this.y = buildParams.positionY;
        this.parentNode = buildParams.parentNode;
        this.splitSize = buildParams.splitSize
        this.ignoreFlag = buildParams.ignoreFlag
        //this.setNumberTexture(cc.textureCache.addImage(paramTexture));
        return this.build();
    },
    build: function () {

        var iNumStr = (this.m_Number + "");   //取得字符

        var stSize = this.m_NumberTexture.getContentSize(); //取得纹理大小，要求纹理中每个数字都是等宽等高，并依照0123456789排列

        var iNumWidth = parseInt(stSize.width / this.splitSize);    //纹理中每个数字的宽度
        var iNumHeight = parseInt(stSize.height);    //纹理中每个数字的高度

        var hasFlag = false;  // 添加  正负号
        if (this.m_Number > 0 && !this.ignoreFlag) {
            hasFlag = true;
            this.addTextureNode(0, iNumWidth, iNumHeight, 0, 0);
        } else if (this.m_Number < 0 && !this.ignoreFlag) {
            hasFlag = true;
            this.addTextureNode(1, iNumWidth, iNumHeight, 0, 0);
            iNumStr = iNumStr.substr(1,iNumStr.length);
        }
        if(this.ignoreFlag){
            hasFlag = false;
        }
        var iNumCount = iNumStr.length;   //取得字符个数
        for (var i = 0; i < iNumCount; i++) {
            var offsetX = i * iNumWidth;
            if (hasFlag) {
                offsetX += iNumWidth;
                var indexValue = parseInt(iNumStr[i]);
                this.addTextureNode(indexValue + 2, iNumWidth, iNumHeight, offsetX, 0);

            }else {

                var indexValue = parseInt(iNumStr[i]);

                if(indexValue == 0 && iNumCount ==1){
                    this.addTextureNode(indexValue + 2, iNumWidth, iNumHeight, offsetX, 0);
                }else {

                    this.addTextureNode(indexValue , iNumWidth, iNumHeight, offsetX, 0);
                }

            }

        }

    },
    addTextureNode: function (imageIndex,
                              iNumWidth,
                              iNumHeight,
                              offsetX,
                              offsetY) {

        var node = new cc.Node("New Sprite");
        node.setAnchorPoint(0, 0);
        var sprite = node.addComponent(cc.Sprite);
        var iNumber = imageIndex;
        var stRect = new cc.rect(iNumber * iNumWidth, 0, iNumWidth, iNumHeight);
        sprite.spriteFrame = new cc.SpriteFrame(this.m_NumberTexture, stRect);
        //sprite.setPosition(i * iNumWidth, 0);
        node.setPosition(this.x + offsetX, this.y + offsetY);
        if(this.parentNode){
            node.parent = this.parentNode;
        }else {
            cc.director.getScene().addChild(node);
        }
    },
    setNumber: function (paramNumber) {
        this.m_Number = paramNumber;
    },
    getNumber: function () {
        return this.m_Number;
    },
    setNumberTexture: function (paramTexture) {
        this.m_NumberTexture = paramTexture;
    },

});