/**
 * 工具类
 */
var WangdaGameUtils = require("WangdaGameUtils");

var NiuNiuCoinSettlement = cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },

    /**
     *
     * sendInfo
     * {
        targetX
        targetY
        startX
        startY
        coinCount
        score
        fromId
        targetId
        scoreParentNode
     }
     * @param sendInfo
     */
    startSend: function (sendInfo) {
        this.sendData = {};
        this.scoreParentNode = sendInfo.scoreParentNode;
        this.sendData[sendInfo.targetId] = {
            sendedCount: 0
        };
        this.schedule(function () {

            var animationNode = new cc.Node();
            var sprite = animationNode.addComponent(cc.Sprite);
            sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/playCards/jb1.png"));

            animationNode.setPosition(sendInfo.startX, sendInfo.startY);
            animationNode.parent = cc.find("Canvas");
            // cc.director.getScene().addChild(this.animationNode);
            this.sendData[sendInfo.targetId].sendedCount++;
            //完成后删除莫牌对象
            var finished = cc.callFunc(function () {
                if (animationNode) {
                    animationNode.destroy();
                }
                if (this.sendData[sendInfo.targetId].sendedCount == sendInfo.coinCount) {
                    this.startCoinAnimation(sendInfo.score);
                }
            }, this);

            // 创建一个移动动作
            var action = cc.moveTo(0.5, sendInfo.targetX, sendInfo.targetY);
            // 执行动作
            animationNode.runAction(cc.sequence(action, finished));

        }, 0.07, sendInfo.coinCount, 0.07);// 间隔时间，重复次数，推迟执行时间


    },

    startCoinAnimation: function ( score) {
        var animationNode = new cc.Node();
        WangdaGameUtils.showNumberNode(score, animationNode);
        var scale1 = cc.scaleTo(0.3, 0.8, 0.8);
        var scale2 = cc.scaleTo(0.5, 1.1, 1.1);
        var scale3 = cc.scaleTo(0.2, 1, 1);

        animationNode.setScale(0.5,0.5);

        animationNode.runAction(cc.sequence(scale1,scale2,scale3));
        animationNode.parent = this.scoreParentNode;
    },
    showCoinAnimation: function (parentNode, score) {
        this.scoreParentNode = parentNode;
        this.startCoinAnimation(score);
    },


    // use this for initialization
    onLoad: function () {
        this.sendData = {};
    },

});