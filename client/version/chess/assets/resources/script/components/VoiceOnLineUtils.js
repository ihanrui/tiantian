/**
 * 发送语音工具类
 *
 */
var onlineVoiceData = {};

var VoiceOnLineUtils = cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },
    statics: {
        // 发送语音
        playVoiceMsg: function (data) {

            cc.log('playVoiceMsg:' + JSON.stringify(data));

            if (data.source == cc.mile.user.id) {
                this.stopAllUserVoiceAnimationStatus(false);
                onlineVoiceData = {};
            }


            if (!this.checkIsPlayRepeat(data.msgid, data.source)) {

                //-------this.stopAllUserVoiceAnimationStatus(false);

                if (Object.keys(onlineVoiceData).length == 1) {
                    this.callNativePlayVoice(data.msgid);
                }


            }

        },
        callNativePlayVoice: function (msgid) {
            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

                jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                    "playVoice", "(Ljava/lang/String;)V", msgid);

            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

                jsb.reflection.callStaticMethod("AppController",
                    "playVoice:", msgid);

            }

        },
        checkIsPlayRepeat: function (msgid, source) {
            if (onlineVoiceData) {
                if (onlineVoiceData[msgid]) {
                    return true;
                } else {
                    if (onlineVoiceData.length > 10) {
                        onlineVoiceData = {};
                    }
                    onlineVoiceData[msgid] = source;
                }
            } else {
                onlineVoiceData[msgid] = source;
            }
            return false;
        },
        onVoicePlayFinished: function (data) {
            cc.log('原生代码回调onVoicePlayFinished：' + JSON.stringify(data));

            cc.log('回调之前:' + JSON.stringify(onlineVoiceData));

            // 删除对应的key 值
            delete onlineVoiceData[data.fileID];

            cc.log('清理之后:' + JSON.stringify(onlineVoiceData));

            var voiceIds = Object.getOwnPropertyNames(onlineVoiceData);

            cc.log('清理之后:' + voiceIds);

            if (voiceIds.length > 0) {
                this.callNativePlayVoice(voiceIds[voiceIds.length - 1]);
            } else {
                cc.audioEngine.resumeAll();
            }

        },
    },

    // use this for initialization
    onLoad: function () {
    },

});