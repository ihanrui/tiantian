

var GameConstants = cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },
    statics: {
        USER_TOKEN_KEY:'user_token_key',
        USER_LOGIN_TYPE:'user_login_type',
        USER_LOGIN_TYPE_WEIXIN:'user_login_weixin',
        USER_LOGIN_TYPE_GUEST:'user_login_guest',
        CREATE_ROOM_TYPE_KEY:'create_room_type',


        // 创建房间类型
        CREATE_ROOM_TYPE_CARD: "card",
        CREATE_ROOM_TYPE_TEACOIN: "teacoin",
        CREATE_ROOM_TYPE_COIN: "coin",
    },
    // use this for initialization
    onLoad: function () {
    },

});



