var Base64 = require("Base64");
var GameConstants = require("GameConstants");
var gameUtils = require("WangdaGameUtils");

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        connectedFlag: false,
    },

    // use this for initialization
    onLoad: function () {
        cc.mile.room_callback = null;  //加入房间回调函数
    },
    ready: function () {
        var check = false;
        if (cc.mile) {
            check = true;
        } else {
            this.scene("login", this);
        }
        return check;
    },
    connect: function (reconnect) {

        cc.mile.recreateConnectionFlag = reconnect;

        this.connectToServer(reconnect);

        this.registerSocketEvent();

        return cc.mile.socket;
    },

    connectToServer: function (reconnect) {


        if(reconnect){
            this.showCenterToast("正在建立网络链接，请稍后");
            this.loadding();
        }
        /**
         * 登录成功后，创建 Socket链接，
         */
        var options = {
            "reconnection": true,
            "reconnectionAttempts": 10,
            "reconnectionDelayMax": 5000,
            "timeout": 5000,
            "reconnectionDelay": 2000,
            "force new connection": true,
            "secure": true,
            "rejectUnauthorized": false
        };

        cc.log("链接参数："+JSON.stringify(options));
        if (cc.mile.socket != null) {

            this.showCenterToast("正在重新建立网络链接，请稍后");

            if(cc.sys.isNative){

                cc.log("connectToServer.原生重连 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");
                //cc.mile.socket.open();
                if (cc.sys.os == cc.sys.OS_IOS) {
                    cc.mile.socket = window.io.connect(cc.mile.http.wsURL + '/bm/game', options);

                } else {
                    cc.mile.socket = window.io.connect(cc.mile.http.androidWS + '/bm/game', options);
                }


                if(cc && cc.eventManager){

                    cc.log("发送重连事件 :" + JSON.stringify(cc.mile.socket));


                    cc.eventManager.dispatchCustomEvent('socketReconnect')
                }
                cc.log("connectToServer.原生重连 :" + JSON.stringify(cc.mile.socket));

            }else {
                cc.log("connectToServer.网页重连 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");
                cc.mile.socket.disconnect();
                cc.mile.socket.connect();
            }


            //cc.mile.socket = null;
        }else {

            var beginTime = Date.now();
            cc.mile.beginTime = beginTime;
            cc.log("connectToServer.开始初始化 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");

            if (cc.sys.os == cc.sys.OS_IOS) {
                cc.mile.socket = window.io.connect(cc.mile.http.wsURL + '/bm/game', options);

            } else {
                cc.mile.socket = window.io.connect(cc.mile.http.androidWS + '/bm/game', options);
            }
            //cc.mile.socket = window.io.connect(cc.mile.http.wsURL + '/bm/game', options);
        }


    },

    _hideViewAction: function () {

        cc.log("hideViewAction");

        var offlineParams = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            roomlinestatus: 'offline'
        };
        this.emitSocketRequest("roomlinestatus", JSON.stringify(offlineParams));
    },
    _showViewAction: function () {
        cc.log("showViewAction");


        var onlineParams = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            roomlinestatus: 'online'
        };
        this.emitSocketRequest("roomlinestatus", JSON.stringify(onlineParams));




    },
    registerSocketEvent: function () {
        var self = this;
        this.closeSocketListener();
        this.sendCount = 0;

        if(cc.sys.isNative){

            cc.eventManager.addCustomListener(cc.game.EVENT_HIDE, function(){
                //处理游戏进入后台的情况
                cc.log(" 手机端  cc.game.EVENT_HIDE TRUE-----22监听自定义事件");
                self._hideViewAction();
            });
            cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function(){
                //处理返回游戏的情况
                cc.log("手机端   cc.game.EVENT_SHOW TRUE-----22监听自定义事件");
                cc.mile.view_event_show = true;
                self._showViewAction();
            });
            
        }else {
            cc.game.on(cc.game.EVENT_HIDE, function (event) {
                //self.alert("HIDE TRUE");
                cc.log("HIDE TRUE");
                cc.mile.view_event_show = false;
                if(cc.mile.gamestatus != "notready" && self.sendCount % 2 ==0){
                    self._hideViewAction();
                }
                self.sendCount++;

            });
            cc.game.on(cc.game.EVENT_SHOW, function (event) {
                cc.log("SHOW TRUE");
                //self.alert("SHOW TRUE");
                if(cc.mile.gamestatus != "notready" && self.sendCount % 2 ==0){

                    self._showViewAction();
                }
                self.sendCount++;
            });

        }


        cc.mile.socket.on('connect', function (data) {

            var beginTime = Date.now();
            cc.log("data.链接完成 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");

            gameUtils.socketOptions.retryTimes = 0;

            self.connectedFlag = true;
            cc.mile.socket.connectedFlag = true;


            console.log("connected to server");
            //self.alert("connected to server");
            if (self.initSocketEvent) {
                console.log("connected to initSocketEvent");
                self.initSocketEvent();
            }
            if(!cc.mile.recreateConnectionFlag || cc.mile.socket.reconnectedFlag){

                self._showViewAction();

            }else {
                var onlineParams = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi,
                    roomlinestatus: 'online'
                };
                self.emitSocketRequest("roomlinestatus", JSON.stringify(onlineParams));
                if(cc.mile.reconnectCommand){
                    self.emitSocketRequest(cc.mile.reconnectCommand,cc.mile.reconnectParams);
                    self.closeloadding();
                }
                
            }
            if(cc.mile.inTeaHouse){
                self.emitGetTeahousInfo();
            }
            cc.mile.reconnectCommand = null;
            cc.mile.reconnectParams = null;
            cc.mile.socket.reconnectedFlag = false;
            cc.mile.recreateConnectionFlag = false;

            self.closeloadding();
        });

        cc.mile.socket.on('disconnect', function (data) {
            console.log("disconnected from server");
            //self.alert("disconnected from server");
            cc.mile.socket.connectedFlag = false;
            cc.mile.socket.reconnectedFlag = false;
            if(cc.mile.recreateConnectionFlag){
                self.showCenterToast("网络连接失败，请重试!")
            }
            cc.mile.recreateConnectionFlag = false;
            self.closeloadding();
            if(cc.mile.view_event_show){
                self.connect(true);
            }
        });


        cc.mile.socket.on('updateuser', function (data) {
            cc.log("updateuser");
            cc.log(data);
            //self.alert("disconnected from server");
            var callbackData = self.parse(data);
            if(cc.mile.user && callbackData.playUserClient){
                cc.mile.user.diamonds = callbackData.playUserClient.diamonds
                cc.mile.user.goldcoins = callbackData.playUserClient.goldcoins
                var  io = require("IOUtils")
                io.put("userinfo", JSON.stringify(cc.mile.user));
            }

            if(cc && cc.eventManager){
                cc.eventManager.dispatchCustomEvent('updateUserInfo')
            }

        });


        cc.mile.socket.on("gamestatus", function (result) {

            cc.log("gamestatus[" + JSON.stringify(result) + "]");

            debugger
            if (result != null) {

                var data = self.parse(result);
                if(data.roomtype){
                    cc.mile.createroomtype = data.roomtype;
                }
                cc.mile.gamestatus = data.gamestatus;
                if(data.teahouse && data.teahouse.id){

                    cc.mile.teahouse = data.teahouse;
                    cc.mile.tablenum = data.tablenum;
                }


                var beginTime = Date.now();
                cc.mile.beginTime = beginTime;
                cc.log("data.开始时间 [" + beginTime + "]");

                if (data.extparams) {
                    self.loadding();
                    cc.mile.extparams = data.extparams;
                    self.loadToHall(data);
                }else if (cc.mile.extparams != null) {
                    self.loadToHall(data);
                } else {
                    self.scene("mazihall", self);
                }
                cc.log("data.gamestatus [" + data.gamestatus + "] data.gametype  [" + data.gametype +
                    "] cc.mile.extparams.gametype[" + cc.mile.extparams + "]");

            }


        });



        /**
         * error创建茶楼
         */
        cc.mile.socket.on("error", function (result) {
            var data = self.parse(result);
            if(data && data.context){
                self.showCenterToast(data.context);
            }
            self.closeloadding();
        });


        cc.mile.socket.on('reconnecting', function (data) {
            console.log("reconnecting from server");
            //self.alert("disconnected from server");
            cc.mile.socket.connectedFlag = false;
            cc.mile.socket.reconnectedFlag = true;
            self.showCenterToast("正在获取数据，请稍后!")
            self.loadding();
        });

        /**
         * error创建茶楼
         */
        cc.mile.socket.on("connect_error", function (result) {
            cc.log("connect_error链接出错： "+JSON.stringify(result));
            cc.mile.socket.reconnectedFlag = false;

            cc.mile.recreateConnectionFlag = false;

            self.closeloadding();
            self.showCenterToast('网络不稳定,正在重试,请稍后...');
        });


        /**
         * error创建茶楼
         */
        cc.mile.socket.on("reconnect_failed", function (result) {
            cc.log("reconnect_failed链接失败： "+JSON.stringify(result));
            cc.mile.recreateConnectionFlag = false;
            //self.myreconnection('网络链接失败,正在重试,请稍后...');
            self.closeloadding();
        });

        /**
         * error创建茶楼
         */
        cc.mile.socket.on("connect_timeout", function (result) {
            cc.log("链接超时： "+JSON.stringify(result));

            cc.mile.recreateConnectionFlag = false;

            self.showCenterToast('网络链接超时,正在重试,请稍后...');
            self.closeloadding();
        });


        /**
         * 全局的消息通知
         */
        cc.mile.socket.on("notice", function (result) {
            var data = self.parse(result);
            self.showCenterToast(data.context);

        });
    },

    closeSocketListener: function () {
        if (cc.mile.socket && cc.mile.socket.off){
            cc.mile.socket.off(cc.game.EVENT_HIDE);
            cc.mile.socket.off(cc.game.EVENT_SHOW);
            cc.mile.socket.off("connect");
            cc.mile.socket.off("disconnect");
            cc.mile.socket.off("updateuser");
            cc.mile.socket.off("gamestatus");
            cc.mile.socket.off("connect_error");
            cc.mile.socket.off("reconnect_failed");
            cc.mile.socket.off("connect_timeout");
            cc.mile.socket.off("error");
            cc.mile.socket.off("notice");
        }

        cc.eventManager.removeCustomListeners(cc.game.EVENT_HIDE);
        cc.eventManager.removeCustomListeners(cc.game.EVENT_SHOW);

    },
    loadToHall: function (data) {

        if (data.gamestatus == "playing" && data.gametype != null) {
            /**
             * 修正重新进入房间后 玩法被覆盖的问题，从服务端发送过来的 玩法数据是 当前玩家所在房间的玩法，是准确的
             */
            cc.log("data.playway [" + data.playway + "] data.gametype   [" + data.gametype + "] data.cardroom [" + data.cardroom + "]");
            if (cc.mile.extparams != null) {

                cc.mile.extparams.playway = data.playway;
                cc.mile.extparams.gametype = data.gametype;
                if (data.cardroom != null && data.cardroom == true) {
                    cc.mile.extparams.gamemodel = "room";
                }
            }
            this.scene(data.gametype, this);
        } else if (data.gamestatus == "timeout") { //会话过期，退出登录 ， 会话时间由后台容器提供控制
            cc.mile.sessiontimeout = true;
            this.alert("登录已过期，请重新登录");
        } else {
            cc.log("加载类型111 cc.mile.extparams.gametype【" + cc.mile.extparams.gametype + "】");
            this.scene(cc.mile.extparams.gametype, this);
        }
    },
    sceneMz: function (gametype) {
        this.scene(gametype, this);
    },
    disconnect: function () {
        if (cc.mile.socket != null) {
            cc.mile.socket.disconnect();
            cc.mile.socket = null;
        }
    },
    emitSocketRequest: function (command, params) {

        if(cc.mile.socket.connectedFlag){
            cc.log(command + "socket cc请求:" + params);
            cc.mile.socket.emit(command, params);
            this.loggerInfo("发送事件:"+command + ",时间："+Date.now());
        }else {
            cc.log(command + "重连 cc请求:" + params);
            if(!cc.mile.recreateConnectionFlag){
                this.loadding();
                cc.mile.reconnectCommand = command;
                cc.mile.reconnectParams = params;
                this.connect(true);
            }
        }
    },
    registercallback: function (callback) {
        cc.mile.room_callback = callback;
    },
    cleancallback: function () {
        cc.mile.room_callback = null;
    },
    getCommon: function (common) {
        var object = cc.find("Canvas/script/" + common);
        return object.getComponent(common);
    },
    getPrefabCommon: function (prefabName, common) {
        var object = cc.find("Canvas/" + prefabName + "/script/" + common);
        return object.getComponent(common);
    },
    loadding: function () {
        if (cc.mile.loadding.size() > 0) {
            this.loaddingDialog = cc.mile.loadding.get();
            this.loaddingDialog.parent = cc.find("Canvas");

            this._animCtrl = this.loaddingDialog.getComponent(cc.Animation);
            var animState = this._animCtrl.play("loadding");
            animState.wrapMode = cc.WrapMode.Loop;
        }
    },
    alert: function (message) {
        if (cc.mile.dialog.alert.size() > 0) {
            this.alertDialog = cc.mile.dialog.alert.get();
            this.alertDialog.parent = cc.find("Canvas");
            var node = this.alertDialog.getChildByName("message");
            if (node != null && node.getComponent(cc.Label)) {
                node.getComponent(cc.Label).string = message;
            }
        }
        this.closeloadding();
    },
    confirm: function (message, okCallback) {
        if (cc.mile.dialog.confirm.size() > 0) {
            this.confirmDialog = cc.mile.dialog.confirm.get();
            this.confirmDialog.parent = cc.find("Canvas");
            var node = this.confirmDialog.getChildByName("message");
            if (node != null && node.getComponent(cc.Label)) {
                node.getComponent(cc.Label).string = message;
            }
            cc.mile.confirm_ok_callback = okCallback;
        }
        this.closeloadding();

    },
    closeloadding: function () {
        if (cc.find("Canvas/loadding")) {
            cc.mile.loadding.put(cc.find("Canvas/loadding"));
        }
    },
    closeOpenWin: function () {
        if (cc.mile.openwin != null) {
            cc.mile.openwin.destroy();
            cc.mile.openwin = null;
        }
    },
    closeOpenWin2: function () {
        if (cc.mile.openwin2 != null) {
            cc.mile.openwin2.destroy();
            cc.mile.openwin2 = null;
        }
    },
    pvalistener: function (context, func) {
        cc.mile.listener = func;
        cc.mile.context = context;
    },
    cleanpvalistener: function () {
        cc.mile.listener = null;
        cc.mile.context = null;
    },
    pva: function (pvatype, balance) {   //客户端资产变更（仅显示，多个地方都会调用 pva方法）
        if (pvatype != null) {
            if (pvatype == "gold") {
                cc.mile.user.goldcoins = balance;
            } else if (pvatype == "cards") {
                cc.mile.user.cards = balance;
            } else if (pvatype == "diamonds") {
                cc.mile.user.diamonds = balance;
            }
        }
    },
    updatepva: function () {
        if (cc.mile.listener != null && cc.mile.context != null) {
            cc.mile.listener(cc.mile.context);
        }
    },
    resize: function () {
        var win = cc.director.getWinSize();
        cc.view.setDesignResolutionSize(win.width, win.height, cc.ResolutionPolicy.EXACT_FIT);
    },
    closeAlert: function () {
        if (cc.find("Canvas/alert")) {
            cc.mile.dialog.alert.put(cc.find("Canvas/alert"));
        }
    },
    closeConfirm: function () {
        if (cc.find("Canvas/confirm")) {
            cc.mile.dialog.confirm.put(cc.find("Canvas/confirm"));
        }
    },
    scene: function (name, self) {
        cc.director.preloadScene(name, function () {
            if (cc.mile) {
                self.closeloadding(self.loaddingDialog);
                self.closeAlert(self.alertDialog);
            }
            cc.director.loadScene(name);
        });
    },
    preload: function (extparams) {
        this.loadding();
        /**
         *切换游戏场景之前，需要先检查是否 是在游戏中，如果是在游戏中，则直接进入该游戏，如果不在游戏中，则执行 新场景游戏
         */
        cc.mile.extparams = extparams;
        /**
         * 发送状态查询请求，如果玩家当前在游戏中，则直接进入游戏回复状态，如果玩家不在游戏中，则创建新游戏场景
         */


        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        };
        this.emitSocketRequest("gamestatus", JSON.stringify(param));
    },
    root: function () {
        return cc.find("Canvas");
    },
    decode: function (data) {
        var cards = new Array();

        if (!cc.sys.isNative) {
            var dataView = new DataView(data);
            for (var i = 0; i < data.byteLength; i++) {
                cards[i] = dataView.getInt8(i);
            }
        } else {
            var Base64 = require("Base64");
            var strArray = Base64.decode(data);

            if (strArray && strArray.length > 0) {
                for (var i = 0; i < strArray.length; i++) {
                    cards[i] = strArray[i];
                }
            }
        }

        return cards;
    },
    parse: function (result) {
        return gameUtils.jsonParse(result);

    },
    reset: function (data, result) {
        //放在全局变量
        cc.mile.authorization = data.token.id;
        cc.mile.user = data.data;
        cc.mile.games = data.games;
        cc.mile.gametype = data.gametype;
        cc.mile.ip = data.token.ip;

        cc.mile.data = data;
        cc.mile.playway = null;
        this.io.put("userinfo", result);

    },

    logout: function () {
        this.closeOpenWin();
        cc.mile.authorization = null;
        cc.mile.user = null;
        cc.mile.games = null;
        cc.mile.playway = null;


        gameUtils.setStorageToLocal(GameConstants.USER_TOKEN_KEY,'');
        this.disconnect();
    },
    socket: function () {
        var socket = cc.mile.socket;
        if (socket == null) {
            socket = this.connect(false);
        }
        return socket;
    },
    map: function (command, callback) {
        if (cc.mile != null && cc.mile.routes[command] == null) {
            cc.mile.routes[command] = callback || function () {
            };
        }
    },
    cleanmap: function () {
        if (cc.mile != null && cc.mile.routes != null) {
            //cc.mile.routes.splice(0 , cc.mile.routes.length) ;
            for (var p in cc.mile.routes) {
                delete cc.mile.routes[p];
            }
        }
    },
    route: function (command) {
        return cc.mile.routes[command] || function () {
        };
    },
    /**
     * 解决Layout的渲染顺序和显示顺序不一致的问题
     * @param target
     * @param func
     */
    layout: function (target, func) {
        if (target != null) {
            var temp = new Array();
            var children = target.children;
            for (var inx = 0; inx < children.length; inx++) {
                temp.push(children[inx]);
            }
            for (var inx = 0; inx < temp.length; inx++) {
                target.removeChild(temp[inx]);
            }

            temp.sort(func);
            for (var inx = 0; inx < temp.length; inx++) {
                temp[inx].parent = target;
            }
            temp.splice(0, temp.length);
        }
    },

    loadUrlJPGImage: function (url, node) {
        if (url) {
            if(url.indexOf("null") != -1 || url.indexOf("undefined") != -1  ){
                var sprite = node.getComponent(cc.Sprite);
                sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/other/empty_head.png"));
            }else {

                var imageUrl = cc.mile.http.baseURL + "/api2/loadingImage?imageUrl=" + url;

                this.loadUrlImage({ url: imageUrl, type: 'jpg' }, node);
            }
        }else  {
            var sprite = node.getComponent(cc.Sprite);
            sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/other/empty_head.png"));
        }

    },

    loadUrlImage: function (urlParams, node) {
        cc.log(urlParams.url);
        var cacheKey = urlParams.url;
        if (cc.mile.clientCaches[cacheKey]) {
            var sprite = node.getComponent(cc.Sprite);
            sprite.spriteFrame = new cc.SpriteFrame();
            sprite.spriteFrame.setTexture(cc.mile.clientCaches[cacheKey]);

        } else {
            cc.loader.load(urlParams, function (err, texture) {

                if (!err) {

                    cc.mile.clientCaches[urlParams.url] = texture;
                    var sprite = node.getComponent(cc.Sprite);
                    sprite.spriteFrame = new cc.SpriteFrame();
                    sprite.spriteFrame.setTexture(texture);

                } else {
                    cc.log("加载失败", err);
                }
            });
        }

    },
    emitGetTeahousInfo: function () {
        var roomType = gameUtils.getStorageToLocal(GameConstants.CREATE_ROOM_TYPE_KEY);
        if (!roomType) {
            roomType = GameConstants.CREATE_ROOM_TYPE_CARD;
        }
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            roomtype: roomType,
            teanum: cc.mile.teahouse.teanum,
        };
        this.emitSocketRequest("getteahouse", JSON.stringify(param));
    },
    getSexPathPrefix: function () {
        var userSex = cc.mile.user.gender;
        if (userSex == 2 || userSex == '2') {
            return 'nv'
        }
        return 'nan';
    },
    showCenterToast: function (msg) {
        gameUtils.showCenterToast(msg);

    },
    loggerInfo: function (content) {
        cc.log(content);
        var userInfo = cc.mile.user.id +":"+ cc.mile.user.username;
        cc.mile.http.httpPost("/api2/catchErrorInfo",
            {
                token: cc.mile.authorization,
                infoType: 'info',
                content: userInfo+content
            });
    },
    loggerError: function (content) {
        cc.log(content);
        var userInfo = cc.mile.user.id +":"+ cc.mile.user.username;
        cc.mile.http.httpPost("/api2/catchErrorInfo",
            {
                token: cc.mile.authorization,
                content: userInfo+content
            });
    }
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
