var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.TOUCH_START, function(e){
            e.stopPropagation();
        });
        /**
         * 关闭ALERT的回调动作
         */
        this.node.on("close", function (event) {
            if(cc.mile!=null && cc.mile.sessiontimeout == true){
                cc.mile.sessiontimeout = null;
                self.scene("login" , self) ;
            }
            event.stopPropagation();
        });
    },
    onConfirmOk:function(){
        var self=this;
        if(cc.mile.confirm_ok_callback!=null) {
            cc.mile.confirm_ok_callback(self);
        }
    },
    onConfirmCancel:function(){
        var dialog = cc.find("Canvas/confirm") ;
        cc.mile.dialog.confirm.put(dialog);
        this.node.dispatchEvent( new cc.Event.EventCustom("close", true) );
    },
    onClose:function(){
        var dialog = cc.find("Canvas/alert") ;
        cc.mile.dialog.alert.put(dialog);
        this.node.dispatchEvent( new cc.Event.EventCustom("close", true) );
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
