var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        playway:{
            default : null ,
            type : cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad :function() {},

    onClick : function() {
        if(this.playway!=null){
            var script = this.playway.getComponent("RoomPlayway") ;
            var roomplayway = cc.instantiate(script.roomoption) ;
            cc.mile.openwin = roomplayway ;
            cc.mile.openwin.parent = this.root();
            var roomoption = roomplayway.getComponent("RoomOption") ;
            cc.log("玩法参数 script.data ["+script.data+"]");
            if(roomoption!=null){
                roomoption.init(script.data);
            }
        }
    },

    // update (dt) {},
});
