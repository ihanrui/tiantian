/**
 * 网达棋牌  工具类
 */
var PictureNumber = require("PictureNumber");

var WangdaGameUtils = cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },
    statics: {
        /**
         * socket 链接配置
         */
        socketOptions: {
            "reconnectionDelay": 1,
            "retryTimes": 0,
            "maxReconnections": 1000,
        },
        /**
         * 复制操作
         * @param text
         */
        copyToClipboard: function (text) {

            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                    "copyToClipboard", "(Ljava/lang/String;)V", text);
            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

                jsb.reflection.callStaticMethod("AppController",
                    "copyToClipboard:", text);

            }
        },

        getViewCenterX: function () {
            return cc.view.getFrameSize().height / 2;
        },
        getViewCenterY: function () {
            return cc.view.getFrameSize().width / 2;
        },
        /**

         NONE(0),    //无牌型
         ONE(1),        //单张
         TWO(2),        //对子
         THREE(3),    //三张
         FOUR(4),    //四带
         LINK2(5),    //连对
         LINK3(6),   //连三张
         LINK4(7),    //连四张


         F510K(11),//副510
         FK510K(12),//方块510K
         MH510K(13),//梅花510K
         HONGT510K(14),//红桃510
         HEIT510K(15),//黑桃510

         GHOST2XW(16),//（两个）小王

         GHOST2DX(17),//大小王

         GHOST2DW(18),//(两个)大王

         BOMB5(19),//5炸
         GHOST3(20),//3个大小王
         BOMB6(21),//6炸
         GHOST4(22),//4个大小王
         BOMB7(23),//7炸
         BOMB8(24);//8炸
         * @param data
         */

        getMaziCardTypeVoiceName: function (cardtype, value) {

            cc.log("获取牌的声音cardtype:" + cardtype + ',value：' + value);

            switch (cardtype.mazicardtype) {


                case 1:
                    var cardValue = parseInt(value / 8) + 3;
                    if (cardValue == 16) {
                        var cardFloatValue = value % 8;
                        if (cardFloatValue > 1) {
                            return "dpdagui.mp3";
                        }
                        return "dpxiaogui.mp3";
                    }
                    return "dp" + cardValue + ".mp3";

                case 2:
                    var cardValue = parseInt(value / 8) + 3;
                    if (cardValue == 16) {
                        var cardFloatValue = value % 8;
                        if (cardFloatValue > 1) {
                            return "dzdagui.mp3";
                        }
                        return "dzxiaogui.mp3";
                    }
                    return "dz" + cardValue + ".mp3";

                case 3:
                    var cardValue = parseInt(value / 8) + 3;
                    if (cardValue == 16) {
                        return "szgui.mp3";
                    }
                    return "sz" + cardValue + ".mp3";
                case 4:
                    var cardValue = parseInt(value / 8) + 3;
                    if (cardValue == 16) {
                        return "sibangui.mp3";
                    }
                    return "siban" + cardValue + ".mp3";
                case 5:
                    return "liandui" + cardtype.typesize + ".mp3";
                case 6:
                    return "feiji.mp3";
                case 7:
                    return "feiji.mp3";
                case 11:
                    return "F510k.mp3";
                case 12:
                    return "Z510k.mp3";
                case 13:
                    return "Z510k.mp3";
                case 14:
                    return "Z510k.mp3";
                case 15:
                    return "Z510k.mp3";
                case 16:
                    return "dzxiaogui.mp3";
                case 17:
                    return "dzdagui.mp3";
                case 18:
                    return "dzdagui.mp3";
                case 19:
                    var cardValue = parseInt(value / 8) + 3;
                    return "wuban" + cardValue + ".mp3";
                case 20:
                    return "szgui.mp3";
                case 21:
                    return "banzi6.mp3";
                case 22:
                    return "sibangui.mp3";
                case 23:
                    return "banzi7.mp3";
                case 24:
                    return "banzi8.mp3";
            }
            return "";
        },
        /**
         *
         * NONE(0), //无点
         S1(1),  //score 1 1点
         S2(2),  //score 2 2点
         S3(3),  //score 3 3点
         S4(4),  //score 4 4点
         S5(5),  //score 5 5点
         S6(6),   //score 6 6点
         S7(7),  //score 7 7点
         S8(8),  //score 8 8点
         S9(9),   //score 9 点
         S10(10), //10点
         SZ(11),//顺子
         TH(12),//同花
         WH(13),//五花
         HL(14),//葫芦
         ZD(15),//炸弹
         WX(16);//五小
         * @param cardtype
         */
        getNiuniuVoiceName: function (cardType) {
            if (cardType) {

                return "res_" + cardType.cardtype + ".mp3";
            }
            return "";
        },


        showNumberNode: function (totalscore, parentNode) {

            var numbSource = "resources/images/common/pdk_num.png";
            if (totalscore < 0) {
                numbSource = "resources/images/common/pdk_num_blue.png";
            }
            cc.loader.load(cc.url.raw(numbSource),
                function (errors, results) {
                    if (errors) {
                        cc.log(errors);
                    }
                    else {

                        /**
                         * * @param paramNumber   需要转换的数字
                         * @param paramTexture  图片的texture
                         * @param splitSize  图片等分数量
                         * @param parentNode  父节点
                         * @param positionX  开始的X 坐标
                         * @param positionY   开始的Y 坐标
                         */
                        var pNum = new PictureNumber();
                        pNum.buildNumber({
                            paramNumber: totalscore,
                            paramTexture: results,
                            splitSize: 12,
                            parentNode: parentNode,
                            positionX: 0,
                            positionY: 0
                        });

                        //pNum.setPosition(200, 200);
                        //pNum.setAnchorPoint(0, 0);
                        //cc.director.getScene().addChild(pNum);
                    }
                });
        },

        showScoreNode: function showNumberNode(totalscore, parentNode) {
            var numbSource = "resources/images/room/niuniu/status/imgBankerScore.png";
            this.showScoreNodeWidthImg(totalscore, parentNode, numbSource);

        },
        showScoreNodeWidthImg: function showNumberNode(totalscore, parentNode, numbSource) {

            cc.loader.load(cc.url.raw(numbSource), function (errors, results) {
                if (errors) {
                    cc.log(errors);
                } else {

                    /**
                     * * @param paramNumber   需要转换的数字
                     * @param paramTexture  图片的texture
                     * @param splitSize  图片等分数量
                     * @param parentNode  父节点
                     * @param positionX  开始的X 坐标
                     * @param positionY   开始的Y 坐标
                     */
                    var pNum = new PictureNumber();
                    pNum.buildNumber({
                        paramNumber: totalscore,
                        paramTexture: results,
                        splitSize: 10,
                        parentNode: parentNode,
                        positionX: 0,
                        positionY: 0,
                        ignoreFlag: true
                    });

                }
            });
        },


        /**
         * 截屏
         * @param  {[function]} func 回调函数
         * @return
         */
        screenShoot: function (func) {
            if (!cc.sys.isNative) {
                return;
            }
            cc.log("--screenShoot--begin---");


            var dirpath = jsb.fileUtils.getWritablePath() + 'ScreenShoot/';
            if (!jsb.fileUtils.isDirectoryExist(dirpath)) {
                jsb.fileUtils.createDirectory(dirpath);
            }
            var name = 'ScreenShoot-' + (new Date()).valueOf() + '.png';


            cc.log("--screenShoot--begin---:" + name);
            var filepath = dirpath + name;

            var size = cc.winSize;
            var rt = cc.RenderTexture.create(size.width, size.height,
                cc.Texture2D.PIXEL_FORMAT_RGBA8888, gl.DEPTH24_STENCIL8_OES);
            //var rt = cc.RenderTexture.create(size.width, size.height);
            cc.director.getScene()._sgNode.addChild(rt);
            rt.setVisible(false);
            rt.begin();
            cc.director.getScene()._sgNode.visit();
            rt.end();


            cc.log("--screenShoot--end2---:" + filepath);


            rt.saveToFile('ScreenShoot/' + name, cc.ImageFormat.JPG, true, function () {
                cc.log('save succ');
                rt.removeFromParent();
                if (func) {
                    func(filepath);
                }
            });
        },

        /**
         * 截屏
         * @param  {[function]} func 回调函数
         * @return
         */
        screenShoot22: function (func) {
            if (!cc.sys.isNative) {
                if (func) {
                    func('test');
                }
                return;
            }

            var size = cc.director.getWinSize();
            var fileName = "result_share.jpg";
            var fullPath = jsb.fileUtils.getWritablePath() + fileName; //拿到可写路径，将图片保存在本地，可以在ios端或者java端读取该文件
            if (jsb.fileUtils.isFileExist(fullPath)) {
                jsb.fileUtils.removeFile(fullPath);
            }
            var texture = new cc.RenderTexture(Math.floor(size.width), Math.floor(size.height));
            texture.setPosition(cc.p(size.width / 2, size.height / 2));
            texture.begin();
            cc.director.getRunningScene().visit(); //这里可以设置要截图的节点，设置后只会截取指定节点和其子节点
            texture.end();

            cc.log("--screenShoot--end2aah---:" + fullPath);
            texture.saveToFile(fileName, cc.ImageFormat.JPG, true, function () {
                cc.log('save succ');
                //texture.removeFromParent();
                if (func) {
                    func(fullPath);
                }
            });

        },

        /**
         * imageShare
         * @param text
         */
        shareImage: function (path) {
            cc.log("---shareImage---：" + path);

            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
                cc.log("---shareImage---android：" + path);

                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                    "imageShare", "(Ljava/lang/String;)V", path);
            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
                cc.log("---shareImage---ios:" + path);
                jsb.reflection.callStaticMethod("AppController",
                    "imageShare:", path);
            }
            cc.log("---shareImage---end:" + path);
        },

        loadQCcodeJPGImage: function loadUrlJPGImage(url, node) {
            if (url != null && url != undefined && url != "") {

                var imageUrl = cc.mile.http.baseURL + "/api2/loadingInviteImage?imageUrl=" + url;

                this.loadUrlImage({url: imageUrl, type: 'jpg'}, node);
            }
        },

        loadUrlImage: function loadUrlImage(urlParams, node) {
            cc.log(urlParams.url);
            var cacheKey = urlParams.url;
            if (cc.mile.clientCaches[cacheKey]) {
                var sprite = node.getComponent(cc.Sprite);
                sprite.spriteFrame = new cc.SpriteFrame();
                sprite.spriteFrame.setTexture(cc.mile.clientCaches[cacheKey]);
            } else {
                cc.loader.load(urlParams, function (err, texture) {

                    if (!err) {

                        cc.mile.clientCaches[urlParams.url] = texture;
                        var sprite = node.getComponent(cc.Sprite);
                        sprite.spriteFrame = new cc.SpriteFrame();
                        sprite.spriteFrame.setTexture(texture);
                    } else {
                        cc.log("加载二维码失败：" + JSON.stringify(err));
                        cc.log("加载失败", err);
                    }
                });
            }
        },
        showCenterToast: function (msg) {
            var toast = Toast.makeText(msg, Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
        },
        setStorageToLocal: function (key, value) {
            cc.log('信息存储--key:[' + key + ']--value:[' + value + ']');
            cc.sys.localStorage.setItem(key, value);
        },
        getStorageToLocal: function (key) {
            return cc.sys.localStorage.getItem(key);
        },
        jsonParse: function (result) {
            var data;
            if (typeof result === "string") {
                data = JSON.parse(result);
            } else {
                data = result;
            }
            return data;
        },

        getNiuniuCardTypeVoiceName: function (cardtype) {

            cc.log("获取牌的声音cardtype:" + cardtype);

            switch (cardtype) {

                case 0:
                    return "res_0.mp3";
                case 1:
                    return "res_1.mp3";
                case 2:
                    return "res_2.mp3";
                case 3:
                    return "res_3.mp3";
                case 4:
                    return "res_4.mp3";
                case 5:
                    return "res_5.mp3";
                case 6:
                    return "res_6.mp3";
                case 7:
                    return "res_7.mp3";
                case 8:
                    return "res_8.mp3";
                case 9:
                    return "res_9.mp3";
                case 9:
                    return "res_10.mp3";
            }
            return "";
        },
    },

    // use this for initialization
    onLoad: function () {
    },

});