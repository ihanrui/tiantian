var beiMiCommon = require("MileCommon");
var gameUtils = require("WangdaGameUtils");
var GameConstants = require("GameConstants");
var loginCommon = null;
cc.Class({
    extends: beiMiCommon,
    properties: {
        weixinLogin: {
            default: null,
            type: cc.Button
        },
        guestLogin: {
            default: null,
            type: cc.Button
        },
        weixinLogin2222: {
            default: null,
            type: cc.Button
        },
        confirmProtocol: {
            default: null,
            type: cc.Node
        },
        confirmProtocolStatus: {
            default: null,
            type: cc.Node
        },
        backgroundAnimation: {
            default: null,
            type: cc.Node
        },
    },
    // use this for initialization
    onLoad: function () {

    },
    confirmProtocolClick: function () {
        console.log("Protocol click！！");

        this.confirmProtocolStatus.active = !this.confirmProtocolStatus.active;

        this.guestLogin.interactable = this.confirmProtocolStatus.active;
        this.weixinLogin.interactable = this.confirmProtocolStatus.active;
        if(this.weixinLogin2222){
            this.weixinLogin2222.interactable = this.confirmProtocolStatus.active;
        }

    },
    login: function () {
        this.io = require("IOUtils");
        this.loadding();
        if (this.io.get("userinfo") == null) {
            //发送游客注册请求
            var xhr = cc.mile.http.httpGet("/api/guest?1=1" + this.getGPSLocation(), this.success, this.loginError, this);
        } else {
            //通过ID获取 玩家信息
            var data = JSON.parse(this.io.get("userinfo"));
            if (data.token != null) {     //获取用户登录信息
                cc.log("自己测试token data.token.id [" + data.token.id + "]");
                var xhr = cc.mile.http.httpGet("/api/guest?token=" + data.token.id + this.getGPSLocation(), this.success, this.loginError, this);
            }
        }
    },
    getGPSLocation: function () {
        var latitude = 0;
        var longitude = 0;
        var locationDesc = '';
        cc.log('---获取用户定位开始： latitude：[' + latitude + '],longitude:[' + longitude + ']');
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            latitude = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getLatitude", "()F");
            longitude = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getLongitude", "()F");
            locationDesc = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getGPSDesc", "()Ljava/lang/String;");

        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            latitude = jsb.reflection.callStaticMethod("AppController",
                "getLatitude");
            longitude = jsb.reflection.callStaticMethod("AppController",
                "getLongitude");
            locationDesc = jsb.reflection.callStaticMethod("AppController",
                "getGPSDesc");
        } else {
            latitude = 12.343455657;
            longitude = 112.098745634;
            locationDesc = '测试-gps地址信息'
        }
        cc.log('---获取用户定位结束： latitude：[' + latitude + '],' +
            'longitude:[' + longitude + ']' +
            'locationDesc:[' + locationDesc + ']');
        return encodeURI('&latitude=' + latitude + '&longitude=' + longitude + '&locationDesc=' + locationDesc);
    },
    wexinLoginClick: function () {
        cc.info("login start");
        this.io = require("IOUtils");
        this.loadding();
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            loginCommon = this;
            cc.checkLoginCallbackJS = this.checkLoginCallbackJS;

            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "loginWithWeixin", "()V");
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            loginCommon = this;
            cc.checkLoginCallbackJS = this.checkLoginCallbackJS;
            jsb.reflection.callStaticMethod("AppController", "sendAuthRequestWithApp");
        } else {
            //发送注册请求
            this.showCenterToast("请在手机上登录");
            this.closeloadding();
        }

    },
    onWexinLogin: function (code, msg) {
        cc.log('onWexinLogin user plugin:' + code);
        cc.log('onWexinLogin user plugin:' + msg);
    },
    success: function (result, object) {

        var data = JSON.parse(result);
        cc.log("返回数据："+JSON.stringify(data));
        if (data != null && data.token != null && data.data != null) {

            gameUtils.setStorageToLocal(GameConstants.USER_TOKEN_KEY, data.token.id);
            gameUtils.setStorageToLocal(GameConstants.USER_LOGIN_TYPE, GameConstants.USER_LOGIN_TYPE_GUEST);
            object.loginToHall(data, result);
        }
    },
    loginError: function (object) {

        object.closeloadding(object.loaddingDialog);
        object.showCenterToast("网络异常，服务访问失败");
        //object.alert("网络异常，服务访问失败");
        object.loginFlag = false;
    },
    autoLogin: function () {
        this.io = require("IOUtils");

        var token = gameUtils.getStorageToLocal(GameConstants.USER_TOKEN_KEY);
        var loginType = gameUtils.getStorageToLocal(GameConstants.USER_LOGIN_TYPE);
        cc.log('自动登录--token:[' + token + ']--loginType:[' + loginType + ']');
        if (token && token.length > 5) {
            if (loginType == GameConstants.USER_LOGIN_TYPE_GUEST) {
                this.loadding();
                cc.mile.http.httpGet("/api/guest?token=" + token + this.getGPSLocation(), this.success, this.loginError, this);
            } else if (loginType == GameConstants.USER_LOGIN_TYPE_WEIXIN) {
                this.loadding();
                cc.mile.http.httpGet("/api2/weixinlogin?token=" + token + this.getGPSLocation(), this.successNative, this.loginError, this);
                //cc.mile.http.httpGet("/api/guest?token=" + data.token.id, this.success, this.error, this);
            }
        }
    },
    update: function (dt) {

    },
    backgroundAnimationFinish: function () {
        if (this.backgroundAnimation != null) {
            this.backgroundAnimation.active = false;
        }
    },
    /**
     *
     *  code, openId,callbackMsg
     * @param callbackData
     */
    checkLoginCallbackJS: function (callbackData) {

        cc.log('原生代码回调232：' + JSON.stringify(callbackData));


        if (callbackData.status) {

            cc.mile.http.httpGet("/api2/weixinlogin?openId=" + callbackData.openId + "&code=" + callbackData.code + loginCommon.getGPSLocation(),
                loginCommon.successNative, loginCommon.loginError, loginCommon);
        } else {
            if (callbackData && callbackData.callbackMsg) {
                loginCommon.showCenterToast(callbackData.callbackMsg);
            }
        }
        loginCommon.closeloadding();
        cc.checkLoginCallbackJS = null;
        loginCommon = null;

    },
    successNative: function (result, object) {

        cc.log(result);
        cc.log("数据登录返回 111context[" + JSON.stringify(result) + "]");

        var data = JSON.parse(result);
        if (data != null && data.token != null
            && data.data != null
            && data.data.gameid != null
        ) {

            gameUtils.setStorageToLocal(GameConstants.USER_TOKEN_KEY, data.token.id);
            gameUtils.setStorageToLocal(GameConstants.USER_LOGIN_TYPE, GameConstants.USER_LOGIN_TYPE_WEIXIN);
            object.loginToHall(data, result);

        } else {
            object.loginFlag = false;
            object.showCenterToast("登录失败，请重新登录");
            object.closeloadding();
        }
    },
    loginToHall: function (data, result) {
        //放在全局变量
        cc.log("放在全局变量:" + result);
        cc.mile.extparams = null;
        this.reset(data, result);


        cc.mile.gamestatus = data.data.gamestatus;
        /**
         * 登录成功后即创建Socket链接
         */
        this.connect();

        //预加载场景
        cc.log(" 预加载场景 cc.mile.gametype[" + cc.mile.gametype + "]");

        this.syncPlayerGameStatus();
        /*if(cc.mile.gametype!=null && cc.mile.gametype != ""){//只定义了单一游戏类型 ，否则 进入游戏大厅
                this.scene(cc.mile.gametype , this) ;
            }else{
                /**
                 * 暂未实现功能
                 */
        //}
    },
    syncPlayerGameStatus: function () {

        this.scheduleOnce(function () {
            cc.log("--syncPlayerGameStatus--");
            if (this.connectedFlag) {

                //this.scene("mazihall", this);

                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi
                };

                this.emitSocketRequest("gamestatus", JSON.stringify(param));

            } else {
                this.syncPlayerGameStatus();
            }
        }, 0.2);
    }


    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
