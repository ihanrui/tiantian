cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        _progress:0.0,
        _splash:null,
        _isLoading:false,
        loaddingPrefab: {
            default: null,
            type: cc.Prefab
        },
        alertPrefab: {
            default: null,
            type: cc.Prefab
        },
        confirmPrefab:{
            default:null,
            type:cc.Prefab
        }
    },

    // use this for initialization
    onLoad: function () {
        if(!cc.sys.isNative && cc.sys.isMobile){
            var canvas = this.node.getComponent(cc.Canvas);
            canvas.fitHeight = true;
            canvas.fitWidth = true;
        }
        var win = cc.director.getWinSize() ;
        cc.view.setDesignResolutionSize(win.width, win.height, cc.ResolutionPolicy.EXACT_FIT);
        this.initMgr();

    },
    start:function(){        
        var self = this;
        var SHOW_TIME = 3000;
        var FADE_TIME = 500;
        /***
         * 
         * 控制登录界面或者广告首屏界面显示时间
         * 
         */
    },
    initMgr:function(){
        if(cc.mile == null){
            /**
             * 增加了游戏全局变量控制，增加了 cc.mile.gamestatus 参数，可选值：ready|notready|playing
             * @type {{}}
             */
            cc.mile = {};
            cc.mile.routes = {} ;
            cc.mile.clientCaches = {} ;
            cc.mile.http = require("HTTP");
            cc.mile.seckey = "mile";
            cc.mile.gamestatus = "none" ;



            cc.mile.dialog={};
            cc.mile.dialog.alert = null ;   //弹出的提示对话框，  alert
            cc.mile.dialog.confirm = null ;   //弹出的确认对话框，  confirm
            cc.mile.openwin = null ;  //弹出的对话窗口，    设置、玩法、战绩等等

            cc.mile.loadding = new cc.NodePool();
            cc.mile.loadding.put(cc.instantiate(this.loaddingPrefab)); // 创建节点

            cc.mile.dialog.alert = new cc.NodePool();
            cc.mile.dialog.alert.put(cc.instantiate(this.alertPrefab)); // 创建节点
            cc.mile.dialog.confirm = new cc.NodePool();
            cc.mile.dialog.confirm.put(cc.instantiate(this.confirmPrefab)); // 创建节点

            /**
             * 游客登录，无需弹出注册对话框，先从本地获取是否有过期的对话数据，如果有过期的对话数据，则使用过期的对话数据续期
             * 如果没有对话数据，则重新使用游客注册接口
             */
            // this.loginFormPool = new cc.NodePool();
            // this.loginFormPool.put(cc.instantiate(this.prefab)); // 创建节点
            cc.mile.game = {
                model : null ,
                playway : null,
                type:function(name){
                    var temp ;
                    if(cc.mile.games !=null){
                        for(var i=0 ; i<cc.mile.games.length ; i++){
                            var gamemodel = cc.mile.games[i] ;
                            for(var inx = 0 ; inx < gamemodel.types.length ; inx++){
                                var  type = gamemodel.types[inx] ;
                                if(type.code == name){
                                    temp = type ;
                                }
                            }
                        }
                    }
                    return temp ;
                }
            };

            var Audio = require("Audio");
            cc.mile.audio = new Audio();
            cc.mile.audio.init();

            if(cc.sys.isNative){
                window.io = SocketIO;
            }else{
                window.io = require("socket.io");
            }
            cc.mile.audio.playBGM();
   
        }
    }

});
