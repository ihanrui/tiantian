var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        myMark: {//我的战绩
            default: null,
            type: cc.Node
        },
        teaMark: {//茶馆战绩
            default: null,
            type: cc.Node
        },
        itemMark: { //战绩统计
            default: null,
            type: cc.Node
        },
        play_history_item_prefab: { //历史战绩
            default: null,
            type: cc.Prefab
        },
        play_history_detail_item_prefab: { //历史战绩
            default: null,
            type: cc.Prefab
        },
        play_history_container: { //战绩统计
            default: null,
            type: cc.Node
        },
        play_history_detail_container: { //战绩统计
            default: null,
            type: cc.Node
        },
        history_detail_back_node: { //战绩统计
            default: null,
            type: cc.Node
        },
    },

    onLoad: function () {
        //加载我的战绩
        this.queryRecord(cc.mile.user.gameid);
        this.initEvent();
        //this.onMyMarkClik();

        ////默认情况
        var myMark = this.myMark.children;
        myMark[0].active = false;
        myMark[1].active = true;
    },
    initEvent: function () {
        var self = this;
        this.node.on('historyCheckDetail', function (event) {
            if (self) {
            console.log('-----teaItemMousedown-----click--!');

            }
        });
    },
    onMyMarkClik: function () {

        var myMark = this.myMark.children;
        myMark[0].active = false;
        myMark[1].active = true;

        var teaMark = this.teaMark.children;
        teaMark[0].active = true;
        teaMark[1].active = false;

        this.queryRecord(cc.mile.user.gameid);
    },
    onTeaMarkClik: function () {

        var teaMark = this.teaMark.children;
        teaMark[0].active = false;
        teaMark[1].active = true;
        var myMark = this.myMark.children;
        myMark[0].active = true;
        myMark[1].active = false;

        this.queryRecord("");
    },
    queryRecord: function (gameid) {
        debugger
        var teanum = "";
        if (cc.mile.teahouse) {
            teanum = cc.mile.teahouse.teanum;
        }
        this.loadding();
        cc.log('数据查询：gameid'+gameid);
        cc.mile.http.httpPost("/record/query",
            {
                token: cc.mile.authorization,
                teanum: teanum,
                gameid:gameid
            },
            this.successGetHistory, this.errorGetHistory, this);
    },
    successGetHistory: function (result, object) {
        cc.log(result);
        object.closeloadding();
        try {
            var data = JSON.parse(result);
            object.loadHistory(data);

        }catch (e){
            cc.log("格式化信息出错:"+JSON.stringify(e));
        }
    },
    errorGetHistory: function (result, object) {
        cc.log(result);
        result.closeloadding();
        result.showCenterToast("数据请求出错，请重试");
    },

    /**
     * 查看详情
     * @param roomid
     */
    queryDetailRecord: function (roomid) {
        if(roomid){
            this.loadding();
            cc.mile.http.httpPost("/record/queryDetails",
                {
                    token: cc.mile.authorization,
                    roomid: roomid
                },
                this.successDetailGetHistory, this.errorDetailGetHistory, this);
        }
    },
    successDetailGetHistory: function (result, object) {
        cc.log(result);

        object.closeloadding();

        var data = JSON.parse(result);
        object.loadDetailHistory(data);

    },
    errorDetailGetHistory: function (result, object) {
        cc.log(result);

        result.closeloadding();
        result.showCenterToast("数据请求出错，请重试");
    },
    loadHistory: function (data) {

        ///删除战绩的子集
        if (this.play_history_container.children.length != 0)
        {
            this.play_history_container.removeAllChildren();
        }
        //删除战绩详情
        if (this.play_history_detail_container.children.length != 0) {
            this.play_history_detail_container.removeAllChildren();
        }
        this.play_history_detail_container.parent.parent.active = false;
        this.play_history_container.parent.parent.active = true;

        var indexX = 0;
        var indexY =-150;
        var self = this;
     
        for (var i = 0; i < data.length; i++) {
            

            cc.log('单条记录信息88111：'+JSON.stringify(data[i]));

            var player = cc.instantiate(this.play_history_item_prefab);
            var playItemRender = player.getComponent("PlayHistoryItemRender");
            player.setPosition(indexX, indexY - 270 * i);
            player.on('historyCheckDetailRender', function (event) {
                if (self) {
                    console.log('-----teaItemMousedown-----click--!');
                    var userData = event.getUserData();
                    self.queryDetailRecord(userData.roomId);
                }
            });
            player.parent = this.play_history_container;
            playItemRender.initView(data[i]);
            playItemRender.addDeskPlayers(data[i].summaryPlayer,data[i].roomtype);
        }
         this.play_history_container.height = 270 * data.length;
    },
    loadDetailHistory: function (data) {
        this.play_history_container.parent.parent.active = false;
        this.play_history_detail_container.parent.parent.active = true;
        //this.history_detail_back_node.active = true;
        var indexX = 0;
        var indexY = -110;
        for (var i = 0; i < data.length; i++) {

            var player = cc.instantiate(this.play_history_detail_item_prefab);
            var playItemRender = player.getComponent("PlayHistoryDetailItemRender");
            player.setPosition(indexX, indexY - 220 * i);
            player.parent = this.play_history_detail_container;
            playItemRender.initView(data[i]);
        }
        this.play_history_detail_container.height = 240 * data.length;
    },

    onLoadDetailHistoryBack: function () {
        this.play_history_detail_container.parent.parent.active = false;
        this.play_history_container.parent.parent.active = true;
        this.history_detail_back_node.active =false;
    },

    start: function () {

    },
    // update (dt) {},
});
