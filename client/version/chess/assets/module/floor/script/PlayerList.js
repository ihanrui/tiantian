var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        player_container_node: {
            default: null,
            type: cc.Node
        },
        playerItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        serachId: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            passflag: 'pass',
            role: 'normal',
        };
        this.queryPlayerRecord();
    },
    onSearch: function () {
        var gameid = this.serachId.getComponent(cc.EditBox).string;
        if (gameid == "") {
            this.showCenterToast("请输入玩家ID");
            return;
        }
        this.param.gameid = gameid;
        this.queryPlayerRecord();
    },
    //获取成员信息
    queryPlayerRecord: function () {
        cc.mile.http.httpPost("/teahouse/getmeminfoList", this.param, this.successGetPlayer, this.errorGetPlayer, this);
    },
    successGetPlayer: function (result, context) {
        cc.log(result);
        var data = context.parse(result);
        context.loadPlayer(data.content);
    },
    errorGetPlayer: function (result, context) {
        cc.log(result);
    },
    loadPlayer: function (players) {
        this.player_container_node.removeAllChildren();
        var self = this;
        var indexX = 5;
        if (players.length == 0 && this.serachId.getComponent(cc.EditBox).string != "") {
            this.showCenterToast("玩家不存在或已经是小二");
            return;
        }
        for (var i = 0; i < players.length; i++) {
            var player = cc.instantiate(this.playerItemPrefab);
            player.setPosition(indexX, -50 - 105 * i);
            player.parent = this.player_container_node;
            player.on('checkEmitCallback', function (event) {
                console.log('-----checkEmitCallback------!');
                if (self) {
                    self.queryApprovedRecord();
                }
            });
            var playRender = player.getComponent("PlayerItemManager");
            playRender.init(i, players[i]);
        }
        this.player_container_node.height = players.length * 110;
    },
    // update (dt) {},
});
