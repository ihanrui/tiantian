
/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        user_number_label: {
            default: null,
            type: cc.Label
        },
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_id_label: {
            default: null,
            type: cc.Label
        },
        user_bean_label: {
            default: null,
            type: cc.Label
        },
        user_score_label: {
            default: null,
            type: cc.Label
        },
        user_head_node: {
            default: null,
            type: cc.Node
        },
        playerTeaCreditScorePrefab: {
            default: null,
            type: cc.Prefab
        },
        playerTeaCreditScoreListPrefab: {
            default: null,
            type: cc.Prefab
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },
    initEvent: function () {
        var self = this;
        this.node.on('onRemovePlayer', function (event) {
            console.log('-----onRemovePlayer-----click--!');
            if (self) {
                self.removeTeaPlayer();
            }
        });

        //审核通过
        this.node.on('onApprovePlayer', function (event) {
            console.log('-----onApprovePlayer-----click--!');
            if (self) {
                self.approveTeaPlayer();
            }
        });
        //审核拒绝
        this.node.on('onRejectPlayer', function (event) {
            console.log('-----onRejectPlayer-----click--!');
            if (self) {
                self.rejectTeaPlayer();
            }
        });
        //添加店小二
        this.node.on('onAddDianxiaoerTeaPlayer', function (event) {
            console.log('-----onAddDianxiaoerTeaPlayer-----click--!');
            if (self) {
                self.addDianxiaoerTeaPlayer();
            }
        });
        //移除店小二
        this.node.on('onRemoveDianxiaoerPlayer', function (event) {
            console.log('-----onRemoveDianxiaoerPlayer-----click--!');
            if (self) {
                self.removeDianxiaoerTeaPlayer();
            }
        });

        //上下分
        this.node.on('onPlayerTeaCreditScore', function (event) {
            console.log('-----onPlayerTeaCreditScore-----click--!');
            if (self) {
                self.playerTeaCreditScore();
            }
        });
        //上下分历史记录
        this.node.on('onPlayerTeaCreditScoreList', function (event) {
            console.log('-----onPlayerTeaCreditScoreList-----click--!');
            if (self) {
                self.playerTeaCreditScoreList();
            }
        });

    },

    /**
     *
     *  APPLY,    //申请
     PASS,   //审核通过
     OUT,     //离开茶楼
     REFUSE,  //拒绝
     FORBID;//禁止加入
     */
    removeTeaPlayer: function () {
        var self = this;
        this.confirm('玩家将不能在茶楼玩游戏，请确认是否删除！', function () {
            self.emitUpdateTeaPlayerSocket('out', 'normal');
            self.closeConfirm();
        });
    },
    //审核通过
    approveTeaPlayer: function () {
        this.emitUpdateTeaPlayerSocket('pass', 'normal');
    },
    //审核拒绝
    rejectTeaPlayer: function () {
        this.emitUpdateTeaPlayerSocket('refuse', 'normal');
    },
    //移除店小二
    removeDianxiaoerTeaPlayer: function () {
        this.emitUpdateTeaPlayerSocket('pass', 'normal');
    },
    //添加店小二
    addDianxiaoerTeaPlayer: function () {
        this.emitUpdateTeaPlayerSocket('pass', 'manager');
    },
    //打开成员上下分
    playerTeaCreditScore: function () {
        var opwin = cc.instantiate(this.playerTeaCreditScorePrefab)
        var render = opwin.getComponent("PlayerTeaCreditScore");
        render.initData(this.playData);
        render.initView();
        cc.mile.openwin2 = opwin;
        cc.mile.openwin2.parent = this.root();
    },
    //打开成员上下分列表
    playerTeaCreditScoreList: function () {
        var opwin = cc.instantiate(this.playerTeaCreditScoreListPrefab)
        var render = opwin.getComponent("PlayerTeaCreditScoreList");
        render.initView(this.playData.gameid);
        cc.mile.openwin2 = opwin;
        cc.mile.openwin2.parent = this.root();
    },
    emitUpdateTeaPlayerSocket: function (passflag, role) {
        var gameid = this.playData.gameid;
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            gameid: gameid,
            passflag: passflag,
            role: role,
        };
        this.emitSocketRequest("memupdate", JSON.stringify(param));
        this.checkEmitCallback();
    },
    checkEmitCallback: function () {
        debugger
        this.scheduleOnce(function () {
            if (cc.mile.teahouse.updatmemeber) {
                this.node.dispatchEvent(new cc.Event.EventCustom("checkEmitCallback", true));
            } else {
                this.checkEmitCallback();
            }
        }, 1);
    },
    onRemovePlayer: function () { // 删除玩家
        this.node.dispatchEvent(new cc.Event.EventCustom("onRemovePlayer", true));
    },
    onApprovePlayer: function () { // 审核通过
        this.node.dispatchEvent(new cc.Event.EventCustom("onApprovePlayer", true));
    },
    onRejectPlayer: function () { // 拒绝
        this.node.dispatchEvent(new cc.Event.EventCustom("onRejectPlayer", true));
    },
    onAddDianxiaoerTeaPlayer: function () { //添加店小二
        this.node.dispatchEvent(new cc.Event.EventCustom("onAddDianxiaoerTeaPlayer", true));
    },
    onRemoveDianxiaoerPlayer: function () { // 删除店小二
        this.node.dispatchEvent(new cc.Event.EventCustom("onRemoveDianxiaoerPlayer", true));
    },
    onPlayerTeaCreditScore: function () { //上下分
        this.node.dispatchEvent(new cc.Event.EventCustom("onPlayerTeaCreditScore", true));
    },
    onPlayerTeaCreditScoreList: function () { //上下分历史记录
        this.node.dispatchEvent(new cc.Event.EventCustom("onPlayerTeaCreditScoreList", true));
    },
    init: function (index, player) {
        debugger;
        this.initEvent();
        this.playData = player;
        this.user_number_label.string = index + 1;
        this.user_name_label.string = player.username;
        this.user_id_label.string = player.gameid;
        if(this.user_score_label){
            this.user_score_label.string = player.score;
        }
        this.loadUrlJPGImage(player.userurl
            + "&username=" + player.userid, this.user_head_node);
    },

    start: function () {

    },

    // update (dt) {},
});
