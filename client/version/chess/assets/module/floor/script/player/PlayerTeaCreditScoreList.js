var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        player_container_node: {
            default: null,
            type: cc.Node
        },
        playerItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        totalPages: {
            default: null,
            type: cc.Label
        },
        totalElements: {
            default: null,
            type: cc.Label
        },
        page: {
            default: null,
            type: cc.Label
        },
    },
    initView: function (gameid) {
        this.gameid = gameid;
        this.page_number = 1;
        this.initEvent();
        this.queryPlayerRecord(gameid);
    },
    initEvent: function () {
        var self = this;
        this.node.on('onBeforePage', function (event) {
            console.log('-----onBeforePage-----click--!');
            if (self) {
                self.beforePage();
            }
        });

        this.node.on('onNextPage', function (event) {
            console.log('-----onApprovePlayer-----click--!');
            if (self) {
                self.nextPage();
            }
        });
    },
    onBeforePage: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("onBeforePage", true));
    },
    onNextPage: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("onNextPage", true));
    },
    //获取信息
    queryPlayerRecord: function (gameid) {
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            page: this.page_number - 1,
            size: 10,
            gameid: this.gameid
        };
        cc.mile.http.httpPost("/teahouse/getteascorerecordlist", param, this.successGetPlayer, this.errorGetPlayer, this);
    },
    successGetPlayer: function (result, context) {
        cc.log(result);
        var data = context.parse(result);
        context.loadItem(data);
        context.totalPagesNumber = data.totalPages > 1 ? data.totalPages - 1 : 1;
        context.page.string = context.page_number + "/" + context.totalPagesNumber;
    },
    errorGetPlayer: function (result, context) {
        cc.log(result);
    },
    loadItem: function (data) {
        this.player_container_node.removeAllChildren();
        var self = this;
        for (var i = 0; i < data.content.length; i++) {
            var player = cc.instantiate(this.playerItemPrefab);
            player.setPosition(0, -50 - 110 * i);
            player.parent = this.player_container_node;
            var playRender = player.getComponent("PlayerTeaCreditScoreItem");
            playRender.init((this.page_number - 1) * 10 + i, data.content[i]);
        }
        this.player_container_node.height = data.content.length * 110;
    },
    beforePage: function () {
        if (this.page_number > 1) {
            this.page_number -= 1;
        }
        this.queryPlayerRecord(this.gameid);
    },
    nextPage: function () {
        if (this.page_number < this.totalPagesNumber) {
            this.page_number += 1;
        }
        this.queryPlayerRecord(this.gameid);
    }
    // update (dt) {},
});
