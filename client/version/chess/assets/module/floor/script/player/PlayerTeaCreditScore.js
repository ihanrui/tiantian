
/**
 * 查看历史
 */

var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        username_label: {
            default: null,
            type: cc.Label
        },
        userid_label: {
            default: null,
            type: cc.Label
        },
        usercredit_label: {
            default: null,
            type: cc.Label
        },
        transfer_input: {
            default: null,
            type: cc.EditBox
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    initEvent: function () {
        var self = this;
        this.node.on('credittanasferMousedown', function (event) {
            console.log('-----credittanasferMousedown-----click--!');
            if (self) {
                console.log('-----credittanasferMousedown---转入积分--!');
                self.transferAction();

            }

        });
    },
    transferAction: function () {
        console.log('-----credittanasferMousedown---转入积分--!'+this.transfer_input.string);
        // 调用解散桌子的请求
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            gameid: this.userdata.gameid,
            score: this.transfer_input.string,
        };
        cc.mile.dealscore = null;
        this.checkCount = 20;
        this.emitSocketRequest("dealscore", JSON.stringify(param));
        this.checkEmitCallback();
    },
    checkEmitCallback: function () {
        this.scheduleOnce(function () {
            if (cc.mile.dealscore) {
                this.updateScoreAndView();
            } else {
                this.checkCount--;
                if(this.checkCount > 0){
                    this.checkEmitCallback();
                }
            }
        }, 0.5);
    },

    updateScoreAndView: function () {
        if(cc.mile.dealscore.flag){
            this.showCenterToast("调整积分成功!");
            var transferNums = this.transfer_input.string;
            var transferNumsInt = parseInt(transferNums);
            this.userdata.score = this.userdata.score + transferNumsInt;
            this.initView();
        }else {
            this.showCenterToast("调整积分失败，请重新操作!");
        }
    },

    initData: function (userdata) {
        this.userdata = userdata;
        this.initEvent();
    },
    initView: function () {

        this.username_label.string = this.userdata.username;
        this.userid_label.string = this.userdata.gameid;
        this.usercredit_label.string = this.userdata.score;
    },

    onConfirmTransfer:function () {
        var transferNums = this.transfer_input.string;
        if(transferNums != ""){

            this.node.dispatchEvent(new cc.Event.EventCustom("credittanasferMousedown", true));
        }else {
            this.showCenterToast("请输入积分值!");
        }
    },
    start: function () {

    },

    // update (dt) {},
});
