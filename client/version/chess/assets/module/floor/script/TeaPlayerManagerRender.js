var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        personBtn1: {
            default: null,
            type: cc.Node
        },
        personBtn2: {
            default: null,
            type: cc.Node
        },
        checkPersonBtn1: {
            default: null,
            type: cc.Node
        },
        checkPersonBtn2: {
            default: null,
            type: cc.Node
        },
        secondPersonBtn1: {
            default: null,
            type: cc.Node
        },
        secondPersonBtn2: {
            default: null,
            type: cc.Node
        },
        addManager: {
            default: null,
            type: cc.Node
        },
        searchNode: {
            default: null,
            type: cc.Node
        },
        searchId: {
            default: null,
            type: cc.Node
        },
        player_container_node: {
            default: null,
            type: cc.Node
        },
        playerItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        playerApproveItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        playerDianxiaoerItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        player_list: {
            default: null,
            type: cc.Prefab
        },
        pageNode: {
            default: null,
            type: cc.Node
        },
        page: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //this.loadPlayer();
        this.page_number = 1;
        this.queryApprovedRecord();
    },
    //点击成员列表
    onPersonListClik: function () {
        this.personBtn1.active = false;
        this.personBtn2.active = true;
        this.checkPersonBtn1.active = true;
        this.checkPersonBtn2.active = false;
        this.secondPersonBtn1.active = true;
        this.secondPersonBtn2.active = false;
        this.addManager.active = false;
        this.searchNode.active = true;
        this.pageNode.setPosition(0, 0);
        this.queryApprovedRecord();
    },
    //点击审核成员
    onCheckPerson: function () {
        this.personBtn1.active = true;
        this.personBtn2.active = false;
        this.checkPersonBtn1.active = false;
        this.checkPersonBtn2.active = true;
        this.secondPersonBtn1.active = true;
        this.secondPersonBtn2.active = false;
        this.addManager.active = false;
        this.searchNode.active = false;
        this.pageNode.setPosition(256, 0);
        this.queryApplyRecord();
    },
    //点击小二管理
    onSecondManager: function () {
        this.personBtn1.active = true;
        this.personBtn2.active = false;
        this.checkPersonBtn1.active = true;
        this.checkPersonBtn2.active = false;
        this.secondPersonBtn1.active = false;
        this.secondPersonBtn2.active = true;
        this.addManager.active = true;
        this.searchNode.active = false;
        this.pageNode.setPosition(256, 0);
        this.queryDianxiaoerRecord();
    },
    //查询审批通过的成员
    queryApprovedRecord: function () {
        console.log("---查询审批通过的成员---")
        this.querytype = "approved";
        this.param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            page: 0,
            size: 10,
            passflag: 'pass'
        };
        this.queryPlayerRecord(this.param);
    },
    //查询 提交申请的成员
    queryApplyRecord: function () {
        console.log("---查询提交申请的成员---")
        this.querytype = "apply";
        this.param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            page: 0,
            size: 10,
            passflag: 'apply'
        };
        this.queryPlayerRecord(this.param);
    },
    //查询店小二
    queryDianxiaoerRecord: function () {
        console.log("---查询店小二---")
        this.querytype = "dianxiaoer";
        this.param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            page: 0,
            size: 10,
            role: 'manager'
        };
        this.queryPlayerRecord(this.param);
    },

    /**
    *
    passflag:
        APPLY,    //申请
        PASS,   //审核通过
        OUT,     //离开茶楼
        REFUSE,  //拒绝
        FORBID;//禁止加入
    */
    queryPlayerRecord: function (param) {
        cc.mile.http.httpPost("/teahouse/getmeminfoList", param, this.successGetPlayer, this.errorGetPlayer, this);

    },
    successGetPlayer: function (result, context) {
        debugger
        cc.log(result);
        var data = JSON.parse(result);
        context.totalPagesNumber = data.totalPages > 1 ? data.totalPages : 1;
        context.page.string = context.param.page + 1 + "/" + context.totalPagesNumber;
        context.loadPlayer(data.content);

    },
    errorGetPlayer: function (result, context) {
        debugger
        cc.log(result);
    },

    loadPlayer: function (players) {
        this.player_container_node.removeAllChildren();
        var self = this;
        var indexX = 0;
        var indexY = -95;
        for (var i = 0; i < players.length; i++) {
            var player = this.getPlayerItem();//cc.instantiate(this.playerItemPrefab);
            player.setPosition(indexX, indexY - 110 * i);
            player.parent = this.player_container_node;
            player.on('checkEmitCallback', function (event) {
                console.log('-----checkEmitCallback------!');
                if (self) {
                    self.refreshPlayerItem();
                }
            });
            var playRender = player.getComponent("PlayerItemManager");
            playRender.init((this.param.page) * this.param.size + i, players[i]);
        }
        this.player_container_node.height =50+ players.length * 110;
    },
    getPlayerItem: function () {
        if (this.querytype == "apply") {
            return cc.instantiate(this.playerApproveItemPrefab);
        } else if (this.querytype == "approved") {
            return cc.instantiate(this.playerItemPrefab);
        } else if (this.querytype == "dianxiaoer") {
            return cc.instantiate(this.playerDianxiaoerItemPrefab);
        }
        return cc.instantiate(this.playerItemPrefab);
    },
    refreshPlayerItem: function () {
        cc.mile.teahouse.updatmemeber = null;
        if (this.querytype == "apply") {
            this.queryApplyRecord();
        } else if (this.querytype == "approved") {
            this.queryApprovedRecord();
        } else if (this.querytype == "dianxiaoer") {
            this.queryDianxiaoerRecord();
        }
    },
    onAddManager: function () {
        debugger
        cc.mile.openwin2 = cc.instantiate(this.player_list);
        cc.mile.openwin2.parent = this.root();
    },
    beforePage: function () {
        if (this.param.page > 0) {
            this.param.page -= 1;
        }
        this.queryPlayerRecord(this.param);
    },
    nextPage: function () {
        if (this.param.page < this.totalPagesNumber - 1) {
            this.param.page += 1;
        }
        this.queryPlayerRecord(this.param);
    },
    onSearch: function () {
        console.log("---根据GameId查询成员---")
        this.querytype = "approved";
        this.param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            page: 0,
            size: 1,
            gameid: this.searchId.getComponent(cc.EditBox).string,
            passflag: 'pass'
        };
        if (this.param.gameid == "") {
            this.showCenterToast("请输入要查找的玩家ID");
            return;
        }
        this.queryPlayerRecord(this.param);
    },

    // update (dt) {},
});
