var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        content: {
            default: null,
            type: cc.EditBox
        }
    },
    onLoad: function () {
        if(cc.mile.teahouse.closenotice){
            this.content.string = cc.mile.teahouse.closenotice;
        }
    },
    init: function (content) {
        this.content.string = content;
    },
    onOk:function(){
        var self = this;
        this.confirm("打烊后，玩家将不能在茶楼玩游戏！", function () {
            var param = {
                teanum: cc.mile.teahouse.teanum,
                token: cc.mile.authorization,
                state: "close",
                closenotice: self.content.string
            };
            self.emitSocketRequest("updateteahouse", JSON.stringify(param));
            self.closeConfirm();
            self.closeOpenWin();
        });
    }
});
