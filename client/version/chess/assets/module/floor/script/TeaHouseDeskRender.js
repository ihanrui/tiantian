// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 茶楼桌子的Person
 */
var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        desk_number: {
            default: null,
            type: cc.Node
        },
        //桌子显示
        desk_container: {
            default: null,
            type: cc.Node
        },
        playerPrefab: {
            default: null,
            type: cc.Prefab
        },
        deskPlayerListPrefab: {
            default: null,
            type: cc.Prefab
        },
        deskBackgroud:
            {
                default: null,
                type: cc.Node
            },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },
    initEvent: function () {
        var self = this;
        this.node.on('teaItemMousedown', function (event) {
            console.log('-----teaItemMousedown-----click--!');
            if (self) {
                if (cc.mile.teahouse && cc.mile.teahouse.state == 'close') {
                    self.showCenterToast('茶楼已打烊!');
                } else {

                    self.loadding();
                    var param = {
                        token: cc.mile.authorization,
                        orgi: cc.mile.user.orgi,
                        tablenum: self.deskIndex,
                        roomtype: cc.mile.createroomtype,
                        teanum: cc.mile.teahouse.teanum,
                    };
                    self.emitSocketRequest("sitdown", JSON.stringify(param));
                }
            }

        });
        this.node.on('deskInfoMousedown', function (event) {
            console.log('-----deskInfoMousedown-----click--!');
            if (self) {
                var deskPlayerList = cc.instantiate(self.deskPlayerListPrefab);
                cc.mile.openwin = deskPlayerList;
                cc.mile.openwin.parent = self.root();

                var render = deskPlayerList.getComponent("DeskPlayerListItemRender")
                render.initView(self.getDeskIndex(), self.playerDatas, self.currentnum, self.begindate);
            }

        });
    },
    init: function (index) {

        this.deskIndex = index;
        var sprite = this.deskBackgroud.getComponent(cc.Sprite);
        var nums = cc.mile.teahouse.nums;
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/table/imgTable" + nums + ".png"));

        this.deskPlayers = [];
        this.initEvent();
        this.show_teahouse_number(index);
        this.deskPlayersPool = new cc.NodePool();
        for (var i = 0; i < 4; i++) {
            var player = cc.instantiate(this.playerPrefab);
            this.deskPlayersPool.put(player);
        }
        //this.addDeskPlayer({name: '测试名称'});
    },
    ///显示数字
    show_teahouse_number: function (index) {
        var name = "img" + (index) + ".png";
        var imageWidht = 16;
        if (index == 0) {
            imageWidht = 8;
        }
        this.desk_number.getComponent(cc.Sprite).spriteFrame =
            new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/" + name + ""), cc.rect(0, 0, imageWidht, 26));
    },
    onDeskClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("teaItemMousedown", true));
    },
    addDeskPlayers: function (players, currentnum, begindate) {

        this.clearDeskPlayer();
        this.playerDatas = players;
        this.currentnum = currentnum;
        this.begindate = begindate;
        if (players && players.length > 0) {
            for (var i = 0; i < players.length; i++) {
                this.addDeskPlayer(players[i], i);
            }
        }
    },
    addDeskPlayer: function (playerData, index) {
        var player = cc.instantiate(this.playerPrefab);
        this.setDeskPlayerPosition(player, index);
        player.parent = this.desk_container;
        var playerRender = player.getComponent("TeaHousePlayerRender");
        playerRender.init(playerData);
        this.deskPlayers.push(player);
    },

    setDeskPlayerPosition: function (node, index) {
        var nums = cc.mile.teahouse.nums;
        if (nums == 6) {
            return this.set6PlayerPosition(node, index);
        } else if (nums == 8) {
            return this.set6PlayerPosition(node, index);
        }
        return this.set4PlayerPosition(node, index);
    },
    set4PlayerPosition: function (node, index) {

        switch (index) {
            case 0:
                node.setPosition(-55, 74);
                break;
            case 1:
                node.setPosition(-67, 19);
                break;
            case 2:
                node.setPosition(55, 20);
                break;
            case 3:
                node.setPosition(64, 75);
                break;
        }
    },
    set6PlayerPosition: function (node, index) {

        switch (index) {
            case 0:
                node.setPosition(-55, 74);
                break;
            case 1:
                node.setPosition(-67, 19);
                break;
            case 2:
                node.setPosition(55, 20);
                break;
            case 3:
                node.setPosition(64, 75);
                break;
            case 4:
                node.setPosition(64, 75);
                break;
            case 5:
                node.setPosition(64, 75);
                break;
        }
    },
    set8PlayerPosition: function (node, index) {

        switch (index) {
            case 0:
                node.setPosition(-55, 74);
                break;
            case 1:
                node.setPosition(-67, 19);
                break;
            case 2:
                node.setPosition(55, 20);
                break;
            case 3:
                node.setPosition(64, 75);
                break;
            case 4:
                node.setPosition(-45, 64);
                break;
            case 5:
                node.setPosition(-57, 9);
                break;
            case 6:
                node.setPosition(45, 10);
                break;
            case 7:
                node.setPosition(54, 65);
                break;
        }
    },
    updateDeskPlayer: function (player) {

    },
    clearDeskPlayer: function () {
        for (var i = this.deskPlayers.length - 1; i >= 0; i--) {
            var player = this.deskPlayers[i];
            this.deskPlayers.splice(i, 1);
            this.desk_container.removeChild(player);
            this.deskPlayersPool.put(player);
        }
    },
    getDeskIndex: function () {
        return this.deskIndex;
    },
    getDeskPlayers: function () {
        return this.deskPlayers;
    },
    //桌子详情
    onDeskInfoClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("deskInfoMousedown", true));
    },

    start: function () {

    },

    // update (dt) {},
});
