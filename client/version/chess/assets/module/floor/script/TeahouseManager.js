var beiMiCommon = require("MileCommon");


cc.Class({
    extends: beiMiCommon,

    properties: {

        btSetBtn0: {
            default: null,
            type: cc.Node
        },
        btSetBtn1: {
            default: null,
            type: cc.Node
        },
        btWaterBtn0: {
            default: null,
            type: cc.Node
        },
        btWaterBtn1: {
            default: null,
            type: cc.Node
        },
        btFoundationBtn0: {
            default: null,
            type: cc.Node
        },
        btFoundationBtn1: {
            default: null,
            type: cc.Node
        },
        btStatusBtn0: {
            default: null,
            type: cc.Node
        },
        btStatusBtn1: {
            default: null,
            type: cc.Node
        },
        btCloseRoomBtn0: {
            default: null,
            type: cc.Node
        },
        btCloseRoomBtn1: {
            default: null,
            type: cc.Node
        },

        operatCondition: { //经营状况
            default: null,
            type: cc.Node
        },
        basicSet: { // 基础设置
            default: null,
            type: cc.Node
        },
        waterSetContainer: { // 抽水设置
            default: null,
            type: cc.Node
        },
        foundationSetContainer: { //茶楼基金
            default: null,
            type: cc.Node
        },
        destroyTeahouse: { //销毁茶楼
            default: null,
            type: cc.Node
        },

        basicSetBtn: { //基础设置
            default: null,
            type: cc.Node
        },
        waterSetBtn: { //抽水设置
            default: null,
            type: cc.Node
        },
        foundationSetBtn: { //茶楼基金设置
            default: null,
            type: cc.Node
        },
        operatConditionBtn: { //经营状况
            default: null,
            type: cc.Node
        },

        destroyTeahouseBtn: { //销毁茶楼
            default: null,
            type: cc.Node
        },
        pressedNode: { //当前点击的按钮
            default: null,
            type: cc.Node
        },

        waterSetPrefab: { //抽水设置
            default: null,
            type: cc.Prefab
        },
        foundationPrefab: { //茶楼基金
            default: null,
            type: cc.Prefab
        },
        teaname: {
            default: null,
            type: cc.EditBox
        },
        teanotice: {
            default: null,
            type: cc.EditBox
        },
        //经营状况列表容器
        operatConditionItemNode: {
            default: null,
            type: cc.Node
        },
        //经营状况列表
        operatConditionItemPrefab:{
            default: null,
            type: cc.Prefab
        },
        //经营状况列表页码
        operatConditionPage_label: {
            default: null,
            type: cc.Label
        },//经营状况列表页码
        configSettingContainer: {
            default: null,
            type: cc.Node
        },
        optionGroupPrefab: {
            default: null,
            type: cc.Prefab
        },
        rowPrefab: {
            default: null,
            type: cc.Prefab
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.updatePressedBtnStatus(this.basicSetBtn);
        this.basicSet.active = true;
        this.teaname.string = cc.mile.teahouse.name;
        if(cc.mile.teahouse.usernotice){
            this.teanotice.string = cc.mile.teahouse.usernotice;
        }

        this.paramsData = {
            groups: [
                {id: "4028ab6", name: "特色设置", code: "othersetting", type: 'checkbox',width: 700}
            ],
            items: [
                {id: "402", name: "同IP禁止",
                    defaultvalue: this.getMethodDefaultValue('ipflag'), code: "ipflag", groupid: "4028ab6"},
                {id: "402", name: "成员审核",
                    defaultvalue: this.getMethodDefaultValue('passflag'),code: "passflag", groupid: "4028ab6"},
                {id: "403", name: "禁止分享", code: "15", groupid: "4028ab6",
                    defaultvalue: this.getMethodDefaultValue('shareflag'),code: "shareflag"}
            ],
        };

        this.optionGroupLst = [];
        this.initSettingParams();

    },

    getMethodDefaultValue: function (flagName) {
        var value =  cc.mile.teahouse[flagName];
        if(flagName == 'shareflag'){
            if(value == 'Y'){
                return false;
            }
            return true;
        }else {

            if(value == 'Y'){
                return true;
            }
            return false;
        }
    },
    initSettingParams: function () {


        if (this.optionGroupPrefab != null) {

            while (this.optionGroupLst.length) {
                var tmp = this.optionGroupLst.pop();
                tmp.destroy();
            }

            var height = 20;
            for (var inx = 0; inx < this.paramsData.groups.length; inx++) {
                var selfPosition = cc.v2(15, height);
                var row = cc.instantiate(this.rowPrefab);
                row.setPosition(selfPosition);
                row.parent = this.configSettingContainer;

                var group = cc.instantiate(this.optionGroupPrefab);
                var optionGroup = group.getComponent("inputAndCheckboxGroup");
                optionGroup.init(this.paramsData.groups[inx], this.paramsData.items);

                group.setPosition(selfPosition);
                group.parent = this.configSettingContainer;
                height -= 70;
                this.optionGroupLst.push(optionGroup);
            }

        }
    },

    updateTeahouseMessage: function () {
        this.teaname.string = cc.mile.teahouse.name;
        if(cc.mile.teahouse.usernotice){
            this.teanotice.string = cc.mile.teahouse.usernotice;
        }
    },
    // 基础设置
    onBasicSetClik: function () {
        this.basicSet.active = true;
        this.operatCondition.active = false;
        this.destroyTeahouse.active = false;
        this.waterSetContainer.active = false;
        this.foundationSetContainer.active = false;

        this.btSetBtn0.active=false;
        this.btSetBtn1.active=true;
        this.btWaterBtn0.active=true;
        this.btWaterBtn1.active=false;
        this.btFoundationBtn0.active=true;
        this.btFoundationBtn1.active=false;
        this.btStatusBtn0.active=true;
        this.btStatusBtn1.active=false;
        this.btCloseRoomBtn0.active=true;
        this.btCloseRoomBtn1.active=false;

        //this.scheduleOnce(function () {
        //    this.updatePressedBtnStatus(this.basicSetBtn);
        //},0.001);
    },
    //打赏模式
    onWaterSetClick: function () {
        this.waterSetContainer.removeAllChildren();
        this.basicSet.active = false;
        this.operatCondition.active = false;
        this.destroyTeahouse.active = false;
        this.foundationSetContainer.active = false;
        this.waterSetContainer.active = true;
        var waterSet = cc.instantiate(this.waterSetPrefab);
        waterSet.parent = this.waterSetContainer;
        waterSet.setPosition(110,100)

        this.btSetBtn0.active=true;
        this.btSetBtn1.active=false;
        this.btWaterBtn0.active=false;
        this.btWaterBtn1.active=true;
        this.btFoundationBtn0.active=true;
        this.btFoundationBtn1.active=false;
        this.btStatusBtn0.active=true;
        this.btStatusBtn1.active=false;
        this.btCloseRoomBtn0.active=true;
        this.btCloseRoomBtn1.active=false;

        //this.scheduleOnce(function () {
        //    this.updatePressedBtnStatus(this.waterSetBtn);
        //},0.001);

    },
    // 基金设置
    onFoundationSetClick: function () {
        this.foundationSetContainer.removeAllChildren();
        this.basicSet.active = false;
        this.operatCondition.active = false;
        this.destroyTeahouse.active = false;
        this.waterSetContainer.active = false;
        this.foundationSetContainer.active = true;

        var foundationPrefab = cc.instantiate(this.foundationPrefab);
        foundationPrefab.parent = this.foundationSetContainer;

        this.btSetBtn0.active=true;
        this.btSetBtn1.active=false;
        this.btWaterBtn0.active=true;
        this.btWaterBtn1.active=false;
        this.btFoundationBtn0.active=false;
        this.btFoundationBtn1.active=true;
        this.btStatusBtn0.active=true;
        this.btStatusBtn1.active=false;
        this.btCloseRoomBtn0.active=true;
        this.btCloseRoomBtn1.active=false;

        //this.scheduleOnce(function () {
        //    this.updatePressedBtnStatus(this.foundationSetBtn);
        //},0.001);
    },
    //经营状况
    onOperatConditionClik: function () {
        debugger
        this.basicSet.active = false;
        this.operatCondition.active = true;
        this.destroyTeahouse.active = false;
        this.waterSetContainer.active = false;
        this.foundationSetContainer.active = false;

        this.btSetBtn0.active=true;
        this.btSetBtn1.active=false;
        this.btWaterBtn0.active=true;
        this.btWaterBtn1.active=false;
        this.btFoundationBtn0.active=true;
        this.btFoundationBtn1.active=false;
        this.btStatusBtn0.active=false;
        this.btStatusBtn1.active=true;
        this.btCloseRoomBtn0.active=true;
        this.btCloseRoomBtn1.active=false;

        //this.scheduleOnce(function () {
        //    this.updatePressedBtnStatus(this.btStatusBtn);
        //},0.001);

        this.param = {
            token: cc.mile.authorization,
            teanum: cc.mile.teahouse.teanum,
            page:0
        };
        this.queryOperatCondition(this.param);

    },
    queryOperatCondition: function (param) {
        cc.mile.http.httpPost("/teahouse/getteareport", param, this.successGetPlayer, this.errorGetPlayer, this);
    },
    //销毁茶楼
    onDestroyTeahouse: function () {
        this.basicSet.active = false;
        this.operatCondition.active = false;
        this.destroyTeahouse.active = true;
        this.waterSetContainer.active = false;
        this.foundationSetContainer.active = false;

        this.btSetBtn0.active=true;
        this.btSetBtn1.active=false;
        this.btWaterBtn0.active=true;
        this.btWaterBtn1.active=false;
        this.btFoundationBtn0.active=true;
        this.btFoundationBtn1.active=false;
        this.btStatusBtn0.active=true;
        this.btStatusBtn1.active=false;
        this.btCloseRoomBtn0.active=false;
        this.btCloseRoomBtn1.active=true;

        //this.scheduleOnce(function () {
        //    this.updatePressedBtnStatus(this.destroyTeahouseBtn);
       //},0.001);
    },

    updatePressedBtnStatus: function (btnNode) {

   //cc.log("-------updatePressedBtnStatus------");
   //if(this.pressedNode){
   //    var btn = this.pressedNode.getComponent(cc.Button);
   //    var sprite = this.pressedNode.getComponent(cc.Sprite);
   //    sprite.spriteFrame = btn.normalSprite;
   //}

   //this.pressedNode = btnNode;
   //var pressedBtn = this.pressedNode.getComponent(cc.Button);
   //var pressedSprite = this.pressedNode.getComponent(cc.Sprite);
   //pressedSprite.spriteFrame = pressedBtn.pressedSprite;

    },

    onBasicSetClikSure: function () {
        debugger;
        var teaname = this.teaname.string;
        var teanotice = this.teanotice.string;
        if (teaname && teaname != '') {

            var ipflag = 'N';
            var passflag = 'N';
            var shareflag = 'Y';
            for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
                var groupitem = this.optionGroupLst[inx];

                for (var j = 0; j < groupitem.groupoptions.length; j++) {
                    var option = groupitem.groupoptions[j];
                    if (option.checked == true) {
                        if(option.getItemCode() == 'ipflag'){
                            ipflag = 'Y';
                        }else if(option.getItemCode() == 'passflag'){
                            passflag = 'Y';
                        }else if(option.getItemCode() == 'shareflag'){
                            shareflag = 'N';
                        }
                    }
                }

            }

            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                teanum: cc.mile.teahouse.teanum,
                name: teaname,
                shareflag: shareflag,
                ipflag: ipflag,
                passflag: passflag,
                usernotice: teanotice
            };
            this.emitSocketRequest("updateteahouse", JSON.stringify(param));

        } else {
            this.showCenterToast("茶楼名称不能为空")
        }


    },

    onDestroyTeahouseSure: function () {
        debugger;
        var self = this;
        this.confirm('请确定是否销毁茶楼？', function (self) {

            var param = {
                teanum: cc.mile.teahouse.teanum,
                token: cc.mile.authorization,
                state: "destory"
            };
            self.emitSocketRequest("updateteahouse", JSON.stringify(param));

            self.closeConfirm();
        });

    },

    successGetPlayer: function (result, context) {
        debugger
        var data;
        try {
            data = JSON.parse(result);
            context.totalPagesNumber = data.totalPages > 1 ? data.totalPages : 1;
            context.operatConditionPage_label.string = context.param.page + 1 + "/" + context.totalPagesNumber;
            context.loadPlayer(data.content);
        }catch (e)
        {
            context.showCenterToast(result)
        }
    },
    errorGetPlayer: function (result, context) {
        cc.log(result);
    },
    loadPlayer: function (content) {
        this.operatConditionItemNode.removeAllChildren();
        var self = this;
        var indexX = 0;
        for (var i = 0; i < content.length; i++) {
            var operatCondition = cc.instantiate(this.operatConditionItemPrefab);
            operatCondition.setPosition(indexX, 170 - 50 * i);
            operatCondition.parent = this.operatConditionItemNode;
            var operateConditions = operatCondition.getComponent("TeaHouseOperatConditionItem");
            operateConditions.init(content[i]);
        }
    },
    beforePage: function () {
        if (this.param.page > 0) {
            this.param.page -= 1;
        }
        this.queryOperatCondition(this.param);
    },
    nextPage: function () {
        if (this.param.page < this.totalPagesNumber - 1) {
            this.param.page += 1;
        }
        this.queryOperatCondition(this.param);
    },
});
