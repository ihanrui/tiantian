// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
    extends: cc.Component,

    properties: {
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_id_label: {
            default: null,
            type: cc.Label
        },
        user_head_node: {
            default: null,
            type: cc.Node
        },
        win_node: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    init: function (player) {
        this.user_name_label.string = player.username;
        WangdaGameUtils.showNumberNode(player.stepscore,this.win_node);

    },

    start: function () {

    },

    // update (dt) {},
});
