// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");

var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,


    properties: {
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_id_label: {
            default: null,
            type: cc.Label
        },
        user_head_node: {
            default: null,
            type: cc.Node
        },
        win_node: {
            default: null,
            type: cc.Node
        },
        ticketcontainer_node: {
            default: null,
            type: cc.Node
        },
        ticket_label: {
            default: null,
            type: cc.Label
        },
        historyScoreContainerNode: {
            default: null,
            type: cc.Node
        },
        historyScoreLabel: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    init: function (player,roomtype) {
        this.user_name_label.string = player.username.substr(0,5);
        this.user_id_label.string = player.gameid;

        cc.log("单个用户信息："+roomtype);
        cc.log("单个用户信息："+JSON.stringify(player));
        this.loadUrlJPGImage(player.userurl
            + "&username=" + player.userid, this.user_head_node);
        if(roomtype == "card"){
            WangdaGameUtils.showNumberNode(player.totalscore,this.win_node);
            this.win_node.setScale(0.8,0.8);
        }else {
            WangdaGameUtils.showNumberNode(player.coins,this.win_node);
            this.win_node.setScale(0.8,0.8);
        }

        if(player.ticket && player.ticket > 0){
            this.ticket_label.string = player.ticket;
            this.ticketcontainer_node.active = true;
        }else{
            this.ticketcontainer_node.active = false;
        }

        if(roomtype != "card"){
            if(player.expenddetail){
                if(player.userid == cc.mile.user.id || cc.mile.teahouserrole != 'normal'){
                    this.historyScoreContainerNode.active = true;
                    this.historyScoreLabel.string = player.expenddetail.expendend
                }
            }
        }

    },

    start: function () {

    },

    // update (dt) {},
});
