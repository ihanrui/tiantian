var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {


        roominfoLabel: {
            default: null,
            type: cc.Label
        },
        overdateLabel: {
            default: null,
            type: cc.Label
        },
        historyDetailSinglePrefab: {
            default: null,
            type: cc.Prefab
        },
        historyContainerNode: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        var self=this;
        this.node.on("playBackMouseDown",function(event){
            if(self&&self.backroomid) {
                cc.mile.backroomid = self.backroomid;
                cc.mile.curnum = self.curnum;
                self.scene("playback",self);
            }
        })
    },
    playBack:function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("playBackMouseDown",true));
    },
    initView: function (playdata) {
        debugger;
        //this.initEvent();
        //this.playdata = playdata;
        cc.mile.backroomid = playdata[0].roomid;

        this.backroomid=playdata[0].roomid;
        this.curnum=playdata[0].curnum;
        this.overdateLabel.string = playdata[0].lastmodify;
        this.roominfoLabel.string = "打码子：茶楼" + playdata[0].teanum + "，房间号:" + playdata[0].roomid;
        this.addDeskPlayers(playdata)
    },
    addDeskPlayers: function (players) {

        for (var i = 0; i < players.length; i++) {
            this.addHistoryPlayer(players[i], i);
        }

    },

    addHistoryPlayer: function (playerData, index) {

        var player = cc.instantiate(this.historyDetailSinglePrefab);
        cc.log(this.deskIndex + "--addDeskPlayer--" + this.deskPosistion);
        var x = -240 + 280 * (index%3);
        var y = 20 -  parseInt(index/3) * 70;
        player.setPosition(x, y);

        player.parent = this.historyContainerNode;

        var playerRender = player.getComponent("PlayHistoryDetailSingleUser");
        playerRender.init(playerData);

    },

    start: function () {

    },

    // update (dt) {},
});
