// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 茶楼桌子的Person
 */
var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {


        roominfoLabel: {
            default: null,
            type: cc.Label
        },
        overdateLabel: {
            default: null,
            type: cc.Label
        },
        historySinglePrefab: {
            default: null,
            type: cc.Prefab
        },
        historyContainerNode: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    onToCheckDetail: function () {
        debugger
        var event = new cc.Event.EventCustom("historyCheckDetail", true);
        event.setUserData({roomId: this.getRoomId()});
        this.node.dispatchEvent(event);
    },
    onToCopyDetail: function () {

        var event = new cc.Event.EventCustom("historyCopyDetail", true);
        this.node.dispatchEvent(event);
    },
    initEvent: function () {
        var self = this;
        this.node.on('historyCheckDetail', function (event) {
            console.log('-----onRemovePlayer-----click--!');
            if (self) {
                var event = new cc.Event.EventCustom("historyCheckDetailRender", true);
                event.setUserData({roomId: self.getRoomId()});
                self.node.dispatchEvent(event);
            }
        });
        this.node.on('historyCopyDetail', function (event) {
            console.log('-----historyCopyDetail-----click--!');
            if (self) {
                self.copyDetail();
            }
        });
    },
    copyDetail: function () {
        var gameUtils = require("WangdaGameUtils");
        var copyStrLst = [];
        cc.log("战绩数据:"+ JSON.stringify(this.playdata));
        var typeDesc = "";
        var roomTypeff = this.playdata.roomtype;
        if (roomTypeff == "card") {
            typeDesc = "经典场";
        } else {
            typeDesc = "积分场";
        }
        cc.log("传递房间类型initView：" + roomTypeff);
        
        copyStrLst.push("时间：" + this.playdata.overdate);

        if(this.playdata.teanum){
            copyStrLst.push("打码子:茶楼" + this.playdata.teanum + "," + typeDesc + "房间号:" + this.playdata.roomid);
        }else {
            copyStrLst.push("打码子" + typeDesc + "房间号:" + this.playdata.roomid);
        }

        for (var i = 0; i < this.playdata.summaryPlayer.length; i++) {
            var player = this.playdata.summaryPlayer[i];
            var jiesuanDesc = "玩家:" + player.username + "(ID" + player.gameid + ")";

            if (this.playdata.roomtype == 'card') {
                jiesuanDesc += '得分：' + player.totalscore;
            } else {
                jiesuanDesc += '得分：' + player.coins;
            }
            if (player.ticket && player.ticket > 0) {
                jiesuanDesc += ',打赏老板：' + player.ticket;
            }
            copyStrLst.push(jiesuanDesc);
        }
        gameUtils.copyToClipboard(copyStrLst.join("\n"));
        this.showCenterToast("复制成功");
    },
    initView: function (playdata) {
        this.initEvent();
        this.playdata = playdata;
        this.overdateLabel.string = playdata.overdate;

        var typeDesc = "";
        var roomTypeff = playdata.roomtype;
        if (roomTypeff == "card") {
            typeDesc = "经典场";
        } else {
            typeDesc = "积分场";
        }
        cc.log("传递房间类型initView：" + roomTypeff);
        if(playdata.teanum){
            this.roominfoLabel.string = "打码子:茶楼" + playdata.teanum + "," + typeDesc + "房间号:" + playdata.roomid;
        }else {
            this.roominfoLabel.string = "打码子" + typeDesc + "房间号:" + playdata.roomid;
        }
        this.addDeskPlayers(playdata.summaryPlayer, roomTypeff);
    },
    addDeskPlayers: function (players, roomTypeff) {
        cc.log("传递房间类型addDeskPlayers：" + roomTypeff);

        for (var i = 0; i < players.length; i++) {
            this.addHistoryPlayer(players[i], i, roomTypeff);
        }
    },

    addHistoryPlayer: function (playerData, index, roomTypeff) {

        var player = cc.instantiate(this.historySinglePrefab);
        cc.log(this.deskIndex + "--addDeskPlayer--" + this.deskPosistion);

        var x = -280 + 280 * (index % 3);
        var y = 45 - parseInt(index / 3) * 120;
        player.setPosition(x, y);

        //player.setPosition(-370 + 220 * index, 50);

        player.parent = this.historyContainerNode;

        cc.log("传递房间类型addDeskPlayer：" + roomTypeff);
        var playerRender = player.getComponent("PlayHistorySingleUser");
        playerRender.init(playerData, roomTypeff);
    },

    getRoomId: function () {
        if (this.playdata) {
            return this.playdata.roomid;
        }
        return "";
    },

    updateDeskPlayer: function (player) {

    },

    getDeskIndex: function () {
        return this.deskIndex;
    },

    start: function () {

    },

    // update (dt) {},
});
