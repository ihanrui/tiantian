var beiMiCommon = require("MileCommon");
var GameUtil = require("WangdaGameUtils");
var GameConstants = require("GameConstants");
cc.Class({
    extends: beiMiCommon,
    properties: {
        deskgroupDownPrefab: {
            default: null,
            type: cc.Prefab
        },
        deskgroupUpPrefab: {
            default: null,
            type: cc.Prefab
        },
        deskgroupContainerNode: {
            default: null,
            type: cc.Node
        },
        marquee: {
            default: null,
            type: cc.Label
        }
    },

    onLoad: function () {
        this.desks = [];

        this.initgame();
        this.testTableIndex = 1;
    },
    initgame: function () {

        var self = this;
        cc.log("初始化游戏  this.ready() 【" + this.ready() + "】")
        if (this.ready()) {


            this.initSocketMapping();

            this.initSocketEvent();

            this.emitGetTeahousInfo();

            cc.mile.inTeaHouse = true;
            this.inited = true;

            //进入后台
            if (cc && cc.eventManager) {
                cc.eventManager.addCustomListener("socketReconnect", function (event) {
                    cc.log('-----socketReconnect---floor--后台更新了--!');

                    if (self) {

                        self.initSocketEvent();
                    }
                });
            }
        }

    },


    getTeahousMemInfo: function () {

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            gameid: cc.mile.user.gameid,
        };
        this.emitSocketRequest("getmeminfo", JSON.stringify(param));

    },

    initSocketEvent: function () {
        var socket = this.socket();
        var self = this;
        socket.on("command_teahouse", function (result) {

            if (self.inited == true) {
                var data = self.parse(result);

                cc.log("事件 data.command [" + data.command + "]  数据内容为:  " + JSON.stringify(result));

                self.route(data.command)(data, self);
            }
        });
        /**
         * 心跳检查，服务端发起的事件，服务端可以设置 PING的时长和 PING的 TimeOut
         */
        socket.on("ping", function () {

        });
    },
    //滚动对象，滚动宽度,滚动完的速度
    marqueeRun: function (text,width,speedtime) {
        //初始位置
        text.node.x=width/2+(text.node.width / 2);
        //移动到的位置
        var moveleft = -width/ 2 - (text.node.width / 2);
        var time = this.getMarqueeTime();
        text.node.runAction(
            cc.repeatForever(
                cc.sequence(cc.moveTo(speedtime, cc.p(moveleft, text.node.y)),
                    cc.callFunc(function () {
                        text.node.x = width;
                        //初始位置=左边+滚动文字宽度除以2就是文字的中点
                        text.node.x =width/2+width/2;
                    })
                )
            ), time);
    },
    getMarqueeTime: function () {
        if(this.marquee.string){
            this.marquee.string.length / 25;
        }
        return 1;
    },
    initSocketMapping: function () {

        this.map("getteahouse", this.getteahouse_event);          //创建茶楼
        this.map("updateteahouse", this.updateteahouse_event);       //修改茶楼  包括销毁 、 打烊 、开业
        this.map("applyjoin", this.applyjoin_event);       //修改茶楼  包括销毁
        this.map("memupdate", this.memupdate_event);       //成员修改   审核、删除、设置为店小二
        this.map("shiftfund", this.shiftfund_event);       // 茶楼基金转入
        this.map("getmeminfo", this.getmeminfo_event);       //获取成员信息
        this.map("dealscore", this.dealscore_event);       //成员上下分
        this.map("tablechange", this.tablechange_event);       //成员上下分
        this.map("sitdown", this.sitdown_event);       // 成员坐下桌子
        //this.map("teahouseplay", this.dealscore_event);       //成员上下分
        this.map("kickplayer", this.kickplayer_event);       //踢出成员成功消息
        this.map("teadissroom", this.teadissroom_event);       //踢出成员成功消息

    },
    initTableDesks: function () {

        this.deskgroupContainerNode.removeAllChildren();

        var indexY = 0;
        for (var i = 0; i < 4; i++) {
            var topDesk = cc.instantiate(this.deskgroupUpPrefab);
            var deskRender = topDesk.getComponent("TeaHouseDeskRender");
            //算出平均相隔的宽度=(总宽度-4*（桌子宽度）)/5

            //获取桌子离左边的距离
            var left = this.compute_teahouse_leftwidth(topDesk.width, i);
            deskRender.init(i + 1);
            topDesk.setPosition(left, indexY + 30);
            topDesk.parent = this.deskgroupContainerNode;
            this.desks.push(topDesk);
        }

        for (var i = 4; i < 8; i++) {
            var downDesk = cc.instantiate(this.deskgroupDownPrefab);
            var deskRender = downDesk.getComponent("TeaHouseDeskRender");
            deskRender.init(i + 1);
            //获取桌子离左边的距离
            var left = this.compute_teahouse_leftwidth(downDesk.width, (i - 4));
            downDesk.setPosition(left, indexY - 150);
            downDesk.parent = this.deskgroupContainerNode;
            this.desks.push(downDesk);
        }
    },
    //计算出桌子与桌子之前的距离
    compute_teahouse_leftwidth: function (topDeskWidth, indexI) {
        ///获取总宽度
        var deskgroupWidth = this.deskgroupContainerNode.parent.parent.width;
        //获取桌子之前的距离
        var deskWidth = (deskgroupWidth - (4 * topDeskWidth)) / 5;
        var left = -(deskgroupWidth / 2) + (topDeskWidth) / 2 + ((indexI) * (topDeskWidth - 15))
            + deskWidth * (indexI + 1) + 15;
        return left;
    },
    /**
     * 新创建牌局，首个玩家加入，进入等待状态，等待其他玩家加入，服务端会推送 players数据
     * @param data
     * @param context
     */
    getteahouse_event: function (data, context) {
        cc.log("茶楼 context[" + JSON.stringify(data) + "]");
        var dataParse = context.parse(data);
        cc.mile.teahouse = data.teahouse;
        context.initTableDesks();
        context.getTeahousMemInfo();
        context.initTeaHouseData(dataParse);
        context.closeloadding();
        //滚动

        /*if (data.teahouse.state == "2") {
            context.closeOpenWin();
            context.sceneMz("mazihall");
            return;
        }*/
    },
    // 成员坐下桌子
    /**
     *
     NOTEXIST,  //房间不存在
     FULL,   //房间已满员
     OK,   //加入成功
     DISABLE, //房间启用了 禁止非邀请加入
     INVALID, //房主已离开房间
     EXIST,  //已在房间
     CREATE,//需要新建房间
     NOTEA; //费茶楼人员
     }
     * @param data
     * @param context
     */
    sitdown_event: function (data, context) {

        cc.log(data);
        if (data.errcontext) {
            context.showCenterToast(data.errcontext);

        } else {
            if (data.sitflag == 'ok' || data.sitflag == 'create') {
                cc.mile.extparams = cc.mile.teahouse.extparams;
                cc.mile.tablenum = data.tablenum;
                context.scene(cc.mile.extparams.gametype, context);
                return;
            } else if (data.sitflag == 'full') {
                context.showCenterToast("房间已满员");
            } else if (data.sitflag == 'notexist') {
                context.showCenterToast("房间不存在");
            } else if (data.sitflag == 'exist') {
                context.showCenterToast("已在房间，请重启打开游戏！");
            } else if (data.sitflag == 'notpass') {
                context.showCenterToast("请等待管理员审核！");
            } else if (data.sitflag == 'nofund') {
                context.showCenterToast("茶楼基金不足，请联系馆长！");
            } else if (data.sitflag == 'noscore') {
                context.showCenterToast("茶楼积分不足，请联系馆长！");
            } else if (data.sitflag == 'notea') {
                context.showCenterToast("非茶楼成员，不允许加入！");
            } else if (data.sitflag == 'ipforbid') {
                context.showCenterToast("同IP禁止加入！");
            }
            context.closeloadding();
        }

    },
    // 成员坐下桌子
    leave_event: function (data, context) {

        cc.log("leave_event" + data);


    },
    // 更新茶楼： 公告、名称
    /**
     UPDATE_NOTICE,  //名称 公告
     DESTORY, //销毁
     TEASTATUS,//打烊/开业
     UPDATE_PLAYWAY,//修改玩法
     UPDATE_SETTING,//修改 同IP禁止，是否分享，是否审核  门票收取方式

     * @param data
     * @param context
     */
    updateteahouse_event: function (data, context) {

        cc.log(data);
        var dataParse = context.parse(data);
        var preNums = cc.mile.teahouse.nums;
        cc.mile.teahouse = dataParse.teahouse;
        if (dataParse.teaflag == 'update_notice') {
            context.showCenterToast("修改成功");

            if (dataParse.teahouse && dataParse.teahouse.teanotice != "") {
                context.marquee.string = dataParse.teahouse.teanotice;
                debugger;
                if(context.marquee.node.x==793&&context.marquee.string!=""&&context.marquee.string!=undefined)
                {
                    context.marqueeRun(context.marquee,1280,25);
                }
            }
            var sceneLoad = context.getCommon("SceneLoad");
            sceneLoad.updateTeaHouseView();
        } else if (dataParse.teaflag == 'teastatus') {
            if (dataParse.teahouse.state == 'open') {
                context.showCenterToast("开业成功！");
            } else {
                context.showCenterToast("打烊成功！");
            }
            var teaHouseClick = context.getCommon("TeaHouseClick");
            teaHouseClick.initTeahouseView(data.teahouse);
        } else if (dataParse.teaflag == 'update_playway') {

            if(preNums != cc.mile.teahouse.nums ){
                context.initTableDesks();
            }

            context.showCenterToast("更新成功！");
        } else if (dataParse.teaflag == 'update_setting') {
            context.showCenterToast("更新成功！");

        } else if (dataParse.teaflag == 'destory') {
            if (cc.mile.socket && cc.mile.socket.off) {
                cc.mile.socket.off("command_teahouse");
            }

            context.scene("mazihall", context);
        }

    },
    // 茶楼成员更新
    tablechange_event: function (data, context) {


        cc.log("tablechange_event" + data);
        var dataParse = context.parse(data);
        context.updateTableChangeView(dataParse);

    },

    updateTableChangeView: function (data) {

        if (data.roomtype == cc.mile.createroomtype) {

            var desk = this.getTabeRenderByIndex(data.tablenum);
            var render = desk.getComponent("TeaHouseDeskRender");
            //var url = this.getTableBackGroud(data.num);
            //desk.height = 210;
            //desk.width = 288;
            //更换桌子背景
            // render.deskBackgroud.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/startCardBg.png"),cc.rect(render.deskBackgroud.width, render.deskBackgroud.height,288, 209));
            ///render.deskBackgroud.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(cc.url.raw(url), cc.rect(0, 0, 288, 209));

            render.addDeskPlayers(data.userInfoList, data.currentnum, data.begindate);
        }

    },
    onTestTableChange: function () {

        var desk = this.getTabeRenderByIndex(this.testTableIndex);
        this.testTableIndex++;
        var render = desk.getComponent("TeaHouseDeskRender");
        render.addDeskPlayers([{username: 'tsetste'}, {username: 'tsetste'}, {username: 'tsetste'}, {username: 'tsetste'}]);
    },
    // 获取茶楼成员列表信息
    getmeminfolist_event: function (data, context) {
        cc.log(data);
        cc.log("茶楼 111context[" + JSON.stringify(data) + "]");

    },// 获取茶楼自己的信息
    getmeminfo_event: function (data, context) {
        cc.log(data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        cc.mile.teahouseSelf = data.teamem;
        var dataParse = context.parse(data);
        context.initTeaMemData(dataParse);

    },
    // 成员加入
    applyjoin_event: function (data, context) {
        cc.log(data);

        context.alert(data.errcontext);

    },
    // 成员信息更新， 审核、删除、设置为店小二
    memupdate_event: function (data, context) {

        cc.log(data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        if (data.teamem && data.teamem.passflag == 'out') {
            if (cc.mile.socket && cc.mile.socket.off) {
                cc.mile.socket.off("command_teahouse");
            }
            context.scene("mazihall", context);
        } else {
            cc.mile.teahouse.updatmemeber = data.teamem;
        }
        //添加小二
        if (data.teamem.passflag == "pass" && data.teamem.role == "manager") {
            context.showCenterToast("你已成功添加该用户为小二");
        }

    },// 茶楼转入基金
    shiftfund_event: function (data, context) {

        cc.log(data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        cc.mile.shiftfund = data;

    },
    // 成员上下分
    dealscore_event: function (data, context) {

        cc.log(data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        cc.mile.dealscore = data;

    },
    //桌子踢出成员成功消息接受
    kickplayer_event: function (data, context) {
        cc.log("踢出成员" + data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        context.closeOpenWin();
    },//桌子强制解散
    teadissroom_event: function (data, context) {
        cc.log("踢出成员" + data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        context.closeOpenWin();
    },
    getTabeRenderByIndex: function (tableIndex) {
        var desk = this.desks[tableIndex - 1];
        return desk;
    },
    clearTablePlayers: function () {

        for(var i = 0 ; i < this.desks.length ; i++){
            var desk = this.desks[i];
            var render = desk.getComponent("TeaHouseDeskRender");
            render.clearDeskPlayer();
        }
    },
    initTeaHouseData: function (data) {

        this.clearTablePlayers();

        for (var i = 0; i < data.tableInfoList.length; i++) {
            this.updateTableChangeView(data.tableInfoList[i]);
        }
        var teaHouseClick = this.getCommon("TeaHouseClick");
        teaHouseClick.initTeahouseView(data.teahouse);
        if (data.teahouse && data.teahouse.teanotice != "") {
            this.marquee.string = data.teahouse.teanotice;
        }
        var text = this.marquee;
       debugger;
        if(this.marquee.node.x==793&&this.marquee.string!=""&&this.marquee.string!=undefined)
        {
            this.marqueeRun(this.marquee,1280,25);
        }
    },
    initTeaMemData: function (data) {
        var teaHouseClick = this.getCommon("TeaHouseClick");
        teaHouseClick.initTeaHouseMemberInfo(data.teamem);
    },
    onDestroy: function () {
        cc.log('-----destroy teahouse view ---');
        cc.mile.inTeaHouse = false;
        if (cc.mile.socket && cc.mile.socket.off) {
            cc.mile.socket.off("command_teahouse");
        }

        if (cc && cc.eventManager) {
            cc.eventManager.removeCustomListeners("socketReconnect");
        }
    }

});
