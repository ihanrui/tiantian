var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        datetime_label: {
            default: null,
            type: cc.Label
        },
        num_label: {
            default: null,
            type: cc.Label
        },
    },
    onLoad: function () {

    },
    init: function (data) {
        this.datetime_label.string = data.reportdate;
        this.num_label.string = data.num;
    },
});
