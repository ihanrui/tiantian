var beiMiCommon = require("MileCommon");


cc.Class({
    extends: beiMiCommon,

    properties: {

        nodeContainer: {
            default: null,
            type: cc.Node
        },
        foundationPrefab: {
            default: null,
            type: cc.Prefab
        },
        creditPrefab: {
            default: null,
            type: cc.Prefab
        },
        ticketManager: {
            default: null,
            type: cc.Prefab
        },
        foundItemsManager: {
            default: null,
            type: cc.Prefab
        },

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //var ff = cc.instantiate(this.foundationPrefab);
        var ff = cc.instantiate(this.ticketManager);
        ff.parent = this.nodeContainer;
        /*this.queryPlayerRecord({
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
        });*/
    },

    /**
     *
     passflag:
     APPLY,    //申请
     PASS,   //审核通过
     OUT,     //离开茶楼
     REFUSE,  //拒绝
     FORBID;//禁止加入
     */
    queryPlayerRecord: function (param) {
        cc.mile.http.httpPost("/teahouse/getteafundrecordlist", param, this.successGetPlayer, this.errorGetPlayer, this);

    },
    successGetPlayer: function (result, context) {
        debugger
        cc.log(result);
        var data = JSON.parse(result);
        context.loadPlayer(data);

    },
    errorGetPlayer: function (result, context) {
        debugger
        cc.log(result);
    },
    loadPlayer: function (items) {
        var ff = cc.instantiate(this.foundItemsManager);
        ff.parent = this.nodeContainer;
    },
    initFoundatioanView: function () {
        var ff = cc.instantiate(this.foundationPrefab);
        ff.parent = this.nodeContainer;
    },

    initCreditView: function () {
        var ff = cc.instantiate(this.creditPrefab);
        ff.parent = this.nodeContainer;
        var member = {"id":"34369fd4-5e02-474a-aaa6-099344141330","gameid":"66093731",
            "userid":"0793c29296174367bd588a4de62ac097","username":"Guest_1p0MBU",
            "userurl":"http://thirdwx.qlogo.cn/mmopen/vi_32/twibiaV0K4L2eEaGvSlhgpnjYUwEZcAv220dSic5cTK9bHoMJiambJt67ThQqU0Wssd3UVM2YaiaicxyekGTM4Wm12Nw/132","teanum":"0000812",
            "score":0,"limitscore":0,"passflag":"pass","role":"owner","lastmodify":"2018-04-25 03:20:49"};
        var render = ff.getComponent("PlayerTeaCreditScore");
        render.initData(member);
        render.initView();
    }


});
