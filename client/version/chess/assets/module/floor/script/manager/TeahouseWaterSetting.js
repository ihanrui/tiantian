var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {

        optionGroupPrefab: {
            default: null,
            type: cc.Prefab
        },
        dropselectGroupPrefab: {
            default: null,
            type: cc.Prefab
        },
        rowPrefab: {
            default: null,
            type: cc.Prefab
        },
        paramContainer: {
            default: null,
            type: cc.Node
        },
        confirmBtn: {
            default: null,
            type: cc.Node
        },
    },
    onLoad: function () {

        this.paramsData = {
            groups: [
                {id: "4028ab6", name: "打赏模式", code: "ticketflag", type: 'radio', width: 700},
                {id: "4028ab2", name: "值", code: "ticketvalue", type: 'input', width: 700},
                {id: "4028ab23", name: "积分下限", code: "limitscore", type: 'input', width: 700}
            ],
            items: [
                {
                    id: "402", name: "固定值",
                    defaultvalue: this.getMethodDefaultValue('fix1'), code: "fix1", groupid: "4028ab6"
                },
                {
                    id: "402", name: "固定比例",
                    defaultvalue: this.getMethodDefaultValue('persent1'), code: "persent1", groupid: "4028ab6"
                },
                {id: "403", name: "请输入值", code: "15", groupid: "4028ab2", value: cc.mile.teahouse.ticketvalue},
                {id: "403", name: "请输入值", code: "151", groupid: "4028ab23", value: cc.mile.teahouse.limitscore},
            ],
        };

        this.optionGroupLst = [];

        this.initSettingParams();

    },
    getMethodDefaultValue: function (method) {
        if (cc.mile.teahouse && cc.mile.teahouse.ticketflag) {
            if (cc.mile.teahouse.ticketflag == method) {
                return true;
            }
        }
        return false;
    },
    initSettingParams: function () {


        if (this.optionGroupPrefab != null) {

            while (this.optionGroupLst.length) {
                var tmp = this.optionGroupLst.pop();
                tmp.destroy();
            }


            for (var inx = 0; inx < this.paramsData.groups.length; inx++) {
                var row = cc.instantiate(this.rowPrefab);
                var rowPosition = cc.v2(0, 110 - inx * 70);
                row.setPosition(rowPosition);
                row.parent = this.paramContainer;
            }

            for (var inx = this.paramsData.groups.length - 1; inx >= 0; inx--) {

                if (this.paramsData.groups[inx].type == 'radio') {

                    var selectGroup = cc.instantiate(this.dropselectGroupPrefab);
                    var selectOptionGroup = selectGroup.getComponent("SelectGroup");
                    selectOptionGroup.init(this.paramsData.groups[inx], this.paramsData.items);
                    var selfSelectPosition = cc.v2(-160, 110 - inx * 70);
                    selectGroup.setPosition(selfSelectPosition);
                    selectGroup.parent = this.paramContainer;

                    this.optionGroupLst.push(selectOptionGroup);

                } else {

                    var group = cc.instantiate(this.optionGroupPrefab);
                    var optionGroup = group.getComponent("inputAndCheckboxGroup");
                    optionGroup.init(this.paramsData.groups[inx], this.paramsData.items);
                    var selfPosition = cc.v2(0, 110 - inx * 70);
                    group.setPosition(selfPosition);
                    group.parent = this.paramContainer;

                    this.optionGroupLst.push(optionGroup);
                }
            }

        }
    },
    onConfirmClick: function () {

        var ticketflag = "";
        var ticketvalue = "";
        var limitscore = "";
        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            if (groupitem.getGroupData()) {
                if (groupitem.getGroupData().code == "ticketflag") {
                    ticketflag = this.getCheckedValue(groupitem.groupoptions);
                } else if (groupitem.getGroupData().code == "ticketvalue") {
                    ticketvalue = this.getInputValue(groupitem.groupoptions);
                } else if (groupitem.getGroupData().code == "limitscore") {
                    limitscore = this.getInputValue(groupitem.groupoptions);
                }
            }
        }
        if (ticketvalue == "" || ticketflag == "") {
            this.showCenterToast("请抽水方式和抽水值!");
            return;
        }
        var fTicketValue = parseFloat(ticketvalue);
        if (fTicketValue < 0) {
            this.showCenterToast("抽水值应该大于0!");
            return;
        }
        if (fTicketValue > 20 && ticketflag == "persent1") {
            this.showCenterToast("固定比例需在0到20之间，如:5 !");
            return;
        }
        cc.log(ticketflag + "---" + ticketvalue);
        var params = {
            ticketflag: ticketflag,
            ticketvalue: ticketvalue,
            limitscore: limitscore,
            token: cc.mile.authorization,
            teanum: cc.mile.teahouse.teanum,
            orgi: cc.mile.user.orgi
        }
        this.emitSocketRequest("updateteahouse", JSON.stringify(params));

    },


    getInputValue: function (groupoptions) {
        for (var j = 0; j < groupoptions.length; j++) {
            var option = groupoptions[j];
            return option.getInputValue();

        }
        return "";
    },


    getCheckedValue: function (groupoptions) {
        for (var j = 0; j < groupoptions.length; j++) {
            var option = groupoptions[j];
            if (option.checked == true) {
                return option.item.code;
            }
        }
        return "";
    }

    // update (dt) {},
});
