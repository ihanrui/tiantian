
/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        index_number_label: {
            default: null,
            type: cc.Label
        },
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_id_label: {
            default: null,
            type: cc.Label
        },
        user_bean_before_label: {
            default: null,
            type: cc.Label
        },
        user_bean_after_label: {
            default: null,
            type: cc.Label
        },
        score_time_node: {
            default: null,
            type: cc.Label
        },
        score_node: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    init: function (index, player) {
        this.playData = player;
        this.index_number_label.string = index + 1;
        this.user_name_label.string = player.username;
        this.user_id_label.string = player.gameid;
        WangdaGameUtils.showNumberNode(player.score,this.score_node);
    },

    start: function () {

    },

    // update (dt) {},
});
