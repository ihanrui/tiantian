
/**
 * 查看历史
 */


var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        teafoundation_label: {
            default: null,
            type: cc.Label
        },
        myfoundation_label: {
            default: null,
            type: cc.Label
        },
        transfer_input: {
            default: null,
            type: cc.EditBox
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.initView();
    },

    transferAction: function () {

        // 调用解散桌子的请求
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            teanum: cc.mile.teahouse.teanum,
            fund: this.transfer_input.string,
        };
        cc.mile.shiftfund = null;
        this.emitSocketRequest("shiftfund", JSON.stringify(param));
        this.checkCount = 20;
        this.checkEmitCallback();
    },
    checkEmitCallback: function () {
        this.scheduleOnce(function () {
            if (cc.mile.shiftfund) {
                this.updateFundsAndView();
            } else {
                this.checkCount--;
                if(this.checkCount > 0){
                    this.checkEmitCallback();
                }
            }
        }, 0.5);
    },

    updateFundsAndView: function () {
        if(cc.mile.shiftfund.flag){
            this.showCenterToast("转入基金成功!");
            var transferNums = this.transfer_input.string;
            var transferNumsInt = parseInt(transferNums);
            var selfNumbs = this.myfoundation_label.string
            var selfNumbsInt = parseInt(selfNumbs);
            cc.log("用户信息："+ JSON.stringify(cc.mile.user));
            if(cc.mile.user && selfNumbsInt == cc.mile.user.diamonds){
                cc.mile.user.diamonds = cc.mile.user.diamonds - transferNumsInt;
            }
            cc.mile.teahouse.fund = cc.mile.teahouse.fund + transferNumsInt;

            this.initView();
        }else {
            this.showCenterToast("转入基金失败，请重新操作!");
        }
    },
    initView: function () {
        if('fund' in cc.mile.teahouse ){
            this.teafoundation_label.string = cc.mile.teahouse.fund;
        }else {
            this.teafoundation_label.string = 0;
        }
        this.myfoundation_label.string = cc.mile.user.diamonds;
    },

    onConfirmTransfer:function () {
        var transferNums = this.transfer_input.string;
        if(transferNums != ""){

            var transferNumsInt = parseInt(transferNums);
            if(transferNumsInt < 0){
                this.showCenterToast("转入基金数量不能小于0");
                return;
            }
            if(transferNums > cc.mile.user.diamonds){
                this.showCenterToast("转入数量不能大于自己的钻石数量："+cc.mile.user.diamonds);
                return;
            }
            this.transferAction();
        }else {
            this.showCenterToast("请输入转入数量!");
        }
    },
    start: function () {

    },

    // update (dt) {},
});
