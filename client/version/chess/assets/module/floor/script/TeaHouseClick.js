var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        openBusinessBtn: {
            default: null,
            type: cc.Node
        },
        closeBusinessBtn: {
            default: null,
            type: cc.Node
        },
        zantingyingye_liuyan: {
            default: null,
            type: cc.Prefab
        },
        zantingyingye: {
            default: null,
            type: cc.Prefab
        },
        teahouseManager: {
            default: null,
            type: cc.Prefab
        },
        teaPerson: {
            default: null,
            type: cc.Prefab
        },
        mark: {
            default: null,
            type: cc.Prefab
        },
        sharePrefab: {
            default: null,
            type: cc.Prefab
        },
        UpdateTeaHouseRule: {
            default: null,
            type: cc.Prefab
        },
        set: {
            default: null,
            type: cc.Prefab
        },
        user_head: {
            default: null,
            type: cc.Node
        },
        userName: {
            default: null,
            type: cc.Label
        },
        userNumber: {
            default: null,
            type: cc.Label
        },
        roomCardNum: {
            default: null,
            type: cc.Label
        },
        room_type_btn: {
            default: null,
            type: cc.Node
        },
        teahouseTestManager: {
            default: null,
            type: cc.Prefab
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
    },

    initTeahouseView: function (teahouseData) {
        this.teahouseData = teahouseData;
        if (this.teahouseData.state == 'open') {
            this.updateOpenBusinessViewStatus(true);
        } else if (this.teahouseData.state == 'close') {
            this.updateOpenBusinessViewStatus(false);
            if(this.teahouseData.closenotice){
                cc.mile.openwin = cc.instantiate(this.zantingyingye);
                cc.mile.openwin.parent = this.root();
                this.getPrefabCommon("zantingyingye", "Zantingyingye").init(this.teahouseData.closenotice);
            }
        }
    },

    initTeaHouseMemberInfo: function (teamem) {
        this.loadUrlJPGImage(cc.mile.user.userurl
            + "&username=" + cc.mile.user.id, this.user_head);
        this.userName.string = cc.mile.user.username;
        this.userNumber.string = cc.mile.user.gameid;
        this.roomCardNum.string = cc.mile.user.diamonds;

        this.teaHouseMemberInfo = teamem;
    },
    onTeahouseManagerClik: function () {

        cc.mile.openwin = cc.instantiate(this.teahouseManager);
        //cc.mile.openwin = cc.instantiate(this.teahouseTestManager);
        cc.mile.openwin.parent = this.root();

    },
    onTeaPersonClik: function () {

        if (this.teaHouseMemberInfo && this.teaHouseMemberInfo.role != "normal") {
            cc.mile.openwin = cc.instantiate(this.teaPerson);
            cc.mile.openwin.parent = this.root();
        } else {
            this.showCenterToast("亲，只有管理员才有权限!")
        }
    },
    // 茶楼战绩
    onMarkClik: function () {

        cc.mile.teahouserrole = this.teaHouseMemberInfo.role;

        cc.mile.openwin = cc.instantiate(this.mark);
        cc.mile.openwin.parent = this.root();
    },
    onShareClik: function () {

        cc.mile.openwin = cc.instantiate(this.sharePrefab);
        cc.mile.openwin.parent = this.root();
        var shareScript = this.getPrefabCommon("share", "FenxiangDialog");
        shareScript.initShareType("share_floor");
    },
    onSetClik: function () {
        cc.mile.openwin = cc.instantiate(this.set);
        cc.mile.openwin.parent = this.root();
        var shareScript = this.getPrefabCommon("musicinstall", "MusicInstall");
        shareScript.updateQuiteBtnViewStatus(true);
    },
    //玩法
    onUpdateRoomClik: function () {
        var updateTeaHouseRuleView = cc.instantiate(this.UpdateTeaHouseRule);
        cc.mile.openwin = updateTeaHouseRuleView;
        cc.mile.openwin.parent = this.root();
        updateTeaHouseRuleView.getComponent("UpdateTeaHouseRule").initRuleData();


    },
    onIsOpenClik: function () {
        if (this.teaHouseMemberInfo && this.teaHouseMemberInfo.role != "normal") {
            if (this.closeBusinessBtn.active) {
                this.closeBusiness();
            } else {
                this.openBusiness();
            }
        } else {
            this.showCenterToast("亲，只有管理员才有权限!")
        }
    },
    openBusiness: function () {
        this.confirm("开业后，玩家可以在茶楼玩游戏！", function (self) {
            var param = {
                teanum: cc.mile.teahouse.teanum,
                token: cc.mile.authorization,
                state: "open"
            };
            self.emitSocketRequest("updateteahouse", JSON.stringify(param));
            self.closeConfirm();
        });
    },

    onSwitchRoomTypeClick: function () {
        var GameConstants = require("GameConstants");
        var WangdaGameUtils = require("WangdaGameUtils");
        var roomType = cc.mile.createroomtype;//WangdaGameUtils.getStorageToLocal(GameConstants.CREATE_ROOM_TYPE_KEY);
        if(GameConstants.CREATE_ROOM_TYPE_CARD == roomType){
            WangdaGameUtils.setStorageToLocal(GameConstants.CREATE_ROOM_TYPE_KEY,GameConstants.CREATE_ROOM_TYPE_TEACOIN);
            this.room_type_btn.getComponent(cc.Sprite).spriteFrame =
                new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/jifenwanfa.png"));
            cc.mile.createroomtype = GameConstants.CREATE_ROOM_TYPE_TEACOIN;
        }else {
            this.room_type_btn.getComponent(cc.Sprite).spriteFrame =
                new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/jingdianwanfa.png"));

            WangdaGameUtils.setStorageToLocal(GameConstants.CREATE_ROOM_TYPE_KEY,GameConstants.CREATE_ROOM_TYPE_CARD);
            cc.mile.createroomtype = GameConstants.CREATE_ROOM_TYPE_CARD;
        }


        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            roomtype: cc.mile.createroomtype,
            teanum: cc.mile.teahouse.teanum,
        };
        this.emitSocketRequest("getteahouse", JSON.stringify(param));
        this.loadding();
    },

    closeBusiness: function () {
        cc.mile.openwin = cc.instantiate(this.zantingyingye_liuyan);
        cc.mile.openwin.parent = this.root();
    },
    updateOpenBusinessViewStatus: function (isOpen) {
        this.openBusinessBtn.active = !isOpen;
        this.closeBusinessBtn.active = isOpen;
    },
    start: function () {

    },

    // update (dt) {},
});
