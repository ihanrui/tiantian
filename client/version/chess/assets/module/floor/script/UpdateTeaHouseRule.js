var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        //游戏节点树（存放所有游戏）
        playways_node: {
            default: null,
            type: cc.Node
        },
        //游戏对象
        playway: {
            default: null,
            type: cc.Prefab
        },
        //游戏属性节点树（存放游戏属性）
        options_group_node: {
            default: null,
            type: cc.Node
        },
        //游戏属性组
        options_group: {
            default: null,
            type: cc.Prefab
        },//游戏属性组
        rowPrefab: {
            default: null,
            type: cc.Prefab
        },
        dropselectGroupPrefab: {
            default: null,
            type: cc.Prefab
        },
        confirmUpdateRule_btn: {
            default: null,
            type: cc.Node
        }
    },
    onLoad: function () {

    },

    initRuleData: function () {


        this.group = new Array();
        this.playwayarray = new Array();
        this.optionGroupLst = new Array();
        this.extparams = cc.mile.teahouse.extparams;
        this.switchIndex = 0;
        this.switchFrameCode = '';
        /**
         * 加载预制的 玩法
         */
        var gametype = cc.mile.game.type("createroom");
        if (gametype != null) {
            //加载游戏列表
            for (var inx = 0; inx < gametype.playways.length; inx++) {
                //从对象池中获取 playway 对象
                var playwayData = gametype.playways[inx];

                if (playwayData.code == this.extparams.gametype) {

                    playwayData.dataIndex = inx;
                    this.switchIndex = inx;
                    this.switchFrameCode = playwayData.code;


                    this.initPlayway(playwayData, inx);


                    var updateRuleBtnJs = this.confirmUpdateRule_btn.getComponent("CreateRoomBtn");
                    updateRuleBtnJs.setIndex(inx);

                    break;
                }
            }
        }

        var self = this;
        this.node.on('ItemCheckChanged', function (event) {
            console.log('-----ItemCheckChanged-----click--!');
            if (self) {
                var itemData = event.getUserData();
                console.log('-----ItemCheckData--:' + JSON.stringify(itemData));

                self.dynamicUpdateView(itemData);
            }
        });
        this.confirmUpdateRule_btn.on(cc.Node.EventType.TOUCH_START, function (e) {
            console.log('-----confirmUpdateRule_btn-----click--!');
            if (self) {
                var btnJS = e.target.getComponent("CreateRoomBtn");
                if(btnJS){
                    console.log('-----confirmUpdateRule_btn-----call');
                    this.onCreateRoomBtnClick(btnJS.getIndex());
                }
            }
            e.stopPropagation();
        }, this, true);


    },
    initCreateType: function (type) {
        this.createType = type;
    },
    dynamicUpdateView: function (checkItemData) {
        if (this.switchFrameCode == 'mazi') {
            this.scheduleOnce(function () {
                if (checkItemData.group.code == 'roomtype') {
                    this.updateSelectDefaultValue();
                    this.updateMaziDefaultValue(checkItemData.group.code, checkItemData.item.code);
                    var playwayData = cc.mile.game.type("createroom").playways[this.switchIndex];
                    this._setPlayWayData(playwayData);
                }
            }, 0.05);
        }
    },
    updateMaziDefaultValue: function (groupCode, itemCode) {

        var playwayData = cc.mile.game.type("createroom").playways[this.switchIndex];
        if (groupCode == 'roomtype') {
            for (var inx = 0; inx < playwayData.groups.length; inx++) {
                var groupitem = playwayData.groups[inx];
                if(itemCode == 'card' && groupitem.code == 'scoreunit'){
                    groupitem.hidden = true;
                }else if( (itemCode == 'teacoin' || itemCode == 'coin') && groupitem.code == 'numofgames'){
                    groupitem.hidden = true;
                }else {
                    groupitem.hidden = false;
                }
            }
        }
    },
    updateSelectDefaultValue: function () {
        var playwayData = cc.mile.game.type("createroom").playways[this.switchIndex];
        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            for (var g = 0; g < playwayData.groups.length; g++) {
                var groupdata = playwayData.groups[inx];
                if (groupitem.data.code == groupdata.code) {
                    this.updateItemDataDefaultValue(groupitem.groupoptions, playwayData.items, groupdata.id);
                }
            }
        }
    },
    updateItemDataDefaultValue: function (groupoptions, dataItems, groupid) {
        for (var j = 0; j < groupoptions.length; j++) {
            var option = groupoptions[j];
            for (var i = 0; i < dataItems.length; i++) {
                if (dataItems[i].groupid == groupid) {
                    if (option.item.id == dataItems[i].id ) {
                        dataItems[i].defaultvalue = option.checked;
                    }
                }
            }
        }
    },


    initPlayway: function (playwayData, inx) {
        var playwayComponent = cc.instantiate(this.playway);
        if (playwayComponent != null) {
            //获取 playway 身上的 PlayWay 脚本 并进行初始化
            var playwayjs = playwayComponent.getComponent("PlayWay");

            playwayjs.setIndex(inx);// 设置当前按钮的 index

            this.playwayarray.push(playwayComponent);

            if (playwayData.code == this.extparams.gametype) {
                for (var i = 0; i < playwayData.groups.length; i++) {

                    var numo = eval("this.extparams." + playwayData.groups[i].code)+'';
                    for (var inx = 0; inx < playwayData.items.length; inx++) {
                        if (playwayData.items[inx].groupid == playwayData.groups[i].id) {
                            playwayData.items[inx].defaultvalue = false;
                            if (numo.indexOf(playwayData.items[inx].value) != -1) {
                                playwayData.items[inx].defaultvalue = true;
                            }
                        }
                    }
                }
                //将生成的节点放在节点树上
                playwayComponent.parent = this.playways_node;

                //this._setSpriteFrame(playwayData, playwayComponent, true);
                //设置属性值
                this._setPlayWayData(playwayData);
            }
        }
    },

    //根据 PlayWay 加载对应的 值
    _setPlayWayData: function (playwayData) {
        debugger
        this.updateRelationDatStatus(playwayData);
        if (this.options_group != null) {
            this.options_group_node.removeAllChildren();
            while (this.optionGroupLst.length) {
                var tmp = this.optionGroupLst.pop();
                tmp.destroy();
            }

            var indexOfRow = 0;

            for (var inx = 0; inx < playwayData.groups.length; inx++) {
                if (playwayData.groups[inx].hidden) {
                    continue;
                }

                var group = playwayData.groups[inx];
                var isBigRow = this.isBigRow(group, playwayData.items);

                var itemrow = cc.instantiate(this.rowPrefab);
                itemrow.parent = this.options_group_node;
                if(isBigRow){
                    itemrow.setPosition(0, 160 - indexOfRow * 55);
                    itemrow.height = 110;
                    indexOfRow++;
                }else {
                    itemrow.setPosition(0, 190 - indexOfRow * 55);
                }
                if (playwayData.groups[inx].type == 'radio'
                    && playwayData.groups[inx].style != 'four'
                    && inx + 1 < playwayData.groups.length
                    && playwayData.groups[inx+1].type == 'radio'
                ) {
                    inx++;
                }
                indexOfRow++;
            }
            this.dropdownselect_groupLst = new Array();
            for (var inx = playwayData.groups.length - 1; inx >= 0; inx--) {
                if (playwayData.groups[inx].hidden) {
                    continue;
                }
                indexOfRow--;
                var group = playwayData.groups[inx];
                var isBigRow = this.isBigRow(group, playwayData.items);
                if(isBigRow){
                    indexOfRow--;
                }
                cc.log("查看信息："+JSON.stringify(playwayData.groups[inx]));
                if (playwayData.groups[inx].type == 'radio'
                    && playwayData.groups[inx].style != 'four'
                    && inx - 1 >= 0
                    && playwayData.groups[inx-1].type == 'radio'
                ) {
                    inx--;
                    this.createGroupItemView(playwayData.groups[inx],playwayData.items,indexOfRow,false,isBigRow);
                    this.createGroupItemView(playwayData.groups[inx+1],playwayData.items,indexOfRow,true,isBigRow);
                }else {

                    this.createGroupItemView(playwayData.groups[inx],playwayData.items,indexOfRow,false,isBigRow);
                }

            }
            for(var i=0;i<this.dropdownselect_groupLst.length;i++) {
                this.dropdownselect_groupLst[i].setList(this.dropdownselect_groupLst);
            }


        }
    },

    createGroupItemView: function (groupdata,items,indexOfRow, isAlignRight,isBigRow) {
        if (groupdata.type == 'radio') {
            var newitem = cc.instantiate(this.dropselectGroupPrefab);
            var script = newitem.getComponent("SelectGroup");
            script.init(groupdata, items);
            newitem.parent = this.options_group_node;
            if(!isAlignRight){
                newitem.setPosition(-155, 190 - indexOfRow * 55);
            }else {
                newitem.setPosition(202, 190 - indexOfRow * 55);
            }
            this.optionGroupLst.push(script);
            this.dropdownselect_groupLst.push(script);
        } else {

            var group = cc.instantiate(this.options_group);
            var optionGroup = group.getComponent("inputAndCheckboxGroup");
            optionGroup.init(groupdata, items);
            group.parent = this.options_group_node;
            if(isBigRow){
                group.setPosition(0, 160 - indexOfRow * 55);
            }else {
                group.setPosition(0, 190 - indexOfRow * 55);
            }
            this.optionGroupLst.push(optionGroup);

        }
    },
    isBigRow: function (group,items) {
        if (group.type == 'radio') {
            return false;
        }
        var itemsnum = this.getGroupItemCount(group.id,items);
        if (group.style != null && group.style == "three") {
            if (itemsnum > 4) {
                return true;
            }
        } else {
            if (itemsnum > 4) {
                return true;
            }
        }
        return false;
    },
    getGroupItemCount: function (groupid, items) {
        var itemsnum = 0;
        for (var inx = 0; inx < items.length; inx++) {
            if (items[inx].groupid == groupid) {
                itemsnum = itemsnum + 1;

            }
        }
        return itemsnum;
    },

    updateRelationDatStatus: function (playwayData) {

        if(this.switchFrameCode == 'mazi'){    // 如果是码子，将卡房和底分调整下关联关系
            for (var inx = 0; inx < playwayData.groups.length; inx++) {
                var group = playwayData.groups[inx];
                if(group.code == 'roomtype' ){
                    for(var i = 0 ; i < playwayData.items.length; i++){
                        var item = playwayData.items[i];
                        if(item.groupid == group.id
                            && (item.code == 'card' || item.code == 'teacoin' || item.code == 'coin')
                            && item.defaultvalue){
                            this.updateMaziDefaultValue(group.code ,item.code);
                            break;
                        }
                    }
                }
            }
        }

    },
    onCreateRoomBtnClick: function (index) {
        debugger
        var playwayData = cc.mile.game.type("createroom").playways[index];
        var extparams = {};

        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            var value = "";
            for (var j = 0; j < groupitem.groupoptions.length; j++) {
                var option = groupitem.groupoptions[j];
                if (option.checked == true) {
                    if (value != "") {
                        value = value + ",";
                    }
                    value = value + option.item.value;
                }
            }
            extparams[groupitem.data.code] = value;
        }
        /**
         * 藏到全局变量里去，进入场景后使用，然后把这个参数置空
         * @type {{}}
         */

        extparams.gametype = playwayData.code;
        extparams.playway = playwayData.id;
        extparams.gamemodel = "room";

        var param = {
            token: cc.mile.authorization,
            teanum: cc.mile.teahouse.teanum,
            extparams: extparams,
        };
        /**
         * 发送修改茶楼的请求
         */

        this.emitSocketRequest("updateteahouse", JSON.stringify(param));
        this.closeOpenWin();
    },
    //取消
    closeOpenWin: function () {
        if (cc.mile.openwin != null) {
            cc.mile.openwin.destroy();
            cc.mile.openwin = null;
        }
    },
});
