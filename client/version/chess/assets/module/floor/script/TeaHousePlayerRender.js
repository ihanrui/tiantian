// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 茶楼桌子的Person
 */
var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        player_head: {
            default: null,
            type: cc.Node
        },
        player_name: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
    },

    init: function (playerData) {
        this.playerData = playerData;
        this.loadUrlJPGImage(playerData.userurl
            + "&username=" + playerData.userid, this.player_head);
        this.player_name.string = playerData.username.length > 5 ? playerData.username.substr(0, 5): playerData.username;
    },

    start: function () {

    },
    getPlayId: function () {
        return this.playerData.playuer;
    }

    // update (dt) {},
});
