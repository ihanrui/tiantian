
/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_id_label: {
            default: null,
            type: cc.Label
        },
        user_ip_label: {
            default: null,
            type: cc.Label
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },
    initEvent: function () {
        debugger
        var self = this;
        this.node.on('tiChuMousedown', function (event) {
            console.log('-----tiChuMousedown-----click--!');
            if (self) {
                console.log('-----tiChuMousedown---调用把用户踢出桌子的请求--click--!');
                self.tiChuMousedown();
            }
        });
    },
    //把用户踢出桌子执行事件
    tiChuMousedown: function (data) {
        debugger
        var self = this;
        this.confirm('确定将'+this.user_id_label.string+'踢出该桌子？', function () {
            cc.log('-----jieSanMousedown---调用把用户踢出桌子的请求--call--!');
            // 调用解散桌子的请求
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                kickplayer: self.playData.userid,
                teanum: cc.mile.teahouse.teanum,
            };
            self.emitSocketRequest("kickplayer", JSON.stringify(param));
            self.closeConfirm();
        });
    },
    //将玩家踢出桌子按钮注册点击事件
    ontiChuClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("tiChuMousedown", true));
    },
    addDeskPlayers: function (player) {
        this.initEvent();
        this.playData = player;
        this.user_name_label.string = player.username;
        this.user_id_label.string = player.gameid;
        this.user_ip_label.string = player.ip;
    },

    start: function () {

    },

    // update (dt) {},
});
