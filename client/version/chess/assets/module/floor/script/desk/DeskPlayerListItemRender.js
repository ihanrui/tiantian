// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

/**
 * 茶楼桌子的Person
 */
var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        desk_player_container: {
            default: null,
            type: cc.Node
        },
        deskPlayerPrefab: {
            default: null,
            type: cc.Prefab
        },
        currentnum_label: {
            default: null,
            type: cc.Label
        },
        begindate_label: {
            default: null,
            type: cc.Label
        },
    },

    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {

    },
    initEvent: function () {
        var self = this;
        this.node.on('jieSanMousedown', function (event) {
            console.log('-----jieSanMousedown-----click--!');
            if (self) {
                console.log('-----jieSanMousedown---调用解散桌子的请求--click--!');
                self.jiesanDeskAction();
            }
        });
    },
    //解散桌子执行事件
    jiesanDeskAction: function () {
        debugger
        var self = this;
        this.confirm('当前进行的游戏将强制解散？', function () {

            cc.log('-----jieSanMousedown---调用解散桌子的请求--call--!');

            // 调用解散桌子的请求
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                tablenum: self.tableIndx,
                teanum: cc.mile.teahouse.teanum,
            };
            self.emitSocketRequest("teadissroom", JSON.stringify(param));

            self.closeConfirm();
        });
        // 调用解散桌子的请求
        /*
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            tablenum: self.deskIndex,
            teanum: cc.mile.teahouse.teanum,
        };
        self.emitSocketRequest("sitdown", JSON.stringify(param));*/
    },
    //初始化桌子成员
    initView: function (tableIndx,players,currentnum,begindate) {
        if(currentnum==undefined||currentnum=="")
        {
            this.currentnum_label.string="";
        }else
            {
                this.currentnum_label.string="局数:"+currentnum;
            }
        if(begindate==undefined||begindate=="")
        {
            this.begindate_label.string="";
        }else {
            this.begindate_label.string="开始时间:"+begindate;
        }
        this.tableIndx = tableIndx;
        this.initEvent();
        if(players && players.length > 0){
            this.addDeskPlayers(players);
        }
    },
    //解散按钮注册点击事件
    onJiesanClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("jieSanMousedown", true));
    },
    //清除桌子成员，再加载桌子当前所有成员
    addDeskPlayers: function (players) {
        this.desk_player_container.removeAllChildren();
        for (var i = 0; i < players.length; i++) {
            this.addDeskPlayer(players[i], i);
        }
    },
    //加载桌子单个成员信息
    addDeskPlayer: function (playerData, index) {

        var player = cc.instantiate(this.deskPlayerPrefab);

        player.setPosition(0, 90 - index * 70);

        player.parent = this.desk_player_container;

        var playerRender = player.getComponent("DeskPlayerItem");
        playerRender.addDeskPlayers(playerData);

    },

    start: function () {

    },

    // update (dt) {},
});
