var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,
    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        numdata: {//绑定teamid
            default:null,
            type:cc.Node


        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad : function () {
        this.teaid = new Array() ;
    },
    onClick:function(event,data){
        if(this.teaid.length < 7){
            this.teaid.push(data);
            this.disTeaHouseId();
        }
        if(this.teaid.length == 7){
            this.closeOpenWin();
            /**
             * 查询服务端的茶楼号码 ， 然后通过茶楼号码找到对应的茶楼游戏类型，玩法等信息
             */
            if(this.ready()){

                /**
                 * 发送 teaHouse请求
                 */
                var param = {
                    token:cc.mile.authorization,
                    teanum:this.teaid.join(""),
                    orgi:cc.mile.user.orgi,
                    userid:cc.mile.user.id,
                    gameid:cc.mile.user.gameid
                } ;
                cc.log("发送到后台token["+cc.mile.authorization+"] teaid["+this.teaid.join("")+"]");
                this.emitSocketRequest("applyjoin" , JSON.stringify(param));

            }
            //this.loadding();
        }
    },

    onDeleteClick:function(){
        this.teaid.splice(this.teaid.length-1 , this.teaid.length) ;
        this.disTeaHouseId();
    },
    onCleanClick:function(){
        this.teaid.splice(0 , this.teaid.length) ;
        this.disTeaHouseId();
    },
    disTeaHouseId:function(){
        var children = this.numdata.children ;
        children[0].getComponent(cc.Label).string ="";
        for(var inx = 0 ; inx < 7 ; inx ++){
            if(inx < this.teaid.length){
                children[0].getComponent(cc.Label).string += this.teaid[inx] ;
           //}else{
           //     children[0].getComponent(cc.Label).string = "" ;
            }
        }
    }
    // update (dt) {},
});
