var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        floor_id_label: {
            default: null,
            type: cc.Label
        },
        floor_name_label: {
            default: null,
            type: cc.Label
        },
        room_type_btn: {
            default: null,
            type: cc.Node
        }
    },
    onLoad: function () {
        this.updateTeaHouseView();
        this.updateRoomTypeView();
    },
    updateTeaHouseView: function () {
        this.floor_id_label.string = "茶馆ID:" + cc.mile.teahouse.teanum;
        this.floor_name_label.string = cc.mile.teahouse.name;

    },
    updateRoomTypeView: function () {
        var GameConstants = require("GameConstants");
        var WangdaGameUtils = require("WangdaGameUtils");
        var roomType = WangdaGameUtils.getStorageToLocal(GameConstants.CREATE_ROOM_TYPE_KEY);
        if(!roomType){
            roomType = GameConstants.CREATE_ROOM_TYPE_CARD;
        }
        cc.mile.createroomtype = roomType;
        if(GameConstants.CREATE_ROOM_TYPE_CARD == roomType){
            this.room_type_btn.getComponent(cc.Sprite).spriteFrame =
                new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/jingdianwanfa.png"));
        }else {
            this.room_type_btn.getComponent(cc.Sprite).spriteFrame =
                new cc.SpriteFrame(cc.url.raw("resources/images/floor/Hall/jifenwanfa.png"));
        }
    },
    //退出
    back: function () {
        if (cc.mile.socket && cc.mile.socket.off){
            cc.mile.socket.off("command_teahouse");
        }
        cc.mile.teahouse = null;
        this.scene("mazihall", this);

    },
});
