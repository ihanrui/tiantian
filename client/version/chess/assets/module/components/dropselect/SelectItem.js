var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        itemname: {
            default: null,
            type: cc.Label
        },
        selectedbox: {
            default: null,
            type: cc.Node
        },
        unselectedbox: {
            default: null,
            type: cc.Node
        },

    },
    onLoad: function () {


    },
    initEvent:function () {
        this.node.on('itemCheck', function (event) {
            cc.log('-----selectedItemCheck-----click--!');
            if (this.selectedbox != null) {
                if (this.checked == false) {
                    if (this.data.type == "radio") {
                        for (var inx = 0; inx < this.options.length; inx++) {
                            var script = this.options[inx];
                            script.doUnChecked();
                        }
                    }
                    this.doChecked();
                } else {
                    if (this.data.type == "radio") {
                        for (var inx = 0; inx < this.options.length; inx++) {

                            var script = this.options[inx];
                            script.doUnChecked();
                        }
                        this.doChecked();
                    } else {
                        this.doUnChecked();
                    }
                }

                var event = new cc.Event.EventCustom("selectedItemCheck", true);
                event.setUserData({item: this.item});
                this.node.dispatchEvent(event);
            }
            event.stopPropagation();
        }, this);
    },
    init: function (item, group, parentoptions) {
        this.initEvent();

        this.data = group;
        this.item = item;
        this.options = parentoptions;

        this.selectedbox.active = true;
        this.itemname.string = item.name;
        if (item.defaultvalue == true) {
            this.selectedbox.active = true;
            this.checked = true;
        } else {
            this.checked = false;
            this.selectedbox.active = false;

        }
    },
    getItemCode: function () {
        return this.item.code;
    },
    onItemClick: function () {
        cc.log('-----onSelectedTitleClick-----click--!');
        var event = new cc.Event.EventCustom("itemCheck", true);
        this.node.dispatchEvent(event);
    },
    doChecked: function () {
        this.checked = true;
        this.selectedbox.active = true;
    },
    doUnChecked: function () {
        this.checked = false;
        this.selectedbox.active = false;
    }
});
