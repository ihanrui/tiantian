var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        itemPrefab: {
            default: null,
            type: cc.Prefab
        },
        itemBgNode: {
            default: null,
            type: cc.Node
        },
        itemsContainerNode: {
            default: null,
            type: cc.Node
        },
        selectedItemLabel: {
            default: null,
            type: cc.Label
        },
        selectedTitleViewNode: {
            default: null,
            type: cc.Node
        },
        selectedTitleContainer: {
            default: null,
            type: cc.Node
        },
        grouptitle: {
            default: null,
            type: cc.Label
        },
    },
    // use this for initialization
    onLoad: function () {

    },
    initEvent: function () {
        var self = this;
        this.node.on('selectedItemCheck', function (event) {
            cc.log('-----selectedItemCheck-----click--!');
            if (self) {
                var userData = event.getUserData();
                self.itemCheck(userData.item);
            }
            event.stopPropagation();
        });
        this.node.on('selectedTitleCheck', function (event) {
            cc.log('-----selectedTitleCheck-----click--!');
            if (self) {
                self.titleCheck();
            }
            event.stopPropagation();
        });
    },
    init: function (group, items, parentoptions) {
        this.data = group;
        this.options = parentoptions;
        this.groupoptions = new Array();
        this.checked = false;
        this.grouptitle.string = group.name + ":";
        this.grouptitle.fontFamily="黑体";
        this.initEvent();
        this.items = items;
        this.group = group;

        if (this.itemBgNode != null && this.groupoptions.length < 1) {
            var itemsnum = 0;
            var isBigItem = false;
            for (var inx = 0; inx < this.items.length; inx++) {
                if (this.items[inx].groupid == this.group.id) {
                    if(this.items[inx].name.length > 14){
                        isBigItem = true;
                    }
                }
            }
            if(isBigItem){
                this.selectedTitleViewNode.width = 330;
            }
            var hasDefault = false;
            for (var inx = 0; inx < this.items.length; inx++) {
                if (this.items[inx].groupid == this.group.id) {
                    itemsnum = itemsnum + 1;

                    cc.log(this.items[inx].groupid + "-11--" + this.items[inx].name);

                    var newitem = cc.instantiate(this.itemPrefab);
                    newitem.parent = this.itemBgNode;

                    newitem.setPosition(-70, (itemsnum) * 80 - 60);

                    var script = newitem.getComponent("SelectItem");
                    this.groupoptions.push(script);
                    script.init(this.items[inx], this.group, this.groupoptions);
                    if (this.items[inx].defaultvalue == true) {
                        hasDefault = true;
                        this.selectedItemLabel.string = this.items[inx].name;
                    }
                }
            }
            this.itemBgNode.height = 60 * itemsnum;
            if(!hasDefault){
                this.selectedItemLabel.string='       ';
            }
        }
    },
    getGroupData: function () {
        return this.data;
    },
    onSelectedTitleClick: function () {
        cc.log('-----onSelectedTitleClick-----click--!');
        var event = new cc.Event.EventCustom("selectedTitleCheck", true);
        this.node.dispatchEvent(event);
    },
    titleCheck: function () {
        for (var i = 0; i < this.list.length; i++) {
            var node = this.list[i].getitemsContainerNode();
            if (node && node != this.itemsContainerNode) {
                node.active = false;
            }
        }
        this.itemsContainerNode.active = !this.itemsContainerNode.active;
    },
    getitemsContainerNode:function () {
        return this.itemsContainerNode;
    },
    onSelectedItemClick: function () {
        cc.log('-----selectedItemCheck-----click--!');
        var event = new cc.Event.EventCustom("selectedTitleCheck", true);
        this.node.dispatchEvent(event);
    },
    setList:function (lst) {
        this.list = lst;
    },
    itemCheck: function (item) {
        this.selectedItemLabel.string = item.name;
        this.itemsContainerNode.active =false;
    }
});
