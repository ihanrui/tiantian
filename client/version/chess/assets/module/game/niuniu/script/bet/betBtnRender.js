var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
    extends: cc.Component,

    properties: {
        //得分
        betNode: {
            default: null,
            type: cc.Node
        },
    },
    onLoad: function () {

    },
    //初始化玩家信息
    initbet: function (score, pushflag) {
        this.pushflag = pushflag;
        this.score = score;
        WangdaGameUtils.showScoreNode(score, this.betNode);
    },
    getPushflag: function () {
        if (this.pushflag) {
            return 'Y';
        }
        return "";
    },
    getScore: function () {

        return this.score;
    }


});
