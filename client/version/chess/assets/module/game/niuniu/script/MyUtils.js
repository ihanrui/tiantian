//var ResManager = require('ResManager');
(function (global, factory) {

    /* AMD */
    if (typeof define === 'function' && define["amd"])
        define(["MyUtils"], factory);
    /* CommonJS */ else if (typeof require === "function" && typeof module === "object" && module && module["exports"])
        module["exports"] = factory(require("MyUtils"), true);
    /* Global */ else
        (global["dcodeIO"] = global["dcodeIO"] || {})["MyUtils"] = factory(global["dcodeIO"]["MyUtils"]);

})(this, function (MyUtils, isCommonJS) {
    "use strict";
    //MyUtils 
    var MyUtils = {};
    MyUtils.eCardType = {
        eCard_None: 0,
        eCard_Diamond: 0, // fangkuai
        eCard_Club: 1, // cao hua
        eCard_Heart: 2, // hong tao
        eCard_Sword: 3, // hei tao 
        eCard_NoJoker: 4,
        eCard_Joker: 4, // xiao wang
        eCard_BigJoker: 5, // da wang
        eCard_Max: 6,
    }
    MyUtils.GetType = function (cardNum) {
        if (cardNum == 53) {
            return MyUtils.eCardType.eCard_Joker;
        }
        else if (cardNum == 54) {
            return MyUtils.eCardType.eCard_BigJoker;
        }
        else {
            return parseInt(cardNum % 4);
        }
    }
    MyUtils.GetCardFaceNum = function (cardNum) {
        if (cardNum == 53) {
            return 53;
        }
        else if (cardNum == 54) {
            return 54;
        }
        else {
            var Type = parseInt(cardNum / 4);
            return Type;
        }
    }
    return MyUtils;
});