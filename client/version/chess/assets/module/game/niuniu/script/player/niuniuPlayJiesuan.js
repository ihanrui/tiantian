/**
 *
 * niuniu 单个用户结算
 *
 */
var beiMiCommon = require("MileCommon");
var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
    extends: beiMiCommon,

    properties: {
        userHeadNode: {
            default: null,
            type: cc.Sprite
        },
        userNameNode: {
            default: null,
            type: cc.Label
        },
        gameIdLabel: {
            default: null,
            type: cc.Label
        },
        qiangLabel: {
            default: null,
            type: cc.Label
        },
        zhuangLabel: {
            default: null,
            type: cc.Label
        },
        pushLabel: {
            default: null,
            type: cc.Label
        },

        winTagNode: {
            default: null,
            type: cc.Node
        },
        loserTagNode: {
            default: null,
            type: cc.Node
        },

        roomOwnerNode: {
            default: null,
            type: cc.Node
        },
        ticketScoreLabel: {
            default: null,
            type: cc.Label
        },
        ticketScoreNode: {
            default: null,
            type: cc.Node
        },
        scoreNode: {
            default: null,
            type: cc.Node
        },
        viewContainerNode: {
            default: null,
            type: cc.Node
        },


    },

    onLoad: function () {
        // this.addTouchEvent();

    },
    initData: function (userdata,roomtype) {
        if(userdata.gameid){

            this.userNameNode.string = userdata.username;
            if(userdata.gameid){
                this.gameIdLabel.string = userdata.gameid;
            }
            this.loadUrlJPGImage(userdata.userurl
                + "&username=" + userdata.userid,this.userHeadNode );
            this.userid = userdata.userid;

            if(roomtype == 'card'){
                WangdaGameUtils.showNumberNode(userdata.totalscore,this.scoreNode);
            }else {
                // 如果是金币场  coins
                WangdaGameUtils.showNumberNode(userdata.coins,this.scoreNode);
            }

            if(userdata.ticket && userdata.ticket > 0){
                this.ticketScoreNode.active = true;
                this.ticketScoreLabel.string = '打赏老板：'+userdata.ticket;
            }else {
                this.ticketScoreNode.active = false;
            }

            var winFlag = userdata.winflag == 1 ? true: false;
            this.winTagNode.active  = winFlag;

            var richflag = userdata.richflag == 1 ? true: false;
            this.loserTagNode.active  = richflag;

            this.qiangLabel.string = userdata.lvlsum ;
            this.zhuangLabel.string = userdata.bankersum;
            this.pushLabel.string = userdata.winsum  ;
        }else {
            this.viewContainerNode.active = false;
        }
    }
});