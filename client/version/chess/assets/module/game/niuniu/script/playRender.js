var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
var NiuNiuCoinSettlement = require("NiuNiuCoinSettlement");

cc.Class({
    extends: beiMiCommon,

    properties: {
        //用户边框图片集
        palyframeBackAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //得分
        score: {
            default: null,
            type: cc.Node
        },
        //用户信息框
        UserBack: {
            default: null,
            type: cc.Node
        },
        //用户头像
        palyImge: {
            default: null,
            type: cc.Node
        },
        //用户头像
        palyImge1: {
            default: null,
            type: cc.Node
        },
        //用户头像遮盖层
        palyBgImge: {
            default: null,
            type: cc.Node
        },
        //操作（准备，加倍）
        operating: {
            default: null,
            type: cc.Node
        },
        //搓牌显示
        cuopai: {
            default: null,
            type: cc.Node
        },
        //用户名称
        playname: {
            default: null,
            type: cc.Label
        },
        //用户得分

        //庄家标记
        zhuang: {
            default: null,
            type: cc.Node
        },
        //下注分背景
        betScore_bg: {
            default: null,
            type: cc.Node
        },
        //显示牌区域
        crads: {
            default: null,
            type: cc.Node
        },
        //抢庄倍数倍数
        qiangMultipleNode: {
            default: null,
            type: cc.Node
        },
        //显示的牌
        crad: {
            default: null,
            type: cc.Prefab
        },
        //背景牌
        crad_Bg: {
            default: null,
            type: cc.Prefab
        },
        //纸牌集
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //负分数字集
        redNumberAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //正分数字集
        yellowNumberAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //数字预制资源
        numberScore: {
            default: null,
            type: cc.Node
        },
        //数字预制资源
        numberPrefab2: {
            default: null,
            type: cc.Prefab
        },


        playreadyNode: {
            default: null,
            type: cc.Node
        },
        playofflineNode: {
            default: null,
            type: cc.Node
        },
        playQiangZhuangNode: {
            default: null,
            type: cc.Node
        },
        // 推注标签
        pushContainerNode: {
            default: null,
            type: cc.Node
        },
        autoNode: {
            default: null,
            type: cc.Node
        },
        finishNode: {
            default: null,
            type: cc.Node
        },
        pushDescLabel: {
            default: null,
            type: cc.Label
        },
        cardTypeImgNode: {
            default: null,
            type: cc.Node
        },
        stepScoreParentNode: {
            default: null,
            type: cc.Node
        },
        // 总得分
        totalScoreLabel: {
            default: null,
            type: cc.Label
        },

        message: {
            default: null,
            type: cc.Node
        },
        messageLab: {
            default: null,
            type: cc.Label
        },

    },
    onLoad: function () {
        this.player = {};
    },
    //初始化玩家信息
    initplayer: function (data) {
        cc.log('----玩家相对自己的initplayer----');
        if (data.playuser) {
            data.id = data.playuser;
        }

        this.relationIndex = data.relationIndex;

        this.positionDirection = data.positionDirection;
        this.updateOfflineViewStatus(data.online);
        this.updateReadyViewStatus(data.gamestatus);
        if (data.hasOwnProperty("totalscore")) {
            this.totalScoreLabel.string = data.totalscore;
        } else {
            this.totalScoreLabel.string = "0";
        }

        if (this.player.id != data.id) {
            this.player = data;
            //玩家名称
            this.playname.string = data.username;
            //玩家初始分数
            this.username = data.username;
            this.userip = data.ip;
            this.gameid = data.gameid;


            this.loadUrlJPGImage(data.userurl
                + "&username=" + data.id, this.palyImge);
        }

        var slef = this;
        //更具index 调整用户信息展示位置
        switch (data.relationIndex) {
            case 0:
                //slef.UserBack.setPosition(40, 70);
                slef.qiangMultipleNode.setPosition(-140, 27);
                slef.betScore_bg.setPosition(-140, -20);
                break;
        }
        ;
    },

    getPlayerId: function () {
        return this.player.id;
    },
    getRelationIndex: function () {
        return this.player.relationIndex;
    },
    setPlayerRelationPosition: function (x, y) {
        this.rx = x;
        this.ry = y;
    },
    getPlayerRelationPositionX: function () {
        return this.rx;
    },
    getPlayerRelationPositionY: function () {
        return this.ry;
    },

    updatePlayerTotalScore: function (score) {
        this.totalScoreLabel.string = score;
    },
    updateOfflineViewStatus: function (status) {
        this.playofflineNode.active = !status;
    },
    updateReadyViewStatus: function (status) {
        if (status == 'ready') {
            this.playreadyNode.active = true;
            this.palyBgImge.active = true;
        } else {
            this.playreadyNode.active = false;
            this.palyBgImge.active = false;
        }
    },
    //显示开牌结果，牛几，显示倍数
    showCardTypeView: function (cardType) {
        debugger
        this.cardTypeImgNode.getComponent(cc.Sprite).spriteFrame =
            new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/pokertype/imgNN" + cardType.cardtype + ".png"));
        this.cardTypeImgNode.active = true;
    },
    updateUserStatus: function (status) {
        cc.log("更新用户状态：" + status);
    },
    //显示搓牌中
    ShowCuopai: function () {
        this.cuopai.active = true;
    },
    //显示分数
    ShowScore: function (score) {
        //显示分数
        this.score.active = true;
        //获取当前总分
    },
    //显示庄家标记
    updateZhuangViewStatus: function (status) {
        this.zhuang.active = status;
    },
    //抢倍数
    updateQiangMultiple: function (num) {
        cc.log("抢庄图标：qiangx" + num + ".png");
        if (num > 0) {

            this.updateQiangzhuangMultipleViewStatus(true);

            this.qiangMultipleNode.getComponent(cc.Sprite).spriteFrame
                = new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/playCards/qiangx" + num + ".png"));
        }
    },
    //抢倍数图标显示
    updateQiangzhuangMultipleViewStatus: function (status) {
        cc.log("抢庄图标状态--：" + status);
        this.qiangMultipleNode.active = status;
    },
    //定庄后倍数显示
    updateDingMultiple: function (num) {
        cc.log("定庄后倍数图标：host" + num + ".png");
        if (num > 0) {
            this.updateQiangzhuangMultipleViewStatus(true);
            this.qiangMultipleNode.getComponent(cc.Sprite).spriteFrame
                = new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/playCards/host" + num + ".png"));
        }
    },
    //倍数
    updateCoinMultiple: function (index, num) {
        this.BetAnimation(index, num);
        this.updatePushCountViewStatus(true);
    },//倍数
    updatePushCountViewStatus: function (status) {
        this.betScore_bg.active = status;
    },
    updateFinishedViewStatus: function (status) {
        if (status) {
            this.updateReadyViewStatus(false);
        }
        this.finishNode.active = status;
        this.palyBgImge.active = status;
    },
    updateQiangzhuangCircleViewStatus: function (status) {
        this.playQiangZhuangNode.active = status;
    },
    playQiangzhuangAnimation: function () {
        var animation = this.playQiangZhuangNode.getComponent(cc.Animation);
        if (animation) {
            animation.play('selfDingZhuang');
        }
    },
    updatePushDescLabel: function (pushscore) {
        this.updatePushContainerNodeViewStatus(true);
        this.pushDescLabel.string = pushscore;
    },
    // 推注标签
    updatePushContainerNodeViewStatus: function (status) {
        this.pushContainerNode.active = status;
    },
    //开牌显示牌
    OpenCrad: function (userid, cardType) {
        var cards = this.decode(cardType.cards);
        var scorecard = [];
        if (cardType.scorecard) {
            scorecard = this.decode(cardType.scorecard);
        }
        var lastcard = cardType.lastcard;
        //this.cards = this.random(5, 0, 103);
        this.clearCards();
        cc.log("显示玩家的牌：" + cards);
        cc.log("显示玩家的scorecard牌：" + scorecard);
        var groupCards = [];
        for (var i = 0; i < cards.length; i++) {
            var hasScorecard = false;
            for (var j = 0; j < scorecard.length; j++) {
                if (scorecard[j] == cards[i]) {
                    hasScorecard = true;
                }
            }
            if (!hasScorecard) {
                groupCards.push(cards[i]);
            }
        }
        for (var i = 0; i < groupCards.length; i++) {
            //将一个一个牌显示在对象池当中
            var xPosition = parseFloat(-43) + parseFloat(25 * i);
            cc.log("牌的位置：" + xPosition);
            this.loadSingleCard(userid, groupCards[i], xPosition, 0, lastcard == groupCards[i]);
        }
        var yPosition = 15;
        for (var i = 0; i < scorecard.length; i++) {
            //将一个一个牌显示在对象池当中
            var xPosition = parseFloat(-43) + parseFloat(25 * (i + groupCards.length));
            cc.log("牌的位置：" + xPosition);
            this.loadSingleCard(userid, scorecard[i], xPosition, yPosition, lastcard == scorecard[i]);
        }
    },
    clearCards: function () {
        if (this.crads) {
            this.crads.removeAllChildren();
        }
    },
    sendSettlementCoin: function (startX, startY, score, fromId) {
        this.clearScoreNode();
        var settlement = new NiuNiuCoinSettlement();
        if (score > 0) {
            settlement.startSend({
                targetX: this.getPlayerRelationPositionX(),
                targetY: this.getPlayerRelationPositionY(),
                startX: startX,
                startY: startY,
                coinCount: 10,
                score: score,
                fromId: fromId,
                targetId: this.getPlayerId(),
                scoreParentNode: this.stepScoreParentNode
            });
        } else {
            settlement.startSend({
                targetX: startX,
                targetY: startY,
                startX: this.getPlayerRelationPositionX(),
                startY: this.getPlayerRelationPositionY(),
                coinCount: 10,
                score: score,
                fromId: fromId,
                targetId: this.getPlayerId(),
                scoreParentNode: this.stepScoreParentNode
            });
        }
    },
    showSettlementCoin: function (score) {
        this.clearScoreNode();
        var settlement = new NiuNiuCoinSettlement();
        settlement.showCoinAnimation(this.stepScoreParentNode, score);
    },
    clearScoreNode: function () {
        this.stepScoreParentNode.removeAllChildren();
    },
    loadSingleCard: function (userid, cardOriginValue, xPosition, yPosition, isLastCard) {
        debugger
        var poker = cc.instantiate(this.crad);
        var cardValue = parseInt(cardOriginValue / 4) * 8 + cardOriginValue % 4;
        poker.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(cardValue);
        poker._name = "pai" + cardOriginValue;
        var pokerRender = poker.getComponent("singleCard");
        if (pokerRender) {
            pokerRender.updateLastCardViewStatus(isLastCard);
        }
        if (userid != cc.mile.user.id) {
            poker.setScale(0.5, 0.5);
        }
        ;
        poker.setPosition(xPosition, yPosition);
        poker.parent = this.crads;
    },
    //发牌
    HairCrad: function (cardindex) {
        debugger
        //将一个一个牌显示在对象池当中
        var width = parseFloat(-43) + parseFloat(20 * cardindex);
        var crad = cc.instantiate(this.crad_Bg);
        crad._name = "crad_Bg" + cardindex;
        crad.setPosition(-this.getPlayerRelationPositionX(), -this.getPlayerRelationPositionY());
        crad.parent = this.crads;
        var moduiAction = this.getPlayCardsDisplayAction(width, 0, 0.8);
        this.crads.children[cardindex].runAction(moduiAction);
    },
    //获取随机数值
    random: function (len, start, end) {
        var arr = [];

        function _inner(start, end) {
            var span = end - start;
            return parseInt(Math.random() * span + start)
        }

        while (arr.length < len) {
            var num = _inner(start, end);
            if (arr.indexOf(num) == -1) {
                arr.push(num);
            }
        }
        return arr;
    },
    //移动（moveTo固定的位置，moveBy位置不固定）
    getPlayCardsDisplayAction: function (x, y, timeDuration) {
        var mv = cc.moveTo(timeDuration, x, y).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(timeDuration, 0.6, 0.6);
        return cc.spawn(mv, scale);
    },

    clearPlayerStatus: function () {
        if (this.cardTypeImgNode) {
            this.cardTypeImgNode.active = false;
        }
        if (this.crads) {
            //this.crads.removeAllChildren();
        }
        this.clearScoreNode();
        this.updateReadyViewStatus('');
        this.updateZhuangViewStatus(false);
        this.updateQiangzhuangCircleViewStatus(false);
        this.updateQiangzhuangMultipleViewStatus(false);
        this.updatePushContainerNodeViewStatus(false);
    },
    //下注动画
    BetAnimation: function (index, score) {
        debugger
        var mv = cc.moveTo(0.5, -25, 0).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(0.5, 1, 1);
        this.betScore_bg.children[0].runAction(cc.spawn(mv, scale));
        var slf = this;
        this.numberScore.removeAllChildren();
        this.scheduleOnce(function () {
            WangdaGameUtils.showScoreNode(score, this.numberScore);
        }, 0.4);
        cc.mile.audio.playSFX("niuniu/random_banker.mp3");
    },

    onOpenMessage: function (message) {
        this.message.active = true;
        this.messageLab.string = message;
        this.scheduleOnce(function () {
            this.message.active = false;
        }, 2);
    },

    playUserEmojAnimation: function playUserEmojAnimation(animationName, animationMp3) {
        debugger;
        var self = this;
        cc.log(this.palyImge1 + '----玩家相对自己的playUserEmojAnimation----' + animationName);
        if (this.palyImge1) {
            self.palyImge1.active = true;
            cc.loader.loadRes("animation/emoji/" + animationName.trim() + ".anim", function (err, clip) {
                if (err) {
                    cc.log(err);
                } else {
                    cc.mile.audio.playSFX("mazi/facevoice/" + animationName.trim() + ".mp3");
                    debugger;
                    cc.log('----玩家相对自己的playUserEmojAnimation----' + animationName);
                    self.palyImge1.getComponent(cc.Animation).addClip(clip, animationName);
                    self.palyImge1.getComponent(cc.Animation).play(animationName);
                    //self.palyImge1.active=false;
                }
            });
        }
    },
});
