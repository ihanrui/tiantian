var beiMiCommon = require("MileCommon");
var GameUtil = require("WangdaGameUtils");
cc.Class({
    extends: beiMiCommon,
    properties: {
        //搓牌区域
        pokerContainer: {
            default: null,
            type: cc.Node
        },
        //搓牌的牌
        cradMin: {
            default: null,
            type: cc.Prefab
        },
        //显示的牌
        crad: {
            default: null,
            type: cc.Prefab
        },
        //背景牌
        crad_Bg: {
            default: null,
            type: cc.Prefab
        },
        //显牌区域
        pokerSeeList: {
            default: null,
            type: cc.Node
        },
        //纸牌集
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //遮盖背景
        gray: {
            default: null,
            type: cc.Node
        },
        //搓牌
        cuoPai_but: {
            default: null,
            type: cc.Node
        },
        //亮牌
        brightCard_but: {
            default: null,
            type: cc.Node
        },
        //金币
        goldCoin: {
            default: null,
            type: cc.Prefab
        },
        //金币父节点
        goldCoinSeeList: {
            default: null,
            type: cc.Node
        },
        //最后一张牌标记
        lastCard: {
            default: null,
            type: cc.Prefab
        },
        cardTypeImgNode: {
            default: null,
            type: cc.Node
        },
        //牌集合
        cards: null,
        //暂存牌对象
        pokers: null,
    },
    //加载
    onLoad: function () {

    },
    //加载牌
    loadCards: function (cards, lastcard) {
        debugger
        cc.log("显示玩家的牌loadCards：" + cards);
        this.gray.active = false;
        this.cardTypeImgNode.active = false;
        this.pokerSeeList.removeAllChildren();
        this.pokerContainer.removeAllChildren();
        for (var i = 0; i < cards.length; i++) {
            //将一个一个牌显示在对象池当中
            var xPosition = parseFloat(-250) + parseFloat(125 * i);
            cc.log("牌的位置：" + xPosition);
            if (cards[i] != -1) {
                this.loadSingleCard(cards[i], xPosition, 0, false);
                if (cards[i] == lastcard) {
                    cc.log("最后一手牌：" + cards[i] + "---" + lastcard);
                    this.loadCardsBg(cards[i], xPosition, i);
                }
            } else {
                //this.loadCardsBg(cards[i], xPosition, i);
            }
        }
    },
    //加载牌
    showCards: function (cardType) {
        debugger
        var cards = this.decode(cardType.cards);
        var scorecard = [];
        if (cardType.scorecard) {
            scorecard = this.decode(cardType.scorecard);
        }
        var lastcard = cardType.lastcard;
        this.gray.active = false;
        this.pokerSeeList.removeAllChildren();
        this.pokerContainer.removeAllChildren();
        cc.log("显示玩家的牌：" + cards);
        cc.log("显示玩家的scorecard牌：" + scorecard);
        cc.log("显示玩家的lastcard牌：" + lastcard);
        var groupCards = [];

        for (var i = 0; i < cards.length; i++) {
            var hasScorecard = false;
            for (var j = 0; j < scorecard.length; j++) {
                if (scorecard[j] == cards[i]) {
                    hasScorecard = true;
                }
            }
            if (!hasScorecard) {
                groupCards.push(cards[i]);
            }
        }


        this.showCardsByList(groupCards, 0, 0, lastcard);
        var yPosition = 40;
        this.showCardsByList(scorecard, groupCards.length, yPosition, lastcard);

    },

    showCardsByList: function (cardLst, startIndex, yPosition, lastcard) {
        debugger
        for (var i = 0; i < cardLst.length; i++) {
            //将一个一个牌显示在对象池当中
            var xPosition = parseFloat(-250) + parseFloat(125 * (i + startIndex));
            cc.log("牌的位置：" + xPosition);
            this.loadSingleCard(cardLst[i], xPosition, yPosition, lastcard == cardLst[i]);
        }
    },
    //加载背景牌
    loadCardsBg: function (cardValue, xPosition, cardIndex) {
        debugger
        //将一个一个牌显示在对象池当中
        cc.log("加载背景：" + cardValue + "---" + xPosition);
        var poker = cc.instantiate(this.crad_Bg);
        poker._name = cardValue;
        poker.active = true;
        poker.setPosition(xPosition, 0);
        poker.parent = this.pokerSeeList;
        var moduiAction = this.getPlayCardsDisplayAction(xPosition, 0, 0.5);
        this.pokerSeeList.children[cardIndex].runAction(moduiAction);
    },
    //加载背景牌
    loadSingleCard: function (cardOriginValue, xPosition, yPosition, isLastCard) {
        //将一个一个牌显示在对象池当中
        cc.log("加载单个牌：" + cardOriginValue + "---" + xPosition);
        var poker = cc.instantiate(this.crad);
        var cardValue = parseInt(cardOriginValue / 4) * 8 + cardOriginValue % 4;
        var pokerRender = poker.getComponent("singleCard");
        if (pokerRender) {
            pokerRender.updateLastCardViewStatus(isLastCard);
        }
        poker.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(cardValue);

        poker._name = cardOriginValue;
        poker.active = true;
        poker.setPosition(xPosition, yPosition);
        poker.parent = this.pokerSeeList;

    },
    //加载金币
    loadGoldCoin: function (cardindex) {
        //将一个一个牌显示在对象池当中
        debugger;
        var widht = parseFloat(-33);
        var jinbi = cc.instantiate(this.goldCoin);
        jinbi._name = cardindex;
        jinbi.active = true;
        jinbi.setPosition(-520, -290);
        jinbi.parent = this.goldCoinSeeList;
        var moduiAction = this.getPlayCardsDisplayAction(widht, 280, 0.5);
        this.goldCoinSeeList.children[cardindex].runAction(moduiAction);
    },
    showCardTypeView: function (cardType) {
        this.cardTypeImgNode.getComponent(cc.Sprite).spriteFrame =
            new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/pokertype/imgNN" + cardType.cardtype + ".png"));
        this.cardTypeImgNode.active = true;

    },

    clearPlayerStatus: function () {
        if (this.cardTypeImgNode) {
            this.cardTypeImgNode.active = false;
        }
        this.pokerSeeList.removeAllChildren();
        this.pokerContainer.removeAllChildren();
    },

    //搓牌加载牌
    CuoPailoadCards: function () {
        debugger

        //this.minpokerpool = new cc.NodePool();
        //for (var i = 0; i < this.cards.length; i++) {
        //    var poker = cc.instantiate(this.cradMin);
        //    this.minpokerpool.put(poker);
        //}
        //var positionX = -20;
        //var positionY = 0;
        //var Rate = 348;
        //for (var i = 0; i < this.cards.length; i++) {
        //    var poker = this.minpokerpool.get();
        //    poker.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(this.cards[i]);
        //    poker._name = this.cards[i];
        // //   poker.active = true;
        //    var X = positionX + (i * 4);
        //    var Y = positionY - (i * 2);
        //    poker.rotation = Rate + (i * 2) >= 360 ? Rate + (i * 2) - 360 : Rate + (i * 2);
        //    poker.setPosition(X, Y);
        //    poker.parent = this.pokerContainer;
        //}
        //this.pokers = this.pokerContainer.children;
    },
    //加载牌的移动事件
    initload: function () {
        for (var i = 0; i < this.pokers.length; i++) {
            this.pokers[i].on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                var x = event.getDeltaX();       //获取鼠标距离上一次事件移动的 X轴距离。
                var y = event.getDeltaY();      //获取鼠标距离上一次事件移动的 Y轴距离。
                this.x += x;
                this.y += y;
            })
        }
    },
    //搓牌
    onCuoPai_Click: function () {
        debugger
        var niuniuBegin = this.getCommon("niuniuBegin");
        cc.log("搓牌的时候的牌：" + niuniuBegin.dealingData.lastcard);
        var cuopai = this.getCommon("CuoPai");
        if (niuniuBegin.dealingData) {
            cuopai.init(niuniuBegin.dealingData.lastcard);
        }else {
            cuopai.init(0);
        }
    },

    //搓牌后理牌
    Lipa: function () {
        var positionX = -25;
        var Rate = 320;
        for (var i = 0; i < this.cards.length; i++) {
            var poker = this.pokerContainer.children[i];
            var X = positionX + (i * 36);
            var Y = 0
            switch (i) {
                case 0:
                    Y = -185;
                    break;
                case 1:
                    Y = -175;
                    break;
                case 2:
                    Y = -175;
                    break;
                case 3:
                    Y = -185;
                    break;
                case 4:
                    Y = -205;
                    break;
            }
            poker.rotation = Rate + (i * 18) >= 360 ? Rate + (i * 18) - 360 : Rate + (i * 18);
            poker.setPosition(X, Y);
        }
        var slf = this;
        this.scheduleOnce(function () {
            // slf.gray.active=false;
            // slf.BrightCard_Click();
        }, 2);
    },
    //清除牌信息
    clearSefAllPokers: function () {

        this.pokerSeeList.removeAllChildren();
        this.pokerContainer.removeAllChildren();
    },
    //移动（moveTo固定的位置，moveBy位置不固定）
    getPlayCardsDisplayAction: function (x, y, timeDuration) {
        var mv = cc.moveTo(timeDuration, x, y).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(timeDuration, 0.8, 0.8);
        return cc.spawn(mv, scale);
    },

    /**
     * 翻牌的语音
     * @param cardType
     * @param cardValue
     */
    trunCardTypeVoice: function () {
        debugger;
        var voiceName = GameUtil.getNiuniuCardTypeVoiceName(1);
        if (voiceName) {
            //this.stopAllUserVoiceAnimationStatus(false);
            cc.mile.audio.playSFX("niuniu/boy/" + voiceName);
        }
    },

});
