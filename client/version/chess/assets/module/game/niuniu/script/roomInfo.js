var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        playing: {//玩法
            default: null,
            type: cc.Label
        },
        roomId: {//房间号
            default: null,
            type: cc.Label
        },
        numberOfPeople: {//人数
            default: null,
            type: cc.Label
        },
        payment: {//支付方式
            default: null,
            type: cc.Label
        },
        bottomScore: {//底分
            default: null,
            type: cc.Label
        },
        numberOfGame: {//局数
            default:null,
            type: cc.Label
        },
        fanbeiRule:{//翻倍规则
            default:null,
            type:cc.Label
        },
        tsPlayingRule:{//特殊玩法
            default:null,
            type:cc.Label
        },
        roomRule:{//房间规则
            default:null,
            type:cc.Label
        }
    },

    initRoomInfo : function(data){
        this.numberOfPeople.string = cc.mile.extparams.playernumber;
        this.numberOfGame.string = data.gamenumbers;
        this.roomId.string = data.roomid;
        this.playing.string = data.playwaydesc;
        this.bottomScore.string = data.basescore;
        this.pay = "";
        if(cc.mile.extparams.tips == "creater"){
            this.pay = "房主支付";
        }else{
            this.pay = "AA支付";
        }
        this.payment.string = this.pay ;
        this.pinshirule = "";
        if(data.pinshirule =="ruleto7"){
            this.pinshirule= "牛牛x4 牛九x3 牛八x2 牛七x2";
        }else{
            this.pinshirule= "牛牛x4 牛九x3 牛八x2 ";
        }
        this.fanbeiRule.string = this.pinshirule;
        this.group = new Array();
        this.group = data.specialcard.split(",");
        var str = "";
        for(var i=0;i<this.group.length;i++){
            if(this.group[i] == "fivesmall"){
              str += "五小牛(8倍),";
            }else if(this.group[i] == "bomb"){
                str += "炸弹牛(6倍),";
            }else if(this.group[i] == "fivecolor"){
                str += "五花牛(5倍),";
            }else if(this.group[i] == "hulu"){
                str += "葫芦牛(6倍),";
            }else if(this.group[i] == "allcolor"){
                str += "同花牛(5倍),";
            }else if(this.group[i] == "straight"){
                str += "顺子牛(5倍)";
            }
        }
        this.tsPlayingRule.string = str;




    },

    RefreshRoom:function (data) {
        this.numberOfGame.string = data.gamenumbers;
    }


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},


    // update (dt) {},
});
