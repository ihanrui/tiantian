var beiMiCommon = require("MileCommon");
cc.Class({
  extends: beiMiCommon,
  properties: {
    playReviewPrefab: {
      default: null,
      type: cc.Prefab
    },
    playerContainer: {
      default: null,
      type: cc.Node
    },

    closeBtn: {
      default: null,
      type: cc.Button
    }
  },
  onLoad: function () {
    this.playerReviewLst = [];
    //this.initUserView([{}, {}, {}, {}, {}, {}, {}, {}, {}]);
  },
  //读取房间的历史数据
  queryPlayBack: function (roomid, curnum) {
    var param = {
      token: cc.mile.authorization,
      roomid: roomid,
      currentnum: curnum + "",
    };
    cc.log("初始化房间信息：" + JSON.stringify(param));
    cc.mile.http.httpPost("/record/queryPlayback", param,
        this.successGetPlayer, this.errorGetPlayer, this);
  },
  successGetPlayer: function (result, context) {
    cc.log("查询上局" + JSON.stringify(result));
    var data = context.parse(result);
    context.initUserView(data);

  },
  errorGetPlayer: function (context) {
    cc.log(context);

  },
  initUserView: function (data) {

    var userlist =  data.hisplayers;
    var pinshihiscards =  data.pinshihiscards;

    for (var i = 0; i < userlist.length; i++) {
      var playJiesan = cc.instantiate(this.playReviewPrefab);
      playJiesan.setPosition(-110, -70 - 120 * i);
      playJiesan.parent = this.playerContainer;
      var render = playJiesan.getComponent("PlayReview");
      for(var j = 0 ; j < pinshihiscards.length ; j++){
        if(pinshihiscards[j].userid == userlist[i].userid){
          render.initData(userlist[i],pinshihiscards[j], data.gameRoom);
        }
      }

      //this.playerReviewLst.push(playJiesan);
    }

  },

  oncloseClick: function () {
    cc.log("onReviewViewClose click");
    this.node.dispatchEvent(
        new cc.Event.EventCustom("onReviewViewClose", true));

  },

  // update (dt) {},
});
