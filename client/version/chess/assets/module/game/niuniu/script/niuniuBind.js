var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        //准备按钮
        Ready_but: {
            default: null,
            type: cc.Node
        },
        //搓牌按钮
        Cuopai_but: {
            default: null,
            type: cc.Node
        },
        //翻牌按钮
        brightCard_but: {
            default: null,
            type: cc.Node
        },
        //翻牌按钮
        brightCard_but: {
            default: null,
            type: cc.Node
        },
        //show牌按钮
        showCard_btn: {
            default: null,
            type: cc.Node
        },
        //打开菜单按钮
        openMenuBtn: {
            default: null,
            type: cc.Node
        },
        //菜单栏
        Menu: {
            default: null,
            type: cc.Node
        },
        //下注按钮集合
        bet_but_List: {
            default: null,
            type: cc.Node
        },
        //下注按钮预置资源
        bet_but_Prefab: {
            default: null,
            type: cc.Prefab
        },
        //复制，邀请
        inviteplayerContainer: {
            default: null,
            type: cc.Node
        },
        //房间信息
        roomInfo_pfb: {
            default: null,
            type: cc.Prefab
        },
        //托管
        trusteeShip_Prefab: {
            default: null,
            type: cc.Prefab
        },
        //游戏设置
        gameSet_Prefab: {
            default: null,
            type: cc.Prefab
        },
        //回顾上一局
        review_Prefab: {
            default: null,
            type: cc.Prefab
        },
        //解散
        jiesan_Prefab: {
            default: null,
            type: cc.Prefab
        },
        quickLangage: {
            default: null,
            type: cc.Node
        },
        //快捷语合集
        quickLangageContext: {
            default: null,
            type: cc.Node
        },
        //快捷语按钮
        btShortCut0: {
            default: null,
            type: cc.Node
        },
        //快捷语按钮
        btShortCut1: {
            default: null,
            type: cc.Node
        },
        //表情按钮
        btExpression0: {
            default: null,
            type: cc.Node
        },
        //表情按钮
        btExpression1: {
            default: null,
            type: cc.Node
        },
        //表情合集
        face: {
            default: null,
            type: cc.Node
        },
        //语音背景
        discourse: {
            default: null,
            type: cc.Node
        },
        //语音提示语句
        discourseLabel: {
            default: null,
            type: cc.Label
        },
        //用户信息
        userinfoDialog: {
            default: null,
            type: cc.Prefab
        },
        //结算信息
        niuniuJiesuanDialog: {
            default: null,
            type: cc.Prefab
        },
        //发送语音预制资源
        voiceOnlinePrefab: {
            default: null,
            type: cc.Prefab
        },
        //发送语音父节点
        voiceOnlineParentViewNode: {
            default: null,
            type: cc.Node
        },

        rightViewContainerNode: {
            default: null,
            type: cc.Node
        },
        roomIdLabel: {
            default: null,
            type: cc.Label
        },
        gameScoreLabel: {
            default: null,
            type: cc.Label
        },
        gameNumberLabel: {
            default: null,
            type: cc.Label
        },
        gameTimeLabel: {
            default: null,
            type: cc.Label
        },
        gameDescLabel: {
            default: null,
            type: cc.Label
        },
        btChat0: {//桌面快捷语按钮
            default: null,
            type: cc.Node
        },

        index: 0,
    },
    onLoad: function () {
        var self = this;
        this.playerRenderLst = new Array();
        this.voiceOnLineView = cc.instantiate(this.voiceOnlinePrefab);
        this.voiceOnLineView.parent = this.voiceOnlineParentViewNode;
    },
    onReviewViewClose: function () {
        this.rightViewContainerNode.removeAllChildren();
    },
    //准备
    Ready_Click: function () {

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        };
        this.emitSocketRequest("start", JSON.stringify(param));

    },
    updateRoomStatus: function (status) {
        if (status == 'ready') {
            this.inviteplayerContainer.active = false;
            this.Ready_but.active = false;
        }
    },
    //打开或关闭菜单
    onOpenOrCloseMenu: function (pos, data) {
        if (data == 'open') {
            this.Menu.runAction(cc.moveTo(0.2, 415, 0));
            this.openMenuBtn.rotation = 90;
        } else {
            this.Menu.runAction(cc.moveTo(0.1, 868, 0));
            this.openMenuBtn.rotation = -90;
        }
    },
    //房间信息
    onOpenRoomInfo: function () {
        cc.mile.openwin = cc.instantiate(this.roomInfo_pfb);
        cc.mile.openwin.parent = this.root();
    },
    //加载倍数按钮
    loadBetbut: function (multiple) {

        this.bet_but_List.removeAllChildren();
        this.bet_but_List.active = true;

        var bet_but = cc.instantiate(this.bet_but_Prefab);
        //不抢
        var widht = -(multiple + 1) / 2 * 130 + 62;
        bet_but.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(
            cc.url.raw("resources/images/room/niuniu/playCards/btn_buqiang.png"))
        bet_but.name = "btn_buqiang";
        bet_but.setPosition(widht, 0);
        bet_but.parent = this.bet_but_List;
        bet_but.on(cc.Node.EventType.TOUCH_START, function (e) {
            this.bet_but_List.removeAllChildren();

            debugger
            this.BetbutClick(0);

        }, this, true);

        for (var i = 1; i <= multiple; i++) {
            widht = widht + 130;
            this.initBetbut(widht, i);
        }
    },
    //初始倍数
    initBetbut: function (widht, inx) {
        var playwayComponent = cc.instantiate(this.bet_but_Prefab);
        if (playwayComponent != null) {
            playwayComponent.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(
                cc.url.raw("resources/images/room/niuniu/playCards/btn_" + inx
                    + "bei.png"))
            //将生成的节点放在节点树上
            playwayComponent.name = "btn_" + inx + "bei";
            playwayComponent.setPosition(widht, 0);
            playwayComponent.parent = this.bet_but_List;
            playwayComponent.on(cc.Node.EventType.TOUCH_START, function (e) {
                this.BetbutClick(inx);
            }, this, true);
        }
    },
    // 抢庄
    BetbutClick: function (inx) {
        this.bet_but_List.removeAllChildren();
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            hogtimes: inx
        };
        this.emitSocketRequest("hog", JSON.stringify(param));
        //var niuniuBegin = this.getCommon("niuniuBegin");
        //niuniuBegin.ShowMultiple(inx);
        //加载分数按钮
        //this.loadScorebut("10,20");
    },
    //加载下注分数
    loadScorebut: function (scores, pushscore) {
        this.bet_but_List.removeAllChildren();
        this.bet_but_List.active = true;
        var score = scores.split(',');
        var width = -185;
        for (var i = 0; i < score.length; i++) {
            width = width + 130;
            this.initScorebut(width, score[i], false);
        }
        if (pushscore) {
            width = width + 130;
            this.initScorebut(width, pushscore, true);
        }
    },
    //初始分数
    initScorebut: function (width, score, pushflag) {

        var playwayComponent = cc.instantiate(this.bet_but_Prefab);
        if (playwayComponent != null) {

            var betBtnRender = playwayComponent.getComponent('betBtnRender');
            betBtnRender.initbet(score, pushflag);

            //playwayComponent.getComponent(cc.Sprite).spriteFrame
            //    = new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/playCards/btn_num" + score + ".png"))
            //将生成的节点放在节点树上
            playwayComponent.setPosition(width, 0);
            playwayComponent.parent = this.bet_but_List;

            playwayComponent.on(cc.Node.EventType.TOUCH_START, function (e) {


                //加载牌
                //var niuniuPoker = this.getCommon("niuniuPoker");
                //niuniuPoker.loadCardsBg(4);
                var betBtnRender = e.target.getComponent('betBtnRender');

                var btnName = e.target.name;
                var bettimes = btnName.substr("btn_num".length, btnName.length);
                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi,
                    bettimes: betBtnRender.getScore(),
                    pushflag: betBtnRender.getPushflag(),
                };
                this.emitSocketRequest("bet", JSON.stringify(param));

                this.bet_but_List.removeAllChildren();

            }, this, true);
        }
    },
    updateCuopaiBrightBtnStatus: function (brightCardStatus, showCardStatus) {
        this.Cuopai_but.active = brightCardStatus;
        this.brightCard_but.active = brightCardStatus;
        this.showCard_btn.active = showCardStatus;

    },
    //翻牌
    onBrightCard_Click: function () {

        var niuniuBegin = this.getCommon("niuniuBegin");
        if (niuniuBegin.dealingData) {

            var mycards = this.decode(niuniuBegin.dealingData.cards);
            var poker = this.getCommon("niuniuPoker");
            poker.clearPlayerStatus();
            poker.showCardsByList(mycards, 0, 0, niuniuBegin.dealingData.lastcard);
        }
        this.updateCuopaiBrightBtnStatus(false, true);

    },
    //亮牌
    onShowCard_Click: function () {

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        };
        this.emitSocketRequest("show", JSON.stringify(param));
    },
    //托管
    onTrusteeShipClick: function () {
        cc.mile.openwin = cc.instantiate(this.trusteeShip_Prefab);
        cc.mile.openwin.parent = this.root();
    },
    //打开快捷语界面
    openQuickLanguage_Click: function () {
        if (this.quickLangage.active) {
            this.quickLangage.active = false;
        } else {
            this.quickLangage.active = true;
        }
    },
    //快捷语
    quickLanguage_Click: function () {
        debugger;
        if (this.btExpression1.active) {
            this.btExpression1.active = false;
            this.btShortCut1.active = true;
            this.quickLangageContext.active = true;
            this.face.active = false;
        }
    },
    //表情页面
    sendFace_Click: function () {
        debugger;
        if (this.btShortCut1.active) {
            this.btExpression1.active = true;
            this.btShortCut1.active = false;
            this.quickLangageContext.active = false;
            this.face.active = true;
        }
    },
    //快捷聊天点击事件
    onDiscourseClick: function (data) {
        //this.discourse.active = true;

        //获取到语音文字对象
        //this.discourseLabel = this.discourse.children[0].getComponent(cc.Label);
        //语音文字内容初始化
        var message = "";
        //音频名称
        var audioName = "";

        //根据点击的控件名称匹配相应的语音文字
        switch (data.target._name) {
            case "1":
                audioName = "daniueat.mp3";
                message = "大牛吃小牛不要伤心哦";
                break;
            case "2":
                audioName = "qd.mp3";
                message = "快点啊！我等到花儿都谢了！";
                break;
            case "3":
                audioName = "qj.mp3";
                message = "接下来见证奇迹的时刻到了！";
                break;
            case "4":
                audioName = "yq.mp3";
                message = "谁给你的勇气，梁静茹吗？";
                break;
            case "5":
                audioName = "ts.mp3";
                message = "看我通杀全场，这些钱都是我的";
                break;
            case "6":
                audioName = "sg.mp3";
                message = "风水轮流转，底裤都输光";
                break;
            case "7":
                audioName = "yz.mp3";
                message = "我的手气全靠颜值撑着";
                break;
            case "8":
                audioName = "zj.mp3";
                message = "我是庄家谁敢挑战我";
                break;
            case "9":
                audioName = "xsn.mp3";
                message = "底牌亮出来，绝对吓死你！";
                break;
            case "10":
                audioName = "hehe.mp3";
                message = "我就呵呵。。";
                break;
            case "11":
                audioName = "lql.mp3";
                message = "大家一起浪起来";
                break;
            default:
                break;
        }
        if (audioName != "") {

            audioName = "mazi/" + this.getSexPathPrefix() + "/" + audioName;
            /*  cc.mile.audio.playSFX(audioName);
              this.discourseLabel.string = message;*/
            //发送消息的对象
            var param = {
                source: cc.mile.user.id,//源用户ID
                msgtype: 1,//消息类型 0，快捷语，1.语音，2.表情，4.文字消息 5. 自己的表情
                sendtype: 1,//发送类型 0.单个发送，1.群发
                msgname: audioName,
                msgcontent: message //消息内容
            };

            var maziBegin = this.getCommon("niuniuBegin");
            maziBegin.sendmsg(param);

        }

        if (this.quickLangage.active) {
            this.quickLangage.active = false;
        }

        //this.scheduleOnce(function () {
        //    this.discourse.active = false;
        //}, 2)

    },
    onSelfFaceClick: function (data) {
        //this.discourse.active = true;

        cc.log("发送自定义表情");

        //获取到语音文字对象
        // this.discourseLabel = this.discourse.children[0].getComponent(cc.Label);
        //语音文字内容初始化
        var message = data.target._name;
        //音频名称
        var audioName = "";

        //根据点击的控件名称匹配相应的语音文字
        switch (data.target._name) {
            case "aixin":
                audioName = "aixin.mp3";

                break;
            case "shaoxiang":
                audioName = "shaoxiang.mp3";

                break;
            case "kaixin":
                audioName = "kaixin.mp3";

                break;
            case "langligelang":
                audioName = "langligelang.mp3";

                break;
            case "tuxue":
                audioName = "tuxue.mp3";

                break;
            case "shouqian":
                audioName = "shouqian.mp3";
                break;
            case "shangdiao":
                audioName = "shangdiao.mp3";
                break;
            case "hejiu":
                audioName = "hejiu.mp3";
                break;
            default:
                break;
        }

        if (audioName != "") {

            audioName = "mazi/" + this.getSexPathPrefix() + "/" + audioName;
            /*cc.mile.audio.playSFX(audioName);
            this.discourseLabel.string = message;*/
            //发送消息的对象
            var param = {
                source: cc.mile.user.id,//源用户ID
                msgtype: 5,//消息类型 0，快捷语，1.语音，2.表情，4.文字消息 5. 自己的表情
                sendtype: 1,//发送类型 0.单个发送，1.群发
                msgname: audioName,
                msgcontent: message //消息内容
            };

            var niuniuBegin = this.getCommon("niuniuBegin");
            niuniuBegin.sendmsg(param);
            /*if (this.newchatbg.active) {
                this.newchatbg.active = false;
            }*/
            if (this.quickLangage.active) {
                this.quickLangage.active = false;
            }
        }

        //this.scheduleOnce(function () {
        //    this.discourse.active = false;
        //}, 2)

    },
    //点击用户信息
    onUserInfoClick: function (pos, userInfo) {
        debugger
        //this.updateNewChatbgViewStatus(false);
        this.chatAnimation = this.getCommon("ChatAnimation");
        this.chatAnimation.setChoicePosition(pos);
        this.chatAnimation.setTargetUserid(userInfo.userid);

        var userinfoDialog = cc.instantiate(this.userinfoDialog);
        cc.mile.openwin = userinfoDialog;
        cc.mile.openwin.parent = this.root();
        var userShow = this.getPrefabCommon("showUserinfo", "showUserInfo");
        if (userShow) {
            userShow.initUserInfo(userInfo);
        }
    },
    //加载金币
    showGoldCoin: function () {
        debugger;
        var self = this;
        console.log(this.index);
        self.callback = function () {
            if (this.index > 100) {
                // 在第六次执行回调时取消这个计时器
                self.unschedule(self.callback);
            }
            else {
                var poker = this.getCommon("niuniuPoker");
                poker.loadGoldCoin(this.index);
                console.log(this.index);
                this.index++;
                self.showGoldCoin();
            }
        }
        self.schedule(self.callback, 0.2);
    },
    //离开房间
    onBackClick: function () {
        this.onOpenOrCloseMenu('', 'close');
        if (cc.mile.gamestatus == 'play') {
            this.showCenterToast('游戏已经开始，不能解散？');
        } else {
            this.confirm('确认退出吗？', function (self) {
                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi
                };
                self.emitSocketRequest("leave", JSON.stringify(param));
                self.loadding();
                self.closeConfirm();
            });
        }
    },
    showJiesuanDialog: function (data) {

        cc.log(data);

        this.closeOpenWin();

        cc.mile.openwin = cc.instantiate(this.niuniuJiesuanDialog);
        cc.mile.openwin.parent = this.root();

        var scriptNdoe = cc.find(
            "Canvas/niuniuJiesuanDialog/script/niuniuJiesuanDialog");
        var dialogScript = scriptNdoe.getComponent("niuniuJiesuanDialog");
        dialogScript.initUserView(data);

    },
    onStartGameClick: function () {
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            roomid: cc.mile.startroomid
        };
        this.emitSocketRequest("gamestart", JSON.stringify(param));
    },
    onSitDownClick: function () {
        this.btChat0.active = true;
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
        };
        this.emitSocketRequest("roomsit", JSON.stringify(param));

    },
    //上局回顾
    onPlayReviewClick: function () {
        this.onOpenOrCloseMenu("", "close");
        var review = cc.instantiate(this.review_Prefab);
        review.setPosition(-255, -450);
        review.parent = this.rightViewContainerNode;
        var reviewScript = review.getComponent("reviewDialog");
        var iNowGamenum = parseInt(cc.mile.nowgamenum);
        reviewScript.queryPlayBack(cc.mile.startroomid, iNowGamenum - 1);
        var self = this;
        reviewScript.node.on('onReviewViewClose', function (event) {
            cc.log('-----onReviewViewClose---close--click--!');
            if (self) {
                self.onReviewViewClose();
            }
        });
    },
    //解散房间
    onJiesanClick: function () {
        this.onOpenOrCloseMenu('', 'close');
        if (cc.mile.gamestatus == 'play') {
            var self = this;
            //this.alert("游戏已经开始，不能离开！");
            this.confirm('解散进行中的游戏？', function () {
                self.Menu.active = false;
                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi,
                    dissflag: 1,
                    firstflag: true,
                };
                self.emitSocketRequest("dissroom", JSON.stringify(param));
                self.closeConfirm();
            });
        }
        else{
            this.showCenterToast("游戏未开始，直接离开房间即可!");
        }
    },
    //解散房间
    showJiesanDialog: function (data, isrecovery) {
        var scriptNdoe = cc.find("Canvas/jiesanDialog/script/niuniuJiesanDialog");
        if (scriptNdoe == null || isrecovery) {
            this.jiesanDialogObj = null;
            this.closeOpenWin();
        }
        this.scheduleOnce(function () {
            debugger
            if (!this.jiesanDialogObj) {
                this.jiesanDialogObj = cc.instantiate(this.jiesan_Prefab);
                cc.mile.openwin = this.jiesanDialogObj;
                cc.mile.openwin.parent = this.root();
                var scriptNdoe = cc.find("Canvas/jiesanDialog/script/niuniuJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("niuniuJiesanDialog");
                dialogScript.initUserView(data.dissroomInfoList, data.userid, data.disssecond, "申请解散房间");
            } else {
                var scriptNdoe = cc.find("Canvas/jiesanDialog/script/niuniuJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("niuniuJiesanDialog");
                dialogScript.updatePlayerStatus(data.dissroomInfoList);
            }
        }, 0.2);
    },
    //游戏设置
    onSettingClick: function () {
        this.onOpenOrCloseMenu('', 'close');
        cc.mile.openwin = cc.instantiate(this.gameSet_Prefab);
        cc.mile.openwin.parent = this.root();
    },
    disAgreeDissroom: function (data) {
        if (data.dealflag == 0) {
            this.showCenterToast("解散超时，请重新申请解散!");

        } else {

            var userName = "";
            for (var i = 0; i < data.dissroomInfoList.length; i++) {
                if (data.dissroomInfoList[i].dissflag == 2) {
                    userName = data.dissroomInfoList[i].username;
                }
            }
            if (userName) {
                this.showCenterToast(userName + "拒绝解散房间!");
            }
        }
        this.jiesanDialogObj = null;
        this.closeOpenWin();

    },
    agreeDissroom: function (data) {
        this.closeOpenWin();
        this.jiesanDialogObj = null;
        //this.scene("mazihall", this);
    },
    initGameInfo: function (data) {
        cc.log("初始化房间信息：" + JSON.stringify(data));
        if (data.roomdesc) {

            this.nowgamenum = data.nowgamenum;
            this.roomIdLabel.string = data.roomdesc;
            this.gameScoreLabel.string = data.basescore;
            this.gameNumberLabel.string = data.nowgamenum + '/' + data.gamenumbers;
            this.gameDescLabel.string = data.playwaydesc;

            var roomInfo = cc.instantiate(this.roomInfo_pfb);
            this.roomInfo_pfb = roomInfo;
            var roomInfoData = roomInfo.getComponent("roomInfo");
            roomInfoData.initRoomInfo(data);
        }

    },
});
