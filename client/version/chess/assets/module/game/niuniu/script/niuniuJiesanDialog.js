var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        playJiesanPrefab: {
            default: null,
            type: cc.Prefab
        },
        playerContainer: {
            default: null,
            type: cc.Node
        },
        clockDescLabel: {
            default: null,
            type: cc.Label
        },
        playApplyDescLabel: {
            default: null,
            type: cc.Label
        },
        agreeBtn: {
            default: null,
            type: cc.Button
        },
        disagreeBtn: {
            default: null,
            type: cc.Button
        }
    },
    onLoad: function () {
        this.playerJiesanLst = [];
        //this.initUserView();
    },
    initUserView: function (userlist, firstUserId, disssecond, des) {

        cc.log("initUserView:" + JSON.stringify(userlist));
        this.jiesanFlag = true;
        // 0  等待， 1 同意 ， 2 拒绝
        for (var i = 0; i < userlist.length; i++) {
            var playJiesan = cc.instantiate(this.playJiesanPrefab);
            var marginWidth = -230 + 150 * i;
            var playScript = playJiesan.getComponent("PlayJiesan");
            var isFirstUser = userlist[i].userid == firstUserId ? true : false;
            playScript.initData(userlist[i], isFirstUser);
            if (isFirstUser) {
                this.playApplyDescLabel.string = "玩家" + userlist[i].username + "" + des + "";
            }
            if (userlist[i].dissflag == 1 && userlist[i].userid == cc.mile.user.id) {
                this.agreeBtn.interactable = false;
                this.disagreeBtn.interactable = false;
            }
            playJiesan.setPosition(marginWidth, 20);

            playJiesan.parent = this.playerContainer;
            this.playerJiesanLst.push(playJiesan);
        }


        this.disssecond = disssecond;

        this.clockDescLabel.string = this.disssecond;


        var self = this;
        if (this.runtimerdd) {
            this.unschedule(this.runtimerdd);
            //self.unscheduleAllCallbacks();
        }
        this.runtimerdd = function () {

            self.disssecond--;
            self.clockDescLabel.string = self.disssecond;
        };
        this.schedule(this.runtimerdd, 1, disssecond - 1, 1);// 间隔时间，重复次数，推迟执行时间
    },

    onAgreeClick: function () {

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            dissflag: 1,
            firstflag: false,
        };
        this.emitSocketRequest("dissroom", JSON.stringify(param));

        this.agreeBtn.interactable = false;
        this.disagreeBtn.interactable = false;

    },
    onDisagreeClick: function () {
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            dissflag: 2,
            firstflag: false,
        };
        this.emitSocketRequest("dissroom", JSON.stringify(param));
        //cc.mile.socket.emit("dissroom", JSON.stringify(param));

        this.agreeBtn.interactable = false;
        this.disagreeBtn.interactable = false;

    },
    updateGiveUpPlayerStatus: function (userlist) {
        for (var i = 0; i < userlist.length; i++) {
            var playScript = this.getPlayJiesanByUserId(userlist[i].userid);
            if (playScript) {
                if (userlist[i].giveupflag == 1) {
                    playScript.setAgree();
                } else if (userlist[i].giveupflag == 0) {
                    playScript.setWaitting();

                } else if (userlist[i].giveupflag == 2) {
                    playScript.setDisagree();
                }
            }
        }
    },
    updatePlayerStatus: function (userlist) {

        for (var i = 0; i < userlist.length; i++) {
            var playScript = this.getPlayJiesanByUserId(userlist[i].userid);
            if (playScript) {
                if (userlist[i].dissflag == 1) {
                    playScript.setAgree();
                } else if (userlist[i].dissflag == 0) {
                    playScript.setWaitting();

                } else if (userlist[i].dissflag == 2) {
                    playScript.setDisagree();
                }
            }
        }
    },
    getPlayJiesanByUserId: function (userid) {
        for (var i = 0; i < this.playerJiesanLst.length; i++) {

            var playScript = this.playerJiesanLst[i].getComponent("PlayJiesan");
            if (playScript.userid == userid) {
                return playScript;
            }
        }
        return null;
    },

    onDestroy: function () {
        if (this.runtimerdd) {
            this.unschedule(this.runtimerdd);
            //self.unscheduleAllCallbacks();
        }
    }

    // update (dt) {},
});
