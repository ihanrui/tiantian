/**
 *
 * niuniu 单个用户结算
 *
 */
var beiMiCommon = require("MileCommon");
var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
  extends: beiMiCommon,

  properties: {
    userHeadNode: {
      default: null,
      type: cc.Sprite
    },
    userNameNode: {
      default: null,
      type: cc.Label
    },

    pushLabel: {
      default: null,
      type: cc.Label
    },

    scoreNode: {
      default: null,
      type: cc.Node
    },

    pokerContainerNode: {
      default: null,
      type: cc.Node
    },

    pokerTypeNode: {
      default: null,
      type: cc.Node
    },
    //显示的牌
    card: {
      default: null,
      type: cc.Prefab
    },
    //纸牌集
    atlas: {
      default: null,
      type: cc.SpriteAtlas
    },
    bankerTypeNode: {
      default: null,
      type: cc.Node
    },
    hogtimesNode: {
      default: null,
      type: cc.Node
    },

  },

  onLoad: function () {
    // this.addTouchEvent();

  },
  initData: function (userdata, pinshihiscards, gameRoom) {
    if (userdata.gameid) {

      this.userNameNode.string = userdata.username;

      this.loadUrlJPGImage(userdata.userurl
          + "&username=" + userdata.userid, this.userHeadNode);
      this.userid = userdata.userid;

      if (gameRoom.roomtype == 'card') {
        WangdaGameUtils.showNumberNode(pinshihiscards.stepscore,
            this.scoreNode);
      } else {
        // 如果是金币场  coins
        WangdaGameUtils.showNumberNode(pinshihiscards.stepscore,
            this.scoreNode);
      }
      this.pushLabel.string = pinshihiscards.bettimes;

      var cards = userdata.cardstr.split(',');
      this.showCardsByList(cards, 0, 0, pinshihiscards.lastcard);
      this.showCardTypeView(pinshihiscards.cardtype);

      this.hogtimesNode.getComponent(cc.Sprite).spriteFrame =
          new cc.SpriteFrame(cc.url.raw("resources/images/room/niuniu/playCards/host" + pinshihiscards.hogtimes + ".png"));

      if (pinshihiscards.banker) {
        this.bankerTypeNode.active = true;
      } else {
        this.bankerTypeNode.active = false;
      }
    } else {
      this.viewContainerNode.active = false;
    }
  },
  showCardsByList: function (cardLst, startIndex, yPosition, lastcard) {

    for (var i = 0; i < cardLst.length && i < 5; i++) {
      //将一个一个牌显示在对象池当中
      var xPosition = parseFloat(-30) + parseFloat(35 * (i + startIndex));
      cc.log("牌的位置：" + xPosition);
      this.loadSingleCard(cardLst[i], xPosition, yPosition, lastcard
          == cardLst[i]);
    }
  },
  showCardTypeView: function (cardType) {
    this.pokerTypeNode.getComponent(cc.Sprite).spriteFrame =
        new cc.SpriteFrame(
            cc.url.raw("resources/images/room/niuniu/pokertype/imgNN" + cardType
                + ".png"));
    this.pokerTypeNode.active = true;

  },
  //加载背景牌
  loadSingleCard: function (cardOriginValue, xPosition, yPosition, isLastCard) {
    //将一个一个牌显示在对象池当中
    cc.log("加载单个牌：" + cardOriginValue + "---" + xPosition);
    var poker = cc.instantiate(this.card);
    var cardValue = parseInt(cardOriginValue / 4) * 8 + cardOriginValue % 4;
    var pokerRender = poker.getComponent("singleCard");
    if (pokerRender) {
      pokerRender.updateLastCardViewStatus(isLastCard);
    }
    poker.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(
        cardValue);

    poker._name = cardOriginValue;
    poker.active = true;
    poker.setScale(0.4, 0.4);
    poker.setPosition(xPosition, yPosition);
    poker.parent = this.pokerContainerNode;

  },
});