
cc.Class({
    extends: cc.Component,

    properties: {
    	lastCard: {
        default: null,
        type: cc.Node
     },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    updateLastCardViewStatus: function (status) {
        this.lastCard.active = status;
    },

    // update (dt) {},
});
