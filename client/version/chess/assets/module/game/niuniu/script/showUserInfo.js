var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {

        username: {
            default: null,
            type: cc.Label
        },
        userid: {
            default: null,
            type: cc.Label
        },
        userip: {
            default: null,
            type: cc.Label
        },
    },

    // use this for initialization
    onLoad: function () {
        this.userParams = {};
    },
    initUserInfo: function (userInfo) {
        cc.log("用户信息展示："+JSON.stringify(userInfo));
        this.userParams = userInfo;
        this.username.string = userInfo.username;
        this.userid.string = userInfo.gameid;
        this.userip.string = userInfo.userip;
    },
    onPlayTest: function (event, data) {
        debugger
        var chatAnimation = this.getCommon("ChatAnimation");
        if (this.userid.string==cc.mile.user.gameid) { //自己点开不处理表情
            this.closeOpenWin();
        } else {
            if (data) {// data 点击不同的表情发送
                chatAnimation.setSourcePosition(chatAnimation.selfPosition);
                chatAnimation.setTargetPosition(chatAnimation.getChoicePosition());
                var isSend = chatAnimation.sendPlayAnimation(cc.mile.user.id,data);
                this.closeOpenWin();
                if(isSend){
                    var niuniuBegin = this.getCommon("niuniuBegin");
                    niuniuBegin.sendmsg({
                        source: cc.mile.user.id,
                        target: this.userParams.userid,
                        msgtype: '2',
                        sendtype: '0',
                        msgid: data,
                        msgname: '',
                        msgcontent: '',
                        mem: ''
                    });
                }
                if (chatAnimation.animationNode) {
                    this.closeOpenWin();
                }
            }
        }
    }
});
