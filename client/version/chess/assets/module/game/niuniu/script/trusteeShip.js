var beiMiCommon = require("MileCommon");

// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: beiMiCommon,
    properties: {
        //抢庄的值
        qiangZhuangValue:
        {
            default: null,
            type: cc.Node
        },

        //抢庄选项类型
        toggleNiuNiuType:
            {
                default: null,
                type: cc.Node
            },
        //牛牛的倍数
        toggleNiuNiuMultiple:
            {
                default: null,
                type: cc.Node
            },
        //抢庄
        qiangZhuangDropDown:
         {
           default: null,
             type: cc.Node
         },
        // 下注的选项值
        xiaZhuDropDown:
            {
                default: null,
                type: cc.Node
            },
        //下注
        xiaZhuList:{
            default: null,
            type: cc.Node
        },
        xiazhuDropDownValue:
            {
                default: null,
                type: cc.Node
            },
    },

    // LIFE-CYCLE CALLBACKS:
    //加载
    onLoad: function () {
            this.selectValueAutomaticAssignment();
    },
    //根据抢庄选项，自动赋值
   selectValueAutomaticAssignment:function () {
       ///抢庄选项值
       var qiangZValue="";
       var toggleNiuNiuValue="";
       ///雷尼格
       for(var i=0;i<this.toggleNiuNiuType.children.length;i++)
       {
           //如何等于选中状态
           if(this.toggleNiuNiuType.children[i].getComponent(cc.Toggle).isChecked==true)
           {
               qiangZValue=this.toggleNiuNiuType.children[i].children[2].getComponent(cc.Label).string;
           }
       }
       ///雷尼格
       for(var i=0;i<this.toggleNiuNiuMultiple.children.length;i++)
       {
           //如何等于选中状态
           if(this.toggleNiuNiuMultiple.children[i].getComponent(cc.Toggle).isChecked==true)
           {
               toggleNiuNiuValue=this.toggleNiuNiuMultiple.children[i].children[2].getComponent(cc.Label).string;
           }
       }
       if(qiangZValue!=""&&toggleNiuNiuValue!="") {
           this.qiangZhuangValue.getComponent(cc.Label).string = qiangZValue+toggleNiuNiuValue;
       }
   },
    //
    onClickNiuNiuType:function(e,index)
    {
        this.selectValueAutomaticAssignment();
    },
    onClickMultiple:function(e,index) {
        this.selectValueAutomaticAssignment();
    },
    //点击文本选中当前的值
    onClickSelectValue:function(e,index) {
        debugger;
        //把当前所有的全部取消
        for(var i=0;i<e.target.parent.parent.children.length;i++) {
            e.target.parent.parent.children[i].getComponent(cc.Toggle).isChecked = false;
        }
        e.target.parent.getComponent(cc.Toggle).isChecked = true;
        this.selectValueAutomaticAssignment();
        var xiazhuvalue="";
        for (var i = 0; i < this.xiaZhuList.children.length; i++) {
            if (this.xiaZhuList.children[i].getComponent(cc.Toggle).isChecked == true) {
                xiazhuvalue=this.xiaZhuList.children[i].children[2].getComponent(cc.Label).string;
            }
        }
        if(xiazhuvalue) {
            this.xiazhuDropDownValue.getComponent(cc.Label).string = xiazhuvalue;
        }
    },
    //抢庄选项显示与隐藏
    onClickQiangzhuangDropDown:function () {
        debugger;
        if (this.qiangZhuangDropDown.active == true) {
            this.qiangZhuangDropDown.active = false;
        }
        else {
            this.qiangZhuangDropDown.active = true;
        }
        this.xiaZhuDropDown.active = false;
    },
    //下注选项显示与隐藏
    onClickxiazhuDropDown:function () {
        if (this.xiaZhuDropDown.active == true) {
            this.xiaZhuDropDown.active = false;
        }
        else {
            this.xiaZhuDropDown.active = true;
        }
        this.qiangZhuangDropDown.active = false;
    },
    //下注选项赋值
    onClickXiaZhuValue:function() {
        var xiazhuvalue="";
        for (var i = 0; i < this.xiaZhuList.children.length; i++) {
            if (this.xiaZhuList.children[i].getComponent(cc.Toggle).isChecked == true) {
                xiazhuvalue=this.xiaZhuList.children[i].children[2].getComponent(cc.Label).string;
            }
        }
        if(xiazhuvalue) {
            this.xiazhuDropDownValue.getComponent(cc.Label).string = xiazhuvalue;
        }
        this.qiangZhuangDropDown.active = false;
    },
    //隐藏选庄和下注选项
    onHideDropDown:function() {
        this.qiangZhuangDropDown.active = false;
        this.xiaZhuDropDown.active = false;
    },
    //显示抢庄的列表
    onShowQiangZhuangDropDown:function() {
        this.qiangZhuangDropDown.active = true;
    },
    //显示抢庄的列表
    onShowXiaZhuDropDown:function() {
        this.xiaZhuDropDown.active = true;
    },
    // update (dt) {},
});
