var beiMiCommon = require("MileCommon");
var GameUtil = require("WangdaGameUtils");
var VoiceOnLineUtils = require("VoiceOnLineUtils");

var niuniuBeginSelf = null;

var status_tips = {
    watch: '旁观中',
    waitting_owner: '等待房主开始',
    waitting_player_join: '等待玩家加入',
    waitting_score: '请选择抢庄倍数',
    waitting_player_score: '等待其他玩家抢庄',
    waitting_bet: '请选择下注倍数',
    waitting_player_bet: '等待其他玩家下注',
    waitting_show: '请亮牌',
    waitting_player_show: '等待玩家亮牌',
    waitting_ready: '请准备',
    waitting_player_ready: '等待玩家准备',
};

cc.Class({
    extends: beiMiCommon,
    properties: {
        //自己信息预制资源
        SelfPalyPrefab: {
            default: null,
            type: cc.Prefab
        },
        //自己位置显示区域
        SelfPalyNode: {
            default: null,
            type: cc.Node
        },
        //用户预制资源
        playRender_Prefab: {
            default: null,
            type: cc.Prefab
        },
        //其他用户加载区域
        palyNode: {
            default: null,
            type: cc.Node
        },
        //自己牌显牌区域
        pokerSeeList: {
            default: null,
            type: cc.Node
        },
        //邀请，复制
        inviteplayerContainer: {
            default: null,
            type: cc.Node
        },
        //邀请，复制预制资源
        inviteplayerPrefab: {
            default: null,
            type: cc.Prefab
        },
        //当前状态按钮（准备,坐下,开始游戏）
        currentBtnContainer: {
            default: null,
            type: cc.Node
        },


        startGameNode: {
            default: null,
            type: cc.Node
        },
        sitdownStartGameNode: {
            default: null,
            type: cc.Node
        },

        ownerSitdownNode: {
            default: null,
            type: cc.Node
        },
        normalSitdownNode: {
            default: null,
            type: cc.Node
        },

        playreadyBtn: {
            default: null,
            type: cc.Node
        },
        tipContainerNode: {
            default: null,
            type: cc.Node
        },
        tipLabel: {
            default: null,
            type: cc.Label
        },
        //下注分背景
        betScore_bg: {
            default: null,
            type: cc.Node
        },
    },

    /**
     *
     * @param gamestep
     * @param stepflag
     * @param snd
     */
    updateStatusTipAndClock: function (gamestep, stepflag, snd) {
        debugger
        cc.log("恢复牌局的玩家状态：" + gamestep + "----stepflag："
            + stepflag + "----startflag：" + cc.mile.nowgamenum);

        var tips = "";
        var sndd = snd;
        if (gamestep == 'show') {
            if ('Y' == stepflag) {
                tips = status_tips.waitting_player_show;
            } else {
                tips = status_tips.waitting_show;
            }
        } else if (gamestep == 'bet') {
            if ('Y' == stepflag) {
                tips = status_tips.waitting_player_bet;
            } else {
                tips = status_tips.waitting_bet;
            }
        } else if (gamestep == 'hog') {
            if ('Y' == stepflag) {
                tips = status_tips.waitting_player_score;
            } else {
                tips = status_tips.waitting_score;
            }
        } else if (gamestep == 'begin') {
            if ('ready' == stepflag) {
                if (cc.mile.nowgamenum == "0") {
                    tips = status_tips.waitting_owner;
                } else {
                    tips = status_tips.waitting_player_join;
                }
            } else if ('Y' == stepflag) {
                debugger
                if (cc.mile.creator == cc.mile.user.gameid) {
                    tips = status_tips.waitting_player_join;
                } else {
                    tips = status_tips.waitting_owner;
                }
            } else if ('N' == stepflag) {
                debugger
                if (cc.mile.creator == cc.mile.user.gameid) {
                    tips = status_tips.waitting_player_join;
                } else {
                    tips = status_tips.waitting_owner;
                }
            } else {
                tips = status_tips.waitting_ready;
            }
        } else if (gamestep == 'watch') {
            tips = status_tips.watch;
        }
        if (sndd) {
            this.updateTiplabel(tips + "  " + sndd);
            this.showBackTime(sndd);
        } else {
            this.updateTiplabel(tips);

        }
    },

    updateTiplabel: function (tip) {
        this.tipLabel.string = tip;
        //this.tipRichLabel.string = tip;
    },

    /**
     *   显示闹钟并倒计时
     *   @param playuser
     */
    showBackTime: function (snd) {

        if (snd) {

            if (this.runClockSchedule) {
                this.unschedule(this.runClockSchedule);
                //self.unscheduleAllCallbacks();
            }

            this.snd = snd;

            this.runClockSchedule = function () {
                var labelStr = this.tipLabel.string;
                if (this.snd > 0) {
                    this.snd--
                    if (labelStr.indexOf("  ") > 0) {
                        this.updateTiplabel(labelStr.substr(0, labelStr.indexOf("  "))
                            + "  " + this.snd);
                    } else {

                        this.updateTiplabel(labelStr + "  " + this.snd);
                    }
                }
            };
            this.schedule(this.runClockSchedule, 1, snd, 1);


        }
    },

    // use this for initialization
    onLoad: function () {
        //保存全局对象
        niuniuBeginSelf = this;
        cc.gvoiceDownloadCallbackJS = this.gvoiceDownloadCallbackJS;
        cc.gvoicePlayFinishedCallbackJS = this.gvoicePlayFinishedCallbackJS;
        /**
         * 适配屏幕尺寸
         */
        this.resize();
        this.playerRenderLst = new Array();     //存放玩家渲染数据
        this.summarypage = null;
        this.selfPoker = this.getCommon("niuniuPoker"); // 存放自己的牌管理对象

        this.inited = false;
        this.lasttip = null;
        this.voiceData = {};

        var beginTime = Date.now();
        cc.log("data.开始初始化 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");

        if (cc.mile != null) {
            if (cc.mile.gamestatus != null && cc.mile.gamestatus == 'play') {
                //恢复数据
                this.updateInviteplayerBtnViewStatus(false);
                this.recovery();
            } else {
                this.updateInviteplayerBtnViewStatus(true);
            }
            this.initgame();
        }

    },

    //// 进入房间需要恢复数据，有个倒计时跟新
    recovery: function () {
        //this.statictimer("正在恢复数据，请稍候", cc.mile.data.waittime);
    },
    // 更新 准备 按钮状态
    updateBeginBtnViewStatus: function (status) {
        //this.current_status.children[0].active = status;
        //this.current_status.children[1].active = !status;
    },
    //更新 邀请/复制
    updateInviteplayerBtnViewStatus: function (status) {
        this.inviteplayerContainer.active = status;
    },
    //  初始化房间，注册socket 监听事件
    initgame: function () {
        cc.log("初始化游戏  this.ready() 【" + this.ready() + "】")
        if (this.ready()) {
            this.updateBeginBtnViewStatus(true);
            this.game = this.getCommon("niuniuBind");

            this.initSocketMapping();
            this.initSocketEvent();
            /**
             *   此处有个问题：游戏中，如果点击了返回，进入过其他游戏，这个 cc.mile.extparams.playway
             *   就发生了改变，会导致当前游戏结束后，继续游戏的时候会出现问题
             * @type {{token: (*|null), playway, orgi, extparams: *}}
             */
            this.emitJoinEvent();

            this.inited = true;
            //进入后台
            if (cc && cc.eventManager) {
                var self = this;
                cc.eventManager.addCustomListener("socketReconnect", function (event) {
                    cc.log('-----socketReconnect-----后台更新了--!');
                    if (self) {
                        self.initSocketEvent();
                    }
                });
            }
        }

        var beginTime = Date.now();
        cc.log("data.结束初始化 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");

        // 初始化邀请好友
        var invitePlayer = cc.instantiate(this.inviteplayerPrefab);
        invitePlayer.parent = this.inviteplayerContainer;
    },

    emitJoinEvent: function () {
        var teanum = "";
        if (cc.mile.teahouse) {
            teanum = cc.mile.teahouse.teanum;
        }

        var param = {
            token: cc.mile.authorization,
            playway: cc.mile.extparams.playway,
            orgi: cc.mile.user.orgi,
            extparams: cc.mile.extparams,
            roomtype: cc.mile.createroomtype,
            teanum: teanum,
            tablenum: cc.mile.tablenum,
        };
        this.emitSocketRequest("joinroom", JSON.stringify(param));
    },
    initSocketEvent: function () {
        var self = this;
        var socket = this.socket();
        cc.log('-----initSocketEvent-------：command_room');
        socket.on("command_room", function (result) {
            if (self.inited == true) {
                var data = self.parse(result);
                cc.log("事件 data.command [" + data.command + "]  数据内容为:  " + JSON.stringify(result));
                self.route(data.command)(data, self);
                try {

                } catch (e) {

                    cc.log("出错 data.command [" + data.command + "]  数据内容为:  " + JSON.stringify(result));
                    self.loggerError('响应房间事件出错：' + cc.mile.user.username
                        + JSON.stringify(e) + result);
                }
            }
        });
        /**
         * 心跳检查，服务端发起的事件，服务端可以设置 PING的时长和 PING的 TimeOut
         */
        socket.on("ping", function (event) {
            cc.log('-----ping-------：' + JSON.stringify(event));
        });
    },
    //注册交互
    initSocketMapping: function () {
        this.map("joinroom", this.joinroom_event);          //加入房间
        this.map("players", this.players_event);            //接受玩家列表
        this.map('play', this.play_event);                      //接受玩家列表


        this.map("roomready", this.roomready_event);              //所有玩家准备
        this.map("hog", this.hog_event);                        //抢庄
        this.map("bind", this.bind_event);              //定庄
        this.map("bet", this.bet_event);              //下注完成
        this.map("dealing", this.dealing_event);              //亮牌
        this.map("show", this.show_event);              //亮牌

        this.map("roomsit", this.roomsit_event);              //所有玩家准备

        this.map("gamestart", this.gamestart_event);            //玩家点击了开始游戏 ， 即准备就绪
        this.map("playerready", this.playerready_event);            //玩家点击了开始游戏 ， 即准备就绪

        this.map("recovery", this.recovery_event);              //恢复牌局数据
        this.map("sendmsg", this.sendmsg_event);              //发送消息

        this.map("leave", this.leave_event);              //恢复牌局数据

        this.map("stepclean", this.stepclean_event);              //单小结
        this.map("clean", this.clean_event);              //总结算

        this.map("roomlinestatus", this.roomlinestatus_event);              //玩家状态更新

        this.map("teadissroom", this.teadissroom_event);              //玩家状态更新

        this.map("dissroom", this.dissroom_event);              //小结
        this.map("createroom", this.createroom_event);              //创建房间，房卡不足
    },
    /**
     * 新创建牌局，首个玩家加入，进入等待状态，等待其他玩家加入，服务端会推送 players数据
     * @param data
     * @param context
     */
    joinroom_event: function (data, context) {
        debugger;
        context.game.initGameInfo(data);

        cc.mile.maxplayers = data.maxplayers;
        cc.mile.curpalyers = data.curpalyers;
        cc.mile.startroomid = data.roomid;
        cc.mile.nowgamenum = data.nowgamenum;
        cc.mile.creator = data.creator;

        if (data.player.gamestatus != 'watch') {

            if (data.player.id && data.player.id == cc.mile.user.id) {

                context.initSelfData(data.player);


            } else {

                ///其他玩家加入，初始化、判断新加入的玩家是否已在房间中
                context.initPlayerView(data.player, false);
            }
        }
        if (data.player.id && data.player.id == cc.mile.user.id) {
            context.updateRoomBtnStatus('owner', data.player.gamestatus);
        }
        cc.mile.joinroomFinish = true;
    },

    //初始化自己信息
    initSelfData: function (playerData) {
        debugger
        if (!this.selfPlayerRender) {

            var niuniuBind = this.getCommon("niuniuBind");
            niuniuBind.btChat0.active = true;

            var player = cc.instantiate(this.SelfPalyPrefab);
            var selfHeadPosition = cc.v2(-570, -290);

            var chatAnimation = this.getCommon("ChatAnimation");
            chatAnimation.initSelfPosition(selfHeadPosition);
            player.parent = this.SelfPalyNode;


            this.selfPlayerRender = player.getComponent("playRender");
            this.selfPlayerRender.setPlayerRelationPosition(selfHeadPosition.x, selfHeadPosition.y);


            this.playerRenderLst.push(player.getComponent("playRender"));
        }
        playerData.relationIndex = -1;
        this.selfPlayerRender.initplayer(playerData);


    },

    testSendCoin: function () {

    },

    //加载用户
    loadplayRender: function (playerData, x, y) {
        debugger
        if (playerData.playuser) {
            playerData.id = playerData.playuser;
        }
        if (playerData.id == cc.mile.user.id) {
            this.initSelfData(playerData);
            this.selfIndex = playerData.index; //当前玩家所在位置
        } else {
            var renderInit = this.getPlayerRenderById(playerData.id);
            if (renderInit) {
                renderInit.initplayer(playerData);
            } else {
                var playRender = cc.instantiate(this.playRender_Prefab);
                playRender._name = "play" + playerData.index;
                playRender.setPosition(x, y);
                playRender.parent = this.palyNode;

                renderInit = playRender.getComponent("playRender");
                renderInit.initplayer(playerData);
                renderInit.setPlayerRelationPosition(x, y);
                playRender.on(cc.Node.EventType.TOUCH_START, function (e) {
                    var bind = this.getCommon("niuniuBind");
                    bind.onUserInfoClick(cc.v2(x - 20, y + 180), {
                        userid: playerData.id,
                        username: playerData.username,
                        userip: playerData.ip,
                        gameid: playerData.gameid,
                    });
                }, this, true);
                this.playerRenderLst.push(renderInit);
            }

            if (cc.mile.creator == cc.mile.user.gameid && this.playerRenderLst.length > 1) {
                this.updateTiplabel("");
                this.tipContainerNode.active = false;
            }
        }
    },

    /**
     * 初始化玩家界面
     * @param playerData
     * @param isRecover
     */
    initPlayerView: function (playerData, isRecover) {
        debugger

        var playerid = playerData.playuser ? playerData.playuser : playerData.id;
        var bRender = this.checkPlayerIsRender(playerid);
        cc.log(bRender + ":playerid:" + playerid + "---initPlayerView:==" + JSON.stringify(playerData));

        if (!bRender) {
            //this.newplayer(playerData);
            var relationIndex = this.getPlayerRelationIndex(playerData.index);

            cc.log("relationIndex:" + playerData.index + "---relationIndex:" + relationIndex
                + "---selfIndex:" + this.selfIndex + "---maxplayers:" + cc.mile.maxplayers);

            var position = this.getPlayerPosition(relationIndex);
            playerData.relationIndex = relationIndex;
            this.loadplayRender(playerData, position.x, position.y);

            if (playerid == cc.mile.user.id) {
                this.recover_selfoperator_status(playerData);  //设置玩家的状态
            }

            this.recover_singleplay_status(playerData, isRecover);

        } else {
            this.recover_singleplay_status(playerData, isRecover);
        }
    },
    clearPlayerView: function (playerData) {

        var playerid = playerData.playuser ? playerData.playuser : playerData.id;


        for (var inx = this.playerRenderLst.length - 1; inx >= 0; inx--) {
            var render = this.playerRenderLst[inx];

            if (render.getPlayerId() == playerid) {
                render.node.destroy();
                this.playerRenderLst.splice(inx, 1);
            }
        }
        if (playerid == cc.mile.user.id) {
            this.updateStatusTipAndClock();
            var pokers = this.getCommon("niuniuPoker");
            pokers.clearPlayerStatus();
            this.selfPlayerRender = null;
            this.updateStatusTipAndClock('watch', '', null);
        }


    },
    continueGameStatusInit: function () {
        if (this.selfPoker) {
            this.selfPoker.clearAllPokers();
            this.selfPoker.clearSefAllPokers();
        }
        this.clock.active = false;
        this.updateBeginBtnViewStatus(true);
        if (cc.mile.gamestatus == 'play') {
            this.updateInviteplayerBtnViewStatus(false);
        } else {
            this.updateInviteplayerBtnViewStatus(true);
        }
        this.setPlayOptionStatus(false, false, false);
    },
    // 根据index  获取玩家的 位置
    checkPlayerIsRender: function (playerId) {

        for (var i = 0; i < this.playerRenderLst.length; i++) {
            var render = this.playerRenderLst[i];
            if (render.userid == playerId) {
                return true;

            }
        }

        return false;
    },
    getPlayerRelationIndex: function (playerIndex) {
        var playUpdateIndex = (playerIndex - this.selfIndex
            + cc.mile.maxplayers) % cc.mile.maxplayers;
        return playUpdateIndex;
    },
    /**
     * 房间大家都准备好了
     *
     * @param data
     * @param context
     */
    roomready_event: function (data, context) {
        cc.log("roomready_event [" + data + "]");

        if (data.cardroom == true) {
            context.updateInviteplayerBtnViewStatus(false);
        } else if (data.cardroom == false) {
            context.updateInviteplayerBtnViewStatus(true);
        }

    },
    /**
     * 玩家抢庄
     *
     * @param data
     * @param context
     */
    hog_event: function (data, context) {
        cc.log("hog_event [" + JSON.stringify(data) + "]");

        var render = context.getPlayerRenderById(data.userid);
        if (render) {
            render.updateFinishedViewStatus(true);
            if (data.userid == cc.mile.user.id) {
                render.updateQiangMultiple(data.hogtimes);
                context.updateTiplabel(status_tips.waitting_player_score);
            }
        }
    },
    /**
     * 下注完成
     *
     * @param data
     * @param context
     */
    bet_event: function (data, context) {
        debugger
        cc.log("bet_event [" + JSON.stringify(data) + "]");
        var render = context.getPlayerRenderById(data.userid);
        if (render) {
            if (data.userid != cc.mile.user.id) {
                var index = render.getRelationIndex();
                render.HairCrad(4);
                render.updateCoinMultiple(index, data.bettimes);
                render.updateFinishedViewStatus(false);
                render.updatePushContainerNodeViewStatus(false);
            }
        }
        if (data.userid == cc.mile.user.id) {
            context.updateTiplabel(status_tips.waitting_player_bet);
            //waitting_show
            var pokers = context.getCommon("niuniuPoker");
            var xPosition = parseFloat(-250) + parseFloat(125 * 4);
            pokers.loadCardsBg(-1, xPosition, 4);

            context.updatebetScoreViewStatus(true);
            context.BetAnimation(data.bettimes);
            render.updateFinishedViewStatus(false);
            render.updatePushContainerNodeViewStatus(false);
        }

    },
    /**
     * 定庄后发送牌过来
     *
     * @param data
     * @param context
     */
    dealing_event: function (data, context) {
        debugger
        cc.log("dealing_event [" + JSON.stringify(data) + "]");

        context.dealingData = data;
        context.game.updateCuopaiBrightBtnStatus(true, false);

        //context.updateTiplabel(status_tips.waitting_show);
        if (data.userid == cc.mile.user.id) {

            context.updateStatusTipAndClock('show', 'N', data.showsnd);
        }

        /*
        var pokers = context.getCommon("niuniuPoker");
        if (!pokers) {
            pokers = this.selfPoker;
        }
        var mycards = context.decode(data.cards);
        pokers.loadCards(mycards, data.lastcard);*/


    },


    /**
     * show牌
     *
     * @param data
     * @param context
     */
    show_event: function (data, context) {
        cc.log("show_event [" + JSON.stringify(data) + "]");
        context.showPlayerCards(data);

    },
    showPlayerCards: function (data) {
        debugger
        var lastCard = data.lastcard;
        data.cardType.lastcard = lastCard;
        if (data.userid == cc.mile.user.id) {


            this.updateTiplabel(status_tips.waitting_player_show);
            var pokers = this.getCommon("niuniuPoker");
            if (!pokers) {
                pokers = this.selfPoker;
            }
            pokers.showCards(data.cardType);
            pokers.showCardTypeView(data.cardType);
            this.game.updateCuopaiBrightBtnStatus(false, false);
        } else {

            var playRender = this.getPlayerRenderById(data.userid);
            playRender.OpenCrad(data.userid, data.cardType);
            playRender.showCardTypeView(data.cardType);
        }
        this.playNiuniuVoice(data);
    },
    playNiuniuVoice: function (data) {
        var voiceName = GameUtil.getNiuniuVoiceName(data.cardType);
        var voicePath = "niuniu/" + this.getNiuNiuVociePathPrefix() + "/" + voiceName
        cc.mile.audio.playSFX(voicePath);
    },
    getNiuNiuVociePathPrefix: function () {
        var e = cc.mile.user.gender;
        return 2 == e || "2" == e ? "girl" : "boy";
    },
    /**
     * 定庄完成
     *
     * @param data
     * @param context
     */
    bind_event: function (data, context) {
        cc.log("bind_event [" + JSON.stringify(data) + "]");

        context.showBankerView(data);

    },
    showBankerView: function (data) {
        debugger
        this.currentBanker = data.banker;

        for (var i = 0; i < this.playerRenderLst.length; i++) {
            var render = this.playerRenderLst[i];
            render.updateQiangzhuangCircleViewStatus(false);
            render.updateQiangzhuangMultipleViewStatus(false);

            render.updateFinishedViewStatus(false);
            render.updateReadyViewStatus('');

        }
        for (var i = 0; i < data.pinshiPlayerList.length; i++) {
            var render = this.getPlayerRenderById(data.pinshiPlayerList[i].playuser);
            if (render) {
                if (data.pinshiPlayerList[i].hogtimes == 0) {
                    if (data.pinshiPlayerList[i].banker) {
                        render.updateDingMultiple(data.bankertimes);
                    } else {
                        render.updateQiangMultiple(data.bankertimes);
                    }
                } else {
                    if (data.pinshiPlayerList[i].banker) {
                        render.updateDingMultiple(data.pinshiPlayerList[i].hogtimes);
                    } else {
                        render.updateQiangMultiple(data.pinshiPlayerList[i].hogtimes);
                    }
                }
                render.updateFinishedViewStatus(false);
                render.updateQiangzhuangCircleViewStatus(true);
                render.playQiangzhuangAnimation();
            }
        }
        this.scheduleOnce(function () {
            debugger
            for (var i = 0; i < data.pinshiPlayerList.length; i++) {
                var render = this.getPlayerRenderById(data.pinshiPlayerList[i].playuser);
                if (render && render.getPlayerId() != data.banker) {
                    render.updateZhuangViewStatus(false);
                    render.updateQiangzhuangCircleViewStatus(false);
                    render.updateQiangzhuangMultipleViewStatus(false);

                } else {
                    if (render) {
                        render.updateZhuangViewStatus(true);
                        render.updateDingMultiple();
                    }
                }
            }
            if (data.banker != cc.mile.user.id) {
                this.updateStatusTipAndClock('bet', 'N', data.betsnd);
            } else {
                //this.updateTiplabel(status_tips.waitting_bet);
                this.updateStatusTipAndClock('bet', 'Y', data.betsnd);
                //this.updateTiplabel(status_tips.waitting_player_bet);

            }
            debugger
            //不是庄家才有下注分数显示
            //if (data.banker != cc.mile.user.id) {
            var baseScore = data.basescore;
            var baseScoreItems = baseScore.split("/");
            this.game.loadScorebut(baseScoreItems.join(","), data.pushscore);
            // }
        }, 0.5);
    },
    /**
     * 坐下房间
     *
     * @param data
     * @param context
     */
    roomsit_event: function (data, context) {
        cc.log("roomsit_event [" + data + "]");
        context.updateRoomSitStatus(data);
    },
    updateRoomSitStatus: function (data) {

    },

    /**
     *
     * 玩家点击 准备按钮
     * @param data
     * @param context
     */
    gamestart_event: function (data, context) {
        cc.log("gamestart_event [" + JSON.stringify(data) + "]");
        //cc.mile.gamestatus = "ready";
        context.updateGamestartStatus();

    }, /**
     *
     * 玩家点击 准备按钮
     * @param data
     * @param context
     */
    playerready_event: function (data, context) {
        cc.log("gamestart_event [" + JSON.stringify(data) + "]");
        //cc.mile.gamestatus = "ready";

        context.updatePlayerReadyStatus(data);
    },
    updatePlayerReadyStatus: function (data) {
        var playerRender = this.getPlayerRenderById(data.userid);
        if (playerRender) {
            playerRender.clearPlayerStatus();
            playerRender.clearScoreNode();
            //playerRender.updateiangMultipleViewStatus(false);
            if (data.userid == cc.mile.user.id) {
                this.updatebetScoreViewStatus(false);
            } else {
                playerRender.updatePushCountViewStatus(false);
            }
            ;
            playerRender.updateReadyViewStatus('ready');
            playerRender.updateQiangzhuangCircleViewStatus(false);
            playerRender.updateQiangzhuangMultipleViewStatus(false);
        }
        if (data.userid == cc.mile.user.id) {
            var pokers = this.getCommon("niuniuPoker");
            pokers.clearPlayerStatus();
            this.game.updateCuopaiBrightBtnStatus(false, false);
            if (this.stepcleanFlag) {
                this.updateRoomBtnStatus('owner', 'play');
            }
        }
        if (data.userid == cc.mile.user.id) {

            this.updateStatusTipAndClock('begin', 'Y', data.beginsnd);
        }
        //this.updateTiplabel(status_tips.waitting_ready);
        //this.showBackTime(data.beginsnd);
    },
    updateGamestartStatus: function () {
        for (var i = 0; i < this.playerRenderLst.length; i++) {
            this.playerRenderLst[i].updateReadyViewStatus('play');
        }
        this.updateRoomBtnStatus('owner', 'play');
    },


    /**
     * 初始化玩家信息
     *
     * @param data
     * @param context
     */
    players_event: function (data, context) {
        debugger
        cc.log("players_event : " + data);

        //坐下加载其他用户（后面接口通了需要去掉）
        cc.mile.maxplayers = data.maxplayers;
        for (var i = 0; i < data.player.length; i++) {
            if (data.player[i].id == cc.mile.user.id) {
                context.selfIndex = data.player[i].index;
            }
        }

        cc.log("relationIndex:" + cc.mile.maxplayers);
        for (var i = 0; i < data.player.length; i++) {
            if (data.player[i].gamestatus != 'watch') {
                context.initPlayerView(data.player[i], false);
            } else {
                context.clearPlayerView(data.player[i]);
            }
        }


    },

    getPlayerPosition: function (relationIndex) {
        debugger
        if (relationIndex == "NaN") {
            relationIndex = 3;
        }
        var x = 0, y = 0;
        switch (relationIndex) {
            case 0:
                x = 510, y = -128;
                break;
            case 1:
                x = 300, y = 60;
                break;
            case 2:
                x = 0, y = 100;
                break;
            case 3:
                x = -300, y = 60;
                break;
            case 4:
                x = -510, y = -128;
                break;
        }
        return {x: x, y: y};
    },
    /**
     * 判断玩家是否在界面已经初始化
     * @param playerId
     * @returns {boolean}
     */
    playerexist: function (playerId) {
        var inroom = false;
        if (player.id == cc.mile.user.id) {
            inroom = true;
        } else {
            for (var j = 0; j < this.playerRender.length; j++) {
                if (this.player[j].userid == playerId) {
                    inroom = true;
                    break;
                }
            }
        }
        return inroom;
    },
    /**
     *
     * 发送消息给服务端
     *
     source，源用户ID
     target，目标用户ID
     msgtype，消息类型 0，快捷语，1.语音，2.表情，4.文字消息
     sendtype，发送类型 0.单个发送，1.群发
     msgid，消息ID
     msgname，消息名称
     msgcontent，消息内容
     memo，消息说明
     *
     * @param msg
     */
    sendmsg: function (msg) {
        debugger
        msg.token = cc.mile.authorization;
        this.emitSocketRequest("sendmsg", JSON.stringify(msg));
        this.testSendCoin();
    },
    /**
     * 有用户离开时间
     * @param data
     * @param context
     */
    leave_event: function (data, context) {
        cc.log(data);

        if (data.userid == cc.mile.user.id) {
            context.eventLeaved = true;
            if (cc.mile.teahouse) {
                context.scene("floor", context);
            } else {
                context.scene("mazihall", context);
            }
        } else {
            var render = context.getPlayerRenderById(data.userid);
            if (render != null) {
                for (var i = 0; i < context.playerRenderLst.length; i++) {
                    if (render.getPlayerId() == context.playerRenderLst[i].getPlayerId()) {
                        context.playerRenderLst.splice(i, 1);
                        break;
                    }

                }
                render.node.destroy();
            }

        }
    },
    /**
     * 每小局的结算事件处理
     * @param data
     * @param context
     *
     *
     */
    stepclean_event: function (data, context) {
        cc.log("stepclean_event : " + data);
        context.dealingData = null;
        context.stepcleanFlag = true;
        context.stepcleanAction(data);
        context.updateTiplabel(status_tips.waitting_ready);
        context.updateRoomBtnStatus('normal', 'ready');
    },

    stepcleanAction: function (data) {
        var banker = data.banker;
        var bankerRender = this.getPlayerRenderById(banker);
        var hasLoser = false;
        var hasWinner = false;
        var delayTime = 0.5;
        var playVoice = "";
        var voiceName = "tongsha.mp3";
        var voicePath = "niuniu/" + this.getNiuNiuVociePathPrefix() + "/"

        //  先结算输家
        for (var i = 0; i < data.stepEvents.length; i++) {
            var playerItem = data.stepEvents[i];
            var playerRender = this.getPlayerRenderById(playerItem.userid);
            if (playerItem.userid != banker) {
                if (playerItem.stepscore < 0) {
                    hasLoser = true;
                    this.stepcleanCoinAnimation(bankerRender, playerRender, playerItem, banker);
                }
            }
        }

        if (!hasLoser) {
            delayTime = 0.01;
            voiceName = "tongpei.mp3";
        }

        //  延迟 结算赢家
        this.scheduleOnce(function () {
            for (var i = 0; i < data.stepEvents.length; i++) {
                var playerItem = data.stepEvents[i];
                var playerRender = this.getPlayerRenderById(playerItem.userid);
                if (playerItem.userid != banker) {
                    if (playerItem.stepscore > 0) {
                        hasWinner = true;
                        this.stepcleanCoinAnimation(bankerRender, playerRender, playerItem, banker);
                    }
                } else {

                    playerRender.updatePlayerTotalScore(playerItem.totalscore);
                    playerRender.showSettlementCoin(playerItem.stepscore);
                }
            }


        }, delayTime);

        //  延迟 结算赢家
        this.scheduleOnce(function () {

            if (!hasLoser || !hasWinner) {
                cc.mile.audio.playSFX(voicePath + voiceName);
            }


        }, 1.5);


    },
    stepcleanCoinAnimation: function (bankerRender, playerRender, playerItem, banker) {
        playerRender.sendSettlementCoin(
            bankerRender.getPlayerRelationPositionX(),
            bankerRender.getPlayerRelationPositionY(),
            playerItem.stepscore, banker
        );
        playerRender.updatePlayerTotalScore(playerItem.totalscore);
    },

    /**
     * 每大局的结算信息
     * @param data
     * @param context
     */
    clean_event: function (data, context) {
        cc.log("clean_event : " + data);
        context.stepcleanFlag = false;
        context.game.showJiesuanDialog(data);
        cc.mile.gamestatus = "notready"
        context.scheduleOnce(function () {
            if (cc.mile.createroomtype != 'coin') {
                this.emitJoinEvent();
            }
        }, 2);
        //context.game.showJiesuanDialog(data);
    },
    /**
     * 离线 事件监听
     * @param data
     * @param context
     */
    roomlinestatus_event: function (data, context) {
        cc.log("data.linestatus [" + data + "]");
        if (data.userid) {
            var render = context.getPlayerRenderById(data.userid);
            if (render) {
                render.updateOfflineViewStatus(data.online);
            }
            if (data.userid == cc.mile.user.id && data.online) {

                if (cc.mile.extparams && cc.mile.extparams.playway) {

                    var teanum = "";
                    var tablenum = "";
                    if (cc.mile.teahouse) {
                        teanum = cc.mile.teahouse.teanum;
                    }
                    if (cc.mile.tablenum) {
                        tablenum = cc.mile.tablenum;
                    }
                    var param = {
                        token: cc.mile.authorization,
                        playway: cc.mile.extparams.playway,
                        orgi: cc.mile.user.orgi,
                        extparams: cc.mile.extparams,
                        teanum: teanum,
                        tablenum: tablenum,
                    };
                    context.emitSocketRequest("recovery", JSON.stringify(param));
                }
            }
        }
    },
    /**
     *
     * @param data
     * @param context
     */
    teadissroom_event: function (data, context) {
        cc.log("data.linestatus [" + data + "]");
    },
    /**
     * 消息监听
     * @param data
     * @param context
     */
    sendmsg_event: function (data, context) {
        debugger
        cc.log("data.cardroom [" + data + "]");
        if (data.msgtype == '1' && data.sendtype == '0') {
            context.playVoiceMsg(data);
        } else if (data.msgtype == '2' && data.sendtype == '0') {
            context.sendChatAnimation(data);
        } else if ((data.msgtype == '1' || data.msgtype == '4') && data.sendtype == '1') {
            context.playFasticoMsg(data, context);
        } else if (data.msgtype == '5' && data.sendtype == '1') {
            context.playSelfEmojiAnimation(data);
        }
    },
    playSelfEmojiAnimation: function (data) {

        var render = this.getPlayerRenderById(data.source);
        if (render != null) {
            render.playUserEmojAnimation(data.msgcontent, data.msgcontent);
        }
    },
    /**
     * 发送语音表情动画
     * @param data
     */
    sendChatAnimation: function (data) {

        var playerSource = this.getUserPositionById(data.source);
        if (playerSource) {

            var chatAnimation = this.getCommon("ChatAnimation");
            var playerTarget = this.getUserPositionById(data.target);
            if (playerTarget == null) {
                playerTarget = this.selfPosition;
            }
            chatAnimation.setSourcePosition(playerSource);
            chatAnimation.setTargetPosition(playerTarget);
            chatAnimation.sendPlayAnimation(data.source, data.msgid);
        }
    },
    //快捷语音
    playFasticoMsg: function (data, context) {
        var render = this.getPlayerRenderById(data.source);
        if (render != null) {
            if (data.msgname != "") {
                cc.mile.audio.playSFX(data.msgname);
            }
            var width = data.msgcontent.length * 25;
            width = width > 100 ? width : 100;
            var _x = 0;
            if (width > 100) {
                _x = (width - 100) / 2;
            }
            var messageX = -34;
            render.message.setPositionX(messageX + _x);
            render.message.width = width;
            render.messageLab.node.width = width - 20;
            render.onOpenMessage(data.msgcontent);

        }
    },
    // 发送语音
    playVoiceMsg: function (data) {

        cc.log('playVoiceMsg:' + JSON.stringify(data));

        if (data.source == cc.mile.user.id) {
            this.stopAllUserVoiceAnimationStatus(false);
        }
        VoiceOnLineUtils.playVoiceMsg(data);

    },

    gvoiceDownloadCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {

            //maziDataBind.sendMsgByUploadFileId(data.fileID);
            var sourceid = VoiceOnLineUtils.onlineVoiceData[data.fileID];
            if (sourceid) {
                niuniuBeginSelf.updateUserVoiceAnimationStatus(sourceid, true);
            }

        } else {
            if (data && data.callbackMsg) {
                niuniuBeginSelf.showCenterToast(data.callbackMsg);
            }
        }
        cc.log('下载回调数据:' + JSON.stringify(data));


    },

    gvoicePlayFinishedCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {
            var sourceid = VoiceOnLineUtils.onlineVoiceData[data.fileID];
            if (sourceid) {
                niuniuBeginSelf.updateUserVoiceAnimationStatus(sourceid, false);
            }

        } else {
            if (data && data.callbackMsg) {
                niuniuBeginSelf.showCenterToast(data.callbackMsg);
            }
        }

        VoiceOnLineUtils.onVoicePlayFinished(data);
    },

    // 更新语音 animation 的状态
    updateUserVoiceAnimationStatus: function (targetUserId, status) {
        var render = this.getPlayerRenderById(targetUserId);
        render.updateVoiceAnimationStatus(status);
    },
    // 更新语音 animation 的状态
    stopAllUserVoiceAnimationStatus: function (status) {

        /* if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
             jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                 "stopPlayVoice", "()V");
         } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
             jsb.reflection.callStaticMethod("AppController", "stopPlayVoice");
         }*/

        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            render.updateVoiceAnimationStatus(status)
        }
        this.selfPlayerRender.updateVoiceAnimationStatus(status);
    },
    /**
     * 接收到服务端的 恢复牌局的数据 恢复牌局
     * @param data
     * @param context
     */
    recovery_event: function (data, context) {
        debugger
        //恢复自己的牌
        cc.log("recovery_event --11:" + data);
        context.game.initGameInfo(data.userboard);

        if (data.player && data.player.playuser == cc.mile.user.id) {
            cc.log("recovery_event 解析玩家牌--22:" + data);
            cc.mile.nowgamenum = data.userboard.nowgamenum;
            cc.mile.startroomid = data.player.roomid;

            //context.game.loadBetbut(4);
            context.initSelfData(data.userboard.player);

            if (context.waittimer != null && context.waittimer._sgNode != null) {
                var timer = context.waittimer.getComponent("MileTimer");
                if (timer) {
                    timer.stop(context.waittimer);
                }
            }


            context.closeOpenWin();

            cc.log("recovery_event 恢复玩家状态--44:");
            context.recover_game_status(data); // 恢复玩家状态

        }


    },
    recover_game_status: function (data) {
        debugger
        cc.log("recover_game_status --11:" + data);
        var selfPlay = data.player;


        cc.log("recover_game_status --恢复自己的按钮状态--");
        this.recover_selfoperator_status(selfPlay);

        //this.updateUserStatus(selfPlay.playuser, selfPlay.gamestatus);

        cc.log("recover_game_status --恢复自己的头像及得分--");
        this.recover_singleplay_status(selfPlay, true);


        cc.log("recovery_event 恢复桌面牌--55:");
        this.recover_desk_cards(selfPlay); // 恢复桌面的牌


        cc.log("recover_game_status 111--33:" + data);


        var otherPlays = data.userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            if (otherPlays[i].playuser) {
                otherPlays[i].id = otherPlays[i].playuser;
            }
            this.initPlayerView(otherPlays[i], true);
            this.recovery_player_cards(otherPlays[i]);
        }

        cc.log("recover_game_status --33:" + data);

        if (selfPlay.gamestatus == 'play') {// 只有为 游戏中的状态
            cc.log("recover_game_status --44:" + data);
            this.inviteplayerContainer.active = false;

        }

    },

    recovery_player_cards: function (playdata) {
        var gamestep = playdata.gamestep;
        if (gamestep) {
            var playRender = this.getPlayerRenderById(playdata.playuser);
            if (gamestep == 'show' && playdata.showflag == 'Y') {
                var cardType = playdata.pinshicardtype;
                cardType.lastcard = playdata.lastcard;
                playRender.OpenCrad(playdata.playuser, cardType);

            } else {
                debugger
                playRender.clearCards();
                debugger
                for (var i = 0; i < 4; i++) {
                    playRender.HairCrad(i);
                }
            }
        }
    },

    getRecoveryNextPlayerIndex: function (data) {
        var nextplayerId = data.nextplayer;
        var otherPlays = data.userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            if (otherPlays[i].playuser == nextplayerId) {
                return otherPlays[i].index;
            }
        }
        return data.player.index;
    },

    /**
     * 恢复单个用户的状态
     * @param playdata
     */
    recover_singleplay_status: function (playdata) {
        debugger;
        var playRender = this.getPlayerRenderById(playdata.playuser);
        if (playRender) {

            if (playdata.hasOwnProperty('banker') && playdata.banker) {
                playRender.updateZhuangViewStatus(true);
                playRender.updateQiangzhuangCircleViewStatus(true);
                if (playdata.hogtimes > 0) {
                    playRender.updateDingMultiple(playdata.hogtimes);
                } else {
                    playRender.updateDingMultiple(1);
                }
                this.currentBanker = playdata.playuser;
            } else {

                playRender.updateQiangzhuangCircleViewStatus(false);
                playRender.updateQiangzhuangMultipleViewStatus(false);
                playRender.updateZhuangViewStatus(false);
            }
            if (playdata.hasOwnProperty('totalscore')) {

                playRender.updatePlayerTotalScore(playdata.totalscore);
            } else {
                playRender.updatePlayerTotalScore(0);
            }
            if (playdata.pushscore > 0) {
                playRender.updatePushDescLabel(playdata.pushscore);
            }

            if (playdata.bettimes > 0) {
                var index = playRender.getRelationIndex();
                if (playdata.playuser == cc.mile.user.id) {
                    this.updatebetScoreViewStatus(true);
                    this.BetAnimation(playdata.bettimes);
                } else {
                    playRender.updateCoinMultiple(index, playdata.bettimes);
                }
                playRender.updateFinishedViewStatus(false);
                playRender.updatePushContainerNodeViewStatus(false);
            }
            //playRender.updateQiangMultiple(playdata.hogtimes);
            playRender.updateOfflineViewStatus(playdata.online);

            if (!playdata.gamestep) {
                playRender.updateReadyViewStatus(playdata.gamestatus);
            }
        }
    },
    /**
     * 恢复单个用户的状态
     * @param playdata
     */
    recover_selfoperator_status: function (playdata) {
        debugger
        var gamestep = playdata.gamestep;
        cc.log("恢复牌局的玩家状态：" + gamestep + "----gamestatus："
            + playdata.gamestatus + "----startflag：" + playdata.startflag);
        if (gamestep) {

            //this.updateRoomBtnStatus('normal', 'play');
            if (gamestep == 'show') {
                this.updateStatusTipAndClock(gamestep, playdata.showflag, playdata.showsnd);
                if (playdata.showflag != 'Y') {
                    this.game.updateCuopaiBrightBtnStatus(false, true);
                } else {
                    this.game.updateCuopaiBrightBtnStatus(false, false);

                }
            } else if (gamestep == 'hog') {
                this.updateStatusTipAndClock(gamestep, playdata.hogflag, playdata.hogsnd);
                if (playdata.hogflag != 'Y') {
                    this.game.loadBetbut(4);
                }
            } else if (gamestep == 'bet') {
                debugger
                if (playdata.banker) {
                    //playdata.betflag="Y";
                }
                this.updateStatusTipAndClock(gamestep, playdata.betflag, playdata.betsnd);
                if (playdata.betflag != 'Y') {
                    //不是房主才有下注分数显示
                    this.game.loadScorebut("1,2", playdata.pushscore);
                }
            }
            /*
              SHOW,    //亮牌时间
              BEGIN,//准备时间
              BET,//下注时间
              HOG;//抢庄
            if (gamestep == 'show') {
                this.game.loadBetbut(4);
            }*/
        } else {
            gamestep = 'begin';
            if (cc.mile.nowgamenum != "0") {
                this.updateStatusTipAndClock(gamestep, playdata.gamestatus, playdata.beginsnd);
            } else {
                this.updateStatusTipAndClock(gamestep, "N", playdata.beginsnd);
            }
            if (playdata.playuser) {
                playdata.id = playdata.playuser;
            }
            if (playdata.id == cc.mile.user.id) {

                this.updateRoomBtnStatus('owner', playdata.gamestatus);
            } else {

                this.updateRoomBtnStatus('normal', playdata.gamestatus);
            }


        }
    },
    /**
     * 恢复自己的牌
     * @param mycards
     */
    recover_desk_cards: function (playdata) {
        debugger
        var mycards = this.decode(playdata.cards);
        var gamestep = playdata.gamestep;
        var pokers = this.getCommon("niuniuPoker");

        if (mycards && mycards.length > 0) {
            if (!gamestep || (gamestep == 'show' && playdata.showflag == 'Y')) {
                var cardType = playdata.pinshicardtype;
                cardType.lastcard = playdata.lastcard;
                pokers.showCards(cardType);
            } else {
                pokers.loadCards(mycards, mycards[mycards.length - 1]);
            }
        }

    },

    /**
     * 清除自己打出去的牌
     */
    clearSelfCard: function () {
        var maziPoker = this.getCommon("MaziPoker");
        var countp = maziPoker.cunpoker.children.length;
        for (var i = 0; i < countp; i++) {
            maziPoker.cunpoker.removeChild(maziPoker.cunpoker.children[0]);
        }
        //for (var i = countp - 1; i >= 0; i--) {

        //}
    },

    /**
     * 接收到服务端的 出牌提示信息
     * @param data
     * @param context
     */
    cardtips_event: function (data, context) {
        cc.log("data [" + data + "]");
        cc.log("context [" + context + "]");
        if (data.tipcards && data.tipcards.length > 0) {
            context.game.tipcards = data.tipcards;
            context.game.tipIndex = 0;
            context.game.cardtips();
        } else {
            if (!context.playAutomic) { // 如果为必须要出牌的情况，不能自动跳过
                context.game.buchuBtnClick();
            }
        }
    },

    /**
     * 接收到服务端的 推送的 开始游戏的玩家数据
     * @param data
     * @param context
     */
    play_event: function (data, context) {
        debugger
        cc.log("---play_event:==" + JSON.stringify(data));
        context.currentBanker = null;
        if (context.inviteplayerContainer != null) {
            context.inviteplayerContainer.destroy();
        }
        cc.mile.gamestatus = data.gamestatus;
        cc.mile.nowgamenum = data.nowgamenum;
        context.updateRoomBtnStatus('owner', 'play');
        context.sendCards(data);
        context.updateStatusTipAndClock('hog', 'N', data.hogsnd);
        //context.updateTiplabel(status_tips.waitting_score);
        //context.showBackTime(data.hogsnd);

    },
    sendCards: function (data) {

        var mycards = this.decode(data.player.cards);
        var poker = this.getCommon("niuniuPoker");
        cc.log("---sendCards:==" + mycards);
        poker.loadCards(mycards, -1);
        cc.log("---推注分:" + data.player.pushscore);
        if (data.player.pushscore) {
            var selfRender = this.getPlayerRenderById(data.player.playuser);
            selfRender.updatePushDescLabel(data.player.pushscore);
        }
        this.game.loadBetbut(4);
        this.sendPlayersCard(data);
    },

    sendPlayersCard: function (data) {
        for (var inx = 0; inx < this.playerRenderLst.length; inx++) {

            if (this.playerRenderLst[inx].getPlayerId()
                != cc.mile.user.id) {
                this.playerRenderLst[inx].clearCards();
                debugger
                for (var i = 0; i < 4; i++) {
                    this.playerRenderLst[inx].HairCrad(i);
                }
            }

            for (var p = 0; p < data.players.length; p++) {
                if (this.playerRenderLst[inx].getPlayerId() == data.players[p].playuser) {
                    if (data.players[p].pushscore) {
                        this.playerRenderLst[inx].updatePushDescLabel(data.players[p].pushscore);
                    } else {
                        this.playerRenderLst[inx].clearPlayerStatus();
                    }
                }
            }
        }
    },

    /**
     * //当前状态按钮（准备,坐下,开始游戏）

     startGameNode:{
            default: null,
            type: cc.Node
        },
     sitdownStartGameNode:{
            default: null,
            type: cc.Node
        },

     ownerSitdownNode: {
            default: null,
            type: cc.Node
        },
     normalSitdownNode: {
            default: null,
            type: cc.Node
        },

     playreadyBtn:{
            default: null,
            type: cc.Node
        },

     * @param role
     * @param status
     */
    updateRoomBtnStatus: function (role, status) {

        cc.log("更新用户操作状态" + role + "----" + status + "---nowgamenum--" + cc.mile.nowgamenum);

        if (role == 'owner') {
            this.normalSitdownNode.active = false;
            this.playreadyBtn.active = false;
            if ((status == 'watch' || status == 'waitting') && cc.mile.nowgamenum == '0') {
                //房主才能有开始游戏按钮
                if (cc.mile.creator == cc.mile.user.gameid) {
                    this.startGameNode.active = true;
                    this.ownerSitdownNode.active = true;
                    this.sitdownStartGameNode.active = false;
                } else {
                    this.normalSitdownNode.active = true;
                }
            } else if (status == 'ready') {
                this.startGameNode.active = false;
                this.ownerSitdownNode.active = false;
                if (cc.mile.nowgamenum == '0') {
                    //房主才能有开始游戏按钮
                    if (cc.mile.creator == cc.mile.user.gameid) {
                        this.sitdownStartGameNode.active = true;
                    }
                } else {
                    this.sitdownStartGameNode.active = false;
                }
            } else if (status == 'play') {
                this.startGameNode.active = false;
                this.ownerSitdownNode.active = false;
                this.sitdownStartGameNode.active = false;
            } else if (cc.mile.nowgamenum != '0') {
                this.playreadyBtn.active = true;
            }
        } else {
            this.startGameNode.active = false;
            this.ownerSitdownNode.active = false;
            this.sitdownStartGameNode.active = false;
            this.playreadyBtn.active = true;
            if (status == 'watch') {
                this.normalSitdownNode.active = true;
            } else if (status == 'ready') {
                this.normalSitdownNode.active = false;
            }
        }
    },
    getUserPositionById: function (userid) {
        for (var i = 0; i < this.playerRenderLst.length; i++) {
            var render = this.playerRenderLst[i];
            if (render.userid == userid) {
                return render.userPosition;
            }

        }
        return null;
    },

    getPlayerRenderById: function (userid) {

        cc.log("查找render的ID:" + userid);

        for (var i = 0; i < this.playerRenderLst.length; i++) {
            var render = this.playerRenderLst[i];
            cc.log("遍历render的ID:" + render.getPlayerId());
            if (render.getPlayerId() == userid) {
                return render;
            }
        }
        return null;
    },
    onCloseClick: function () {
        this.continuegamebtn.active = true;
    },

    statictimer: function (message, time) {
        this.waittimer = cc.instantiate(this.waitting);
        this.waittimer.parent = this.root();

        var timer = this.waittimer.getComponent("MileTimer");
        if (timer) {
            timer.init(message, time, this.waittimer);
        }
    },
    onDestroy: function () {
        this.cleanmap();

        cc.gvoicePlayFinishedCallbackJS = null;
        cc.gvoiceDownloadCallbackJS = null;
        cc.gvoiceUploadCallbackJS = null;

        cc.mile.gamestatus = "notready";
        if (!this.eventLeaved) {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            this.emitSocketRequest("disconnect", JSON.stringify(param));
        }
        if (cc.mile.socket && cc.mile.socket.off) {
            cc.mile.socket.off("command_room");
        }

        if (cc && cc.eventManager) {
            cc.eventManager.removeCustomListeners("socketReconnect");
        }
        cc.mile.maxplayers = null;
        cc.mile.curpalyers = null;

    },

    dissroom_event: function (data, context) {
        debugger;
        if (data.dissflag == 1) {
            context.game.agreeDissroom(data);
        } else if (data.dissflag == 2) {
            context.game.disAgreeDissroom(data);
        } else {
            context.game.showJiesanDialog(data);
        }
    },
    createroom_event: function (data, context) {
        cc.log('createroom_数据出错：' + JSON.stringify(data))
        var data = context.parse(data);
        context.showCenterToast(data.context);
        context.scheduleOnce(function () {
            this.scene("mazihall", this);
        }, 3);
    },
    //自己的下注动画
    BetAnimation: function (score) {
        debugger
        var mv = cc.moveTo(0.5, -30, 0).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(0.5, 1, 1);
        this.betScore_bg.children[0].runAction(cc.spawn(mv, scale));
        var slf = this;
        this.betScore_bg.children[1].removeAllChildren();
        this.scheduleOnce(function () {
            GameUtil.showScoreNode(score, this.betScore_bg.children[1]);
        }, 0.4);
        cc.mile.audio.playSFX("niuniu/random_banker.mp3");
    },
    //下注分数背景
    updatebetScoreViewStatus: function (status) {
        this.betScore_bg.active = status;
    },
});
