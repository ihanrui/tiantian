var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        setting: {
            default: null,
            type: cc.Prefab
        },
        chat: {
            default: null,
            type: cc.Prefab
        },
        voiceChat: {
            default: null,
            type: cc.Button
        }
    },

    // use this for initialization
    onLoad: function () {
        if(this.voiceChat != null){

        }
    },
    back:function(){
        this.confirm('确认退出吗？',function(self) {
            self.loadding();
            setTimeout(function () {
                self.scene(cc.mile.gametype, self);
                self.closeConfirm();
            }, 500)
        });
    },
    onSettingClick:function(){
        cc.mile.openwin = cc.instantiate(this.setting) ;
        cc.mile.openwin.parent = this.root();

    },
    onChatClick:function(){
        cc.mile.openwin = cc.instantiate(this.chat) ;
        cc.mile.openwin.parent = this.root();

    },
    onVoiceChatClick: function () {
        this.onVoiceChatStart();
        var self = this;
        setTimeout(function () {
            self.onVoiceChatStop();
        },10000);
    },
    onVoiceChatStart: function () {
        if(cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "startVoice", "()V");
        }
    },
    onVoiceChatStop: function () {
        if(cc.sys.os == cc.sys.OS_ANDROID){

            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "stopVoice", "()V");
        }
    }
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
