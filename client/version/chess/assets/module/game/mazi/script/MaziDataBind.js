var beiMiCommon = require("MileCommon");
var maziDataBind = null;
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        //语音背景
        discourse: {
            default: null,
            type: cc.Node
        },
        //语音提示语句
        discourseLabel: {
            default: null,
            type: cc.Label
        },
        sound_recording_btn: {
            default: null,
            type: cc.Node
        },
        sound_recording_animation: {
            default: null,
            type: cc.Animation
        },
        sound_recording_container: {
            default: null,
            type: cc.Node
        },
        sound_recording_cancel: {
            default: null,
            type: cc.Node
        },
        feijiNode: {
            default: null,
            type: cc.Node
        },
        feiji: {
            default: null,
            type: sp.Skeleton
        },
        //文字语音面板
        newchatbg: {
            default: null,
            type: cc.Node
        },
        //查看剩余分数
        newchatbg_writing: {
            default: null,
            type: cc.Node
        },
        //聊天输入框
        EnterContent: {
            default: null,
            type: cc.EditBox
        },
        //点击用户头像弹出
        userinfoDialog: {
            default: null,
            type: cc.Prefab
        },
        roomidLabel: {
            default: null,
            type: cc.Label
        },
        roomNumbersLabel: {
            default: null,
            type: cc.Label
        },
        gameTimeLabel: {
            default: null,
            type: cc.Label
        },
        gameDescLabel: {
            default: null,
            type: cc.Label
        },
        //解散
        maziJiesanDialog: {
            default: null,
            type: cc.Prefab
        },
        //投降
        giveupDialog: {
            default: null,
            type: cc.Prefab
        },
        //结算
        maziJiesuanDialog: {
            default: null,
            type: cc.Prefab
        },//报喜
        baoxiDialog: {
            default: null,
            type: cc.Prefab
        },
        seeScoreDialog: {
            default: null,
            type: cc.Prefab
        },
        seeCardsDialog: {
            default: null,
            type: cc.Prefab
        },
        seeCardsNode: {
            default: null,
            type: cc.Node
        },
        splash: {
            default: null,
            type: cc.Node
        },
        chupaiBtn: {
            default: null,
            type: cc.Node
        },
        tishiBtn: {
            default: null,
            type: cc.Node
        },
        benlunfenshuLabel: {
            default: null,
            type: cc.Label
        },
        testParamsPrefab: {
            default: null,
            type: cc.Prefab
        },
        gpsPrefab: {
            default: null,
            type: cc.Prefab
        },
        set: {
            default: null,
            type: cc.Prefab
        },
        lipaiBtn: {
            default: null,
            type: cc.Node
        },
        huifuBtn: {
            default: null,
            type: cc.Node
        },
        mingpaiBtn: {
            default: null,
            type: cc.Node
        },
        yimingaiBtn: {
            default: null,
            type: cc.Node
        },
        playOptionsContainer22: {
            default: null,
            type: cc.Node
        },
    },


    // use this for initialization
    onLoad: function () {

        maziDataBind = this;
        cc.gvoiceUploadCallbackJS = this.gvoiceUploadCallbackJS;
        this._registerEvent();

        this.settledcardsFlag = true;
        this.baoxiCount = 0;
        this.updateGameTime();
        this.schedule(function () {
            this.updateGameTime();
        }, 60, 100, 60);

        this.preClickTime = 0;
        this.nextClickTime = 0;

        if (cc.mile.extparams) {
            if (cc.mile.extparams['remembercardflag'] == 'yes') {
                this.seeCardsNode.active = true;
            } else {
                this.seeCardsNode.active = false;
            }

        } else {
            this.seeCardsNode.active = false;
        }

    },

    _registerEvent: function () {

        this.sound_recording_btn.on(cc.Node.EventType.TOUCH_START, function (e) {
            console.log('poker TOUCH_START:' + e.getLocationX() + "---" + e.getLocationY());
            this.beginSoundRecording();
            this.beginRecordingPositionY = e.getLocationY();
            this.beginRecordingPositionX = e.getLocationX();
            e.stopPropagation();
        }, this);
        this.sound_recording_btn.on(cc.Node.EventType.TOUCH_MOVE, function (e) {
            console.log('poker TOUCH_MOVE:' + e.getLocationX() + "---" + e.getLocationY());
            if (this.sound_recording_status) {
                var positionY = e.getLocationY();
                var positionX = e.getLocationX();
                if (positionY - this.beginRecordingPositionY > 50) {
                    this.endSoundRecording();
                } else if (positionY - this.beginRecordingPositionY > 30) {
                    this.cancelSoundRecording();
                } else if (positionX - this.beginRecordingPositionX > 20
                    || positionX - this.beginRecordingPositionX < -20) {
                    this.endSoundRecording();
                }
            }
            e.stopPropagation();
        }, this);
        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）
        this.sound_recording_btn.on(cc.Node.EventType.TOUCH_END, function (e) {
            console.log('poker TOUCH_END');
            this.endSoundRecording();
            e.stopPropagation();

        }, this);

        //监听房间多层显示事件
        this.splash.on(cc.Node.EventType.TOUCH_START, function (event) {

            this.updateNewChatbgViewStatus(false);
            var beginTime = Date.now();
            cc.log('-----监听房间多层显示事件---' + beginTime);
            cc.log('-----监听房间多层显示事件---' + this.preClickTime);
            if (this.preClickTime > 0 && beginTime - this.preClickTime < 500) {
                this.preClickTime = 0;
                cc.log('-----双击事件来了---');
                this.getCommon("MaziPoker").clearSelectedCards();
            } else {

                this.preClickTime = beginTime;
            }
        }, this);
    },

    updateNewChatbgViewStatus: function (status) {
        if (this.newchatbg.active || this.newchatbg_writing.active) {
            this.newchatbg.active = status;
            this.newchatbg_writing.active = status;
        }
    },
    //点击语音面板
    onNewchatbgClick: function () {
        if (this.newchatbg.active) {
            this.newchatbg.active = false;
        } else {
            this.newchatbg.active = true;
            this.newchatbg_writing.active = false;
        }
    },
    //点击聊天输入
    onNewWritingchatbgClick: function () {
        if (this.newchatbg_writing.active) {
            this.newchatbg_writing.active = false;
        } else {
            this.newchatbg.active = false;
            this.newchatbg_writing.active = true;
        }
    },
    //点击看分
    onSeeScoresClick: function () {

        this.updateNewChatbgViewStatus(false);
        this.closeOpenWin();
        if (cc.mile.gamestatus == 'play') {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            this.emitSocketRequest("seescore", JSON.stringify(param));
        } else {
            this.showSeescoreDialog([]);
        }
        this.newchatbg.active = false;
    },
    onSeeCardsClick: function () {
        this.updateNewChatbgViewStatus(false);
        this.closeOpenWin();
        if (cc.mile.gamestatus == 'play') {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            this.emitSocketRequest("remembercard", JSON.stringify(param));
        } else {
            this.showSeeCardsDialog([]);
        }
        this.newchatbg.active = false;
    },
    //
    onSettlecardsClick: function () {
        this.updateNewChatbgViewStatus(false);
        if (cc.mile.gamestatus == 'play') {
            this.settledcardsFlag = !this.settledcardsFlag
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                settleFlag: this.settledcardsFlag + ""
            };
            this.emitSocketRequest("settlecards", JSON.stringify(param));
            this.lipaiBtn.active = this.settledcardsFlag;
            this.huifuBtn.active = !this.settledcardsFlag;
        } else {
            this.showCenterToast("游戏未开始");
        }
    },
    onSeecardsClick: function () {
        try {
            this.updateNewChatbgViewStatus(false);
            var pokers = this.getCommon("MaziPoker");
            var cardLength = pokers.getLeftCardCount();
            if (cardLength > 1 && cardLength <= 20) {
                var self = this;
                this.confirm('确认明牌吗？', function () {

                    var param = {
                        token: cc.mile.authorization,
                        orgi: cc.mile.user.orgi
                    };
                    self.emitSocketRequest("seecards", JSON.stringify(param));
                    self.closeConfirm();
                });
            } else {
                this.showCenterToast("开始游戏，且牌数小于20才能明牌!");
            }

        } catch (e) {
            this.loggerError('明牌出错：' + JSON.stringify(e) + e);
        }

    },
    showSeescoreDialog: function (userPlayCards, myCardsNodeList) {

        this.closeOpenWin();


        var seeScoreDialog = cc.instantiate(this.seeScoreDialog);
        cc.mile.openwin = seeScoreDialog;
        cc.mile.openwin.parent = this.root();
        var dialogScript = seeScoreDialog.getComponent("SeeScoreDialog");
        dialogScript.addSelectedCards(userPlayCards, myCardsNodeList);

    },
    showSeeCardsDialog: function (userPlayCards, myCardsNodeList) {

        this.closeOpenWin();
        this.closeOpenWin();

        var seeCardsDialog = cc.instantiate(this.seeCardsDialog);
        cc.mile.openwin = seeCardsDialog;
        cc.mile.openwin.parent = this.root();
        var dialogScript = seeCardsDialog.getComponent("SeeCardsDialog");
        dialogScript.addSelectedCards(userPlayCards, myCardsNodeList);

    },
    //快捷聊天点击事件
    onDiscourseClick: function (data) {
        //this.discourse.active = true;

        //获取到语音文字对象
        this.discourseLabel = this.discourse.children[0].getComponent(cc.Label);
        //语音文字内容初始化
        var message = "";
        //音频名称
        var audioName = "";

        //根据点击的控件名称匹配相应的语音文字
        switch (data.target._name) {
            case "btn_chupai":
                audioName = "mchat1.mp3";
                message = "快点出牌咯？";
                break;
            case "lab_S_01":
                audioName = "qd.mp3";
                message = "快点啊！我等到花儿都谢了！";
                break;
            case "lab_S_01_shenme":
                audioName = "shenmedongxida.mp3";
                message = "你用什么东西打！";
                break;
            case "lab_S_01_suibian":
                audioName = "suibianda.mp3";
                message = "随便打咯！";
                break;
            case "lab_S_01_duoshaofen":
                audioName = "youduoshaofen.mp3";
                message = "你还有多少分子！";
                break;
            case "lab_S_02":
                audioName = "qj.mp3";
                message = "接下来见证奇迹的时刻到了！";
                break;
            case "btn_zhengdi":
                audioName = "mchat2.mp3";
                message = "我正滴套！";
                break;
            case "btn_guisi":
                audioName = "mchat3.mp3";
                message = "鬼是死滴！";
                break;
            case "lab_S_03":
                audioName = "yq.mp3";
                message = "谁给你的勇气，梁静茹吗？";
                break;
            case "lab_S_04":
                audioName = "ts.mp3";
                message = "看我通杀全场，这些钱都是我的";
                break;
            case "btn_youwushik":
                audioName = "mchat4.mp3";
                message = "你有五十k么？";
                break;
            case "btn_jitao":
                audioName = "mchat5.mp3";
                message = "你有几套哦！";
                break;
            case "lab_S_05":
                audioName = "sg.mp3";
                message = "风水轮流转，底裤都输光";
                break;
            case "btn_dagui":
                audioName = "mchat6.mp3";
                message = "打滴鬼死么？";
                break;
            case "lab_S_06":
                audioName = "yz.mp3";
                message = "我的手气全靠颜值撑着";
                break;
            case "btn_youzheng":
                audioName = "mchat7.mp3";
                message = "你有正滴套吗？";
                break;
            case "lab_S_07":
                audioName = "zj.mp3";
                message = "我是庄家谁敢挑战我";
                break;
            case "lab_S_08":
                audioName = "xsn.mp3";
                message = "底牌亮出来，绝对吓死你！";
                break;
            case "lab_S_09":
                audioName = "hehe.mp3";
                message = "我就呵呵。。";
                break;
            case "lab_S_10":
                audioName = "lql.mp3";
                message = "大家一起浪起来";
                break;
            case "btn_newChat_yaosha":
                message = "你要么子哦？";
                audioName = "mchat18.mp3";
                break;
            case "btn_newChat_woguan":
                message = "你不好打，我来！";
                audioName = "mchat19.mp3";
                break;
            case "btn_newChat_niguan":
                message = "我打不起你打罗！";
                audioName = "mchat20.mp3";
                break;
            case "btn_newChat_guine":
                message = "你有几个鬼哦？";
                audioName = "mchat21.mp3";
                break;
            case "btn_newChat_woyou":
                message = "我有";
                audioName = "mchat22.mp3";
                break;
            case "btn_newChat_meiyou":
                message = "我没有";
                audioName = "mchat23.mp3";
                break;
            case "btn_newChat_shangfen":
                message = "往里面上分,我铁上！";
                audioName = "mchat24.mp3";
                break;
            case "btn_newChat_yizhang":
                message = "打一个的";
                audioName = "mchat11.mp3";
                break;
            case "btn_newChat_liangzhang":
                message = "打对子";
                audioName = "mchat12.mp3";
                break;
            case "btn_newChat_sanzhang":
                message = "打三个的";
                audioName = "mchat13.mp3";
                break;
            case "btn_newChat_sizhang":
                message = "打四个的";
                audioName = "mchat14.mp3";
                break;
            case "btn_newChat_liandui":
                message = "打两连对";
                audioName = "mchat15.mp3";
                break;
            case "btn_newChat_liandui3":
                message = "打三连对";
                audioName = "mchat16.mp3";
                break;
            case "btn_newChat_banzi":
                message = "你有板子冒罗？";
                audioName = "mchat17.mp3";
                break;
            default:
                break;
        }
        if (audioName != "") {

            audioName = "mazi/" + this.getSexPathPrefix() + "/" + audioName;
            //cc.mile.audio.playSFX(audioName);
            //this.discourseLabel.string = message;
            //发送消息的对象
            var param = {
                source: cc.mile.user.id,//源用户ID
                msgtype: 1,//消息类型 0，快捷语，1.语音，2.表情，4.文字消息
                sendtype: 1,//发送类型 0.单个发送，1.群发
                msgname: audioName,
                msgcontent: message //消息内容
            };

            var maziBegin = this.getCommon("MaziBegin");
            maziBegin.sendmsg(param);
            /*if (this.newchatbg.active) {
                this.newchatbg.active = false;
            }*/
            if (this.newchatbg_writing.active) {
                this.newchatbg_writing.active = false;
            }
        }

        //this.scheduleOnce(function () {
        //    this.discourse.active = false;
        //}, 2)

    },
    onSelfFaceClick: function (data) {
        //this.discourse.active = true;

        cc.log("发送自定义表情");

        //获取到语音文字对象
        this.discourseLabel = this.discourse.children[0].getComponent(cc.Label);
        //语音文字内容初始化
        var message = data.target._name;
        //音频名称
        var audioName = "";

        //根据点击的控件名称匹配相应的语音文字
        switch (data.target._name) {
            case "aixin":
                audioName = "aixin.mp3";

                break;
            case "shaoxiang":
                audioName = "shaoxiang.mp3";

                break;
            case "kaixin":
                audioName = "kaixin.mp3";

                break;
            case "langligelang":
                audioName = "langligelang.mp3";

                break;
            case "tuxue":
                audioName = "tuxue.mp3";

                break;
            case "shouqian":
                audioName = "shouqian.mp3";
                break;
            default:
                break;
        }

        if (audioName != "") {

            audioName = "mazi/" + this.getSexPathPrefix() + "/" + audioName;
            //cc.mile.audio.playSFX(audioName);
            //this.discourseLabel.string = message;
            //发送消息的对象
            var param = {
                source: cc.mile.user.id,//源用户ID
                msgtype: 5,//消息类型 0，快捷语，1.语音，2.表情，4.文字消息 5. 自己的表情
                sendtype: 1,//发送类型 0.单个发送，1.群发
                msgname: audioName,
                msgcontent: message //消息内容
            };

            var maziBegin = this.getCommon("MaziBegin");
            maziBegin.sendmsg(param);
            /*if (this.newchatbg.active) {
                this.newchatbg.active = false;
            }*/
            if (this.newchatbg_writing.active) {
                this.newchatbg_writing.active = false;
            }
        }

        //this.scheduleOnce(function () {
        //    this.discourse.active = false;
        //}, 2)

    },

    //发送聊天内容
    onSendEnterContent: function (data) {
        var message = this.EnterContent.string;
        this.EnterContent.string = "";
        //发送消息的对象
        var param = {
            source: cc.mile.user.id,//源用户ID
            msgtype: 4,//消息类型 0，快捷语，1.语音，2.表情，4.文字消息
            sendtype: 1,//发送类型 0.单个发送，1.群发
            msgname: "",
            msgcontent: message //消息内容
        };

        if (message.length > 20) {
            this.showCenterToast("发送消息不能大于20个字，请重新编辑后发送");
        }
        else if (message.length == 0) {
            this.showCenterToast("请输入内容后再点击发送");
        }
        else {
            var maziBegin = this.getCommon("MaziBegin");
            maziBegin.sendmsg(param);
        }

        //this.scheduleOnce(function () {
        //    this.discourse.active = false;
        //}, 2);

    },
    playFeiji: function () {

        this.feijiNode.active = true;
        this.feiji.setAnimation(0, "feiji2", false);
    },
    beginSoundRecording: function () {
        if (this.sound_recording_status != 3) {

            cc.audioEngine.pauseAll();

            this.sound_recording_status = 1;
            this.sound_recording_container.active = true;
            this.sound_recording_animation.play();
            this.onVoiceChatStart();

        } else {
            if (this.sound_recording_check_count > 0) {
                this.sound_recording_check_count--;
                this.sound_recording_status = 1;
            }
            cc.audioEngine.resumeAll();
            this.showCenterToast("语音发送中，请稍等！");

        }
    },
    endSoundRecording: function () {
        if (this.sound_recording_container.active
            || this.sound_recording_cancel.active) {
            this.sound_recording_animation.stop();
            this.sound_recording_container.active = false;
            this.sound_recording_cancel.active = false;
            this.onVoiceChatStop();
        }

        /*var pokers = this.getCommon("MaziPoker");


        pokers.clearSelectedCards();
        var currentTipCards = [66,23];
        for(var i = 0 ;i < currentTipCards.length ; i++){
            pokers.addSelectCard(currentTipCards[i]);
        }
        pokers.doSelectCard();
*/
    },
    cancelSoundRecording: function () {
        this.sound_recording_status = 2;
        this.sound_recording_container.active = false;
        this.sound_recording_animation.stop();
        this.sound_recording_cancel.active = true;
    },
    onVoiceChatStart: function () {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "startVoice", "()V");
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppController", "startRecord");
        }
    },
    onVoiceChatStop: function () {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "stopVoice", "(I)V", this.sound_recording_status);
            var isOpenVoice = jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "isOpenVoice", "()Z");
            if (!isOpenVoice) {
                this.showCenterToast('发送失败，请开启录音话筒权限!');
                return;
            }
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

            jsb.reflection.callStaticMethod("AppController",
                "stopVoice:", this.sound_recording_status);
            var isOpenVoice = jsb.reflection.callStaticMethod("AppController",
                "isOpenVoice");
            if (!isOpenVoice) {
                this.showCenterToast('发送失败，请开启录音话筒权限!');
                return;
            }
        }

        if (this.sound_recording_status == 1) {
            this.sound_recording_status = 3; // 发送中
            this.sound_recording_check_count = 1; // 发送中

        }
    },

    gvoiceUploadCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {

            maziDataBind.sendMsgByUploadFileId(data.fileID);
        } else {
            if (data && data.callbackMsg) {
                maziDataBind.showCenterToast(data.callbackMsg);
            }
        }
        maziDataBind.sound_recording_status = 1;

    },


    sendMsgByUploadFileId: function (uploadFileId) {
        var maziBegin = this.getCommon("MaziBegin");
        maziBegin.sendmsg({
            source: cc.mile.user.id,
            target: '',
            msgtype: '1',
            sendtype: '0',
            msgid: uploadFileId,
            msgname: '',
            msgcontent: '',
            mem: ''
        });
    },
    onUserInfoClick: function (pos, userInfo) {
        this.updateNewChatbgViewStatus(false);
        this.chatAnimation = this.getCommon("ChatAnimation");
        this.chatAnimation.setChoicePosition(pos);
        this.chatAnimation.setTargetUserid(userInfo.userid);

        var userinfoDialog = cc.instantiate(this.userinfoDialog);
        cc.mile.openwin = userinfoDialog;
        cc.mile.openwin.parent = this.root();
        var userShow = this.getPrefabCommon("userinfoshow", "userInfoShow");
        if (userShow) {
            userShow.initUserInfo(userInfo);
        }


    },
    back: function () {

        /*var pokers = this.getCommon("MaziPoker");

        pokers.loadcards([39,3,21,2,23,43,54,66,10,8,44],true);
        pokers.clearSelectedCards();
        var currentTipCards = [21,66,23];
        for(var i = 0 ;i < currentTipCards.length ; i++){
            pokers.addSelectCard(currentTipCards[i]);
        }
        pokers.doSelectCard();
        */


        this.updateNewChatbgViewStatus(false);

        if (cc.mile.gamestatus == 'play') {

            //this.alert("游戏已经开始，不能离开！");
            this.confirm('解散进行中的游戏？', function (self) {

                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi,
                    dissflag: 1,
                    firstflag: true,
                };
                self.emitSocketRequest("dissroom", JSON.stringify(param));
                self.closeConfirm();

            });


        } else {
            this.confirm('确认退出吗？', function (self) {

                var param = {
                    token: cc.mile.authorization,
                    orgi: cc.mile.user.orgi
                };
                self.emitSocketRequest("leave", JSON.stringify(param));

                self.loadding();
                self.closeConfirm();

            });
        }

    },
    showGpsDialog: function () {
        this.updateNewChatbgViewStatus(false);
        var gpsView = cc.instantiate(this.gpsPrefab);
        cc.mile.openwin = gpsView;
        cc.mile.openwin.parent = this.root();
        var jsRender = gpsView.getComponent("GPSPlayerDialogRender");
        jsRender.initView({});
    },
    showJiesanDialog: function (data, isrecovery) {
        this.updateNewChatbgViewStatus(false);

        var scriptNdoe = cc.find("Canvas/maziJiesanDialog/script/MaziJiesanDialog");
        if (scriptNdoe == null || isrecovery) {
            this.maziJiesanDialogObj = null;
            this.closeOpenWin();
        }

        this.scheduleOnce(function () {
            if (!this.maziJiesanDialogObj) {
                this.maziJiesanDialogObj = cc.instantiate(this.maziJiesanDialog);
                cc.mile.openwin = this.maziJiesanDialogObj;
                cc.mile.openwin.parent = this.root();
                var scriptNdoe = cc.find("Canvas/maziJiesanDialog/script/MaziJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("MaziJiesanDialog");
                dialogScript.initUserView(data.dissroomInfoList, data.userid, data.disssecond, "申请解散房间");
            } else {
                var scriptNdoe = cc.find("Canvas/maziJiesanDialog/script/MaziJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("MaziJiesanDialog");
                dialogScript.updatePlayerStatus(data.dissroomInfoList);
            }
        }, 0.2);

    },

    showGiveUpDialog: function (data, isrecovery) {
        this.updateNewChatbgViewStatus(false);
        var scriptNdoe = cc.find("Canvas/giveupDialog/script/MaziJiesanDialog");
        if (scriptNdoe == null || isrecovery) {
            this.maziGiveUpObj = null;
            this.closeOpenWin();
        }
        this.scheduleOnce(function () {


            if (!this.maziGiveUpObj) {
                this.maziGiveUpObj = cc.instantiate(this.giveupDialog);
                cc.mile.openwin = this.maziGiveUpObj;
                cc.mile.openwin.parent = this.root();
                var scriptNdoe = cc.find("Canvas/giveupDialog/script/MaziJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("MaziJiesanDialog");
                dialogScript.initUserGiveView(data.giveupInfoList, data.userid, data.giveupsecond, "申请投降");
            } else {
                var scriptNdoe = cc.find("Canvas/giveupDialog/script/MaziJiesanDialog");
                var dialogScript = scriptNdoe.getComponent("MaziJiesanDialog");
                dialogScript.updateGiveUpPlayerStatus(data.giveupInfoList);
            }
        }, 0.2);
    },
    //同意投降
    disGiveUpAgreeDissroom: function (data) {
        if (data.giveupflag == 0) {
            this.showCenterToast("投降超时，请重新申请投降!");
        } else {
            var userName = "";
            for (var i = 0; i < data.giveupInfoList.length; i++) {
                if (data.giveupInfoList[i].giveupflag == 2) {
                    userName = data.giveupInfoList[i].username;
                }
            }
            if (userName) {
                this.showCenterToast(userName + "拒绝投降!");
            }
        }
        this.maziGiveUpObj = null;
        this.closeOpenWin();
    },
    ///拒绝投降
    dissGiveUpDissroom: function (data) {

        var userName = "";
        for (var i = 0; i < data.giveupInfoList.length; i++) {
            if (data.giveupInfoList[i].giveupflag == 2) {
                userName = data.giveupInfoList[i].username;
            }
        }
        if (userName) {
            this.showCenterToast(userName + "拒绝投降!");
        }

        this.maziGiveUpObj = null;
        this.closeOpenWin();
    },

    disAgreeDissroom: function (data) {
        if (data.dealflag == 0) {
            this.showCenterToast("解散超时，请重新申请解散!");

        } else {

            var userName = "";
            for (var i = 0; i < data.dissroomInfoList.length; i++) {
                if (data.dissroomInfoList[i].dissflag == 2) {
                    userName = data.dissroomInfoList[i].username;
                }
            }
            if (userName) {
                this.showCenterToast(userName + "拒绝解散房间!");
            }
        }
        this.maziJiesanDialogObj = null;
        this.closeOpenWin();

    },
    agreeDissroom: function (data) {
        this.closeOpenWin();
        this.maziJiesanDialogObj = null;
        //this.scene("mazihall", this);
    },
    chupaiBtnClick: function () {

        if (this.playOptionsContainer22) {
            this.playOptionsContainer22.active = false;
        }
        var beginTime = Date.now();
        cc.log("出牌时间：" + beginTime);
        cc.mile.chupaiBegin = beginTime;

        var maziPoker = this.getCommon("MaziPoker");
        var pokers = maziPoker.selectedCards;
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            cards: pokers.join(","),
        };
        this.emitSocketRequest("takecards", JSON.stringify(param));

    },
    tishiBtnClick: function () {
        if (this.tipcards && this.tipcards.length > 0) {
            this.cardtips();

        } else {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
            };
            this.emitSocketRequest("cardtips", JSON.stringify(param));

        }
    },
    cardtips: function () {

        var currentTipCards = this.tipcards[this.tipIndex % this.tipcards.length];
        this.tipIndex++;
        cc.log(currentTipCards);
        var maziPoker = this.getCommon("MaziPoker");
        maziPoker.clearSelectedCards();
        for (var i = 0; i < currentTipCards.length; i++) {
            maziPoker.addSelectCard(currentTipCards[i]);
        }
        maziPoker.doSelectCard();
        //提示牌的界面展现


    },
    buchuBtnClick: function () {

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
        };
        this.emitSocketRequest("nocards", JSON.stringify(param));

        var maziPoker = this.getCommon("MaziPoker");
        //点击不出重新整理牌的位置
        maziPoker.Homing();
    },

    updateBenLunFenShuClick: function (score) {
        this.benlunfenshuLabel.string = "本轮桌面分数:" + score;
    },

    onBaoxiClick: function () {

        var socket = this.socket();

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi,
            newsflag: true,
        };
        this.emitSocketRequest("reportnews", JSON.stringify(param));

    },
    onBubaoxiClick: function () {

        this.confirm('放弃报喜，将收不到玩家的喜钱？', function (self) {

            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                newsflag: false,
            };
            self.emitSocketRequest("reportnews", JSON.stringify(param));

            self.closeConfirm();
        });

    },

    testbaoxi: function () {
        var cards = [54, 53, 52, 51, 50, 49, 48];
        cc.mile.openwin = cc.instantiate(this.baoxiDialog);
        cc.mile.openwin.parent = this.root();
        var baoxi = this.getPrefabCommon('baoxi', 'BaoXi');
        baoxi.showTipCards(cards);

        this.scheduleOnce(function () {
            this.closeOpenWin();
        }, 4);
    },
    /**
     * 报喜接受消息
     */
    baoxi_event: function (data) {

        this.baoxiCount++;
        this.updateNewChatbgViewStatus(false);
        this.closeOpenWin();
        cc.mile.openwin = cc.instantiate(this.baoxiDialog);
        cc.mile.openwin.parent = this.root();
        var baoxi = this.getPrefabCommon('baoxi', 'BaoXi');
        baoxi.showTipCards(data.listcard);

        this.scheduleOnce(function () {
            this.baoxiCount--;
            if (this.baoxiCount < 1) {
                this.closeOpenWin();
                this.closeOpenWin();
                this.closeOpenWin();
                this.getCommon("MaziBegin").hideUserSettlementInfo();
            }
        }, 4);

    },
    onTouxiangClick: function () {


        if (cc.mile.extparams && cc.mile.extparams.other) {
            var otherFlag = cc.mile.extparams.other;
            if (otherFlag && otherFlag.indexOf('giveup') != -1) {

                //在等待中
                if (cc.mile.gamestatus == 'waitting') {
                    this.showCenterToast("游戏未开始，无法投降!");

                    var param = {
                        token: cc.mile.authorization,
                        orgi: cc.mile.user.orgi,
                        userid: cc.mile.user.id,
                        gameid: cc.mile.user.gameid,
                    };
                    this.emitSocketRequest("giveuptest", JSON.stringify(param));

                } else if (cc.mile.gamestatus == 'play') { ///只有在玩的情况，才能允许投降
                    this.confirm('确认投降吗？', function (self) {
                        var param = {
                            token: cc.mile.authorization,
                            orgi: cc.mile.user.orgi,
                            giveupflag: 1,
                            firstflag: true,
                        };
                        self.emitSocketRequest("mazigiveup", JSON.stringify(param));
                        self.closeConfirm();
                    });
                }
            } else {
                this.showCenterToast("当前玩法不允许投降!");
            }

        } else {

            this.showCenterToast("当前玩法不允许投降!");
        }
    },
    onSettingClick: function () {
        this.updateNewChatbgViewStatus(false);
        cc.mile.openwin = cc.instantiate(this.set);
        cc.mile.openwin.parent = this.root();


    },
    showTestDialog: function (data) {
        this.updateNewChatbgViewStatus(false);
        this.closeOpenWin();
        cc.mile.openwin = cc.instantiate(this.testParamsPrefab);
        cc.mile.openwin.parent = this.root();
    },
    showJiesuanDialog: function (data) {

        cc.log(data);

        this.closeOpenWin();

        this.updateNewChatbgViewStatus(false);

        cc.mile.openwin = cc.instantiate(this.maziJiesuanDialog);
        cc.mile.openwin.parent = this.root();

        var scriptNdoe = cc.find("Canvas/maziJiesuanDialog/script/MaziJiesuanDialog");
        var dialogScript = scriptNdoe.getComponent("MaziJiesuanDialog");
        dialogScript.initUserView(data);

    },
    roomnum_format: function (data) {
        cc.log(data);
        cc.mile.roominfo = data;
        this.roomidLabel.string = data.roomdesc;
        this.roomNumbersLabel.string = data.nowgamenum;
        this.gameDescLabel.string = data.playwaydesc;
    },
    updateGameTime: function () {
        var testDate = new Date();
        var m = testDate.getMinutes();
        var strM = "" + m;
        if (m < 10) {
            strM = "0" + strM;
        }
        this.gameTimeLabel.string = testDate.getFullYear() + "-" + (testDate.getMonth() + 1) + "-" +
            testDate.getDate() + " " + testDate.getHours() + ":" + strM;
    },

    updateMingpaiViewStatus: function (isMingpai) {
        this.mingpaiBtn.active = !isMingpai;
        this.yimingaiBtn.active = isMingpai;
    },

    update: function () {

    },
});
