var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        playJiesuanPrefab: {
            default: null,
            type: cc.Prefab
        },
        playerContainer: {
            default: null,
            type: cc.Node
        },
        playerTime: {
            default: null,
            type: cc.Label
        },
        roominfoLabel: {
            default: null,
            type: cc.Label
        },
        copyBtn: {
            default: null,
            type: cc.Button
        },
        shareBtn: {
            default: null,
            type: cc.Button
        }
    },
    onLoad: function () {
        this.playerJiesuanLst = [];
        this.roomtype = '';
        //this.initUserView();
    },
    initUserView: function (data) {
        this.userData = data;
        for (var i = 0; i < data.summaryPlayer.length; i++) {
            var playJiesan = cc.instantiate(this.playJiesuanPrefab);
            var marginWidth = -350 + 230 * i;
            var playScript = playJiesan.getComponent("PlayJiesuan");
            this.roomtype = data.roomtype;
            playScript.initData(data.summaryPlayer[i], data.roomtype);
            playJiesan.setPosition(marginWidth, 0);
            playJiesan.parent = this.playerContainer;
            this.playerJiesuanLst.push(playJiesan);
        }
        var typeDesc = "";
        if (this.roomtype == "card") {
            typeDesc = "经典场";
        } else {
            typeDesc = "积分场";
        }

        if (data.teanum) {
            this.roominfoLabel.string = "茶楼号:" + data.teanum + "," + typeDesc + "房间：" + data.roomid;
        } else {
            this.roominfoLabel.string = "房间号：" + data.roomid;
        }
        this.playerTime.string = data.overdate;
    },

    onShareClick: function () {

        if (cc.mile.teahouse && cc.mile.teahouse.shareflag == 'N') {
            this.showCenterToast("茶楼房间已禁止分享结算图，请【复制操作】");
        } else {

            var gameUtils = require("WangdaGameUtils");

            gameUtils.screenShoot(this.screenShootShare);
        }

        /*if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "shareSettlementToWeixinSession", "()V");
        }else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppController",
                "shareSettlementToWeixinSession");
        }*/
    },
    screenShootShare: function (filepath) {
        cc.log(filepath);
        var gameUtils = require("WangdaGameUtils");
        gameUtils.shareImage(filepath);
    },
    onCopyClick: function () {
        var gameUtils = require("WangdaGameUtils");
        var copyStrLst = [];
        copyStrLst.push("时间：" + this.userData.overdate);

        for (var i = 0; i < this.userData.summaryPlayer.length; i++) {
            var player = this.userData.summaryPlayer[i];
            var jiesuanDesc = "玩家:" + player.username + "(ID" + player.gameid + ")";

            if (this.roomtype == 'card') {
                jiesuanDesc += '得分：' + player.totalscore;
            } else {
                jiesuanDesc += '得分：' + player.coins;
            }
            if (player.ticket && player.ticket > 0) {
                jiesuanDesc += ',打赏老板：' + player.ticket;
            }
            copyStrLst.push(jiesuanDesc);
        }
        gameUtils.copyToClipboard(copyStrLst.join("\n"));
        this.showCenterToast("复制成功");
    },
    updatePlayerStatus: function (userlist) {
        for (var i = 0; i < userlist.length; i++) {
            var playScript = this.getPlayJiesanByUserId(userlist[i].userid);
            if (playScript) {
                if (userlist[i].dissflag == 1) {
                    playScript.setAgree();
                } else if (userlist[i].dissflag == 0) {
                    playScript.setWaitting();

                } else if (userlist[i].dissflag == 2) {
                    playScript.setDisagree();
                }
            }
        }
    },
    getPlayJiesanByUserId: function (userid) {
        for (var i = 0; i < this.playerJiesuanLst.length; i++) {

            var playScript = this.playerJiesuanLst[i].getComponent("PlayJiesan");
            if (playScript.userid == userid) {
                return playScript;
            }
        }
        return null;
    },
    onCloseDialog: function () {

        if (cc.mile.createroomtype == 'coin') {
            this.scene("mazihall", this);
        } else {

            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            this.emitSocketRequest("leave", JSON.stringify(param));
            this.loadding();
        }


    },
    onContinueClick: function () {
        if (cc.mile.createroomtype == 'coin') {
            this.closeOpenWin();

            var param = {
                token: cc.mile.authorization,
                playway: cc.mile.extparams.playway,
                orgi: cc.mile.user.orgi,
                roomtype: cc.mile.createroomtype,
                extparams: cc.mile.extparams
            };
            this.emitSocketRequest("joinroom", JSON.stringify(param));
            this.loadding();

        } else {
            this.closeOpenWin();
        }


    }
    // update (dt) {},
});
