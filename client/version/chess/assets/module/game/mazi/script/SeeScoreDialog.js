var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        singlePokerPrefab: {
            default: null,
            type: cc.Prefab
        },
        pokerContainer: {
            default: null,
            type: cc.Node
        },
        residualFraction: {
            default: null,
            type: cc.Label,
        },
        sorce: 200,
    },
    onLoad: function () {
        this.heitao = {
            initvalues: [16, 20, 56, 60, 80, 84],
        };
        this.hongtao = {
            initvalues: [17, 21, 57, 61, 81, 85],
        };
        this.meihua = {
            initvalues: [18, 22, 58, 62, 82, 86],
        };
        this.fangkuai = {
            initvalues: [19, 23, 59, 63, 83, 87],
        };

        this.cardNodes = [];
        this.selectedValues = [];
        this.initScorePokers();
        this.residualFraction.string = "剩余牌分 ：" + this.sorce;
    },
    initScorePokers: function () {

        this.createPokerNodes(this.meihua.initvalues, -250, -100);
        this.createPokerNodes(this.fangkuai.initvalues, 60, -100);
        this.createPokerNodes(this.heitao.initvalues, -250, 70);
        this.createPokerNodes(this.hongtao.initvalues, 60, 70);
    },
    createPokerNodes: function (lstValues, startX, startY) {
        for (var i = 0; i < lstValues.length; i++) {
            var card = cc.instantiate(this.singlePokerPrefab);
            var cardScript = card.getComponent("SinglePoker");
            cardScript.cardNode.getComponent(cc.Sprite).spriteFrame
                = cardScript.cardAtlas.getSpriteFrame(lstValues[i]);
            card._name = lstValues[i];

            var marginWidth = startX + parseFloat(40 * i);
            card.setPosition(marginWidth, startY);
            card.parent = this.pokerContainer;
            this.cardNodes.push(card);
        }

    },
    showCardMask: function (value) {
        for (var i = 0; i < this.cardNodes.length; i++) {
            var singleCard = this.cardNodes[i];
            if (singleCard._name == value) {
                var cardScript = singleCard.getComponent("SinglePoker");
                cardScript.updateMaskStatus(true);
                var cardValue = parseInt(value / 8) > 5 ? 10 : 5;
                this.sorce = this.sorce - cardValue;
            }
        }
        this.residualFraction.string = "剩余牌分 ：" + this.sorce;
    },
    //自己的分数做个标记
    showScoreMy: function (value) {
        for (var i = 0; i < this.cardNodes.length; i++) {
            var singleCard = this.cardNodes[i];
            var cardScript = singleCard.getComponent("SinglePoker");
            if (!cardScript.cardMask.active) {
                if (singleCard._name == value) {
                    cardScript.updateScoreMy(true);
                }
            }
        }
    },
    addSelectedCard: function (value) {
        this.selectedValues.push(value);
        this.showCardMask(value);

    },
    addSelectedCards: function (cardLst) {
        for (var i = 0; i < cardLst.length; i++) {
            this.addSelectedCard(cardLst[i]);
        }
        var myCardsNodeList = cc.find("Canvas/script/MaziPoker").getComponent("MaziPoker").pokerContainer._children;
        if (myCardsNodeList != undefined) {
            for (var i = 0; i < myCardsNodeList.length; i++) {
                this.showScoreMy(myCardsNodeList[i]._name);
            }
        }

    }
    // update (dt) {},
});
