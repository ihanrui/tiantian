var beiMiCommon = require("MileCommon");
var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
    extends: beiMiCommon,
    properties: {
        user_name: {
            default: null,
            type: cc.Label
        },
        user_head: {
            default: null,
            type: cc.Node
        },
        user_head_face: {
            default: null,
            type: cc.Node
        },
        user_offline: {
            default: null,
            type: cc.Node
        },
        userHandUpdateNode: {
            default: null,
            type: cc.Node
        },
        user_id: {
            default: null,
            type: cc.Label
        },
        message: {
            default: null,
            type: cc.Node
        },
        messageLab: {
            default: null,
            type: cc.Label
        },
        //准备
        getready: {
            default: null,
            type: cc.Node
        },
        buchuNode: {
            default: null,
            type: cc.Node
        },//准备
        masterFlagImg: {
            default: null,
            type: cc.Node
        },//准备
        voice_chat: {
            default: null,
            type: cc.Node
        },
        voice_animation: {
            default: null,
            type: cc.Animation
        },
        zhuaLabel: {
            default: null,
            type: cc.Label
        },
        fenLabel: {
            default: null,
            type: cc.Label
        },
        baojinNode: {
            default: null,
            type: cc.Node
        },
        leftCardTipContainerNode: {
            default: null,
            type: cc.Node
        },
        leftCardTipNode: {
            default: null,
            type: cc.Node
        },
        numberAtlas: {
            default: null,
            type: cc.Node
        },
        winIndexNode: {
            default: null,
            type: cc.Node
        },
        winIndexBaoLiZiNode: {
            default: null,
            type: cc.Node
        },
        baojinSkeleton: {
            default: null,
            type: sp.Skeleton
        },
        settlementNode: {
            default: null,
            type: cc.Node
        },
        settlementAnimation: {
            default: null,
            type: cc.Animation
        },
        settlementNumberNode: {
            default: null,
            type: cc.Node
        },
        laypaker: {
            default: null,
            type: cc.Node
        },
        pokerexample: {
            default: null,
            type: cc.Prefab
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        animationAtlas: {
            default: null,
            type: cc.SpriteAtlas
        }, //明牌
        laymingpai: {
            default: null,
            type: cc.Node
        },
        mingpaiAtlas: {
            default: null,
            type: cc.SpriteAtlas
        }, //明牌
        userid: '',
        userPosition: null,
        username: null,
        userip: null,
        gameid: null,
        masterFlag: false,
        seeCardsFlag: false,
        positionDirection: '',
        relationIndex: -1,
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {
        this.player = {};
        this.updateReadyStatus(false);
        this.exchangeImages = ['effect_a1', 'effect_a2', 'effect_a3', 'effect_a4',
            'effect_a5', 'effect_a6', 'effect_a7', 'effect_a8'];
        //this.startHeadExchangeAnimation();
    },

    updateReadyStatus: function (status) {
        cc.log(this.player.username + '设置玩家的状态updateReadyStatus：' + status);
        if (this.getready) {
            this.getready.active = status;
        }
    },
    updateReadyByPlayerStatus: function (status) {

        cc.log(this.player.username + '设置玩家的状态updateReadyByPlayerStatus：' + status);
        if (status == 'ready') {
            this.getready.active = true;
        } else {
            this.getready.active = false;
        }
    },
    onOpenMessage: function (message) {
        this.message.active = true;

        this.messageLab.string = message;
        this.scheduleOnce(function () {
            this.message.active = false;
        }, 2);
    },

    playUserEmojAnimation: function (animationName,animationMp3) {
        var  self = this;
        cc.log(this.user_head_face +'----玩家相对自己的playUserEmojAnimation----'+animationName);
        if(this.user_head_face){
            cc.loader.loadRes("animation/emoji/"+animationName.trim()+".anim", function (err, clip) {
                if (err) {
                    cc.log(err);
                }
                else {
                    cc.mile.audio.playSFX("mazi/facevoice/"+animationName.trim()+".mp3");
                    cc.log('----玩家相对自己的playUserEmojAnimation----'+animationName);
                    self.user_head_face.getComponent(cc.Animation).addClip(clip, animationName);
                    self.user_head_face.getComponent(cc.Animation).play(animationName);
                }
            });
        }
    },
    initplayer: function (data, pos) {
        this.relationIndex = data.relationIndex;
        this.positionDirection = data.positionDirection;
        this.updateOfflineViewStatus(data.online);
        this.userPosition = pos;
        cc.log('----玩家相对自己的initplayer----');
        if (this.player.id != data.id) {

            this.player = data;
            this.userid = data.id;
            this.username = data.username;
            this.userip = data.ip;
            this.gameid = data.gameid;
            this.user_name.string = data.username;
            this.user_id.string = data.id;

            this.loadUrlJPGImage(data.userurl
                + "&username=" + data.id, this.user_head);
        }
    },
    initbackplayer: function (data, pos, pDirection) {
        //  this.relationIndex = data.relationIndex;
        this.player = data;
        this.userid = data.userid;
        this.username = data.username;
        this.userip = data.ip;
        this.gameid = data.gameid;
        this.positionDirection = pDirection;
        this.userPosition = pos;
        this.user_name.string = data.username;
        this.user_id.string = data.userid;
        this.loadUrlJPGImage(data.userurl
            + "&username=" + data.id, this.user_head);
    },
    // 其他玩家相对自己的位置
    getRelationIndex: function () {
        return this.relationIndex;
    },
    updatePlayUserInfo: function (player) {
        if (this.player.id != player.id) {

            this.player = player;
            this.userid = player.playuser;
            this.username = player.username;
            this.userip = player.ip;
            this.gameid = player.gameid;
            this.user_name.string = player.username;
            this.user_id.string = player.playuser;

            this.startHeadExchangeAnimation();
            this.loadUrlJPGImage(player.userurl
                + "&username=" + player.playuser, this.user_head);
        }
    },
    startHeadExchangeAnimation: function () {
        this.userHandUpdateNode.active = true;
        this.user_head.active = false;
        var animation = this.userHandUpdateNode.getComponent(cc.Animation);
        if (animation == null) {
            animation = this.userHandUpdateNode.addComponent(cc.Animation);
        }
        // frames 这是一个 SpriteFrame 的数组.
        var frames = this.getExchangeImageSpriteFrame();
        var clip = cc.AnimationClip.createWithSpriteFrames(frames, 6);
        clip.name = "anim_run";
        //完成后删除莫牌对象

        animation.addClip(clip);
        animation.on('finished', this.onExchangeAnimationFinished, this);
        animation.play('anim_run');
    },
    onExchangeAnimationFinished: function () {
        this.userHandUpdateNode.active = false;
        this.user_head.active = true;
    },
    getExchangeImageSpriteFrame: function () {
        var lstFrames = [];
        for (var i = 0; i < this.exchangeImages.length; i++) {
            lstFrames.push(this.animationAtlas.getSpriteFrame(this.exchangeImages[i]));
        }
        return lstFrames;
    },
    updateBaojinViewStatus: function (status, numbs) {
        //var play = this.feiji.findAnimation('feiji2');
        if (status) {
            this.baojinNode.active = true;
            this.baojinSkeleton.setAnimation(0, "baojing", false);//this.feiji.start();
        } else {
            this.baojinNode.active = false;
        }
        this.updateLeftCardsStatus(numbs);
    },
    updateLeftCardsStatus: function (numbs) {
        if (numbs > 0 && !this.seeCardsFlag) {
            this.leftCardTipContainerNode.active = true;
            var sprite = this.numberAtlas.getComponent(cc.Sprite);
            sprite.spriteFrame =
                new cc.SpriteFrame(cc.url.raw("resources/images/room/amount/" + numbs + ".png"));
            for (var i = 4; i >= numbs; i--) {
                this.leftCardTipContainerNode.children[i].active = false;
            }
        } else {
            this.leftCardTipContainerNode.active = false;

            var childLength = this.leftCardTipContainerNode.children.length;
            for (var i = 0; i < childLength; i++) {
                this.leftCardTipContainerNode.children[i].active = true;
            }
        }
    },
    updateOfflineViewStatus: function (status) {
        if (!status) {
            this.user_offline.active = true;
        } else {
            this.user_offline.active = false;
        }
    },
    getOffilineStatus:function () {

        if(this.user_offline){
            return !this.user_offline.active;
        }
        return false;
    },
    showWinTag: function (index,isRecover) {

        var sprite = this.winIndexNode.getComponent(cc.Sprite);
        sprite.spriteFrame =
            new cc.SpriteFrame(cc.url.raw("resources/images/room/Ui_you" + index + ".png"));
        this.winIndexNode.active = true;
        if (index <= 2 && this.winIndexBaoLiZiNode && !isRecover) {
            this.winIndexBaoLiZiNode.active = true;
        }
    },
    updateWinTagViewStatus: function (status) {
        this.winIndexNode.active = status;
    },
    showSettlement: function (numbers) {

        if (this.settlementNumberNode) {
            this.settlementNumberNode.removeAllChildren();
        }
        WangdaGameUtils.showNumberNode(numbers, this.settlementNumberNode);

        this.settlementNode.active = true;
        this.settlementAnimation.play();
    },
    setFenSettlement: function (fen) {
        this.fenLabel.string = fen;
    },
    updateSettlementViewStatus: function (status) {
        this.settlementNode.active = status;
    },

    updateZhuafen: function (score) {
        this.zhuaLabel.string = score;
    },
    updateFen: function (score) {
        this.fenLabel.string = score;
    },
    //点击用户头像
    onUserInfoClick: function (event) {

    },

    updateVoiceAnimationStatus: function (status) {
        if (status) {
            if (!this.voice_chat.active) {
                this.voice_chat.active = true;
                this.voice_animation.play();
            }
        } else {

            this.voice_chat.active = false;
            this.voice_animation.stop();
        }
    },

    setMasterFlag: function (isMaster) {
        this.masterFlag = isMaster;
        if (isMaster) {
            this.masterFlagImg.active = true;
        } else {
            this.masterFlagImg.active = false;
        }
    },
    //将牌复制出来，然后打出来
    opencords: function (cordslist, x, y, postion) {

        cc.log("opencords 请求 1111:" + cordslist);
        //循环要打出来的牌
        for (var i = 0; i < cordslist.length; i++) {
            var a = cc.instantiate(this.pokerexample);
            a.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame((cordslist[i]));
            a._name = cordslist[i];
            a.setPosition(x, y);
            //a.setScale(1);
            a.parent = this.laypaker;
            // this.poker.children[i].active = false;

            //获取总宽度，打出去的牌永远显示在最中间的位置
            //var width = parseFloat([23 * (1 - cordslist.length)]) + parseFloat(46 * i);
            //左边
            var width = 0;
            var yy = 0;
            if (postion == "left") {
                yy = -50;
                width = parseFloat(40 * i);
            }
            else if (postion == "right") {
                yy = -50;
                width = -parseFloat(40 * (cordslist.length - i - 1));
            }
            else if (postion == "top") {
                yy = 30;
                width = parseFloat(79) + parseFloat([20 * (1 - cordslist.length)]) + parseFloat(40 * i);
            }
            // var width=(0-parseFloat(23*cordslist.length)/2);
            var moduiAction = this.getPlayCardsDisplayAction(width, yy, 0.4);
            this.laypaker.children[i].runAction(moduiAction);
        }
    },
    updateBuchuStatus: function (status) {
        if(this.buchuNode){
            this.buchuNode.active = status;
        }
    },
    clearPlayCardLayer: function () {
        cc.log("清理玩家的牌：" + JSON.stringify(this.player));
        if (this.laypaker) {
            this.laypaker.removeAllChildren();
        }
        this.updateBuchuStatus(false);
    },
    getPlayCardsDisplayAction: function (x, y, timeDuration) {
        // moveTo固定的位置，moveBy位置不固定
        var mv = cc.moveTo(timeDuration, x, y).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(timeDuration, 0.6, 0.6);
        return cc.spawn(mv, scale);
    },

    //显示明牌
    showSeecards: function (parklist) {
        cc.log(parklist);

        if (this.laymingpai) {
            this.seeCardsFlag = true;
            this.updateLeftCardsStatus(0);

            this.clearMingPaiContainer();
            var cardWidth = 30;
            var cardHeight = 10;

            this.laymingpai.active = true;
            if (this.positionDirection == "top") {
                this.renderTopPlayerCardPosition(parklist, cardWidth, cardHeight);
            } else if (this.positionDirection == "left") {
                this.renderLeftPlayerCardPosition(parklist, cardWidth, cardHeight);
            } else if (this.positionDirection == "right") {
                this.renderRightPlayerCardPosition(parklist, cardWidth, cardHeight);
            }
        }
    },

    renderLeftPlayerCardPosition: function (parklist, cardWidth, cardHeight) {

        for (var i = 0; i < parklist.length; i++) {
            //将一个一个牌显示在对象池当中
            var poker = new cc.Node();
            var sprite = poker.addComponent(cc.Sprite);
            sprite.spriteFrame = this.mingpaiAtlas.getSpriteFrame(parklist[i]);

            var marglwidth = -220 + (i % 16) * 18 + parseInt(i / 16) * 90;
            var marglHeight = -parseInt(i / 16) * (42);


            poker.setPosition(marglwidth, marglHeight);
            poker._name = parklist[i] + '';
            poker.active = true;
            poker.scaleX = 0.6;
            poker.scaleY = 0.6;

            poker.parent = this.laymingpai;
        }
    },
    renderRightPlayerCardPosition: function (parklist, cardWidth, cardHeight) {

        var appendX = 0;
        if (parklist.length < 15) {
            appendX = (15 - parklist.length) * 18;
        }

        for (var i = 0; i < parklist.length; i++) {
            //将一个一个牌显示在对象池当中
            var poker = new cc.Node();
            var sprite = poker.addComponent(cc.Sprite);
            sprite.spriteFrame = this.mingpaiAtlas.getSpriteFrame(parklist[i]);
            var marglwidth = 240 - (16 - i % 16) * 18 + appendX;

            var marglHeight = -parseInt(i / 16) * (42);

            poker.setPosition(marglwidth, marglHeight);
            poker._name = parklist[i] + '';
            poker.active = true;
            poker.scaleX = 0.6;
            poker.scaleY = 0.6;
            poker.parent = this.laymingpai;
        }
    },
    renderTopPlayerCardPosition: function (parklist, cardWidth, cardHeight) {

        for (var i = 0; i < parklist.length; i++) {
            //将一个一个牌显示在对象池当中
            var poker = new cc.Node();
            var sprite = poker.addComponent(cc.Sprite);
            sprite.spriteFrame = this.mingpaiAtlas.getSpriteFrame(parklist[i]);
            var marglwidth = -200 + (i % 16) * 18;
            var marglHeight = -parseInt(i / 16) * (42) + 50;

            poker.setPosition(marglwidth, marglHeight);
            poker._name = parklist[i] + '';
            poker.active = true;
            poker.scaleX = 0.6;
            poker.scaleY = 0.6;

            poker.width = cardWidth;
            poker.height = cardHeight;
            poker.parent = this.laymingpai;
        }
    },

    clearMingPaiContainer: function () {

        if (this.laymingpai) {
            this.laymingpai.removeAllChildren();
        }
    },
    cleanToReady: function (status) {
        if (status) {
            this.seeCardsFlag = false;
            this.updateBuchuStatus(false); //  不出
            this.updateBaojinViewStatus(false, 0); // 报警
            this.updateWinTagViewStatus(false);//  一游 二游
            this.updateSettlementViewStatus(false); // 结算
            this.clearMingPaiContainer(); //  明牌
            this.updateZhuafen(0); // 抓分

            this.clearPlayCardLayer();
        }
    }
    // update (dt) {},
});
