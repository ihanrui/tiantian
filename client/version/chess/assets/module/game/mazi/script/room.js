// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

var GameUtil = require("WangdaGameUtils");

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        number: {
            default: null,
            type: cc.Node
        },
        number1: {
            default: null,
            type: cc.Node
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        cardlist: {
            default: null,
            type: cc.Node
        },
        cardprefab: {
            default: null,
            type: cc.Prefab
        },
        maziStartAnimation: {
            default: null,
            type: cc.Animation
        },
        maziStartNode: {
            default: null,
            type: cc.Node
        },
        maziModuiNode: {
            default: null,
            type: cc.Node
        },
        maziModuiAnimation: {
            default: null,
            type: cc.Animation
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.minpokerpool = new cc.NodePool(); //背面
        for (i = 0; i < 15; i++) {
            //实例化预制资源按时阿斯蒂芬
            var codes = cc.instantiate(this.cardprefab);
            var cd = 'card_0' + i + '';
            //获取对应的牌给预制资源
            codes.children[0].getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(cd);
            //codes.children[0].getComponent(cc.Button).clickEvents.push(this.onClickCads);
            // codes.children[0].getComponent(cc.Button).node.on('click',this.onClickCads);
            //加入到对象当中
            this.minpokerpool.put(codes);     //牌-背面
        }

        //判断对象是否存在值
        if (this.minpokerpool.size() > 0) {
            //循环对象
            for (i = 0; i < this.minpokerpool.size(); i++) {
                //将一个一个牌显示在对象池当中
                var width = i * 50;
                var a = this.minpokerpool._pool[i];
                a.setPosition(width, 0);
                a.parent = this.cardlist;
            }
        }
    },
    start: function () {

    },
    //点击牌的动作
    onClickCads: function (event) {
        if (event.target.y == this.posy) {
            event.target.y = event.target.y + 30;
            beiMiCard.selected = true;
        } else {
            event.target.y = event.target.y - 30;
        }
    },
    onMaziStart: function () {
        /*if (this.maziStartAnimation != null){
            this.maziStartNode.active = true;
            this.maziStartAnimation.play();
        }*/
        this.onMaziModui();
    },

    onMaziModui: function () {

        var moDuiNode = new cc.Node();
        var sprite = moDuiNode.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/card_1a.png"), cc.rect(0, 0, 116, 151));

        moDuiNode.setPosition(GameUtil.getViewCenterY(), GameUtil.getViewCenterX() + 50);
        cc.director.getScene().addChild(moDuiNode);

        var finished = cc.callFunc(function (target) {
            this.onModuiJumpCallback(target);
        }, this);//动作完成

        // 获取跳动的action
        var moduiAction = this.getModuiJumpAction(0.3, 30);
        moDuiNode.runAction(cc.sequence(moduiAction, finished));

    },
    onModuiJumpCallback: function (target) {
        //完成后删除摸排
        var finished = cc.callFunc(function (obj) {
            obj.destroy();
        }, this, target);

        var moduiAction = this.getModuiDisplayAction(20, 300, 1.2);
        target.runAction(cc.sequence(moduiAction, finished));

    },
    getModuiJumpAction: function (jumpDuration, jumpHeight) {
        // 
        var jumpUp = cc.moveBy(jumpDuration, cc.p(0, jumpHeight)).easing(cc.easeCubicActionOut());
        // 
        var jumpDown = cc.moveBy(jumpDuration, cc.p(0, -jumpHeight)).easing(cc.easeCubicActionIn());
        // 
        return cc.repeat(cc.sequence(jumpUp, jumpDown), 3);
    },
    getModuiDisplayAction: function (x, y, timeDuration) {
        // 
        var mv = cc.moveBy(timeDuration, x, y);
        var scale = cc.scaleTo(timeDuration, 0.4, 0.4);
        return cc.spawn(mv, scale);
    }
    // update (dt) {},
});
