var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        room_id: {
            default: null,
            type: cc.Label
        },
        room_number: {
            default: null,
            type: cc.Sprite
        },
        room_pay: {
            default: null,
            type: cc.Label
        },
        room_time: {
            default: null,
            type: cc.Label
        }
    },
    onLoad:function() {

    },
    init:function(context,roominfo) {
        this.room_id = roominfo.room_id;
        this.room_number = roominfo.room_number;
        this.room_pay = roominfo.room_pay;
        this.room_time = roominfo.room_time;
    },
    // update (dt) {},
});
