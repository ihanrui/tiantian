var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        singlePokerPrefab: {
            default: null,
            type: cc.Prefab
        },
        pokerContainer: {
            default: null,
            type: cc.Node
        },
        residualFraction: {
            default: null,
            type: cc.Label,
        }
    },
    onLoad: function () {


        this.heitao = {
            initvalues: [],
        };
        this.hongtao = {
            initvalues: [],
        };
        this.meihua = {
            initvalues: [],
        };
        this.fangkuai = {
            initvalues: [],
        };

        for(var i = 103; i >= 0 ; i--){
            if(i >= 78){
                this.heitao.initvalues.push(i);
            }else if(i < 78 && i >= 52){
                this.hongtao.initvalues.push(i);
            }else if(i < 52 && i >= 26){
                this.meihua.initvalues.push(i);
            }else if(i < 26){
                this.fangkuai.initvalues.push(i);
            }
        }
        this.cardNodes = [];
        this.selectedValues = [];
        this.initScorePokers();

    },
    initScorePokers: function () {

        this.createPokerNodes(this.meihua.initvalues, -190, -45);
        this.createPokerNodes(this.fangkuai.initvalues, -190, -145);
        this.createPokerNodes(this.heitao.initvalues, -190, 155);
        this.createPokerNodes(this.hongtao.initvalues, -190, 60);

        this.createPokerGuiNodes(105,-260, 135);

        this.createPokerGuiNodes(104,-260, 45);
        this.createPokerGuiNodes(106,-260, -60);
        this.createPokerGuiNodes(107,-260, -160);
    },
    createPokerNodes: function (lstValues, startX, startY) {
        cc.log("扑克牌数量："+lstValues);
        for (var i = 0; i < lstValues.length; i++) {
            var card = cc.instantiate(this.singlePokerPrefab);
            var cardScript = card.getComponent("SinglePoker");
            cardScript.cardNode.getComponent(cc.Sprite).spriteFrame
                = cardScript.cardAtlas.getSpriteFrame(lstValues[i]);
            card._name = lstValues[i];
            card.setScale(0.2,0.2);
            var marginWidth = startX + parseFloat(38 * (i % 13));
            if(i / 13 >= 1){
                card.setPosition(marginWidth, startY - 45);
            }else {
                card.setPosition(marginWidth, startY);
            }
            card.parent = this.pokerContainer;
            this.cardNodes.push(card);
        }

    },
    createPokerGuiNodes: function (value, startX, startY) {
        cc.log("扑克牌数量："+value);
        var card = cc.instantiate(this.singlePokerPrefab);
        var cardScript = card.getComponent("SinglePoker");
        cardScript.cardNode.getComponent(cc.Sprite).spriteFrame
            = cardScript.cardAtlas.getSpriteFrame(value);
        card._name = value;
        card.setScale(0.4,0.36);
        var marginWidth = startX ;
        card.setPosition(marginWidth, startY);
        card.parent = this.pokerContainer;
        this.cardNodes.push(card);
    },
    showCardMask: function (value) {
        for (var i = 0; i < this.cardNodes.length; i++) {
            var singleCard = this.cardNodes[i];
            if (singleCard._name == value) {
                var cardScript = singleCard.getComponent("SinglePoker");
                cardScript.updateMaskStatus(true);
            }
        }
    },
    //自己的分数做个标记
    showScoreMy: function (value) {
        cc.log("showScoreMy："+value);
        for (var i = 0; i < this.cardNodes.length; i++) {
            var singleCard = this.cardNodes[i];
            var cardScript = singleCard.getComponent("SinglePoker");
            if (!cardScript.cardMask.active) {
                if (singleCard._name == value) {
                    cardScript.updateScoreMy(true);
                }
            }
        }
    },
    addSelectedCard: function (value) {
        this.selectedValues.push(value);
        this.showCardMask(value);

    },
    addSelectedCards: function (cardLst) {
        for (var i = 0; i < cardLst.length; i++) {
            this.addSelectedCard(cardLst[i]);
        }
        var myCardsNodeList = cc.find("Canvas/script/MaziPoker").getComponent("MaziPoker").pokerContainer._children;
        if (myCardsNodeList != undefined) {
            for (var i = 0; i < myCardsNodeList.length; i++) {
                this.showScoreMy(myCardsNodeList[i]._name);
            }
        }

    }
    // update (dt) {},
});
