cc.Class({
    extends: cc.Component,

    properties: {
        pokerContainer: {
            default: null,
            type: cc.Node
        },
        pokerexample: {
            default: null,
            type: cc.Prefab
        },
        cardMask: {
            default: null,
            type: cc.Prefab
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        cunpoker: {
            default: null,
            type: cc.Node
        },
        A: {
            default: null,
            type: cc.Node
        }
    },

    onLoad: function () {
        // var parklist = [1, 2, 3, 4, 5, 6, 7, 8,10,18,20,21,22,23,24,25,26,28];
        //加载所有牌
        //this.loadcards(parklist);
        //牌
        this.leftPokers = this.pokerContainer.children;
        //牌初始位置
        this.cardInitY = this.leftPokers[0].y;
        //触摸选择到的牌
        this.touchedCards = [];
        //选中的牌
        this.selectedCards = [];

        console.info(this.leftPokers);

        //this.minpokerpool = new cc.NodePool();
        //for (var i = 0; i < 27; i++) {
        //    var poker = cc.instantiate(this.pokerexample);
        //    this.minpokerpool.put(poker);
        //}
        //this.initTouchContainerEvent();
    },

    start: function () {
        // this.leftPokers = this.poker.children;
        // console.info(this.leftPokers);

    },
    loadcards: function (parklist, directShow) {
        this.minpokerpool = new cc.NodePool();
        for (var i = 0; i < parklist.length; i++) {
            var poker = cc.instantiate(this.pokerexample);
            this.minpokerpool.put(poker);
        }
        this.initTouchContainerEvent();
        var widthWidth = 0;
        var xBegin = -22 * 26;
        if (parklist.length > 22) {
            widthWidth = (cc.director.getWinSize().width - 157 * 0.6 - 40) / (parklist.length - 1);
        } else {
            widthWidth = (cc.director.getWinSize().width - 157 * 0.6 - 40) / 22;
            xBegin = xBegin + (23 - parklist.length) * widthWidth * 0.5;
        }
        cc.log("loadcards00 width:" + parklist.length);
        cc.log("loadcards00 width:" + widthWidth);
        for (var i = 0; i < parklist.length; i++) {
            //将一个一个牌显示在对象池当中
            var widht = xBegin + i * widthWidth;

            cc.log("loadcards00 width:" + widht);

            var poker = this.minpokerpool.get();
            poker.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(parklist[i]);
            poker._name = parklist[i];
            poker.active = directShow;
            poker.setPosition(widht, 0);
            poker.parent = this.pokerContainer;
        }
        this.initCardsEvent();

    },

    /**
     * 添加事件
     */
    initTouchContainerEvent: function () {
        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）

        this.pokerContainer.on(cc.Node.EventType.TOUCH_START, function (event) {
            //牌
            var card = event.target;
            //起始触摸位置（和第一张card一样，相对于poker的位置）
            this.touchStartLocation = this.leftPokers[0].convertTouchToNodeSpace(event);
            //计算牌位置
            var index = 0;
            for (var i = 0; i < this.leftPokers.length; i++) {
                var c = this.leftPokers[i];
                if (c.name == card.name) {
                    index = i;
                    break;
                }
            }

            //暂存第一次触摸到的牌
            var touchedCard = {
                index: index,
                card: card
            };
            this.firstTouchedCard = touchedCard;
            this.moveStepPosition = null;
            //暂存
            this.pushTouchedCards(touchedCard.index, touchedCard.card);

        }, this, true);

        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）
        this.pokerContainer.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            //先清除原先触摸到的牌
            //触摸点转换为card节点坐标
            var nodeLocation = this.leftPokers[0].convertTouchToNodeSpace(event);
            var x = nodeLocation.x;
            var y = nodeLocation.y;

            if (this.moveStepPosition) { // 如果移动偏移量小于10 则不换算
                var moveStep = Math.abs(this.moveStepPosition.x - x);
                if (moveStep < 20) {
                    return;
                } else {
                    this.moveStepPosition = nodeLocation;
                }
            } else {
                this.moveStepPosition = nodeLocation;
                return;
            }


            this.clearTouchedCards();
            //保存第一张牌
            this.pushTouchedCards(this.firstTouchedCard.index, this.firstTouchedCard.card);


            //找到当前选中的牌
            var currentCard = null;
            for (var i = 0; i < this.leftPokers.length; i++) {
                var card = this.leftPokers[i];
                var cardX = card.x;
                var cardY = card.y;
                //console.log('card x=' + cardX + ',y=' + cardY);
                //当开始位置大于移到最后的位置时，说明是往左移

                //某张牌范围包括了鼠标位置，选中此牌与触摸开头的所有牌

                if ((cardX - (card.width / 2)) <= x
                    && x <= (cardX - (card.width / 2) + 46)
                    && -(card.height / 2) <= y && y <= (card.height / 2)) {
                    currentCard = card;
                    //暂存触摸到的牌
                    this.pushTouchedCards(i, card);

                    break;
                }
            }

            //添加开头与此牌直接的所有牌
            var startTouchLocation = this.touchStartLocation;
            for (var i = 0; i < this.leftPokers.length; i++) {
                var card = this.leftPokers[i];
                var cardX = card.x;
                cardX = (cardX - (card.width / 2));
                //框选的范围包括了的牌
                var min, max;
                if (startTouchLocation.x < nodeLocation.x) {
                    min = startTouchLocation.x;
                    max = nodeLocation.x;
                } else {
                    min = nodeLocation.x;
                    max = startTouchLocation.x;
                }
                //console.log('min=' + min + ', max=' + max);

                if (min <= cardX && cardX <= max) {
                    //暂存触摸到的牌
                    this.pushTouchedCards(i, card);
                }
            }


        }, this, true);

        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）
        this.pokerContainer.on(cc.Node.EventType.TOUCH_END, function (event) {
            //console.log('poker TOUCH_END');
            this.doSelectCard();
        }, this, true);

        //父节点监听touch事件（直接子节点必须注册同样的事件方能触发）
        this.pokerContainer.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
            //console.log('poker TOUCH_CANCEL');
            this.doSelectCard();
        }, this, true);
    },
    initCardsEvent: function () {
        //给所有的牌注册事件，会自动冒泡到poker节点
        for (var i = 0; i < this.leftPokers.length; i++) {
            var cards = this.leftPokers;
            //闭包传递i值
            (function (i) {
                var card = cards[i];
                card.on(cc.Node.EventType.TOUCH_START, function (event) {
                    //console.log('card TOUCH_START');
                }, card);

                card.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                    //console.log('card TOUCH_MOVE');
                }, card);

                card.on(cc.Node.EventType.TOUCH_END, function (event) {
                    //console.log('card TOUCH_END');
                }, card);

                card.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
                    //console.log('card TOUCH_CANCEL');
                }, card);

            })(i)

        }
    },

    addSelectCard: function (cardName) {
        //计算牌位置
        var index = 0;
        var card = null;
        for (var i = 0; i < this.leftPokers.length; i++) {
            var c = this.leftPokers[i];
            if (c.name == cardName) {
                index = i;
                card = c;
                break;
            }
        }
        if (card) {
            //暂存第一次触摸到的牌
            var touchedCard = {
                index: index,
                name: card.name,
                directAdd: true
            };

            //暂存
            this.pushToTouchedArray(touchedCard, card);

        }
    },
    /**
     * 暂存触摸到的牌
     */
    pushTouchedCards: function (index, card) {
        //构造牌对象s
        var cardObj = {
            index: index,
            name: card.name,
            isSelected: card.y == this.cardInitY ? false : true //高度不一样，表示选中
        };

        this.pushToTouchedArray(cardObj, card);

    },
    pushToTouchedArray: function (cardObj, card) {
        if (card._name != "postion") {

            //防止重复添加
            var existCard = this.touchedCards.find(function (obj) {
                if (obj.name == cardObj.name) {
                    return obj;
                } else {
                    return null;
                }
            });
            if (!existCard) {
                //添加暂存
                this.touchedCards.push(cardObj);

                //包含提示
                this.addCardMask(card);
            }
        }
    },
    /**
     * 清除原先暂存的触摸到的牌
     */
    clearTouchedCards: function () {
        for (var i = 0; i < this.touchedCards.length; i++) {
            var cardIndex = this.touchedCards[i].index;
            var card = this.leftPokers[cardIndex];
            card.removeChild(card.children[0]);
        }
        this.touchedCards = [];
    },

    clearSelectedCards: function () {
        //改变牌状态
        for (var i = 0; i < this.selectedCards.length; i++) {
            for (var j = 0; j < this.leftPokers.length; j++) {
                var card = this.leftPokers[j];
                if (card.name == this.selectedCards[i]) {
                    if (card.y == this.cardInitY + 30) { // 添加的牌，直接选中
                        card.y = card.y - 30;
                    }
                }
            }
        }
        this.selectedCards = [];
    },
    /**
     * 选择牌
     */
    doSelectCard: function () {
        this.selectedCards = [];

        this.updateTouchedCardPosition();

        //重置
        this.clearTouchedCards();

        //显示选中的牌
        this.showSelectedCards();
    },

    updateTouchedCardPosition: function () {
        console.log(this.touchedCards);
        //改变牌状态
        for (var i = 0; i < this.touchedCards.length; i++) {
            var cardObj = this.touchedCards[i];
            var card = this.leftPokers[cardObj.index];
            if (cardObj.directAdd) {
                if (card.y != this.cardInitY + 30) { // 添加的牌，直接选中
                    card.y = card.y + 30;
                }
            } else {
                if (cardObj.isSelected) { //如果是选中改为不选中
                    card.y = card.y - 30;
                } else { //不选中改为选中状态
                    card.y = card.y + 30;
                }
            }
        }
    },
    /**
     * 包含牌遮罩
     */
    addCardMask: function (card) {
        var cardName = card.name + '';

        cc.log("添加遮罩名称：" + cardName);

        if (cardName.length > 0) {
            var cardMask = cc.instantiate(this.cardMask);
            cardMask.setPosition(cc.p(0, 0));
            card.addChild(cardMask);
        }
    },

    /**
     * 显示选中的牌
     */
    showSelectedCards: function () {
        this.selectedCards = [];
        for (var i = 0; i < this.leftPokers.length; i++) {
            var card = this.leftPokers[i];
            var isSelected = card.y == this.cardInitY ? false : true;
            if (isSelected && card.name != 'postion') {
                this.selectedCards.push(card.name);
            }
        }
        //输出
        //console.info("selected cards is: " + JSON.stringify(this.selectedCards));
    },
    getPokerOutDisplayAction: function (x, y, timeDuration) {
        // moveTo固定的位置，moveBy位置不固定
        var mv = cc.jumpTo(timeDuration, x, y).easing(cc.easeCubicActionOut());
        var scale = cc.scaleTo(timeDuration, 0.6, 0.6);
        return cc.spawn(mv, scale);
    },
    //点击出的效果
    PlayACard: function (outCards) {

        this.showPokerOutAnimation(outCards);

        this.removeChildPokers(outCards);

        this.initCardsEvent();

        //this.Justification();

        ///清空所有的选项
        this.selectedCards = [];

    },

    showPokerOutAnimation: function (outCards) {
        var leftPokers = this.leftPokers;
        //进来的时候，先删除上次打出去的牌
        this.cunpoker.removeAllChildren();


        //循环读取所有选中的
        for (var j = 0; j < outCards.length; j++) {

            var xPosition = this.leftPokers[0].x;
            for (var i = 0; i < this.leftPokers.length; i++) {
                //和选中的相等
                if (outCards[j] == this.leftPokers[i]._name) {

                    this.leftPokers[i].active = false;
                    //this.poker.removeChild(this.poker.children[i]);
                }
            }
            this.addCunPoker(outCards[j], xPosition, outCards.length, j);
        }
    },
    addCunPoker: function (pockerName, xPosition, outCardsCount, indexCard) {

        var a = cc.instantiate(this.pokerexample);
        a.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(pockerName);
        a._name = pockerName;
        a.setPosition(xPosition, (-263 + 30));
        a.parent = this.cunpoker;

        //获取总宽度，打出去的牌永远显示在最中间的位置
        var width = parseFloat([20 * (1 - outCardsCount)]) + parseFloat(40 * indexCard);
        var moduiAction = this.getPokerOutDisplayAction(width, 160, 0.2);
        this.cunpoker.children[indexCard].runAction(moduiAction);
    },
    removeChildPokers: function (outCards) {
        //循环读取所有选中的
        for (var j = 0; j < outCards.length; j++) {
            //循环所有的牌
            for (var i = 0; i < this.leftPokers.length; i++) {

                if (this.leftPokers[i]._name == "postion") {
                    continue;
                }
                if (outCards[j] == this.leftPokers[i]._name) {
                    var card = this.leftPokers[i];
                    this.pokerContainer.removeChild(card);
                    this.minpokerpool.put(card);
                }
            }
        }
    },
    Homing: function () {
        //牌
        for (var j = 0; j < this.leftPokers.length; j++) {
            if (this.leftPokers[j]._name == "postion") {
                continue;
            }
            //  var width = parseFloat([23 * (1 - (this.leftPokers.length - 1))]) + parseFloat(46 * j);
            if (this.leftPokers[j].y != 0) {
                this.leftPokers[j].setPosition(this.leftPokers[j].x, 0);
            }
        }
    },
    getLeftCardCount: function () {
        if (this.leftPokers) {
            return this.leftPokers.length;
        }
        return -1;
    },

    ///获取牌的总数，计算出牌最第一张牌的位置
    getLeftRange: function (cardscount) {
        //获取总宽度
        //var width=this.conlist.width;
        //求中间值,130是最后一个牌的位置
        var left = ((50 * cardscount) - 157) / 2;
        return left;
    },
    showCard: function (showindex) {
        this.leftPokers[showindex + 1].active = true;
    },
    clearAllPokers: function () {
        for (var i = this.leftPokers.length - 1; i >= 0; i--) {
            var card = this.leftPokers[i];
            if (card.name != 'postion') {
                this.pokerContainer.removeChild(card);
                this.minpokerpool.put(card);
            }
        }
    },
    //清空自己打出去的牌
    clearSefAllPokers: function () {
        this.cunpoker.removeAllChildren();
    }
});