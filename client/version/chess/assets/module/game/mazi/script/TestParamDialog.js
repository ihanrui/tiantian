var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        optionPrefab: {
            default: null,
            type: cc.Prefab
        },
        optionGroupPrefab: {
            default: null,
            type: cc.Prefab
        },
        paramContainer: {
            default: null,
            type: cc.Node
        },
        confirmBtn: {
            default: null,
            type: cc.Node
        },
    },
    onLoad: function () {

        this.paramsData = {
            groups: [
                {id: "4028ab6", name: "数量选择", code: "numbers", type: 'radio'},
                {id: "4028ab2", name: "牌型选择", code: "cardtype", type: 'radio'}
            ],
            items: [
                {id: "402", name: "1", defaultvalue: true, code: "1", groupid: "4028ab6"},
                {id: "402", name: "2", code: "2", groupid: "4028ab6"},

                {id: "402", name: "510K", code: "15", groupid: "4028ab2"},
                {id: "403", name: "两个王", code: "18", groupid: "4028ab2"},
                {id: "404", name: "5炸", defaultvalue: true, code: "19", groupid: "4028ab2"},
                {id: "405", name: "三个王", code: "20", groupid: "4028ab2",},
                {id: "406", name: "6炸", code: "21", groupid: "4028ab2",},
                {id: "407", name: "四个王", code: "22", groupid: "4028ab2",},
                {id: "408", name: "7炸", code: "23", groupid: "4028ab2",},
                {id: "4010", name: "8炸", code: "24", groupid: "4028ab2",},
            ],
        };

        this.optionGroupLst = [];

        this.initTestParams();

        this.confirmBtn.on(cc.Node.EventType.TOUCH_START, function (e) {
            console.log('poker TOUCH_START:' + e.getLocationX() + "---" + e.getLocationY());
            this.onConfirmClick();
            this.closeOpenWin();
            e.stopPropagation();
        }, this);

    },
    initTestParams: function () {


        if (this.optionGroupPrefab != null) {

            while (this.optionGroupLst.length) {
                var tmp = this.optionGroupLst.pop();
                tmp.destroy();
            }


            var height = 140;
            for (var inx = 0; inx < this.paramsData.groups.length; inx++) {
                var group = cc.instantiate(this.optionGroupPrefab);
                var optionGroup = group.getComponent("OptionsGroup");
                optionGroup.init(this.paramsData.groups[inx], this.optionPrefab, this.paramsData.items);
                var selfPosition = cc.v2(0, height - 30);
                group.setPosition(selfPosition);
                group.parent = this.paramContainer;
                height -= optionGroup.height - 20;
                this.optionGroupLst.push(optionGroup);
            }

        }
    },
    onConfirmClick: function () {

        var number = "";
        var cardType = "";
        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            if (groupitem.getGroupData()) {
                if (groupitem.getGroupData().code == "numbers") {
                    number = this.getCheckedValue(groupitem.groupoptions);
                } else if (groupitem.getGroupData().code == "cardtype") {
                    cardType = this.getCheckedValue(groupitem.groupoptions);
                }
            }
            var value = "";

        }
        cc.log(number + "---" + cardType);
        var params = {
            usernumtype:cc.mile.user.id + ":" + number + ":" + cardType,
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        }
        this.emitSocketRequest("innertest", JSON.stringify(params));

    },

    getCheckedValue: function (groupoptions) {
        for (var j = 0; j < groupoptions.length; j++) {
            var option = groupoptions[j];
            if (option.checked == true) {
                return option.item.code;
            }
        }
        return "";
    }

    // update (dt) {},
});
