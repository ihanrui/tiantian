var beiMiCommon = require("MileCommon");
var GameUtil = require("WangdaGameUtils");

var maziBeginSelf = null;

cc.Class({
    extends: beiMiCommon,
    properties: {
        //准备(准备按钮)
        gameReadybtn: {
            default: null,
            type: cc.Node
        },
        //准备(点击准备后的文字)

        startCardBg: {
            default: null,
            type: cc.Node
        },
        maziStartAnimation: {
            default: null,
            type: cc.Animation
        },
        maziStartNode: {
            default: null,
            type: cc.Node
        },
        continuegamebtn: {
            default: null,
            type: cc.Node
        },
        poker: {
            default: null,
            type: cc.Node
        },
        lastCardsPanel: {   //底牌
            default: null,
            type: cc.Node
        },
        waitting: {
            default: null,
            type: cc.Prefab
        },
        ratio: {   //底牌
            default: null,
            type: cc.Label
        },
        summary_win: {
            default: null,
            type: cc.Prefab
        },
        summary: {
            default: null,
            type: cc.Prefab
        },
        inviteplayerPrefab: {
            default: null,
            type: cc.Prefab
        },
        inviteplayerContainer: {
            default: null,
            type: cc.Node
        },
        player_top: {
            default: null,
            type: cc.Prefab
        },
        player_left: {
            default: null,
            type: cc.Prefab
        },
        player_bottom: {
            default: null,
            type: cc.Prefab
        },
        player_right: {
            default: null,
            type: cc.Prefab
        },
        playOptionsContainer: {
            default: null,
            type: cc.Node
        },
        newsOptionsContainer: {
            default: null,
            type: cc.Node
        },
        buchuBtn: {
            default: null,
            type: cc.Button
        },
        //闹钟
        clock: {
            default: null,
            type: cc.Node
        },
        //闹钟数字图片集
        clocknumAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        //闹钟数字1
        clocknum1: {
            default: null,
            type: cc.Node
        },
        //闹钟数字2
        clocknum2: {
            default: null,
            type: cc.Node
        },
        selfIndex: -1,
        selfPosition: null,
        roomReady: false,
    },

    // use this for initialization
    onLoad: function () {

        //保存全局对象

        maziBeginSelf = this;
        cc.gvoiceDownloadCallbackJS = this.gvoiceDownloadCallbackJS;
        cc.gvoicePlayFinishedCallbackJS = this.gvoicePlayFinishedCallbackJS;
        /**
         * 适配屏幕尺寸

         */
        this.resize();
        this.playerRender = new Array();     //存放玩家渲染数据
        this.summarypage = null;
        this.selfPoker = this.getCommon("MaziPoker"); // 存放自己的牌管理对象

        this.inited = false;
        this.lasttip = null;
        this.voiceData = {};

        this.takecardFinish = true;

        var beginTime = Date.now();
        cc.log("data.开始初始化 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");

        this.checkValidateCount = 0;

        if (cc.mile != null) {
            if (cc.mile.gamestatus != null && cc.mile.gamestatus == 'play') {
                //恢复数据
                this.updateInviteplayerBtnViewStatus(false);
                this.recovery();
            } else {
                this.updateInviteplayerBtnViewStatus(true);
            }
            this.initgame();
        }


    },
    /// 点击开始，发送状态到server端
    begin: function () {

        this.resetPlayerStatus();
        this.resetSelfPlayerStatus();

        //清空所有人的信息
        this.clearAllPlayCardLayer();
        //清楚自己的牌信息
        var pokers = this.getCommon("MaziPoker");
        pokers.clearSefAllPokers();
        this.clock.active = false;

        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        };
        this.emitSocketRequest("start", JSON.stringify(param));

    },
    //// 进入房间需要恢复数据，有个倒计时跟新
    recovery: function () {
        this.statictimer("正在恢复数据，请稍候", cc.mile.data.waittime);
    },
    // 更新 准备 按钮状态
    updateBeginBtnViewStatus: function (status) {
        this.gameReadybtn.active = status;
    },// 更新 准备 按钮状态
    updateInviteplayerBtnViewStatus: function (status) {
        this.inviteplayerContainer.active = status;
    },
    //  初始化房间，注册socket 监听事件
    initgame: function () {
        cc.log("初始化游戏  this.ready() 【" + this.ready() + "】")
        if (this.ready()) {
            this.updateBeginBtnViewStatus(true);
            this.game = this.getCommon("MaziDataBind");
            this.initSocketMapping();
            this.initSocketEvent();
            /**
             *   此处有个问题：游戏中，如果点击了返回，进入过其他游戏，这个 cc.mile.extparams.playway
             *   就发生了改变，会导致当前游戏结束后，继续游戏的时候会出现问题
             * @type {{token: (*|null), playway, orgi, extparams: *}}
             */

            this.inited = true;

            this.emitJoinEvent();


            //进入后台
            if (cc && cc.eventManager) {
                var self = this;
                cc.eventManager.addCustomListener("socketReconnect", function (event) {
                    cc.log('-----socketReconnect-----后台更新了--!');
                    if (self) {
                        self.initSocketEvent();
                    }
                });
            }
        }
        var beginTime = Date.now();
        cc.log("data.结束初始化 消耗时间 " + beginTime + "[" + Math.floor((beginTime - cc.mile.beginTime) / 1000) + "]");
        // 初始化邀请好友
        var invitePlayer = cc.instantiate(this.inviteplayerPrefab);
        invitePlayer.parent = this.inviteplayerContainer;
    },
    emitJoinEvent: function () {

        var teanum = "";
        if (cc.mile.teahouse) {
            teanum = cc.mile.teahouse.teanum;
        }

        var param = {
            token: cc.mile.authorization,
            playway: cc.mile.extparams.playway,
            orgi: cc.mile.user.orgi,
            extparams: cc.mile.extparams,
            roomtype: cc.mile.createroomtype,
            teanum: teanum,
            tablenum: cc.mile.tablenum,
        };
        this.emitSocketRequest("joinroom", JSON.stringify(param));

    },

    initSocketEvent: function () {

        var self = this;
        var socket = this.socket();

        cc.log('-----initSocketEvent-------：command_room');

        socket.on("command_room", function (result) {

            if (self.inited == true) {

                try{
                    var data = self.parse(result);

                    cc.log("事件 data.command [" + data.command + "]  数据内容为:  " + JSON.stringify(result));
                    self.loggerInfo("接收事件:"+data.command  + ",时间："+Date.now());

                    self.route(data.command)(data, self);

                }catch (e){
                    self.loggerError('响应房间事件出错：' + cc.mile.user.username
                        + JSON.stringify(e) + result);

                }
            }
        });
        /**
         * 心跳检查，服务端发起的事件，服务端可以设置 PING的时长和 PING的 TimeOut
         */
        socket.on("ping", function (event) {
            cc.log('-----ping-------：' + JSON.stringify(event));
        });


    },
    initSocketMapping: function () {
        this.map("joinroom", this.joinroom_event);          //加入房间
        this.map("players", this.players_event);            //接受玩家列表
        this.map("takecards", this.takecards_event);            //出牌信息
        this.map('play', this.play_event);                      //接受玩家列表
        this.map("cardtips", this.cardtips_event);              //提示 出牌
        this.map("settlecards", this.settlecards_event);        //理牌
        this.map("seescore", this.seescore_event);              //看分
        this.map("remembercard", this.remembercard_event);              //记牌器
        this.map("seecards", this.seecards_event);              //明牌
        this.map("roomready", this.roomready_event);              //所有玩家准备
        this.map("playerready", this.playerready_event);            //玩家点击了开始游戏 ， 即准备就绪
        this.map("recovery", this.recovery_event);              //恢复牌局数据
        this.map("sendmsg", this.sendmsg_event);              //恢复牌局数据
        this.map("leave", this.leave_event);              //恢复牌局数据
        this.map("reportnews", this.reportnews_event);              //报喜
        this.map("stepclean", this.stepclean_event);              //单小结
        this.map("clean", this.clean_event);              //总结算
        this.map("roomlinestatus", this.roomlinestatus_event);              //玩家状态更新
        this.map("teadissroom", this.teadissroom_event);              //玩家状态更新
        this.map("mazigiveup", this.mazigiveup_event);              //投降
        this.map("giveuptest", this.giveuptest_event);              //测试
        this.map("dissroom", this.dissroom_event);              //小结
        this.map("createroom", this.createroom_event);              //创建房间，房卡不足
    },
    /**
     * 新创建牌局，首个玩家加入，进入等待状态，等待其他玩家加入，服务端会推送 players数据
     * @param data
     * @param context
     */
    joinroom_event: function (data, context) {
        debugger
        cc.log("data.cardroom [" + data.cardroom + "]context.inviteplayer [" + context.inviteplayer + "]");
        context.closeloadding();

        if (data.cardroom == true && context.inviteplayer != null) {

            context.game.roomnum_format(data);

        }
        cc.log("加入房间玩家id data.player.id [" + data.player.id + "] data.roomid[" + data.roomid + "]");

        cc.mile.maxplayers = data.maxplayers;
        cc.mile.curpalyers = data.curpalyers;
        if (data.player.id && data.player.id == cc.mile.user.id) {

            cc.mile.gamestatus = data.gamestatus;
            context.initSelfData(data.player);
            context.game.roomnum_format(data);

        } else {
            ///其他玩家加入，初始化、判断新加入的玩家是否已在房间中
            context.initPlayerView(data.player, false);

        }

        cc.mile.joinroomFinish = true;

    },
    initPlayerView: function (playerData, isRecover) {
        var playerid = playerData.playuser ? playerData.playuser : playerData.id;
        var position = this.getPlayerPosition(playerid, playerData.index);
        cc.log(position + ":playerid:" + playerid + "---initPlayerView:==" + JSON.stringify(playerData));
        if (position != "") {
            this.newplayer(playerData, position);
            if (isRecover) {
                this.recover_singleplay_status(playerData, isRecover);
            }
        } else {

            this.recover_singleplay_status(playerData, isRecover);
        }
    },
    continueGameStatusInit: function () {
        if (this.selfPoker) {
            this.selfPoker.clearAllPokers();
            this.selfPoker.clearSefAllPokers();
        }
        this.clock.active = false;
        this.updateBeginBtnViewStatus(true);
        if (cc.mile.gamestatus == 'play') {
            this.updateInviteplayerBtnViewStatus(false);
        } else {
            this.updateInviteplayerBtnViewStatus(true);
        }
        this.setPlayOptionStatus(false, false, false);
    },
    /**
     * joinroom  清理桌面信息
     * @param data
     */

    clearTableInfo: function () {
        for (var inx = this.playerRender.length - 1; inx >= 0; inx--) {
            var render = this.playerRender[inx];
            render.node.destroy();
            this.playerRender.pop();
        }
        if (this.selfPlayerRender) {
            this.selfPoker.clearAllPokers();
            this.selfPlayerRender.cleanToReady(true);
            this.selfPlayerRender.updateFen(0); // 得分
        }
    },
    initSelfData: function (playerData) {

        cc.log("initSelfDataffff:" + playerData);
        cc.log("initSelfData:" + JSON.stringify(playerData))
        if (playerData.playuser) {
            playerData.id = playerData.playuser;
        }
        if (!this.selfPlayerRender) {

            var player = cc.instantiate(this.player_bottom);
            var selfPosition = cc.v2(-484, -302.6);
            var selfHeadPosition = cc.v2(-570, -290);
            var chatAnimation = this.getCommon("ChatAnimation");
            chatAnimation.initSelfPosition(selfHeadPosition);

            this.selfPosition = selfHeadPosition;
            player.setPosition(selfPosition);
            player.parent = cc.find("Canvas/global");
            this.selfPlayerRender = player.getComponent("PlayerRender");
        }


        this.clearTableInfo();

        this.continueGameStatusInit();

        this.updateUserReadyStatus(playerData.id, playerData.gamestatus);

        //this.selfPlayerRender.showSettlement(-191919000022);
        this.selfPlayerRender.initplayer(playerData, selfHeadPosition);


        //var maziPoker = this.getCommon("MaziPoker");
        //maziPoker.loadcards([91,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27],true);


        this.selfIndex = playerData.index; //当前玩家所在位置

        cc.log("initSelfData:" + this.selfIndex);

    },
    // 根据index  获取玩家的 位置
    getPlayerPosition: function (playerId, playerIndex) {
        var inroom = false;
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            cc.log(render.userid + ":----getPlayerPosition----:" + playerId);
            if (render.userid == playerId) {
                inroom = true;
                break;
            }
        }
        if (!inroom) {
            var playUpdateIndex = (playerIndex - this.selfIndex + 4) % 4;
            cc.log(playerIndex + ":----getPlayerPosition----:" + playUpdateIndex + "--selfIndex--" + this.selfIndex);
            switch (playUpdateIndex) {
                case 1:
                    return "right";
                case 2:
                    return "top";
                case 3:
                    return "left";
            }
        }
        return "";
    },
    /**
     * 房间大家都准备好了
     *
     * @param data
     * @param context
     */
    roomready_event: function (data, context) {
        cc.log("roomready_event [" + data + "]");

        context.updateInviteplayerBtnViewStatus(false);


    },
    /**
     *
     * 玩家点击 准备按钮
     * @param data
     * @param context
     */
    playerready_event: function (data, context) {
        cc.log("playerready_event [" + data + "]");
        //cc.mile.gamestatus = "ready";
        context.updateUserReadyStatus(data.userid, "ready");
    },
    /**
     *  更新玩家的状态，
     * @param userid
     * @param readyStatus   gamestatus
     */
    updateUserReadyStatus: function (userid, ready_status) {

        var readyStatus = ready_status == "ready" ? true : false;
        var waittingStatus = ready_status == "waitting" ? true : false;


        var render = this.getUserRenderById(userid);
        render.updateReadyStatus(readyStatus);
        render.cleanToReady(readyStatus);
        //render.showSeecards([1,2,3,4,5,6,72,22,33,44,66,77,2,12,8,9,10,44,55,77,88])

        if (userid == cc.mile.user.id) {
            if (readyStatus) { // 如果是自己，准备了则清楚poker的牌
                this.updateBeginBtnViewStatus(false);
                this.selfPoker.clearAllPokers();
                this.game.updateMingpaiViewStatus(false);
            }
            this.updateBeginBtnViewStatus(waittingStatus);
        }
    },
    /**
     * 初始化玩家信息
     *
     * @param data
     * @param context
     */
    players_event: function (data, context) {
        cc.log("players_event : " + data);

        for (var i = 0; i < data.player.length; i++) {


            if (data.player[i].id == cc.mile.user.id) {
                continue;
            }
            //从房间创建者开始 循环
            var position = context.getPlayerPosition(data.player[i].id, data.player[i].index);
            if (position != "") {
                context.newplayer(data.player[i], position);
            }
            context.updateUserReadyStatus(data.player[i].id, data.player[i].gamestatus);
        }

    },
    /**
     * 判断玩家是否在界面已经初始化
     * @param playerId
     * @returns {boolean}
     */
    playerexist: function (playerId) {
        var inroom = false;
        if (player.id == cc.mile.user.id) {
            inroom = true;
        } else {
            for (var j = 0; j < this.playerRender.length; j++) {
                if (this.player[j].userid == playerId) {
                    inroom = true;
                    break;
                }
            }
        }
        return inroom;
    },
    /**
     *
     * 发送消息给服务端
     *
     source，源用户ID
     target，目标用户ID
     msgtype，消息类型 0，快捷语，1.语音，2.表情，4.文字消息
     sendtype，发送类型 0.单个发送，1.群发
     msgid，消息ID
     msgname，消息名称
     msgcontent，消息内容
     memo，消息说明
     *
     * @param msg
     */
    sendmsg: function (msg) {
        msg.token = cc.mile.authorization;
        this.emitSocketRequest("sendmsg", JSON.stringify(msg));
    },
    /**
     * 有用户离开时间
     * @param data
     * @param context
     */
    leave_event: function (data, context) {
        cc.log(data);

        if (data.userid == cc.mile.user.id) {
            if (data.changeroom == '1') {
                cc.log('换桌不错处理！');
            } else {
                context.eventLeaved = true;
                if (cc.mile.teahouse) {
                    context.scene("floor", context);
                } else {
                    context.scene("mazihall", context);
                }
            }
        } else {
            var render = context.getUserRenderById(data.userid);
            if (render != null) {
                for (var i = 0; i < context.playerRender.length; i++) {
                    if (render.userid == context.playerRender[i].userid) {
                        context.playerRender.splice(i, 1);
                        break;
                    }

                }
                render.node.destroy();
            }

        }
        if(cc.mile.curpalyers && cc.mile.curpalyers > 0){
            cc.mile.curpalyers--;
        }
    },
    /**
     * 用户报喜事件处理
     * @param data
     * @param context
     */
    reportnews_event: function (data, context) {
        var userNews = data.ls;
        if (userNews) {
            for (var i = 0; i < userNews.length; i++) {
                var userId = userNews[i].userid;
                var score = userNews[i].newscore;
                var totalScore = userNews[i].totalscore;
                var render = context.getUserRenderById(userId);

                render.showSettlement(score);
                render.setFenSettlement(totalScore);
            }
            context.game.baoxi_event(data);
        }
        if (data.userid == cc.mile.user.id) {
            context.newsflag = data.newsflag;
        }
        if (!context.newsflag) { //  如果有喜，则继续报喜； 否则显示用户的  出牌按钮
            if (context.currentPlaycardUserId == cc.mile.user.id) {
                context.setPlayOptionStatus(true, context.playAutomic, data.newsflag);
            } else {
                if (data.userid == cc.mile.user.id) {
                    context.setPlayOptionStatus(false, context.playAutomic, context.newsflag);
                } else {
                    context.setPlayOptionStatus(false, context.playAutomic, data.newsflag);

                }
            }
        }

    },
    /**
     *  隐藏用户 报喜的 结算信息
     */
    hideUserSettlementInfo: function () {
        for (var inx = 0; inx < this.playerRender.length; inx++) {
            var render = this.playerRender[inx];
            render.updateSettlementViewStatus(false);
        }
        this.selfPlayerRender.updateSettlementViewStatus(false);
    },
    /**
     * 每小局的结算事件处理
     * @param data
     * @param context
     */
    stepclean_event: function (data, context) {
        var stepEvents = data.stepEvents;
        context.currentPlaycardUserId = null;
        for (var i = 0; i < stepEvents.length; i++) {
            var userId = stepEvents[i].userid;
            var stepscore = stepEvents[i].stepscore;
            var totalscore = stepEvents[i].totalscore;
            var render = context.getUserRenderById(userId);
            render.showSettlement(stepscore);
            render.setFenSettlement(totalscore);
            //render.clearPlayCardLayer();
            var cards = context.decode(stepEvents[i].cards);
            cc.log(cards);
            render.showSeecards(cards);
        }
        context.updateBeginBtnViewStatus(true);
        context.setPlayOptionStatus(false, false, false);


    },
    /**
     * 每大局的结算信息
     * @param data
     * @param context
     */
    clean_event: function (data, context) {
        context.currentPlaycardUserId = null;
        context.game.showJiesuanDialog(data);
        if (cc.mile.createroomtype != 'coin') {
            context.emitJoinEvent();
        }
    },
    /**
     * 离线 事件监听
     * @param data
     * @param context
     */
    roomlinestatus_event: function (data, context) {
        cc.log("data.linestatus [" + data + "]");
        if (data.userid) {
            var render = context.getUserRenderById(data.userid);
            if (render) {
                render.updateOfflineViewStatus(data.online);
            }
            if (data.online) {
                context.updateUserReadyStatus(data.userid, data.gamestatus);
            }
            if (data.userid == cc.mile.user.id && data.online) {

                context.emitRecoveryEvent();

            }
        }
    },
    emitRecoveryEvent: function () {
        cc.log("请求恢复数据");
        if (cc.mile.extparams && cc.mile.extparams.playway) {
            //this.showCenterToast("正在同步最新数据");
            var teanum = "";
            var tablenum = "";
            if (cc.mile.teahouse) {
                teanum = cc.mile.teahouse.teanum;
            }
            if (cc.mile.tablenum) {
                tablenum = cc.mile.tablenum;
            }
            var param = {
                token: cc.mile.authorization,
                playway: cc.mile.extparams.playway,
                orgi: cc.mile.user.orgi,
                extparams: cc.mile.extparams,
                teanum: teanum,
                tablenum: tablenum,
            };
            this.emitSocketRequest("recovery", JSON.stringify(param));
        }
    },
    /**
     *
     * @param data
     * @param context
     */
    teadissroom_event: function (data, context) {
        cc.log("data.linestatus [" + data + "]");

    },
    /**
     * 投降
     * @param data
     * @param context
     */
    giveuptest_event: function (data, context) {

        cc.log("data.linestatus [" + data + "]");
        cc.log("context.linestatus [" + context + "]");
        if (data.flag) {
            context.game.showTestDialog(data);
        }
    }, /**
     * 投降
     * @param data
     * @param context
     */
    mazigiveup_event: function (data, context) {
        debugger;
        //同意投降
        if (data.giveupflag == 1) {
            context.game.disGiveUpAgreeDissroom(data);
        }
        //拒绝投降
        else if (data.giveupflag == 2) {
            context.game.dissGiveUpDissroom(data);
        }
        else {
            context.game.showGiveUpDialog(data, false);
        }
        cc.log("data.linestatus [" + data + "]");
        cc.log("context.linestatus [" + context + "]");
        //if (data.flag) {
        //    context.game.showTestDialog(data);
        //}
    },
    /**
     * 消息监听
     * @param data
     * @param context
     */
    sendmsg_event: function (data, context) {
        cc.log("data.cardroom [" + data + "]");
        if (data.msgtype == '1' && data.sendtype == '0') {
            context.playVoiceMsg(data);
        } else if (data.msgtype == '2' && data.sendtype == '0') {
            context.sendChatAnimation(data);
        } else if ((data.msgtype == '1' || data.msgtype == '4') && data.sendtype == '1') {
            context.playFasticoMsg(data, context);
        } else if (data.msgtype == '5' && data.sendtype == '1') {
            context.playSelfEmojiAnimation(data);
        }
        context.validateUserInfoStatus(data);
    },
    validateUserInfoStatus: function (data) {

        cc.log("数据状态恢复：" + this.currentPlaycardUserId);
        if (data.nextplayer) {
            if (this.currentPlaycardUserId != data.nextplayer) {
                this.checkValidateCount++;
                if (this.checkValidateCount > 2) {
                    this.emitRecoveryEvent();
                    this.checkValidateCount = 0;
                }
            } else {
                this.checkValidateCount = 0;
            }
        }
        if (data.source) {
            var render = this.getUserRenderById(data.source);
            cc.log("数据渲染：" + JSON.stringify(render.player));

            if (!render.getOffilineStatus()) {
                this.emitRecoveryEvent();
            }
        }
    },
    playSelfEmojiAnimation: function (data) {

        var render = this.getUserRenderById(data.source);
        if (render != null) {
            render.playUserEmojAnimation(data.msgcontent, data.msgcontent);
        }
    },
    /**
     * 发送语音表情动画
     * @param data
     */
    sendChatAnimation: function (data) {

        var playerSource = this.getUserPositionById(data.source);
        if (playerSource) {

            var chatAnimation = this.getCommon("ChatAnimation");
            var playerTarget = this.getUserPositionById(data.target);
            if (playerTarget == null) {
                playerTarget = this.selfPosition;
            }
            chatAnimation.setSourcePosition(playerSource);
            chatAnimation.setTargetPosition(playerTarget);
            chatAnimation.sendPlayAnimation(data.source, data.msgid);
        }
    },
    //快捷语音
    playFasticoMsg: function (data, context) {
        var render = this.getUserRenderById(data.source);
        if (render != null) {
            if (data.msgname != "") {
                cc.mile.audio.playSFX(data.msgname);
            }
            var width = data.msgcontent.length * 25;
            width = width > 100 ? width : 100;
            var _x = 0;
            if (width > 100) {
                _x = (width - 100) / 2;
            }
            var messageX = -24;
            if (render.node._name == "player_top") {
                messageX = 133;
            } else if (render.node._name == "player_right") {
                messageX = -70;
                _x = _x * -1;
            } else if (render.node._name == "player_left") {
                messageX = 64;
            }
            render.message.setPositionX(messageX + _x);
            render.message.width = width;
            render.messageLab.node.width = width - 20;
            render.onOpenMessage(data.msgcontent);

        }
    },
    // 发送语音
    playVoiceMsg: function (data) {

        cc.log('playVoiceMsg:' + JSON.stringify(data));

        if (data.source == cc.mile.user.id) {
            this.stopAllUserVoiceAnimationStatus(false);
            this.voiceData = {};
        }


        if (!this.checkIsPlayRepeat(data.msgid, data.source)) {

            //-------this.stopAllUserVoiceAnimationStatus(false);
            cc.log(JSON.stringify("缓存语音信息:" + this.voiceData));

            if (Object.keys(this.voiceData).length == 1) {
                this.callNativePlayVoice(data.msgid);
            }


        }

    },
    callNativePlayVoice: function (msgid) {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "playVoice", "(Ljava/lang/String;)V", msgid);

        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

            jsb.reflection.callStaticMethod("AppController",
                "playVoice:", msgid);

        }

    },
    checkIsPlayRepeat: function (msgid, source) {
        if (this.voiceData) {
            if (this.voiceData[msgid]) {
                return true;
            } else {
                if (this.voiceData.length > 10) {
                    this.voiceData = {};
                }
                this.voiceData[msgid] = source;
            }
        } else {
            this.voiceData[msgid] = source;
        }
        return false;
    },

    gvoiceDownloadCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {

            cc.audioEngine.pauseAll();

            //maziDataBind.sendMsgByUploadFileId(data.fileID);
            var sourceid = maziBeginSelf.voiceData[data.fileID];
            if (sourceid) {
                maziBeginSelf.updateUserVoiceAnimationStatus(sourceid, true);
            }

        } else {
            if (data && data.callbackMsg) {
                maziBeginSelf.showCenterToast(data.callbackMsg);
            }
        }
        cc.log('下载回调数据:' + JSON.stringify(data));


    },


    gvoicePlayFinishedCallbackJS: function (data) {
        cc.log('原生代码回调gvoiceUploadCallbackJS：' + JSON.stringify(data));

        if (data.status) {
            var sourceid = maziBeginSelf.voiceData[data.fileID];
            if (sourceid) {
                maziBeginSelf.updateUserVoiceAnimationStatus(sourceid, false);
            }

        } else {
            if (data && data.callbackMsg) {
                maziBeginSelf.showCenterToast(data.callbackMsg);
            }
        }

        cc.log('播放完成回调数据:' + JSON.stringify(data));
        cc.log('回调之前:' + JSON.stringify(maziBeginSelf.voiceData));

        // 删除对应的key 值
        delete maziBeginSelf.voiceData[data.fileID];

        cc.log('清理之后:' + JSON.stringify(maziBeginSelf.voiceData));

        var voiceIds = Object.getOwnPropertyNames(maziBeginSelf.voiceData);

        cc.log('清理之后:' + voiceIds);

        if (voiceIds.length > 0) {
            maziBeginSelf.callNativePlayVoice(voiceIds[voiceIds.length - 1]);
        } else {
            cc.audioEngine.resumeAll();
        }

    },

    // 更新语音 animation 的状态
    updateUserVoiceAnimationStatus: function (targetUserId, status) {
        var render = this.getUserRenderById(targetUserId);
        render.updateVoiceAnimationStatus(status);
    },
    // 更新语音 animation 的状态
    stopAllUserVoiceAnimationStatus: function (status) {

        /* if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
             jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                 "stopPlayVoice", "()V");
         } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
             jsb.reflection.callStaticMethod("AppController", "stopPlayVoice");
         }*/

        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            render.updateVoiceAnimationStatus(status)
        }
        this.selfPlayerRender.updateVoiceAnimationStatus(status);
    },
    /**
     * 接收到服务端的 恢复牌局的数据 恢复牌局
     * @param data
     * @param context
     */
    recovery_event: function (data, context) {
        //恢复自己的牌
        cc.log("recovery_event --11:" + data);
        if (data.player && data.player.playuser == cc.mile.user.id) {

            context.initSelfData(data.userboard.player);

            cc.log("recovery_event 解析玩家牌--22:" + data);
            var mycards = context.decode(data.player.cards);
            cc.log("recovery_event --33:" + mycards);
            if (context.waittimer != null && context.waittimer._sgNode != null) {
                var timer = context.waittimer.getComponent("MileTimer");
                if (timer) {
                    timer.stop(context.waittimer);
                }
            }
            context.game.endSoundRecording();
            context.stopAllUserVoiceAnimationStatus();

            context.closeOpenWin();

            cc.log("recovery_event 恢复玩家状态--44:");
            context.recover_game_status(data); // 恢复玩家状态

            cc.log("recovery_event 恢复桌面牌--55:");
            context.recover_desk_cards(mycards); // 恢复桌面的牌

            cc.log("recovery_event 恢复闹钟 66:" + data.nextplayer);
            context.showClock(data.nextplayer); // 显示闹钟


            cc.mile.gamestatus = data.userboard.gamestatus;

            context.game.roomnum_format(data.userboard);

            context.recoverGiveUpDialog(data.userboard);
            context.recoverJiesanDialog(data.userboard);
        }


    },
    recover_game_status: function (data) {
        cc.log("recover_game_status --11:" + data);
        var selfPlay = data.player;
        this.newsflag = selfPlay.newsflag;
        cc.log("recover_game_status --恢复自己的按钮状态--");

        this.updateUserReadyStatus(selfPlay.playuser, selfPlay.gamestatus);

        cc.log("recover_game_status --恢复自己的头像及得分--");
        this.recover_singleplay_status(selfPlay, true);

        cc.log("recover_game_status 111--33:" + data);

        var otherPlays = data.userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            if (otherPlays[i].playuser) {
                otherPlays[i].id = otherPlays[i].playuser;
            }
            this.initPlayerView(otherPlays[i], true);

            //this.recover_singleplay_status(otherPlays[i], true);
        }
        cc.log("recover_game_status --33:" + data);

        if (selfPlay.gamestatus == 'play') {// 只有为 游戏中的状态
            cc.log("recover_game_status --44:" + data);
            var bankerId = data.banker.userid;
            this.inviteplayerContainer.active = false;
            if (data.nextplayer == cc.mile.user.id) { // 如果当前玩是自己
                this.setPlayOptionStatus(true, data.automic, selfPlay.newsflag);
            } else if (cc.mile.user.id == bankerId && selfPlay.cards.length == 27) {
                // 庄家是自己，且还没有出牌
                this.setPlayOptionStatus(true, true, selfPlay.newsflag);
            }
            cc.log("recover_game_status --22-44:" + data);
        } else {

            this.setPlayOptionStatus(false, true, false);

        }
        cc.log("recover_game_status fffff99999--55:" + data);

        if (data.last) {
            var nextIndex = this.getRecoveryNextPlayerIndex(data);

            for (var otherIndex = 0; otherIndex < otherPlays.length; otherIndex++) {
                if (otherPlays[otherIndex].takecards
                    && data.nextplayer != otherPlays[otherIndex].userid) {
                    this.playOutCards(otherPlays[otherIndex].takecards, true);
                }
            }
            if (selfPlay.takecards && data.nextplayer != selfPlay.userid) {
                this.playOutCards(selfPlay.takecards, true);
            }
            cc.log("recover_game_status tttt--66:" + data.last);
        }

    },

    recoverGiveUpDialog: function (userboard) {
        cc.log("recoverGiveUpDialog--111:" + userboard.giveupuserid);

        if (userboard && userboard.giveupuserid) {
            var data = this.getRecoverGiveupData(userboard);
            cc.log("recoverGiveUpDialog--222:" + JSON.stringify(data));
            var game = this.getCommon("MaziDataBind");
            game.showGiveUpDialog(data, true);

        }

    },

    recoverJiesanDialog: function (userboard) {
        cc.log("recoverJiesanDialog--111:" + userboard.dissuserid);
        if (userboard && userboard.dissuserid) {
            var data = this.getRecoverJiesanData(userboard);
            cc.log("recoverJiesanDialog--222:" + JSON.stringify(data));
            var game = this.getCommon("MaziDataBind");
            game.showJiesanDialog(data, true);
        }
    },


    getRecoverGiveupData: function (userboard) {
        var jiesanData = {
            giveupInfoList: [],
            userid: userboard.giveupuserid,
            giveupsecond: userboard.giveupleftsecond
        };

        var otherPlays = userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            jiesanData.giveupInfoList.push({
                userid: otherPlays[i].userid,
                username: otherPlays[i].username,
                userurl: otherPlays[i].userurl,
                giveupflag: otherPlays[i].giveupflag
            });
        }

        jiesanData.giveupInfoList.push({
            userid: userboard.player.userid,
            username: userboard.player.username,
            userurl: userboard.player.userurl,
            giveupflag: userboard.player.giveupflag
        });

        return jiesanData;
    },


    getRecoverJiesanData: function (userboard) {
        var jiesanData = {
            dissroomInfoList: [],
            userid: userboard.dissuserid,
            disssecond: userboard.dissleftsecond
        };

        var otherPlays = userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            jiesanData.dissroomInfoList.push({
                userid: otherPlays[i].userid,
                userurl: otherPlays[i].userurl,
                username: otherPlays[i].username,
                dissflag: otherPlays[i].dissflag
            });
        }

        jiesanData.dissroomInfoList.push({
            userid: userboard.player.userid,
            userurl: userboard.player.userurl,
            username: userboard.player.username,
            dissflag: userboard.player.dissflag
        });
        return jiesanData;
    },
    getRecoveryNextPlayerIndex: function (data) {
        var nextplayerId = data.nextplayer;
        var otherPlays = data.userboard.players;
        for (var i = 0; i < otherPlays.length; i++) {
            if (otherPlays[i].playuser == nextplayerId) {
                return otherPlays[i].index;
            }
        }
        return data.player.index;
    },

    /**
     * 恢复单个用户的状态
     * @param playdata
     */
    recover_singleplay_status: function (playdata, isRecovery) {

        cc.log('恢复单个用户的状态:' + JSON.stringify(playdata));
        var playerId = playdata.playuser ? playdata.playuser : playdata.id;
        var playRender = this.getUserRenderById(playerId);
        if (playRender) {

            if (playdata.hasOwnProperty('banker')) {
                playRender.setMasterFlag(playdata.banker);
            }
            if (playdata.hasOwnProperty('score')) {

                playRender.updateZhuafen(playdata.score);
            }

            if (playdata.hasOwnProperty('totalscore')) {

                playRender.updateFen(playdata.totalscore);
            }

            if (playdata.hasOwnProperty('cardsnum')) {
                var baojingStatus = false;
                if (playdata.cardsnum <= 5 && playdata.cardsnum > 0) {
                    baojingStatus = true;
                }
                playRender.updateBaojinViewStatus(baojingStatus, playdata.cardsnum);// 更新报警

            }

            if (playdata.lvl != 0) {

                playRender.showWinTag(playdata.lvl, true);
            }

            if (playdata.hasOwnProperty('cards')) {
                try {

                    var cards = this.decode(playdata.cards);
                    playRender.showSeecards(cards);
                    if (playdata.playuser == cc.mile.user.id && playdata.seeflag) {
                        this.game.updateMingpaiViewStatus(true);
                    }
                } catch (e) {
                    cc.log('解析牌出问题:' + JSON.stringify(e));
                    cc.log('解析用户信息:' + JSON.stringify(playdata));
                }
            } else {
                playRender.clearMingPaiContainer();
            }
            playRender.updateOfflineViewStatus(playdata.online);
            var readyStatus = (playdata.gamestatus == "ready" && cc.mile.gamestatus != 'play') ? true : false;
            playRender.updateReadyStatus(readyStatus);
        }
    },
    /**
     * 重置用户的状态，
     */
    resetPlayerStatus: function () {
        for (var i = 0; i < this.playerRender.length; i++) {
            var playRender = this.playerRender[i];
            playRender.updateWinTagViewStatus(false);
            playRender.updateBaojinViewStatus(false, 0);
            playRender.updateSettlementViewStatus(false);
        }

    },
    /**
     * 重置自己的状态
     */
    resetSelfPlayerStatus: function () {
        this.selfPlayerRender.updateWinTagViewStatus(false);
        this.selfPlayerRender.updateBaojinViewStatus(false, 0);
        this.selfPlayerRender.updateSettlementViewStatus(false);

    },
    /**
     * 恢复自己的牌
     * @param mycards
     */
    recover_desk_cards: function (mycards) {

        this.startCardBg.active = false;

        var pokers = this.getCommon("MaziPoker");
        if (!pokers) {
            pokers = this.selfPoker;
        }
        //this.getready.active = false;
        pokers.clearAllPokers();
        pokers.loadcards(mycards, true);
    },
    /**
     * 看牌操作
     * @param data
     * @param context
     */
    seescore_event: function (data, context) {
        var scoreCards = [];
        if (data.cards && data.cards.length > 0) {
            scoreCards = data.cards;
        }
        context.game.showSeescoreDialog(scoreCards);
    },
    /**
     * 记牌器操作
     * @param data
     * @param context
     */
    remembercard_event: function (data, context) {
        var scoreCards = [];
        if (data.history && data.history.length > 0) {
            scoreCards = data.history;
        }
        context.game.showSeeCardsDialog(scoreCards);
    },
    /**
     * 玩家出牌信息
     * @param data
     * @param context
     */
    takecards_event: function (data, context) {

        context.takecardsActionSchedule(data);
    },
    takecardsActionSchedule: function (data) {
        if (this.takecardFinish) {
            try {

                this.takecardsAction(data);
            } catch (e) {

                this.takecardFinish = true;

                this.loggerError('出牌出错：' + JSON.stringify(e) + e);
            }
        } else {
            this.scheduleOnce(function () {
                this.takecardsActionSchedule(data);
            }, 0.2);
        }
    },
    takecardsAction: function (data) {
        this.takecardFinish = false;
        cc.log("takecards_event请求111:" + data);
        var beginTime = Date.now();
        if (cc.mile.chupaiBegin) {
            cc.log("出牌完成返回：" + beginTime + "，时间戳[" + Math.floor((beginTime - cc.mile.chupaiBegin) / 1000) + "]");
        }

        this.lasttip = null;
        //出牌正确的时候
        if (data.allow == true) {
            //获取用户的信息

            //如果下一个用户等于自己，那么清空自己手上的牌
            if (data.nextplayer == cc.mile.user.id) {
                this.setPlayOptionStatus(true, data.automic, this.newsflag);
                this.playAutomic = data.automic;  // 判断是否允许不出
                this.clearSelfCard();
            }

            cc.log("takecards_event请求222:" + data);
            //当前用户
            if (data.userid == cc.mile.user.id) {
                this.setPlayOptionStatus(false, false, false);
                this.game.tipcards = [];  // 去掉操作提示牌的缓存

            }

            cc.log("takecards_event请求333:" + data);
            this.playOutCards(data, false); // 出牌

            this.showClock(data.nextplayer);// 调整闹钟

            this.game.updateBenLunFenShuClick(data.roundscore); // 更新桌面抓分

            this.updateUserBaojing(data.userid, data.cardsnum); // 更新报警

            this.updateUserLvl(data);  //  更新 1游 2游状态

            cc.log("takecards_event请求444:" + data);
            if (data.scoreuserid) {
                this.updateUserZhuafen(data.scoreuserid, data.changescore);
                //this.clearAllPlayCardLayer();// 清除所有玩家的牌
            }

            //删除下一个用户的子节点
            var nextplayer = this.getUserRenderById(data.nextplayer);
            //删除所有子节点
            nextplayer.clearPlayCardLayer();

            cc.log("takecards_event请求555:" + data);

            if (data.leftcards) { // 其他玩家明牌了，剩余的牌
                var leftcards = this.decode(data.leftcards);
                this.showPlaySeeCards(data.leftuserid, leftcards, false);
            }

            if (data.mycards) { // 其他玩家明牌了，剩余的牌
                var mycards = this.decode(data.mycards);

                var maziPoker = this.getCommon("MaziPoker");
                //首先清空自己手里的牌，然后重新加载受伤的牌
                maziPoker.clearAllPokers();
                maziPoker.loadcards(mycards, true);
            }

            cc.log("takecards_event请求666:" + data);
            this.currentPlaycardUserId = data.nextplayer;

            if (data.tableclean) {
                this.clearAllPlayCardLayer();// 清除所有玩家的牌
            }

            // nextplayer.laypaker.removeAllItems();

        } else { //出牌不符合规则，需要进行提示
            if (data.tipstr) {
                this.showCenterToast(data.tipstr);
            } else {
                //this.stopAllUserVoiceAnimationStatus(false);
                cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/buyao1.mp3");
            }


        }
        this.takecardFinish = true;
    },
    /**
     * 清除自己打出去的牌
     */
    clearSelfCard: function () {
        var maziPoker = this.getCommon("MaziPoker");
        var countp = maziPoker.cunpoker.children.length;
        for (var i = 0; i < countp; i++) {
            maziPoker.cunpoker.removeChild(maziPoker.cunpoker.children[0]);
        }
        //for (var i = countp - 1; i >= 0; i--) {

        //}
    },
    /**
     * 清除所有玩家打出去的牌
     */
    clearAllPlayCardLayer: function () {
        for (var j = 0; j < this.playerRender.length; j++) {
            var userRender = this.playerRender[j];
            userRender.clearPlayCardLayer();
        }
        //删除自己的牌
        var pokers = this.getCommon("MaziPoker");
        pokers.clearSefAllPokers();
        this.selfPlayerRender.clearPlayCardLayer();
    },
    /**
     * 出牌
     * @param data
     */
    playOutCards: function (data, isRecovery) {
        var mycards = null;
        if (data.cards) {
            mycards = this.decode(data.cards);
        }
        cc.log("playOutCards请求 111:" + mycards);
        if (mycards && mycards.length > 0) {
            var maziPoker = this.getCommon("MaziPoker");
            var firstCardValue = mycards[0];
            if (data.userid == cc.mile.user.id) {

                cc.log("playOutCards请求 ffff222:" + mycards);

                maziPoker.PlayACard(mycards);

                if (!isRecovery) { // 如果不是恢复牌局
                    var nextPlay = this.getUserRenderById(data.nextplayer);
                    //删除所有子节点
                    nextPlay.clearPlayCardLayer();
                }
            } else {
                var currentPlay = this.getUserRenderById(data.userid);
                //
                var x = currentPlay.userPosition.x;
                //Y
                var y = currentPlay.userPosition.y;
                if (!isRecovery) { // 如果不是恢复牌局

                    ///找到打牌的下一家，然后清空下一家的牌
                    cc.log("playOutCards请求 eeeeee222:" + mycards);
                    var nextPlay = this.getUserRenderById(data.nextplayer);
                    //删除所有子节点
                    nextPlay.clearPlayCardLayer();
                }

                cc.log("playOutCards请求 eeeeee333:" + currentPlay.node._name);

                //右边方向,向左移100
                if (currentPlay.node._name == "player_right") {
                    currentPlay.opencords(mycards, 150, 50, "right");
                }
                //从头部
                else if (currentPlay.node._name == "player_top") {
                    currentPlay.opencords(mycards, 55, 165, "top");
                }
                //从左边方向
                else if (currentPlay.node._name == "player_left") {
                    currentPlay.opencords(mycards, -150, 50, "left");
                }
            }
            cc.log("playOutCards请求 222:" + mycards);
            this.playCardTypeAnimation(data);
            cc.log("playOutCards请求 333:" + mycards);
            this.playCardTypeVoice(data.cardType, firstCardValue);
            cc.log("playOutCards请求 444:" + mycards);

        } else {
            cc.log("playOutCards请求 ww222:" + mycards);
            var currentPlay = this.getUserRenderById(data.userid);
            cc.log("playOutCards请求 ww333:" + mycards);
            currentPlay.updateBuchuStatus(true);
            cc.log("playOutCards请求 ww444:" + mycards);
            //this.stopAllUserVoiceAnimationStatus(false);
            cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/buyao1.mp3");
        }

    },
    /**
     * 打牌的动画
     * @param data
     */
    playCardTypeAnimation: function (data) {
        if (data.cardType && data.cardType.mazicardtype == 6) { //飞机
            this.game.playFeiji();
            cc.mile.audio.playSFX("texiao/newfeiji.mp3");
        } else if (data.cardType && data.cardType.mazicardtype >= 11) { //五十K及以上
            cc.mile.audio.playSFX("texiao/newK105.mp3");
        }
    },
    /**
     * 打牌的语音
     * @param cardType
     * @param cardValue
     */
    playCardTypeVoice: function (cardType, cardValue) {
        var voiceName = GameUtil.getMaziCardTypeVoiceName(cardType, cardValue);
        if (voiceName) {
            //this.stopAllUserVoiceAnimationStatus(false);
            cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/" + voiceName);
        }
    },
    /**
     * 更新用户的报警状态
     * @param userId
     * @param cardsnum
     */
    updateUserBaojing: function (userId, cardsnum) {
        var baojingStatus = false;
        if (cardsnum <= 5 && cardsnum > 0) {
            baojingStatus = true;
        }
        var render = this.getUserRenderById(userId);
        render.updateBaojinViewStatus(baojingStatus, cardsnum);

    },
    /**
     * 更新用户的抓分信息
     * @param scoreUserId
     * @param changescore
     */
    updateUserZhuafen: function (scoreUserId, changescore) {
        var render = this.getUserRenderById(scoreUserId);
        render.updateZhuafen(changescore);
    },
    /**
     * 更新用户的 1游  2游 ....  状态
     * @param data
     */
    updateUserLvl: function (data) {
        if (data.lvl != 0) {
            var render = this.getUserRenderById(data.userid);
            render.clearMingPaiContainer();
            render.showWinTag(data.lvl, false);
        }
    },
    /**
     * 接收到服务端的 出牌提示信息
     * @param data
     * @param context
     */
    cardtips_event: function (data, context) {
        cc.log("data [" + data + "]");
        cc.log("context [" + context + "]");
        if (data.tipcards && data.tipcards.length > 0) {
            context.game.tipcards = data.tipcards;
            context.game.tipIndex = 0;
            context.game.cardtips();
        } else {
            if (!context.playAutomic) { // 如果为必须要出牌的情况，不能自动跳过
                context.game.buchuBtnClick();
            }
        }
    },
    /**
     * 接收到服务端的 理牌信息
     * @param data
     * @param context
     */
    settlecards_event: function (data, context) {
        cc.log("data [" + data + "]");
        cc.log("context [" + context + "]");
        var maziPoker = context.getCommon("MaziPoker");
        if (!maziPoker) {
            maziPoker = context.selfPoker;
        }
        var mycards = context.decode(data.cards);
        //首先清空自己手里的牌，然后重新加载手上的牌
        maziPoker.clearAllPokers();
        maziPoker.loadcards(mycards, true);
    },
    /**
     * 接收到服务端的 明牌信息
     * @param data
     * @param context
     */
    seecards_event: function (data, context) {


        cc.log("data [" + data + "]");
        cc.log("context [" + context + "]");
        if (data.cards) {
            var cards = context.decode(data.cards);
            context.showPlaySeeCards(data.userid, cards, true);
            if (data.userid == cc.mile.user.id) {
                context.game.updateMingpaiViewStatus(true);
            }
        }
        if (data.leftcards) {
            var leftcards = context.decode(data.leftcards);
            context.showPlaySeeCards(data.leftuserid, leftcards, true);
        }
    },

    /**
     *
     * @param userid
     * @param cards
     * @param playvoice
     */
    showPlaySeeCards: function (userid, cards, playvoice) {
        cc.log("showPlaySeeCards:" + cards);
        if (userid && cards) {
            var render = this.getUserRenderById(userid);
            render.showSeecards(cards);
            if (playvoice) {
                //this.stopAllUserVoiceAnimationStatus(false);
                cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/buyao2.mp3");
            }
        }

    },
    /**
     * 接收到服务端的 推送的 开始游戏的玩家数据
     * @param data
     * @param context
     */
    play_event: function (data, context) {

        if (context.invite != null) {
            context.invite.destroy();
        }
        cc.mile.gamestatus = data.gamestatus;
        var mycards = context.decode(data.player.cards);

        cc.log(data);
        cc.log(mycards);

        var pokers = context.getCommon("MaziPoker");
        if (!pokers) {
            pokers = context.selfPoker;
        }
        pokers.loadcards(mycards, false);

        context.teamCard = data.teamCard;
        context.teamOwner = data.teamOwner;
        context.newsflag = data.player.newsflag;

        if (data.player.banker) {
            context.currentPlaycardUserId = data.player.playuser;
            context.playAutomic = data.player.banker;
        }
        context.game.roomnum_format(data);
        context.updatePlayStatus(data);
        context.onGameStart(data);

    },
    /**
     * 更新玩家的游戏状态
     * @param playdata
     */
    updatePlayStatus: function (playdata) {

        for (var i = 0; i < playdata.players.length; i++) {
            var player = playdata.players[i];
            var playUpdateIndex = (player.index - playdata.player.index + 4) % 4;
            cc.log(player.username + "--" + player.index + "---" + playdata.player.index + "------" + playUpdateIndex);
            for (var j = 0; j < this.playerRender.length; j++) {
                var userRender = this.playerRender[j];

                cc.log(userRender.getRelationIndex() + "--relationIndex--");
                if (userRender.getRelationIndex() == playUpdateIndex) {
                    userRender.updatePlayUserInfo(player);
                    userRender.setMasterFlag(player.banker);
                    userRender.updateFen(player.totalscore);
                    userRender.updateReadyByPlayerStatus(player.gamestatus);
                }
            }
        }

        this.selfPlayerRender.setMasterFlag(playdata.player.banker);
        this.selfPlayerRender.updateFen(playdata.player.totalscore);
        this.selfIndex = playdata.player.index;
        this.updateUserReadyStatus(playdata.player.playuser, playdata.player.gamestatus);

    },

    /**
     *  更新玩家的  出牌、报喜 按钮状态
     * @param status
     * @param disableBuchu
     * @param newsflag
     */
    setPlayOptionStatus: function (status, disableBuchu, newsflag) {
        if (newsflag) {
            this.newsOptionsContainer.active = newsflag;
            this.playOptionsContainer.active = false;
        } else {
            this.newsOptionsContainer.active = newsflag;
            this.playOptionsContainer.active = status;
            this.buchuBtn.interactable = !disableBuchu;
        }
    },

    newplayer: function (data, positionDirection) {

        cc.log(positionDirection + ":newplayer:" + JSON.stringify(data));
        data.positionDirection = positionDirection;
        data.relationIndex = (data.index - this.selfIndex + 4) % 4;//计算相对位置

        var renderIndex = this.getRenderByIndex(data.index);
        if (renderIndex) {
            renderIndex.initplayer(data, headPos);
        } else {

            var game = this.getCommon("MaziDataBind");
            if (game) {
                //默认右边 "right"
                var player = cc.instantiate(this.player_right);
                var pos = cc.v2(580, 75);
                var headPos = cc.v2(600, 160);
                if (positionDirection == "top") {
                    //对面
                    player = cc.instantiate(this.player_top);
                    pos = cc.v2(-128, 300);
                    headPos = cc.v2(-60, 320);
                } else if (positionDirection == "left") {
                    //左边
                    player = cc.instantiate(this.player_left);
                    pos = cc.v2(-580, 75);
                    headPos = cc.v2(-560, 160);
                }
                player.setPosition(pos)
                player.parent = cc.find("Canvas/global");
                player.on(cc.Node.EventType.TOUCH_START, function (e) {
                    var clickUser = e.currentTarget.getComponent("PlayerRender");
                    var render = clickUser.getComponent("PlayerRender");
                    this.game.onUserInfoClick(render.userPosition, {
                        userid: render.userid,
                        username: render.username,
                        userip: render.userip,
                        gameid: render.gameid,
                    });

                }, this, true);


                var render = player.getComponent("PlayerRender");
                render.initplayer(data, headPos);
                //render.showSeecards([91,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27])
                this.playerRender.push(render);
            }

        }
    },
    getRenderByIndex: function (index) {
        var relationIndex = (index - this.selfIndex + 4) % 4;//计算相对位置
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            if (render.getRelationIndex() == relationIndex) {
                return render;
            }

        }
        return null;
    },

    getUserPositionById: function (userid) {
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            if (render.userid == userid) {
                return render.userPosition;
            }

        }
        return null;
    },
    getUserRenderById: function (userid) {
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            if (render.userid == userid) {
                return render;
            }
        }
        return this.selfPlayerRender;
    },
    onCloseClick: function () {
        this.continuegamebtn.active = true;
    },

    statictimer: function (message, time) {
        this.waittimer = cc.instantiate(this.waitting);
        this.waittimer.parent = this.root();

        var timer = this.waittimer.getComponent("MileTimer");
        if (timer) {
            timer.init(message, time, this.waittimer);
        }
    },
    onDestroy: function () {
        this.cleanmap();

        cc.gvoicePlayFinishedCallbackJS = null;
        cc.gvoiceDownloadCallbackJS = null;
        cc.gvoiceUploadCallbackJS = null;

        cc.mile.gamestatus = "notready";
        if (!this.eventLeaved) {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            this.emitSocketRequest("disconnect", JSON.stringify(param));
        }
        if (cc.mile.socket && cc.mile.socket.off) {
            cc.mile.socket.off("command_room");
        }

        if (cc && cc.eventManager) {
            cc.eventManager.removeCustomListeners("socketReconnect");
        }
        cc.mile.maxplayers = null;
        cc.mile.curpalyers = null;

    },
    /**
     * 开始游戏
     */
    onGameStart: function (playdata) {
        if (this.maziStartAnimation != null) {
            this.maziStartNode.active = true;
            this.maziStartAnimation.play();
        }
        this.pokerCount = 27;
        this.startCardBg.active = true;
        this.startPokerPosistion = this.node.convertToWorldSpace(this.startCardBg.getPosition());

        cc.log("----执行发牌-----");

        var self = this;
        this.sendCardCallback = function () {
            self.sendPokerUp();
            self.sendPokerLeft();
            self.sendPokerRight();
            self.sendPokerDown();
            if (self.pokerCount % 5 == 0) {
                cc.mile.audio.playSFX("texiao/fapai.mp3");
            }
            self.pokerCount--;
            cc.log("----repeat---:" + self.pokerCount);
            // 执行动作
            if (self.pokerCount == 0) {
                self.startCardBg.active = false;
                self.onMaziModui();
                self.setPlayOptionStatus(this.selfPlayerRender.masterFlag, true, this.newsflag);
                self.showFirstClock(playdata);
                self.unschedule(self.sendCardCallback);
            }
        };

        this.schedule(this.sendCardCallback, 0.07, 26, 0.07);// 间隔时间，重复次数，推迟执行时间
    },

    /**
     *  莫对动画
     */
    onMaziModui: function () {

        var maziPoker = this.getCommon("MaziPoker");
        var moDuiNode = new cc.Node();
        var sprite = moDuiNode.addComponent(cc.Sprite);

        sprite.spriteFrame = maziPoker.atlas.getSpriteFrame(this.teamCard);

        var wpos = this.node.convertToWorldSpace(this.startCardBg.getPosition());

        moDuiNode.setPosition(wpos);
        moDuiNode.width = 50;
        moDuiNode.height = 70;

        cc.director.getScene().addChild(moDuiNode);

        var finished = cc.callFunc(function (target) {
            this.onModuiJumpCallback(target);
        }, this);//动作完成

        // 获取跳动的action
        var moduiAction = this.getModuiJumpAction(0.3, 30);
        moDuiNode.runAction(cc.sequence(moduiAction, finished));

    },
    /**
     * 莫对跳动动画
     * @param target
     */
    onModuiJumpCallback: function (target) {
        //完成后删除摸排
        var finished = cc.callFunc(function (obj) {
            obj.destroy();
        }, this, target);
        var movePosition = this.getUserPositionById(this.teamOwner);
        if (movePosition == null) {
            movePosition = this.selfPosition;
        }
        var moduiAction = this.getModuiDisplayAction(movePosition.x, movePosition.y, 1.2);
        target.runAction(cc.sequence(moduiAction, finished));

    },
    getModuiJumpAction: function (jumpDuration, jumpHeight) {
        // 
        var jumpUp = cc.moveBy(jumpDuration, cc.p(0, jumpHeight)).easing(cc.easeCubicActionOut());
        // 
        var jumpDown = cc.moveBy(jumpDuration, cc.p(0, -jumpHeight)).easing(cc.easeCubicActionIn());
        // 
        return cc.repeat(cc.sequence(jumpUp, jumpDown), 3);
    },

    getModuiDisplayAction: function (x, y, timeDuration) {
        // 

        var mv = cc.moveBy(timeDuration, x, y);
        var scale = cc.scaleTo(timeDuration, 0.4, 0.4);
        return cc.spawn(mv, scale);
    },
    sendPokerUp: function () {
        var pok = new cc.Node();
        pok.height = 70;
        pok.width = 50;

        var sprite = pok.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/startCardBg.png")
            , cc.rect(0, 0, 50, 70));

        pok.setPosition(this.startPokerPosistion);

        //pok.setPosition(this.startCardBg.getPosition());

        cc.director.getScene().addChild(pok);

        // 创建一个移动动作
        var action = cc.moveTo(0.6, this.startPokerPosistion.x, GameUtil.getViewCenterY() * 2 - 120);
        //完成后删除pok
        var finished = cc.callFunc(function (obj) {
            obj.destroy();
        }, this, pok);

        pok.runAction(cc.sequence(action, finished));

    },
    sendPokerLeft: function () {
        var pok = new cc.Node();
        pok.height = 70;
        pok.width = 50;

        var sprite = pok.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/startCardBg.png")
            , cc.rect(0, 0, 50, 70));

        pok.setPosition(this.startPokerPosistion);

        //pok.setPosition(this.startCardBg.getPosition());

        cc.director.getScene().addChild(pok);

        // 创建一个移动动作
        var action = cc.moveTo(0.6, 130, this.startPokerPosistion.y);
        //完成后删除pok
        var finished = cc.callFunc(function (obj) {
            obj.destroy();
        }, this, pok);

        pok.runAction(cc.sequence(action, finished));


    },
    sendPokerRight: function () {
        var pok = new cc.Node();
        pok.height = 70;
        pok.width = 50;

        var sprite = pok.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/startCardBg.png")
            , cc.rect(0, 0, 50, 70));

        pok.setPosition(this.startPokerPosistion);

        //pok.setPosition(this.startCardBg.getPosition());

        cc.director.getScene().addChild(pok);

        // 创建一个移动动作
        var action = cc.moveTo(0.6, GameUtil.getViewCenterY() * 2 - 130, this.startPokerPosistion.y);
        //完成后删除pok
        var finished = cc.callFunc(function (obj) {
            obj.destroy();
        }, this, pok);

        pok.runAction(cc.sequence(action, finished));

    },
    dissroom_event: function (data, context) {
        debugger;
        if (data.dissflag == 1) {
            context.game.agreeDissroom(data);
        } else if (data.dissflag == 2) {
            context.game.disAgreeDissroom(data);
        } else {
            context.game.showJiesanDialog(data, false);
        }
    },
    createroom_event: function (data, context) {
        cc.log('createroom_数据出错：' + JSON.stringify(data))
        var data = context.parse(data);
        context.showCenterToast(data.context);
        context.scheduleOnce(function () {
            this.scene("mazihall", this);
        }, 3);
    },

    sendPokerDown: function () {
        var pok = new cc.Node();
        pok.height = 70;
        pok.width = 50;

        var sprite = pok.addComponent(cc.Sprite);
        sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/room/pdkgame/cards/startCardBg.png")
            , cc.rect(0, 0, 50, 70));

        pok.setPosition(this.startPokerPosistion);

        //pok.setPosition(this.startCardBg.getPosition());

        cc.director.getScene().addChild(pok);
        pok.pokerIndex = 27 - this.pokerCount;
        var xSingle = GameUtil.getViewCenterY() * 2 / 27;
        var xPosition = GameUtil.getViewCenterY() * 2 - (xSingle * this.pokerCount);
        // 创建一个移动动作
        var action = cc.moveTo(0.2, xPosition, 100);
        //完成后删除pok
        var finished = cc.callFunc(function (obj) {
            this.selfPoker.showCard(pok.pokerIndex)
            obj.destroy();
            cc.log("----调用show牌--");
        }, this, pok);

        pok.runAction(cc.sequence(action, finished));

    },
    /**
     游戏开始动画结束后，显示闹钟
     */
    showFirstClock: function (playdata) {
        for (var i = 0; i < playdata.players.length; i++) {
            var player = playdata.players[i];
            if (player.banker) {
                this.showClock(player.playuser);
            }
        }
        if (playdata.player.banker) {
            this.showClock(playdata.player.playuser);
        }
    },
    /**
     *   显示闹钟并倒计时
     *   @param playuser
     */
    showClock: function (playuser) {
        this.currentPlaycardUserId = playuser;
        var self = this;
        self.clock.active = false;
        var po = cc.v2(-4, 36);
        if (playuser) {
            var _render = self.getUserRenderById(playuser);
            if (_render.positionDirection == "top") {
                po = cc.v2(-75, 204);
            } else if (_render.positionDirection == "right") {
                po = cc.v2(418, 130);
            } else if (_render.positionDirection == "left") {
                po = cc.v2(-435, 130);
            }
        }

        var _clocknum1 = 2;
        var _clocknum2 = 0;
        self.clocknum1.getComponent(cc.Sprite).spriteFrame = self.clocknumAtlas.getSpriteFrame("切图-数字-" + _clocknum1);
        self.clocknum2.getComponent(cc.Sprite).spriteFrame = self.clocknumAtlas.getSpriteFrame("切图-数字-" + _clocknum2);
        self.clock.setPosition(po);
        self.clock.active = true;
        if (self.runtimer) {
            self.unschedule(self.runtimer);
            //self.unscheduleAllCallbacks();
        }
        self.runtimer = function () {


            if (_clocknum2 == 0) {
                _clocknum1 -= 1;
                _clocknum2 = 9;
            } else if (_clocknum2 > 0 && _clocknum1 >= 0) {
                _clocknum2 -= 1;
            }
            if (_clocknum1 == 0 && _clocknum2 == 0) {
                self.unschedule(self.runtimer);
            }
            self.clocknum1.getComponent(cc.Sprite).spriteFrame = self.clocknumAtlas.getSpriteFrame("切图-数字-" + _clocknum1);
            self.clocknum2.getComponent(cc.Sprite).spriteFrame = self.clocknumAtlas.getSpriteFrame("切图-数字-" + _clocknum2);
        }
        self.schedule(self.runtimer, 1, 20, 1);
    },

});
