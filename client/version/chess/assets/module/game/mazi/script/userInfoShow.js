var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {

        username: {
            default: null,
            type: cc.Label
        },
        userid: {
            default: null,
            type: cc.Label
        },
        userip: {
            default: null,
            type: cc.Label
        },
    },

    // use this for initialization
    onLoad: function () {
        this.userParams = {};
    },
    initUserInfo: function (userInfo) {
        cc.log("用户信息展示："+JSON.stringify(userInfo));
        this.userParams = userInfo;
        this.username.string = userInfo.username;
        this.userid.string = userInfo.gameid;
        this.userip.string = userInfo.userip;
    },
    onPlayTest: function (event, data) {
        var chatAnimation = this.getCommon("ChatAnimation");
        // chatAnimation = new chatAnimation();;
        if (chatAnimation.positionX == 0 || chatAnimation.positionY == 0) { //自己点开不处理表情
            this.closeOpenWin();
        } else {
            if (data) {// data 点击不同的表情发送
                //if (chatAnimation.animationNode != null) { chatAnimation.animationNode.destroy(); chatAnimation.animationNode = null; }
                chatAnimation.setSourcePosition(chatAnimation.selfPosition);
                chatAnimation.setTargetPosition(chatAnimation.getChoicePosition());
                var isSend = chatAnimation.sendPlayAnimation(cc.mile.user.id,data);
                this.closeOpenWin();
                if(isSend){
                    var maziBegin = this.getCommon("MaziBegin");

                    maziBegin.sendmsg({
                        source: cc.mile.user.id,
                        target: this.userParams.userid,
                        msgtype: '2',
                        sendtype: '0',
                        msgid: data,
                        msgname: '',
                        msgcontent: '',
                        mem: ''
                    });
                }
                //}else{
                //    var toast = Toast.makeText('先喝杯茶吧', Toast.LENGTH_SHORT);
                //    toast.setGravity(Toast.CENTER, 0, 0);
                //    toast.show();
                //    return;
                //}
                if (chatAnimation.animationNode) {
                    this.closeOpenWin();
                }
            }
        }
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
