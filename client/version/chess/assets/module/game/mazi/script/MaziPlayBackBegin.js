var beiMiCommon = require("MileCommon");
var GameUtil = require("WangdaGameUtils");
cc.Class({
    extends: beiMiCommon,
    properties: {
        roomidLabel: {
            default: null,
            type: cc.Label
        },
        roomNumbersLabel: {
            default: null,
            type: cc.Label
        },
        gameTimeLabel: {
            default: null,
            type: cc.Label
        },
        gameDescLabel: {
            default: null,
            type: cc.Label
        },
        //准备(准备按钮)
        gameReadybtn: {
            default: null,
            type: cc.Node
        },
        poker: {
            default: null,
            type: cc.Node
        },
        player_top: {
            default: null,
            type: cc.Prefab
        },
        player_left: {
            default: null,
            type: cc.Prefab
        },
        player_bottom: {
            default: null,
            type: cc.Prefab
        },
        player_right: {
            default: null,
            type: cc.Prefab
        },
        buchuBtn: {
            default: null,
            type: cc.Button
        },
        //闹钟
        clock: {
            default: null,
            type: cc.Node
        },
    },

    // use this for initialization
    onLoad: function () {
        //出牌的速度
        this.outCardTime = 1500;
        //定义四个用户手中的牌
        this.bottomPokers = new Array();
        //左边手中的牌
        this.leftCards = new Array();
        //右边手中的牌
        this.rightCards = new Array();
        //头部手中的牌
        this.topCards = new Array();

        this.playerRender = new Array();     //存放玩家渲染数据
        this.palyIndex = 0;
        this.selfPlayerId = '';
        this.queryPalyBack();
    },
    /// 点击开始，发送状态到server端
    begin: function () {
        this.resetPlayerStatus();
        this.resetSelfPlayerStatus();
        //清空所有人的信息
        this.clearAllPlayCardLayer();
        //清楚自己的牌信息
        var pokers = this.getCommon("MaziPoker");
        pokers.clearSefAllPokers();
        this.clock.active = false;
    },

    //读取房间的历史数据
    queryPalyBack: function () {
        var param = {
            token: cc.mile.authorization,
            roomid: cc.mile.backroomid,
            currentnum: cc.mile.curnum,
        };
        cc.mile.http.httpPost("/record/queryPlayback", param, this.successGetPlayer, this.errorGetPlayer, this);
    },
    successGetPlayer: function (result, context) {
        cc.log(result);
        var data = context.parse(result);
        //初始左上角信息
        context.roomnum_format(data.gameRoom);
        //定位每个人的信息
        context.initPlayerPosition(data.hisplayers);
        //开始出牌
        context.hiscards = data.hiscards;
        //出牌
        context.setTimePlayCard();
    },

    initPlayerPosition: function (hisplayers) {
        var findSelf = false;
        for (var i = 0; i < hisplayers.length; i++) {
            if (hisplayers[i].userid == cc.mile.user.id) {
                findSelf = true;
                this.selfPlayerId = hisplayers[i].userid;
                this.initSelfData(hisplayers[i]);
                break;
            }
        }

        if (!findSelf) { // 如果没有找到自己，则选择第一个
            this.selfPlayerId = hisplayers[0].userid;
            this.initSelfData(hisplayers[0]);
        }
        debugger;
        for (var i = 0; i < hisplayers.length; i++) {
            if (hisplayers[i].sitindex == this.selfIndex) {
                continue;
            }
            var relationIndex = (hisplayers[i].sitindex - this.selfIndex + 4) % 4;
            if (relationIndex == 1) {
                this.newplayer(hisplayers[i], "right");
            } else if (relationIndex == 2) {
                this.newplayer(hisplayers[i], "top");
            } else if (relationIndex == 3) {
                this.newplayer(hisplayers[i], "left");
            }
        }
    },

    setTimePlayCard: function () {
        var self = this;
        this.palyIndex = 0;
        this.callback = function () {
            //获取当前打出去的牌
            if (this.palyIndex < this.hiscards.length) {
                var palyCards = this.hiscards[this.palyIndex];
                setTimeout(function () {
                    //显示闹钟
                    self.showClock(palyCards.userid, true);
                    //清空
                    setTimeout(function () {
                        //影藏闹钟
                        self.showClock(palyCards.userid, false);
                        //出牌
                        self.playOutCards(palyCards);
                    }, 750)
                }, 750)
            }
            else {
                this.unschedule(this.callback);
            }
            this.palyIndex++;
        };
        this.schedule(this.callback, this.outCardTime / 1000);
    },
    errorGetPlayer: function (result, context) {
        cc.log(result);
    },

    roomnum_format: function (data) {
        cc.log(data);

        this.roomidLabel.string = data.roomdesc;
        this.roomNumbersLabel.string = data.nowgamenum;
        this.gameDescLabel.string = data.playwaydesc;
        this.gameTimeLabel.string = data.createtime;
    },

    /**
     * joinroom  初始化自己的位置信息
     * @param data
     */
    initSelfData: function (data) {
        if (!this.selfPlayerRender) {
            var player = cc.instantiate(this.player_bottom);
            var selfPosition = cc.v2(-484, -302.6);
            var selfHeadPosition = cc.v2(-570, -290);
            var chatAnimation = this.getCommon("ChatAnimation");
            chatAnimation.initSelfPosition(selfHeadPosition);

            this.selfPosition = selfHeadPosition;
            player.setPosition(selfPosition);
            player.parent = cc.find("Canvas/global");
            this.selfPlayerRender = player.getComponent("PlayerRender");
        }
        cc.mile.gamestatus = data.gamestatus;
        //this.selfPlayerRender.showSettlement(-191919000022);
        this.selfPlayerRender.initbackplayer(data, selfHeadPosition);
        var maziPoker = this.getCommon("MaziPoker");
        var parklist = this.getStrArray(data.cardstr);
        //自己手中的牌
        this.bottomPokers = parklist;
        maziPoker.loadcards(parklist, true);
        this.selfIndex = data.sitindex; //当前玩家所在位置
        //this.game.roomnum_format(data);
    },

    newplayer: function (data, positionDirection) {
        var parklist = this.getStrArray(data.cardstr);
        //默认右边 "right"
        var player = cc.instantiate(this.player_right);
        var pos = cc.v2(580, 75);
        var headPos = cc.v2(600, 160);
        if (positionDirection == "top") {
            //对面
            player = cc.instantiate(this.player_top);
            pos = cc.v2(-128, 300);
            headPos = cc.v2(-60, 320);
            this.topCards = parklist;
        } else if (positionDirection == "left") {
            //左边
            player = cc.instantiate(this.player_left);
            pos = cc.v2(-580, 75);
            headPos = cc.v2(-560, 160);
            this.leftCards = parklist;
        }
        else if (positionDirection == "right") {
            this.rightCards = parklist;
        }
        player.setPosition(pos)
        player.parent = cc.find("Canvas/global");
        data.positionDirection = positionDirection;
        data.relationIndex = (data.sitindex - this.selfIndex + 4) % 4;//计算相对位置
        var render = player.getComponent("PlayerRender");
        render.initbackplayer(data, headPos, positionDirection);
        render.showSeecards(parklist);
        this.playerRender.push(render);
    },
    //根据用户ID，找到当前是哪个用户
    getPlayUserIndex: function (playerId, context) {

    },
    /**
     * 出牌
     * @param data
     */
    playOutCards: function (data) {
        var currentPlay = this.getUserRenderById(data.userid);
        //抓分
        //currentPlay.updateZhuafen(data.changescore);
        //总分
        //currentPlay.updateFen(data.totalscore);
        var maziPoker = this.getCommon("MaziPoker");
        //清除上次打的牌
        currentPlay.clearPlayCardLayer();

        if (data.scoreuserid) {
            cc.log("有玩家得分拉----：" + JSON.stringify(data));
            this.updateUserZhuafen(data.scoreuserid, data.changescore);
        }

        //console.log(data);
        if (data.cards != null) {
            //要出的牌
            var outcard = this.getStrArray(data.cards);
            //x
            var x = currentPlay.userPosition.x;
            //Y
            var y = currentPlay.userPosition.y;
            console.log(outcard);
            //右边方向,向左移100
            if (currentPlay.node._name == "player_right") {
                currentPlay.opencords(outcard, 150, 50, "right");
                this.deleteArray(this.rightCards, outcard, "right");
                currentPlay.showSeecards(this.rightCards);
                console.log(this.rightCards);
            }
            //从头部
            else if (currentPlay.node._name == "player_top") {
                currentPlay.opencords(outcard, 55, 165, "top");
                this.deleteArray(this.topCards, outcard, "top");
                currentPlay.showSeecards(this.topCards);
                console.log(this.topCards);
            }
            //从左边方向
            else if (currentPlay.node._name == "player_left") {
                currentPlay.opencords(outcard, -150, 50, "left");
                this.deleteArray(this.leftCards, outcard, "left");
                currentPlay.showSeecards(this.leftCards);
                console.log(this.leftCards);
            }
            else if (currentPlay.node._name == "player_bottom") {
                //删除自己的牌
                maziPoker.clearSefAllPokers();
                this.selfPlayerRender.clearPlayCardLayer();
                maziPoker.PlayACard(outcard);
                this.deleteArray(this.bottomPokers, outcard, "bottom");
                maziPoker.clearAllPokers();
                maziPoker.loadcards(this.bottomPokers, true);
                console.log(this.bottomPokers);
            }
            if (data.lvl != 0) {
                currentPlay.showWinTag(data.lvl);
            }


            if (data.tableclean) {
                this.clearAllPlayCardLayer();// 清除所有玩家的牌
            }
            this.playCardTypeAnimation(data);
            debugger;
            this.playCardTypeVoice(data, outcard[0]);
        } else {
            maziPoker.clearSefAllPokers();
            currentPlay.updateBuchuStatus(true);
            cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/buyao1.mp3");
        }
    },
    //删除数组中指定的数字
    deleteArray: function (array, outcard, postion) {
        for (var j = 0; j < outcard.length; j++) {
            for (var i = 0; i < array.length; i++) {
                if (outcard[j] == array[i]) {
                    if (postion == "top") {
                        this.topCards.splice(i, 1);
                    }
                    else if (postion == "left") {
                        this.leftCards.splice(i, 1);
                    }
                    else if (postion == "right") {
                        this.rightCards.splice(i, 1);
                    }
                    else if (postion == "bottom") {
                        this.bottomPokers.splice(i, 1);
                    }
                }
            }
        }
    },
    /**
     * 打牌的动画
     * @param data
     */
    playCardTypeAnimation: function (data) {
        if (data.cardType && data.cardType.mazicardtype == 6) { //飞机
            this.game.playFeiji();
        }
    },
    /**
     * 清除所有玩家打出去的牌
     */
    clearAllPlayCardLayer: function () {
        for (var j = 0; j < this.playerRender.length; j++) {
            var userRender = this.playerRender[j];
            userRender.clearPlayCardLayer();
        }
        //删除自己的牌
        var pokers = this.getCommon("MaziPoker");
        pokers.clearSefAllPokers();
        this.selfPlayerRender.clearPlayCardLayer();
    },

    /**
     * 更新用户的抓分信息
     * @param scoreUserId
     * @param changescore
     */
    updateUserZhuafen: function (scoreUserId, changescore) {
        cc.log("更新用户抓分---：" + scoreUserId);
        var render = this.getUserRenderById(scoreUserId);
        render.updateZhuafen(changescore);
    },

    /**
     * 打牌的语音
     * @param cardType
     * @param cardValue
     */
    playCardTypeVoice: function (cardType, cardValue) {
        debugger;
        var voiceName = GameUtil.getMaziCardTypeVoiceName(cardType, cardValue);
        if (voiceName) {
            cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/" + voiceName);
        }
    },

    // 根据index  获取玩家的 位置
    getPlayerPosition: function (playerId, playerIndex) {
        var inroom = false;
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            if (render.userid == playerId) {
                inroom = true;
                break;
            }
        }
        if (!inroom) {
            var playUpdateIndex = (playerIndex - this.selfIndex + 4) % 4;
            switch (playUpdateIndex) {
                case 1:
                    return "right";
                case 2:
                    return "top";
                case 3:
                    return "left";
            }
        }
        return "";
    },


    /**
     * 每大局的结算信息
     * @param data
     * @param context
     */
    clean_event: function (data, context) {

        context.game.showJiesuanDialog(data);

    },


    /**
     * 发送语音表情动画
     * @param data
     */
    sendChatAnimation: function (data) {

        var playerSource = this.getUserPositionById(data.source);
        if (playerSource) {

            var chatAnimation = this.getCommon("ChatAnimation");
            var playerTarget = this.getUserPositionById(data.target);
            if (playerTarget == null) {
                playerTarget = this.selfPosition;
            }
            chatAnimation.setSourcePosition(playerSource);
            chatAnimation.setTargetPosition(playerTarget);
            chatAnimation.sendPlayAnimation(data.msgid);
        }
    },
    //快捷语音
    playFasticoMsg: function (data, context) {
        var render = this.getUserRenderById(data.source);
        if (render != null) {
            if (data.msgname != "") {
                cc.mile.audio.playSFX(data.msgname);
            }
            var width = data.msgcontent.length * 25;
            width = width > 100 ? width : 100;
            var _x = 0;
            if (width > 100) {
                _x = (width - 100) / 2;
            }
            var messageX = -24;
            if (render.node._name == "player_top") {
                messageX = 133;
            } else if (render.node._name == "player_right") {
                messageX = -70;
                _x = _x * -1;
            } else if (render.node._name == "player_left") {
                messageX = 64;
            }
            render.message.setPositionX(messageX + _x);
            render.message.width = width;
            render.messageLab.node.width = width - 20;
            render.onOpenMessage(data.msgcontent);
            if (this.game.newchatbg.active) {
                this.game.newchatbg.active = false;
            }
            if (this.game.newchatbg_writing.active) {
                this.game.newchatbg_writing.active = false;
            }
        }
    },
    // 发送语音
    playVoiceMsg: function (data) {
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "playVoice", "(Ljava/lang/String;)V", data.msgid);

        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

            jsb.reflection.callStaticMethod("AppController",
                "playVoice:", data.msgid);

        }

        this.scheduleOnce(function () {
            this.sound_recording_check_count = 20;
            this.showVoiceMsgAnimation(data.msgid, data.source);
        }, 0.2);
    },
    // 显示语音的animation
    showVoiceMsgAnimation: function (msgid, targetUserId) {
        var downloaded = true;
        var playing = false;


        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

            downloaded = jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                "getDownLoadedStatus", "(Ljava/lang/String;)Z", msgid);
            if (downloaded) {
                playing = jsb.reflection.callStaticMethod("com/wangda/game/voice/gvoice/GvoiceManager",
                    "getDownLoadPlayingStatus", "(Ljava/lang/String;)Z", msgid);
            }
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

            downloaded = jsb.reflection.callStaticMethod("AppController",
                "getDownloadLoadFinishStatus:", msgid);
            if (downloaded) {
                playing = jsb.reflection.callStaticMethod("AppController",
                    "getPlayingFinishStatus:", msgid);
            }
        }
        if (!downloaded) { //检查10，如果都是没有下载完成
            this.sound_recording_check_count--;
        }
        if (this.sound_recording_check_count < 1) {
            this.updateUserVoiceAnimationStatus(targetUserId, false);
            return;
        }
        if (downloaded) {
            if (playing) {

                this.updateUserVoiceAnimationStatus(targetUserId, true);
                this.scheduleOnce(function () {
                    this.showVoiceMsgAnimation(msgid, targetUserId);
                }, 0.2);

                this.sound_recording_check_count--;
            } else {
                this.updateUserVoiceAnimationStatus(targetUserId, false);
            }
        } else {
            this.scheduleOnce(function () {
                this.showVoiceMsgAnimation(msgid, targetUserId);
            }, 0.4);
        }
    },
    /**
     * 清除自己打出去的牌
     */
    clearSelfCard: function () {
        var maziPoker = this.getCommon("MaziPoker");
        var countp = maziPoker.cunpoker.children.length;
        for (var i = 0; i < countp; i++) {
            maziPoker.cunpoker.removeChild(maziPoker.cunpoker.children[0]);
        }
        //for (var i = countp - 1; i >= 0; i--) {

        //}
    },
    /**
     * 清除所有玩家打出去的牌
     */
    clearAllPlayCardLayer: function () {
        for (var j = 0; j < this.playerRender.length; j++) {
            var userRender = this.playerRender[j];
            userRender.clearPlayCardLayer();
        }
        //删除自己的牌
        var pokers = this.getCommon("MaziPoker");
        pokers.clearSefAllPokers();
        this.selfPlayerRender.clearPlayCardLayer();
    },

    /**
     * 打牌的动画
     * @param data
     */
    playCardTypeAnimation: function (data) {
        if (data.cardType && data.cardType.mazicardtype == 6) { //飞机
            this.game.playFeiji();
        }
    },
    /**
     * 打牌的语音
     * @param cardType
     * @param cardValue
     */
    playCardTypeVoice: function (cardType, cardValue) {
        var voiceName = GameUtil.getMaziCardTypeVoiceName(cardType, cardValue);
        if (voiceName) {
            cc.mile.audio.playSFX("mazi/" + this.getSexPathPrefix() + "/" + voiceName);
        }
    },
    getUserRenderById: function (userid) {
        for (var i = 0; i < this.playerRender.length; i++) {
            var render = this.playerRender[i];
            if (render.userid == userid) {
                return render;
            }
        }
        return this.selfPlayerRender;
    },
    //返回数组
    getStrArray: function (str) {
        if (str != null) {
            return str.substr(0, str.length - 1).split(",");
        }
        else {
            return null;
        }
    },
    back: function () {
        this.confirm('确认退出吗？', function (self) {
            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi
            };
            self.loadding();
            self.closeConfirm();
            self.scene("mazihall", self);
        });
    },
    /**
     *   显示闹钟并倒计时
     *   @param playuser
     */
    showClock: function (playuser, clockShow) {
        var self = this;
        self.clock.active = clockShow;
        var po = cc.v2(-4, 36);
        if (playuser) {
            var _render = self.getUserRenderById(playuser);
            //清空桌面上的不出
            _render.clearPlayCardLayer();
            if (_render.positionDirection == "top") {
                po = cc.v2(-75, 204);
            } else if (_render.positionDirection == "right") {
                po = cc.v2(418, 130);
            } else if (_render.positionDirection == "left") {
                po = cc.v2(-435, 130);
            }
        }
        var _clocknum1 = 2;
        var _clocknum2 = 0;
        self.clock.setPosition(po);
        self.clock.active = clockShow;
    },
});
