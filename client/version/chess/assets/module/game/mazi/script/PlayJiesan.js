/**
 *
 * 单个用户解散
 *
 *
 */
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        userHeadNode: {
            default: null,
            type: cc.Sprite
        },
        userNameNode: {
            default: null,
            type: cc.Label
        },
        waittingNode: {
            default: null,
            type: cc.Node
        },
        agreeNode: {
            default: null,
            type: cc.Node
        },
        disagreeNode: {
            default: null,
            type: cc.Node
        },
    },

    onLoad: function () {
        // this.addTouchEvent();
    },
    initGiveUpData: function (userdata, isSendFirst) {
        debugger;
        this.userNameNode.string = userdata.username;
        this.userid = userdata.userid;
        if (userdata.giveupflag == 1 ) {
            this.setAgree();
        } else {
            this.setWaitting();
        }

        this.loadUrlJPGImage(userdata.userurl
            + "&username=" + userdata.userid, this.userHeadNode);
    },
    initData: function (userdata,isSendFirst) {
        this.userNameNode.string = userdata.username;
        this.userid = userdata.userid;
        if(userdata.dissflag == 1){
            this.setAgree();
        }else {
            this.setWaitting();
        }

        this.loadUrlJPGImage(userdata.userurl
            + "&username=" + userdata.userid,this.userHeadNode);
    },
    setWaitting: function () {
        this.waittingNode.active = true;
        this.agreeNode.active = false;
        this.disagreeNode.active = false;
    },
    setAgree: function () {
        this.waittingNode.active = false;
        this.agreeNode.active = true;
        this.disagreeNode.active = false;
    },
    setDisagree: function () {
        this.waittingNode.active = false;
        this.agreeNode.active = false;
        this.disagreeNode.active = true;
    },

});