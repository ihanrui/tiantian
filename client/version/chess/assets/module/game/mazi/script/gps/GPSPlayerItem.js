
/**
 * 查看历史
 */

var WangdaGameUtils = require("WangdaGameUtils");
var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        user_name_label: {
            default: null,
            type: cc.Label
        },
        user_gps_desc_label: {
            default: null,
            type: cc.Label
        },
        user_head_node: {
            default: null,
            type: cc.Node
        },
        open_gps_node: {
            default: null,
            type: cc.Node
        },
        close_gps_node: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.gameid = '';
    },
    initView: function (gpsData) {
        if(gpsData && gpsData.gameid){
            this.gameid = gpsData.gameid;
            this.user_name_label.string = gpsData.username;
            this.open_gps_node.active = true;
            this.close_gps_node.active = false;
            if(gpsData.userurl){
                this.loadUrlJPGImage(gpsData.userurl
                    + "&username=" + gpsData.userid, this.user_head_node);
            }else {
                this.user_head_node.active = false;
            }
            if(gpsData.locationdesc){
                this.user_gps_desc_label.string = gpsData.locationdesc;
            }
        }else {
            this.user_name_label.string = "座位为空";
            this.open_gps_node.active = false;
            this.close_gps_node.active = true;
            this.user_head_node.active = false;
        }

    },
    getItemGameId: function () {
      return this.gameid;
    },
    updateGpsDesc: function (desc) {
        if(desc){
            this.user_gps_desc_label.string = desc;
        }else {
            this.user_gps_desc_label.string = '';
        }
    },
    start: function () {

    },

    // update (dt) {},
});
