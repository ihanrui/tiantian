/**
 * gps 的界面
 */
var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        center_v_label: {
            default: null,
            type: cc.Label
        },
        center_h_label: {
            default: null,
            type: cc.Label
        },
        top_r_label: {
            default: null,
            type: cc.Label
        },
        top_l_label: {
            default: null,
            type: cc.Label
        },
        bottom_r_label: {
            default: null,
            type: cc.Label
        },
        bottom_l_label: {
            default: null,
            type: cc.Label
        },
        warn_desc_label: {
            default: null,
            type: cc.Label
        },
        gps_player_prefab: {
            default: null,
            type: cc.Prefab
        },
        container_node: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {
        this.itemRender = [];
    },

    initView: function () {
        cc.log('----获取GPS----');
        this.queryRecord();
        //this.bottom_r_label.setColor(Color3B(0,110,255))
    },
    setLabelRedColor: function (label) {
        label.node.color = new cc.Color(235, 6, 43);
    },
    setLabelGreyColor: function (label) {
        label.node.color = new cc.Color(97, 98, 112);
    },
    queryRecord: function () {


        cc.mile.http.httpPost("/record/querygps",
            {
                token: cc.mile.authorization,
            },
            this.successGetGPS, this.errorGetGPS, this);
    },
    successGetGPS: function (result, object) {
        cc.log(result);
        try {
            var data = JSON.parse(result);
            object.loadGPS(data);

        } catch (e) {
            cc.log("格式化信息出错:" + JSON.stringify(e));
        }
    },
    errorGetGPS: function (result, object) {
        cc.log(result);
        result.showCenterToast('获取GPS信息失败！');
    },

    loadGPS: function (data) {

        for (var i = 0; i < 4; i++) {
            if (data.userInfoList && data.userInfoList.length > i) {
                this.addGPSPlayer(data.userInfoList[i], i);
            } else {
                this.addGPSPlayer({}, i);
            }
        }
        this.updateUserDistance(data.gpsInfoList);
    },
    addGPSPlayer: function (playerData, index) {
        var player = cc.instantiate(this.gps_player_prefab);
        this.setGPSPlayerPosition(player, index);
        player.parent = this.container_node;
        var playerRender = player.getComponent("GPSPlayerItem");
        playerRender.initView(playerData);
        this.itemRender.push(playerRender);
    },

    updateUserDistance: function (gpsInfoList) {

        if (gpsInfoList.length > 1) {

            var topUserGameid = this.itemRender[0].getItemGameId();
            var leftUserGameid = this.itemRender[1].getItemGameId();
            var bottomUserGameid = this.itemRender[2].getItemGameId();

            var checkAllOpenGps = true;
            for (var i = 0; i < gpsInfoList.length; i++) {
                var gpsItem = gpsInfoList[i];
                if (gpsItem.gameid1 == topUserGameid &&
                    gpsItem.gameid2 == this.itemRender[1].getItemGameId()) {
                    this.setDistanceLabelInfo(this.top_l_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }
                if (gpsItem.gameid1 == topUserGameid &&
                    gpsItem.gameid2 == this.itemRender[2].getItemGameId()) {
                    this.setDistanceLabelInfo(this.center_v_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }
                if (gpsItem.gameid1 == topUserGameid &&
                    gpsItem.gameid2 == this.itemRender[3].getItemGameId()) {

                    this.setDistanceLabelInfo(this.top_r_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }


                if (gpsItem.gameid1 == leftUserGameid &&
                    gpsItem.gameid2 == this.itemRender[2].getItemGameId()) {

                    this.setDistanceLabelInfo(this.bottom_l_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }

                if (gpsItem.gameid1 == leftUserGameid &&
                    gpsItem.gameid2 == this.itemRender[3].getItemGameId()) {
                    this.setDistanceLabelInfo(this.center_h_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }

                if (gpsItem.gameid1 == bottomUserGameid &&
                    gpsItem.gameid2 == this.itemRender[3].getItemGameId()) {
                    this.setDistanceLabelInfo(this.bottom_r_label, gpsItem.distance);
                    if (gpsItem.distance < 1) {
                        checkAllOpenGps = false;
                    }
                }
            }
        }

        if (!checkAllOpenGps) {
            this.warn_desc_label.node.active = true;
        } else {
            this.warn_desc_label.node.active = false;
        }
    },

    setDistanceLabelInfo: function (label, distance) {

        cc.log("距离设置："+label._name+":"+distance);

        if (distance > 0) {
            label.string = '相距' + this.formatDistance(distance);
            this.setLabelGreyColor(label);
        } else {
            label.string = '未知距离';
            this.setLabelRedColor(label);

        }
    },
    formatDistance:function (disance) {
        var distanceInt = parseInt(disance);
        if(distanceInt > 1000){
            return Math.round(distanceInt / 1000) + '千米';
        }
        return distanceInt + '米';
    },
    setGPSPlayerPosition: function (node, index) {

        switch (index) {
            case 0:
                node.setPosition(4, 200);
                break;
            case 1:
                node.setPosition(-310, 20);
                break;
            case 2:
                node.setPosition(4, -150);
                break;
            case 3:
                node.setPosition(310, 20);
                break;
        }
    },

    start: function () {

    },

    // update (dt) {},
});
