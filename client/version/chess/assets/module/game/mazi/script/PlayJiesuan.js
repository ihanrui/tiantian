/**
 *
 * 单个用户结算
 *
 */
var beiMiCommon = require("MileCommon");
var WangdaGameUtils = require("WangdaGameUtils");

cc.Class({
    extends: beiMiCommon,

    properties: {
        userHeadNode: {
            default: null,
            type: cc.Sprite
        },
        userNameNode: {
            default: null,
            type: cc.Label
        },
        gameIdLabel: {
            default: null,
            type: cc.Label
        },
        winNumbersLabel: {
            default: null,
            type: cc.Label
        },
        zhuafenLabel: {
            default: null,
            type: cc.Label
        },
        firstTimesLabel: {
            default: null,
            type: cc.Label
        },
        newsTimesLabel: {
            default: null,
            type: cc.Label
        },

        bigWinTagNode: {
            default: null,
            type: cc.Node
        },
        totalScoreNode: {
            default: null,
            type: cc.Node
        },
        ticketScoreLabel: {
            default: null,
            type: cc.Label
        },
        ticketScoreNode: {
            default: null,
            type: cc.Node
        },

    },

    onLoad: function () {
        // this.addTouchEvent();
        /**
         *
         "username": "Guest_18tMEt",
         "totalscore": 0,
         "bankersum": 0,
         "lvlsum": 0,
         "newssum": 0,
         "winsum": 0,
         "maxscore": 0,
         "winflag": 0,
         "richflag": 0
         *
         */
    },
    initData: function (userdata,roomtype) {
        this.userNameNode.string = userdata.username;
        if(userdata.gameid){
            this.gameIdLabel.string = userdata.gameid;
        }
        this.loadUrlJPGImage(userdata.userurl
            + "&username=" + userdata.userid,this.userHeadNode );
        this.userid = userdata.userid;
        this.winNumbersLabel.string = userdata.winsum;// 总得分
        this.zhuafenLabel.string = userdata.maxscore;  // 抓分
        this.firstTimesLabel.string = userdata.lvlsum;
        this.newsTimesLabel.string = userdata.newssum;
        this.totalscore = userdata.totalscore;
        if(roomtype == 'card'){
            WangdaGameUtils.showNumberNode(userdata.totalscore,this.totalScoreNode);
        }else {
            // 如果是金币场  coins
            WangdaGameUtils.showNumberNode(userdata.coins,this.totalScoreNode);
        }

        if(userdata.ticket && userdata.ticket > 0){
            this.ticketScoreNode.active = true;
            this.ticketScoreLabel.string = '打赏老板：'+userdata.ticket;
        }else {
            this.ticketScoreNode.active = false;
        }

        var winFlag = userdata.winflag == 1 ? true: false;
        this.bigWinTagNode.active  = winFlag;
    }
});