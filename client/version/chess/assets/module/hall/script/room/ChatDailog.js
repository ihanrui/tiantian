// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        send_msg_btn: {
            default: null,
            type: cc.Button
        },
        send_msg_input: {
            default: null,
            type: cc.EditBox
        },
        tabQuickBtn: {
            default: null,
            type: cc.Button
        },
        emojiQuickBtn: {
            default: null,
            type: cc.Button
        },
        tabQuickScrollView: {
            default: null,
            type: cc.Node
        },
        emojiQuickScrollView: {
            default: null,
            type: cc.Node
        },
        itemTemplate: { // item template to instantiate other items
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad :function() {
        this.tabQuickScrollView.active = true;
        this.emojiQuickScrollView.active = false;
    },

    start :function() {

    },

    onItemClick:function (data) {
        console.log(data);
        cc.mile.audio.playSFX("chat/nan/"+data.currentTarget.name+".mp3");
        this.closeOpenWin();
        var desStr = data.currentTarget.children[0]. getComponents(cc.Component)[0].string;
        this.getCommon("DizhuDataBind").playermysql.getComponent("PlayerRender").showChatText(desStr);
    },
    onEmojiClick:function (data) {
        console.log(data);
        this.getCommon("DizhuDataBind").playermysql.getComponent("PlayerRender").playEmojiAnimation(data.currentTarget.name);

        this.closeOpenWin();
    },
    ontTabQuickBtn:function () {
        this.tabQuickScrollView.active = true;
        this.emojiQuickScrollView.active = false;
    },
    onEmojiQuickBtn:function () {
        this.tabQuickScrollView.active = false;
        this.emojiQuickScrollView.active = true;
    },
    onMsgBtnClick:function () {
        this.closeOpenWin();
        if(this.send_msg_input.string && this.send_msg_input.string.length > 0){
            this.getCommon("DizhuDataBind").playermysql.getComponent("PlayerRender").showChatText(this.send_msg_input.string);
        }
    }
    // update (dt) {},
});
