// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        musicopen:
            {
                default: null,
                type: cc.Node
            },
        musicclose:
            {
                default: null,
                type: cc.Node
            },
        soundopen:
            {
                default: null,
                type: cc.Node
            },
        soundclose:
            {
                default: null,
                type: cc.Node
            },
        speciallyopen:
            {
                default: null,
                type: cc.Node
            },
        speciallyclose:
            {
                default: null,
                type: cc.Node
            },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {

    },

    start:function () {

    },
    //���������
    onMusicOpenClick: function ()
    {
        this.musicopen.active =false;
        this.musicclose.active = true;
        cc.mile.audio.pauseAll();
    },
    //����ر�����
    onMusicCloseClick: function () {
        this.musicopen.active = true;
        this.musicclose.active = false;
        cc.mile.audio.playBGM();
    },
    //�������Ч
    onSoundOpenClick: function () {
        this.soundopen.active = false;
        this.soundclose.active = true;
    },
    //����ر���Ч
    onSoundCloseClick: function () {
        this.soundopen.active = true;
        this.soundclose.active = false;
    },
    onSpeciallyopenOpenClick: function () {
        this.speciallyopen= false;
        this.speciallyclose= true;
    },
    onSpeciallyopenCloseClick: function () {
        this.speciallyopen= true;
        this.speciallyclose= false;
    }
    // update (dt) {},
});
