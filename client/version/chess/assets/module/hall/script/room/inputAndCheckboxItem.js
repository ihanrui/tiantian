var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        itemname: {
            default: null,
            type: cc.Label
        },
        checkbox: {
            default: null,
            type: cc.Node
        },
        checkboxnode: {
            default: null,
            type: cc.Node
        },
        inputnode: {
            default: null,
            type: cc.Node
        },
        inputEditBox: {
            default: null,
            type: cc.EditBox
        }
    },
    onLoad: function () {

        this.node.on('checkbox', function (event) {
            if (this.checkbox != null) {
                if (this.checked == false) {
                    if (this.data.type == "radio") {
                        for (var inx = 0; inx < this.options.length; inx++) {
                            var script = this.options[inx];
                            script.doUnChecked();
                        }
                    }
                    this.doChecked();
                } else {
                    if (this.data.type == "radio") {
                        for (var inx = 0; inx < this.options.length; inx++) {

                            var script = this.options[inx];
                            script.doUnChecked();
                        }
                        this.doChecked();
                    } else {
                        this.doUnChecked();
                    }
                }
                var event = new cc.Event.EventCustom("ItemCheckChanged", true);
                event.setUserData({
                    group: this.data,
                    item: this.item
                });
                this.node.dispatchEvent(event);

            }
            event.stopPropagation();
        }, this);
    },
    init: function (item, group) {
        this.data = group;
        this.item = item;

        /**
         * 以下代码修正 OPTION超出宽度导致 点击错误的 问题

         if(group.style == "three"){
            this.itemname.node.width = 220 ;
            this.itemname.node.x = 107 ;
        }else{
            this.itemname.node.width = 220 ;
            this.itemname.node.x = 77 ;
        } */

        if (group.type == 'input') {
            this.inputnode.active = true;
            this.checkboxnode.active = false;
            this.inputEditBox.placeholder = item.name;
            if (item.value) {
                this.inputEditBox.string = item.value;
            }
        } else {
            if (this.inputnode) {
                this.inputnode.active = false;
            }
            this.checkboxnode.active = true;
            this.itemname.string = item.name;
            if (item.defaultvalue == true) {
                this.doChecked();
            } else {
                this.doUnChecked();
            }
        }
    },
    getItemCode: function () {
        return this.item.code;
    },
    getInputValue: function () {
        return this.inputEditBox.string;
    },
    doChecked: function () {
        this.checked = true;
        this.checkbox.active = true;
    },
    doUnChecked: function () {
        this.checked = false;
        this.checkbox.active = false;
    }
});
