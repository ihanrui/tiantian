var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        //游戏节点树（存放所有游戏）
        playways_node: {
            default: null,
            type: cc.Node
        },
        //游戏对象
        playway: {
            default: null,
            type: cc.Prefab
        },
        //游戏对象玩法容器（左侧菜单）
        playwaychild_container: {
            default: null,
            type: cc.Node
        },
        //游戏对象玩法（左侧菜单）
        playwaychild_node: {
            default: null,
            type: cc.Prefab
        },
        //游戏属性节点树（存放游戏属性）
        options_group_node: {
            default: null,
            type: cc.Node
        },
        //游戏属性组
        inputAndCheckbox_group: {
            default: null,
            type: cc.Prefab
        },
        dropdownselect_group: {
            default: null,
            type: cc.Prefab
        },
        row_group: {
            default: null,
            type: cc.Prefab
        },
        introduce: {
            default: null,
            type: cc.Label
        },
        createroombtn_node: {
            default: null,
            type: cc.Node
        },
        createroom_btn: {
            default: null,
            type: cc.Prefab
        },
        createroom_bg_node: {
            default: null,
            type: cc.Node
        },
        createType: 'room'
    },
    onLoad: function () {
        this.group = new Array();
        this.playwayarray = new Array();
        this.playwaychildarray = new Array();
        this.optionGroupLst = new Array();

        /**
         * 加载预制的 玩法
         */

        var gametype = cc.mile.game.type("createroom");
        if (gametype != null) {
            //加载游戏列表
            for (var inx = 0; inx < gametype.playways.length; inx++) {
                //从对象池中获取 playway 对象
                var playwayData = gametype.playways[inx];
                playwayData.childs = [{code: playwayData.code, groups: playwayData.groups, items: playwayData.items}];
                playwayData.dataIndex = inx;
                this.initPlayWay(playwayData, inx);
            }
        }

        try {
            var self = this;
            this.node.on('ItemCheckChanged', function (event) {
                console.log('-----ItemCheckChanged-----click--!');
                if (self) {
                    var itemData = event.getUserData();
                    console.log('-----ItemCheckData--:' + JSON.stringify(itemData));
                    self.dynamicUpdateView(itemData);
                }
            });
            this.node.on(cc.Node.EventType.TOUCH_START, function (e) {
                cc.log('-----nodeCheck-----click--!');
                for (var i = 0; i < this.dropdownselect_groupLst.length; i++) {
                    var cNode = this.dropdownselect_groupLst[i].getitemsContainerNode();
                    if (cNode && cNode.active && e.target._name != "selectList" && e.target._name != "itemlabel") {
                        cNode.active = false;
                    }
                }
            }, this, true);

        } catch (e) {
            this.loggerError('初始化房间出错：' + JSON.stringify(e) + e);
        }
    },
    initCreateType: function (type) {
        this.createType = type;
        if (this.createType != 'room') {
          //  var sprite = this.createroom_bg_node.getComponent(cc.Sprite);
          //  sprite.spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/hall/bg/chuangjianchalou.png"));
        }
    },
    dynamicUpdateView: function (checkItemData) {
        if (this.switchFrameCode == 'mazi') {
            this.scheduleOnce(function () {
                if (checkItemData.group.code == 'roomtype') {
                    this.updateSelectDefaultValue();
                    this.updateMaziDefaultValue(checkItemData.group.code, checkItemData.item.code);
                    this.onPlayWaySwitch(this.switchIndex);
                }
            }, 0.05);
        }
    },
    //初始化 PlayWay
    initPlayWay: function (playwayData, inx) {
        var playwayComponent = cc.instantiate(this.playway);
        if (playwayComponent == null) {
            return;
        }
        var playwayjs = playwayComponent.getComponent("PlayWay");//获取 playway 身上的 PlayWay 脚本 并进行初始化
        playwayjs.setIndex(inx);// 设置当前按钮的 index
        playwayComponent.parent = this.playways_node; //将生成的节点放在节点树上
        playwayComponent.on(cc.Node.EventType.TOUCH_START, function (e) {
            var clickPlayWay = e.currentTarget.getComponent("PlayWay");
            this.onPlayWaySwitch(clickPlayWay.getIndex());
        }, this, true);
        this._setPlayWaySpriteFrame(playwayData, playwayComponent, inx == 0);//根据玩法设置不同的图片
        this.playwayarray.push({node: playwayComponent, data: playwayData});
        if (inx == 0) {
            this.switchIndex = inx;
            this.loadPlayWayChildList(playwayData); //加载左侧玩法子项
        }
    },
    //PlayWay 点击事件
    onPlayWaySwitch: function (index) {
        cc.log("---playway---onPlaywaySwitch---");
        for (var inx = 0; inx < this.playwayarray.length; inx++) {
            var playway = this.playwayarray[inx].node;
            var spriteName = playway.getChildByName("item").getComponent(cc.Sprite).spriteFrame.name;
            playway.getChildByName("item").getComponent(cc.Sprite).spriteFrame = playway.getComponent("PlayWay").atlas.getSpriteFrame(spriteName.replace("1", ""));
        }
        this.switchIndex = index;
        //设置描述
        var playwayData = this.playwayarray[index].data;
        var playwayComponent = this.playwayarray[index].node;
        //cc.log("playwayData:" + JSON.stringify(playwayData));
        this._setPlayWaySpriteFrame(playwayData, playwayComponent, true);//切换背景图
        this.loadPlayWayChildList(playwayData);//加载左侧玩法子项
        //设置属性值
        //this.initPlayFrames(playwayData, this.playwayarray[index]);

    },
    //获取 PlayWay 背景图片
    _getPlayWaySpriteFrameName: function (code, isSelected) {
        var spriteFrame = '';
        if (code == "dizhu") {
            spriteFrame = "run";
        } else if (code == "pinshi") {
            spriteFrame = "douniu";
        } else {
            spriteFrame = code;
        }
        if (isSelected) {
            spriteFrame += "1";
        }
        return spriteFrame;
    },
    //设置 PlayWay 背景图片
    _setPlayWaySpriteFrame: function (playwayData, playwayComponent, isSelected) {
        this.switchFrameCode = playwayData.code;
        var spriteFrameName = this._getPlayWaySpriteFrameName(playwayData.code, isSelected);
        playwayComponent.getChildByName("item").getComponent(cc.Sprite).spriteFrame = playwayComponent.getComponent("PlayWay").atlas.getSpriteFrame(spriteFrameName);
    },
    //加载左侧玩法子项
    loadPlayWayChildList: function (playwayData) {
        this.playwaychild_container.removeAllChildren();
        while (this.playwaychildarray.length) {
            var tmp = this.playwaychildarray.pop();
            tmp.node.destroy();
        }
        for (var inx = 0; inx < playwayData.childs.length; inx++) {
            var childData = playwayData.childs[inx];
            childData.dataIndex = inx;
            this.initPlayWayChild(playwayData, childData, inx, inx == 0);
        }
    },
    //初始化 PlayWay 玩法子项
    initPlayWayChild: function (playwayData, childData, inx, isSelected) {
        var playwayChildComponent = cc.instantiate(this.playwaychild_node);
        if (playwayChildComponent == null) {
            return;
        }
        var playwayChildJs = playwayChildComponent.getComponent("PlayWayChild");//获取 playway 身上的 PlayWay 脚本 并进行初始化
        playwayChildJs.setIndex(inx);// 设置当前按钮的 index
        playwayChildComponent.setPosition(0, -45 - 90 * inx);
        playwayChildComponent.parent = this.playwaychild_container;
        playwayChildComponent.on(cc.Node.EventType.TOUCH_START, function (e) {
            var playWayChildJs = e.currentTarget.getComponent("PlayWayChild");
            this.onPlayWayChildSwitch(playwayData, playWayChildJs.getIndex());
        }, this, true);
        this._setPlayChildSpriteFrame(childData, playwayChildComponent, isSelected);//根据玩法设置不同的图片
        this.playwaychildarray.push({node: playwayChildComponent, data: childData});
        if (isSelected) {
            this.initPlayFrames(playwayData, childData, playwayChildComponent);//设置属性值
        }

    },
    //PlayWay 玩法子项 点击事件
    onPlayWayChildSwitch: function (playwayData, index) {
        for (var inx = 0; inx < this.playwaychildarray.length; inx++) {
            var playwayChildComponent = this.playwaychildarray[inx].node;
            var playwayChildData = this.playwaychildarray[inx].data;
            this._setPlayChildSpriteFrame(playwayChildData, playwayChildComponent, false);
        }
        var playwayChildData = this.playwaychildarray[index].data;
        var playwayChildComponent = this.playwaychildarray[index].node;
        this._setPlayChildSpriteFrame(playwayChildData, playwayChildComponent, true);
        this.initPlayFrames(playwayData, playwayChildData, playwayChildComponent);//设置属性值

    },
    //获取 PlayWay 子项背景图片
    _getPlayWayChildSpriteFrameName: function (code, isSelected) {
        var spriteFrame = '';
        if (code == "pinshi") {
            spriteFrame = "kanpaiqiangzhuang";
        } else {
            spriteFrame = code;
        }
        if (isSelected) {
            spriteFrame += "1";
        }
        return spriteFrame;
    },
    //设置 PlayWay 子项背景图片
    _setPlayChildSpriteFrame: function (childData, playwayChildComponent, isSelected) {
        var spriteFrameName = this._getPlayWayChildSpriteFrameName(childData.code, isSelected);
        playwayChildComponent.getChildByName("item").getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/common/create/" + spriteFrameName + ".png"));
    },
    //初始化 玩法 属性参数
    initPlayFrames: function (playwayData, childData, childComponent) {
        // 更新关联的状态信息
        this.updateRelationDatStatus(childData);
        //设置属性值
        this._setPlayWayChildData(playwayData, childData, childComponent);

    },
    // 更新关联的状态信息
    updateRelationDatStatus: function (childData) {

        if (this.switchFrameCode == 'mazi') {    // 如果是码子，将卡房和底分调整下关联关系
            for (var inx = 0; inx < childData.groups.length; inx++) {
                var group = childData.groups[inx];
                if (group.code == 'roomtype') {
                    for (var i = 0; i < childData.items.length; i++) {
                        var item = childData.items[i];
                        if (item.groupid == group.id && (item.code == 'card' || item.code == 'teacoin' || item.code == 'coin') && item.defaultvalue) {
                            this.updateMaziDefaultValue(group.code, item.code);
                            break;
                        }
                    }
                }
            }
        }

    },
    //设置属性值
    _setPlayWayChildData: function (playwayData, childData, childComponent) {
        this.options_group_node.removeAllChildren();
        if (this.inputAndCheckbox_group != null) {
            while (this.optionGroupLst.length) {
                var tmp = this.optionGroupLst.pop();
                tmp.destroy();
            }
            var indexOfRow = 0;

            for (var inx = 0; inx < childData.groups.length; inx++) {
                if (childData.groups[inx].hidden) {
                    continue;
                }

                var group = childData.groups[inx];
                var isBigRow = this.isBigRow(group, childData.items);

                var itemrow = cc.instantiate(this.row_group);
                itemrow.parent = this.options_group_node;
                if(isBigRow){
                    itemrow.setPosition(70, 90 - indexOfRow * 55);
                    itemrow.height = 110;
                    indexOfRow++;
                }else {
                    itemrow.setPosition(70, 120 - indexOfRow * 55);
                }
                if (childData.groups[inx].type == 'radio'
                    && childData.groups[inx].style != 'four'
                    && inx + 1 < childData.groups.length
                    && childData.groups[inx+1].type == 'radio'
                ) {
                    inx++;
                }
                indexOfRow++;
            }
            this.dropdownselect_groupLst = new Array();
            for (var inx = childData.groups.length - 1; inx >= 0; inx--) {
                if (childData.groups[inx].hidden) {
                    continue;
                }
                indexOfRow--;
                var group = childData.groups[inx];
                var isBigRow = this.isBigRow(group, childData.items);
                if(isBigRow){
                    indexOfRow--;
                }

                if (childData.groups[inx].type == 'radio'
                    && childData.groups[inx].style != 'four'
                    && inx - 1 >= 0
                    && childData.groups[inx-1].type == 'radio'

                ) {
                    inx--;
                    this.createGroupItemView(childData.groups[inx],childData.items,indexOfRow,false,isBigRow);
                    this.createGroupItemView(childData.groups[inx+1],childData.items,indexOfRow,true,isBigRow);
                }else {

                    this.createGroupItemView(childData.groups[inx],childData.items,indexOfRow,false,isBigRow);
                }

            }
            for(var i=0;i<this.dropdownselect_groupLst.length;i++) {
                this.dropdownselect_groupLst[i].setList(this.dropdownselect_groupLst);
            }
            this.createroombtn_node.removeAllChildren();
            var createroom_btn = cc.instantiate(this.createroom_btn);
            var createRoomBtnJs = createroom_btn.getComponent("CreateRoomBtn");
            createRoomBtnJs.setIndex(playwayData.dataIndex);
            createRoomBtnJs.setChildIndex(childData.dataIndex);
            //createRoomBtnJs.init(playwayData, playwayData.groups, groupLst);
            createroom_btn.on(cc.Node.EventType.TOUCH_START, function (e) {
                var btnJs = e.currentTarget.getComponent("CreateRoomBtn");
                this.onCreateRoomBtnClick(btnJs.getIndex(), btnJs.getChildIndex());
            }, this, true);
            createroom_btn.parent = this.createroombtn_node;

        }

    },
    createGroupItemView: function (groupdata,items,indexOfRow, isAlignRight,isBigRow) {
        if (groupdata.type == 'radio') {
            var newitem = cc.instantiate(this.dropdownselect_group);
            var script = newitem.getComponent("SelectGroup");
            script.init(groupdata, items);
            newitem.parent = this.options_group_node;
            if(!isAlignRight){
                newitem.setPosition(-88, 122 - indexOfRow * 55);
            }else {
                newitem.setPosition(282, 122 - indexOfRow * 55);
            }
            this.optionGroupLst.push(script);
            this.dropdownselect_groupLst.push(script);
        } else {
            var group = cc.instantiate(this.inputAndCheckbox_group);
            var optionGroup = group.getComponent("inputAndCheckboxGroup");
            optionGroup.init(groupdata, items);
            group.parent = this.options_group_node;
            if(isBigRow){
                group.setPosition(70, 90 - indexOfRow * 55);
            }else {
                group.setPosition(70, 120- indexOfRow * 55);
            }
            this.optionGroupLst.push(optionGroup);

        }
    },
    isBigRow: function (group,items) {
        if (group.type == 'radio') {
            return false;
        }
        var itemsnum = this.getGroupItemCount(group.id,items);
        if (group.style != null && group.style == "three") {
            if (itemsnum > 4) {
                return true;
            }
        } else {
            if (itemsnum > 4) {
                return true;
            }
        }
        return false;
    },
    getGroupItemCount: function (groupid, items) {
        var itemsnum = 0;
        for (var inx = 0; inx < items.length; inx++) {
            if (items[inx].groupid == groupid) {
                itemsnum = itemsnum + 1;

            }
        }
        return itemsnum;
    },
    updateMaziDefaultValue: function (groupCode, itemCode) {
        var playwayData = cc.mile.game.type("createroom").playways[this.switchIndex];
        if (groupCode == 'roomtype') {
            for (var inx = 0; inx < playwayData.groups.length; inx++) {
                var groupitem = playwayData.groups[inx];
                if (itemCode == 'card' && groupitem.code == 'scoreunit') {
                    groupitem.hidden = true;
                } else if ((itemCode == 'teacoin' || itemCode == 'coin') && groupitem.code == 'numofgames') {
                    groupitem.hidden = true;
                } else {
                    groupitem.hidden = false;
                }
            }
        }
    },
    updateSelectDefaultValue: function () {
        var playwayData = cc.mile.game.type("createroom").playways[this.switchIndex];
        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            for (var g = 0; g < playwayData.groups.length; g++) {
                var groupdata = playwayData.groups[inx];
                if (groupitem.data.code == groupdata.code) {
                    this.updateItemDataDefaultValue(groupitem.groupoptions, playwayData.items, groupdata.id);
                }
            }
        }
    },
    updateItemDataDefaultValue: function (groupoptions, dataItems, groupid) {
        for (var j = 0; j < groupoptions.length; j++) {
            var option = groupoptions[j];
            for (var i = 0; i < dataItems.length; i++) {
                if (dataItems[i].groupid == groupid) {
                    if (option.item.id == dataItems[i].id) {
                        dataItems[i].defaultvalue = option.checked;
                    }
                }
            }
        }
    },
    //创建房间 点击事件
    onCreateRoomBtnClick: function (index, childIndex) {
        var playwayData = this.playwayarray[index].data;
        var playwayChildData = this.playwaychildarray[childIndex].data;
        var extparams = {};
        var values = new Array();
        for (var inx = 0; inx < this.optionGroupLst.length; inx++) {
            var groupitem = this.optionGroupLst[inx];
            var value = "";
            for (var j = 0; j < groupitem.groupoptions.length; j++) {
                var option = groupitem.groupoptions[j];
                if (option.checked == true) {
                    if (value != "") {
                        value = value + ",";
                    }
                    value = value + option.item.value;
                }
            }
            extparams[groupitem.data.code] = value;
        }
        /**
         * 藏到全局变量里去，进入场景后使用，然后把这个参数置空
         * @type {{}}
         */
        extparams.gametype = playwayData.code;
        extparams.playway = playwayData.id;
        extparams.gamemodel = "room";
        extparams.random = Math.random();

        debugger
        /**
         * 发送创建房间开始游戏的请求
         */
        if (this.createType == 'room') {
            cc.mile.createroomtype = require("GameConstants").CREATE_ROOM_TYPE_CARD;
            this.preload(extparams, this);
        } else {
            var param = {
                playwayid: playwayData.id,
                extparams: extparams,
                token: cc.mile.authorization,
                gameid: cc.mile.user.gameid
            };
            this.emitSocketRequest("createteahouse", JSON.stringify(param));
            this.closeOpenWin();
        }
    }
});
