var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        grouptitle: {
            default: null,
            type: cc.Label
        },
        groupbox: {
            default: null,
            type: cc.Node
        },
        dropselectPrefab: {
            default: null,
            type: cc.Prefab
        },
        content: {
            default: null,
            type: cc.Node
        },
        //游戏属性
        option_item: {
            default: null,
            type: cc.Prefab
        },
    },
    // use this for initialization
    onLoad: function () {

    },
    init: function (group, items) {
        debugger
        this.data = group;
        this.groupoptions = new Array();
        this.checked = false;
        if (this.groupbox != null && this.option_item != null) {
            this.grouptitle.string = group.name + ":";

            var itemsnum = 0;
            for (var inx = 0; inx < items.length; inx++) {
                if (items[inx].groupid == group.id) {
                    itemsnum = itemsnum + 1;

                    cc.log(items[inx].groupid + "-11--" + items[inx].name);

                    var newitem = cc.instantiate(this.option_item);
                    newitem.parent = this.groupbox;
                    newitem.setPosition(0, -200 + inx * 150);
                    var script = newitem.getComponent("inputAndCheckboxItem");
                    this.groupoptions.push(script);
                    script.init(items[inx], group);
                }
            }

            if (group.style != null && group.style == "three") {
                if (itemsnum > 4) {
                    this.content.height = 30 + 45 * (parseInt((itemsnum - 1) / 3) + 1);
                    this.groupbox.height = 45 * (parseInt((itemsnum - 1) / 3) + 1);
                }
            } else {
                if (itemsnum > 4) {
                    this.content.height = 34 + 45 * (parseInt((itemsnum - 1) / 4) + 1);
                    this.groupbox.height = 49 * (parseInt((itemsnum - 1) / 4) + 1);
                }
            }


        }
    },
    getGroupData: function () {
        return this.data;
    }
});
