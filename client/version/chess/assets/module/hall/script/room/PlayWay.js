var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        item:{
            default:null ,
            type : cc.Node
        },
        createroom_btn:{
            default:null ,
            type : cc.Prefab
        },
        index: -1,
    },
    onLoad :function() {
        this.group = new Array();
    },
    setIndex: function (index) {
      this.index = index;
    },
    getIndex: function () {
      return this.index;
    },
    onPlayMouseDown:function(data){
       this.node.dispatchEvent( new cc.Event.EventCustom("playmousedown", true) );
    }
});