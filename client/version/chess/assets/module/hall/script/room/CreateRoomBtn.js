var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        index: -1,
        childIndex: -1,
    },
    setIndex: function (index) {
        this.index = index;
    },
    getIndex: function () {
        return this.index;
    },
    setChildIndex: function (index) {
        this.childIndex = index;
    },
    getChildIndex: function () {
        return this.childIndex;
    },
    onLoad:function(){

    }
});
