// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        number  : {
            default: null,
            type: cc.Node
        },
        number1  : {
            default: null,
            type: cc.Node
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        conlist:{
            default: null,
            type: cc.Node
        },
        card: {
            default: null,
            type: cc.Prefab
        },
        posy:cc.Integer,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        
        //存放玩家数据
        this.player = new Array() ;
        //存放玩家选中的牌
        this.pokercards= new Array() ;
        this.minpokerpool = new cc.NodePool(); //背面
        for (i = 0; i <27; i++) {
            //实例化预制资源按时阿斯蒂芬
            var codes=cc.instantiate(this.card);
            //将用户下的牌加入到数组当中
            this.player[i]=i;
            //获取对应的牌给预制资源
           // codes.children[0].getComponent(cc.Sprite).spriteFrame=this.atlas.getSpriteFrame(i);
            codes.getComponent(cc.Sprite).spriteFrame=this.atlas.getSpriteFrame(i);

           //codes.children[0].getComponent(cc.Button).clickEvents.push(this.onClickCads);
          //  codes.children[0].getComponent(cc.Button).node.on('click',this.onClickCads);
            //加入到对象当中
            this.minpokerpool.put(codes);
        }
        this.pokerarr= new Array() ;
        //判断对象是否存在值
        if (this.minpokerpool.size() > 0) {
            //获取最左边的位置
            var leftwith= this.getLeftRange(this.minpokerpool.size());
            //循环对象
            for(i=0;i<this.minpokerpool.size();i++) {
                //将一个一个牌显示在对象池当中
                var margleftwidth = leftwith-i * 46;
                var a = this.minpokerpool._pool[i];
                /*
                var script = a.getComponent("poker");
                script.init(this.pokercards);
                */
                a.setPosition(-margleftwidth, 0);
                a.parent = this.conlist;
            }
        }
        //当前坐标
        this.posy =23;
    },
    start:function () {

    },
    //点击牌的动作
   onClickCads:function (event) {
        if(this.pokercards.count()>0)
        {
            this.pokercards[this.pokercards.count()+1]=event.target.parent.getComponent(cc.Sprite).spriteFrame._name;
        }
        else {
            this.pokercards[0] = event.target.parent.getComponent(cc.Sprite).spriteFrame._name;
        }
        alert(this.pokercards.count());
           if (event.target.y== 23) {
               event.target.y = event.target.y+ 30;
               //beiMiCard.selected = true;
           } else {
               event.target.y = event.target.y - 30;
       }
   },
    //获取最右边的位置
    ///获取牌的总数，计算出牌最第一张牌的位置
   getLeftRange:function (cardscount) {
        //获取总宽度
       //var width=this.conlist.width;
       //求中间值,130是最后一个牌的位置
       var left=((46*cardscount)-150)/2;
       return left;
   }
// update (dt) {},
});
