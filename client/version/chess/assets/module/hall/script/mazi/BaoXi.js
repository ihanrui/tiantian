// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        card4Container:{
            default: null,
            type: cc.Node
        },
        card5Container:{
            default: null,
            type: cc.Node
        },
        card6Container:{
            default: null,
            type: cc.Node
        },
        card7Container:{
            default: null,
            type: cc.Node
        },
        card8Container:{
            default: null,
            type: cc.Node
        },
        cardAtlas:{
            default: null,
            type: cc.SpriteAtlas
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad:function() {

    },
    showTipCards:function (cards) {

        cc.log('报喜牌:'+ cards);

        var node = this.getCardsContainer(cards.length);
        if(node){
            var childItems = node.children;
            for(var i = 0 ; i < childItems.length;i++){
                this.updateChildCards(childItems[i],cards[i]);
            }
            node.active = true;
        }
    },
    updateChildCards: function (node,cardValue) {

        var width = node.width;
        var height = node.height;

        node.getComponent(cc.Sprite).spriteFrame =
                this.cardAtlas.getSpriteFrame(cardValue);
        node.width = width;
        node.height = height;
    },
    getCardsContainer: function (size) {
        switch(size){
            case 4:
                return this.card4Container;
            case 5:
                return this.card5Container;
            case 6:
                return this.card6Container;
            case 7:
                return this.card7Container;
            case 8:
                return this.card8Container;
        }

    },
    start:function() {

    },

    // update (dt) {},
});
