

var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        musicopen:
            {
                default: null,
                type: cc.Node
            },
        musicclose:
            {
                default: null,
                type: cc.Node
            },
        soundopen:
            {
                default: null,
                type: cc.Node
            },
        soundclose:
            {
                default: null,
                type: cc.Node
            },
        speciallyopen:
            {
                default: null,
                type: cc.Node
            },
        speciallyclose:
            {
                default: null,
                type: cc.Node
            },
        quitTeaHoseNode:
            {
                default: null,
                type: cc.Node
            },
        toggleContainer: {
            default: null,
            type: cc.Node
        },
        bgInit:true,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        var bgIndex=cc.mile.audio.getBgIndex();
        this.updateMusicIndexView(bgIndex);
        this.toggleContainer.active=false;
        if (cc.mile.audio.getBgVolume() > 0) {
            this.updateMusicStatusView(true);

        } else {
            this.updateMusicStatusView(false)
        }
        if (cc.mile.audio.getSfxVolume() > 0) {
            this.updateSoundStatusView(true);

        } else {
            this.updateSoundStatusView(false)
        }
    },

    start: function () {

    },
    //���������
    onMusicOpenClick: function () {
        cc.mile.audio.setBgVolume(0);
        cc.mile.audio.stopAll();
        this.updateMusicStatusView(false)

    },
    //����ر�����
    onMusicCloseClick: function (play) {
        cc.mile.audio.setBgVolume(1);
        cc.mile.audio.playBGM();
        this.updateMusicStatusView(true);
    },//���������

    updateMusicStatusView: function (openVoice) {
        this.musicopen.active = openVoice;
        this.musicclose.active = !openVoice;
    },
    //选择背景音乐
    onMusicIndexClick: function (e,index) {
        debugger
        if(!this.bgInit){
            cc.mile.audio.setBgIndex(index);
            cc.mile.audio.playBGM();
        }
        //this.updateMusicStatusView(true);
    },
    updateMusicIndexView: function (index) {
        debugger
        this.scheduleOnce(function () {
            debugger
            for (var i=0;i<3;i++)
            {
                if(i!=index-1){
                    this.toggleContainer.children[i].getComponent(cc.Toggle).isChecked=false;
                }else{
                    this.toggleContainer.children[index-1].getComponent(cc.Toggle).isChecked=true;
                }
            }
            this.toggleContainer.active=true;
            this.bgInit=false;
        }, 0.01);
    },
    //�������Ч
    onSoundOpenClick: function () {
        cc.mile.audio.setSFXVolume(0);
        this.updateSoundStatusView(false);
    },
    //����ر���Ч
    onSoundCloseClick: function () {
        cc.mile.audio.setSFXVolume(1);
        this.updateSoundStatusView(true);

    },
    updateSoundStatusView: function (openSound) {

        this.soundopen.active = openSound;
        this.soundclose.active = !openSound;

    },

    //�������Ч
    onSpeciallyOpenClick: function () {
        this.speciallyopen.active = false;
        this.speciallyclose.active = true;
    },
    //����ر���Ч
    onSpeciallyCloseClick: function () {
        this.speciallyopen.active = true;
        this.speciallyclose.active = false;
    },
    updateQuiteBtnViewStatus: function (status) {
        if(this.quitTeaHoseNode){
            this.quitTeaHoseNode.active = status;
        }
    },
    onQuitTeaHouseClick: function () {
        var self = this;
        this.confirm('离开后将看不到此茶楼？', function (self) {

            var param = {
                token: cc.mile.authorization,
                orgi: cc.mile.user.orgi,
                teanum: cc.mile.teahouse.teanum,
                gameid: cc.mile.user.gameid,
                passflag: "out",
                role: "normal",
            };
            self.emitSocketRequest("memupdate", JSON.stringify(param));

            self.closeConfirm();
        });
    },
    // update (dt) {},
});
