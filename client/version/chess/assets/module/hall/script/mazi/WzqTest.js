var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        gamebtn: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:
     onLoad:function () {
         console.log('load');
         this.resize();
         this.player = new Array() ;     //存放玩家数据
         this.pokercards = new Array();
         this.lastcards = new Array();
         this.lastCardsPanel.active = false ;
         this.summarypage = null ;
         this.inited = false ;
         this.lasttip = null ;
         var a=1;
         if(cc.mile!=null){
             if(cc.mile.gamestatus!=null && cc.mile.gamestatus == "playing"){
                 //恢复数据
                 this.recovery() ;
             }else if(cc.mile.extextparams.gamemodel == "room"){
                 /**
                  *params!=null && cc.mile. 房卡模式，开始启动游戏，当前玩家进入等待游戏的状态，显示邀请好友游戏，并分配 6位数字的房间号码
                  */
                 /**
                  * 处理完毕，清理掉全局变量
                  * @type {null}
                  */
                 // this.invite = cc.instantiate(this.inviteplayer) ;
             }
             this.initgame();
         }
     },
    initgame:function(){
        var self = this ;
        this.gamebtn.active = true ;
        cc.log("初始化游戏  this.ready() 【"+this.ready()+"】")
        if(this.ready()) {
            var socket = this.socket();
            this.game = this.getCommon("DizhuDataBind");
            this.map("joinroom" , this.joinroom_event) ;          //加入房价
            this.map("players" , this.players_event) ;            //接受玩家列表
            this.map("play" , this.play_event) ;                      //接受玩家列表
            this.map("playeready" , this.playeready_event) ;            //玩家点击了开始游戏 ， 即准备就绪
            this.map("cardtips" , this.cardtips_event) ;              //提示
            socket.on("command" , function(result){
                cc.mile.gamestatus = "playing" ;
                if(self.inited == true){
                    var data = self.parse(result) ;
                    self.route(data.command)(data , self);
                }
            });
            /**
             * 心跳检查，服务端发起的事件，服务端可以设置 PING的时长和 PING的 TimeOut
             */
            socket.on("ping" , function(){

            });

            /**
             *   此处有个问题：游戏中，如果点击了返回，进入过其他游戏，这个 cc.mile.extparams.playway
             *   就发生了改变，会导致当前游戏结束后，继续游戏的时候会出现问题
             * @type {{token: (*|null), playway, orgi, extparams: *}}
             */
            
            var param = {
                token:cc.mile.authorization,
                playway:cc.mile.extparams.playway,
                orgi:cc.mile.user.orgi,
                extparams:cc.mile.extparams
            } ;
            socket.emit("joinroom" ,JSON.stringify(param)) ;
            this.inited = true ;
        }
    },
    /**
     * 新创建牌局，首个玩家加入，进入等待状态，等待其他玩家加入，服务端会推送 players数据
     * @param data
     * @param context
     */
    joinroom_event:function(data , context){
        
        cc.log("data.cardroom ["+data.cardroom  +"]context.inviteplayer ["+context.inviteplayer+"]");

        if(data.cardroom == true && context.inviteplayer!=null){
            /*var script = context.invite.getComponent("MileQR")
            script.init(data.roomid);
            context.invite.parent = context.root() ;*/
            context.game.roomnum_format(context.game,data.roomid);
        }
        //显示 匹配中，并计时间，超过设定的倒计时时间即AI加入，根据当前的 玩家数量 匹配机器人
        if(data.player.id && data.player.id == cc.mile.user.id){
            //本人，开始计时
            //console.log("本人") ;
            //self.player[0] = data ;
            context.index = data.index ;   //当前玩家所在位置
        }else{
            //其他玩家加入，初始化
            var inroom = false ;
            for(var i = 0 ; i < context.player.length ; i++){
                var player = context.player[i].getComponent("PlayerRender") ;
                if(player.userid == data.player.id){
                    inroom = true ;
                }
            }
            if(inroom == false){
                context.newplayer(context.player.length , context , data.player , context.index + 1 == data.index) ;
            }
        }
    },
    // update (dt) {},
});
