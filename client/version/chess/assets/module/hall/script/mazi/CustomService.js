// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {

        guanwangLabel: {
            default: null,
            type: cc.Label
        },
        qqLabel: {
            default: null,
            type: cc.Label
        },
        weixin001Label: {
            default: null,
            type: cc.Label
        },
        weixin002Label: {
            default: null,
            type: cc.Label
        },
        phoneLabel: {
            default: null,
            type: cc.Label
        },
        gongzhonghaoLabel: {
            default: null,
            type: cc.Label
        },
    },

    onLoad: function () {
        this.queryRecord();
    },
    queryRecord: function () {
        //获取配置信息
        var params = {
            code: 'linkstr',
        };

        cc.mile.http.httpPost("/hall/getJsonstr",
            params,
            this.successGetHistory, this.errorGetHistory, this);
    },
    successGetHistory: function (result, object) {
        cc.log(result);
        try {
            var data = JSON.parse(result);
            object.initView(data);

        } catch (e) {
            cc.log("格式化信息出错:" + JSON.stringify(e));
        }


    },
    errorGetHistory: function (result, object) {
        cc.log(result);
        require('WangdaGameUtils').showCenterToast('网络请求失败');
    },

    initView: function (data) {
        this.setLabelValue(this.qqLabel, data.qq);
        this.setLabelValue(this.phoneLabel, data.phone);
        this.setLabelValue(this.weixin001Label, data.wechat001);
        this.setLabelValue(this.weixin002Label, data.wechat002);
        this.setLabelValue(this.gongzhonghaoLabel, data.servicewechat);
        this.setLabelValue(this.guanwangLabel, data.website);
        this.setLabelValue();
        this.setLabelValue();
        this.setLabelValue();
    },

    setLabelValue: function (label, value) {
        if (value) {
            label.string = value;
        } else {
            label.string = "";
        }
    },
    copyToClipboard: function (txt) {
        if (txt) {
            cc.log("复制内容：" + txt);
            var gameUtils = require("WangdaGameUtils");
            gameUtils.copyToClipboard(txt);
            gameUtils.showCenterToast('复制成功');
        }

    },

    onCopyWeixin001CopyClick: function () {
        this.copyToClipboard(this.weixin001Label.string);
    },
    onCopyWeixin002CopyClick: function () {
        this.copyToClipboard(this.weixin002Label.string);
    },
    onQQCopyClick: function () {
        this.copyToClipboard(this.qqLabel.string);
    },
    onPhoneCopyClick: function () {
        this.copyToClipboard(this.phoneLabel.string);
    },
    onGongzhonghaoCopyClick: function () {
        this.copyToClipboard(this.phoneLabel.string);
    },

    onCopyPhoneNumClick: function () {

        var gameUtils = require("WangdaGameUtils");

        gameUtils.copyToClipboard("15074960287");

        var toast = Toast.makeText('复制成功', Toast.LENGTH_SHORT);
        toast.setGravity(Toast.CENTER, 0, 0);
        toast.show();
    },

    // update (dt) {},
});
