/**
 *
 * 大厅  我的茶楼列表
 *
 */

var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,

    properties: {
        teahouse_name: {
            default: null,
            type: cc.Label
        },
        teaowner_head: {
            default: null,
            type: cc.Node
        },
        approve_status: {
            default: null,
            type: cc.Node
        },
        teaowner_name: {
            default: null,
            type: cc.Label
        },
        passflag: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        var self = this;

        this.node.on('teaItemMousedown', function (event) {
            console.log('-----teaItemMousedown-----click--!');
            if (self && self.teahouseData) {
                cc.mile.teahouse = self.teahouseData;
                //cc.mile.extparams = JSON.parse(self.teahouseData.extparams);
                //"extparams": "{\"numofgames\":\"4\",\"newsflag\":\"Y\",\"giveupflag\":\"N\",\"gametype\":\"mazi\",\"gamemodel\":\"room\",\"teamflag\":\"Y\",\"coinexpend\":\"Y\",\"playway\":\"4028ab6c621a9d8301621ac4713d0003\"}",
                self.scene("floor", self);
            }

        });

    },

    start: function () {

    },

    initItem: function (data) {

        console.log("大厅加载我的茶楼");
        cc.log(data);
        this.teahouseData = data.teahouse;
        this.teahouse_name.string = data.teahouse.name + "(" + data.teahouse.teanum+")";
        this.teaowner_name.string ="老板:"+ data.teahouse.username;

        this.loadUrlJPGImage(data.teahouse.userurl
            + "&username=" + data.teahouse.userid, this.teaowner_head);
        if (data.teamem.passflag != "pass") {
            this.passflag.active = true;
        }

    },

    onTeaItemClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("teaItemMousedown", true));
    }
    // update (dt) {},
});
