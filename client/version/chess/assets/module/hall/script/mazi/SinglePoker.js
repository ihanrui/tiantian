// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        cardMask: {
            default: null,
            type: cc.Node
        },
        cardAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        cardNode: {
            default: null,
            type: cc.Node
        },
        scoreMy: {
            default: null,
            type: cc.Node
        },
        laiziFlagNode: {
            default: null,
            type: cc.Node
        },
        lastCardFlagNode: {
            default: null,
            type: cc.Node
        }
    },

    initPoker: function (pokerValue) {
        this.cardNode.getComponent(cc.Sprite).spriteFrame = this.cardAtlas.getSpriteFrame(pokerValue);
    },

    updateMaskStatus: function (status) {
        this.cardMask.active = status;
    },
    updateScoreMy: function (status) {
        this.scoreMy.active = status;
    },
    updateLaiziFlag: function (status) {
        this.laiziFlagNode.active = status;
    },
    updateLastCardFlag: function (status) {
        this.lastCardFlagNode.active = status;
    }

    // update (dt) {},
});
