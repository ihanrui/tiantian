var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        username: {
            default: null,
            type: cc.EditBox
        },
        idcard: {
            default: null,
            type: cc.EditBox
        }
      },

      realNameSubmit: function () {
        this.io = require("IOUtils");
        //this.loadding();
            //通过ID获取 玩家信息
            var data = JSON.parse(this.io.get("userinfo"));
            if (data.token != null) {     //获取用户登录信息
                var params ={
                    token: data.token.id,
                    uname: this.username.string,
                    idcard:this.idcard.string,
                    mobile:"15292188398"
                };
                if(params.uname==""){
                    var toast = Toast.makeText('姓名必填', Toast.LENGTH_SHORT);
                    toast.setGravity(Toast.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if(params.idcard==""){
                    var toast = Toast.makeText('身份证号必填', Toast.LENGTH_SHORT);
                    toast.setGravity(Toast.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if(!this.isCardNo(this.idcard.string)){
                    var toast = Toast.makeText('身份证号不符合规则', Toast.LENGTH_SHORT);
                    toast.setGravity(Toast.CENTER, 0, 0);
                    toast.show();
                    return;   
                }
                cc.log("自己测试token data.token.id ["+data.token.id+"]");
                var xhr = cc.mile.http.httpPost("/hall/gameCertification",params, this.sucess, this.error, this);
            }
        
    },

    sucess: function (result, object) {
        var data = JSON.parse(result);
        if(data){
            //  设置显示文本与显示时间
            var toast = Toast.makeText('实名认证成功', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
            setTimeout(function(){object.closeOpenWin();},1000)
        }
    },
    error: function (object) {
        object.closeloadding(object.loaddingDialog);
        object.alert("网络异常，服务访问失败");
        object.loginFlag = false;
    },
    
     isCardNo:function(card)  {  
       // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X  
       var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
       if(reg.test(card) === false){  
        return false; 
       } 
       return true; 
    },  

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},
});
