var beiMiCommon = require("MileCommon");
// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: beiMiCommon,
    properties: {
        combatgains: {
            default: null,
            type: cc.Prefab
        },
        customservice: {
            default: null,
            type: cc.Prefab
        },
        share: {
            default: null,
            type: cc.Prefab
        },
        realname: {
            default: null,
            type: cc.Prefab
        },
        agent: {
            default: null,
            type: cc.Prefab
        },
        feedback: {
            default: null,
            type: cc.Prefab
        },
        shopPrefab: {
            default: null,
            type: cc.Prefab
        },
        music: {
            default: null,
            type: cc.Prefab
        },
        createRoom: {
            default: null,
            type: cc.Prefab
        },
        createTeahouse: {
            default: null,
            type: cc.Prefab
        },
        invitePlayerPrefab: {
            default: null,
            type: cc.Prefab
        },

        first: {
            default: null,
            type: cc.Node
        },
        second: {
            default: null,
            type: cc.Node
        },
        gamepoint: {
            default: null,
            type: cc.Node
        },
        title: {
            default: null,
            type: cc.Node
        },
        global: {
            default: null,
            type: cc.Node
        },
        coinPlaywayPrefab: {
            default: null,
            type: cc.Prefab
        },
        content: {
            default: null,
            type: cc.Node
        },
        girl: {
            default: null,
            type: cc.Node
        }
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {

        this.playwayarray = new Array();

    },

    start: function () {
        this.getGPSLocation();
    },
    getGPSLocation: function () {
        var latitude = 0;
        var longitude = 0;
        var locationDesc = '';
        cc.log('---获取用户定位开始： latitude：[' + latitude + '],longitude:[' + longitude + ']');
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            latitude = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getLatitude", "()F");
            longitude = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getLongitude", "()F");
            locationDesc = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                "getGPSDesc", "()Ljava/lang/String;");

        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            latitude = jsb.reflection.callStaticMethod("AppController",
                "getLatitude");
            longitude = jsb.reflection.callStaticMethod("AppController",
                "getLongitude");
            locationDesc = jsb.reflection.callStaticMethod("AppController",
                "getGPSDesc");
        }
        cc.log('---获取用户定位结束： latitude：[' + latitude + '],' +
            'longitude:[' + longitude + ']' +
            'locationDesc:[' + locationDesc + ']');
    },
    // 历史战绩
    onCombatGainsClick: function () {
        cc.mile.teahouserrole = 'normal';
        cc.mile.openwin = cc.instantiate(this.combatgains);
        cc.mile.openwin.parent = this.root();
    },
    onCustomServiceClick: function () {
        cc.mile.openwin = cc.instantiate(this.customservice);
        cc.mile.openwin.parent = this.root();
    },
    onAgentClick: function () {
        cc.mile.openwin = cc.instantiate(this.agent);
        cc.mile.openwin.parent = this.root();
    },
    onShareClick: function () {
        cc.mile.openwin = cc.instantiate(this.share);
        cc.mile.openwin.parent = this.root();
        var shareScript = this.getPrefabCommon("share", "FenxiangDialog");
        shareScript.initShareType("share_hall");
    },
    onInvitePlayerClick: function () {
        cc.mile.openwin = cc.instantiate(this.invitePlayerPrefab);
        cc.mile.openwin.parent = this.root();
    },
    onRealNameClick: function () {
        cc.mile.openwin = cc.instantiate(this.realname);
        cc.mile.openwin.parent = this.root();
    },
    onFeedbackClick: function () {
        cc.mile.openwin = cc.instantiate(this.feedback);
        cc.mile.openwin.parent = this.root();
    },
    onMusicClick: function () {
        cc.mile.openwin = cc.instantiate(this.music);
        cc.mile.openwin.parent = this.root();
    },


    onSecondBack: function (event, data) {
        this.playToRight();
        this.collect();
        this.disMenu("first");
    },
    onFirstClick: function () {

        if (cc.mile.coinPlayway) {
            this.loadPlayway();
        } else {
            this.getCoinPlayway();
        }
    },
    disMenu: function (order) {
        if (order == 'first') {
            this.first.active = true;
            this.second.active = false;
            if (this.third != null) {
                this.third.active = false;
            }
        } else if (order == 'second') {
            this.first.active = false;
            this.second.active = true;
            if (this.third != null) {
                this.third.active = false;
            }
        } else if (order == 'third') {
            this.first.active = false;
            this.second.active = false;
            if (this.third != null) {
                this.third.active = true;
            }
        }
    },

    getCoinPlayway: function () {

        cc.mile.http.httpPost("/hall/getgames",
            {
                token: cc.mile.authorization,
                code: 'mazi'
            },
            this.successGetHistory, this.errorGetHistory, this);
    },
    successGetHistory: function (result, object) {
        cc.log(result);
        object.closeloadding();
        try {
            var data = JSON.parse(result);
            cc.mile.coinPlayway = data;
            object.loadPlayway();

        } catch (e) {
            cc.log("格式化信息出错:" + JSON.stringify(e));
        }
    },
    errorGetHistory: function (result, object) {
        result.closeloadding();
        result.showCenterToast("数据请求出错，请重试");
    },

    loadPlayway: function () {
        this.playToLeft();
        //this.collect();
        this.disMenu("second");

        this._secondAnimCtrl = this.second.getComponent(cc.Animation);
        this._secondAnimCtrl.play("playway_display");

        /**
         * 加载预制的 玩法
         */
        var gametype = cc.mile.coinPlayway;
        cc.log("加载预制的 玩法：" + JSON.stringify(gametype));

        if (gametype != null && gametype.length > 0) {
            var games = gametype[0];
            var gametypes = games.types[0];

            this.content.removeAllChildren();

            for (var inx = 0; inx < gametypes.playways.length; inx++) {
                /**
                 * 此处需要做判断，检查 对象池有足够的对象可以使用
                 */
                cc.log("玩法11：" + JSON.stringify(gametypes.playways[inx]));

                var playItem = cc.instantiate(this.coinPlaywayPrefab);

                var script = playItem.getComponent("Playway");

                if (script) {
                    script.init(gametypes.playways[inx]);
                }
                playItem.parent = this.content;
                this.playwayarray.push(playItem);

            }
        }
    },
    collect: function () {
        /*for(var inx =0 ; inx < this.playwayarray.length ; inx++){
            this.playwaypool.put(this.playwayarray[inx]);
        }*/

        this.playwayarray.splice(0, this.playwayarray.length);
    },
    playToLeft: function () {
        this._girlAnimCtrl = this.girl.getComponent(cc.Animation);
        this._girlAnimCtrl.play("women-to-left");
    },
    playToRight: function () {
        this._girlAnimCtrl = this.girl.getComponent(cc.Animation);
        this._girlAnimCtrl.play("women-to-right");
    },

    onShopClick: function (e, data) {

        if (cc.mile.appRecharge) {
            cc.mile.openwin = cc.instantiate(this.shopPrefab);
            cc.mile.openwin.parent = this.root();
            this.getPrefabCommon("houseshop", "ShopDialog").init(data);

        } else {
            var toast = Toast.makeText('如需充值，请联系客服！', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();

        }

    },
    onLotteryClick: function () {
        var toast = Toast.makeText('抽奖显示未开始。本赛季活动未开始，敬请关注。', Toast.LENGTH_SHORT);
        toast.setGravity(Toast.CENTER, 0, 0);
        toast.show();
    },
    //创建房间
    onCreateRoom: function () {
        var createView = cc.instantiate(this.createRoom);
        cc.mile.openwin = createView;
        cc.mile.openwin.parent = this.root();
    },
    //创建茶楼
    onCreateTeahouse: function () {
        var createView = cc.instantiate(this.createTeahouse);
        cc.mile.openwin = createView;
        cc.mile.openwin.parent = this.root();
        createView.getComponent("CreateRoom").initCreateType('teaHouse');
    }
});
