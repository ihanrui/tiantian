var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        mobile:{
            default:null,
            type:cc.EditBox
        },
        wx:{
            default:null,
            type:cc.EditBox
        },
        qq:{
            default:null,
            type:cc.EditBox
        }
    },
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {},
    onPost:function(){
        this.io = require("IOUtils");
        //this.loadding();
        var params={
            token:"",
            mobile:this.mobile.string,
            wx:this.wx.string,
            qq:this.qq.string,
        };
        if(params.mobile==""){
            var toast = Toast.makeText('手机号码必填', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
            return;
        }
        if(params.wx==""){
            var toast = Toast.makeText('微信号必填', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
            return;
        }
        //发送申请代理请求
        var xhr = cc.mile.http.httpPost("/hall/gameAgent",params, this.success, this.error, this);
    },
    success: function (result, object) {
        var data = JSON.parse(result);
        if(data){
            //  设置显示文本与显示时间
            var toast = Toast.makeText('代理申请提交成功', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
            setTimeout(function(){object.closeOpenWin();},1000);
        }
    },
    error: function (object) {
        object.closeloadding(object.loaddingDialog);
        object.alert("网络异常，服务访问失败");
        object.loginFlag = false;
    },


    // update (dt) {},
});
