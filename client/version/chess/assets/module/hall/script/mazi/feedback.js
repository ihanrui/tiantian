var beiMiCommon = require("MileCommon");
// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: beiMiCommon,
    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        wexinnumber: {
            default: null,
            type: cc.EditBox
        },
        content: {
            default: null,
            type: cc.EditBox
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //this.title.string = "223233";
    },

    start: function () {

    },
    onClickSubmit: function () {
        //反馈内容
        var content = this.content.string;
        //微信号
        var wexinnumber = this.wexinnumber.string;
        if (wexinnumber == "") {
            this.showCenterToast('微信号不能为空');
            return;
        }
        if (content == "") {
            this.showCenterToast('反馈内容不能为空');

            return;
        }

        var params =
            {
                token: cc.mile.authorization,
                wx: wexinnumber,
                question: content
            };
        cc.mile.http.httpPost("/hall/gameSuggest", params, this.sucess, this.error, this);
    },
    sucess: function (result, object) {
        var data = JSON.parse(result);
        ///成功调用
        if (data) {
            var toast = Toast.makeText('反馈成功', Toast.LENGTH_SHORT);
            toast.setGravity(Toast.CENTER, 0, 0);
            toast.show();
            setTimeout(function(){object.closeOpenWin();},1000);
        }
    },
    error: function (object) {

        //object.closeloadding(object.loaddingDialog);
        object.showCenterToast("网络异常，服务访问失败");
        object.loginFlag = false;
    },
    // update (dt) {},
});
