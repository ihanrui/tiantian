/**
 *
 *
 * 茶楼大厅事件监听
 *
 */

var beiMiCommon = require("MileCommon");

cc.Class({
    extends: beiMiCommon,
    properties: {
        teahouseItemContainer: {
            default: null,
            type: cc.Node
        },
        teahouseItemPrefab: {
            default: null,
            type: cc.Prefab
        },
        marquee: {
            default: null,
            type: cc.Label
        }
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {
        this.initgame();

    },
    initgame: function () {

        cc.log("初始化游戏  this.ready() 【" + this.ready() + "】")
        if (this.ready()) {

            this.initSocketEvent();
            this.updateTeahouseData();
            this.inited = true;
        }


    },
    initSocketEvent: function () {
        var socket = this.socket();
        var self = this;
        socket.on("applyjoin", function (result) {
            debugger;
            var data = self.parse(result);
            cc.log("茶楼 context[" + JSON.stringify(result) + "]");
            if (data.tipstr != null && data.flag == "N") {
                self.showCenterToast(data.tipstr);
            }
            else if (data.teamem.passflag == "apply") {
                self.showCenterToast('申请成功 请等待管理员审核');
            }
            self.updateTeahouseData();
        });
        socket.on("command_teahouse", function (result) {
            debugger;
            if (self.inited == true) {

                var data = self.parse(result);

                cc.log("事件 data.command [" + data.command + "]  数据内容为:  " + JSON.stringify(result));
                //  self.route(data.command)(data, self);
            }
        });

        /**
         * 加入房卡模式的游戏类型 ， 需要校验是否是服务端发送的消息
         */
        socket.on("searchroom", function (result) {
            //result 是 GamePlayway数据，如果找到了 房间数据，则进入房间，如果未找到房间数据，则提示房间不存在
            if (result != null && cc.mile.room_callback != null) {
                cc.mile.room_callback(result, self);
            }
        });

        /**
         * 创建茶楼
         */
        socket.on("createteahouse", function (result) {
            var data = self.parse(result);
            cc.mile.teahouse = data.teahouse;
            self.scene("floor", self);
        });

        /**
         * 心跳检查，服务端发起的事件，服务端可以设置 PING的时长和 PING的 TimeOut
         */
        socket.on("ping", function () {

        });
    },
    updateTeahouseData: function () {

        cc.mile.http.httpPost("/teahouse/getmyteahouses",
            { gameid: cc.mile.user.gameid },
            this.successGetTeaHouse, this.errorGetTeaHouse, this);
    },
    successGetTeaHouse: function (result, object) {
        cc.log(result);
        var data = JSON.parse(result);
        object.createMyTeaHouseItem(data);
    },
    errorGetTeaHouse: function (object) {
        cc.log(object);
        object.showCenterToast("网络异常，服务访问失败");
    },
    getmyteahouse_event: function (data, context) {
        cc.log("-------getmyteahouse_event------")
    },
    getmeminfo_event: function (data, context) {
        cc.log("-------getmeminfo_event------")
        cc.log(data);

    },

    onDestroy: function () {
        if (cc.mile.socket && cc.mile.socket.off) {

            cc.mile.socket.off("searchroom");
            cc.mile.socket.off("applyjoin");
            cc.mile.socket.off("createteahouse");
        }
    },

    createMyTeaHouseItem: function (lst) {
        cc.log("-------createMyTeaHouseItem------")
        cc.log(lst);
        this.teahouseItemContainer.removeAllChildren();
        var height = 80;
        for (var i = 0; i < lst.length; i++) {
            var item = cc.instantiate(this.teahouseItemPrefab);
            var itemRender = item.getComponent("TeaHouseItem");
            itemRender.initItem(lst[i]);
            item.setPosition(0, -45 - height * i);
            item.parent = this.teahouseItemContainer;

        }
        this.teahouseItemContainer.height = lst.length * 80;
    },
    // 成员信息更新， 审核、删除、设置为店小二
    memupdate_event: function (data, context) {

        cc.log(data);
        cc.log(" context[" + JSON.stringify(data) + "]");
        if (data.teamem && data.teamem.passflag == 'out') {
            if (cc.mile.socket && cc.mile.socket.off) {
                cc.mile.socket.off("command_teahouse");
            }
            context.scene("mazihall", context);
        } else {
            cc.mile.teahouse.updatmemeber = data.teamem;
        }
        debugger
        //添加小二
        if (data.teamem.passflag == "pass" && data.teamem.role == "manager") {
            if (data.teamem.userid == cc.mile.user.id) {
                context.alert("你已被管理员添加为小二");
            } else {
                context.showCenterToast("你已成功添加该用户为小二");
            }
        }
    },
    start: function () {

    },

});
