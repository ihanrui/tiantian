// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        title_playway: {
            default: null,
            type: cc.Node
        },
        title_record: {
            default: null,
            type: cc.Node
        },
        content_playway: {
            default: null,
            type: cc.Node
        },
        content_record: {
            default: null,
            type: cc.Node
        },
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        itemTemplate: { // item template to instantiate other items
            default: null,
            type: cc.Prefab
        },
        spawnCount: 10, // how many items we actually spawn
        totalCount: 0, // how many items we need for the whole list
        spacing: 0, // space between each item
        bufferZone: 0, // when item is away from bufferZone, we relocate it

    },

    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {
        this.title_playway.active = true;
        this.content_playway.active = true;
        this.title_record.active = false;
        this.content_record.active = false;


        /**
         * 从远程加载数据，如果加载数据失败，则显示提示消息，并注册拖动刷新事件
         */
        this.content = this.scrollView.content;
        this.items = []; // array to store spawned items
        this.initialize();
        this.updateTimer = 0;
        this.updateInterval = 0.2;
        this.lastContentPosY = 0; // use this variable to detect if we are scrolling up or down
    },
    initialize: function () {

        this.playrecordpool = new cc.NodePool();
        this.recordarray = new Array();
        for(var i=0 ; i<20 ; i++){ //最大数量不能超过20
            this.playrecordpool.put(cc.instantiate(this.itemTemplate));
        }

        for(var inx =0 ; inx < 20 ; inx++){
            /**
             * 此处需要做判断，检查 对象池有足够的对象可以使用
             */
            var playrecord = this.playrecordpool.get();
            playrecord.parent = this.content ;
            this.recordarray.push(playrecord) ;
        }

        /*this.content.height = this.totalCount * (this.itemTemplate.height + this.spacing) + this.spacing; // get total content height

        for (var i = 0; i < this.spawnCount; ++i) { // spawn items, we only need to do this once
            var item = cc.instantiate(this.itemTemplate);
            this.content.addChild(item);
            item.setPosition(0, -item.height * (0.5 + i) - this.spacing * (i + 1));
            this.items.push(item);
        }*/
    },
    update: function (dt) {
        this.updateTimer += dt;
        if (this.updateTimer < this.updateInterval) return; // we don't need to do the math every frame
        this.updateTimer = 0;
        var items = this.items;
        var buffer = this.bufferZone;
        var isDown = this.scrollView.content.y < this.lastContentPosY; // scrolling direction
        var offset = (this.itemTemplate.height + this.spacing) * items.length;
        for (var i = 0; i < items.length; ++i) {
            var viewPos = this.getPositionInView(items[i]);
            if (isDown) {
                // if away from buffer zone and not reaching top of content
                if (viewPos.y < -buffer && items[i].y + offset < 0) {
                    items[i].setPositionY(items[i].y + offset);
                    var item = items[i].getChildByName('msgLabel');
                    var itemId = item.itemID - items.length; // update item id
                    console.log("update---down");
                }
            } else {
                // if away from buffer zone and not reaching bottom of content
                if (viewPos.y > buffer && items[i].y - offset > -this.content.height) {
                    items[i].setPositionY(items[i].y - offset);
                    var item = items[i].getChildByName('msgLabel');
                    var itemId = item.itemID + items.length;
                    console.log("update---down");
                }
            }
        }
        // update lastContentPosY
        this.lastContentPosY = this.scrollView.content.y;

    },
    getPositionInView: function (item) { // get item position in scrollview's node space
        var worldPos = item.parent.convertToWorldSpaceAR(item.position);
        var viewPos = this.scrollView.node.convertToNodeSpaceAR(worldPos);
        return viewPos;
    },
    onPlaywayBack: function () {
        this.title_playway.active = true;
        this.content_playway.active = true;
        this.title_record.active = false;
        this.content_record.active = false;
    },
    onToRecord: function () {
        console.log("onMsgReplay----");
        this.title_playway.active = false;
        this.content_playway.active = false;
        this.title_record.active = true;
        this.content_record.active = true;
    },
    onReply: function () {
        this.title_playway.active = true;
        this.content_playway.active = true;
        this.title_record.active = false;
        this.content_record.active = false;
    }


    // update (dt) {},
});
