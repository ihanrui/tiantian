// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        num:{
            default: null,
            type: cc.Label
        },
        amt:{
            default: null,
            type: cc.Label
        },
        numimg:{
            default: null,
            type: cc.Node
        },
        shop_desc_img: {
            default: null,
            type: cc.Node
        },
        shop_button_img: {
            default: null,
            type: cc.Node
        }
    },

    onLoad: function () {

    },
    initEvent: function () {
        var self = this;
        this.node.on('rechargeButtonMousedown', function (event) {

            if (self) {
                var customEvent = new cc.Event.EventCustom("rechargeButtonClick", true);
                customEvent.setUserData({id:self.itemdata.id});
                self.node.dispatchEvent(customEvent);
            }

        });
    },
    init: function (itemdata) {
        this.itemdata = itemdata;
        this.num.string = itemdata.num + itemdata.shopname;
        var cointype = "z";
        if (itemdata.cointype == 2) {
            cointype = "d";
        }
        var nummg = "";
        if (itemdata.num <= 1000) {
            nummg = 1;
        } else if (itemdata.num <= 5000) {
            nummg = 2;
        } else if (itemdata.num <= 15000) {
            nummg = 3;
        } else {
            nummg = 4;
        }
        this.numimg.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(cc.url.raw("resources/images/shop/HouseShop/shopitem/" + cointype + nummg + ".png"));
        this.amt.string = "￥" + itemdata.amt / 100;
        this.initEvent();
    },

    onRechargeBtnClick: function () {
        this.node.dispatchEvent(new cc.Event.EventCustom("rechargeButtonMousedown", true));
    }

    // update (dt) {},
});
