var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        setting: {
            default: null,
            type: cc.Prefab
        },
        message: {
            default: null,
            type: cc.Prefab
        },
        share: {
            default: null,
            type: cc.Prefab
        },
        playway: {
            default: null,
            type: cc.Prefab
        },
        feedback: {
            default: null,
            type: cc.Prefab
        },
        shop: {
            default: null,
            type: cc.Prefab
        },
        userinfo2: {
            default: null,
            type: cc.Prefab
        },
          userinfo: {
            default: null,
            type: cc.Prefab
        },
    },


    // use this for initialization
    onLoad: function () {

    },
    onSettingClick:function(){
        cc.mile.openwin = cc.instantiate(this.setting) ;
        cc.mile.openwin.parent = this.root();
    },
    onMessageClick:function(){
        cc.mile.openwin = cc.instantiate(this.message) ;
        cc.mile.openwin.parent = this.root();
    },
    onShareClick:function(){
        cc.mile.openwin = cc.instantiate(this.share) ;
        cc.mile.openwin.parent = this.root();
    },
    onPlaywayClick:function(){
        cc.mile.openwin = cc.instantiate(this.playway) ;
        cc.mile.openwin.parent = this.root();
    },
    onRecordClick:function(){
        cc.mile.openwin = cc.instantiate(this.playway) ;
        cc.mile.openwin.parent = this.root();
    },
    onFeedBackClick:function(){
        cc.mile.openwin = cc.instantiate(this.feedback) ;
        cc.mile.openwin.parent = this.root();
    },
    onShopBackClick:function(){
        cc.mile.openwin = cc.instantiate(this.shop) ;
        cc.mile.openwin.parent = this.root();
    },
    onUserInfoClick: function () {
        cc.mile.openwin = cc.instantiate(this.userinfo);
        cc.mile.openwin.parent = this.root();
    },
    onUserInfo2Click: function (event,data) {
        cc.mile.openwin = cc.instantiate(this.userinfo2);
        cc.mile.openwin.parent = this.root();
        this.chatAnimation =this.getCommon("ChatAnimation");
        // this.chatAnimation = new chatAnimation();
        this.chatAnimation.init(data);
    }
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
