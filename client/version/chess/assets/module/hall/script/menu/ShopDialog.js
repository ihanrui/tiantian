// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        z_shop_items: {
            default: null,
            type: cc.Node
        },
        d_shop_items: {
            default: null,
            type: cc.Node
        },
        z_shop_item_container: {
            default: null,
            type: cc.Node
        },
        d_shop_item_container: {
            default: null,
            type: cc.Node
        },
        shop_item_prefab: {
            default: null,
            type: cc.Prefab
        },
        tab: {
            default: null,
            type: cc.Node
        },
    },

    onLoad: function () {
        this.queryShopRecords();
    },
    init: function (cointtype) {
        this.z_shop_items.active = cointtype == "1";
        this.d_shop_items.active = cointtype == "2";
        this.tab.getComponent(cc.Sprite).spriteFrame =
            new cc.SpriteFrame(cc.url.raw("resources/images/shop/HouseShop/Btn_shop_Label" + cointtype + ".png"));
    },
    loadShopItems: function (datas) {
        debugger
        var self = this;
        var dlist = [];
        var zlist = [];
        for (var i = 0; i < datas.length; i++) {
            var data = datas[i];
            if (data.cointype == 1) {
                zlist.push(data);
            } else {
                dlist.push(data);
            }
        }

       this.z_shop_item_container.removeAllChildren();
        for (var i = 0; i < zlist.length; i++) {
            this.addShopItemView(this.z_shop_item_container,zlist[i], i, self);
        }
        this.z_shop_item_container.width = zlist.length * 266;
        this.z_shop_item_container.setPosition(this.z_shop_item_container.position.x + ((zlist.length - 4) * 133),
            this.z_shop_item_container.position.y);

       this.d_shop_item_container.removeAllChildren();
        for (var i = 0; i < dlist.length; i++) {

            this.addShopItemView(this.d_shop_item_container,dlist[i], i, self);
        }
        this.d_shop_item_container.width = dlist.length * 266;
        this.d_shop_item_container.setPosition(this.d_shop_item_container.position.x + ((dlist.length - 4) * 133),
            this.d_shop_item_container.position.y);

        // console.log("this.shop_item_container.width=" + this.shop_item_container.width);
    },
    queryShopRecords: function () {

        cc.mile.http.httpPost("/hall/getallshop",
            {
                token: cc.mile.authorization,
            },
            this.successGetItem, this.errorGetItem, this);
    },
    successGetItem: function (result, object) {

        cc.log(result);

        var data = object.parse(result);
        object.loadShopItems(data);
    },
    errorGetItem: function (result, object) {
        cc.log(result);
    },
    addShopItemView: function (container,itemdata, index, self) {
        var shopItem = cc.instantiate(this.shop_item_prefab);
        shopItem.setPosition(index * 266, -195);
        shopItem.on('rechargeButtonClick', function (event) {
            var userData = event.getUserData();

            if (self) {
                self.onShopClick(userData.id);
            }
        });
        shopItem.parent = container;
        var shopRender = shopItem.getComponent("ShopItemRender");
        shopRender.init(itemdata);
    },

    successTrade: function (result, object) {
        cc.log(result);

        object.closeloadding(object.loaddingDialog);
        var data = object.parse(result);
        if (data.status) {
            //var decodeXml = decodeURIComponent(data.data.prepayXml);
            object.prepareId = data.data.prepareId;
            object.appId = data.data.appId;
            object.mchId = data.data.mchId;
            object.apiKey = data.data.apiKey;

            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                    "startRecharge",
                    "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                    data.data.appId,
                    data.data.mchId,
                    data.data.prepareId,
                    data.data.nonceStr,
                    data.data.timeStamp,
                    data.data.paySign
                );
            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {


                jsb.reflection.callStaticMethod("AppController",
                    "startRecharge:prepayid:mchid:noncestr:timeStamp:sign:",
                    data.data.appId,
                    data.data.mchId,
                    data.data.prepareId,
                    data.data.nonceStr,
                    data.data.timeStamp,
                    data.data.paySign
                );
            }

        } else {
            object.alert(data.msg);
        }
    },
    errorTrade: function (object) {
        object.closeloadding(object.loaddingDialog);
        object.alert("网络异常，服务访问失败");

    },
    onShopClick: function (shopId) {
        var rechargeSource = "web";
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            rechargeSource = "android";
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            rechargeSource = "ios";
        }
        var xhr = cc.mile.http.httpPost("/payment/createtrade",
            {
                token: cc.mile.authorization,
                shopId: shopId,
                rechargeType: 'weixin_pay',
                rechargeSource: rechargeSource,
            },
            this.successTrade, this.errorTrade, this);
        this.loadding();

    },
    onChange:function (e,data) {
        this.init(data);
    },
    onCloseShopDialog:function () {
        debugger
        var param = {
            token: cc.mile.authorization,
            orgi: cc.mile.user.orgi
        };
        this.emitSocketRequest("updateuser", JSON.stringify(param));
        this.closeOpenWin();
    }
    // update (dt) {},
});
