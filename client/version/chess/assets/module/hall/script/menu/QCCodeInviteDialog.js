cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...

        qccodeContainer:{
            default: null,
            type: cc.Node
        },
        shareBtnNode:{
            default: null,
            type: cc.Node
        }
    },

    // use this for initialization
    onLoad: function () {

        var gameUtils = require("WangdaGameUtils");
        gameUtils.loadQCcodeJPGImage(cc.mile.user.userurl
            + "&gameid=" + cc.mile.user.gameid
            , this.qccodeContainer);

    },
    onWeixinShareClick:function(){
        this.shareBtnNode.active = false;
        var gameUtils = require("WangdaGameUtils");

        gameUtils.screenShoot(this.screenShootShare);
    },
    screenShootShare: function (filepath) {
        cc.log(filepath);
        var gameUtils = require("WangdaGameUtils");
        gameUtils.shareImage(filepath);

    },
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
