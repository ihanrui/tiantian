/**
 *
 *


 *
 *
 *
 */


var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        fenxiangTimeLine: {
            default: null,
            type: cc.Button
        },
        fenxiangSession: {
            default: null,
            type: cc.Button
        },
        inviteContainer: {
            default: null,
            type: cc.Node
        },
        changeTableContainer: {
            default: null,
            type: cc.Node
        },
        shareCode: "share_room"
    },


    // use this for initialization
    onLoad: function () {

        if(cc.mile.createroomtype == 'coin'){
            if(this.changeTableContainer){
                this.changeTableContainer.active = true;
            }

            if(this.inviteContainer){
                this.inviteContainer.active = false;
            }
        }else {
            if(this.inviteContainer){
                this.inviteContainer.active = true;
            }
            if(this.changeTableContainer){
                this.changeTableContainer.active = false;
            }
        }
    },
    initShareType: function (shareCode) {
        this.shareCode = shareCode;
    },
    //分享到聊天
    shareToWeixinChat: function () {
        this.shareType = 1;
        cc.log("分享到微信："+this.shareCode);
        if(this.shareCode == "share_room"){

            if(cc.mile.teahouse){
                cc.log("分享到微信shareflag："+cc.mile.teahouse.shareflag);
            }
            if(cc.mile.teahouse && cc.mile.teahouse.shareflag == 'N'){
                this.showCenterToast("茶楼房间已禁止分享链接，请【复制操作】");
                return;
            }
        }
        //this.loadding();
        //通过ID获取 玩家信息
        var params = {
            token: cc.mile.authorization,
            code: this.shareCode
        };
        cc.mile.http.httpPost("/hall/getJsonstr", params,
            this.getJsonStrSuccess, this.getJsonStrFailure, this);

    },
    //分享到朋友圈
    shareToWeixinTimeLine: function () {

        this.shareType = 2;

        cc.log("分享到微信："+this.shareCode);
        if(this.shareCode == "share_room"){

            if(cc.mile.teahouse){
                cc.log("分享到微信shareflag："+cc.mile.teahouse.shareflag);
            }
            if(cc.mile.teahouse && cc.mile.teahouse.shareflag == 'N'){
                this.showCenterToast("茶楼房间已禁止分享链接，请【复制操作】");
                return;
            }
        }

        //this.loadding();
        //通过ID获取 玩家信息
        var params = {
            token: cc.mile.authorization,
            code: this.shareCode
        };
        cc.mile.http.httpPost("/hall/getJsonstr", params,
            this.getJsonStrSuccess, this.getJsonStrFailure, this);

    },
    callShareNativeMethod: function (config) {
        cc.info("shareToWeixinSession start" + config);
        cc.info("caption start" + this.getReplaceString(config.caption));
        cc.info("description start" + this.getReplaceString(config.description));
        cc.info("share_link start" + this.getReplaceString(config.share_link));

        var caption = this.getReplaceString(config.caption);
        var description = this.getReplaceString(config.description);
        var share_link = this.getReplaceString(config.share_link);

        if(this.copyFlag){

            this.copyFlag = false;
            var gameUtils = require("WangdaGameUtils");
            gameUtils.copyToClipboard(caption+"\n"+description);
            this.showCenterToast("复制成功");

        }else {

            if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {

                if (this.shareType == 1) {

                    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                        "shareToWeixinSession", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                        caption, description, share_link, config.appid);
                } else {

                    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity",
                        "shareToWeixinTimeLine", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
                        caption, description, share_link, config.appid);
                }

            } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {

                if (this.shareType == 1) {
                    jsb.reflection.callStaticMethod("AppController",
                        "shareToWeixinSession:description:shareLink:picture:", caption, description, share_link, config.appid);

                } else {

                    jsb.reflection.callStaticMethod("AppController",
                        "shareToWeixinTimeLine:description:shareLink:picture:", caption, description, share_link, config.appid);
                }

            }
        }

    },
    getReplaceString: function (source) {
        if (this.shareCode == 'share_floor') {
            if (cc.mile.teahouse) {
                source = source.replace("$teanum", cc.mile.teahouse.teanum);
            }

        } else if (this.shareCode == 'share_room') {
            if (cc.mile.roominfo) {
                source = source.replace("$roomdesc", cc.mile.roominfo.roomdesc);
                source = source.replace("$roomid", cc.mile.roominfo.roomid);
            }
            if (cc.mile.teahouse) {
                source = source.replace("$teanum", cc.mile.teahouse.teanum);
            }
            if (cc.mile.tablenum > 0) {
                source = source.replace("$tableindex", cc.mile.tablenum);
            }
            if (cc.mile.maxplayers && cc.mile.maxplayers > 0 && cc.mile.curpalyers && cc.mile.curpalyers > 0) {

                source = source.replace("$playNums", "[" + cc.mile.curpalyers + "缺" + (cc.mile.maxplayers - cc.mile.curpalyers) + "]");

            }

        }
        if (cc.mile.createroomtype == "card") {
            source = source.replace("$gameType", "经典场");
        } else  {
            source = source.replace("$gameType", "积分场");
        }
        source = source.replace("$gameid", cc.mile.user.gameid);
        //把所有沒有替换的值设置
        source = source.replace("$username", cc.mile.user.username);
        source = source.replace("$playNums", "");
        source = source.replace("$teanum", "");
        source = source.replace("$roomdesc", "");
        source = source.replace("$roomid", "");
        source = source.replace("$tableindex", "");

        return source;
    },

    onCopyInvitePlayerStr: function () {
        if (cc.mile.roominfo) {
            this.copyFlag = true;
            this.shareToWeixinChat();


        }
    },

    onChangeTableClick: function () {

        if(cc.mile.joinroomFinish){

            var teanum = "";
            var param = {
                token: cc.mile.authorization,
                playway: cc.mile.extparams.playway,
                orgi: cc.mile.user.orgi,
                changeroom: '1',
                extparams: cc.mile.extparams,
                roomtype: cc.mile.createroomtype,
                teanum: teanum,
            };
            this.emitSocketRequest("joinroom", JSON.stringify(param));
        }else {
            this.showCenterToast("老板换桌太频繁，请先喝口茶歇会！")
        }
    },

    getJsonStrSuccess: function (result, object) {
        var data = object.parse(result);
        if (data != null) {
            object.callShareNativeMethod(data);
        }
    },
    getJsonStrFailure: function (result, object) {
        result.showCenterToast("服务器信息获取失败，请检测网络链接");
        return;
    }
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
