var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        tag: {
            default: null,
            type: cc.Node
        },
        score: {
            default: null,
            type: cc.Label
        },
        onlineusers: {
            default: null,
            type: cc.Label
        },
        scorelimit: {
            default: null,
            type: cc.Label,

        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        itemBg: {
            default: null,
            type: cc.Node
        },
        mincoins: 0,
        maxcoins: 0

    },

    // use this for initialization
    onLoad: function () {

    },
    init: function (playway) {
        /**
         * 需要预先请求 在线人数
         */
        if (playway) {
            var frameName = "初级";
            if (playway.level == '2') {
                frameName = "高级"
            }
            this.data = playway;

            if (playway.shuffle == false) {
                this.tag.active = false;
            } else {
                this.tag.active = true;
            }

            frameName = frameName + playway.skin;

            if(this.itemBg){
                this.itemBg.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(frameName);
                this.itemBg.on('click', this.onClick, this);
            }

            this.onlineusers.string = playway.onlineusers + " 人 ";
            var min = this.getCoinDesc(playway.mincoins);

            var max = this.getCoinDesc(playway.maxcoins);

            this.scorelimit.string = min + "-" + max;

            this.score.string = playway.score;

            this.mincoins = playway.mincoins;
            this.maxcoins = playway.maxcoins;

            this.getCoinPlaywayNumber(this.data.id);


        }
    },
    getCoinPlaywayNumber: function (playwayid) {

        cc.mile.http.httpPost("/hall/getusernum",
            {
                token: cc.mile.authorization,
                playway: playwayid
            },
            this.successGetHistory, this.errorGetHistory, this);
    },
    successGetHistory: function (result, object) {

        object.onlineusers.string = result + " 人 ";
    },
    errorGetHistory: function (result, object) {
        result.closeloadding();
        result.showCenterToast("数据请求出错，请重试");
    },

    getCoinDesc: function (coins) {
        if (coins > 10000) {
            return parseInt(coins / 10000) + "万";
        } else if (coins > 1000) {

            return parseInt(coins / 1000) + "千";
        }
        return coins;
    },
    onClick: function () {

        cc.log(this.mincoins);


        if(this.mincoins > cc.mile.user.goldcoins){//余额小于最小金额， 不能进入
              this.showCenterToast("金币不够,请选择合适的场次");
              return;
        }
        this.getCoinPlayway();


    },

    getPlaywayCallback: function (playway) {

        var extparams = playway;
        extparams.playway = this.data.id;
        extparams.gametype = this.data.code;
        extparams.scoreunit = this.data.score;
        cc.mile.createroomtype = 'coin';

        cc.log("onClick----:" + JSON.stringify(extparams));

        this.closeOpenWin();
        this.preload(extparams);
    },

    getCoinPlayway: function () {

        //通过ID获取 玩家信息
        var params = {
            token: cc.mile.authorization,
            code: "coin_playway"
        };
        cc.mile.http.httpPost("/hall/getJsonstr", params,
            this.getJsonStrSuccess, this.getJsonStrFailure, this);

    },
    getJsonStrSuccess: function (result, object) {
        var data = object.parse(result);
        if (data != null) {
            object.getPlaywayCallback(data);
        }
    },
    getJsonStrFailure: function (result, object) {
        result.showCenterToast("服务器信息获取失败，请检测网络链接");
        return;
    },


    createRoom: function (event, data) {

        this.loadding();
        this.scheduleOnce(function () {
            this.scene(data, this);
        }, 200);

    }



    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
