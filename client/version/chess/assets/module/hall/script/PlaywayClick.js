var beiMiCommon = require("MileCommon");
cc.Class({
        extends: beiMiCommon,

        properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ..
        playway:{
            default: null,
            type: cc.Node
        },
    },

    // use this for initialization
    onLoad: function () {

    },
    onClick:function(){


        var self = this ;

        var selectPlayway = this.getCommon("SelectPlayway");

        var thisplayway = this.playway.getComponent("Playway");

        var extparams = {
            gametype : thisplayway.data.code ,
            playway  : thisplayway.data.id
        } ;
        this.closeOpenWin();
        this.preload(extparams , self);
    },
    createRoom:function(event,data){

        this.loadding();
        this.scheduleOnce(function () {
            this.scene(data, this) ;
        },200);
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
