// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        byteProgress: {
            default: null,
            type: cc.ProgressBar
        },
        byteProgressBar: {
            default: null,
            type: cc.Node
        },
        byteProgressNumNode: {
            default: null,
            type: cc.Node
        },
        byteProgressNumLabel: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

     onLoad:function () {
         this.byteProgress.progress = 0;
         var self = this;
         if (self.runtimer) {
             self.unscheduleAllCallbacks();
         }
         var filecount = 10*100;
         var cw = 0;
         var num=0;
         self.runtimer = function () {
             num += (1 / filecount) * 100;
             this.byteProgress.progress += (1 / filecount);
             var s = this.byteProgressBar.width - cw;
             cw = this.byteProgressBar.width;
             this.byteProgressNumNode.setPosition(this.byteProgressNumNode.position.x + s, this.byteProgressNumNode.position.y);
             this.byteProgressNumLabel.string = parseInt(num) + "%";
             if (s == 0) {
                 self.unscheduleAllCallbacks();
             }
         }
         self.schedule(self.runtimer, 0.05);
     }  ,


    // update (dt) {},
});
