var beiMiCommon = require("MileCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        username: {
            default: null,
            type: cc.Label
        },
        user_head_Sprite: {
            default: null,
            type: cc.Sprite
        },
        user_head_node: {
            default: null,
            type: cc.Node
        },
        userid: {
            default: null,
            type: cc.Label
        },
        goldcoins: {
            default: null,
            type: cc.Label
        },
        cards: {
            default: null,
            type: cc.Label
        }
        ,
        girl: {
            default: null,
            type: cc.Node
        },
        marquee: {
            default: null,
            type: cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {
        var self = this;
        if (this.ready()) {
            cc.log(cc.mile.user)
            this.loadUrlJPGImage(cc.mile.user.userurl
                + "&username=" + cc.mile.user.id, this.user_head_node);
            this.username.string = cc.mile.user.nickname ? cc.mile.user.nickname : cc.mile.user.username;
            this.userid.string = "ID:" + cc.mile.user.gameid;
            this.pva_format(cc.mile.user.goldcoins, cc.mile.user.cards, cc.mile.user.diamonds, self);
            this.pvalistener(self, function (context) {
                context.pva_format(cc.mile.user.goldcoins, cc.mile.user.cards, cc.mile.user.diamonds, context);
            });
            this.init();
        }
        //进入后台
        if (cc && cc.eventManager) {

            cc.eventManager.addCustomListener("updateUserInfo", function (event) {
                cc.log('-----updateUserInfo-----后台更新了--!');
                if (self) {
                    self.pva_format(cc.mile.user.goldcoins, cc.mile.user.cards, cc.mile.user.diamonds, self);
                }
            });
        }

    },
    pva_format: function (coins, cards, diamonds, object) {
        if (coins > 9999) {
            var num = coins / 10000;
            object.goldcoins.string = num.toFixed(2) + '万';
        } else {
            object.goldcoins.string = coins;
        }
        object.cards.string = diamonds;
    },
    playToLeft: function () {
        this._girlAnimCtrl = this.girl.getComponent(cc.Animation);
        this._girlAnimCtrl.play("girl_to_left");
    },
    playToRight: function () {
        this._girlAnimCtrl = this.girl.getComponent(cc.Animation);
        this._girlAnimCtrl.play("girl_to_right");
    },
    onDestroy: function () {
        this.cleanpvalistener();
        //进入后台
        if (cc && cc.eventManager) {
            cc.eventManager.removeCustomListeners("updateUserInfo");
        }
    },
    init: function () {//跑马灯接口调用
        this.io = require("IOUtils")
        var data = JSON.parse(this.io.get("userinfo"));
        if (data.token != null) {

            var params = {};

            var xhr = cc.mile.http.httpPost("/hall/getMarquee", params, this.sucessMarquee, this.errorMarquee, this);
        }

    },
    //滚动对象，滚动宽度,滚动完的速度
    marqueeRun: function (text,width,speedtime) {
        //初始位置
        text.node.x=width/2+(text.node.width / 2);
        //移动到的位置
        var moveleft = -width/ 2 - (text.node.width / 2);
        var time = this.getMarqueeTime();
        text.node.runAction(
            cc.repeatForever(
                cc.sequence(cc.moveTo(speedtime, cc.p(moveleft, text.node.y)),
                    cc.callFunc(function () {
                        text.node.x = width;
                        //初始位置=左边+滚动文字宽度除以2就是文字的中点
                        text.node.x =width/2+width/2;
                    })
                )
            ), time);
    },
    getMarqueeTime: function () {
        if(this.marquee.string){
            this.marquee.string.length / 25;
        }
        return 1;
    },
    sucessMarquee: function (result, object) {
        var data = result.toString();
        if (data) {
            object.marquee.string = data;
            if(object.marquee.string!=""&&object.marquee.string!=undefined)
            {
                debugger;
                object.marqueeRun(object.marquee,1280,23);
            }
        }
    },
    errorMarquee: function (object) {
        object.closeloadding(object.loaddingDialog);
        object.alert("网络异常，服务访问失败");
        object.loginFlag = false;
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
