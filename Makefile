SHELL := /bin/bash

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  start              to start service "
	@echo "  stop              to stop service "




start:
	@echo "start server"
	mvn spring-boot:run -X -Drun.jvmArguments='-Dserver.port=8080' -Djava.rmi.server.hostname='127.0.0.1'
	@echo "ok"


stop:
	@echo "stop server"
	 mvn spring-boot:stop
	@echo "ok"

start_background
    @echo "start server"
    nohup mvn spring-boot:run -X -Drun.jvmArguments='-Dserver.port=8080' &
    @echo "output to nohup ok"

