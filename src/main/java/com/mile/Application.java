package com.mile;

import com.mile.core.BMDataContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableAutoConfiguration
@SpringBootApplication
@EnableAsync
@EnableJpaRepositories({"com.mile.web.service.repository.jpa", "com.mile.core.teahouse.jpa",
        "com.mile.core.record.jpa", "com.mile.core.agent.jpa", "com.mile.core.hall.jpa", "com.mile.core.game.jpa"})
public class Application {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(Application.class) ;
		BMDataContext.setApplicationContext(springApplication.run(args));
	}

}
