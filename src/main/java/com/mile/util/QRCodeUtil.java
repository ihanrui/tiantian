package com.mile.util;

import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Hashtable;

/**
 * 二维码生成工具类
 *
 * @author Cloud
 * @data 2016-12-15
 * QRCode
 */

public class QRCodeUtil {

    private static final String CHARSET = "utf-8";
    private static final String FORMAT_NAME = "JPG";
    // 二维码尺寸
    private static final int QRCODE_SIZE = 300;
    // LOGO宽度
    private static final int WIDTH = 60;
    // LOGO高度
    private static final int HEIGHT = 60;


    /**
     * @param content
     * @param needCompress
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static BufferedImage createImage(String content, boolean needCompress) throws Exception {
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }

        return image;
    }

    /**
     * @param imgPath
     * @param isUrl
     * @return
     * @throws IOException
     */
    private static Image getInsertImage(String imgPath, boolean isUrl) throws IOException {
        if (isUrl) {
            return ImageIO.read(new URL(imgPath));
        } else {

            File file = new File(imgPath);
            if (false && !file.exists()) {
                System.err.println("" + imgPath + "   该文件不存在！");
                return null;
            }

            return ImageIO.read(new File(imgPath));
        }
    }

    private static void insertImage(BufferedImage source, Image src,
                                    boolean needCompress) {

        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (needCompress) { // 压缩LOGO
            if (width > WIDTH) {
                width = WIDTH;
            }
            if (height > HEIGHT) {
                height = HEIGHT;
            }
            Image image = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QRCODE_SIZE - width) / 2;
        int y = (QRCODE_SIZE - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }


    /**
     * @param content      二维码路径
     * @param imgPath      需要插入图片的路径
     * @param destPath     目标路径
     * @param needCompress 是否需要压缩
     * @param file         生成目标文件
     * @throws Exception
     */
    public static void encode(String content, String imgPath, String destPath, boolean needCompress, String file) throws Exception {
        BufferedImage image = QRCodeUtil.createImage(content, needCompress);
        if (imgPath != null && !"".equals(imgPath)) {
            Image src = getInsertImage(imgPath, false);
            QRCodeUtil.insertImage(image, src, needCompress);
        }
        // 插入图片
        mkdirs(destPath);
        //String file = new Random().nextInt(99999999)+".jpg";
        ImageIO.write(image, FORMAT_NAME, new File(destPath + "/" + file));
    }


    /**
     * @param content      二维码路径
     * @param urlPath      需要插入图片的路径
     * @param destPath     目标路径
     * @param needCompress 是否需要压缩
     * @param file         生成目标文件
     * @throws Exception
     */
    public static void encodeUrlInsert(String content, String urlPath, String destPath, boolean needCompress, String file) throws Exception {
        BufferedImage image = encodeUrlInsertImage(content,urlPath,needCompress);
        // 插入图片
        mkdirs(destPath);
        //String file = new Random().nextInt(99999999)+".jpg";
        ImageIO.write(image, FORMAT_NAME, new File(destPath + "/" + file));
    }

    /**
     * @param content      二维码路径
     * @param urlPath      需要插入图片的路径
     * @param needCompress 是否需要压缩
     * @throws Exception
     */
    public static BufferedImage encodeUrlInsertImage(String content, String urlPath, boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtil.createImage(content, needCompress);
        if (urlPath != null && !"".equals(urlPath)) {
            Image src = getInsertImage(urlPath, true);
            QRCodeUtil.insertImage(image, src, needCompress);
        }
        return image;
    }


    public static void mkdirs(String destPath) {
        File file = new File(destPath);
        //当文件夹不存在时，mkdirs会自动创建多层目录，区别于mkdir．(mkdir如果父目录不存在则会抛出异常)
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
    }


    public static void encode(String content, String imgPath, String destPath)
            throws Exception {
        QRCodeUtil.encode(content, imgPath, destPath, false, null);
    }


    public static void encode(String content, String destPath,
                              boolean needCompress) throws Exception {
        QRCodeUtil.encode(content, null, destPath, needCompress, null);
    }


    public static void encode(String content, String destPath) throws Exception {
        QRCodeUtil.encode(content, null, destPath, false, null);
    }


    public static void encode(String content, String imgPath,
                              OutputStream output, boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtil.createImage(content, needCompress);
        if (imgPath != null || !"".equals(imgPath)) {

            Image src = getInsertImage(imgPath, false);
            QRCodeUtil.insertImage(image, src, needCompress);
        }
        // 插入图片
        ImageIO.write(image, FORMAT_NAME, output);
    }


    public static void encode(String content, OutputStream output)
            throws Exception {
        QRCodeUtil.encode(content, null, output, false);
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    public static String decode(File file) throws Exception {
        BufferedImage image;
        image = ImageIO.read(file);
        if (image == null) {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result result;
        Hashtable hints = new Hashtable();
        hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        result = new MultiFormatReader().decode(bitmap, hints);
        String resultStr = result.getText();
        return resultStr;
    }


    public static String decode(String path) throws Exception {
        return QRCodeUtil.decode(new File(path));
    }


    public static void main(String[] args) throws Exception {
        String text = "http://www.baidu.com";
        /*QRCodeUtil.encode(text, "/Users/linkli/Docs/tmp/logo.png",
                "/Users/linkli/Docs/tmp/",
                false, "wxgame2.jpg");*/

        QRCodeUtil.encodeUrlInsert(text,
                "http://thirdwx.qlogo.cn/mmopen/vi_32/twibiaV0K4L2eEaGvSlhgpnjYUwEZcAv220dSic5cTK9bHoMJiambJt67ThQqU0Wssd3UVM2YaiaicxyekGTM4Wm12Nw/132",
                "/Users/linkli/Docs/tmp/",
                true, "wxgame33.jpg");
    }

}