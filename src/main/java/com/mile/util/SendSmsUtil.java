package com.mile.util;
//接口类型：互亿无线触发短信接口，支持发送验证码短信、订单通知短信等。
// 账户注册：请通过该地址开通账户http://sms.ihuyi.com/register.html
// 注意事项：
//（1）调试期间，请用默认的模板进行测试，默认模板详见接口文档；
//（2）请使用APIID（查看APIID请登录用户中心->验证码短信->产品总览->APIID）及 APIkey来调用接口；
//（3）该代码仅供接入互亿无线短信接口参考使用，客户可根据实际需要自行编写；


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SendSmsUtil {

    private static final Logger logger = Logger.getLogger(SendSmsUtil.class);

    private static String SMS_URL = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";

    public static JSONObject send(String phoneNumber) throws Exception {

        int mobile_code = (int) ((Math.random() * 9 + 1) * 100000);
        String content = new String("您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。");
        JSONObject result = send(phoneNumber, content);
        JSONUtil.putJsonData(result, "mobile_code", mobile_code);
        return result;
    }


    public static JSONObject send(String phoneNumber, String content) throws Exception {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(SMS_URL);

        client.getParams().setContentCharset("GBK");
        method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=GBK");


        String account = "";//ResourceUtil.getConfigByName("smsAPIID");
        String password = "";//ResourceUtil.getConfigByName("smsAPIKEY");

        NameValuePair[] data = {//提交短信
                new NameValuePair("account", account), //查看用户名是登录用户中心->验证码短信->产品总览->APIID
                new NameValuePair("password", password),  //查看密码请登录用户中心->验证码短信->产品总览->APIKEY
                new NameValuePair("mobile", phoneNumber),
                new NameValuePair("content", content),
        };
        method.setRequestBody(data);

        try {
            client.executeMethod(method);

            String SubmitResult = method.getResponseBodyAsString();

            Document doc = DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();

            String code = root.elementText("code");
            String msg = root.elementText("msg");
            String smsid = root.elementText("smsid");

            System.out.println(code);
            System.out.println(msg);
            System.out.println(smsid);

            JSONObject result = new JSONObject();
            result.put("code", code);
            result.put("msg", msg);

            result.put("smsid", smsid);

            return result;
        } catch (HttpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("发送短信失败", e);
            throw new Exception("发送短信失败", e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("发送短信失败", e);
            throw new Exception("发送短信失败", e);
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("发送短信失败", e);
            throw new Exception("发送短信失败", e);
        } catch (JSONException e) {
            e.printStackTrace();
            logger.error("发送短信失败", e);
            throw new Exception("发送短信失败", e);
        }

    }

    public static void main(String[] args) {

        try {
            send("12121");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}