package com.mile.util;

import com.mile.web.model.BeiMiDic;
import com.mile.web.model.SysDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class DicUtil {

    private static final Logger logger = LoggerFactory.getLogger(DicUtil.class);

    public static final String BEIMI_SYSTEM_KEY_CONST = "com.system.key";





    public static String getSystemKeyDicNameByName(String name) {
        return getDicChildNameByName(BEIMI_SYSTEM_KEY_CONST,name);
    }


    public static String getDicChildNameByName(String parentKey,String name) {
        try {
            List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(parentKey);
            if (null == sysDicList)
                return null;

            for (SysDic sysDic : sysDicList) {
                if (name.equals(sysDic.getName()))
                    return sysDic.getCode();
            }

            logger.info("getDicChildNameByName未找到游戏常量[" + name + "]");
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getDicChildNameByName 获取游戏常量[" + name + "]失败", e);
            return null;
        }
    }
}
