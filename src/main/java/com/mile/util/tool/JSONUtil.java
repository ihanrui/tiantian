package com.mile.util.tool;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.apache.commons.lang.WordUtils.capitalize;

public class JSONUtil {

    private static Map store = new HashMap();
    private static String defaultId = "default_id";

    private static Map<String, JSONObject> buzConfig = new HashMap<>();

    /***
     * Note: only can get once time.
     *
     * @param id
     * @return
     */
    public static Object retriveObj(String id) {
        Object v = store.get(id);
        store.remove(id);
        return v;
    }

    public static Object retriveObj() {
        Object v = store.get(defaultId);
        store.remove(defaultId);
        return v;
    }

    public static void storeObj(String id, Object obj) {
        store.put(id, obj);
    }

    public static void storeObj(Object obj) {
        store.remove(defaultId);
        store.put(defaultId, obj);
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Iterator<String> keysItr = object.keys();
            while (keysItr.hasNext()) {
                String key = keysItr.next();
                Object value = object.get(key);

                if (value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                } else if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }

                if (value != null) {
                    map.put(key, value);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) {
        List<Object> list = new ArrayList<Object>();
        try {
            for (int i = 0, cnt = array.length(); i < cnt; i++) {
                Object value = array.get(i);
                if (value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                } else if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
                list.add(value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static String toString(JSONObject object, String key) {
        String rst = "";
        if (object == null || object.isNull(key)) return rst;
        try {
            rst = object.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rst;
    }

    public static Double toDouble(JSONArray labelDatas, int i) {
        if (labelDatas == null || labelDatas.length() >= i) return 0.0;
        double rst = 0;
        try {
            rst = labelDatas.getDouble(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rst;
    }


    public static Object toObject(JSONArray labelDatas, int i) {
        if (labelDatas == null || labelDatas.length() <= i) return null;
        Object rst = null;
        try {
            rst = labelDatas.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rst;
    }

    public static Object toObject(JSONObject labelDatas, String key) {
        if (labelDatas == null || labelDatas.isNull(key)) return null;
        Object rst = null;
        try {
            rst = labelDatas.get(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rst;
    }

    public static boolean toBoolean(JSONObject object, String key) {
        if (object == null || object.isNull(key)) return false;
        boolean rst = false;
        try {
            rst = object.getBoolean(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rst;
    }

    public static int toInt(JSONObject object, String key) {
        int i = -1;
        try {
            i = object.getInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return i;
    }

    public static double toDouble(JSONObject object, String key) {
        double i = -1;
        try {
            i = object.getDouble(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return i;
    }

    public static List toList(JSONObject obj, String key) {
        if (obj == null || obj.isNull(key)) return null;
        ArrayList llist = new ArrayList();
        try {
            JSONArray array = obj.getJSONArray(key);
            for (int i = 0, c = array.length(); i < c; i++) {
                llist.add(array.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return llist;
    }

    public static JSONArray toJSONArray(JSONObject obj, String key) {
        JSONArray res = null;
        if (obj == null || obj.isNull(key)) return null;
        try {
            res = obj.getJSONArray(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static Object toJSONObject(JSONObject obj, int index) {
        JSONArray res = null;
        if (obj == null || obj.length() < index) return null;
        Iterator<String> it = obj.keys();
        int i = 0;
        while (it.hasNext()) {
            if (index == i) {
                String key = it.next();
                return JSONUtil.toObject(obj, key);
            }
            i++;
        }
        return res;
    }

    public static String toJSONkey(JSONObject obj, int index) {
        String key = null;
        if (obj == null || obj.length() < index) return null;
        Iterator<String> it = obj.keys();
        int i = 0;
        while (it.hasNext()) {
            if (index == i) {
                key = it.next();
            } else {
                it.next();
            }
            i++;
        }
        return key;
    }


    public static JSONArray toJSONArray(JSONArray obj, int i) {
        JSONArray res = null;
        if (obj.length() > i) {
            try {
                res = obj.getJSONArray(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public static JSONObject toJSONObject(String str) {
        JSONObject obj = null;
        try {
            if (str != null)
                obj = new JSONObject(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }


    public static JSONObject toJSONObject(JSONObject jsonArray, String key) {
        JSONObject res = null;
        if (jsonArray == null || jsonArray.isNull(key)) {
            return null;
        }
        try {
            res = jsonArray.getJSONObject(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static int toJSONInt(JSONArray jsonArray, int index) {
        int obj = 0;
        if (jsonArray == null || jsonArray.length() <= index) {
            return obj;
        }
        try {
            obj = jsonArray.getInt(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static void putJsonData(JSONObject jsonObject, String key, Object value) {
        if (jsonObject != null) {
            try {
                jsonObject.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void putJsonData(JSONArray jsonObject, int index, Object value) {
        if (jsonObject != null) {
            try {
                jsonObject.put(index, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static String toJSONString(JSONArray jsonArray, int index) {
        String obj = "";
        if (jsonArray == null || jsonArray.length() <= index) {
            return obj;
        }
        try {
            obj = jsonArray.getString(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static JSONObject toJSONObject(JSONArray jsonArray, int index) {
        JSONObject obj = null;
        if (jsonArray == null) return null;
        try {
            obj = jsonArray.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static List sortJSONArray(JSONArray array) {
        LinkedList llist = new LinkedList();
        try {
            for (int i = 0, c = array.length(); i < c; i++) {
                llist.add(array.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Collections.sort((List<String>) llist, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareToIgnoreCase(s1);
            }
        });
        return llist;
    }

    public static void sortJSONArray(List array, final String key) {

        Collections.sort((List<JSONObject>) array, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                String v1 = JSONUtil.toString(o1, key);
                String v2 = JSONUtil.toString(o2, key);
                if (v2 == null) {
                    v2 = "";
                }
                if (v1 == null) {
                    v1 = "";
                }
                return v2.compareToIgnoreCase(v1);
            }
        });
    }

    public static void sortJSONArrayAscending(List array, final String key) {

        Collections.sort((List<JSONObject>) array, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                String v1 = JSONUtil.toString(o1, key);
                String v2 = JSONUtil.toString(o2, key);
                return v1.compareToIgnoreCase(v2);
            }
        });
    }

    public static JSONObject convertJSONObject(String string) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(string);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static Object objectJSONValue(Object o) throws Exception {

        if (o instanceof List) {
            List lst = (List) o;
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < lst.size(); i++) {
                JSONUtil.putJsonData(jsonArray, i, objectJSONValue(lst.get(i)));
            }
            return jsonArray;
        } else if (o instanceof Integer
                || o instanceof Double
                || o instanceof Boolean
                || o instanceof Long
                || o instanceof Float
                || o instanceof String) {
            return o;
        } else if (o instanceof Map) {
            return getMapToJson((Map) o);
        } else {
            return String.valueOf(o);
        }
    }

    public static JSONObject getMapToJson(Map o) throws Exception {
        JSONObject json = new JSONObject();
        try {
            Iterator iterator = o.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                String key = (String) entry.getKey();
                Object bodyObj = o.get(key);
                json.put(key, objectJSONValue(bodyObj));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            throw new Exception("ObjectToJson", e);
        }

        return json;
    }

    public static JSONArray getListMapToJson(List<Map<String, Object>> lst) throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (Map item : lst) {
            jsonArray.put(getMapToJson(item));
        }
        return jsonArray;
    }


    @SuppressWarnings("PARAMETER")
    public static void invokeObject2Map(
            Class sourceClass, Object source, Map retParametersMap) throws Exception {
        Field[] declaredField = sourceClass.getDeclaredFields();
        for (Field field : declaredField) {
            String methodName = "get" + capitalize(field.getName());
            String fieldName = field.getName();
            Method[] methods = sourceClass.getDeclaredMethods();
            for (Method m : methods) {
                if (m.getName().equalsIgnoreCase(methodName)) {
                    setParameterToMap(m, source, retParametersMap, fieldName);
                }
            }
        }
    }

    private static void setParameterToMap(
            Method m, Object source, Map retParametersMap, String paramKey) throws Exception {
        Object[] invokeParams = null;
        Object objValue = null;
        try {
            objValue = m.invoke(source, invokeParams);
            if (objValue != null) {
                retParametersMap.put(paramKey, objValue);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new Exception("IllegalAccessException", e);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new Exception("InvocationTargetException", e);
        }
    }


    public static String getResponseInfo(boolean flag, Object count, JSONArray content, String msg) {
        JSONObject object = new JSONObject();
        JSONUtil.putJsonData(object, "success", flag);
        JSONUtil.putJsonData(object, "msg", msg);
        JSONUtil.putJsonData(object, "count", count);
        JSONUtil.putJsonData(object, "content", content);
        return object.toString();
    }

    public static String getStatusResponseInfo(boolean flag, JSONObject statusInfo,
                                               JSONObject teamInfo, String msg) {
        JSONObject object = new JSONObject();
        JSONUtil.putJsonData(object, "success", flag);
        JSONUtil.putJsonData(object, "msg", msg);
        JSONUtil.putJsonData(object, "statusInfo", statusInfo);
        JSONUtil.putJsonData(object, "teamInfo", teamInfo);
        return object.toString();
    }
}
