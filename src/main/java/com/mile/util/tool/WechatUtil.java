package com.mile.util.tool;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.weixin4j.Weixin;

import java.io.IOException;


public class WechatUtil {

    private static final Logger logger = Logger.getLogger(WechatUtil.class);

    private static String OAUTH2_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
    private static String USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

    private static Weixin weixinUtil;

    public static JSONObject getAccessToken(String code, String appId, String secret) throws Exception {
        HttpClient client = new HttpClient();
        String url = String.format(OAUTH2_URL, appId, secret, code);
        //GetMethod method = new GetMethod(url);
        logger.info(url);
        HttpMethod method = new PostMethod(url);
        try {
            client.executeMethod(method);

            String ubmitResult = new String(method.getResponseBody(), "utf-8");
            logger.info(ubmitResult);
            JSONObject tokenResult = JSONUtil.convertJSONObject(ubmitResult);

            return tokenResult;
        } catch (HttpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("获取用户信息出错", e);
            throw new Exception("获取用户信息出错", e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("获取用户信息出错", e);
            throw new Exception("获取用户信息出错", e);
        }

    }

    public static JSONObject getWeUserInfo(String accessToken, String userOpenid) throws Exception {
        HttpClient client = new HttpClient();
        String url = String.format(USERINFO_URL, accessToken + "", userOpenid + "");
        //GetMethod method = new GetMethod(url);
        HttpMethod method = new PostMethod(url);
        try {
            client.executeMethod(method);

            String ubmitResult = new String(method.getResponseBody(), "utf-8");

            logger.info(ubmitResult);
            JSONObject tokenResult = JSONUtil.convertJSONObject(ubmitResult);

            return tokenResult;
        } catch (HttpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("获取用户信息出错", e);
            throw new Exception("获取用户信息出错", e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("获取用户信息出错", e);
            throw new Exception("获取用户信息出错", e);
        }

    }


    public static String weixinJoinInfo(JSONObject weixindata){

        if(weixindata.has("unionid")){
            return JSONUtil.toString(weixindata,"unionid");
        }

        return String.format("%s-%s-%s-%s-%s-%s",
                JSONUtil.toObject(weixindata,"nickname"),
                JSONUtil.toObject(weixindata,"sex") + "",
                JSONUtil.toObject(weixindata,"language"),
                JSONUtil.toObject(weixindata,"city"),
                JSONUtil.toObject(weixindata,"province"),
                JSONUtil.toObject(weixindata,"country")
                );
    }

}