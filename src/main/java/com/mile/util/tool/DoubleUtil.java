package com.mile.util.tool;


public class DoubleUtil {

    public static double getDoubleValue(Double value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    public static double add(Double value, Double value2) {
        return getDoubleValue(value) + getDoubleValue(value2);
    }


    public static double minu(Double value, Double value2) {
        return getDoubleValue(value) - getDoubleValue(value2);
    }

}