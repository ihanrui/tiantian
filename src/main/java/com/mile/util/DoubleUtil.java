package com.mile.util;


import org.apache.commons.lang.StringUtils;

public class DoubleUtil {

    public static double getDoubleValue(Double value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    public static double add(Double value, Double value2) {
        return getDoubleValue(value) + getDoubleValue(value2);
    }


    public static double minu(Double value, Double value2) {
        return getDoubleValue(value) - getDoubleValue(value2);
    }

    public static double getStringValue(String value) {
        if (StringUtils.isNotBlank(value)) {
            return Double.parseDouble(value);
        }
        return 0;
    }
}