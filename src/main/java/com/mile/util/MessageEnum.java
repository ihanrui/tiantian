package com.mile.util;

public class MessageEnum {
	public static final String USER_REGISTER_SUCCESS = "user_register_success";
	public static final String USER_REGISTER_FAILD_USERNAME = "user_register_faild_username";

	public static final String USER_NOT_EXIST = "user_not_exist";
	public static final String USER_GET_SUCCESS = "user_get_success";


	public static final String INTERNAL_SERVER_ERROR = "服务器出错，请联系客服";

	public static final String USER_TOKEN_INVALID = "用户令牌失效，请重新登录";
	public static final String SHOP_NOT_FOUND = "没有找到商品记录";
	public static final String USER_NOT_FOUND = "没有找到用户";
	public static final String RECHARGE_TYPE_ERROR = "充值类型有误";
	public static final String RECHARGE_WEIXIN_ERROR = "获取微信信息有误";



}
