package com.mile.util.payment.wechat;


import com.mile.util.payment.ParamModel;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.tools.ant.filters.StringInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;

public class WeixinUtil {

    private static final String TAG = "SDK_Sample.Util";

    private static final Logger logger = LoggerFactory.getLogger(WeixinUtil.class);

    public static byte[] httpGet(final String url) {
        if (url == null || url.length() == 0) {
            logger.error(TAG, "httpGet, url is null");
            return null;
        }

        HttpClient httpClient = getNewHttpClient();
        HttpGet httpGet = new HttpGet(url);

        try {
            HttpResponse resp = httpClient.execute(httpGet);
            if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                logger.error(TAG, "httpGet fail, status code = " + resp.getStatusLine().getStatusCode());
                return null;
            }

            return EntityUtils.toByteArray(resp.getEntity());

        } catch (Exception e) {
            logger.error(TAG, "httpGet exception, e = " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] httpPost(String url, String entity) {
        if (url == null || url.length() == 0) {
            logger.error(TAG, "httpPost, url is null");
            return null;
        }

        HttpClient httpClient = getNewHttpClient();

        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new StringEntity(entity));
            httpPost.setHeader("Accept", "application/json");
            //httpPost.setHeader("Content-type", "application/json;charset=gb2312");
            httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
            httpPost.addHeader("Content-Type", "text/xml;charset=UTF-8");
            HttpResponse resp = httpClient.execute(httpPost);
            if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                logger.error(TAG, "httpGet fail, status code = " + resp.getStatusLine().getStatusCode());
                return null;
            }

            return EntityUtils.toByteArray(resp.getEntity());
        } catch (Exception e) {
            logger.error(TAG, "httpPost exception, e = " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    private static class SSLSocketFactoryEx extends SSLSocketFactory {

        SSLContext sslContext = SSLContext.getInstance("TLS");

        public SSLSocketFactoryEx(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
                }
            };

            sslContext.init(null, new TrustManager[]{tm}, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

    private static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public static byte[] readFromFile(String fileName, int offset, int len) {
        if (fileName == null) {
            return null;
        }

        File file = new File(fileName);
        if (!file.exists()) {
            logger.error(TAG, "readFromFile: file not found");
            return null;
        }

        if (len == -1) {
            len = (int) file.length();
        }


        if (offset < 0) {
            logger.error(TAG, "readFromFile invalid offset:" + offset);
            return null;
        }
        if (len <= 0) {
            logger.error(TAG, "readFromFile invalid len:" + len);
            return null;
        }
        if (offset + len > (int) file.length()) {
            logger.error(TAG, "readFromFile invalid file len:" + file.length());
            return null;
        }

        byte[] b = null;
        try {
            RandomAccessFile in = new RandomAccessFile(fileName, "r");
            b = new byte[len]; // ´´½¨ºÏÊÊÎÄ¼þ´óÐ¡µÄÊý×é
            in.seek(offset);
            in.readFully(b);
            in.close();

        } catch (Exception e) {
            logger.error(TAG, "readFromFile : errMsg = " + e.getMessage());
            e.printStackTrace();
        }
        return b;
    }


    public static String sha1(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }

        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
            mdTemp.update(str.getBytes());

            byte[] md = mdTemp.digest();
            int j = md.length;
            char buf[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<String> stringsToList(final String[] src) {
        if (src == null || src.length == 0) {
            return null;
        }
        final List<String> result = new ArrayList<String>();
        for (int i = 0; i < src.length; i++) {
            result.add(src[i]);
        }
        return result;
    }


    public static String genNonceStr() {
        Random random = new Random();
        return MD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
    }


    private static String genAppSign(List<NameValuePair> params, String appSecret) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(appSecret);

        String appSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        logger.info("orion", appSign);
        return appSign;
    }

    public static long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }


    public static String getProductArgs(ParamModel paramModel) {

        try {


            List<NameValuePair> packageParams = getPackageParams(paramModel);

            String sign = genPackageSign(packageParams, paramModel.getApiKey());

            packageParams.add(new BasicNameValuePair("sign", sign));


            String xmlstring = toXml(packageParams);

            return xmlstring;

        } catch (Exception e) {
            logger.error(TAG, "genProductArgs fail, ex = " + e.getMessage());
            return "";
        }


    }



    public static List<NameValuePair> getPackageParams(ParamModel paramModel) {
        List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
        packageParams.add(new BasicNameValuePair("appid", paramModel.getAppId()));
        packageParams.add(new BasicNameValuePair("body", paramModel.getGoodDesc()));
        packageParams.add(new BasicNameValuePair("mch_id", paramModel.getMchId()));
        packageParams.add(new BasicNameValuePair("nonce_str", paramModel.getNonceStr()));
        packageParams.add(new BasicNameValuePair("notify_url", paramModel.getNotifyUrl()));
        if(StringUtils.isNotBlank(paramModel.getOpenid())){
            packageParams.add(new BasicNameValuePair("openid", paramModel.getOpenid()));
        }

        packageParams.add(new BasicNameValuePair("out_trade_no", paramModel.getOutTreadId()));
        packageParams.add(new BasicNameValuePair("spbill_create_ip", "127.0.0.1"));
        packageParams.add(new BasicNameValuePair("total_fee", paramModel.getTotalFee()));
        packageParams.add(new BasicNameValuePair("trade_type", paramModel.getTradeType()));


        return packageParams;
    }

    public static List<NameValuePair> getPackageParams(Map<String, String> xml) {


        List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
        Iterator iterator = xml.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String value = xml.get(key);
            if ("sign".equals(key)) {
                continue;
            }
            packageParams.add(new BasicNameValuePair(key, value));
        }

        Collections.sort(packageParams, new Comparator<NameValuePair>() {
            @Override
            public int compare(NameValuePair s1, NameValuePair s2) {

                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        return packageParams;
    }

    /**
     * 生成签名
     */

    public static String genPackageSign(List<NameValuePair> params, String apiKey) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(apiKey);
        logger.info(sb.toString());

        String packageSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        logger.info(packageSign);
        return packageSign;
    }

    private static String toISOEncoding(String value){
        try {
            return new String(value.getBytes(), "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            logger.error("新增转码出错"+e.getLocalizedMessage());
        }
        return "";
    }
    private static String toXml(List<NameValuePair> params) {

        StringBuffer sb = new StringBuffer();
        sb.append("<xml>");
        for (int i = 0; i < params.size(); i++) {

            String k = params.get(i).getName();
            String v = params.get(i).getValue();
            if ("attach".equalsIgnoreCase(k) || "body".equalsIgnoreCase(k) || "sign".equalsIgnoreCase(k)) {
                sb.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");
            } else {
                sb.append("<" + k + ">" + v + "</" + k + ">");
            }
        }
        sb.append("</xml>");

        logger.info("--toXml---orion:"+sb.toString());
        try {


            String isoString = new String(sb.toString().getBytes("utf-8"), "ISO8859-1");

            return isoString;

        } catch (Exception e) {
            logger.error(TAG, "genProductArgs fail, ex = " + e.getMessage());
            return sb.toString();
        }

    }

    public static Map<String, String> decodeXml(String content) {

        Map<String, String> xml = new HashMap<String, String>();
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new StringInputStream(content));
            Element element = doc.getDocumentElement();
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (StringUtils.isNotBlank(node.getNodeName()) && !node.getNodeName().startsWith("#text")) {
                    xml.put(node.getNodeName(), node.getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return xml;

    }

}
