package com.mile.util.event;

import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings("rawtypes")
public class UserDataEvent{
	private long id ;

	private UserEvent event ;
	private String command ;


	private JpaRepository dbRes ;


	public JpaRepository getDbRes() {
		return dbRes;
	}
	public void setDbRes(JpaRepository dbRes) {
		this.dbRes = dbRes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserEvent getEvent() {
		return event;
	}

	public void setEvent(UserEvent event) {
		this.event = event;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
}
