package com.mile.util.event;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Transient;


@SuppressWarnings("rawtypes")
public abstract class UserEventRes {


	private JpaRepository dbRes ;


	@Transient
	public JpaRepository getDbRes() {
		return dbRes;
	}
	public void setDbRes(JpaRepository dbRes) {
		this.dbRes = dbRes;
	}

}
