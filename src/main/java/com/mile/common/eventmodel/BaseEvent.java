package com.mile.common.eventmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.Message;

import javax.persistence.Transient;

public abstract class BaseEvent extends BaseEntity implements Message{

	private String command;

	@Override
	@Transient
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}
}
