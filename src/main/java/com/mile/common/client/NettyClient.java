package com.mile.common.client;

import com.mile.core.game.BeiMiClient;
import com.mile.core.entity.Token;

public interface NettyClient {

	public BeiMiClient getClient(String key) ;

	public void putClient(Token token, BeiMiClient client) ;

	public void removeClient(String key) ;
}
