package com.mile.common.client;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.SocketIOClient;
import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.game.BeiMiClient;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NettySystemClient implements NettyClient {

    private static final Logger logger = LoggerFactory.getLogger(NettySystemClient.class);

    private Map<String, BeiMiClient> systemClientsMap = new HashMap<String, BeiMiClient>();

    public BeiMiClient getClient(String key) {
        //logger.info("getClient 获取缓存对象的token为 key[" + key + "] ");
        BeiMiClient client = systemClientsMap.get(key);
        if (null == client) {
            List<Token> tokens = BMDataContext.getContext().getBean(TokenRepository.class).findByUserid(key);
            if (null != tokens && !tokens.isEmpty()) {
                Token token = tokens.get(0);
                client = getClient(token);
            }

        }
        if (null == client) {
            logger.info(" key[" + key + "] 未获取到缓存对象 BeiMiClient");
        }
        return client;
    }

    public BeiMiClient getClient(Token token) {
        //logger.info("getClient 获取缓存对象的token为 [" + token + "] ");

        BeiMiClient client = null;
        if (!StringUtils.isEmpty(token.getToken())) {
            client = systemClientsMap.get(token.getToken());
        }

        if (null == client && !StringUtils.isEmpty(token.getId())) {
            client = systemClientsMap.get(token.getId());
        }

        if (null == client && !StringUtils.isEmpty(token.getUserid())) {
            client = systemClientsMap.get(token.getUserid());
        }
        if (null == client) {
            logger.info(" token[" + token + "] 未获取到缓存对象 BeiMiClient");
        }
        return client;
    }

    public BeiMiClient getClientByData(SocketIOClient client, String data) {
        String token = JSON.parseObject(data).getString("token");
        //logger.info("getClientByData 获取缓存对象的token为 token[" + token + "] ");

        BeiMiClient beiMiClient = systemClientsMap.get(token);


        Token tokenobj = BMDataContext.getContext().getBean(TokenRepository.class).findById(token);

        if (null == beiMiClient && null != tokenobj) {
            beiMiClient = getClient(tokenobj);
        }

        if (null == beiMiClient || null == beiMiClient.getClient() || !beiMiClient.getClient().getSessionId().toString().equals(client.getSessionId().toString())) {
            logger.info(" token[" + token + "] 未获取到缓存对象 BeiMiClient");

            BeiMiClient beiMiClientSave = JSON.parseObject(data, BeiMiClient.class);
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClientSave.getToken(), beiMiClientSave.getOrgi());
            logger.info(" beiMiClientSave.getToken()[" + beiMiClientSave.getToken() + "] 未获取到缓存对象 BeiMiClient");

            if (null != userToken) {

                beiMiClientSave.setClient(client);
                beiMiClientSave.setUserid(userToken.getUserid());
                beiMiClientSave.setSession(client.getSessionId().toString());
                /**
                 * 心跳时间
                 */
                beiMiClientSave.setTime(System.currentTimeMillis());


                NettyClients.getInstance().putClient(userToken, beiMiClientSave);

                PlayUserClient playerUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), beiMiClientSave.getOrgi());

                if(null == playerUserClient){
                    logger.info("userToken.getUserid()["+userToken.getUserid()+"] 牌局中需要重新获取");
                    playerUserClient = (PlayUserClient)CacheHelper.getGamePlayerCacheBean().getPlayer(userToken.getUserid(), userToken.getOrgi());
                }


                if (null == playerUserClient) {
                    logger.info("userToken.getUserid()["+userToken.getUserid()+"] 什么都没有， 只能获取库中的");
                    playerUserClient = BMDataContext.getContext().getBean(PlayUserClientRepository.class).findById(userToken.getUserid());
                }

                if (null != playerUserClient) {
                    playerUserClient.setOnline(true);
                    CacheHelper.getApiUserCacheBean().put(playerUserClient.getId(), playerUserClient, playerUserClient.getOrgi());

                    if (null != CacheHelper.getGamePlayerCacheBean().getPlayer(playerUserClient.getId(), playerUserClient.getOrgi())) {
                        CacheHelper.getGamePlayerCacheBean().put(playerUserClient.getId(), playerUserClient, playerUserClient.getOrgi());
                    }
                }else{
                    logger.info("实在没办法， 重连失败 userToken.getUserid()["+userToken.getUserid()+"]");
                }

                logger.info("返回本次新的client userToken.getUserid()["+userToken.getUserid()+"] token["+userToken.getId()+"]");
                return beiMiClientSave;
            } else {
                logger.info(" token[" + token + "] 未获取到缓存对象 BeiMiClient     无效");
            }
        }


        UKTools.copyProperties(JSON.parseObject(data, BeiMiClient.class),beiMiClient);
        return beiMiClient;

    }

    public void putClient(Token token, BeiMiClient client) {
        //logger.info("putClient  token  [" + token + "]");
        if (null == token)
            return;

        if (!StringUtils.isEmpty(token.getToken()))
            systemClientsMap.put(token.getToken(), client);
        if (!StringUtils.isEmpty(token.getId()))
            systemClientsMap.put(token.getId(), client);
        if (!StringUtils.isEmpty(token.getUserid()))
            systemClientsMap.put(token.getUserid(), client);
        if (!StringUtils.isEmpty(client.getSession()))
            systemClientsMap.put(client.getSession(), client);
    }

    public void removeClient(String id) {
        BeiMiClient beiMiClient = this.getClient(id);
        systemClientsMap.remove(id);
        if (beiMiClient != null) {
            systemClientsMap.remove(beiMiClient.getUserid());
        }
    }

    public void joinRoom(String userid, String roomid) {
        BeiMiClient beiMiClient = this.getClient(userid);
        if (beiMiClient != null) {
            beiMiClient.getClient().joinRoom(roomid);
        }
    }

    public void sendGameEventMessage(String userid, String event, Object data) {
        BeiMiClient beiMiClient = this.getClient(userid);
        if (beiMiClient != null) {
            beiMiClient.getClient().sendEvent(event, data);
        }
    }
}
