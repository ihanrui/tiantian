package com.mile.common.constants;

public class ContextConstants {

    public static String WECHAT_LOGIN_TYPE_DOWNLOAD = "download";  // 代理系统登录类型  下载
    public static String WECHAT_LOGIN_TYPE_AGENT = "agent";  // 代理系统登录类型  登录代理系统
    public static String WECHAT_LOGIN_TYPE_AGENT_RECHARGE = "agentRecharge";  // 充值


    public static String SESSESION_KEY_WEIXIN_GAME_ACCOUNT = "WEIXIN_AGENT_GAME_ACCOUNT";  // 充值
    public static String SESSESION_KEY_WEIXIN_OPEN_ID = "openid";  // 微信open id
    public static String SESSESION_KEY_WEIXIN_PLAY_ACCOUNT = "WEIXIN_PLAY_ACCOUNT";  // 充值

    public static String WECHAT_TRADE_TYPE_APP = "APP";  // 充值
    public static String WECHAT_TRADE_TYPE_JSAPI = "JSAPI";  // 充值



    public static String SHOP_ITEM_APP = "app";  // 充值
    public static String SHOP_ITEM_WEB = "web";  // 充值





}
