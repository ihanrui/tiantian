package com.mile.common.constants;

public class SystemDicKeyConstants {

    public static String WECHAT_AGENT_APPID = "wechat.agent.appid";  // 代理系统微信 appid key
    public static String WECHAT_AGENT_SECRET = "wechat.agent.secret";  // 代理系统微信
    public static String WECHAT_AGENT_APIKEY = "wechat.agent.apiKey";  // 代理系统微信


    public static String WECHAT_AGENT_MCHID = "wechat.agent.mchid";  // 代理系统微信
    public static String AGENT_PAYMENT_NOTIFY_URL = "web.payment.notify_url";  // 充值回调

    public static String AGENT_BASEINFO_URL = "agent.baseinfo";  // 获取微信 base信息回调url
    public static String AGENT_USERINFO_URL = "agent.userInfo";  // 获取微信 user信息回调url



    public static String WECHAT_APPID = "wechat.appid";  // app系统微信 appid key
    public static String WECHAT_SECRET = "wechat.secret";  // app系统微信
    public static String WECHAT_APIKEY = "wechat.apiKey";  // 签名密钥



    public static String WECHAT_MCHID = "wechat.mchid";  // 商户平台
    public static String PAYMENT_NOTIFY_URL = "payment.notify_url";  // 充值回调
    public static String PAYMENT_APPNAME = "payment.appName";  // 充值商品名称




    public static String WECHAT_LOGIN_APPID = "wechat.login.appid";  // app系统微信 appid key
    public static String WECHAT_LOGIN_SECRET = "wechat.login.secret";  // app系统微信




    public static String SETTLEMENT_MIN = "settlement_min";  // 结算单笔最小值
    public static String SETTLEMENT_MAX = "settlement_max";  // 结算单笔最大值

    public static String GOLDCOIN_MIN = "goldcoin_min";  // 结算单笔最小值
    public static String GOLDCOIN_MAX = "goldcoin_max";  // 结算单笔最大值

}
