package com.mile.common.server;

import com.mile.core.BMDataContext;
import com.mile.core.game.RoomEventHandler;
import com.mile.core.game.mazi.handler.MaziGameEventHandler;
import com.mile.core.game.pinshi.handler.PinshiGameEventHandler;
import com.mile.core.teahouse.handler.TeahouseEventHandler;
import com.mile.core.game.GameEventHandler;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ServerRunner implements CommandLineRunner {
    private final SocketIOServer server;
    private final SocketIONamespace gameSocketNameSpace ;

    @Autowired
    public ServerRunner(SocketIOServer server) {
        this.server = server;
        gameSocketNameSpace = server.addNamespace(BMDataContext.NameSpaceEnum.GAME.getNamespace())  ;
    }

    @Bean(name="gameNamespace")
    public SocketIONamespace getGameSocketIONameSpace(SocketIOServer server ){
    	gameSocketNameSpace.addListeners(new GameEventHandler(server));
        gameSocketNameSpace.addListeners(new MaziGameEventHandler(server));
        gameSocketNameSpace.addListeners(new TeahouseEventHandler(server));
        gameSocketNameSpace.addListeners(new RoomEventHandler(server));
        gameSocketNameSpace.addListeners(new PinshiGameEventHandler(server));

    	return gameSocketNameSpace  ;
    }

    public void run(String... args) throws Exception {
        server.start();
        BMDataContext.setIMServerStatus(true);	//IMServer 启动成功
    }
}