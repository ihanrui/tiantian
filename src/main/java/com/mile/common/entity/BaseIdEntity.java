package com.mile.common.entity;

import java.util.UUID;

public class BaseIdEntity extends BaseEntity{

	protected String id = UUID.randomUUID().toString();
}
