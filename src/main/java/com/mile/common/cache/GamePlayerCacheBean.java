package com.mile.common.cache;

import com.mile.core.entity.PlayUserClient;

import java.util.List;

public interface GamePlayerCacheBean {
	/**
	 *
	 */
	public void put(String key , Object value , String orgi) ;

	/**
	 *
	 * @param key
	 * @param orgi
	 * @return
	 */
	public Object delete(String key , String orgi) ;

	/**
	 *
	 * @param roomid
	 * @param orgi
	 * @return
	 */
	public void clean(String roomid , String orgi) ;

	/**
	 *
	 * @param key
	 * @param orgi
	 * @return
	 */
	public Object getPlayer(String key, String orgi) ;

	/**
	 * 获取房间内的所有玩家
	 * @param key
	 * @param orgi
	 * @return
	 */
	public List<PlayUserClient> getCacheObject(String key , String orgi) ;

	public Object getCache();


	public long getSize();

	public GamePlayerCacheBean getCacheInstance(String string);

}
