package com.mile.common.cache.hazelcast.impl;

import com.mile.core.BMDataContext;
import com.mile.common.cache.GamePlayerCacheBean;
import com.mile.core.DataConstants;
import com.mile.core.entity.PlayUserClient;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.SqlPredicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("multi_cache")
public class GamePlayerCache implements GamePlayerCacheBean {

	private static final Logger logger = LoggerFactory.getLogger(GamePlayerCache.class);


	@Autowired
	public HazelcastInstance hazelcastInstance;

	public HazelcastInstance getInstance(){
		return hazelcastInstance ;
	}

	public void clean(String roomid ,String orgi) {
		List<PlayUserClient> palyers = getCacheObject(roomid , orgi );
		if(palyers!=null && palyers.size() > 0){
			for(PlayUserClient player : palyers){
				this.delete(player.getId() , orgi) ;
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PlayUserClient> getNonWatchCacheObject(String key, String orgi) {
		if(StringUtils.isEmpty(key))
			return null;
		PagingPredicate<String,PlayUserClient > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate( " roomid = '" + key + "' and gamestatus <> '"+ BMDataContext.GameStatusEnum.WATCH.toString()+"'") , 100 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PlayUserClient> getAllCacheObject(String key, String orgi) {
		if(StringUtils.isEmpty(key))
			return null;
		PagingPredicate<String,PlayUserClient > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate( " roomid = '" + key + "' ") , 100 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PlayUserClient> getCacheObject(String key, String orgi) {
		if(StringUtils.isEmpty(key))
			return null;
		PagingPredicate<String,PlayUserClient > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate( " roomid = '" + key + "' ") , 100 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}






	public List<PlayUserClient> getCacheObject(String key,String gamestatus, String orgi) {
		if(StringUtils.isEmpty(key))
			return null;
		PagingPredicate<String,PlayUserClient > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate( " roomid = '" + key + "' and gamestatus = '"+ gamestatus+"'") , 100 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}

	public String getName() {
		return BMDataContext.BEIMI_GAME_PLAYWAY ;
	}
	@Override
	public void put(String key, Object value, String orgi) {

		if(value instanceof PlayUserClient){
			PlayUserClient p = (PlayUserClient)value;
			logger.info(" 用户对象放入GamePlayerCache gameid ["+p.getGameid()+"] username ["+p.getUsername()+"] 在线标志 ["+p.isOnline()+"] 金币["+p.getGoldcoins()+"] 钻石["+p.getDiamonds()+"]");
		}

		getInstance().getMap(getName()).put(key, value) ;
	}
	@Override
	public Object delete(String key, String orgi) {
		return getInstance().getMap(getName()).remove(key) ;
	}
	@Override
	public Object getPlayer(String key, String orgi) {
		return getInstance().getMap(getName()).get(key) ;
	}

	@Override
	public Object getCache() {
		return getInstance().getMap(getName());
	}
	@Override
	public long getSize() {
		// TODO Auto-generated method stub
		return getInstance().getMap(getName()).size();
	}

	@Override
	public GamePlayerCacheBean getCacheInstance(String string) {
		return this;
	}
}
