package com.mile.common.cache.hazelcast.impl;

import com.mile.common.cache.CacheBean;
import com.mile.core.entity.GameRoom;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.SqlPredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;

@Service("game_room_cache")
public class GameRoomCache implements CacheBean{

	private static final Logger logger = LoggerFactory.getLogger(GameRoomCache.class);


	@Autowired
	public HazelcastInstance hazelcastInstance;

	private String cacheName ;

	public HazelcastInstance getInstance(){
		return hazelcastInstance ;
	}
	public CacheBean getCacheInstance(String cacheName){
		this.cacheName = cacheName ;
		return this ;
	}

	@Override
	public void put(String key, Object value, String orgi) {
		logger.info("GameRoomCache 更新缓存  key["+key+"] value.class ["+value.getClass().getName()+"]");
		if(StringUtils.isEmpty(key)) {
			logger.error("更新缓存异常，key为空");
			return;
		}
		getInstance().getMap(getName()).put(key, value) ;
	}

	@Override
	public void clear(String orgi) {
		getInstance().getMap(getName()).clear();
	}

	@Override
	public Object delete(String key, String orgi) {
		logger.info("GameRoomCache 删除缓存  key["+key+"] ");
		return getInstance().getMap(getName()).remove(key) ;
	}

	@Override
	public void update(String key, String orgi, Object value) {
		getInstance().getMap(getName()).put(key, value);
	}

	@Override
	public Object getCacheObject(String key, String orgi) {
		logger.info("GameRoomCache 获取缓存  key["+key+"] ");
		if(StringUtils.isEmpty(key))
			return null;
		return getInstance().getMap(getName()).get(key);
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<GameRoom> getGameroomObject(String teanum) {
		logger.info("通过茶楼号["+teanum+"] 获取所有房间信息 ");
		if(StringUtils.isEmpty(teanum))
			return null;
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "'") , 1000 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<GameRoom> getGameroomObject(String teanum,String roomtype) {
		logger.info("通过茶楼号["+teanum+"] 获取所有房间信息 ");
		if(StringUtils.isEmpty(teanum))
			return null;
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "' and  roomtype = '" + roomtype + "'") , 1000 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		return playerList;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GameRoom getGameroomObject(String teanum,int tablenum) {
		logger.info("通过茶楼号["+teanum+"] tablenum["+tablenum+"]获取所有房间信息 ");
		if(StringUtils.isEmpty(teanum))
			return null;
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "' and tablenum = "+ tablenum) , 1000 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		if(null != playerList && !playerList.isEmpty()) {
			return (GameRoom) playerList.get(0);
		}
		return null;
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GameRoom getGameroomObject(String teanum,int tablenum,String roomtype) {
		logger.info("通过茶楼号["+teanum+"] tablenum["+tablenum+"]获取所有房间信息 ");
		if(StringUtils.isEmpty(teanum))
			return null;
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "' and tablenum = "+ tablenum +" and roomtype = '" + roomtype + "' ") , 1000 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		if(null != playerList && !playerList.isEmpty()) {
			return (GameRoom) playerList.get(0);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GameRoom getGameroomByRoomid(String roomid) {
		logger.info("通过房间号["+roomid+"] 获取所有房间信息 ");
		if(StringUtils.isEmpty(roomid))
			return null;
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " roomid = '" + roomid + "'") , 1000 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		if(null != playerList && !playerList.isEmpty()) {
			return (GameRoom) playerList.get(0);
		}
		return null;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GameRoom getGameroomByTea(String teanum,int tablenum) {
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "' and tablenum="+tablenum) , 1 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		if(null != playerList && !playerList.isEmpty()) {
			return (GameRoom) playerList.get(0);
		}
		return null;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GameRoom getGameroomByTea(String teanum,int tablenum,String roomtype) {
		PagingPredicate<String,GameRoom > pagingPredicate = null ;
		List playerList = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameRoom>(  new SqlPredicate( " teanum = '" + teanum + "' and tablenum="+tablenum+"  and roomtype = '" + roomtype + "'") , 1 );
		playerList.addAll((getInstance().getMap(getName())).values(pagingPredicate) ) ;
		if(null != playerList && !playerList.isEmpty()) {
			return (GameRoom) playerList.get(0);
		}
		return null;
	}

	public String getName() {
		return cacheName ;
	}

//	@Override
	public void service() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<?> getAllCacheObject(String orgi) {
		return getInstance().getMap(getName()).keySet();
	}
	@Override
	public Object getCacheObject(String key, String orgi, Object defaultValue) {
		return getCacheObject(key, orgi);
	}
	@Override
	public Object getCache() {
		return getInstance().getMap(cacheName);
	}

	@Override
	public Lock getLock(String lock , String orgi) {
		// TODO Auto-generated method stub
		return getInstance().getLock(lock);
	}
	@Override
	public long getSize() {
		return getInstance().getMap(getName()).size();
	}
	@Override
	public long getAtomicLong(String cacheName) {
		return getInstance().getAtomicLong(getName()).incrementAndGet();
	}
	@Override
	public void setAtomicLong(String cacheName, long start) {
		getInstance().getAtomicLong(getName()).set(start);
	}
}
