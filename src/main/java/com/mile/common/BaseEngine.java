package com.mile.common;

import com.corundumstudio.socketio.SocketIOServer;
import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.engine.game.GameBoard;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.*;
import com.mile.core.game.majiang.task.CreateMJRaiseHandsTask;
import com.mile.core.game.msgmodel.*;
import com.mile.util.GameUtils;
import com.mile.util.rules.model.Action;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class BaseEngine {

    private static final Logger logger = LoggerFactory.getLogger(BaseEngine.class);

    @Autowired
    protected SocketIOServer server;

    @Resource
    private KieSession kieSession;


    /**
     * 抢地主，斗地主
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public void actionRequest(String roomid, PlayUserClient playUser, String orgi, boolean accept) {
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            DiZhuBoard board = (DiZhuBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            Player player = board.player(playUser.getId());
            board = ActionTaskUtils.doCatch(board, player, accept);

            ActionTaskUtils.sendEvent("catchresult", new GameBoard(player.getPlayuser(), player.isAccept(), board.isDocatch(), board.getRatio()), gameRoom);
            GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.AUTO.toString(), 15);    //通知状态机 , 继续执行

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());

            CacheHelper.getExpireCache().put(gameRoom.getRoomid(), ActionTaskUtils.createAutoTask(1, gameRoom));
        }
    }

    /**
     * 点击准备
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public void startGameRequest(String roomid, PlayUserClient playUser, String orgi, boolean opendeal) {
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            playUser.setRoomready(true);
            if (opendeal == true) {
                playUser.setOpendeal(opendeal);
            }

            CacheHelper.getGamePlayerCacheBean().put(playUser.getId(), playUser, playUser.getOrgi());
            ActionTaskUtils.roomReady(gameRoom, GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()), playUser);


        }
    }



    /**
     * 提示按钮 后台功能处理
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public TakeCards getNextCards(String roomid, PlayUserClient playUser, String orgi) {
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            DiZhuBoard board = (DiZhuBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            Player player = board.player(playUser.getId());

            TakeCards takeCards = null;

            if (board.getLast() != null && !board.getLast().getUserid().equals(player.getPlayuser())) {    //当前无出牌信息，刚开始出牌，或者出牌无玩家 压
                takeCards = board.cardtip(player, board.getLast());
            } else {
                takeCards = board.cardtip(player, null);
            }

            if (takeCards.getCards() == null) {
                takeCards.setAllow(false);    //没有 管的起的牌
            }
            return takeCards;
        }
        return null;
    }


    /**
     * 检查是否所有玩家 都已经处于就绪状态，如果所有玩家都点击了 继续开始游戏，则发送一个 ALL事件，继续游戏，
     * 否则，等待10秒时间，到期后如果玩家还没有就绪，就将该玩家T出去，等待新玩家加入
     *
     * @param roomid
     * @param userid
     * @param orgi
     * @return
     */
    public void restartRequest(String roomid, PlayUserClient playerUser, BeiMiClient beiMiClient, boolean opendeal) {
        boolean notReady = false;
        List<PlayUserClient> playerList = null;
        GameRoom gameRoom = null;
        if (!StringUtils.isBlank(roomid)) {
            gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, playerUser.getOrgi());
            playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            if (playerList != null && playerList.size() > 0) {
                /**
                 * 有一个 等待
                 */
                for (PlayUserClient player : playerList) {
                    if (player.isRoomready() == false) {
                        notReady = true;
                        break;
                    }
                }
            }
        }
        if (notReady == true && gameRoom != null) {
            /**
             * 需要增加一个状态机的触发事件：等待其他人就绪，超过5秒以后未就绪的，直接踢掉，然后等待机器人加入
             */
            this.startGameRequest(roomid, playerUser, playerUser.getOrgi(), opendeal);
        } else if (playerList == null || playerList.size() == 0 || gameRoom == null) {//房间已解散
            BMDataContext.getGameEngine().gameRequest(playerUser, beiMiClient);
            /**
             * 结算后重新开始游戏
             */
            playerUser.setRoomready(true);
            CacheHelper.getGamePlayerCacheBean().put(playerUser.getId(), playerUser, playerUser.getOrgi());
        }
    }

    /**
     * 出牌，并校验出牌是否合规
     *
     * @param roomid
     * @param userid
     * @param orgi
     * @return
     */
    public SelectColor selectColorRequest(String roomid, String userid, String orgi, String color) {
        SelectColor selectColor = null;
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            if (board != null) {
                //超时了 ， 执行自动出牌
//				Player[] players = board.getPlayers() ;
                /**
                 * 检查是否所有玩家都已经选择完毕 ， 如果所有人都选择完毕，即可开始
                 */
                selectColor = new SelectColor(board.getBanker());
                if (!StringUtils.isBlank(color)) {
                    if (!StringUtils.isBlank(color) && color.matches("[0-2]{1}")) {
                        selectColor.setColor(Integer.parseInt(color));
                    } else {
                        selectColor.setColor(0);
                    }
                    selectColor.setTime(System.currentTimeMillis());
                    selectColor.setCommand("selectresult");

                    selectColor.setUserid(userid);
                }
                boolean allselected = true;
                for (Player ply : board.getPlayers()) {
                    if (ply.getPlayuser().equals(userid)) {
                        if (!StringUtils.isBlank(color) && color.matches("[0-2]{1}")) {
                            ply.setColor(Integer.parseInt(color));
                        } else {
                            ply.setColor(0);
                        }
                        ply.setSelected(true);
                    }
                    if (!ply.isSelected()) {
                        allselected = false;
                    }
                }
                CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());    //更新缓存数据
                ActionTaskUtils.sendEvent("selectresult", selectColor, gameRoom);
                /**
                 * 检查是否全部都已经 定缺， 如果已全部定缺， 则发送 开打
                 */
                if (allselected) {
                    /**
                     * 重置计时器，立即执行
                     */
                    CacheHelper.getExpireCache().put(gameRoom.getId(), new CreateMJRaiseHandsTask(1, gameRoom, gameRoom.getOrgi()));
                    GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.RAISEHANDS.toString(), 0);
                }
            }
        }
        return selectColor;
    }

    /**
     * 麻将 ， 杠碰吃胡过
     *
     * @param roomid
     * @param userid
     * @param orgi
     * @return
     */
    public ActionEvent actionEventRequest(String roomid, String userid, String orgi, String action) {
        ActionEvent actionEvent = null;
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            if (board != null) {
                Player player = board.player(userid);
                byte card = board.getLast().getCard();
                actionEvent = new ActionEvent(board.getBanker(), userid, card, action);
                if (!StringUtils.isBlank(action) && action.equals(BMDataContext.PlayerAction.GUO.toString())) {
                    /**
                     * 用户动作，选择 了 过， 下一个玩家直接开始抓牌
                     * bug，待修复：如果有多个玩家可以碰，则一个碰了，其他玩家就无法操作了
                     */
                    board.dealRequest(gameRoom, board, orgi, false, null);
                } else if (!StringUtils.isBlank(action) && action.equals(BMDataContext.PlayerAction.PENG.toString()) && allowAction(card, player.getActions(), BMDataContext.PlayerAction.PENG.toString())) {
                    Action playerAction = new Action(userid, action, card);

                    int color = card / 36;
                    int value = card % 36 / 4;
                    List<Byte> otherCardList = new ArrayList<Byte>();
                    for (int i = 0; i < player.getCards().length; i++) {
                        if (player.getCards()[i] / 36 == color && (player.getCards()[i] % 36) / 4 == value) {
                            continue;
                        }
                        otherCardList.add(player.getCards()[i]);
                    }
                    byte[] otherCards = new byte[otherCardList.size()];
                    for (int i = 0; i < otherCardList.size(); i++) {
                        otherCards[i] = otherCardList.get(i);
                    }
                    player.setCards(otherCards);
                    player.getActions().add(playerAction);

                    board.setNextplayer(new NextPlayer(userid, false));

                    actionEvent.setTarget(board.getLast().getUserid());
                    ActionTaskUtils.sendEvent("selectaction", actionEvent, gameRoom);

                    CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());    //更新缓存数据

                    board.playcards(board, gameRoom, player, orgi);

                } else if (!StringUtils.isBlank(action) && action.equals(BMDataContext.PlayerAction.GANG.toString()) && allowAction(card, player.getActions(), BMDataContext.PlayerAction.GANG.toString())) {
                    if (board.getNextplayer().getNextplayer().equals(userid)) {
                        card = GameUtils.getGangCard(player.getCards());
                        actionEvent = new ActionEvent(board.getBanker(), userid, card, action);
                        actionEvent.setActype(BMDataContext.PlayerGangAction.AN.toString());
                    } else {
                        actionEvent.setActype(BMDataContext.PlayerGangAction.MING.toString());    //还需要进一步区分一下是否 弯杠
                    }
                    /**
                     * 检查是否有弯杠
                     */
                    Action playerAction = new Action(userid, action, card);
                    for (Action ac : player.getActions()) {
                        if (ac.getCard() == card && ac.getAction().equals(BMDataContext.PlayerAction.PENG.toString())) {
                            ac.setGang(true);
                            ac.setType(BMDataContext.PlayerGangAction.WAN.toString());
                            playerAction = ac;
                            break;
                        }
                    }
                    int color = card / 36;
                    int value = card % 36 / 4;
                    List<Byte> otherCardList = new ArrayList<Byte>();
                    for (int i = 0; i < player.getCards().length; i++) {
                        if (player.getCards()[i] / 36 == color && (player.getCards()[i] % 36) / 4 == value) {
                            continue;
                        }
                        otherCardList.add(player.getCards()[i]);
                    }
                    byte[] otherCards = new byte[otherCardList.size()];
                    for (int i = 0; i < otherCardList.size(); i++) {
                        otherCards[i] = otherCardList.get(i);
                    }
                    player.setCards(otherCards);
                    player.getActions().add(playerAction);

                    actionEvent.setTarget("all");    //只有明杠 是 其他人打出的 ， target 是单一对象

                    ActionTaskUtils.sendEvent("selectaction", actionEvent, gameRoom);

                    /**
                     * 杠了以后， 从 当前 牌的 最后一张开始抓牌
                     */
                    board.dealRequest(gameRoom, board, orgi, true, userid);
                } else if (!StringUtils.isBlank(action) && action.equals(BMDataContext.PlayerAction.HU.toString())) {    //判断下是不是 真的胡了 ，避免外挂乱发的数据
                    Action playerAction = new Action(userid, action, card);
                    player.getActions().add(playerAction);
                    GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(gameRoom.getPlayway(), gameRoom.getOrgi());
                    /**
                     * 不同的胡牌方式，处理流程不同，推倒胡，直接进入结束牌局 ， 血战：当前玩家结束牌局，血流：继续进行，下一个玩家
                     */
                    if (gamePlayway.getWintype().equals(BMDataContext.MaJiangWinType.TUI.toString())) {        //推倒胡
                        GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.ALLCARDS.toString(), 0);    //打完牌了,通知结算
                    } else { //血战到底
                        if (gamePlayway.getWintype().equals(BMDataContext.MaJiangWinType.END.toString())) {        //标记当前玩家的状态 是 已结束
                            player.setEnd(true);
                        }
                        player.setHu(true);    //标记已经胡了
                        /**
                         * 当前 Player打上标记，已经胡牌了，杠碰吃就不会再有了
                         */
                        /**
                         * 下一个玩家出牌
                         */
                        player = board.nextPlayer(board.index(player.getPlayuser()));
                        /**
                         * 记录胡牌的相关信息，推倒胡 | 血战 | 血流
                         */
                        board.setNextplayer(new NextPlayer(player.getPlayuser(), false));

                        actionEvent.setTarget(board.getLast().getUserid());
                        /**
                         * 用于客户端播放 胡牌的 动画 ， 点胡 和 自摸 ，播放不同的动画效果
                         */
                        ActionTaskUtils.sendEvent("selectaction", actionEvent, gameRoom);
                        CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());    //更新缓存数据

                        /**
                         * 杠了以后， 从 当前 牌的 最后一张开始抓牌
                         */
                        board.dealRequest(gameRoom, board, orgi, true, player.getPlayuser());
                    }
                }
            }
        }
        return actionEvent;
    }

    /**
     * 为防止同步数据错误，校验是否允许刚碰牌
     *
     * @param card
     * @param actions
     * @return
     */
    public boolean allowAction(byte card, List<Action> actions, String actiontype) {
        int take_color = card / 36;
        int take_value = card % 36 / 4;
        boolean allow = true;
        for (Action action : actions) {
            int color = action.getCard() / 36;
            int value = action.getCard() % 36 / 4;
            if (take_color == color && take_value == value && action.getAction().equals(actiontype)) {
                allow = false;
                break;
            }
        }
        return allow;
    }

    /**
     * 出牌，不出牌
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public void noCardsRequest(String roomid, PlayUserClient playUser, String orgi) {

    }





}
