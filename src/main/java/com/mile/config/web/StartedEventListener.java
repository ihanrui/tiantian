package com.mile.config.web;

import com.mile.core.BMDataContext;
import com.mile.core.entity.GamePlayway;
import com.mile.core.game.GameEngine;
import com.mile.core.game.RoomEngine;
import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.mazi.engine.MaziGameEngine;
import com.mile.core.game.pinshi.engine.PinshiGameEngine;
import com.mile.core.teahouse.service.TeahouseService;
import com.mile.common.cache.CacheHelper;
import com.mile.web.model.*;
import com.mile.web.service.repository.jpa.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class StartedEventListener implements ApplicationListener<ContextRefreshedEvent> {

	@Resource
	private GameEngine gameEngine ;


	@Resource
	private MaziGameEngine maziGameEngine ;

	@Resource
	private RoomEngine roomEngine ;


	@Resource
	private TeahouseService teahouseService ;



	private SysDicRepository sysDicRes;


	@Resource
	private PinshiGameEngine pinshiGameEngine ;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	if(BMDataContext.getContext() == null){
    		BMDataContext.setApplicationContext(event.getApplicationContext());
    	}
    	BMDataContext.setGameEngine(gameEngine);

		BMDataContext.setMaziGameEngine(maziGameEngine);

		BMDataContext.setTeahouseService(teahouseService);

		BMDataContext.setRoomEngine(roomEngine);

		BMDataContext.setPinshiGameEngine(pinshiGameEngine);



    	sysDicRes = event.getApplicationContext().getBean(SysDicRepository.class) ;
    	List<SysDic> sysDicList = sysDicRes.findAll() ;

    	for(SysDic dic : sysDicList){
    		CacheHelper.getSystemCacheBean().put(dic.getId(), dic, dic.getOrgi());
			if(dic.getParentid().equals("0")){
				List<SysDic> sysDicItemList = new ArrayList<SysDic>();
				for(SysDic item : sysDicList){
					if(item.getDicid()!=null && item.getDicid().equals(dic.getId())){
						sysDicItemList.add(item) ;
					}
				}
				CacheHelper.getSystemCacheBean().put(dic.getCode(), sysDicItemList, dic.getOrgi());
			}
		}
    	/**
    	 * 加载系统全局配置
    	 */
    	SystemConfigRepository systemConfigRes = event.getApplicationContext().getBean(SystemConfigRepository.class) ;
    	SystemConfig config = systemConfigRes.findByOrgi(BMDataContext.SYSTEM_ORGI) ;
    	if(config != null){
    		CacheHelper.getSystemCacheBean().put("systemConfig", config, BMDataContext.SYSTEM_ORGI);
    	}


    	GamePlaywayRepository playwayRes = event.getApplicationContext().getBean(GamePlaywayRepository.class) ;
    	List<GamePlayway> gamePlaywayList = playwayRes.findAll() ;
    	if(gamePlaywayList.size() > 0){
    		for(GamePlayway playway : gamePlaywayList){
    			CacheHelper.getSystemCacheBean().put(playway.getId(), playway, playway.getOrgi());
    		}
    	}

//    	GameRoomRepository gameRoomRes = event.getApplicationContext().getBean(GameRoomRepository.class) ;
//    	List<GameRoom> gameRoomList = gameRoomRes.findAll() ;
//    	if(gameRoomList.size() > 0){
//    		for(GameRoom gameRoom : gameRoomList){
//    			if(gameRoom.isCardroom()){
//    				gameRoomRes.delete(gameRoom);//回收房卡房间资源
//    			}else{
//    				CacheHelper.getQueneCache().put(gameRoom, gameRoom.getOrgi());
//    				CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
//    			}
//    		}
//    	}

    	GenerationRepository generationRes = event.getApplicationContext().getBean(GenerationRepository.class) ;
    	List<Generation> generationList = generationRes.findAll() ;
    	for(Generation generation : generationList){
    		CacheHelper.getSystemCacheBean().setAtomicLong(BMDataContext.ModelType.ROOM.toString(), generation.getStartinx());
    	}
    }

	public GameEngine getGameEngine() {
		return gameEngine;
	}

	public void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	public MaziGameEngine getMaziGameEngine() {
		return maziGameEngine;
	}

	public void setMaziGameEngine(MaziGameEngine maziGameEngine) {
		this.maziGameEngine = maziGameEngine;
	}

	public TeahouseService getTeahouseService() {
		return teahouseService;
	}

	public void setTeahouseService(TeahouseService teahouseService) {
		this.teahouseService = teahouseService;
	}

	public SysDicRepository getSysDicRes() {
		return sysDicRes;
	}

	public void setSysDicRes(SysDicRepository sysDicRes) {
		this.sysDicRes = sysDicRes;
	}
}