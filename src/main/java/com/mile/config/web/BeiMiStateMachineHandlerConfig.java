package com.mile.config.web;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mile.config.web.model.Game;
import com.mile.core.statemachine.BeiMiStateMachine;
import com.mile.core.statemachine.impl.BeiMiMachineHandler;

@Configuration
public class BeiMiStateMachineHandlerConfig {

	@Resource(name="dizhu")
	private BeiMiStateMachine<String,String> dizhuConfigure ;

	@Resource(name="majiang")
	private BeiMiStateMachine<String,String> maJiangConfigure ;

    @Resource(name="mazi")
    private BeiMiStateMachine<String,String> maziConfigure ;

    @Resource(name="pinshi")
    private BeiMiStateMachine<String,String> pinshiConfigure ;


    @Bean("dizhuGame")
    public Game dizhu() {
        return new Game(new BeiMiMachineHandler(this.dizhuConfigure));
    }

    @Bean("majiangGame")
    public Game majiang() {
        return new Game(new BeiMiMachineHandler(this.maJiangConfigure));
    }

    @Bean("maziGame")
    public Game mazi() {
        return new Game(new BeiMiMachineHandler(this.maziConfigure));
    }


    @Bean("pinshiGame")
    public Game pinshi() {
        return new Game(new BeiMiMachineHandler(this.pinshiConfigure));
    }
}
