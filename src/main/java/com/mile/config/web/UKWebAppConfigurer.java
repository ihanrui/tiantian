package com.mile.config.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.mile.web.interceptor.CrossInterceptorHandler;
import com.mile.web.interceptor.UserInterceptorHandler;

@Configuration
public class UKWebAppConfigurer
        extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
    	registry.addInterceptor(new UserInterceptorHandler()).addPathPatterns("/**").excludePathPatterns("/login.html")
                .excludePathPatterns("/tokens").excludePathPatterns("/api/**").excludePathPatterns("/api2/**")
                .excludePathPatterns("/hall/**").excludePathPatterns("/record/**").excludePathPatterns("/test/**")
                .excludePathPatterns("/teahouse/**")
                .excludePathPatterns("/payment/notifyback")
                .excludePathPatterns("/payment/createtrade")
                .excludePathPatterns("/webpayment/notifyback")
                .excludePathPatterns("/webpayment/createtrade")
                .excludePathPatterns("/.well-known/pki-validation/fileauth.txt")
                .excludePathPatterns("/agent/**/**")
                .excludePathPatterns("/agent/**")
                .excludePathPatterns("/agent/gameagent/*");
    	registry.addInterceptor(new CrossInterceptorHandler()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}