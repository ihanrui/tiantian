package com.mile.core.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 字典分组
 */
@Entity
@Table(name = "wd_s_typegroup")
@org.hibernate.annotations.Proxy(lazy = false)
public class DicTypeGroup extends BaseEntity{


	private String id = UKTools.getUUID();
	private String  typegroupcode;//
	private String typegroupname;//

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTypegroupcode() {
		return typegroupcode;
	}

	public void setTypegroupcode(String typegroupcode) {
		this.typegroupcode = typegroupcode;
	}

	public String getTypegroupname() {
		return typegroupname;
	}

	public void setTypegroupname(String typegroupname) {
		this.typegroupname = typegroupname;
	}
}
