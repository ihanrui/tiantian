package com.mile.core.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "wd_r_play_record")
@org.hibernate.annotations.Proxy(lazy = false)
public class PlayRecord extends BaseEntity{


	private String id = UKTools.getUUID();;
	private String roomid;//房间号
	private String teahouseid;//茶楼
	private String tableid;//桌号
	private int seat;//座位号
	private String userid;//用户ID
	private Date createdate;//创建时间
	private String  cards;//消耗房卡
	private double integral;//所得积分
	private double goldcoins;//网豆输赢
	private double diamonds;//钻石输赢
	private String memo;//备注

    private String username;//用户名称  同微信名称
    private String userurl;//用户头像地址



    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间




    @Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getTeahouseid() {
        return teahouseid;
    }

    public void setTeahouseid(String teahouseid) {
        this.teahouseid = teahouseid;
    }

    public String getTableid() {
        return tableid;
    }

    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    public double getIntegral() {
        return integral;
    }

    public void setIntegral(double integral) {
        this.integral = integral;
    }

    public double getGoldcoins() {
        return goldcoins;
    }

    public void setGoldcoins(double goldcoins) {
        this.goldcoins = goldcoins;
    }

    public double getDiamonds() {
        return diamonds;
    }

    public void setDiamonds(double diamonds) {
        this.diamonds = diamonds;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}
