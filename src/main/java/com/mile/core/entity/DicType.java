package com.mile.core.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


/**
 * 字典类型
 */
@Entity
@Table(name = "wd_s_type")
@org.hibernate.annotations.Proxy(lazy = false)
public class DicType extends BaseEntity {


    private String id = UKTools.getUUID();

    private String typecode;
    private String typename;
    private String typegroupid;

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTypecode() {
        return typecode;
    }

    public void setTypecode(String typecode) {
        this.typecode = typecode;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getTypegroupid() {
        return typegroupid;
    }

    public void setTypegroupid(String typegroupid) {
        this.typegroupid = typegroupid;
    }
}
