package com.mile.core.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.core.record.entity.Summary;
import com.mile.util.UKTools;
import com.mile.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;



@Entity
@Table(name = "bm_game_room")
@org.hibernate.annotations.Proxy(lazy = false)
public class GameRoom extends BaseEntity implements UserEvent,   Comparable<GameRoom>{

	private static final long serialVersionUID = 1L;


	private String id = UKTools.getUUID();
	private String name ;
	private String code ;
	private String roomid ;	//房间ID，房卡游戏的 房间ID是 6位数字，其他为 UUID


	private String playwaycode;//玩法编码

	private String teanum;//茶楼号

	private Integer tablenum;//桌号

	private boolean matchmodel ;	//是否比赛房间
	private String matchid ;//赛事ID
	private Integer matchscreen;//比赛场次
	private String matchtype;	//比赛类型

	private String lastwinner ;	//最后赢的人 ， 可多人 ， 逗号隔开


	private String createtime ;
	private String parentid ;
	private String typeid ;
	private String creater;
	private String username ;

	private String status ;	//当前状态

	private Date updatetime ;
	private String orgi ;
	private String area ;

	private String game ;	//游戏类型 ： 麻将：地主：德州
	private Integer players ;	//最大游戏人数
	private Integer cardsnum ;	//发牌数量
	private Integer curpalyers ;	//当前人数

	private boolean cardroom ;	//是否房卡模式

	private String master ;	//房主 ，开设房间的人 或第一个进入的人

	private String roomtype ;	//房间类型， 房卡：大厅

	private String playway ;	//玩法

	private Integer numofgames ;	//局数
	private Integer currentnum ;	//已完局数

	private PlayUser masterUser ;	//房间的创建人
	private GamePlayway gamePlayway ;	//房间玩法



	private Map<String,String> extparams ;//房卡模式下的自定义参数          这个里面记录座次号  site1,site2,site3,site4.



	private Summary summary;//结算信息


	private String  deductflag="N";//是否可以扣卡 Y/N  首局结算前，无需扣卡。


	private String nowgamenum;//局数描述

	private String playwaydesc;//玩法描述

	private String roomdesc;//房间描述













	//Y.摸队/N.铁对
	private String  teamflag;


	//Y.报喜/N.不报喜
	private String  newsflag;


	//扣卡类型  N.房主，Y.大赢家，2.AA
	private String coinexpend;


	//投降类型  Y.可以投降，N.不可投降
	private String giveupflag;//是否可以投降


	private Integer scoreunit;//底分

	private String noscore;//无分


	private boolean startflag;//开始标志




	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getRoomtype() {
		return roomtype;
	}
	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
	public String getPlayway() {
		return playway;
	}
	public void setPlayway(String playway) {
		this.playway = playway;
	}
		public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		public boolean isCardroom() {
		return cardroom;
	}
	public void setCardroom(boolean cardroom) {
		this.cardroom = cardroom;
	}
	@Transient
	public PlayUser getMasterUser() {
		return masterUser;
	}
	public void setMasterUser(PlayUser masterUser) {
		this.masterUser = masterUser;
	}
	@Transient
	public GamePlayway getGamePlayway() {
		return gamePlayway;
	}
	public void setGamePlayway(GamePlayway gamePlayway) {
		this.gamePlayway = gamePlayway;
	}
	public boolean isMatchmodel() {
		return matchmodel;
	}
	public void setMatchmodel(boolean matchmodel) {
		this.matchmodel = matchmodel;
	}
	public String getMatchid() {
		return matchid;
	}
	public void setMatchid(String matchid) {
		this.matchid = matchid;
	}
		public String getMatchtype() {
		return matchtype;
	}
	public void setMatchtype(String matchtype) {
		this.matchtype = matchtype;
	}
		public String getLastwinner() {
		return lastwinner;
	}
	public void setLastwinner(String lastwinner) {
		this.lastwinner = lastwinner;
	}

	@Transient
	public Map<String, String> getExtparams() {
		return extparams;
	}

	public void putExtparams(String key,String value){
		this.getExtparams().put(key,value);
	}
	public void setExtparams(Map<String, String> extparams) {
		this.extparams = extparams;
	}

	@Override
	public int compareTo(GameRoom o) {
		// TODO Auto-generated method stub
		return this.currentnum - o.getCurpalyers();
	}






	@Transient
	public Summary getSummary() {
		return summary;
	}

	public void setSummary(Summary summary) {
		this.summary = summary;
	}



	public String getTeamflag() {
		return teamflag;
	}

	public void setTeamflag(String teamflag) {
		this.teamflag = teamflag;
	}

	public String getNewsflag() {
		return newsflag;
	}

	public void setNewsflag(String newsflag) {
		this.newsflag = newsflag;
	}


	public String getCoinexpend() {
		return coinexpend;
	}

	public void setCoinexpend(String coinexpend) {
		this.coinexpend = coinexpend;
	}



	public String getGiveupflag() {
		return giveupflag;
	}

	public void setGiveupflag(String giveupflag) {
		this.giveupflag = giveupflag;
	}



	public String getTeanum() {
		return teanum;
	}

	public void setTeanum(String teanum) {
		this.teanum = teanum;
	}


	public String getDeductflag() {
		return deductflag;
	}

	public void setDeductflag(String deductflag) {
		this.deductflag = deductflag;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}

	public Integer getMatchscreen() {
		return matchscreen;
	}

	public void setMatchscreen(Integer matchscreen) {
		this.matchscreen = matchscreen;
	}

	public Integer getPlayers() {
		return players;
	}

	public void setPlayers(Integer players) {
		this.players = players;
	}

	public Integer getCardsnum() {
		return cardsnum;
	}

	public void setCardsnum(Integer cardsnum) {
		this.cardsnum = cardsnum;
	}

	public Integer getCurpalyers() {
		return curpalyers;
	}

	public void setCurpalyers(Integer curpalyers) {
		this.curpalyers = curpalyers;
	}

	public Integer getNumofgames() {
		return numofgames;
	}

	public void setNumofgames(Integer numofgames) {
		this.numofgames = numofgames;
	}

	public Integer getCurrentnum() {
		return currentnum;
	}

	public void setCurrentnum(Integer currentnum) {
		this.currentnum = currentnum;
	}

	public Integer getScoreunit() {
		return scoreunit;
	}

	public void setScoreunit(Integer scoreunit) {
		this.scoreunit = scoreunit;
	}

	public String getNowgamenum() {
		return nowgamenum;
	}

	public void setNowgamenum(String nowgamenum) {
		this.nowgamenum = nowgamenum;
	}

	public String getPlaywaydesc() {
		return playwaydesc;
	}

	public void setPlaywaydesc(String playwaydesc) {
		this.playwaydesc = playwaydesc;
	}

	public String getRoomdesc() {
		return roomdesc;
	}

	public void setRoomdesc(String roomdesc) {
		this.roomdesc = roomdesc;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getNoscore() {
		return noscore;
	}

	public void setNoscore(String noscore) {
		this.noscore = noscore;
	}

	public String getPlaywaycode() {
		return playwaycode;
	}

	public void setPlaywaycode(String playwaycode) {
		this.playwaycode = playwaycode;
	}


	@Transient
	public boolean isStartflag() {
		return startflag;
	}

	public void setStartflag(boolean startflag) {
		this.startflag = startflag;
	}
}
