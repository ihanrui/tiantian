package com.mile.core.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.core.DataConstants;
import com.mile.core.game.Message;
import com.mile.util.UKTools;
import com.mile.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


/**
 * @author jaddy0302 Rivulet User.java 2010-3-17
 *
 */
@Entity
@Table(name = "bm_playuser")
@org.hibernate.annotations.Proxy(lazy = false)
public class PlayUserClient extends BaseEntity implements UserEvent ,Message,  Comparable<PlayUserClient>{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
    @Id
	private String id = UKTools.getUUID().toLowerCase();

	private String username ;

	private String orgi ;
	private String creater;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private Date passupdatetime = new Date();

	private long playerindex ;

	private String command ;	//指令

	private String memo;
	private String city ;	//城市
	private String province ;//省份
	private Boolean login ;		//是否登录
	private Boolean online ; 	//是否在线
	private String status ;		//
	private Boolean datastatus ;//数据状态，是否已删除
	private Boolean headimg ; 	//是否上传头像

	private String roomid ;		//加入的房间ID
	private Boolean roomready ;	//在房间中已经准备就绪
	private Boolean opendeal ;	//明牌

	private String gender;//0.未知 1.男，2.女。

	private String gamestatus ;	//玩家在游戏中的状态 ： READY : NOTREADY : PLAYING ：MANAGED/托管

	private String playertype ;	//玩家类型 ： 玩家：托管玩家，AI

	private String token ;

	private String playerlevel ;//玩家级别 ， 等级
	private Integer experience  ;	//玩家经验


	private String openid ;	//微信
	private String qqid ;


	private Date lastlogintime = new Date();	//最后登录时间

	private Integer fans ;			//粉丝
	private Integer follows ;		//关注
	private Integer integral ;		//积分

	private Integer cards;			//房卡数量
	private Integer goldcoins;		//金币数量
	private Integer diamonds ;		//网豆数量

	/**
	 *对金币+房卡+id进行RSA签名 ， 任何对ID,cards ， goldcoins 进行修改之前，都需要做签名验证，
	 *签名验证通过后才能进行修改，修改之后，重新签名
	 */
	private String sign ;

	private String nickname ;
	private String  alipay;//支付宝
	private String  invcoode;//邀请码
	private String  gameid;//游戏ID
	private String userurl= DataConstants.GAME_USER_DEFAULT_URL;//用户图像URL
	private String idcard;//身份证号码

	private String ip;//用户IP



	private Integer index=-1;//座位号

	private String teanum;//加入茶楼的茶楼号



	// 纬度
	private  Double latitude = 0.0;
	// 经度
	private  Double longitude = 0.0;

	private  String locationdesc = "";





	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getOrgi() {
		return orgi;
	}


	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}


	public String getCreater() {
		return creater;
	}


	public void setCreater(String creater) {
		this.creater = creater;
	}


	public Date getCreatetime() {
		return createtime;
	}


	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}


	public Date getUpdatetime() {
		return updatetime;
	}


	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}


	public Date getPassupdatetime() {
		return passupdatetime;
	}


	public void setPassupdatetime(Date passupdatetime) {
		this.passupdatetime = passupdatetime;
	}


	public String getMemo() {
		return memo;
	}


	public void setMemo(String memo) {
		this.memo = memo;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public Boolean isLogin() {
		return login;
	}


	public void setLogin(Boolean login) {
		this.login = login;
	}


	public Boolean isOnline() {
		return online;
	}


	public void setOnline(Boolean online) {
		this.online = online;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Boolean isDatastatus() {
		return datastatus;
	}


	public void setDatastatus(Boolean datastatus) {
		this.datastatus = datastatus;
	}


	public Boolean isHeadimg() {
		return headimg;
	}


	public void setHeadimg(Boolean headimg) {
		this.headimg = headimg;
	}


	public String getPlayerlevel() {
		return playerlevel;
	}


	public void setPlayerlevel(String playerlevel) {
		this.playerlevel = playerlevel;
	}


	public Integer getExperience() {
		return experience;
	}


	public void setExperience(Integer experience) {
		this.experience = experience;
	}


	public Date getLastlogintime() {
		return lastlogintime;
	}


	public void setLastlogintime(Date lastlogintime) {
		this.lastlogintime = lastlogintime;
	}


	public Integer getFans() {
		return fans;
	}


	public void setFans(Integer fans) {
		this.fans = fans;
	}


	public Integer getFollows() {
		return follows;
	}


	public void setFollows(Integer follows) {
		this.follows = follows;
	}


	public Integer getIntegral() {
		return integral;
	}


	public void setIntegral(Integer integral) {
		this.integral = integral;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public Integer getCards() {
		return cards;
	}


	public void setCards(Integer cards) {
		this.cards = cards;
	}


	public Integer getGoldcoins() {
		return goldcoins;
	}


	public void setGoldcoins(Integer goldcoins) {
		this.goldcoins = goldcoins;
	}


	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public Integer getDiamonds() {
		return diamonds;
	}


	public void setDiamonds(Integer diamonds) {
		this.diamonds = diamonds;
	}


	public String getOpenid() {
		return openid;
	}


	public void setOpenid(String openid) {
		this.openid = openid;
	}


	public String getQqid() {
		return qqid;
	}


	public void setQqid(String qqid) {
		this.qqid = qqid;
	}


	public String getPlayertype() {
		return playertype;
	}


	public void setPlayertype(String playertype) {
		this.playertype = playertype;
	}

	@Transient
	public long getPlayerindex() {
		return playerindex;
	}


	public void setPlayerindex(long playerindex) {
		this.playerindex = playerindex;
	}

	@Transient
	public String getCommand() {
		return command;
	}


	public void setCommand(String command) {
		this.command = command;
	}


	public String getGamestatus() {
		return gamestatus;
	}


	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public String getRoomid() {
		return roomid;
	}


	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public Boolean isRoomready() {
		return roomready;
	}


	public void setRoomready(Boolean roomready) {
		this.roomready = roomready;
	}

	public Boolean isOpendeal() {
		return opendeal;
	}


	public void setOpendeal(Boolean opendeal) {
		this.opendeal = opendeal;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getInvcoode() {
		return invcoode;
	}

	public void setInvcoode(String invcoode) {
		this.invcoode = invcoode;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public String getUserurl() {
//		if (StringUtils.isEmpty(this.getUserurl()))
//			return DataConstants.GAME_USER_DEFAULT_URL;
//		else
			return userurl;
	}

	public void setUserurl(String userurl) {
		this.userurl = userurl;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	@Override
	public int compareTo(PlayUserClient o) {
		return (int) (this.playerindex - o.getPlayerindex());
	}


	@Transient
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	@Transient
	public String getTeanum() {
		return teanum;
	}

	public void setTeanum(String teanum) {
		this.teanum = teanum;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getLocationdesc() {
		return locationdesc;
	}

	public void setLocationdesc(String locationdesc) {
		this.locationdesc = locationdesc;
	}



}
