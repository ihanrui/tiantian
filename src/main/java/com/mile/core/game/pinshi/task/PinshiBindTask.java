package com.mile.core.game.pinshi.task;

import com.mile.common.cache.CacheHelper;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.BankerInfo;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.util.PinshiRoomUtil;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.SysdicUtil;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.util.GameUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;

/**
 * 定庄
 */
public class PinshiBindTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(PinshiBindTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public PinshiBindTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {
        if (null !=gameRoom ) {
            PinshiBoard board = (PinshiBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            List<PinshiPlayer> pinshiPlayerList= board.getHogplayers();

            if (1 == pinshiPlayerList.size()) {
                board.setBanker(pinshiPlayerList.get(0).getPlayuser());
                pinshiPlayerList.get(0).setBanker(true);
            } else {
                int randomindex = new Random().nextInt(pinshiPlayerList.size() - 1);
                board.setBanker(pinshiPlayerList.get(randomindex).getUserid());
                pinshiPlayerList.get(randomindex).setBanker(true);


            }

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(),board,gameRoom.getOrgi());
            PinshiPlayer banker = (PinshiPlayer)board.player(board.getBanker());

            int bankertimes = ( 0 == banker.getHogtimes()?1:banker.getHogtimes());
            banker.setBankertimes(bankertimes);

            for(Player player:board.getPlayers()) {

                BankerInfo bankerInfo = new BankerInfo(pinshiPlayerList, board.getBanker(), RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_BASESCORE));
                SummaryPlayer summaryPlayer = getSummaryPlayerByUserid(gameRoom, player.getUserid());
                bankerInfo.setCommand(PinshiDataConstants.GAME_EVENT_BIND);



                if (null != summaryPlayer && "Y".equals(summaryPlayer.getBolusflag())) {
                    bankerInfo.setPushscore(summaryPlayer.getBolustimes()+"");
                }

                bankerInfo.setBankertimes(banker.getBankertimes());


                if(bankerInfo.getBankertimes() <= 0 ){
                    bankerInfo.setBankertimes(1);
                    banker.setBankertimes(1);
                }

                bankerInfo.setBetsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BET.toString()));

                //推注的后面在处理
                ActionTaskUtils.sendEvent(player.getUserid(), bankerInfo);
            }


            for(Player player:board.getPlayers()){
                PinshiPlayer pinshiPlayer = (PinshiPlayer)player;
                pinshiPlayer.setGamestep(PinshiDataConstants.Gamestep.BET.toString());
            }

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());


            //定庄后 闲家下注,下注完成就发牌。
//            GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.DEAL1.toString(),
//                    SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BET.toString()));    //通知状态机 , 此处应由状态机处理异步执行

        }




    }


    private SummaryPlayer getSummaryPlayerByUserid(GameRoom gameRoom, String userid) {

        if (null != gameRoom.getSummary() && null != gameRoom.getSummary().getSummaryPlayer()) {
            List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();

            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                if (summaryPlayer.getUserid().equals(userid)) {
                    return summaryPlayer;
                }

            }
            return null;
        } else
            return null;

    }
}
