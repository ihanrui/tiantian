package com.mile.core.game.pinshi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.DataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.util.MaziInnertestUtil;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.util.RoomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 发牌工具类
 */
public class PinshidealingUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshidealingUtil.class);




    /**
     * 处理牌局开始时的发牌，摸对等处理。
     *
     * @param gameRoom
     * @param banker 上一把的庄家
     * @param players
     * @param cards
     * @param board
     */
    public static void dealBeginCards(GameRoom gameRoom,Player[] players, List<Byte> cards, Board board, List<PlayUserClient> playUsers) {

        int winindex = 0;

        logger.info("庄家的座位号  winindex [" + winindex + "]");


        int cardnum = 5 * players.length;
        //拼十发牌
        for (int i = 0; i < 5 * players.length; i++) {
            int pos = (i + winindex) % players.length;
            Player ptemp = players[pos];
            byte cardtemp  = cards.get(i);
            logger.info("拼十发牌 i["+i+"]  players.length["+players.length+"] cardtemp["+cardtemp+"]");
            ptemp.getCards()[i / players.length] = cardtemp;
        }


//        for (Player p: players) {
//            p.getCards()[4]=-1;
//        }

        logger.info("------------玩家牌局信息-----------------");
        for (Player p: players) {
            logger.info("玩家 [" + p.getUsername() + "][" + p.getGameid() + "] 牌[" + p.getCards()[0] + "," + p.getCards()[1] + "," + p.getCards()[2] + "," + p.getCards()[3] + "," + p.getCards()[4] + "]");
            logger.info("玩家 [" + p.getUsername() + "][" + p.getGameid() + "] 牌[" + p.getCards()+"]");

        }
        board.setGameroomid(gameRoom.getId());
        board.setRoomid(gameRoom.getRoomid());
        PinshiBoard pinshiBoard = (PinshiBoard)board;

        List<Byte> linkList = new LinkedList<Byte>();
        linkList.addAll(cards.subList(cardnum,cards.size()));


        String cardstr= "整局的牌信息 cards[";
        logger.info("整局的牌信息 cards[");
        for(byte b:cards){
            cardstr+=b+",";
        }
        logger.info(cardstr+"]");

        pinshiBoard.setLeftcards(linkList);


        String lstr= "整局的牌信息 cards[";
        logger.info("剩下牌信息 cards[");
        for(byte b:linkList){
            lstr+=b+",";
        }
        logger.info(lstr+"]");

        for (Player p : players) {
            logger.info("拼十 发牌位置 玩家 [" + p.getUsername() + "][" + p.getGameid() + "] 位置[" + p.getIndex() + "]");
            MaziSummaryUtil.dealHisplayer(p, gameRoom);

        }



    }

}