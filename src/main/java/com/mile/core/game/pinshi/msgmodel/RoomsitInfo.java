package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.entity.PlayUserClient;

import java.util.List;

/**
 * Created by michael on 2018/6/12.
 */
public class RoomsitInfo extends BaseEvent {

    private PlayUserClient playUserClient;//坐下的人

    public RoomsitInfo(PlayUserClient playUserClient) {
        this.playUserClient = playUserClient;
    }

    public PlayUserClient getPlayUserClient() {
        return playUserClient;
    }

    public void setPlayUserClient(PlayUserClient playUserClient) {
        this.playUserClient = playUserClient;
    }
}
