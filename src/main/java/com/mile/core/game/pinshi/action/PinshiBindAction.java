package com.mile.core.game.pinshi.action;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.mazi.task.CreateMaziCleanTask;
import com.mile.core.game.pinshi.task.PinshiBindTask;
import com.mile.core.statemachine.action.Action;
import com.mile.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.mile.core.statemachine.message.Message;
import org.apache.commons.lang3.StringUtils;

/**
 * 总结算00
 *
 * @param <T>
 * @param <S>
 * @author iceworld
 */
public class PinshiBindAction<T, S> implements Action<T, S> {

    @Override
    public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
        String room = (String) message.getMessageHeaders().getHeaders().get("room");
        if (!StringUtils.isBlank(room)) {
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI);
            if (gameRoom != null) {
                Double interval = new Double(message.getMessageHeaders().getHeaders().get("interval").toString());

                if (null == interval) {
                    interval = 0.0;
                }

                CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new PinshiBindTask(interval, gameRoom, gameRoom.getOrgi()));
            }
        }
    }
}
