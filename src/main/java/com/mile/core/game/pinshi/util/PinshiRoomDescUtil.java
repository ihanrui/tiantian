package com.mile.core.game.pinshi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.JoinRoom;
import com.mile.core.game.pinshi.msgmodel.PinshiJoinRoom;
import com.mile.core.game.pinshi.msgmodel.PinshiUserBoard;
import com.mile.core.game.util.RoomUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

/**
 * 房间玩法描述工具类
 */
public class PinshiRoomDescUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshiRoomDescUtil.class);


    public static JoinRoom formatRoomdesc(GameRoom gameRoom, JoinRoom joinRoom) {
        if (null == gameRoom || null == joinRoom)
            return joinRoom;
        joinRoom.setNowgamenum(""+gameRoom.getCurrentnum());//当前局数

        joinRoom.setPlaywaydesc("明牌抢庄");//玩法
        joinRoom.setRoomdesc(MaziTipsUtil.getRoomdesc(gameRoom));
        joinRoom.setGamestatus(gameRoom.getStatus());

        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if(null !=  playerList && !playerList.isEmpty()) {
            joinRoom.setCurpalyers(playerList.size());
        }else{
            joinRoom.setCurpalyers(0);
        }

        PinshiJoinRoom pinshiJoinRoom = new PinshiJoinRoom();

        UKTools.copyProperties(joinRoom,pinshiJoinRoom);
        pinshiJoinRoom.setPlaywaycode(gameRoom.getPlaywaycode());
        pinshiJoinRoom.setBasescore(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_BASESCORE));
        pinshiJoinRoom.setAdditional(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_ADDITIONAL));
        pinshiJoinRoom.setGamenumbers(Integer.parseInt(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_GAMENUMBERS)));
        pinshiJoinRoom.setPinshirule(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_PINSHIRULE));
        pinshiJoinRoom.setSenior(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_SENIOR));
        pinshiJoinRoom.setSpecialcard(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_SPECIALCARD));


        pinshiJoinRoom.setCreator(gameRoom.getCreater());

        return pinshiJoinRoom;
    }






    public static void formatRoomdesc(GameRoom gameRoom, PinshiUserBoard pinshiUserBoard) {
        if (null == gameRoom || null == pinshiUserBoard)
            return;
        pinshiUserBoard.setNowgamenum(""+gameRoom.getCurrentnum());
        pinshiUserBoard.setPlaywaydesc("明牌抢庄");
        pinshiUserBoard.setRoomdesc(MaziTipsUtil.getRoomdesc(gameRoom));
        pinshiUserBoard.setGamestatus(gameRoom.getStatus());


        pinshiUserBoard.setBasescore(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_BASESCORE));
        pinshiUserBoard.setAdditional(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_ADDITIONAL));
        pinshiUserBoard.setGamenumbers(Integer.parseInt(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_GAMENUMBERS)));
        pinshiUserBoard.setPinshirule(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_PINSHIRULE));
        pinshiUserBoard.setSenior(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_SENIOR));
        pinshiUserBoard.setSpecialcard(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),PinshiDataConstants.CONT_ROOM_SPECIALCARD));



        gameRoom.setNowgamenum(""+gameRoom.getCurrentnum());
        gameRoom.setPlaywaydesc("明牌抢庄");
        gameRoom.setRoomdesc(MaziTipsUtil.getRoomdesc(gameRoom));



    }






}