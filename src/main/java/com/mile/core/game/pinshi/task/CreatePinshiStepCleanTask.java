package com.mile.core.game.pinshi.task;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.eventmodel.StepSumEvent;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.msgmodel.Pinshicardtype;
import com.mile.core.game.pinshi.util.PinshiScoreUtil;
import com.mile.core.game.util.SysdicUtil;
import com.mile.core.record.entity.Summary;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 一小局牌局结算
 */
public class CreatePinshiStepCleanTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(CreatePinshiStepCleanTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreatePinshiStepCleanTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {

        try {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            board.setFinished(true);
            GamePlayway gamePlayWay = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(gameRoom.getPlayway(), gameRoom.getOrgi());
            boolean gameOver = false;

            StepSumEvent stepSumEvent = null;
            if (gamePlayWay != null) {
                /**
                 * 结算信息 ， 更新 玩家信息
                 */
                PinshiScoreUtil.analyzeScore(board, gameRoom);//结算信息
                Summary summary = board.stepSummary(board, gameRoom, gamePlayWay);
                logger.info("发送结算信息  庄家为board.getBanker()[" + board.getBanker() + "]");
                stepSumEvent = new StepSumEvent(board.getPlayers(), board.getBanker());
                stepSumEvent.setReadysnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BEGIN.toString()));
                sendEvent(DataConstants.GAME_EVENT_STEPCLEAN, stepSumEvent, gameRoom);    //通知所有客户端结束牌局，进入结算
                MaziScoreUtil.saveStepsum(gameRoom, board.getPlayers());
            }
            for (Player player : board.getPlayers()) {
                PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(player.getPlayuser(), this.orgi);
                if (playUserClient != null) {
                    playUserClient.setGamestatus(BMDataContext.GameStatusEnum.WAITTING.toString());

                    playUserClient.setRoomready(false);

                    CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, gameRoom.getOrgi());

                    //playUserClient = (PlayUserClient) CacheHelper.getGamePlayerCacheBean().getPlayer(player.getPlayuser(), this.orgi);

                    CacheHelper.getGamePlayerCacheBean().put(playUserClient.getId(), playUserClient, gameRoom.getOrgi());

                }

                if (1 == player.getLvl()) {
                    logger.info("房间号[" + gameRoom.getRoomid() + "] 一游Lastwinner [" + player.getUsername() + "]");
                    gameRoom.setLastwinner(player.getPlayuser());
                }

                if (DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())) {
                    PinshiPlayer pinshiPlayer = (PinshiPlayer) player;
                    pinshiPlayer.setGamestep("");

                    logger.info("玩家[" + pinshiPlayer.getUsername() + "][" + pinshiPlayer.getGameid() + "] 保存牌局信息");
                    PinshiScoreUtil.dealPinshihiscard(board, pinshiPlayer);
                }

            }
            gameRoom.setDeductflag("Y");//首局结算，可扣卡

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());

            logger.info("gameroom  [" + gameRoom.getRoomid() + "] gameRoom.getNumofgames() ["
                    + gameRoom.getNumofgames() + "] gameRoom.getCurrentnum()[" + gameRoom.getCurrentnum() + "]");
            if (gameRoom.getNumofgames() <= gameRoom.getCurrentnum() || gameRoom.getNumofgames() >= 999999) {//金币场 也要结算

                logger.info("局数已经满了， 进入结算");
                CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

                GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.CLEARING.toString(), 2);//通知状态机 , 继续执行
                //BMDataContext.getContext().getBean(GameRoomRepository.class).save(gameRoom);
                //BMDataContext.getContext().getBean(MaziBoardRepository.class).save((Board) board);
            } else {
                CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

                logger.info("局数未满， 进入下一盘");
                GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.JOIN.toString(), Double.parseDouble(TeahouseUtil.getConstantsByName(DataConstants.CONST_CLEANSLEEP)));    //通知状态机 , 继续执行

            }

        }catch (Exception e){
            e.printStackTrace();
            logger.error("拼十小结结算失败",e);

        }















    }
}
