package com.mile.core.game.pinshi.util;

import com.mile.core.PinshiDataConstants;
import com.mile.core.game.CardType;
import com.mile.core.game.pinshi.msgmodel.Pinshicardtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by michael on 2018/6/20.
 * <p>
 * 牌型分析
 */
public class PinshiCardtypeUtil {
    private static final Logger logger = LoggerFactory.getLogger(PinshidealingUtil.class);

    public static CardType getCardtype(byte[] cards) {


        if (null == cards || cards.length == 0 || 5 != cards.length) {

            logger.info("Pinsshi Cardtype length is null or not 5[" + cards + "]");
            return new Pinshicardtype(getCardMax(cards), PinshiDataConstants.PinshiCardsTypeEnum.NONE.getType(),cards);

        } else if (isWX(cards)) {//五小牛处理
            logger.info("五小牛");
            return new Pinshicardtype(getCardMax(cards), PinshiDataConstants.PinshiCardsTypeEnum.WX.getType(),cards);
        } else if (isZD(cards)) {//炸弹牛处理
            logger.info("炸弹牛");
            return new Pinshicardtype(getCardSame(cards), PinshiDataConstants.PinshiCardsTypeEnum.ZD.getType(),cards);
        } else if (isHL(cards)) {//葫芦牛处理
            logger.info("葫芦牛");
            return new Pinshicardtype(getCardSame(cards), PinshiDataConstants.PinshiCardsTypeEnum.HL.getType(),cards);
        } else if (isSZ(cards)) {//顺子牛处理
            logger.info("顺子牛");
            return new Pinshicardtype(getCardMax(cards), PinshiDataConstants.PinshiCardsTypeEnum.SZ.getType(),cards);
        } else {//顺子牛处理
            logger.info("其他牛");
            Pinshicardtype pinshicardtype = new Pinshicardtype(getCardMax(cards), getPinshiScore(cards),cards);
            if(pinshicardtype.getCardtype() > 0){
                pinshicardtype.setScorecard(getScorecard(cards));
            }
            return  pinshicardtype;
        }

    }

    /**
     * 是否为5小牛 所有牌均小于5，且所有牌加起来小于等于10
     *
     * @param cards
     * @return
     */
    private static boolean isWX(byte[] cards) {
        int sum = 0;
        boolean flag = true;
        for (Byte b : cards) {
            if (2 > b / 4 || b / 4 == 11 || b / 4 == 12) {
                sum += (b / 4 + 3) % 13;
            } else {
                flag = false;
                break;
            }
        }

        if (flag && sum <= 10) {
            return true;
        }

        return false;
    }

    /**
     * 是否为炸弹牛 有4张数值相同的牌
     *
     * @param cards
     * @return
     */
    private static boolean isZD(byte[] cards) {
        boolean flag = false;

        for (Byte b : cards) {
            int num = 0;
            for (Byte b1 : cards) {
                if (b/4 == b1/4) {
                    num++;
                }
            }

            if (num >= 4)
                flag = true;
        }
        return flag;
    }

    /**
     * 是否为葫芦牛 三张数值相同的牌加上另外两张数值相同的牌。
     *
     * @param cards
     * @return
     */
    private static boolean isHL(byte[] cards) {
        int sum = 0;
        boolean flag2 = false;
        boolean flag3 = false;

        for (Byte b : cards) {
            int num = 0;
            for (Byte b1 : cards) {
                if (b / 4 == b1 / 4)
                    num++;
            }

            if (num == 2)
                flag2 = true;
            if (num == 3)
                flag3 = true;
        }
        return flag2 && flag3;
    }

    /**
     * 是否为五花牛 五张牌都由J,Q,K中的任意一张组成。
     *
     * @param cards
     * @return
     */
    private static boolean isWH(byte[] cards) {
        int sum = 0;
        boolean flag = true;

        for (Byte b : cards) {
            if (b / 4 < 8 || b / 4 > 10) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 是否为同花 五张牌的花色相同。
     *
     * @param cards
     * @return
     */
    private static boolean isTH(byte[] cards) {
        int sum = 0;
        boolean flag = true;

        int h = cards[0] % 4;
        for (Byte b : cards) {
            if (h != b % 4) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 是否为顺子 五张连续的牌。
     *
     * @param cards
     * @return
     */
    private static boolean isSZ(byte[] cards) {
        int sum = 0;
        boolean flag = true;

        int h = cards[0] % 4;

        Arrays.sort(cards);
        for (int i = 0; i < cards.length - 1; i++) {
            int b =  ( cards[i] / 4 + 3 )%13;

            if(b == 0)
                b =  13;


            int e =  ( cards[i+1] / 4 + 3 )%13;

            if(e == 0)
                e =  13;
            logger.info("b["+b+"]e["+e+"] i["+i+"]");
            if ( ! (b+1 == e || e+4 == b || (i ==3 && e==1 && b == 13))) {
                flag = false;
                break;
            }
        }

        /*if((cards[4] / 4+3) %13 == 2 && flag  ) {//排除J,Q,K,A,2
            flag = false;
        }*/
        return flag;
    }

    /**
     * 获取拼十的点数。
     *
     * @param cards
     * @return
     */
    private static int getPinshiScore(byte[] cards) {
        int score = 0;
        boolean flag = true;

        int h = cards[0] % 4;

        Arrays.sort(cards);

        //int cards[] = {9,9,2,1,2};
        int s = 0;
        Map<Integer, Integer> dict = new HashMap<Integer, Integer>();

        for (int i = 0; i < cards.length; i++) {
            int ci = getCardInt(cards[i]);
            s += ci;
            dict.put(ci, dict.containsKey(ci) ? dict.get(ci) + 1 : 1);
        }
        logger.info("总点数为  s["+s+"]");
        int point = s % 10;

        if(point == 0){
            point = 10;
        }

        boolean exists = false;
        for (Integer i : dict.keySet()) {
            Integer other = (10 + point - i) % 10;

            if(other == 0){
                other = 10;
            }

            if (dict.containsKey(other)) {
                if ((other == i && dict.get(other) >= 2) || (other != i && dict.get(other) >= 1)) {
                    exists = true;
                    break;
                }
            }
        }
        return (exists ? point : 0);
    }




    /**
     * 获取点数的牌。
     *
     * @param cards
     * @return
     */
    public static byte[] getScorecard(byte[] cards) {
        int score = 0;
        boolean flag = true;

        byte[] ret = new byte[2];

        int[] intret = new int[2];


        int s = 0;
        Map<Integer, Integer> dict = new HashMap<Integer, Integer>();

        for (int i = 0; i < cards.length; i++) {
            int ci = getCardInt(cards[i]);
            s += ci;
            dict.put(ci, dict.containsKey(ci) ? dict.get(ci) + 1 : 1);
        }
        int point = s % 10;

        if(point == 0){
            point = 10;
        }

        boolean exists = false;
        for (Integer i : dict.keySet()) {
            Integer other = (10 + point - i) % 10;
            if(other == 0 )
                other = 10;
            if (dict.containsKey(other)) {
                if ((other == i && dict.get(other) >= 2) || (other != i && dict.get(other) >= 1)) {

                    if(i == 0)
                        intret[0] = 10;
                    else
                        intret[0] = i;

                    if(i == 0)
                        intret[1] = 10;
                    else
                        intret[1] = other;


                    intret[1] = other;
                    exists = true;
                    break;
                }
            }
        }



        for (int i = 0,j=0; i < cards.length&&j<ret.length; i++) {
            int ci = getCardInt(cards[i]);
            if(intret[0] == ci ){
                ret[j++] =cards[i];
                intret[0]=-1;
                continue;
            }

            if(intret[1] == ci ){
                ret[j++] =cards[i];
                intret[1] = -1;
                continue;
            }
        }
        return ret;
    }

    private static int getCardInt(byte b) {

        int ret = -1;
        if (b / 4 >= 7 && b / 4 <= 10) {
            ret= 10;
        } else if (b / 4 > 10) {
            ret = (b / 4) % 10;
        } else {
            ret = ((b / 4) + 3) % 10;
        }
        System.out.println("b["+b+"]对应的牌为--"+ret);
        return ret;
    }

    private static byte getCardMax(byte[] bytes) {
        byte max = 0;
        int s = 0;//点数
        int h = 0;//花色  黑红梅芳
        for (byte b : bytes) {
            if (b / 4 <= 10 && (b / 4 > s || (b / 4 == s && b % 4 < h))) {
                s = b / 4;
                h = b % 4;
                max = b;
            }
        }

        return max;
    }

    private static byte getCardSame(byte[] bytes) {
        Map<Integer, Integer> dict = new HashMap<Integer, Integer>();

        int max = 0;
        byte maxcard = 0;

        for (int i = 0; i < bytes.length; i++) {

            int ci = getCardInt(bytes[i]);
            int sum = dict.containsKey(ci) ? dict.get(ci) + 1 : 1;

            if (sum > max) {
                max = sum;
                maxcard = bytes[i];
            }

            dict.put(ci, sum);
        }
        return maxcard;
    }



    public static int compare(Pinshicardtype a,Pinshicardtype b){
        if(a.getCardtype() > b.getCardtype()){
            return 1;
        }else if(a.getCardtype() < b.getCardtype()){
            return -1;
        }else if(a.getCardtype() == b.getCardtype()){
            int sa= (a.getMaxcard()/4+3)%13;
            if(sa == 0)
                sa = 13;
            int sb= (b.getMaxcard()/4+3)%13;
            if(sb == 0)
                sb = 13;

            int ha = a.getMaxcard() %4;
            int hb = b.getMaxcard() %4;
            if( sa > sb ){
                return 1;
            }else  if( sa < sb ){
                return -1;
            }else if(sa == sb){
                return ha<hb?1:-1;
            }
        }

        logger.info("比较拼十类型出错");
        return 0;
    }



}
