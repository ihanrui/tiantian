package com.mile.core.game.pinshi.handler;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.Token;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiClient;
import com.mile.core.game.mazi.handler.MaziGameEventHandler;
import com.mile.core.game.mazi.util.MaziInnertestUtil;
import com.mile.core.game.msgmodel.TestEvent;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by michael on 2018/6/11.
 */
public class PinshiGameEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(PinshiGameEventHandler.class);

    protected SocketIOServer server;

    @Autowired
    public PinshiGameEventHandler(SocketIOServer server) {
        this.server = server;
    }

    //进入房间坐下
    @OnEvent(value = PinshiDataConstants.GAME_EVENT_ROOMSIT)
    public void onRoomsit(SocketIOClient client, String data) {

        try {

            logger.info("进入房间坐下 参数 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

            BMDataContext.getPinshiGameEngine().roomsit(beiMiClient);
        } catch (Exception e) {
            logger.error("进入房间坐下失败", e);
        }
    }

    //房主开始
    @OnEvent(value = PinshiDataConstants.GAME_EVENT_GAMESTART)
    public void onGamestart(SocketIOClient client, String data) {
        try {
            logger.info("房主开始 参数 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            BMDataContext.getPinshiGameEngine().gamestart(beiMiClient);
        } catch (Exception e) {
            logger.error("房主开始 失败", e);
        }
    }

    //抢庄
    @OnEvent(value = PinshiDataConstants.GAME_EVENT_HOG)
    public void onHog(SocketIOClient client, String data) {

        logger.info("抢庄 参数 传入  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        BMDataContext.getPinshiGameEngine().hog(beiMiClient);
    }

    //闲家下注
    @OnEvent(value = PinshiDataConstants.GAME_EVENT_BET)
    public void onBet(SocketIOClient client, String data) {
        try {
            logger.info("闲家下注 参数 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            BMDataContext.getPinshiGameEngine().bet(beiMiClient);
        } catch (Exception e) {
            logger.error("闲家下注 失败", e);
        }
    }

    //玩家亮牌
    @OnEvent(value = PinshiDataConstants.GAME_EVENT_SHOW)
    public void onShow(SocketIOClient client, String data) {
        try {
            logger.info("亮牌 参数 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            BMDataContext.getPinshiGameEngine().show(beiMiClient);
        } catch (Exception e) {
            logger.error("玩家亮牌 失败", e);
        }
    }

}
