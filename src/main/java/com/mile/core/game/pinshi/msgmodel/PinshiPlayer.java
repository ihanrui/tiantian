package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.DataConstants;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import com.mile.util.UKTools;
import com.mile.util.rules.model.Action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;




public class PinshiPlayer extends Player{

    /**
     *
     */
    private static final long serialVersionUID = 1L;




    public PinshiPlayer() {

    }

    public PinshiPlayer(String id, int index) {
        super(id,index);

    }


    private int hogtimes=0;//抢庄倍数

    private int bankertimes=0;//庄家倍数

    private String hogflag;//是否已抢庄

    private int bettimes=0;//闲家下注倍数

    private String betflag;//是否已下注

    private byte lastcard;//最后一张牌


    private String gamestep;//游戏阶段

    private Pinshicardtype pinshicardtype;//牌型


    private String showflag;//是否已亮牌

    private String pushflag;//是否可以推注

    private int pushscore=0;//推注的分数




    public int getHogtimes() {
        return hogtimes;
    }

    public void setHogtimes(int hogtimes) {
        this.hogtimes = hogtimes;
    }

    public int getBettimes() {
        return bettimes;
    }

    public void setBettimes(int bettimes) {
        this.bettimes = bettimes;
    }

    public byte getLastcard() {
        return lastcard;
    }

    public void setLastcard(byte lastcard) {
        this.lastcard = lastcard;
    }

    public String getHogflag() {
        return hogflag;
    }

    public void setHogflag(String hogflag) {
        this.hogflag = hogflag;
    }

    public String getBetflag() {
        return betflag;
    }

    public void setBetflag(String betflag) {
        this.betflag = betflag;
    }


    public String getGamestep() {
        return gamestep;
    }

    public void setGamestep(String gamestep) {
        this.gamestep = gamestep;
    }

    public Pinshicardtype getPinshicardtype() {
        return pinshicardtype;
    }

    public void setPinshicardtype(Pinshicardtype pinshicardtype) {
        this.pinshicardtype = pinshicardtype;
    }

    public String getShowflag() {
        return showflag;
    }

    public void setShowflag(String showflag) {
        this.showflag = showflag;
    }

    public String getPushflag() {
        return pushflag;
    }

    public void setPushflag(String pushflag) {
        this.pushflag = pushflag;
    }

    public int getPushscore() {
        return pushscore;
    }

    public void setPushscore(int pushscore) {
        this.pushscore = pushscore;
    }

    public int getBankertimes() {
        return bankertimes;
    }

    public void setBankertimes(int bankertimes) {
        this.bankertimes = bankertimes;
    }
}
