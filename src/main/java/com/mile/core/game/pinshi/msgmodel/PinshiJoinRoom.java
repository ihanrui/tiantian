package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.GameEvent;
import com.mile.core.game.Message;
import com.mile.core.game.msgmodel.JoinRoom;
import com.mile.util.UKTools;

import java.util.List;

/**
 * 拼十加入房间信息载体
 */
public class PinshiJoinRoom extends JoinRoom{
	public  PinshiJoinRoom(){

	}


	private String basescore;//底分描述

	//maxplayers 人数
	//玩法 playwaydesc
	//支付方式 房费 tips
	private String tips;

	private int gamenumbers;//局数

	private String pinshirule;//翻倍规则

	private String additional;//其他规则

	private String specialcard;//特殊牌型

	private String  senior;//高级玩法

	public String getBasescore() {
		return basescore;
	}

	public void setBasescore(String basescore) {
		this.basescore = basescore;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

	public int getGamenumbers() {
		return gamenumbers;
	}

	public void setGamenumbers(int gamenumbers) {
		this.gamenumbers = gamenumbers;
	}

	public String getPinshirule() {
		return pinshirule;
	}

	public void setPinshirule(String pinshirule) {
		this.pinshirule = pinshirule;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getSpecialcard() {
		return specialcard;
	}

	public void setSpecialcard(String specialcard) {
		this.specialcard = specialcard;
	}

	public String getSenior() {
		return senior;
	}

	public void setSenior(String senior) {
		this.senior = senior;
	}
}
