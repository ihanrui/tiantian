package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

/**
 * 发最后一张牌的事件
 * Created by michael on 2018/6/12.
 */
public class DealingInfo extends BaseEvent {

    private String userid;

    private byte[] cards;//所有的牌

    private byte lastcard;//最后一张牌

    private int showsnd;//亮牌时间

    public DealingInfo(String userid, byte[] cards, byte lastcard) {
        this.userid = userid;
        this.cards = cards;
        this.lastcard = lastcard;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public byte getLastcard() {
        return lastcard;
    }

    public void setLastcard(byte lastcard) {
        this.lastcard = lastcard;
    }

    public int getShowsnd() {
        return showsnd;
    }

    public void setShowsnd(int showsnd) {
        this.showsnd = showsnd;
    }
}
