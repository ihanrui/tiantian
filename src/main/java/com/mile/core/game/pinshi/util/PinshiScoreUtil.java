package com.mile.core.game.pinshi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.config.web.model.Game;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.eventmodel.UserinfoEvent;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.msgmodel.Pinshicardtype;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.record.entity.Hiscard;
import com.mile.core.record.entity.Pinshihiscard;
import com.mile.core.record.entity.Stepsum;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.jpa.HiscardRepository;
import com.mile.core.record.jpa.PinshihiscardRepository;
import com.mile.core.record.jpa.StepsumRepository;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by michael on 3/18/18.
 */
public class PinshiScoreUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshiScoreUtil.class);






    public static void analyzeScore(Board board,GameRoom gameRoom) {


        String banker = board.getBanker();

        PinshiPlayer bankerplay = (PinshiPlayer)board.player(banker);

        bankerplay.setStepscore(0);


        logger.info(" 小结 庄家为  bankerplay ["+bankerplay+"]");


        Player[] playUsers = board.getPlayers();

        for(Player player : playUsers){
            PinshiPlayer pinshiPlayer = (PinshiPlayer)player;

            logger.info("闲家信息  pinshiPlayer["+pinshiPlayer+"]");

            if(bankerplay.getUserid().equals(pinshiPlayer.getUserid())){
                logger.info("庄家自己跳过 pinshiPlayer.getUsername() ["+pinshiPlayer.getUsername()+"]  bankerplay.getUsername() ["+bankerplay.getUsername()+"]");
                continue;
            }
            int r = PinshiCardtypeUtil.compare(pinshiPlayer.getPinshicardtype(),bankerplay.getPinshicardtype());

            int score = r * getScore(r,bankerplay,pinshiPlayer,gameRoom);



            logger.info("闲家 输赢 score["+score+"]");
            pinshiPlayer.setStepscore(score);
            pinshiPlayer.setTotalscore(pinshiPlayer.getTotalscore() + pinshiPlayer.getStepscore());
            pinshiPlayer.setWinflag(pinshiPlayer.getStepscore() > 0);

            bankerplay.setStepscore(bankerplay.getStepscore()-score);


        }
        logger.info("庄家 输赢 bankerplay.getStepscore()["+bankerplay.getStepscore()+"]");
        bankerplay.setWinflag(bankerplay.getStepscore() > 0);
        bankerplay.setTotalscore(bankerplay.getTotalscore() + bankerplay.getStepscore());
    }



    private static int getScore(int r,PinshiPlayer banker, PinshiPlayer player, GameRoom gameRoom){

        int times = 1;
        if(r>0){
            logger.info("闲家赢了");
            times = getConstantsByName(gameRoom,player.getPinshicardtype().getCardtype()+"");
        }else{
            logger.info("庄家赢了");
            times = getConstantsByName(gameRoom,banker.getPinshicardtype().getCardtype()+"");
        }

        logger.info("庄家["+banker.getUsername()+"]["+banker.getGameid()+"]  闲家["+player.getUsername()+"]["+player.getGameid()+"]  " +
                "   times ["+times+"] banker.getBankertimes()["+banker.getBankertimes()+"] player.getBettimes()["+player.getBettimes()+"]");
        int score = times * banker.getBankertimes() * player.getBettimes();


       return score;
    }






    public static SummaryPlayer formatStepSum(SummaryPlayer summaryPlayer, Player player, GameRoom gameRoom) {
        PinshiPlayer pinshiPlayer = (PinshiPlayer)player;
        if (null == summaryPlayer) {
            summaryPlayer = new SummaryPlayer();
            summaryPlayer.setUserid(player.getPlayuser());
            summaryPlayer.setUsername(player.getUsername());
            summaryPlayer.setUserurl(player.getUserurl());
            summaryPlayer.setGameid(player.getGameid());
            summaryPlayer.setRoomid(gameRoom.getRoomid());
            summaryPlayer.setGameroomid(gameRoom.getId());
            summaryPlayer.setTablenum(gameRoom.getTablenum());
            summaryPlayer.setTeanum(gameRoom.getTeanum());
            summaryPlayer.setBegindate(gameRoom.getCreatetime());
            summaryPlayer.setEnddate(new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        }

        summaryPlayer.setBankersum(summaryPlayer.getBankersum() + (pinshiPlayer.isBanker() ? 1 : 0));//当庄次数
        summaryPlayer.setWinsum(summaryPlayer.getWinsum() + ("Y".equals(pinshiPlayer.getPushflag()) ? 1 : 0));//推注次数
        summaryPlayer.setLvlsum(summaryPlayer.getLvlsum() + ((pinshiPlayer.getHogtimes()>0) ? 1 : 0));//抢庄次数




        summaryPlayer.setTotalscore(player.getTotalscore());
        summaryPlayer.setScoreunit(gameRoom.getScoreunit());
        if (null != gameRoom.getScoreunit())
            summaryPlayer.setCoins(summaryPlayer.getTotalscore() * gameRoom.getScoreunit());

        //summaryPlayer.setLvlsum(summaryPlayer.getLvlsum() + (player.getLvl() == 1 ? 1 : 0));
        //summaryPlayer.setNewssum(summaryPlayer.getNewssum() + (player.getNewsscore() > 0 ? 1 : 0));
        summaryPlayer.setMaxscore(summaryPlayer.getMaxscore() + (("Y".equals(pinshiPlayer.getHogflag()) ? 1 : 0)));



        if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
            logger.info("拼十推注处理  玩家["+player.getUsername()+"] player.isBanker()["+player.isBanker() +"] player.getStepscore()["+player.getStepscore()+"] summaryPlayer.getBolusflag()["+summaryPlayer.getBolusflag()+"]");

            if(!player.isBanker() && player.getStepscore() > 0 && !"Y".equals(summaryPlayer.getBolusflag())){
                logger.info("玩家["+player.getUsername()+"]["+player.getGameid()+"] 非庄家才能推注 而且赢了 才能推注");
                summaryPlayer.setBolusflag("Y");
                summaryPlayer.setBolustimes(PinshiTipUtil.getBolustimes(gameRoom,(PinshiPlayer) player,summaryPlayer));

            }else{
                summaryPlayer.setBolusflag("N");
                summaryPlayer.setBolustimes(0);

            }
        }
        return summaryPlayer;

    }



    public static int getConstantsByName(GameRoom gameRoom,String name) {
        try {
            logger.info("获取倍率参数 name["+name+"]");

            String paramname = "";

            if(StringUtils.isEmpty(name)){
                logger.info("name为空，默认一倍");
                return 1;
            }

            paramname = PinshiDataConstants.RULETO+"_"+name;
            logger.info("通用获取倍率参数 paramname["+paramname+"]");

            String constr = TeahouseUtil.getConststr(paramname, PinshiDataConstants.COM_PINSHI_RULETO);

            if(!StringUtils.isEmpty(constr)){
                logger.info("通用倍率为 constr["+constr+"]");
                return Integer.parseInt(constr);
            }


            paramname = RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_PINSHIRULE)+"_"+name;
            logger.info("房间参数获取倍率参数 paramname["+paramname+"]");

            constr = TeahouseUtil.getConststr(paramname, PinshiDataConstants.COM_PINSHI_RULETO);

            if(!StringUtils.isEmpty(constr)){
                logger.info("房间参数倍率为 constr["+constr+"]");
                return Integer.parseInt(constr);
            }


            logger.info("未找到任何倍率值name["+name+"] 默认返回1");
            return 1;



        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取游戏常量[" + name + "]失败", e);
            return 1;
        }
    }

    /**
     *
     * 保存用户牌局信息
     * @param board
     * @param pinshiPlayer
     */
    public static void dealPinshihiscard(Board board,PinshiPlayer pinshiPlayer) {

        try {
            board.setCardorder(board.getCardorder() + 1);

            Pinshihiscard pinshihiscard = new Pinshihiscard();
            UKTools.copyProperties(pinshiPlayer, pinshihiscard);

            String scorecardstr = "";
            if(null != pinshiPlayer.getPinshicardtype().getScorecard() && pinshiPlayer.getPinshicardtype().getScorecard().length>1) {
                scorecardstr = pinshiPlayer.getPinshicardtype().getScorecard()[0] + "," + pinshiPlayer.getPinshicardtype().getScorecard()[1];
            }

            pinshihiscard.setScorecard(scorecardstr);

            pinshihiscard.setCurrentnum(board.getCurrentnum());

            pinshihiscard.setCardtype(pinshiPlayer.getPinshicardtype().getCardtype());

            BMDataContext.getContext().getBean(PinshihiscardRepository.class).save(pinshihiscard);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("保存用户牌局信息失败",e);
        }

    }

}