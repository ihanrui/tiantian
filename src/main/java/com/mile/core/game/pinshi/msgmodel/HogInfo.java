package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

import java.util.List;

/**
 * Created by michael on 2018/6/12.
 */
public class HogInfo extends BaseEvent {

    private String userid;

    private int hogtimes;//0.暗枪 1，2，3，4 抢庄倍数  -1.未抢庄

    public HogInfo(String userid, int hogtimes) {
        this.userid = userid;
        this.hogtimes = hogtimes;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getHogtimes() {
        return hogtimes;
    }

    public void setHogtimes(int hogtimes) {
        this.hogtimes = hogtimes;
    }
}
