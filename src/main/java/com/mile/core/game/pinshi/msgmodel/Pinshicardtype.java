package com.mile.core.game.pinshi.msgmodel;

import com.mile.core.game.CardType;

import java.util.List;

/**
 * Created by michael on 2018/6/13.
 */
public class Pinshicardtype extends CardType {

    private  static final long serialVersionUID = 1L;

    //最大的牌 maxcard
    //牌型 cardtype

    private byte lastcard;//最后一张牌

    private byte[] pinshicard;//3张可以有十的牌

    private byte[] scorecard;//算点数的牌

    private byte[] cards;//所有牌

    public Pinshicardtype(int maxcard, int cardtype, byte lastcard, byte[] pinshicard, byte[] scorecard) {
        super(maxcard, cardtype);
        this.lastcard = lastcard;
        this.pinshicard = pinshicard;
        this.scorecard = scorecard;
    }

    public Pinshicardtype(int maxcard, int cardtype) {
        super(maxcard, cardtype);
    }

    public Pinshicardtype() {
    }

    public Pinshicardtype(int maxcard, int cardtype, byte[] cards) {
        super(maxcard, cardtype);
        this.cards = cards;
    }

    public Pinshicardtype(int maxcard, int cardtype, byte[] cards,byte lastcard) {
        this(maxcard, cardtype,cards);
        this.lastcard = lastcard;
    }

    public byte getLastcard() {
        return lastcard;
    }

    public void setLastcard(byte lastcard) {
        this.lastcard = lastcard;
    }



    public byte[] getPinshicard() {
        return pinshicard;
    }

    public void setPinshicard(byte[] pinshicard) {
        this.pinshicard = pinshicard;
    }

    public byte[] getScorecard() {
        return scorecard;
    }

    public void setScorecard(byte[] scorecard) {
        this.scorecard = scorecard;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }
}
