package com.mile.core.game.pinshi;

import com.mile.core.BMDataContext;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.iface.ChessGame;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.NextPlayer;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.util.PinshiRoomUtil;
import com.mile.core.game.pinshi.util.PinshidealingUtil;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PinshiGame implements ChessGame {

    private static final Logger logger = LoggerFactory.getLogger(PinshiGame.class);

    /**
     * 发牌
     *
     * @return
     */
    public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker, int cardsnum) {

        try {
            gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
            gameRoom.setStartflag(true);
            Board board = new PinshiBoard();
            board.setCards(null);
            board.setCurrentnum(gameRoom.getCurrentnum());

            List<Byte> temp = new ArrayList<Byte>();

            int cardnum = 52;

            if (PinshiRoomUtil.isLaizi(gameRoom)) {
                logger.info("房间[" + gameRoom.getRoomid() + "] 有癞子");
                cardnum = 54;
            }

            for (int i = 0; i < cardnum; i++) {
                temp.add((byte) i);
            }

            if (RoomUtil.isShuffle()) {
                for (int i = 0; i < playway.getShuffletimes() + 1; i++) {
                    Collections.shuffle(temp);
                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, j, new Random().nextInt(cardnum));
                        ;
                    }

                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, cardnum - j - 1, new Random().nextInt(cardnum));
                        ;
                    }

                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, new Random().nextInt(cardnum), new Random().nextInt(cardnum));
                        ;
                    }
                }

            }

            logger.info("process begin 房间中 extparam参数  " + gameRoom.getExtparams());

            Player[] players = new Player[playUsers.size()];

            byte[] cards = new byte[cardnum];

            for (int i = 0; i < temp.size(); i++) {
                cards[i] = temp.get(i);
            }
            board.setCards(cards);

            int inx = 0;
            logger.info("playUsers size [" + playUsers.size() + "]");
            for (int i = 0; i < playUsers.size(); i++) {

                PlayUserClient client = playUsers.get(i);

                int index = client.getIndex();

                logger.info("玩家client " + client.getUsername() + "][" + client.getGameid() + "] 位置为 index[" + index + "]");

                if (index < 0 || index > playUsers.size() - 1) {
                    index = RoomUtil.getIndex(playUsers, client);
                    client.setIndex(index);
                }

                PinshiPlayer player = new PinshiPlayer(client.getId(), index);
                UKTools.copyProperties(client, player);
                player.setCards(new byte[5]); //牛牛只需要5张牌
                player.setGameroomid(gameRoom.getId());
                player.setRoomid(gameRoom.getRoomid());
                player.setTablenum(gameRoom.getTablenum());
                player.setTeanum(gameRoom.getTeanum());
                player.setUserid(client.getId());
                player.setStepscore(0);
                player.setGamestep(PinshiDataConstants.Gamestep.BEGIN.toString());

                SummaryPlayer summaryPlayer = getSummaryPlayerByUserid(gameRoom, player.getPlayuser());

                if (null != summaryPlayer) {
                    player.setTotalscore(summaryPlayer.getTotalscore());
                }

                logger.info("玩家 " + player.getUsername() + "][" + player.getGameid() + "] 位置为 index[" + index + "]");

                players[index] = player;//数组根据座位号来  数组下标从0开始，位置号从1开始。

                logger.info("当前分配的playuser信息 i[" + i + "]  " + player.getUsername() + "-------" + client.getIndex());

            }

            //处理发牌摸对
            PinshidealingUtil.dealBeginCards(gameRoom, players, temp, board, playUsers);

            board.setPlayers(players);

            logger.info("process finish 房间中 extparam参数  " + gameRoom.getExtparams());

            return board;
        }catch (Exception e ){
            logger.error("开始拼十游戏失败",e);
            e.printStackTrace();
            return null;
        }
    }

    private SummaryPlayer getSummaryPlayerByUserid(GameRoom gameRoom, String userid) {

        if (null != gameRoom.getSummary() && null != gameRoom.getSummary().getSummaryPlayer()) {
            List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();

            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                if (summaryPlayer.getUserid().equals(userid)) {
                    return summaryPlayer;
                }

            }
            return null;
        } else
            return null;

    }




}
