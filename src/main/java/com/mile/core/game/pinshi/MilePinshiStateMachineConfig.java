package com.mile.core.game.pinshi;

import com.mile.core.engine.game.action.EnoughAction;
import com.mile.core.engine.game.action.EnterAction;
import com.mile.core.engine.game.action.JoinAction;
import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.majiang.action.PlayCardsAction;
import com.mile.core.game.mazi.action.MaziClearAction;
import com.mile.core.game.mazi.action.MaziDissAction;
import com.mile.core.game.mazi.action.MaziStepClearAction;
import com.mile.core.game.pinshi.action.PinshiBindAction;
import com.mile.core.game.pinshi.action.PinshiDealingAction;
import com.mile.core.game.pinshi.action.PinshiStepClearAction;
import com.mile.core.statemachine.BeiMiStateMachine;
import com.mile.core.statemachine.config.StateConfigurer;
import com.mile.core.statemachine.config.StateMachineTransitionConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MilePinshiStateMachineConfig<T, S>  {

	@Bean("pinshi")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}

	/**
	 * 准备 -> 发牌 -> 抢庄 -> 定庄 ->闲家下注 -> 发牌 -> 看牌-> 亮牌 ->比牌->结算
	 *
	 *
	 * NONE->创建房间CRERATED->等待(WAITTING)->准备(READY)->发牌4(DEAL4)->定庄(BIND)->发牌1(DEAL1)->结算(STEPCLEAN)->准备(READY)......结算(CLEAN)->结束(END)
	 * @param states
	 * @throws Exception
	 */
    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(MileGameEnum.NONE.toString())
                    .state(MileGameEnum.CRERATED.toString())
                    .state(MileGameEnum.WAITTING.toString())
                    .state(MileGameEnum.READY.toString())
                    .state(MileGameEnum.BEGIN.toString())
                    .state(MileGameEnum.DEAL4.toString())
				    .state(MileGameEnum.BIND.toString())
				    .state(MileGameEnum.DEAL1.toString())
				    .state(MileGameEnum.STEPCLEARING.toString())
				    .state(MileGameEnum.CLEARING.toString())
				    .state(MileGameEnum.END.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
		/**
		 * 状态切换：NONE->创建房间CRERATED->等待(WAITTING)->准备(READY)->发牌4(DEAL4)->定庄(BIND)->发牌1(DEAL1)->结算(STEPCLEAN)->准备(READY)......结算(CLEAN)->结束(END)
		 */
        transitions
	        .withExternal()
		    	.source(MileGameEnum.NONE.toString()).target(MileGameEnum.CRERATED.toString())
		    	.event(MileGameEvent.ENTER.toString()).action(new EnterAction<String,String>())
		    	.and()
		    .withExternal()
	        	.source(MileGameEnum.CRERATED.toString()).target(MileGameEnum.WAITTING.toString())
	        	.event(MileGameEvent.JOIN.toString()).action(new JoinAction<String,String>())
	        	.and()
            .withExternal()
                .source(MileGameEnum.WAITTING.toString()).target(MileGameEnum.READY.toString())
                .event(MileGameEvent.ENOUGH.toString()).action(new EnoughAction<String, String>())//发牌
                .and()
            .withExternal()
                .source(MileGameEnum.READY.toString()).target(MileGameEnum.DEAL4.toString())
                .event(MileGameEvent.DEAL4.toString()).action(new PinshiBindAction<String,String>())
                .and()
            .withExternal()
                .source(MileGameEnum.DEAL4.toString()).target(MileGameEnum.BIND.toString())   //
                .event(MileGameEvent.BIND.toString()).action(new PinshiBindAction<String, String>())//定庄
                .and()
			.withExternal()
				.source(MileGameEnum.BIND.toString()).target(MileGameEnum.DEAL1.toString())   //
				.event(MileGameEvent.DEAL1.toString()).action(new PinshiDealingAction<String,String>()) //发最后一张牌
				.and()
			.withExternal()
				.source(MileGameEnum.DEAL1.toString()).target(MileGameEnum.STEPCLEARING.toString())   //
				.event(MileGameEvent.STEPCLEARING.toString()).action(new PinshiStepClearAction<String,String>())//小结结算
				.and()
			.withExternal()
				.source(MileGameEnum.DISSROOM.toString()).target(MileGameEnum.END.toString())
				.event(MileGameEvent.DISSROOM.toString()).action(new PinshiBindAction<String,String>())
				.and()
			.withExternal()
				.source(MileGameEnum.STEPCLEARING.toString()).target(MileGameEnum.CLEARING.toString())
				.event(MileGameEvent.CLEARING.toString()).action(new MaziClearAction<String,String>())
				.and()
            ;
    }
}
