package com.mile.core.game.pinshi.engine;

import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GamePlayerCache;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.common.cache.hazelcast.impl.QueneCache;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.game.*;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.Playeready;
import com.mile.core.game.pinshi.msgmodel.*;
import com.mile.core.game.pinshi.util.PinshiCardtypeUtil;
import com.mile.core.game.pinshi.util.PinshiRoomUtil;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.SysdicUtil;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by michael on 2018/6/11.
 */

@Service(value = "pinshiGameEngine")
public class PinshiGameEngine {

    private static final Logger logger = LoggerFactory.getLogger(PinshiGameEngine.class);

    /**
     * 坐下并且准备
     * @param beiMiClient
     */
    public void roomsit(BeiMiClient beiMiClient) {

        try {

            Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());

            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(token.getUserid(), token.getOrgi());

            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());

            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

            GamePlayerCache gamePlayerCache = (GamePlayerCache) CacheHelper.getGamePlayerCacheBean();

            List<PlayUserClient> players = gamePlayerCache.getNonWatchCacheObject(roomid, gameRoom.getOrgi());

            playUser.setIndex(RoomUtil.getIndex(players, playUser));
            playUser.setGamestatus(BMDataContext.GameStatusEnum.READY.toString());

            CacheHelper.getGamePlayerCacheBean().put(playUser.getId(), playUser, beiMiClient.getOrgi());

            CacheHelper.getApiUserCacheBean().put(playUser.getId(), playUser, beiMiClient.getOrgi());


            List<PlayUserClient> playUserClientList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(),gameRoom.getOrgi());

            ActionTaskUtils.sendPlayers(gameRoom,playUserClientList);

            //ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_ROOMSIT,new RoomsitInfo(playUser),gameRoom);

            PinshiRoomUtil.roomReady(gameRoom, GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()), playUser);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("玩家坐下异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家坐下异常", false));
        }

    }

    /**
     * 坐下并且准备
     *
     * @param beiMiClient
     */
    public void gamestart(BeiMiClient beiMiClient) {

        try {
            Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());
            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(token.getUserid(), token.getOrgi());
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

            logger.info("房间号["+gameRoom.getRoomid()+"] gameRoom.getCreater() ["+gameRoom.getCreater()+"] playUser.getGameid()["+playUser.getGameid()+"]");
            if (null == gameRoom.getCreater() || !gameRoom.getCreater().equals(playUser.getGameid())) {
                logger.info("roomid[" + roomid + "] 不正常 ");
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("非房主不能开始游戏", false));
                return;
            }

            List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            if(null == playerUserList || playerUserList.size() < 2){
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("少于两人不能开始游戏", false));
                return;
            }

            GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.ENOUGH.toString());    //通知状态机 , 此处应由状态机处理异步执行

            ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_GAMESTART, new NoticeEvent("开始游戏", true),gameRoom);


        } catch (Exception e) {
            e.printStackTrace();
            logger.error("房主开始游戏异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家坐下异常", false));
        }

    }




    /**
     * 抢庄
     *
     * @param beiMiClient
     */
    public void hog(BeiMiClient beiMiClient) {

        try {
            Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());
            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(token.getUserid(), token.getOrgi());
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
            PinshiBoard pinshiBoard = (PinshiBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            PinshiPlayer pinshiPlayer = (PinshiPlayer) pinshiBoard.player(playUser.getId());
            pinshiPlayer.setHogtimes(beiMiClient.getHogtimes());
            pinshiPlayer.setHogflag("Y");//Y 已抢庄  N 未抢庄

            CacheHelper.getBoardCacheBean().put(roomid,pinshiBoard,gameRoom.getOrgi());


            ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_HOG, new HogInfo(pinshiPlayer.getPlayuser(),beiMiClient.getHogtimes()),gameRoom);


            boolean bindflag = true;
            for(Player player:pinshiBoard.getPlayers()){
                PinshiPlayer p = (PinshiPlayer)player;
                if(!"Y".equals(p.getHogflag())){
                    bindflag = false;
                    break;
                }
            }

            if(bindflag){
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.BIND.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("抢庄异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家抢庄异常", false));
        }

    }





    /**
     * 下注
     *
     * @param beiMiClient
     */
    public void bet(BeiMiClient beiMiClient) {

        try {
            Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());
            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(token.getUserid(), token.getOrgi());
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
            PinshiBoard pinshiBoard = (PinshiBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            PinshiPlayer pinshiPlayer = (PinshiPlayer) pinshiBoard.player(playUser.getId());
            pinshiPlayer.setBettimes(beiMiClient.getBettimes());
            pinshiPlayer.setBetflag("Y");//Y 已抢庄  N 未抢庄
            pinshiPlayer.setPushflag(beiMiClient.getPushflag());
            CacheHelper.getBoardCacheBean().put(roomid,pinshiBoard,gameRoom.getOrgi());
            ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_BET, new BetInfo(pinshiPlayer.getPlayuser(),beiMiClient.getBettimes()),gameRoom);


            boolean betflag = true;
            for(Player player:pinshiBoard.getPlayers()){
                PinshiPlayer p = (PinshiPlayer)player;
                if(!"Y".equals(p.getBetflag())){
                    betflag = false;
                    break;
                }
            }

            if(betflag){
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.DEAL1.toString());    //通知状态机 , 此处应由状态机处理异步执行
            }


        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下注异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家下注异常", false));
        }

    }



    /**
     * 下注
     *
     * @param beiMiClient
     */
    public void show(BeiMiClient beiMiClient) {

        try {
            Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());
            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(token.getUserid(), token.getOrgi());
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());



            PinshiPlayer pinshiPlayer = null;
            Pinshicardtype cardType = null;
            PinshiBoard pinshiBoard = null;
            synchronized (this) {
                pinshiBoard =  (PinshiBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                pinshiPlayer = (PinshiPlayer) pinshiBoard.player(playUser.getId());
                cardType =  (Pinshicardtype) PinshiCardtypeUtil.getCardtype(pinshiPlayer.getCards());
                pinshiPlayer.setPinshicardtype(cardType);
                if(!PinshiDataConstants.Gamestep.SHOW.toString().equals(pinshiPlayer.getGamestep())){
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("非亮牌"+pinshiPlayer.getGamestep()+"阶段玩家不能亮牌", false));
                    logger.error("非亮牌"+pinshiPlayer.getGamestep()+"阶段玩家不能亮牌!!!!!!!!");
                    return ;
                }

                pinshiPlayer.setShowflag("Y");

                CacheHelper.getBoardCacheBean().put(gameRoom.getId(), pinshiBoard, gameRoom.getOrgi());

                logger.info("亮牌信息  cardType["+cardType+"]");
            }
            ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_SHOW, new ShowInfo(pinshiPlayer.getUserid(),pinshiPlayer.getLastcard(),cardType),gameRoom);


            boolean showflag = true;
            for(Player player:pinshiBoard.getPlayers()){
                PinshiPlayer p = (PinshiPlayer)player;
                if(!"Y".equals(p.getShowflag())){
                    showflag = false;
                    break;
                }
            }

            if(showflag){
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.STEPCLEARING.toString());    //通知状态机 , 此处应由状态机处理异步执行
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("亮牌异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家亮牌异常", false));
        }

    }

}
