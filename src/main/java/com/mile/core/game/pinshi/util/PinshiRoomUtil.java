package com.mile.core.game.pinshi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GamePlayerCache;
import com.mile.config.web.model.Game;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.CardType;
import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.mazi.util.MaziInnertestUtil;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.NextPlayer;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.RoomReady;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 房间工具类
 */
public class PinshiRoomUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshiRoomUtil.class);


    public static boolean isLaizi(GameRoom gameRoom){
        if(null == gameRoom || null == gameRoom.getExtparams() || null == gameRoom.getExtparams().get("senior")){
            return false;
        }

        logger.info("房间["+gameRoom.getRoomid()+"] 癞子信息["+RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),"senior:laizi")+"]");
        if(StringUtils.isEmpty(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(),"senior:laizi"))){
           return false;
        }else{
            return true;
        }

    }

    /**
     * 通知就绪
     *
     * @param gameRoom
     * @param game
     */
    public static void roomReady(GameRoom gameRoom, Game game, PlayUserClient playUser) {


        int autobeginflag = RoomUtil.getCreateIntvalue(gameRoom,"autobeginflag");

        GamePlayerCache gamePlayerCache = (GamePlayerCache)CacheHelper.getGamePlayerCacheBean();

        boolean enough = false;

        List<PlayUserClient> playerList = gamePlayerCache.getNonWatchCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (autobeginflag !=0 && autobeginflag < playerList.size()) {
                enough = true;
                gameRoom.setStatus(MileGameEnum.READY.toString());
                //ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_ROOMREADY, new RoomReady(gameRoom), gameRoom);
                ActionTaskUtils.sendEvent(PinshiDataConstants.GAME_EVENT_GAMESTART, new NoticeEvent("开始游戏", true),gameRoom);

        } else {
            gameRoom.setStatus(MileGameEnum.WAITTING.toString());
        }

        if (null != playUser) {
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_PLAYERREADY, new RoomReady(gameRoom, playUser.getId()), gameRoom);
        }
        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
        /**
         * 所有人都已经举手
         */
        if (enough == true) {
            game.change(gameRoom, MileGameEvent.ENOUGH.toString());    //通知状态机 , 此处应由状态机处理异步执行
        }
    }







}