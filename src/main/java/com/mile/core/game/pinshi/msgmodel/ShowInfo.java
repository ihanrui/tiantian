package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.game.CardType;

/**
 * Created by michael on 2018/6/24.
 */
public class ShowInfo extends BaseEvent {

    private String userid;

    private byte lastcard;//最后一张牌

    private CardType cardType;//牌型

    public ShowInfo(String userid, byte lastcard, CardType cardType) {
        this.userid = userid;
        this.lastcard = lastcard;
        this.cardType = cardType;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public byte getLastcard() {
        return lastcard;
    }

    public void setLastcard(byte lastcard) {
        this.lastcard = lastcard;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }
}
