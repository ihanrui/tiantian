package com.mile.core.game.pinshi.action;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.mazi.task.CreateMaziStepCleanTask;
import com.mile.core.game.pinshi.task.CreatePinshiStepCleanTask;
import com.mile.core.statemachine.action.Action;
import com.mile.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.mile.core.statemachine.message.Message;
import org.apache.commons.lang3.StringUtils;

/**
 * 一盘结束结算
 * @author iceworld
 *
 * @param <T>
 * @param <S>
 */
public class PinshiStepClearAction<T,S> implements Action<T, S>{

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T,S> configurer) {
		String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
		if(!StringUtils.isBlank(room)){
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ;
			if(gameRoom!=null){
				CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreatePinshiStepCleanTask(0.0 , gameRoom , gameRoom.getOrgi()));
			}
		}
	}
}
