package com.mile.core.game.pinshi.task;

import com.mile.common.cache.CacheHelper;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.BankerInfo;
import com.mile.core.game.pinshi.msgmodel.BetInfo;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.util.PinshiEventUtil;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.SysdicUtil;
import com.mile.util.GameUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;

/**
 * 拼十发最后一张牌
 */
public class PinshiDealingTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(PinshiDealingTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public PinshiDealingTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {
        if (null !=gameRoom ) {
            PinshiBoard board = (PinshiBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            Player[] players = board.getPlayers();


            int i=0 ;
            for(;i<players.length;i++){
                if(board.getBanker().equals(players[i].getPlayuser())){
                    break;
                }
            }


            String basescore = RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_BASESCORE);
            int mintimes = Integer.parseInt(basescore.split("/")[0]);


            //拼十不再发牌
            for (int j = 0; j <  players.length;  j++) {

                PinshiPlayer ptemp = (PinshiPlayer)players[j];
//                byte cardtemp  = board.getLeftcards().get(j);

//                if(ptemp.getBettimes() <= 0){
//                    logger.info("玩家["+ptemp.getUsername()+"]["+ptemp.getGameid()+"]默认下注");
//                    ptemp.setBettimes(mintimes);
//                }


//                ptemp.getCards()[4] = cardtemp;
                ptemp.setLastcard(ptemp.getCards()[4]);

                logger.info("玩家 最后一张牌 [" + ptemp.getUsername() + "][" + ptemp.getGameid() + "] 牌[" + ptemp.getCards()[4] + "]["+ptemp.getLastcard()+"]");

            }


            PinshiEventUtil.sendDealingEvent(gameRoom,PinshiDataConstants.GAME_EVENT_DEALING,board);

            for(Player player:board.getPlayers()){
                PinshiPlayer pinshiPlayer = (PinshiPlayer)player;
                pinshiPlayer.setGamestep(PinshiDataConstants.Gamestep.SHOW.toString());
            }

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(),board,gameRoom.getOrgi());

            //定庄后 闲家下注,下注完成就发牌。
            //GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.STEPCLEARING.toString(), SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.SHOW.toString()));    //通知状态机 , 此处应由状态机处理异步执行

        }

    }
}
