package com.mile.core.game.pinshi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiClient;
import com.mile.core.game.pinshi.msgmodel.BetInfo;
import com.mile.core.game.pinshi.msgmodel.DealingInfo;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.util.SysdicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by michael on 2018/6/12.
 */
public class PinshiEventUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshiEventUtil.class);

    public static void sendDealingEvent(GameRoom gameRoom, String event, PinshiBoard pinshiBoard){


        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());



        if (null != players && !players.isEmpty()) {
            logger.info("发送房间["+event+"]事件 roomid["+gameRoom.getRoomid()+"],所有玩家个数为["+players.size()+"] ");
            for (PlayUserClient user : players) {

                BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
                logger.info("发送房间事件 client.getSessionId()["+client.getClient().getSessionId()+"] userToken["+user.getToken()+"] userid["+user.getId()+"]");

                PinshiPlayer pinshiPlayer =  (PinshiPlayer)pinshiBoard.player(user.getId());
                DealingInfo dealingInfo = new DealingInfo(user.getId(),pinshiPlayer.getCards(),pinshiPlayer.getLastcard());

                dealingInfo.setCommand(event);
                dealingInfo.setShowsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.SHOW.toString()));

                if (client != null && ActionTaskUtils.online(user.getId(), user.getOrgi())) {
                    client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, dealingInfo);
                }
            }
        }

    }




    public static void sendGameroomEvent(GameRoom gameRoom, String event, PinshiBoard pinshiBoard){


        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());



        if (null != players && !players.isEmpty()) {
            logger.info("发送房间["+event+"]事件 roomid["+gameRoom.getRoomid()+"],所有玩家个数为["+players.size()+"] ");
            for (PlayUserClient user : players) {

                BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
                logger.info("发送房间事件 client.getSessionId()["+client.getClient().getSessionId()+"] userToken["+user.getToken()+"] userid["+user.getId()+"]");

                PinshiPlayer pinshiPlayer =  (PinshiPlayer)pinshiBoard.player(user.getId());
                DealingInfo dealingInfo = new DealingInfo(user.getId(),pinshiPlayer.getCards(),pinshiPlayer.getLastcard());

                dealingInfo.setCommand(event);

                if (client != null && ActionTaskUtils.online(user.getId(), user.getOrgi())) {
                    client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, dealingInfo);
                }
            }
        }

    }

}
