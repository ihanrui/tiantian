package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.PinshiDataConstants;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.msgmodel.MaziBoard;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.util.SysdicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

public class PinshiUserBoard extends UserBoard {

    private static final Logger logger = LoggerFactory.getLogger(PinshiUserBoard.class);
    /**
     *
     */
    private static final long serialVersionUID = -1224310911110772375L;




    private String pinshirule;//翻倍规则

    private String additional;//其他规则

    private String specialcard;//特殊牌型

    private String  senior;//高级玩法


    private String basescore;//底分描述

    //maxplayers 人数
    //玩法 playwaydesc
    //支付方式 房费 tips
    private String tips;

    private int gamenumbers;//局数



    protected int hogsnd;//抢庄秒数

    protected int betsnd;//下注秒数

    protected int showsnd;//亮牌秒数

    protected int beginsnd;//亮牌秒数


    private boolean startflag;


    public PinshiUserBoard() {
        super();
    }




    public PinshiUserBoard(Board board, String curruser, String command) {
        super(board,curruser,command);
        this.setHogsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.HOG.toString()));
        this.setBeginsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BEGIN.toString()));
        this.setBetsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BET.toString()));
        this.setShowsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.SHOW.toString()));

    }

    public PinshiUserBoard(Board board, String curruser, String command, byte teamCard, String teamOwner) {
         super(board,curruser,command,teamCard,teamOwner);

        this.setHogsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.HOG.toString()));
        this.setBeginsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BEGIN.toString()));
        this.setBetsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.BET.toString()));
        this.setShowsnd(SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.SHOW.toString()));
    }





    public int getHogsnd() {
        return hogsnd;
    }

    public void setHogsnd(int hogsnd) {
        this.hogsnd = hogsnd;
    }

    public int getBetsnd() {
        return betsnd;
    }

    public void setBetsnd(int betsnd) {
        this.betsnd = betsnd;
    }

    public int getShowsnd() {
        return showsnd;
    }

    public void setShowsnd(int showsnd) {
        this.showsnd = showsnd;
    }

    public int getBeginsnd() {
        return beginsnd;
    }

    public void setBeginsnd(int beginsnd) {
        this.beginsnd = beginsnd;
    }

    public String getPinshirule() {
        return pinshirule;
    }

    public void setPinshirule(String pinshirule) {
        this.pinshirule = pinshirule;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getSpecialcard() {
        return specialcard;
    }

    public void setSpecialcard(String specialcard) {
        this.specialcard = specialcard;
    }

    public String getSenior() {
        return senior;
    }

    public void setSenior(String senior) {
        this.senior = senior;
    }

    public String getBasescore() {
        return basescore;
    }

    public void setBasescore(String basescore) {
        this.basescore = basescore;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public int getGamenumbers() {
        return gamenumbers;
    }

    public void setGamenumbers(int gamenumbers) {
        this.gamenumbers = gamenumbers;
    }

    public boolean isStartflag() {
        return startflag;
    }

    public void setStartflag(boolean startflag) {
        this.startflag = startflag;
    }
}
