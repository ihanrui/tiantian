package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

import java.util.List;

/**
 * Created by michael on 2018/6/12.
 */
public class BankerInfo extends BaseEvent {

    private List<PinshiPlayer> pinshiPlayerList;//可抢庄的

    private String banker;//庄家

    private String basescore;//底分

    private String pushscore;//推注分数

    private int bankertimes;//抢庄倍数

    private int betsnd;//下注秒数

    public BankerInfo(List<PinshiPlayer> pinshiPlayerList, String banker, String basescore) {
        this.pinshiPlayerList = pinshiPlayerList;
        this.banker = banker;
        this.basescore = basescore;
    }

    public BankerInfo(List<PinshiPlayer> pinshiPlayerList, String banker) {
        this.pinshiPlayerList = pinshiPlayerList;
        this.banker = banker;
    }

    public List<PinshiPlayer> getPinshiPlayerList() {
        return pinshiPlayerList;
    }

    public void setPinshiPlayerList(List<PinshiPlayer> pinshiPlayerList) {
        this.pinshiPlayerList = pinshiPlayerList;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public String getBasescore() {
        return basescore;
    }

    public void setBasescore(String basescore) {
        this.basescore = basescore;
    }

    public String getPushscore() {
        return pushscore;
    }

    public void setPushscore(String pushscore) {
        this.pushscore = pushscore;
    }

    public int getBankertimes() {
        return bankertimes;
    }

    public void setBankertimes(int bankertimes) {
        this.bankertimes = bankertimes;
    }

    public int getBetsnd() {
        return betsnd;
    }

    public void setBetsnd(int betsnd) {
        this.betsnd = betsnd;
    }
}
