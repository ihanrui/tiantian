package com.mile.core.game.pinshi.util;

import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.record.entity.SummaryPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

/**
 * Created by michael on 2018/6/13.
 */
public class PinshiTipUtil {

    private static final Logger logger = LoggerFactory.getLogger(PinshiTipUtil.class);

    /**
     * 得到推注倍数  最小下注*推注倍数 + 上局赢的分数
     *
     * @param gameRoom
     * @return
     */
    public static int getBolustimes(GameRoom gameRoom, PinshiPlayer pinshiPlayer, SummaryPlayer summaryPlayer) {
        if (pinshiPlayer.getStepscore() < 0) {
            logger.info("玩家[" + pinshiPlayer.getUsername() + "][" + pinshiPlayer.getGameid() + "] 输了");
            return 0;
        }

        String basescore = RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_BASESCORE);

        String pushflag = RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_PUSHFLAG);

        if (StringUtils.isEmpty(pushflag)) {
            logger.info("房间 [" + gameRoom.getRoomid() + "] 无推注选项 ");
            return 0;
        }

        if (!pinshiPlayer.isBanker()) {
            int minTimes = Integer.parseInt(basescore.split("/")[0]);
            int pushtimes = Integer.parseInt(pushflag);

            int maxtimes = minTimes * pushtimes;

            int times = pinshiPlayer.getStepscore() + minTimes;

            logger.info("倍数  minTimes[" + minTimes + "] pushtimes[" + pushtimes + "] maxtimes[" + maxtimes + "] times[" + times + "]");

            return maxtimes > times ? times : maxtimes;
        } else {
            logger.info("玩家[" + pinshiPlayer.getUsername() + "] pinshiPlayer.isBanker()[" + pinshiPlayer.isBanker() + "] 输了");
        }

        return 0;

    }

    public static boolean getStartflag(GameRoom gameRoom, List<PlayUserClient> playerList) {
        try {
            int autobeginflag = Integer.parseInt(RoomUtil.getPinshiCreatevalue(gameRoom.getExtparams(), PinshiDataConstants.CONT_ROOM_AUTOBEGINFLAG));

            if(autobeginflag == 0 || playerList.size() <= 1)
                return false;
            if(autobeginflag <= playerList.size())
                return true;

            return false;
        }catch (Exception e){
            logger.error("获取开始标志失败",e);
            e.printStackTrace();
            return false;
        }
    }

}
