package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

/**
 * Created by michael on 2018/6/12.
 */
public class BetInfo extends BaseEvent {

    private String userid;

    private int bettimes;// 1，2，3，4 下注倍数  -1.未下注

    public BetInfo(String userid, int bettimes) {
        this.userid = userid;
        this.bettimes = bettimes;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getBettimes() {
        return bettimes;
    }

    public void setBettimes(int bettimes) {
        this.bettimes = bettimes;
    }
}
