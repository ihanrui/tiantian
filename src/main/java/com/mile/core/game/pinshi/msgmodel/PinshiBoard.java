package com.mile.core.game.pinshi.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.CardType;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.BaseBoard;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import com.mile.core.game.pinshi.util.PinshiScoreUtil;
import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.model.BeiMiDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Michael
 */

public class PinshiBoard extends BaseBoard implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(PinshiBoard.class);

    private List<Byte> leftcards;

    /**
     *
     */
    private static final long serialVersionUID = 901134723522873348L;

    @Override
    public TakeCards takeCardsRequest(GameRoom gameRoom, Board board, Player player,
                                      String orgi, boolean auto, byte[] playCards) {
        logger.info("拼十不需要出牌");
        return null;
    }

    @Override
    public Summary stepSummary(Board board, GameRoom gameRoom, GamePlayway playway) {
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        Summary summary = gameRoom.getSummary();

        if (null == summary) {
            summary = new Summary();
            summary.setRoomid(gameRoom.getRoomid());
            summary.setGameplaywayid(gameRoom.getGamePlayway().getId());
            summary.setGameplaywaycode(gameRoom.getGamePlayway().getCode());
            summary.setGameroomid(gameRoom.getId());
            summary.setTeanum(gameRoom.getTeanum());
            summary.setTablenum(gameRoom.getTablenum());
            summary.setBegindate(gameRoom.getCreatetime());//开始时间

        }

        List<SummaryPlayer> summaryPlayerList = summary.getSummaryPlayer();
        //summary.setSummaryPlayer(new ArrayList<SummaryPlayer>());
        List<SummaryPlayer> summaryPlayers = new ArrayList<SummaryPlayer>();

        logger.info("房间号[" + gameRoom.getRoomid() + "]一盘格式化信息");
        for (Player player : board.getPlayers()) {


            SummaryPlayer summaryPlayer = MaziScoreUtil.findSumByUserid(summary.getSummaryPlayer(), player.getPlayuser());

            //格式化小结结算信息
            summaryPlayer = PinshiScoreUtil.formatStepSum(summaryPlayer, player, gameRoom);

            summaryPlayers.add(summaryPlayer);
        }
        summary.setSummaryPlayer(summaryPlayers);

        logger.info("房间号[" + gameRoom.getRoomid() + "]设置拼十大赢家和土豪");
        int winflag = 0;
        int richflag = 0;
        for (SummaryPlayer summaryPlayer : summary.getSummaryPlayer()) {
            winflag = summaryPlayer.getTotalscore() > winflag ? summaryPlayer.getTotalscore() : winflag;
            richflag = summaryPlayer.getTotalscore() < richflag ? summaryPlayer.getTotalscore() : richflag;

        }

        for (SummaryPlayer summaryPlayer : summary.getSummaryPlayer()) {
            summaryPlayer.setRichflag(richflag == summaryPlayer.getTotalscore() ? 1 : 0);
            summaryPlayer.setWinflag(winflag == summaryPlayer.getTotalscore() ? 1 : 0);

        }

        logger.info("房间号[" + gameRoom.getRoomid() + "]拼十单局结算信息  summary [" + summary + "]");
        gameRoom.setSummary(summary);

        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

        return summary;
    }

    public List<Byte> getLeftcards() {
        return leftcards;
    }

    public void setLeftcards(List<Byte> leftcards) {
        this.leftcards = leftcards;
    }

    public PinshiBoard() {
    }



    public List<PinshiPlayer> getHogplayers() {
        Player[] pinshiPlayers = this.getPlayers();
        List<PinshiPlayer> pinshiPlayerList = new LinkedList<PinshiPlayer>();

        int hogtimes = TeahouseUtil.getIntConstantsByName(PinshiDataConstants.CONST_HOGTIMES);

        for(;hogtimes>-1;hogtimes--) {
            for (Player player : pinshiPlayers) {
                PinshiPlayer pinshiPlayer = (PinshiPlayer) player;
                if (null != pinshiPlayer && hogtimes == pinshiPlayer.getHogtimes()) {
                    pinshiPlayerList.add(pinshiPlayer);
                }
            }


            if (null == pinshiPlayerList || pinshiPlayerList.isEmpty()) {
                continue;
            }else {
                break;
            }
        }





        if (null == pinshiPlayerList || pinshiPlayerList.isEmpty()) {
            for (Player player : pinshiPlayers) {
                PinshiPlayer pinshiPlayer = (PinshiPlayer) player;
                pinshiPlayerList.add(pinshiPlayer);
            }
        }
        return pinshiPlayerList;
    }

}
