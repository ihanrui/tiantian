package com.mile.core.game;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.CardEvent;
import com.mile.core.engine.game.eventmodel.LeaveEvent;
import com.mile.core.engine.game.eventmodel.MsgEvent;
import com.mile.core.entity.*;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.GameStatus;
import com.mile.core.game.msgmodel.SearchRoom;
import com.mile.core.game.msgmodel.SearchRoomResult;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.util.UKTools;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class GameEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(GameEventHandler.class);

    @Autowired
    protected SocketIOServer server;

    @Autowired
    public GameEventHandler(SocketIOServer server) {
        this.server = server;
    }

    @OnConnect
    public void onConnect(SocketIOClient client) {
        try {
            //获取当前session的客户端信息，包括token,seesionid,socketid等等
            Date connectbegin = new Date(System.currentTimeMillis());

            BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
            if (beiMiClient != null && !StringUtils.isBlank(beiMiClient.getUserid())) {
                //判断房间ID是否为空
                if (CacheHelper.getRoomMappingCacheBean().getCacheObject(beiMiClient.getUserid(), beiMiClient.getOrgi()) != null) {

                    //方法有空指针错误
                    ActionTaskUtils.sendEvent("", beiMiClient.getUserid(), null);
                }

                Token token = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());

                beiMiClient.setClient(client);
                beiMiClient.setSession(client.getSessionId().toString());
                /**
                 * 心跳时间
                 */
                beiMiClient.setTime(System.currentTimeMillis());
                NettyClients.getInstance().putClient(token, beiMiClient);

                logger.info("玩家[" + beiMiClient.getGameid() + "]  connect 上线");

                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(beiMiClient.getUserid(), beiMiClient.getOrgi());

            }

            Date connectend = new Date(System.currentTimeMillis());
            logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 建立连接开始时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectbegin));
            logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 建立连接结束时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectend));
        }catch (Exception e){
            logger.error("连接失败",e);
        }
    }

    //添加@OnDisconnect事件，客户端断开连接时调用，刷新客户端信息
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
         try {
             BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
             if (beiMiClient != null) {
                 BMDataContext.getGameEngine().disconnect(beiMiClient, client);
             }
         }catch (Exception e){
             logger.error("断线失败",e);
         }
    }


    @OnEvent(value = DataConstants.GAME_EVENT_GAMESTATUS)
    public void onGameStatus(SocketIOClient client, String data) {
       try{
        Date connectbegin = new Date(System.currentTimeMillis());
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        logger.info("onGameStatus client [" + client.getSessionId() + "]  begin data[" + data + "]");
        BMDataContext.getGameEngine().gamestatus(beiMiClient,client);
        logger.info("onGameStatus client [" + beiMiClient.getClient().getSessionId() + "]  end data[" + data + "]");
        Date connectend = new Date(System.currentTimeMillis());
        logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 同步游戏状态开始时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectbegin));
        logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 同步游戏状态结束时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectend));
       }catch (Exception e){
           logger.error("游戏状态同步失败",e);
       }
    }

    //抢地主事件
    @OnEvent(value = "docatch")
    public void onCatch(SocketIOClient client, String data) {
        try{
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().actionRequest(roomid, playUser, playUser.getOrgi(), true);
            }
        }

        }catch (Exception e){
            logger.error("抢地主失败",e);
        }
    }

    //弃权
    @OnEvent(value = "giveup")
    public void onGiveup(SocketIOClient client, String data) {
        onMazigiveup(client,data);
    }






    //投降
    @OnEvent(value = DataConstants.GAME_EVENT_MAZIGIVEUP)
    public void onMazigiveup(SocketIOClient client, String data) {

        logger.info("投降信息data " + data);

        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

        int giveupflag = Integer.parseInt(JSON.parseObject(data).getString("giveupflag"));
        boolean firstflag = Boolean.parseBoolean(JSON.parseObject(data).getString("firstflag"));

        try {

            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                    logger.info("投降时的 roomid[" + roomid + "]");

                    BMDataContext.getGameEngine().mazigiveup(gameRoom, board, userToken.getUserid(), giveupflag, firstflag);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统投降异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统投降异常", false));
        }

    }

    //提示
    @OnEvent(value = DataConstants.GAME_EVENT_CARDTIPS)
    public void onCardTips(SocketIOClient client, String data) {
        try{
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().cardTips(roomid, playUser, playUser.getOrgi());
            }
        }

        }catch (Exception e){
            logger.error("牌型提示失败",e);
        }
    }

    //出牌
    @OnEvent(value = DataConstants.GAME_EVENT_TAKECARDS)
    public void onTakecards(SocketIOClient client, String data) {


        logger.info("出牌 信息  data [" + data + "]");

        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        try {
        String cardstr = JSON.parseObject(data).getString("cards");

        String token = beiMiClient.getToken();

            if (!StringUtils.isBlank(token) && !StringUtils.isBlank(cardstr)) {

                    BMDataContext.getGameEngine().takeCardsRequest(cardstr,token,beiMiClient.getUserid() , beiMiClient.getOrgi(), false);

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("出牌报错", e);
            MaziTakeCards takeCards = new MaziTakeCards();
            takeCards.setAllow(false);
            takeCards.setTipstr("出牌系统错误data[" + data + "]");
            logger.info("出牌不符合规则的， 只有自己才能收到");
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统出牌异常", false));
        }


    }

    //没有
    @OnEvent(value = DataConstants.GAME_EVENT_NOCARDS)
    public void onNoCards(SocketIOClient client, String data) {
        try{
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().takeCardsRequest("",token, beiMiClient.getUserid(), beiMiClient.getOrgi(), false);
            }
        }
        }catch (Exception e){
            logger.error("不出失败",e);
        }
    }

    //出牌
    @OnEvent(value = "selectcolor")
    public void onSelectColor(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().selectColorRequest(roomid, playUser.getId(), userToken.getOrgi(), data);
            }
        }
    }

    //出牌  麻将系列 过
    @OnEvent(value = "selectaction")
    public void onActionEvent(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().actionEventRequest(roomid, playUser.getId(), userToken.getOrgi(), data);
            }
        }
    }

    //重新开始
    @OnEvent(value = "restart")
    public void onRestart(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().restartRequest(roomid, playUser, beiMiClient, "true".equals(data));
            }
        }
    }

    //准备
    @OnEvent(value = DataConstants.GAME_EVENT_START)
    public void onStart(SocketIOClient client, String data) {

        try {
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {

                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getGamePlayerCacheBean().getPlayer(userToken.getUserid(), userToken.getOrgi());
                    if (BMDataContext.GameStatusEnum.READY.toString().equals(playUser.getGamestatus())) {
                        logger.info("玩家[" + playUser.getUsername() + "]gameid[" + playUser.getGameid() + "]  请勿重复点击!!!!!");
                        return;
                    }
                    playUser.setGamestatus(BMDataContext.GameStatusEnum.READY.toString());
                    if (playUser != null) {
                        BMDataContext.getGameEngine().startGameRequest(playUser.getRoomid(), playUser, userToken.getOrgi(), "true".equals(data));
                    }

                    CacheHelper.getGamePlayerCacheBean().put(playUser.getId(), playUser, playUser.getOrgi()); //将用户加入到 room ， MultiCache
                    CacheHelper.getApiUserCacheBean().put(playUser.getId(), playUser, playUser.getOrgi()); //将用户加入到 room ， MultiCache
                }
            }
        }catch (Exception e){
            logger.error("玩家准备失败",e);
        }
    }

    //恢复牌局
    @OnEvent(value = DataConstants.GAME_EVENT_RECOVERY)
    public void onRecovery(SocketIOClient client, String data) {
        try {
            logger.info("恢复牌局事件  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            ;
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                    if (null == CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi())) {
                        logger.info("玩家[" + playUser.getUsername() + "][" + playUser.getGameid() + "]已经离开房间，无法恢复");
                        ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_LEAVE, userToken.getUserid(), new LeaveEvent(userToken.getUserid()));
                    } else {
                        BMDataContext.getGameEngine().recovery(beiMiClient, playUser);
                    }

                }
            }
        }catch (Exception e){
            logger.error("恢复牌局失败",e);
        }
    }




    //获取用户信息
    @OnEvent(value = DataConstants.GLOBAL_GAME_EVENT_UPDATEUSER)
    public void updateuser(SocketIOClient client, String data) {
        logger.info("获取用户信息数据  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUser playuser = BMDataContext.getContext().getBean(PlayUserRepository.class).findById(userToken.getUserid());
                MaziScoreUtil.synchronizationUsercache(playuser);
            }
        }
    }








    //发送消息
    @OnEvent(value = DataConstants.GAME_EVENT_SENDMSG)
    public void onsendmsg(SocketIOClient client, String data) {

        MsgEvent msgevent = JSON.parseObject(data, MsgEvent.class);

        logger.info("发送的消息内容为 " + msgevent);

        logger.info("发送消息 传入  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        BMDataContext.getGameEngine().sendmsg(beiMiClient,msgevent);

    }




    //记牌器
    @OnEvent(value = DataConstants.GAME_EVENT_REMEMBERCARD)
    public void onremembercard(SocketIOClient client, String data) {

        logger.info("获取记牌器数据数据  data[" + data + "]");

        try {
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUser playuser = BMDataContext.getContext().getBean(PlayUserRepository.class).findById(userToken.getUserid());

                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playuser.getId(), playuser.getOrgi());

                    if (StringUtils.isEmpty(roomid)) {
                        logger.info("房间号为空");
                        return;
                    }

                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                    if (null == gameRoom) {
                        logger.info("房间为空");
                        return;
                    }

                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                    if (null == gameRoom) {
                        logger.info("牌局未开始");
                        return;
                    }

                    ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_REMEMBERCARD, userToken.getUserid(), new CardEvent(board.getHistory()));

                }
            }
        }catch (Exception e){
            logger.error("记牌器错误",e);
        }
    }



}