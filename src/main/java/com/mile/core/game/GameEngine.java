package com.mile.core.game;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.mile.common.BaseEngine;
import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.*;
import com.mile.core.entity.*;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.*;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.RoomdescUtil;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.core.teahouse.util.TeahouseeventUtil;
import com.mile.util.GameUtils;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service(value = "gameEngine")
public class GameEngine extends BaseEngine {

    private static final Logger logger = LoggerFactory.getLogger(GameEngine.class);

    @Autowired
    protected SocketIOServer server;
    @Resource
    private KieSession kieSession;

    public void gameRequest(PlayUserClient userClient, BeiMiClient beiMiClient) {
        try {

            GameEvent gameEvent = gameRequest(userClient.getId(), beiMiClient.getPlayway(), beiMiClient, beiMiClient.getOrgi(), userClient);
            if (gameEvent != null) {
                boolean joinflag = true;
                Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameEvent.getRoomid(), gameEvent.getOrgi());
                if (board != null) {
                    Player currentPlayer = board.player(userClient.getId());
                    if (currentPlayer != null && null != CacheHelper.getRoomMappingCacheBean().getCacheObject(userClient.getId(), "")) {
                        recovery(board, gameEvent.getGameRoom(), currentPlayer);
                        joinflag = false;
                        logger.info("房间号[" + gameEvent.getGameRoom().getRoomid() + "] 玩家[" + userClient.getUsername() + "][" + userClient.getGameid() + "] 有恢复牌局事件，无需发送joinroom事件");
                    }
                } else {
                    //通知状态
                    logger.info("通知状态机 , 此处应由状态机处理异步执行");
                    GameUtils.getGame(beiMiClient.getPlayway(), gameEvent.getOrgi()).change(gameEvent);    //通知状态机 , 此处应由状态机处理异步执行
                }

                if (joinflag) {
                    logger.info("房间[" + gameEvent.getGameRoom().getRoomid() + "] [" + userClient.getUsername() + "] 玩家加入房间");
                    BMDataContext.getRoomEngine().sendJoinroomEvent(userClient, gameEvent, beiMiClient);
                } else {
                    logger.info("----joinflag[" + joinflag + "]-----房间号[" + gameEvent.getGameRoom().getRoomid() + "] 玩家[" + userClient.getUsername() + "][" + userClient.getGameid() + "] 有恢复牌局事件，无需发送joinroom事件");
                }
            } else {
                logger.info("未创建房间");

            }
        } catch (Exception e) {
            logger.error("玩家游戏请求失败", e);
        }
    }

    /**
     * @param userid
     * @param orgi
     * @return
     */
    public synchronized GameEvent gameRequest(String userid, String playway, BeiMiClient beiMiClient, String orgi, PlayUserClient playUserClient) {
        try {
            GameEvent gameEvent = null;
            //得到房间ID
            logger.info("本次获取房间ID参数为 userid【" + userid + "】 orgi【" + orgi + "】 playway[" + playway + "]");
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userid, orgi); //吴真修改
            logger.info("本次用户的房间ID为testRoomid [" + roomid + "] ");

            //通过玩法，及组织结构 获取玩法对象
            GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(playway, orgi);
            if (gamePlayway != null) {
                gameEvent = new GameEvent(gamePlayway.getPlayers(), gamePlayway.getCardsnum(), orgi);

                GameRoom gameRoom = BMDataContext.getRoomEngine().searchRoom(beiMiClient, roomid, playUserClient, gamePlayway);

                if (gameRoom != null) {

                    logger.info("保存GameRoom信息 gameRoom【" + gameRoom + "】");
                    CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, orgi);

                    List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                    if (playerList.size() == 0) {
                        gameEvent.setEvent(MileGameEvent.ENTER.toString());
                    } else {
                        gameEvent.setEvent(MileGameEvent.JOIN.toString());
                    }
                    gameEvent.setGameRoom(gameRoom);
                    gameEvent.setRoomid(gameRoom.getId());

                    BMDataContext.getRoomEngine().joinRoom(gameRoom, playUserClient, playerList);

                    if (DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())) {
                        logger.info("gameRequest 麻子无旁观者 设置座位号");

                        gameEvent.setIndex(RoomUtil.getIndex(gameRoom, playUserClient));

                        logger.info("gameroom[" + gameRoom.getRoomid() + "] 分配位置  玩家[" + playUserClient.getUsername() + "] 位置[" + playUserClient.getIndex() + "]");

                        TeahouseeventUtil.sendTablechange(gameRoom);
                    } else if (DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())) {
                        logger.info("gameRequest 牛牛有旁观者 不设置座位号");
                    }

                    if (playerList.size() < gamePlayway.getPlayers() && !gameRoom.isCardroom()) {
                        CacheHelper.getQueneCache().put(gameRoom, orgi);    //未达到最大玩家数量，加入到游戏撮合 队列，继续撮合
                    }

                } else {
                    gameEvent = null;
                }
            }
            return gameEvent;
        } catch (Exception e) {
            logger.error("玩家游戏请求失败", e);
            return null;
        }
    }

    /**
     * 提示按钮 后台功能处理
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public void cardTips(String roomid, PlayUserClient playUser, String orgi) {
        try{
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
        if (gameRoom != null) {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            Player player = board.player(playUser.getId());

            TakeCards takeCards = null;

            if (takeCards == null || takeCards.getCards() == null) {
                if (board.getLast() != null && !board.getLast().getUserid().equals(player.getPlayuser())) {    //当前无出牌信息，刚开始出牌，或者出牌无玩家 压
                    takeCards = board.cardtip(player, board.getLast());
                } else {
                    takeCards = board.cardtip(player, null);
                }
            }

            if (takeCards.getCards() == null) {
                takeCards.setAllow(false);    //没有 管的起的牌
            }

            logger.info("牌型提示 只放给提示者， 不放给其他人");
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_CARDTIPS, playUser.getId(), takeCards);
        }
        } catch (Exception e) {
            logger.error("玩家牌型提示失败", e);

        }
    }

    /**
     * 出牌
     *
     * @param cardstr
     * @param token
     * @param userid
     * @param orgi
     * @param auto
     * @return
     */
    public TakeCards takeCardsRequest(String cardstr, String token, String userid, String orgi, boolean auto) {

        try {
            TakeCards takeCards = null;
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            if (userToken != null) {
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                byte[] playCards = null;

                if (!StringUtils.isEmpty(cardstr)) {
                    String[] cards = cardstr.split(",");

                    playCards = new byte[cards.length];
                    for (int i = 0; i < cards.length; i++) {
                        playCards[i] = Byte.parseByte(cards[i]);
                    }
                }

                GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
                if (gameRoom != null) {
                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                    if (board != null) {
                        Player player = board.player(userid);
                        if (board.getNextplayer() != null && player.getPlayuser().equals(board.getNextplayer().getNextplayer()) && board.getNextplayer().isTakecard() == false) {
                            takeCards = board.takeCardsRequest(gameRoom, board, player, orgi, auto, playCards);
                        }
                    }
                }
            }

            return takeCards;
        } catch (Exception e) {
            logger.error("玩家出牌失败", e);
            return null;
        }
    }

    /**
     * 检查是否所有玩家 都已经处于就绪状态，如果所有玩家都点击了 继续开始游戏，则发送一个 ALL事件，继续游戏，
     * 否则，等待10秒时间，到期后如果玩家还没有就绪，就将该玩家T出去，等待新玩家加入
     *
     * @param roomid
     * @param userid
     * @param orgi
     * @return
     */
    public void restartRequest(String roomid, PlayUserClient playerUser, BeiMiClient beiMiClient, boolean opendeal) {
        boolean notReady = false;
        List<PlayUserClient> playerList = null;
        GameRoom gameRoom = null;
        if (!StringUtils.isBlank(roomid)) {
            gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, playerUser.getOrgi());
            playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            if (playerList != null && playerList.size() > 0) {
                /**
                 * 有一个 等待
                 */
                for (PlayUserClient player : playerList) {
                    if (player.isRoomready() == false) {
                        notReady = true;
                        break;
                    }
                }
            }
        }
        if (notReady == true && gameRoom != null) {
            /**
             * 需要增加一个状态机的触发事件：等待其他人就绪，超过5秒以后未就绪的，直接踢掉，然后等待机器人加入
             */
            this.startGameRequest(roomid, playerUser, playerUser.getOrgi(), opendeal);
        } else if (playerList == null || playerList.size() == 0 || gameRoom == null) {//房间已解散
            BMDataContext.getGameEngine().gameRequest(playerUser, beiMiClient);
            /**
             * 结算后重新开始游戏
             */
            playerUser.setRoomready(true);
            CacheHelper.getGamePlayerCacheBean().put(playerUser.getId(), playerUser, playerUser.getOrgi());
        }
    }

    /**
     * 结束 当前牌局
     *
     * @param orgi
     * @return
     */
    public void finished(GameRoom gameRoom, String orgi) {
        try{
        if (!StringUtils.isBlank(gameRoom.getId())) {//
            CacheHelper.getExpireCache().remove(gameRoom.getId());
            CacheHelper.getBoardCacheBean().delete(gameRoom.getId(), orgi);

            CacheHelper.getExpireCache().remove(gameRoom.getId());
            GameUtils.removeGameRoom(gameRoom.getId(), gameRoom.getPlayway(), gameRoom.getOrgi());
            CacheHelper.getGameRoomCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());
            CacheHelper.getBoardCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());
            List<PlayUserClient> playerUsers = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            for (PlayUserClient tempPlayUser : playerUsers) {
                CacheHelper.getRoomMappingCacheBean().delete(tempPlayUser.getId(), gameRoom.getOrgi());
            }
            CacheHelper.getGamePlayerCacheBean().clean(gameRoom.getId(), gameRoom.getOrgi());

            CacheHelper.getQueneCache().delete(gameRoom.getId());
            GameUtils.deleteUserRoom(gameRoom);
        }
        } catch (Exception e) {
            logger.error("牌局结束失败", e);
        }
    }

    /**
     * 恢复牌局
     *
     * @param board
     * @param gameRoom
     * @param currentPlayer
     */
    public void recovery(Board board, GameRoom gameRoom, Player currentPlayer) {
        try{
        boolean automic = false;
        if ((board.getLast() != null && board.getLast().getUserid().equals(currentPlayer.getPlayuser())) || (board.getLast() == null)) {
            automic = true;
        }

        RecoveryData recoveryData = new RecoveryData(currentPlayer, board.getLasthands(), board.getNextplayer() != null ? board.getNextplayer().getNextplayer() : null, 25, automic, board, gameRoom.getStatus());

        RoomdescUtil.formatRoomdesc(gameRoom, recoveryData.getUserboard());

        recoveryData.setRoomtype(gameRoom.getRoomtype());
        recoveryData.initTeahouseinfo(gameRoom);
        ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_RECOVERY, currentPlayer.getPlayuser(), recoveryData);
        } catch (Exception e) {
            logger.error("玩家恢复牌局失败", e);
        }
    }

    public void recovery(BeiMiClient beiMiClient, PlayUserClient playUserClient) {
        try {
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUserClient.getId(), playUserClient.getOrgi());
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(roomid, playUserClient.getOrgi());
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, playUserClient.getOrgi());

            if (null == board || null == roomid || null == gameRoom) {
                logger.info("未开牌 ，无需恢复牌局,直接发送player事件，玩家[" + playUserClient.getUsername() + "][" + playUserClient.getGameid() + "]");
                ActionTaskUtils.sendPlayers(beiMiClient, gameRoom);
                return;
            }
            recovery(board, gameRoom, board.player(playUserClient.getId()));
        } catch (Exception e) {
            logger.error("玩家恢复牌局失败", e);
        }

    }

    /**
     * 发送消息
     *
     * @param beiMiClient
     * @param msgEvent
     */
    public void sendmsg(BeiMiClient beiMiClient, MsgEvent msgevent) {
        try {

            String token = beiMiClient.getToken();
            if (!org.apache.commons.lang.StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(roomid, BMDataContext.SYSTEM_ORGI);

                    if (null != board && null != board.getNextplayer()) {
                        msgevent.setNextplayer(board.getNextplayer().getNextplayer());
                    }

                    ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_SENDMSG, msgevent, gameRoom);
                }
            }
        } catch (Exception e) {
            logger.error("玩家发送消息失败",e);
            e.printStackTrace();
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家发送异常", false));
        }
    }

    public int getDissSecond(String name) {
        return Integer.parseInt(TeahouseUtil.getConstantsByName(name));
    }

    /**
     * 解散房间
     *
     * @param gameRoom
     * @param board
     * @param userid
     * @param dissflag
     */
    public void mazigiveup(GameRoom gameRoom, Board board, String userid, int giveupflag, boolean firstflag) {


        try {

            logger.info("投降 userid [" + userid + "] giveupflag[" + giveupflag + "] firstflag[" + firstflag + "]");

            if (StringUtils.isEmpty(RoomUtil.getCreatevalue(gameRoom, "other:giveupflag"))) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, userid, new NoticeEvent("房间玩法为不可投降", false));
                return;
            }

            if (board.isFinished()) {
                logger.info("房间roomid[" + gameRoom.getRoomid() + "]结算后， 不能再次投降");
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, userid, new NoticeEvent("结算后不能再次投降", false));
                return;
            }

            List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            boolean giveup = true;

            List<GiveupInfo> giveupInfoList = new LinkedList<GiveupInfo>();

            for (PlayUserClient playUserClient : playerUserList) {
                Player player = board.player(playUserClient.getId());
                if (userid.equals(playUserClient.getId()))
                    player.setGiveupflag(giveupflag);

                if (1 != player.getGiveupflag()) {
                    logger.info("player.getUsername() [" + player.getUsername() + "]");
                    giveup = false;
                }

                giveupInfoList.add(new GiveupInfo(playUserClient.getId(), playUserClient.getUsername(),
                        playUserClient.getUserurl(), player.getGiveupflag()));

            }

            if (firstflag) {
                logger.info("房间[" + board.getRoomid() + "]发起投降的人 userid [" + userid +
                        "] 玩家名字[" + board.player(userid).getUsername() + "]["
                        + board.player(userid).getGameid() + "] 编队[" + board.player(userid).getTeamflag() + "]");
                board.setGiveupuserid(userid);
                board.setGiveupbegindate(new Date(System.currentTimeMillis()));
                board.setGiveupsecond(getDissSecond("disssecond"));

                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.GIVEUP.toString(), board.getGiveupsecond());
                board.setGiveupteamflag(board.player(userid).getTeamflag());

            }

            if (giveupflag != 2) {//未点击拒绝
                if (giveup) {
                    giveupflag = 1;
                } else {
                    giveupflag = 0;
                }
            }

            GiveupEvent giveupEvent = new GiveupEvent();
            giveupEvent.setGiveupInfoList(giveupInfoList);
            giveupEvent.setGiveupflag(giveupflag);
            giveupEvent.setUserid(board.getGiveupuserid());
            giveupEvent.setGiveupbegindate(board.getGiveupbegindate());
            giveupEvent.setGiveupsecond(board.getGiveupsecond());
            giveupEvent.setDealflag(0);
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_MAZIGIVEUP, giveupEvent, gameRoom);

            if (giveup) {
                logger.info("所有人都已经同意投降，投降开始");
                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.STEPCLEARING.toString(), 3);    //通知状态机 , 继续执行
            } else {
                logger.info("还有玩家未同意投降");
            }
            if (giveupflag == 2) {
                for (Player player : board.getPlayers()) {
                    logger.info("投降恢复玩家 player[" + player.getPlayuser() + "] [" + player.getUsername() + "]的状态");
                    player.setGiveupflag(0);

                }
                board.setDisssecond(0);
                board.setDissuserid(null);
                board.setDissbegindate(null);

                board.setGiveupteamflag(0);
                board.setGiveupsecond(0);
                board.setGiveupuserid(null);
                board.setGiveupbegindate(null);
                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());

            }
            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
        } catch (Exception e) {
            logger.error("玩家投降失败", e);
        }

    }

    /**
     * 用户离线
     *
     * @param beiMiClient
     * @param client
     */
    public void disconnect(BeiMiClient beiMiClient, SocketIOClient client) {
        try {
            /**
             * 玩家离线
             */
            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getUserid(), beiMiClient.getOrgi());
            logger.info("用户离线，无需操作");

            if (playUserClient != null) {
                playUserClient.setOnline(false);//离线

                logger.info("玩家[" + playUserClient.getUsername() + "]  onDisconnect 离线");

                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUserClient.getId(), playUserClient.getOrgi());

                synchronized (this) {

                    beiMiClient.setClient(client);
                    beiMiClient.setUserid(playUserClient.getId());
                    beiMiClient.setSession(client.getSessionId().toString());
                    Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi());

                    NettyClients.getInstance().putClient(userToken, beiMiClient);

                    LeaveEvent eventobj = new LeaveEvent(playUserClient.getId(), false);
                    if (null == roomid) {//不在房间中
                        TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_LINESTATUS, playUserClient.getId(), eventobj);
                    } else {
                        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                        if (null != gameRoom) {
                            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_ROOMLINESTATUS, eventobj, gameRoom);
                        }

                        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

                        if ( (BMDataContext.GameStatusEnum.READY.toString().equals(playUserClient.getGamestatus())
                                && BMDataContext.GameStatusEnum.WAITTING.toString().equals(gameRoom.getStatus()))
                                && DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())
                                ) {
                            playUserClient.setRoomready(false);
                            playUserClient.setGamestatus(BMDataContext.GameStatusEnum.WAITTING.toString());

                            CacheHelper.getGamePlayerCacheBean().put(playUserClient.getId(), playUserClient, beiMiClient.getOrgi());
                            CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, beiMiClient.getOrgi());
                        }

                    }
                }

                BMDataContext.getContext().getBean(PlayUserClientRepository.class).save(playUserClient);

                logger.info("onDisconnect playUserClient.getGamestatus() [" + playUserClient.getGamestatus() + "]");
            }
        } catch (Exception e) {
            logger.error("玩家离线失败", e);
        }
    }

    /**
     * 同步游戏状态
     *
     * @param beiMiClient
     * @param client
     */
    public void gamestatus(BeiMiClient beiMiClient, SocketIOClient client) {
        try {
            Token userToken;
            GameStatus gameStatus = new GameStatus();
            gameStatus.setGamestatus(BMDataContext.GameStatusEnum.NOTREADY.toString());

            if (beiMiClient != null && !org.apache.commons.lang.StringUtils.isBlank(beiMiClient.getToken()) && (userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi())) != null) {

                //鉴权完毕
                PlayUserClient userClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                if (userClient != null) {
                    userClient.setOnline(true);//上线

                    beiMiClient.setClient(client);
                    beiMiClient.setUserid(userClient.getId());
                    beiMiClient.setSession(client.getSessionId().toString());
                    /**
                     * 心跳时间
                     */
                    beiMiClient.setTime(System.currentTimeMillis());
                    NettyClients.getInstance().putClient(userToken, beiMiClient);

                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userClient.getId(), userClient.getOrgi());
                    CacheHelper.getApiUserCacheBean().put(userClient.getId(), userClient, userClient.getOrgi());

                    LeaveEvent eventobj = new LeaveEvent(beiMiClient.getUserid(), true);
                    if (null == roomid) {//不在房间中
                        TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_LINESTATUS, beiMiClient.getUserid(), eventobj);
                    } else {
                        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
                        if (null != gameRoom) {
                            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_ROOMLINESTATUS, eventobj, gameRoom);
                        }
                    }

                    // 去掉校验 吴真
                    if (!org.apache.commons.lang.StringUtils.isBlank(roomid)) {
                        gameStatus.setUserid(userClient.getId());
                        gameStatus.setOrgi(userClient.getOrgi());

                        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, userClient.getOrgi());
                        if (null != gameRoom) {
                            logger.info("初次登录游戏 玩家[" + userClient.getUsername() + "] [" + userClient.getGameid() + "] 需要进入房间[" + gameRoom.getRoomid() + "]");
                            GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(gameRoom.getPlayway(), userClient.getOrgi());

                            gameStatus.setGametype(gamePlayway.getCode());
                            gameStatus.setPlayway(gamePlayway.getId());
                            //gameStatus.setGamestatus(BMDataContext.GameStatusEnum.PLAYING.toString());//此处不应该给状态 吴真
                            gameStatus.setGamestatus(gameRoom.getStatus());
                            gameStatus.setExtparams(gameRoom.getExtparams());
                            if (gameRoom.isCardroom()) {
                                gameStatus.setCardroom(true);
                            }
                            gameStatus.setRoomtype(gameRoom.getRoomtype());
                            gameStatus.initTeahouseinfo(gameRoom);

                        } else {
                            gameStatus.setExtparams(null);
                        }

                    }
                }
            } else {
                gameStatus.setGamestatus(BMDataContext.GameStatusEnum.TIMEOUT.toString());
            }
            beiMiClient.getClient().sendEvent(BMDataContext.BEIMI_GAMESTATUS_EVENT, gameStatus);
        } catch (Exception e) {
            logger.error("玩家游戏状态同步失败", e);
        }
    }
}
