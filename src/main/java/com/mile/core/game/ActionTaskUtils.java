package com.mile.core.game;

import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.config.web.model.Game;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.dizhu.task.CreateAutoTask;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.*;
import com.mile.core.game.pinshi.util.PinshiTipUtil;
import com.mile.util.GameUtils;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ActionTaskUtils {

    private static final Logger logger = LoggerFactory.getLogger(ActionTaskUtils.class);

    /**
     * @param times
     * @param gameRoom
     * @return
     */
    public static AbstractTask createAutoTask(int times, GameRoom gameRoom) {
        return new CreateAutoTask(times, gameRoom, gameRoom.getOrgi());
    }

    public static void sendEvent(String event, Message message, GameRoom gameRoom) {
        if (null == gameRoom) {
            logger.info("sendEvent gameRoom is null ");
            return;
        }
        message.setCommand(event);
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if (null != players && !players.isEmpty()) {
            logger.info("发送房间["+event+"]事件 roomid["+gameRoom.getRoomid()+"],所有玩家个数为["+players.size()+"] 消息内容["+message+"]");
            for (PlayUserClient user : players) {
                BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
                logger.info("发送房间事件 client.getSessionId()["+client.getClient().getSessionId()+"] userToken["+user.getToken()+"] userid["+user.getId()+"]");

                if (client != null && online(user.getId(), user.getOrgi())) {
                    client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, message);
                }
            }
        }
    }

    public static void sendEvent(String event, String userid, Message message) {
        message.setCommand(event);
        BeiMiClient client = NettyClients.getInstance().getClient(userid);
        if (client != null) {
            if (online(userid, client.getOrgi())) {
                client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, message);
            }
        }
    }

    /**
     * 通知就绪
     *
     * @param gameRoom
     * @param game
     */
    public static void roomReady(GameRoom gameRoom, Game game, PlayUserClient playUser) {
        /**
         *
         */
        boolean enough = false;
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (isfull(gameRoom,playerList)) {

            boolean hasnotready = false;
            for (PlayUserClient player : playerList) {
                if (player.isRoomready() == false) {
                    hasnotready = true;
                    break;
                }
            }
            if (hasnotready == false) {
                enough = true;
                gameRoom.setStatus(MileGameEnum.READY.toString());
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_ROOMREADY, new RoomReady(gameRoom), gameRoom);

            }else{

            }

        } else {
            gameRoom.setStatus(MileGameEnum.WAITTING.toString());
        }

        if (null != playUser) {
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_PLAYERREADY, new RoomReady(gameRoom, playUser.getId()), gameRoom);
        }

        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

        /**
         * 所有人都已经举手
         */
        if (enough == true) {
            if( DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
                if(gameRoom.isStartflag() || PinshiTipUtil.getStartflag(gameRoom,playerList)){
                    game.change(gameRoom, MileGameEvent.ENOUGH.toString());    //通知状态机 , 此处应由状态机处理异步执行
                }
            }else{
                game.change(gameRoom, MileGameEvent.ENOUGH.toString());    //通知状态机 , 此处应由状态机处理异步执行
            }

        }
    }



    private static boolean isfull(GameRoom gameRoom,List<PlayUserClient> playerList){
        if(DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())){
            return gameRoom.getPlayers() == playerList.size();
        }else if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
            return playerList.size() > 1;
        }else {
            logger.info(" 玩法code["+gameRoom.getPlaywaycode()+"] 不正确 ！！！！");
            return false;
        }

    }



    public static void roomReady(GameRoom gameRoom, Game game) {
        roomReady(gameRoom, game, null);
    }

    /**
     * 发送消息给 玩家
     *
     * @param beiMiClient
     * @param event
     * @param gameRoom
     */
    public static void sendPlayers(BeiMiClient beiMiClient, GameRoom gameRoom) {
        if (online(beiMiClient.getUserid(), beiMiClient.getOrgi())) {
            List<PlayUserClient> playUserClientList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), beiMiClient.getOrgi());
            if(null != playUserClientList && !playUserClientList.isEmpty()){
                for(PlayUserClient p:playUserClientList) {
                    logger.info("palyer事件位置 玩家 [" + p.getUsername() + "][" + p.getGameid() + "] 位置[" + p.getIndex() + "] 状态["+p.getGamestatus()+"]");
                }

            }
            beiMiClient.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, new GamePlayers(gameRoom.getPlayers(),
                    playUserClientList ,
                    DataConstants.GAME_EVENT_PLAYERS));
        }
    }

    /**
     * 检查玩家是否在线
     *
     * @param userid
     * @param orgi
     * @return
     */
    public static boolean online(String userid, String orgi) {
        PlayUserClient playerUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userid, orgi);
        //&& !BMDataContext.PlayerTypeEnum.LEAVE.toString().equals(playerUserClient.getPlayertype()
        if(null == playerUserClient){
            logger.info("用户["+userid+"] [] 未上线， 不发送事件给他");
        }
        return playerUserClient != null;
    }

    /**
     * @param gameRoom
     * @param players
     */
    public static void sendPlayers(GameRoom gameRoom, List<PlayUserClient> players) {
        for (PlayUserClient user : players) {
            BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
            if (client != null && online(client.getUserid(), client.getOrgi())) {
                client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT,
                        new GamePlayers(gameRoom.getPlayers(),gameRoom.getCreater(),
                        CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), client.getOrgi()),
                        DataConstants.GAME_EVENT_PLAYERS));
            }else{
                logger.info("用户["+user.getUsername()+"] ["+user.getGameid()+"] 未上线， 不发送玩家事件");
            }
        }
    }

    /**
     * 发送消息给 玩家
     *
     * @param beiMiClient
     * @param event
     * @param gameRoom
     */
    public static void sendEvent(PlayUserClient playerUser, Message message) {
        if (online(playerUser.getId(), playerUser.getOrgi())) {
            NettyClients.getInstance().sendGameEventMessage(playerUser.getId(), BMDataContext.BEIMI_MESSAGE_EVENT, message);
        }
    }

    /**
     * 发送消息给 玩家
     *
     * @param beiMiClient
     * @param event
     * @param gameRoom
     */
    public static void sendEvent(String userid, Message message) {
        BeiMiClient client = NettyClients.getInstance().getClient(userid);
        if (client != null && online(userid, client.getOrgi())) {
            NettyClients.getInstance().sendGameEventMessage(userid, BMDataContext.BEIMI_MESSAGE_EVENT, message);
        }
    }

    public static PlayUserClient getPlayUserClient(String roomid, String player, String orgi) {
        PlayUserClient playUserClient = null;
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(roomid, orgi);
        for (PlayUserClient user : players) {
            if (player.equals(user.getId())) {
                playUserClient = user;
            }
        }
        return playUserClient;
    }

    /**
     * 更新玩家状态
     *
     * @param userid
     * @param orgi
     */
    public static void updatePlayerClientStatus(PlayUserClient playUser, String status) {
        GameUtils.updatePlayerClientStatus(playUser.getId(), playUser.getOrgi(), status);
    }

    public static Object json(Object data) {
        return UKTools.json(data);
    }

    /**
     * 临时放这里，重构的时候 放到 游戏类型的 实现类里
     * 抢地主的时候，首个抢地主 不翻倍
     *
     * @param board
     * @param player
     * @return
     */
    public static DiZhuBoard doCatch(DiZhuBoard board, Player player, boolean result) {
        player.setAccept(result); //抢地主
        player.setDocatch(true);
        board.setDocatch(true);
        if (result) {    //抢了地主
            if (board.isAdded() == false) {
                board.setAdded(true);
            } else {
                board.setRatio(board.getRatio() * 2);
            }
            board.setBanker(player.getPlayuser());
        }
        return board;
    }

    /**
     * 临时放这里，重构的时候 放到 游戏类型的 实现类里
     *
     * @param board
     * @param player
     * @return
     */
    public static void doBomb(Board board, boolean add) {
        if (add) {    //抢了地主
            board.setRatio(board.getRatio() * 2);
        }
    }

    /**
     * 校验当前出牌是否合规
     *
     * @param playCardType
     * @param lastCardType
     * @return
     */
    public static boolean allow(CardType playCardType, CardType lastCardType) {
        boolean allow = false;
        if (playCardType.isKing()) {    //王炸，无敌
            allow = true;
        } else if (playCardType.isBomb()) {
            if (lastCardType.isBomb()) { //都是炸弹
                if (playCardType.getMaxcard() > lastCardType.getMaxcard()) {
                    allow = true;
                }
            } else if (lastCardType.isKing()) {
                allow = false;
            } else {
                allow = true;
            }
        } else if (lastCardType.isBomb()) {    //最后一手牌是炸弹 ， 当前出牌不是炸弹
            allow = false;
        } else if (playCardType.getCardtype() == lastCardType.getCardtype() && playCardType.getCardtype() > 0 && lastCardType.getCardtype() > 0) {
            if (playCardType.getMaxcard() > lastCardType.getMaxcard()) {
                allow = true;
            } else if (playCardType.getMaxcardvalue() == 53) {
                allow = true;
            }
        }
        return allow;
    }

    /**
     * 校验当前出牌是否合规
     *
     * @param playCardType
     * @param lastCardType
     * @return
     */
    public static boolean maziAllow(CardType playCardType, CardType lastCardType) {
        boolean allow = false;

        if (playCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.NONE.getType())
            allow = false;

        if (lastCardType.getMazicardtype() < 10 && playCardType.getMazicardtype() > 10) {
            allow = true;
        } else if ((playCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB5.getType()
                && lastCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB5.getType())
                || (playCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB6.getType()
                && lastCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB6.getType())
                || (playCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB7.getType()
                && lastCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB7.getType())
                || (playCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB8.getType()
                && lastCardType.getMazicardtype() == BMDataContext.MaziCardsTypeEnum.BOMB8.getType())) {
            if (playCardType.getMincard() > lastCardType.getMincard())
                allow = true;
            else
                allow = false;
        } else if (lastCardType.getMazicardtype() > 10 && playCardType.getMazicardtype() > 10) {
            allow = playCardType.getMazicardtype() > lastCardType.getMazicardtype();
        } else if (lastCardType.getMazicardtype() < 10 && playCardType.getMazicardtype() < 10
                && lastCardType.getMazicardtype() != playCardType.getMazicardtype()) {
            allow = false;
        } else if (lastCardType.getMazicardtype() < 10 && playCardType.getMazicardtype() < 10
                && lastCardType.getMazicardtype() == playCardType.getMazicardtype()) {
            allow = ((playCardType.getMincard() > lastCardType.getMincard())
                    && playCardType.getTypesize() == lastCardType.getTypesize());
        } else
            allow = false;
        return allow;
    }

    /**
     * 分类
     *
     * @param cards
     * @return
     */
    public static Map<Integer, Integer> type(byte[] cards) {
        Map<Integer, Integer> types = new HashMap<Integer, Integer>();
        for (int i = 0; i < cards.length; i++) {
            int card = cards[i] / 4;
            if (types.get(card) == null) {
                types.put(card, 1);
            } else {
                types.put(card, types.get(card) + 1);
            }
        }
        return types;
    }

    /**
     * 牌型识别
     *
     * @param cards
     * @return
     */
    public static CardType identification(byte[] cards) {
        CardType cardTypeBean = new CardType();
        Map<Integer, Integer> types = new HashMap<Integer, Integer>();

        //max 最大牌的张数 ，mincard最小的牌    maxcard 最大的牌       min 最小的牌的张数
        int max = -1, maxcard = -1, cardtype = 0, mincard = -1, min = 100;
        for (int i = 0; i < cards.length; i++) {
            int card = cards[i] / 4;
            if (types.get(card) == null) {
                types.put(card, 1);
            } else {
                types.put(card, types.get(card) + 1);
            }
            if (types.get(card) > max) {
                max = types.get(card);
                maxcard = card;
            }
            if (types.get(card) == max) {
                if (mincard < 0 || mincard > card) {
                    mincard = card;
                }
            }

            if (cards[i] > cardTypeBean.getMaxcardvalue()) {
                cardTypeBean.setMaxcardvalue(cards[i]);
            }
        }

        Iterator<Integer> iterator = types.keySet().iterator();
        while (iterator.hasNext()) {
            Integer key = iterator.next();
            if (types.get(key) < min) {
                min = types.get(key);
            }
        }

        cardTypeBean.setCardnum(max);
        cardTypeBean.setMincard(mincard);
        cardTypeBean.setTypesize(types.size());
        cardTypeBean.setMaxcard(maxcard);

        switch (types.size()) {
            case 1:
                switch (max) {
                    case 1:
                        cardtype = BMDataContext.CardsTypeEnum.ONE.getType();
                        break;        //单张
                    case 2:
                        if (mincard == 13) {
                            cardtype = BMDataContext.CardsTypeEnum.ELEVEN.getType();
                        } else {
                            cardtype = BMDataContext.CardsTypeEnum.TWO.getType();
                        }
                        break;        //一对
                    case 3:
                        cardtype = BMDataContext.CardsTypeEnum.THREE.getType();
                        break;        //三张
                    case 4:
                        cardtype = BMDataContext.CardsTypeEnum.TEN.getType();
                        break;        //炸弹
                }
                ;
                break;
            case 2:
                switch (max) {
                    case 3:
                        if (min == 1) {//三带一
                            cardtype = BMDataContext.CardsTypeEnum.FOUR.getType();
                        } else if (min == 2) {//三带一对
                            cardtype = BMDataContext.CardsTypeEnum.FORMTWO.getType();
                        } else if (min == 3) {//飞机不带
                            cardtype = BMDataContext.CardsTypeEnum.SEVEN.getType();
                        }
                        break;
                    case 4:
                        cardtype = BMDataContext.CardsTypeEnum.NINE.getType();
                        break;    //四带一对
                }
                ;
                break;
            case 3:
                switch (max) {
                    case 1:
                        ;
                        break;    //无牌型
                    case 2:
                        if (cards.length == 6 && isAva(types, mincard)) {
                            cardtype = BMDataContext.CardsTypeEnum.SIX.getType();
                        }
                        break;        //3连对
                    case 3:
                        if (isAva(types, mincard) && min == max) {
                            cardtype = BMDataContext.CardsTypeEnum.SEVEN.getType();
                        }
                        break;        //三顺
                    case 4:
                        if (cards.length == 6 || cards.length == 8) {
                            cardtype = BMDataContext.CardsTypeEnum.NINE.getType();
                        }
                        break;        //四带二
                }
                break;
            case 4:
                switch (max) {
                    case 1:
                        ;
                        break;        //无牌型
                    case 2:
                        if (cards.length == 8 && isAva(types, mincard)) {
                            cardtype = BMDataContext.CardsTypeEnum.SIX.getType();
                        }
                        break;        //4连对
                    case 3:
                        if (isAva(types, mincard)) {
                            if (cards.length == 8) {
                                cardtype = BMDataContext.CardsTypeEnum.EIGHT.getType();
                            } else if (cards.length == 10) {
                                cardtype = BMDataContext.CardsTypeEnum.EIGHTONE.getType();
                            }
                        }
                        break;        //飞机
                }
                ;
                break;
            case 5:
                switch (max) {
                    case 1:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.FIVE.getType();
                        }
                        break;        //连子
                    case 2:
                        if (cards.length == 10 && isAva(types, mincard)) {
                            cardtype = BMDataContext.CardsTypeEnum.SIX.getType();
                        }
                        break;        //5连对
                    case 3:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.SEVEN.getType();
                        }
                        break;        //5飞机
                }
                ;
                break;
            case 6:
                switch (max) {
                    case 1:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.FIVE.getType();
                        }
                        break;        //连子
                    case 2:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.SIX.getType();
                        }
                        break;        //6连对
                    case 3:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.SEVEN.getType();
                        }
                        break;        //6飞机
                }
                ;
                break;
            default:
                switch (max) {
                    case 1:
                        if (isAva(types, mincard)) {
                            cardtype = BMDataContext.CardsTypeEnum.FIVE.getType();
                        }
                        break;        //连子
                    case 2:
                        if (isAva(types, mincard) && max == min) {
                            cardtype = BMDataContext.CardsTypeEnum.SIX.getType();
                        }
                        break;        //连对
                }
                ;
                break;
        }
        cardTypeBean.setCardtype(cardtype);
        cardTypeBean.setKing(cardtype == BMDataContext.CardsTypeEnum.ELEVEN.getType());
        cardTypeBean.setBomb(cardtype == BMDataContext.CardsTypeEnum.TEN.getType());

        //给一个码子牌型  吴真
        cardTypeBean.setMazicardtype(MaziCardtypeUtil.getMaziCardtype(cards));
        return cardTypeBean;
    }

    public static CardType identificationMazi(List<Byte> cards) {
        if (null != cards && !cards.isEmpty()) {
            byte[] b = new byte[cards.size()];
            for (int i = 0; i < cards.size(); i++) {
                b[i] = cards.get(i);
            }

            return identificationMazi(b);

        } else
            return null;

    }

    public static CardType identificationMazi(Byte[] cards) {
        if (null != cards && cards.length > 0) {
            byte[] b = new byte[cards.length];
            for (int i = 0; i < cards.length; i++) {
                b[i] = cards[i];
            }
            return identificationMazi(b);

        } else
            return null;

    }

    /**
     * 牌型识别
     *
     * @param cards
     * @return
     */
    public static CardType identificationMazi(byte[] cards) {

        Arrays.sort(cards);
        cards = (GameUtils.reverseCards(cards));

        CardType cardTypeBean = new CardType();
        Map<Integer, Integer> types = new HashMap<Integer, Integer>();

        //max 最大牌的张数 ，mincard最小的牌    maxcard 最大的牌       min 最小的牌的张数
        int max = -1, maxcard = -1, cardtype = 0, mincard = -1, min = 100;
        for (int i = 0; i < cards.length; i++) {
            int card = cards[i] / 8;

            if (cards[i] > 105)
                card++;//大鬼加一

            if (types.get(card) == null) {
                types.put(card, 1);
            } else {
                types.put(card, types.get(card) + 1);
            }
            if (types.get(card) > max) {
                max = types.get(card);
                maxcard = card;
            }
            if (types.get(card) == max) {
                if (mincard < 0 || mincard > card) {
                    mincard = card;
                }
            }

            if (cards[i] > cardTypeBean.getMaxcardvalue()) {
                cardTypeBean.setMaxcardvalue(cards[i]);
            }
        }

        Iterator<Integer> iterator = types.keySet().iterator();
        while (iterator.hasNext()) {
            Integer key = iterator.next();
            if (types.get(key) < min) {
                min = types.get(key);
            }
        }

        cardTypeBean.setCardnum(max);
        cardTypeBean.setMincard(mincard);
        cardTypeBean.setTypesize(types.size());
        cardTypeBean.setMaxcard(maxcard);

        cardTypeBean.setKing(cardtype == BMDataContext.CardsTypeEnum.ELEVEN.getType());
        cardTypeBean.setBomb(cardtype == BMDataContext.CardsTypeEnum.TEN.getType());

        //给一个码子牌型  吴真
        cardTypeBean.setMazicardtype(MaziCardtypeUtil.getMaziCardtype(cards));
        cardTypeBean.setScore(MaziScoreUtil.getScore(cards));

        return cardTypeBean;
    }

    private static boolean isAva(Map<Integer, Integer> types, int mincard) {
        boolean ava = true;
        for (int i = mincard; i < (mincard + types.size()); i++) {
            if (types.get(i) == null) {
                ava = false;
            }
        }
        return ava;
    }




}
