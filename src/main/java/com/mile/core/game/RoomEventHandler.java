package com.mile.core.game;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.LeaveEvent;
import com.mile.core.engine.game.eventmodel.MsgEvent;
import com.mile.core.entity.*;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.GameStatus;
import com.mile.core.game.msgmodel.SearchRoom;
import com.mile.core.game.msgmodel.SearchRoomResult;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.util.UKTools;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RoomEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(RoomEventHandler.class);

    protected SocketIOServer server;

    @Autowired
    public RoomEventHandler(SocketIOServer server) {
        this.server = server;
    }

    //进入房间
    @OnEvent(value = DataConstants.GAME_EVENT_JOINROOM)
    public void onJoinRoom(SocketIOClient client, AckRequest request, String data) {
        try {
            logger.info("加入房间         参数列表data[" + data + "]");
            NettyClients.getInstance().getClientByData(client, data);
            Date connectbegin = new Date(System.currentTimeMillis());
            BeiMiClient beiMiClient = JSON.parseObject(data, BeiMiClient.class);
            String token = beiMiClient.getToken();

            if (!StringUtils.isBlank(token)) {

                Token userToken;
                if (beiMiClient != null && !StringUtils.isBlank(token) && (userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, beiMiClient.getOrgi())) != null) {
                    //鉴权完毕
                    PlayUserClient userClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                    /**
                     * 更新当前玩家状态，在线|离线
                     */
                    userClient.setOnline(true);
                    beiMiClient.setClient(client);
                    beiMiClient.setUserid(userClient.getId());
                    beiMiClient.setSession(client.getSessionId().toString());
                    /**
                     * 心跳时间
                     */
                    beiMiClient.setTime(System.currentTimeMillis());
                    NettyClients.getInstance().putClient(userToken, beiMiClient);

                    /**
                     * 更新状态
                     */
                    ActionTaskUtils.updatePlayerClientStatus(userClient, BMDataContext.PlayerTypeEnum.NORMAL.toString());

                    //??????不清楚下面这个方法需要干什么
                    UKTools.published(userClient, BMDataContext.getContext().getBean(PlayUserClientRepository.class));

                    BMDataContext.getGameEngine().gameRequest(userClient, beiMiClient);

                    CacheHelper.getApiUserCacheBean().put(userClient.getId(), userClient, userClient.getOrgi());

                }
            }

            Date connectend = new Date(System.currentTimeMillis());
            logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 加入房间开始时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectbegin));
            logger.info("client.getSessionId().toString() [" + client.getSessionId().toString() + "] 加入房间结束时间" + new SimpleDateFormat("yyyyMMdd HHmmssSSS").format(connectend));
        } catch (Exception e) {
            logger.error("加入房间失败", e);
        }

    }

    //查找房间
    @OnEvent(value = DataConstants.GAME_EVENT_SEARCHROOM)
    public void onSearchRoom(SocketIOClient client, String data) {
        NettyClients.getInstance().getClientByData(client, data);
        SearchRoom searchRoom = JSON.parseObject(data, SearchRoom.class);

        try {

            logger.info("查询房间    参数列表data[" + data + "]");

            GamePlayway gamePlayway = null;
            SearchRoomResult searchRoomResult = null;
            boolean joinRoom = false;

            String result = "";

            if (searchRoom != null && !StringUtils.isBlank(searchRoom.getUserid())) {
                GameRoomRepository gameRoomRepository = BMDataContext.getContext().getBean(GameRoomRepository.class);
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(searchRoom.getUserid(), searchRoom.getOrgi());
                if (playUser != null) {
                    GameRoom gameRoom = null;

                    //得到他之前加入房间的ID。非本次加入的ID
                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());

                    GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();

                    if (!StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
                        gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, playUser.getOrgi());

                    }

                    if (null == gameRoom) {
                        CacheHelper.getRoomMappingCacheBean().delete(playUser.getId(), playUser.getOrgi());
                        gameRoom = gameRoomCache.getGameroomByRoomid(searchRoom.getRoomid());
                    }

                    if (gameRoom != null) {

                        if (!StringUtils.isEmpty(gameRoom.getTeanum())) {
                            logger.info("非茶楼人员不能进入房间");
                            result = BMDataContext.SearchRoomResultType.NOTEA.toString();
                        }

                        /**
                         * 将玩家加入到 房间 中来 ， 加入的时候需要处理当前的 房间 已满员或未满员，如果满员，需要检查是否允许围观
                         */
                        gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(gameRoom.getPlayway(), gameRoom.getOrgi());
                        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                        if (playerList.size() < gamePlayway.getPlayers() && !BMDataContext.SearchRoomResultType.NOTEA.toString().equals(result)) {
                            BMDataContext.getRoomEngine().joinRoom(gameRoom, playUser, playerList);
                            joinRoom = true;
                        }
                        /**
                         * 获取的玩法，将玩法数据发送给当前请求的玩家
                         */
                    }
                }
            }
            if (gamePlayway != null) {
                //通知客户端
                if (joinRoom == true) {        //加入成功 ， 是否需要输入加入密码？
                    searchRoomResult = new SearchRoomResult(gamePlayway.getId(), gamePlayway.getCode(), BMDataContext.SearchRoomResultType.OK.toString());
                } else {                        //加入失败

                    searchRoomResult = new SearchRoomResult(BMDataContext.SearchRoomResultType.FULL.toString());
                }
            } else { //房间不存在
                result = StringUtils.isEmpty(result) ? BMDataContext.SearchRoomResultType.NOTEXIST.toString() : result;
                searchRoomResult = new SearchRoomResult(result);
            }

            client.sendEvent(DataConstants.GAME_EVENT_SEARCHROOM, searchRoomResult);

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("查询房间异常", e);
            MaziTakeCards takeCards = new MaziTakeCards();
            takeCards.setAllow(false);
            takeCards.setTipstr("加入房间异常data[" + data + "]");
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, searchRoom.getUserid(), new NoticeEvent("加入房间异常", false));

        }
    }

    @OnEvent(value = DataConstants.GAME_EVENT_ROOMLINESTATUS)
    public void roomlinestatus(SocketIOClient client, String data) {
        try {
            logger.info("上线下线通知 data[" + data + "] client.getSessionId()[" + client.getSessionId() + "]");

            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
            Token userToken;
            if (beiMiClient != null && !StringUtils.isBlank(beiMiClient.getToken()) && (userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi())) != null) {

                logger.info("上线下线通知 data[" + data + "] client.getSessionId()[" + client.getSessionId() + "] userToken[" + userToken.getId() + "] userid[" + userToken.getUserid() + "]");

                //鉴权完毕
                PlayUserClient userClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                if (userClient != null) {
                    BMDataContext.getRoomEngine().roomlinestatus(userClient, beiMiClient);
                }
            }
        } catch (Exception e) {
            logger.error("玩家上下线失败", e);
        }
    }

    //玩家离开
    @OnEvent(value = DataConstants.GAME_EVENT_LEAVE)
    public void onLeave(SocketIOClient client, String data) {

        logger.info("玩家离开房间  发送消息 传入  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

        try {
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                BMDataContext.getRoomEngine().leaveRoom(token, "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("玩家离开房间异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家离开房间异常", false));
        }
    }

    //解散房间
    @OnEvent(value = DataConstants.GAME_EVENT_DISSROOM)
    public void onDissroom(SocketIOClient client, String data) {
        try {
            logger.info("data " + data);

            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

            int dissflag = Integer.parseInt(JSON.parseObject(data).getString("dissflag"));
            boolean firstflag = Boolean.parseBoolean(JSON.parseObject(data).getString("firstflag"));

            BMDataContext.getRoomEngine().dissroom(beiMiClient, dissflag, firstflag);
        } catch (Exception e) {
            logger.error("玩家解散房间失败", e);
        }

    }

}