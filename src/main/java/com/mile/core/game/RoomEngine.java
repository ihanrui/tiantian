package com.mile.core.game;

import com.corundumstudio.socketio.SocketIOServer;
import com.mile.common.BaseEngine;
import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.common.cache.hazelcast.impl.QueneCache;
import com.mile.common.client.NettyClients;
import com.mile.common.entity.Playway;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.DissroomEvent;
import com.mile.core.engine.game.eventmodel.DissroomInfo;
import com.mile.core.engine.game.eventmodel.LeaveEvent;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.*;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.RoomdescUtil;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.eventmodel.TableInfo;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.core.teahouse.util.TeahouseeventUtil;
import com.mile.util.GameUtils;
import com.mile.util.RandomCharUtil;
import com.mile.util.UKTools;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service(value = "roomEngine")
public class RoomEngine extends BaseEngine {

    private static final Logger logger = LoggerFactory.getLogger(RoomEngine.class);

    @Resource
    private KieSession kieSession;

    /**
     * 玩家加入房间
     *
     * @param gameRoom
     * @param playUser
     * @param playerList
     */
    public synchronized void joinRoom(GameRoom gameRoom, PlayUserClient playUser, List<PlayUserClient> playerList) {
        try {
            boolean inroom = false;
            for (PlayUserClient user : playerList) {
                if (user.getId().equals(playUser.getId())) {
                    inroom = true;
                    break;
                }
            }

            if (inroom == false) {

                if (null != playerList && !playerList.isEmpty() && null != gameRoom.getPlayers() &&
                        gameRoom.getPlayers() <= playerList.size()) {
                    logger.info("roomid[" + gameRoom.getRoomid() + "] 人数已达到上限 [" + playUser.getUsername() + "]["
                            + playUser.getGameid()
                            + "] 无法加入");
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, playUser.getId(), new NoticeEvent("玩家个数已达到上限，无法加入", false));

                    return;
                }

                //处理金币场
                if (DataConstants.Roomtype.COIN.toString().equals(gameRoom.getRoomtype())) {

                    GamePlayway gamePlayway = BMDataContext.getContext().getBean(GamePlaywayRepository.class).findById(gameRoom.getPlayway());

                    if (null == playUser.getGoldcoins() || gamePlayway.getMincoins() > playUser.getGoldcoins()) {
                        TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, playUser.getId(),
                                new NoticeEvent("玩家金币[" + playUser.getGoldcoins() + "]低于[" + gamePlayway.getMincoins() + "],请充值后继续加入!!!!", false));
                        return;
                    }

                }

                playUser.setPlayerindex(System.currentTimeMillis());
                playUser.setGamestatus(RoomUtil.getJoinStatus(gameRoom));
                playUser.setPlayertype(BMDataContext.PlayerTypeEnum.NORMAL.toString());
                playUser.setRoomid(gameRoom.getId());
                playUser.setRoomready(false);

                if (DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())) {
                    logger.info(" joinRoom 麻子无旁观者 设置座位号");
                    playUser.setIndex(RoomUtil.getIndex(playerList, playUser));//设置座位号
                } else if (DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())) {
                    logger.info("joinRoom 牛牛有旁观者 不设置座位号");
                }

                playerList.add(playUser);
                NettyClients.getInstance().joinRoom(playUser.getId(), gameRoom.getId());
                CacheHelper.getGamePlayerCacheBean().put(playUser.getId(), playUser, playUser.getOrgi()); //将用户加入到 room ， MultiCache
                CacheHelper.getApiUserCacheBean().put(playUser.getId(), playUser, playUser.getOrgi()); //将用户加入到 room ， MultiCache
            }

            /**
             *	不管状态如何，玩家一定会加入到这个房间
             */
            logger.info("用户成功加入房间 playUser[" + playUser.getId() + "]gameRoom[" + gameRoom + "]");

            CacheHelper.getRoomMappingCacheBean().put(playUser.getId(), gameRoom.getId(), playUser.getOrgi());

            logger.info("本次获取房间ID参数为 playUser.getId()【" + playUser.getId() + "】 playUser.getOrgi()【" + playUser.getOrgi() + "】 ");
            String testRoomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());

            logger.info("本次用户的房间ID为testRoomid [" + testRoomid + "] ");
        } catch (Exception e) {
            logger.error("玩家加入房间失败", e);
        }

    }

    /**
     * 加入房间，房卡游戏
     *
     * @param roomid
     * @param orgi
     * @return
     */
    public GameRoom joinRoom(String roomid, PlayUserClient playUser, String orgi) {
        try {
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);
            if (gameRoom != null) {
                CacheHelper.getGamePlayerCacheBean().put(gameRoom.getId(), playUser, orgi); //将用户加入到 room ， MultiCache
            }
            return gameRoom;
        } catch (Exception e) {
            logger.error("玩家加入房间失败", e);
            return null;
        }
    }

    /**
     * 退出房间
     * 1、房卡模式，userid是房主，则解散房间
     * 2、大厅模式，如果游戏未开始并且房间仅有一人，则解散房间
     *
     * @param orgi
     * @return
     */
    public GameRoom leaveRoom(PlayUserClient playUser, String orgi) {
        try {
            GameRoom gameRoom = whichRoom(playUser.getId(), orgi);
            if (gameRoom != null) {
                List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi);

                if (null == players || players.isEmpty()) {
                    logger.info("房间 [" + gameRoom.getRoomid() + "] 没有任何玩家，直接返回");
                    return gameRoom;
                }

                if (players.size() == 1) {
                    logger.info("离开房间 playUser.getGameid() [" + playUser.getGameid() + "] players.get(0).getGameid() [" + players.get(0).getGameid() + "]");
                }

                if (players.size() <= 1) {
                    //解散房间 , 保留 ROOM资源 ， 避免 从队列中取出ROOM
                    CacheHelper.getGamePlayerCacheBean().clean(gameRoom.getId(), orgi);
                    logger.info("房间  [" + gameRoom.getRoomid() + "] 小于一个人 ");
                    CacheHelper.getGameRoomCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());
                    CacheHelper.getRoomMappingCacheBean().delete(playUser.getId(), gameRoom.getOrgi());
                    BMDataContext.getContext().getBean(GameRoomRepository.class).delete(gameRoom);

                    PlayUserClient del = null;
                    for (PlayUserClient p : players) {
                        if (null != p && p.getId().equals(playUser.getId())) {
                            del = p;
                        }
                    }
                    players.remove(del);

                } else {
                    logger.info("gameRoom.getStatus()   [" + gameRoom.getStatus() + "] 正在打牌， [" + playUser.getUsername() + "] [" + playUser.getRoomid() + "] [" + playUser.getGamestatus() + "]");

                    if (!MileGameEnum.PLAY.toString().equals(gameRoom.getStatus())) {
                        CacheHelper.getGamePlayerCacheBean().delete(playUser.getId(), orgi);
                        CacheHelper.getRoomMappingCacheBean().delete(playUser.getId(), gameRoom.getOrgi());
                        PlayUserClient del = null;
                        for (PlayUserClient p : players) {
                            if (null != p && p.getId().equals(playUser.getId())) {
                                del = p;
                            }
                        }
                        players.remove(del);
                    } else {
                        logger.info("gameRoom.getStatus()   [" + gameRoom.getStatus() + "] 正在打牌，不能退出 [" + playUser.getUsername() + "] [" + playUser.getRoomid() + "] [" + playUser.getGamestatus() + "]");
                    }
                }

                if (!StringUtils.isEmpty(gameRoom.getTeanum())) {
                    TableInfo tableInfo = new TableInfo();
                    tableInfo.setUserInfoList(TeahouseUtil.formatPlayer2Userinfo(players));
                    UKTools.copyProperties(gameRoom, tableInfo);
                    tableInfo.setNum(players.size());
                    tableInfo.setBegindate(gameRoom.getCreatetime());

                    TeahouseActionTaskUtils.sendTeahouseEvent(DataConstants.TEAHOUSE_EVENT_TABLECHANGE, gameRoom.getTeanum(), tableInfo);
                } else {
                    logger.info("未在茶楼，不用发送事件");
                }

                putCoinroom(gameRoom);

            } else {
                logger.info("房间 [" + gameRoom.getRoomid() + "] 未在缓存中，直接返回NULL");
            }

            return gameRoom;
        } catch (Exception e) {
            logger.error("玩家离开房间失败", e);
            return null;
        }
    }

    public void leaveRoom(String token, String changeroom) {
        try {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

            GameRoom gameRoom = null;
            if (!org.apache.commons.lang.StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
                gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, userToken.getOrgi());
            }
            if (userToken != null) {
                //GameUtils.updatePlayerClientStatus(beiMiClient.getUserid(), beiMiClient.getOrgi(), BMDataContext.PlayerTypeEnum.LEAVE.toString());
                PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), "");
                playUserClient.setPlayertype(BMDataContext.PlayerTypeEnum.LEAVE.toString());//
                leaveRoom(playUserClient, playUserClient.getOrgi());
            }
            if (null == gameRoom || !MileGameEnum.PLAY.toString().equals(gameRoom.getStatus())) {
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_LEAVE, new LeaveEvent(userToken.getUserid()), gameRoom);
                logger.info("玩家 TOKEN[" + userToken.getId() + "] USERID[" + userToken.getUserid() + "] 离开房间");
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_LEAVE, userToken.getUserid(), new LeaveEvent(userToken.getUserid(), changeroom));

            } else {
                logger.info("玩家 TOKEN[" + userToken.getId() + "] USERID[" + userToken.getUserid() + "] 游戏状态为PLAY,不能离开房间");
            }

            putCoinroom(gameRoom);
        } catch (Exception e) {
            logger.error("玩家离开房间失败", e);
        }
    }

    /**
     * 当前用户所在的房间
     *
     * @param userid
     * @param orgi
     * @return
     */
    public GameRoom whichRoom(String userid, String orgi) {
        GameRoom gameRoom = null;
        String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userid, orgi);
        if (!StringUtils.isBlank(roomid)) {//
            gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);        //直接加入到 系统缓存 （只有一个地方对GameRoom进行二次写入，避免分布式锁）
        }
        return gameRoom;
    }

    /**
     * 创建新房间 ，需要传入房间的玩法 ， 玩法定义在 系统运营后台，玩法创建后，放入系统缓存 ， 客户端进入房间的时候，传入 玩法ID参数
     *
     * @param playway
     * @param userid
     * @return
     */
    private synchronized GameRoom creatGameRoom(GamePlayway playway, String gameid, boolean cardroom, BeiMiClient beiMiClient, PlayUserClient playUserClient) {
        try {
            GameRoom gameRoom = new GameRoom();
            gameRoom.setCreatetime(new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis())));
            gameRoom.setRoomid(UKTools.getUUID());
            gameRoom.setUpdatetime(new Date());

            if (playway != null) {
                gameRoom.setPlayway(playway.getId());
                //gameRoom.setRoomtype(playway.getRoomtype());此处无需设置
                gameRoom.setPlayers(playway.getPlayers());
            }
            gameRoom.setGamePlayway(playway);
            gameRoom.setPlaywaycode(playway.getCode());
            gameRoom.setPlayers(playway.getPlayers());
            gameRoom.setCardsnum(playway.getCardsnum());

            gameRoom.setCurpalyers(1);
            gameRoom.setCardroom(cardroom);

            gameRoom.setStatus(MileGameEnum.CRERATED.toString());

            gameRoom.setCardsnum(playway.getCardsnum());

            gameRoom.setCurrentnum(0);

            gameRoom.setTablenum(beiMiClient.getTablenum());
            gameRoom.setTeanum(beiMiClient.getTeanum());

            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(beiMiClient.getTeanum());
            if (null != teahouse) {
                gameRoom.setCreater(teahouse.getGameid());//如果是茶楼，需要设置成茶楼楼主
            } else {
                gameRoom.setCreater(gameid);//如果是茶楼，需要设置成茶楼楼主
            }

            gameRoom.setMaster(gameid);
            // gameRoom.setNumofgames(playway.getNumofgames());   //无限制
            gameRoom.setOrgi(playway.getOrgi());

            /**
             * 房卡模式启动游戏
             */
            if (beiMiClient.getExtparams() != null) {
                gameRoom.setCardroom(cardroom);
                gameRoom.setExtparams(beiMiClient.getExtparams());

                String otherstr = beiMiClient.getExtparams().get("other");
                if (!StringUtils.isEmpty(otherstr)) {
                    gameRoom.setTeamflag(otherstr.indexOf("teamflag") > -1 ? "Y" : "N");
                    gameRoom.setNewsflag(otherstr.indexOf("newsflag") > -1 ? "Y" : "N");
                    gameRoom.setGiveupflag(otherstr.indexOf("giveupflag") > -1 ? "Y" : "N");
                    gameRoom.setNoscore(otherstr.indexOf("noscore") > -1 ? "Y" : "N");
                }

                if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("coinexpend"))) {//耗卡类型
                    gameRoom.setCoinexpend(beiMiClient.getExtparams().get("coinexpend"));//耗卡类型
                } else if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("tips"))) {//局数
                    gameRoom.setCoinexpend(beiMiClient.getExtparams().get("tips"));
                } else {
                    gameRoom.setCoinexpend("aa");
                }

                if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("numofgames"))) {//局数
                    gameRoom.setNumofgames(Integer.parseInt(beiMiClient.getExtparams().get("numofgames")));
                } else if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("gamenumbers"))) {//局数
                    gameRoom.setNumofgames(Integer.parseInt(beiMiClient.getExtparams().get("gamenumbers")));
                } else {
                    gameRoom.setNumofgames(1);
                }

                //gameRoom.setRoomtype(beiMiClient.getExtparams().get("roomtype"));
                gameRoom.setRoomtype(beiMiClient.getRoomtype());//房间类型

                if (DataConstants.Roomtype.TEACOIN.toString().equals(gameRoom.getRoomtype())) {
                    gameRoom.setScoreunit(TeahouseUtil.getIntConstantsByName("scoreunit"));//底分
                } else if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("scoreunit"))) {
                    gameRoom.setScoreunit(Integer.parseInt(beiMiClient.getExtparams().get("scoreunit")));
                }

//            if (!StringUtils.isEmpty(beiMiClient.getExtparams().get("scoreunit"))) {
//                gameRoom.setScoreunit(Integer.parseInt(beiMiClient.getExtparams().get("scoreunit")));
//            }scoreunit

                /**
                 * 产生 房间 ID ， 麻烦的是需要处理冲突 ，准备采用的算法是 先生成一个号码池子，然后重分布是缓存的 Queue里获取
                 */
                String roomid = RandomCharUtil.getRandomNumberChar(6);

                //房间号判重
                for (int i = 0; i < 50; i++) {
                    if (null == CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, BMDataContext.SYSTEM_ORGI)) {
                        break;
                    } else {
                        logger.info("房间号重复 roomid[" + roomid + "]");
                        roomid = RandomCharUtil.getRandomNumberChar(6);
                    }

                }
                gameRoom.setRoomid(roomid);

                /**
                 * 分配房间号码 ， 并且，启用 规则引擎，对房间信息进行赋值
                 */
                GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();
                if (!StringUtils.isEmpty(beiMiClient.getTeanum()) && null != beiMiClient.getTablenum()) {
                    GameRoom gr = gameRoomCache.getGameroomByTea(beiMiClient.getTeanum(), beiMiClient.getTablenum(), beiMiClient.getRoomtype());
                    if (null != gr) {
                        logger.info("避免重复创建房间  beiMiClient.getTeanum() [" + beiMiClient.getTeanum() + "],beiMiClient.getTablenum()[" + beiMiClient.getTablenum() + "]");
                        return gr;
                    }
                }

                if ((DataConstants.Roomtype.CARD.toString().equals(gameRoom.getRoomtype()) ||
                        DataConstants.Roomtype.TEACOIN.toString().equals(gameRoom.getRoomtype()))
                        && playUserClient.getDiamonds() < 1
                        && StringUtils.isEmpty(gameRoom.getTeanum())) {
                    ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_CREATEROOM, playUserClient.getId(), new NoticeEvent("房卡不足,请充值！！！", false));
                    return null;
                }

                kieSession.insert(gameRoom);
                kieSession.fireAllRules();
            } else {

            }

            CacheHelper.getQueneCache().put(gameRoom, playway.getOrgi());    //未达到最大玩家数量，加入到游戏撮合 队列，继续撮合

            //吴真加  beiMiClient增加房间号
            //beiMiClient.setRoom(gameRoom.getRoomid());
            //NettyClients.getInstance().putClient(beiMiClient.getUserid(), beiMiClient);

            if (null != teahouse) {
                gameRoom.setCreater(teahouse.getGameid());//如果是茶楼，需要设置成茶楼楼主

                if ((DataConstants.Roomtype.CARD.toString().equals(gameRoom.getRoomtype()) ||
                        DataConstants.Roomtype.TEACOIN.toString().equals(gameRoom.getRoomtype()))
                        && teahouse.getFund() < 1) {
                    ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_CREATEROOM, playUserClient.getId(), new NoticeEvent("茶楼基金不足,请充值！！！", false));
                    return null;
                }
            }

            BMDataContext.getContext().getBean(GameRoomRepository.class).save(gameRoom);

            CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

            return gameRoom;
        } catch (Exception e) {
            logger.error("玩家创建房间失败", e);
            return null;
        }
    }

    /**
     * 发送房间事件
     *
     * @param userClient
     * @param gameEvent
     * @param beiMiClient
     */
    public void sendJoinroomEvent(PlayUserClient userClient, GameEvent gameEvent, BeiMiClient beiMiClient) {
        try {
            JoinRoom joinRoom = new JoinRoom(userClient, gameEvent.getIndex(), gameEvent.getGameRoom().getPlayers(), gameEvent.getGameRoom());

            joinRoom.player2client(userClient, gameEvent);

            joinRoom = RoomdescUtil.formatRoomdesc(gameEvent.getGameRoom(), joinRoom);
            joinRoom.setCreator(gameEvent.getGameRoom().getCreater());

            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_JOINROOM, joinRoom, gameEvent.getGameRoom());
            /**
             * 发送给单一玩家的消息
             */
            ActionTaskUtils.sendPlayers(beiMiClient, gameEvent.getGameRoom());
        } catch (Exception e) {
            logger.error("玩家发送加入房间事件失败", e);
        }
    }

    /**
     * 查找房间， 若未找到房间，就创建房间
     *
     * @param beiMiClient
     * @param roomid
     * @param playUserClient
     * @param gamePlayway
     * @return
     */
    public GameRoom searchRoom(BeiMiClient beiMiClient, String roomid, PlayUserClient playUserClient, GamePlayway gamePlayway) {
        try {
            GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();
            GameRoom gameRoom = (GameRoom) gameRoomCache.getCacheObject(roomid, playUserClient.getOrgi());

            logger.info("房间类型 roomtype[" + beiMiClient.getRoomtype() + "]");
            boolean cardroom = false;

            QueneCache queneCache = CacheHelper.getQueneCache();

            if ("1".equals(beiMiClient.getChangeroom()) && null != gameRoom) {

                leaveRoom(beiMiClient.getToken(), "1");

                logger.info("金币场换桌处理");
                gameRoom = queneCache.poll(gamePlayway.getId(), roomid, BMDataContext.SYSTEM_ORGI);

                if (gameRoom != null) {
                    while (isDel(gameRoom)) {
                        CacheHelper.getQueneCache().delete(gameRoom.getId());
                        gameRoom = (GameRoom) queneCache.poll(gamePlayway.getId(), roomid, BMDataContext.SYSTEM_ORGI);
                        if (gameRoom == null) {
                            break;
                        }
                    }
                }
                cardroom = false;

            } else {
                if (null == gameRoom) {
                    if (!DataConstants.Roomtype.COIN.toString().equals(beiMiClient.getRoomtype())) {
                        if (!StringUtils.isEmpty(beiMiClient.getTeanum()) && null != beiMiClient.getTablenum()) {
                            logger.info("playUser [" + playUserClient.getUsername() + "] [" + playUserClient.getGameid() + "]通过 roomid[" + roomid + "] 未到获取房间信息");
                            gameRoom = gameRoomCache.getGameroomByTea(beiMiClient.getTeanum(), beiMiClient.getTablenum(), beiMiClient.getRoomtype());
                        }
                        cardroom = true;
                    } else {
                        gameRoom = (GameRoom) queneCache.poll(gamePlayway.getId(), BMDataContext.SYSTEM_ORGI);

                        if (gameRoom != null) {
                            while (isDel(gameRoom)) {
                                CacheHelper.getQueneCache().delete(gameRoom.getId());
                                gameRoom = (GameRoom) queneCache.poll(gamePlayway.getId(), BMDataContext.SYSTEM_ORGI);
                                if (gameRoom == null) {
                                    break;
                                }
                            }
                        }
                        cardroom = false;

                    }
                }
            }

            if (null == gameRoom) {
                logger.info("playUser [" + playUserClient.getUsername() + "] [" + playUserClient.getGameid() + "]通过 teanum[" + beiMiClient.getTeanum() + "]  tableum[" + beiMiClient.getTablenum() + "]未到获取房间信息");
                if (beiMiClient.getExtparams() != null) {
                    gameRoom = this.creatGameRoom(gamePlayway, playUserClient.getGameid(), cardroom, beiMiClient, playUserClient);
                }
            }
            return gameRoom;
        } catch (Exception e) {
            logger.error("玩家查找房间失败", e);
            return null;
        }
    }

    private boolean isDel(GameRoom gameRoom) {
        if (CacheHelper.getGameRoomCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi()) == null) {
            logger.info("缓存中不存在房间[" + gameRoom.getRoomid() + "],删除");
            return true;
        }
//        if(CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(),gameRoom.getOrgi()) == null){
//            logger.info("未有面板对象 房间["+gameRoom.getRoomid()+"],删除");
//            return true;
//        }
//
//
//        List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
//        if(null != playerUserList && playerUserList.size() >= gameRoom.getPlayers()){
//            logger.info("人员已满 房间["+gameRoom.getRoomid()+"],删除");
//            return true;
//        }

        return false;

    }

    /**
     * 玩家的上线/下线
     *
     * @param userClient
     * @param beiMiClient
     */
    public void roomlinestatus(PlayUserClient userClient, BeiMiClient beiMiClient) {

        try {
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userClient.getId(), userClient.getOrgi());

            boolean online = DataConstants.Roomlinestatus.ONLINE.toString().equals(beiMiClient.getRoomlinestatus());

            synchronized (this) {
                LeaveEvent eventobj = new LeaveEvent(userClient.getId(), online);
                eventobj.setGamestatus(userClient.getGamestatus());

                if (null == roomid) {//不在房间中
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_LINESTATUS, userClient.getId(), eventobj);
                } else {
                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                    if (DataConstants.Roomlinestatus.OFFLINE.toString().equals(beiMiClient.getRoomlinestatus())
                            ) {
                        userClient.setOnline(false);
                        if (BMDataContext.GameStatusEnum.WAITTING.toString().equals(gameRoom.getStatus())
                                && DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode()) ) {
                            userClient.setRoomready(false);
                            userClient.setGamestatus(BMDataContext.GameStatusEnum.WAITTING.toString());
                        }

                    } else {
                        userClient.setOnline(true);
                    }
                    if (null != CacheHelper.getGamePlayerCacheBean().getPlayer(userClient.getId(), beiMiClient.getOrgi())) {
                        CacheHelper.getGamePlayerCacheBean().put(userClient.getId(), userClient, beiMiClient.getOrgi());
                    }

                    CacheHelper.getApiUserCacheBean().put(userClient.getId(), userClient, beiMiClient.getOrgi());

                    if (null != gameRoom) {
                        logger.info("玩家发送 上下线通知 [" + eventobj + "] ");
                        ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_ROOMLINESTATUS, eventobj, gameRoom);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("玩家上下线失败", e);
        }
    }

    /**
     * 解散处理
     *
     * @param beiMiClient
     * @param dissflag
     * @param firstflag
     */
    public void dissroom(BeiMiClient beiMiClient, int dissflag, boolean firstflag) {
        try {

            String token = beiMiClient.getToken();
            if (!org.apache.commons.lang.StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());

                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                    if (board.isFinished()) {
                        logger.info("房间roomid[" + roomid + "]结算后， 不能再次解散");
                        TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("结算后不能再次解散", false));
                        return;
                    }

                    logger.info("解散时的 roomid[" + roomid + "]");

                    dissroom(gameRoom, board, userToken.getUserid(), dissflag, firstflag);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统解散异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统解散异常", false));
        }
    }

    /**
     * 解散房间
     *
     * @param gameRoom
     * @param board
     * @param userid
     * @param dissflag
     */
    public void dissroom(GameRoom gameRoom, Board board, String userid, int dissflag, boolean firstflag) {
        try {

            logger.info("userid [" + userid + "] dissflag[" + dissflag + "]");

            List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

            boolean diss = true;

            List<DissroomInfo> dissroomInfoList = new LinkedList<DissroomInfo>();

            for (PlayUserClient playUserClient : playerUserList) {
                Player player = board.player(playUserClient.getId());
                if (userid.equals(playUserClient.getId()))
                    player.setDissflag(dissflag);

                if (1 != player.getDissflag()) {
                    logger.info("player.getUsername() [" + player.getUsername() + "]");
                    diss = false;
                }

                dissroomInfoList.add(new DissroomInfo(playUserClient.getId(), playUserClient.getUsername(),
                        playUserClient.getUserurl(), player.getDissflag()));

            }

            if (firstflag) {
                logger.info("第一个发起解散的人 userid [" + userid + "]");
                board.setDissuserid(userid);
                board.setDissbegindate(new Date(System.currentTimeMillis()));
                board.setDisssecond(getDissSecond("disssecond"));

            }

            if (dissflag != 2) {//未点击拒绝
                if (diss) {
                    dissflag = 1;
                } else {
                    dissflag = 0;
                }
            }

            DissroomEvent dissroomEvent = new DissroomEvent();
            dissroomEvent.setDissroomInfoList(dissroomInfoList);
            dissroomEvent.setDissflag(dissflag);
            dissroomEvent.setUserid(board.getDissuserid());
            dissroomEvent.setDissbegindate(board.getDissbegindate());
            dissroomEvent.setDisssecond(board.getDisssecond());
            dissroomEvent.setDealflag(0);

            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_DISSROOM, dissroomEvent, gameRoom);

            if (firstflag) {
                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.DISSROOM.toString(), dissroomEvent.getDisssecond());

            }

            if (diss) {
                logger.info("所有人都已经同意解散，解散开始");

                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());
                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.CLEARING.toString(), 0);    //通知状态机 , 继续执行
            } else {
                logger.info("还有玩家未同意解散");
            }

            if (dissflag == 2) {
                for (Player player : board.getPlayers()) {
                    logger.info("解散恢复玩家 player[" + player.getPlayuser() + "] [" + player.getUsername() + "]的状态");
                    player.setDissflag(0);
                }
                CacheHelper.getExpireCache().remove(gameRoom.getRoomid());

                board.setDisssecond(0);
                board.setDissuserid(null);
                board.setDissbegindate(null);

                board.setGiveupteamflag(0);
                board.setGiveupsecond(0);
                board.setGiveupuserid(null);
                board.setGiveupbegindate(null);
            }

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
        } catch (Exception e) {
            logger.error("玩家解散房间失败", e);
        }

    }

    public int getDissSecond(String name) {
        return Integer.parseInt(TeahouseUtil.getConstantsByName(name));
    }

    public void putCoinroom(GameRoom gameRoom) {
        if (null != gameRoom) {
            List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), BMDataContext.SYSTEM_ORGI);
            if ((null == players || players.size() < gameRoom.getPlayers()) && !gameRoom.isCardroom()) {
                CacheHelper.getQueneCache().put(gameRoom, BMDataContext.SYSTEM_ORGI);    //未达到最大玩家数量，加入到游戏撮合 队列，继续撮合
            }
        } else {
            logger.info("房间为空，无法加入到积分队列");
        }
    }

}
