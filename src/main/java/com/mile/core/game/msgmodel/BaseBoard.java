package com.mile.core.game.msgmodel;

import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.record.entity.Summary;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michael
 */

public abstract class BaseBoard extends Board {

    private static final Logger logger = LoggerFactory.getLogger(BaseBoard.class);


    /**
     * 翻底牌 ， 斗地主
     */
    @Override
    public byte[] pollLastHands() {
        return null;//码子 没有翻底牌操作。
    }

    /**
     * 暂时不做处理，根据业务规则修改，例如：底牌有大王翻两倍，底牌有小王 翻一倍，底牌是顺子 翻两倍 ====
     */
    @Override
    public int calcRatio() {
        return 1;
    }

    @Override
    public TakeCards takeCards(Player player, String playerType, TakeCards current) {
        logger.info("TakeCards takeCards(Player player, String playerType, TakeCards current)  错误调用");
        return new MaziTakeCards();
    }

    /**
     * 找到玩家的 位置
     *
     * @param board
     * @param userid
     * @return
     */
    public int index(String userid) {
        int index = 0;
        for (int i = 0; i < this.getPlayers().length; i++) {
            Player temp = this.getPlayers()[i];
            if (temp.getPlayuser().equals(userid)) {
                index = temp.getIndex();
                break;
            }
        }
        return index;
    }

    /**
     * 找到下一个可以抢庄玩家
     *
     * @param board
     * @param index
     * @return
     */
    public Player next(int index) {
        Player catchPlayer = null;
        if (index == (this.getPlayers().length - 1)) {    //fixed
            index = -1;
        }
        for (int i = index + 1; i < this.getPlayers().length; ) {
            Player player = this.getPlayers()[i];
            if (player.isDocatch() == false) {
                catchPlayer = player;
                break;
            } else if (player.isRandomcard()) {    //重新遍历一遍，发现找到了地主牌的人，终止查找
                break;
            } else if (i == (this.getPlayers().length - 1)) {
                i = 0;
                continue;
            }
            i++;
        }
        return catchPlayer;
    }

    //找到未出完牌的玩家
    public Player nextPlayerx(int index) {
        if (index >= this.getPlayers().length)
            return getPlayers()[index];//如果没有找到，返回自己， 表示已经完成了。
        int myIndex = 0;
        if (index == (this.getPlayers().length - 1)) {
            myIndex = 0;
        } else {
            myIndex = index + 1;
        }
        if (getPlayers()[index].isOverflag()
                || null == getPlayers()[index].getCards()
                || getPlayers()[index].getCards().length == 0) {
            return nextPlayerx(index + 1);
        } else {
            for (Player p : getPlayers()) {
                if (myIndex == p.getIndex()) {
                    return p;
                }
            }
            logger.info("未找到对应下一个玩家");
            return this.getPlayers()[0];
        }
    }

    //找到未出完牌的玩家
    public Player nextPlayer(int index) {



        Player retp = null;

        int onum = 0;

        for (Player p : getPlayers()) {
            if (p.isOverflag()) {
                onum++;
            } else {
                retp = p;
            }
        }

        if (onum >= this.getPlayers().length) {
            logger.info("查找玩家 只剩下最后一家");
            return retp;
        }

        ++index;

        if (index >= this.getPlayers().length) {
            index = 0;
        }

        for (Player p : getPlayers()) {
            if (p.getIndex() == index && !p.isOverflag()) {
                logger.info("玩家 ["+p.getUsername()+"] 未出完，找到");
                return p;
            } else if (p.getIndex() == index && p.isOverflag()) {
                logger.info("玩家 ["+p.getUsername()+"] 已经出完");
                return nextPlayer(index);
            }

        }

        logger.info("未找到下一个玩家信息");
        return null;

    }


    /**
     * 当前玩家随机出牌，能管住当前出牌的 最小牌
     *
     * @param player
     * @param current
     * @return
     */
    public TakeCards takecard(Player player) {
        return new MaziTakeCards();
    }



    @Override
    public boolean isWin() {
        return false;
    }

    @Override
    public void dealRequest(GameRoom gameRoom, Board board, String orgi, boolean reverse, String nextplayer) {
        /**
         * 斗地主无发牌动作
         */
    }

    @Override
    public void playcards(Board board, GameRoom gameRoom, Player player,
                          String orgi) {
    }

    @Override
    public Summary summary(Board board, GameRoom gameRoom, GamePlayway playway) {


        Summary summary = null;

        logger.info("码子单局结算信息  summary [" + summary + "]");

        return summary;
    }


    @Override
    public TakeCards takecard(Player player, TakeCards last) {
        logger.info("TakeCards takecard(Player player, TakeCards last) 错误调用");
        return null;
    }

}
