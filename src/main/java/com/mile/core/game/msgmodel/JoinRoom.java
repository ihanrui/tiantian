package com.mile.core.game.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.GameEvent;
import com.mile.core.game.Message;
import com.mile.util.UKTools;

import java.util.List;

public class JoinRoom implements Message{
	protected String command ;
	protected PlayUserClient player ;
	protected int index ;
	protected int maxplayers ;
	protected boolean cardroom ;
	protected String roomid ;

	protected String playwaycode;


	protected Integer curpalyers ; //当前人数

	protected String gamestatus;//游戏状态

	protected String playwaydesc;//玩法描述

	protected String nowgamenum;//当前局数

	protected String roomdesc;//房间 描述

	protected String creator;//房主




	public JoinRoom(){

	}

	public JoinRoom(PlayUserClient player , int index , int maxplayer , GameRoom gameRoom){
		this.player = player;
		this.index = index;
		this.maxplayers = maxplayer ;
		this.cardroom = gameRoom.isCardroom() ;
		this.roomid = gameRoom.getRoomid() ;
	}

	public void player2client( PlayUserClient userClient,GameEvent gameEvent){

		List<PlayUserClient> playUserClientList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameEvent.getRoomid(), gameEvent.getOrgi());
		if(null != playUserClientList && !playUserClientList.isEmpty()){
			for(PlayUserClient p:playUserClientList) {
				if(null != p && p.getId().equals(player.getId())){
					UKTools.copyProperties(p,this.player);

				}

				if(null != p && p.getId().equals(userClient.getId())){
					UKTools.copyProperties(p,userClient);

				}
			}

		}


	}

	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public PlayUserClient getPlayer() {
		return player;
	}
	public void setPlayer(PlayUserClient player) {
		this.player = player;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getMaxplayers() {
		return maxplayers;
	}
	public void setMaxplayers(int maxplayers) {
		this.maxplayers = maxplayers;
	}

	public boolean isCardroom() {
		return cardroom;
	}

	public void setCardroom(boolean cardroom) {
		this.cardroom = cardroom;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getPlaywaydesc() {
		return playwaydesc;
	}

	public void setPlaywaydesc(String playwaydesc) {
		this.playwaydesc = playwaydesc;
	}

	public String getNowgamenum() {
		return nowgamenum;
	}

	public void setNowgamenum(String nowgamenum) {
		this.nowgamenum = nowgamenum;
	}

	public String getRoomdesc() {
		return roomdesc;
	}

	public void setRoomdesc(String roomdesc) {
		this.roomdesc = roomdesc;
	}

	public String getGamestatus() {
		return gamestatus;
	}

	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public Integer getCurpalyers() {
		return curpalyers;
	}

	public void setCurpalyers(Integer curpalyers) {
		this.curpalyers = curpalyers;
	}

	public String getPlaywaycode() {
		return playwaycode;
	}

	public void setPlaywaycode(String playwaycode) {
		this.playwaycode = playwaycode;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}




}
