package com.mile.core.game.msgmodel;

import com.mile.core.game.Message;
import com.mile.core.entity.PlayUserClient;

import java.util.List;

public class GamePlayers implements Message{
	private int maxplayers ;
	private String command ;
	private String creator;
	private List<PlayUserClient> player ;

	public GamePlayers(int maxplayers , List<PlayUserClient> player ,String command){
		this.maxplayers = maxplayers ;
		this.player = player ;
		this.command = command ;
	}

	public GamePlayers(int maxplayers, String creator, List<PlayUserClient> player,String command) {
		this.maxplayers = maxplayers;
		this.command = command;
		this.creator = creator;
		this.player = player;
	}

	public int getMaxplayers() {
		return maxplayers;
	}
	public void setMaxplayers(int maxplayers) {
		this.maxplayers = maxplayers;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public List<PlayUserClient> getPlayer() {
		return player;
	}
	public void setPlayer(List<PlayUserClient> player) {
		this.player = player;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
}
