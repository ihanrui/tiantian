package com.mile.core.game.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

public class GpsInfo extends BaseEvent{

    private String gameid1;

    private String  gameid2;

    private double distance;//距离

	public GpsInfo(String gameid1, String gameid2, double distance) {
		this.gameid1 = gameid1;
		this.gameid2 = gameid2;
		this.distance = distance;
	}

	public String getGameid1() {
		return gameid1;
	}

	public void setGameid1(String gameid1) {
		this.gameid1 = gameid1;
	}

	public String getGameid2() {
		return gameid2;
	}

	public void setGameid2(String gameid2) {
		this.gameid2 = gameid2;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
}
