package com.mile.core.game.msgmodel;

import com.mile.common.eventmodel.BaseEvent;

public class TestEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String context;//错误内容

	private boolean flag;

	public TestEvent(boolean flag) {
		this.flag = flag;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
