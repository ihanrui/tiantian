package com.mile.core.game.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.teahouse.eventmodel.UserInfo;

import java.util.LinkedList;
import java.util.List;

public class GpsEvent extends BaseEvent{

	private List<GpsInfo> gpsInfoList;

	private List<UserInfo> userInfoList;

	public List<GpsInfo> getGpsInfoList() {
		return gpsInfoList;
	}

	public void setGpsInfoList(List<GpsInfo> gpsInfoList) {
		this.gpsInfoList = gpsInfoList;
	}

	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	public void setUserInfoList(List<UserInfo> userInfoList) {
		this.userInfoList = userInfoList;
	}

	public void addGpsInfo(GpsInfo gpsInfo){
		if(null == gpsInfoList)
			gpsInfoList = new LinkedList<GpsInfo>();
		gpsInfoList.add(gpsInfo);
	}

	public void addUserInfo(UserInfo userInfo){
		if(null == userInfoList)
			userInfoList = new LinkedList<UserInfo>();
		userInfoList.add(userInfo);
	}
}
