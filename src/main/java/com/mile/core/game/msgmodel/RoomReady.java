package com.mile.core.game.msgmodel;

import com.mile.core.game.Message;
import com.mile.core.entity.GameRoom;

public class RoomReady implements Message{
	private String command ;
	private boolean cardroom ;
	private String roomid ;
	private String userid;


	public RoomReady(GameRoom gameRoom){
		this.cardroom = gameRoom.isCardroom() ;
		this.roomid = gameRoom.getRoomid() ;
	}


	public RoomReady(GameRoom gameRoom,String userid){
		this.cardroom = gameRoom.isCardroom() ;
		this.roomid = gameRoom.getRoomid() ;
		this.userid = userid;

	}


	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public boolean isCardroom() {
		return cardroom;
	}

	public void setCardroom(boolean cardroom) {
		this.cardroom = cardroom;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
