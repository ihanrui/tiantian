package com.mile.core.game.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.CardType;
import com.mile.core.game.Message;

import javax.persistence.Transient;
import java.util.List;

/**
 * 当前出牌信息
 * 出牌人
 * 牌
 * @author zhangtianyi
 *
 */
public abstract class TakeCards extends BaseEntity implements Message{

	/**
	 *
	 */
	protected static final long serialVersionUID = 8718778983090104033L;

	protected String banker ;
	protected boolean allow ;		//符合出牌规则 ，
	protected boolean donot ;		//出 OR 不出
	protected String userid ;
	protected byte[] cards ;
	protected byte card ;			//麻将出牌

	protected boolean over ;		//已结束

	protected boolean bomb ;		//炸

	protected long time ;
	protected int type ;		//出牌类型 ： 1:单张 | 2:对子 | 3:三张 | 4:四张（炸） | 5:单张连 | 6:连对 | 7:飞机 : 8:4带2 | 9:王炸

	protected CardType cardType ;//出牌的牌型

	protected int mazicardtype;//码子牌型

	protected String command ;

	protected boolean sameside ;	//同一伙

	protected int cardsnum ;	//当前出牌的人 剩下多少张 牌

	protected String nextplayer ;		// 下一个出牌玩家
	protected boolean automic ;		//是否允许不出牌过


	protected List<List<Byte>> tipcards;//提示牌型集合

	protected String tipstr;//提示语句




	/**
	 * 移除出牌
	 * @param cards
	 * @param start
	 * @param end
	 * @return
	 */
	public byte[] removeCards(byte[] cards , int start , int end){
		byte[] retCards = new byte[cards.length - (end - start)] ;
		int inx = 0 ;
		for(int i=0; i<cards.length ; i++){
			if(i<start || i >= end){
				retCards[inx++] = cards[i] ;
			}
		}
		return retCards ;
	}

	/**
	 * 移除出牌
	 * @param cards
	 * @param start
	 * @param end
	 * @return
	 */
	public byte[] removeCards(byte[] cards , byte[] playcards){
		byte[] retCards = new byte[cards.length - playcards.length] ;
		int cardsindex = 0 ;
		for(int i=0; i<cards.length ; i++){
			boolean found = false ;
			for(int inx = 0 ;inx<playcards.length ; inx++){
				if(cards[i] == playcards[inx]){
					found = true ; break ;
				}
			}
			if(found == false){
				retCards[cardsindex++] = cards[i] ;
			}
		}
		return retCards ;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	@Transient
	public byte[] getCards() {
		return cards;
	}
	public void setCards(byte[] cards) {
		this.cards = cards;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getCardsnum() {
		return cardsnum;
	}
	public void setCardsnum(int cardsnum) {
		this.cardsnum = cardsnum;
	}
	public String getNextplayer() {
		return nextplayer;
	}
	public void setNextplayer(String nextplayer) {
		this.nextplayer = nextplayer;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public boolean isAllow() {
		return allow;
	}

	public void setAllow(boolean allow) {
		this.allow = allow;
	}

	public boolean isDonot() {
		return donot;
	}

	public void setDonot(boolean donot) {
		this.donot = donot;
	}

	public boolean isSameside() {
		return sameside;
	}

	public void setSameside(boolean sameside) {
		this.sameside = sameside;
	}

	public String getBanker() {
		return banker;
	}

	public void setBanker(String banker) {
		this.banker = banker;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public byte getCard() {
		return card;
	}

	public void setCard(byte card) {
		this.card = card;
	}

	public boolean isAutomic() {
		return automic;
	}

	public void setAutomic(boolean automic) {
		this.automic = automic;
	}

	public boolean isBomb() {
		return bomb;
	}

	public void setBomb(boolean bomb) {
		this.bomb = bomb;
	}

	public boolean isOver() {
		return over;
	}

	public void setOver(boolean over) {
		this.over = over;
	}

	public int getMazicardtype() {
		return mazicardtype;
	}

	public void setMazicardtype(int mazicardtype) {
		this.mazicardtype = mazicardtype;
	}

	@Transient
	public List<List<Byte>> getTipcards() {
		return tipcards;
	}

	public void setTipcards(List<List<Byte>> tipcards) {
		this.tipcards = tipcards;
	}

	public String getTipstr() {
		return tipstr;
	}

	public void setTipstr(String tipstr) {
		this.tipstr = tipstr;
	}


}
