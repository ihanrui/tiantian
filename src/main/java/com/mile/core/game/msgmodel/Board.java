package com.mile.core.game.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.record.entity.Summary;
import com.mile.util.UKTools;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public abstract class Board extends BaseEntity {

    private static final Logger logger = LoggerFactory.getLogger(Board.class);

    /**
     *
     */

    protected String id = UKTools.getUUID().toLowerCase();

    protected Date createdate = new Date(System.currentTimeMillis());

    protected byte[] cards;    //4个Bit描述一张牌，麻将：136+2/2 = 69 byte ; 扑克 54/2 = 27 byte

    protected Player[] players;//3~10人(4 byte)

    protected List<Byte> deskcards;

    protected TakeCards last; //上一家最后一个牌

    protected boolean finished;

    protected List<Byte> history = new ArrayList<Byte>();

    protected String winner;        //赢的玩家

    protected NextPlayer nextplayer;

    protected String command;

    protected String roomid;        //房间ID（4 byte）

    protected String gameroomid;    //房间UUID

    protected byte[] lasthands;    //底牌

    protected byte[] powerful;    //癞子 ， 麻将里的 单张癞子 ， 地主里的 天地癞子

    protected int position;        //地主牌

    protected boolean docatch;    //叫地主 OR 抢地主
    protected int ratio = 1;            //倍数
    protected boolean added;            //已翻倍

    protected String banker;        //庄家|地主
    protected String currplayer;    //当前出牌人
    protected byte currcard;        //当前出牌

    protected byte teamCard;//摸对的牌

    protected List<Byte> cardK105 = new LinkedList<Byte>();//已出的K105

    protected String teamOwner;//抓到摸对牌的人

    protected Integer score = 0;//本手牌的分数

    protected Integer roundscore = 0;//本局分数

    protected Integer nonums = 0;//本轮不出人的个数   若人数达到players.size -1 就可以给play加分了。

    protected Integer currentnum = 0;//盘数


    protected String dissuserid;//发起解散人

    protected Date dissbegindate;//解散开始时间


    protected Integer disssecond=0;//默认解散秒数


    protected String giveupuserid;//投降发起人

    protected Date giveupbegindate;//解散开始时间


    protected Integer giveupsecond=0;//默认解散秒数

    private int cardorder = 0;


    protected int giveupteamflag = 0;



    /**
     * @return
     */
    public abstract byte[] pollLastHands();

    /**
     * 计算倍率， 规则每种游戏不同，斗地主 翻到底牌 大小王 翻倍
     *
     * @return
     */
    public abstract int calcRatio();

    public abstract TakeCards takeCards(Player player, String playerType, TakeCards current);

    /**
     * 找到玩家的 位置
     *
     * @param board
     * @param userid
     * @return
     */
    public abstract int index(String userid);

    /**
     * 计算结算信息
     *
     * @return
     */
    public abstract Summary summary(Board board, GameRoom gameRoom, GamePlayway playway);

    /**
     * 是否赢了
     *
     * @return
     */
    public abstract boolean isWin();

    /**
     * 找到下一个玩家
     *
     * @param board
     * @param index
     * @return
     */
    protected abstract Player next(int index);

    public abstract Player nextPlayer(int index);

    /**
     * 玩家随机出牌，能管住当前出牌的 最小牌
     *
     * @param player
     * @param current
     * @return
     */
    public TakeCards takecard(Player player, boolean allow, byte[] playCards) {
        logger.info("牌型格式化  进入空方法 Player player, boolean allow, byte[] playCards");
        return null;
    }

    public TakeCards takecard(Player player, byte[] playCards) {
        logger.info("牌型格式化  进入空方法 Player player, byte[] playCards");
        return null;
    }

    /**
     * 当前玩家随机出牌
     *
     * @param player
     * @param current
     * @return
     */
    public abstract TakeCards takecard(Player player);

    /**
     * 出牌请求
     *
     * @param roomid
     * @param playUserClient
     * @param orgi
     * @param auto
     * @param playCards
     * @return
     */
    public abstract TakeCards takeCardsRequest(GameRoom gameRoom, Board board, Player player, String orgi, boolean auto, byte[] playCards);

    /**
     * 发牌动作
     *
     * @param gameRoom
     * @param board
     * @param orgi
     */
    public abstract void dealRequest(GameRoom gameRoom, Board board, String orgi, boolean reverse, String nextplayer);

    /**
     * 下一个出牌玩家
     *
     * @param board
     * @param gameRoom
     * @param player
     * @param orgi
     */
    public abstract void playcards(Board board, GameRoom gameRoom, Player player, String orgi);

    /**
     * 玩家出牌
     *
     * @param player
     * @param cards
     * @return
     */
    public abstract TakeCards takecard(Player player, TakeCards last);

    public Summary stepSummary(Board board, GameRoom gameRoom, GamePlayway playway) {
        logger.info("调用了 空 stepSummary ");
        return null;
    }

    public TakeCards cardtip(Player player, TakeCards last) {
        return null;
    }

    /**
     * 顺序提示玩家出牌
     *
     * @param player
     * @param tipcards
     * @return
     */
    public TakeCards getCardTips(Player player, byte[] tipcards) {
        return null;
    }

    /**
     * 找到玩家数据
     *
     * @param userid
     * @return
     */
    public Player player(String userid) {
        Player temp = null;
        if (this.players != null) {
            for (Player user : players) {
                if (user.getPlayuser() != null && user.getPlayuser().equals(userid)) {
                    temp = user;
                    break;
                }
            }
        }
        return temp;
    }

    /**
     * 找到玩家数据
     *
     * @param userid
     * @return
     */
    public PlayUserClient getPlayerClient(List<PlayUserClient> players, String userid) {
        PlayUserClient temp = null;
        for (PlayUserClient user : players) {
            if (user.getId().equals(userid)) {
                temp = user;
                break;
            }
        }
        return temp;
    }

    public static Logger getLogger() {
        return logger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public List<Byte> getDeskcards() {
        return deskcards;
    }

    public void setDeskcards(List<Byte> deskcards) {
        this.deskcards = deskcards;
    }

    public TakeCards getLast() {
        return last;
    }

    public void setLast(TakeCards last) {
        this.last = last;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public List<Byte> getHistory() {
        return history;
    }

    public void setHistory(List<Byte> history) {
        this.history = history;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public NextPlayer getNextplayer() {
        return nextplayer;
    }

    public void setNextplayer(NextPlayer nextplayer) {
        this.nextplayer = nextplayer;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public byte[] getLasthands() {
        return lasthands;
    }

    public void setLasthands(byte[] lasthands) {
        this.lasthands = lasthands;
    }

    public byte[] getPowerful() {
        return powerful;
    }

    public void setPowerful(byte[] powerful) {
        this.powerful = powerful;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isDocatch() {
        return docatch;
    }

    public void setDocatch(boolean docatch) {
        this.docatch = docatch;
    }

    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public String getCurrplayer() {
        return currplayer;
    }

    public void setCurrplayer(String currplayer) {
        this.currplayer = currplayer;
    }

    public byte getCurrcard() {
        return currcard;
    }

    public void setCurrcard(byte currcard) {
        this.currcard = currcard;
    }

    public byte getTeamCard() {
        return teamCard;
    }

    public void setTeamCard(byte teamCard) {
        this.teamCard = teamCard;
    }

    public List<Byte> getCardK105() {
        return cardK105;
    }

    public void setCardK105(List<Byte> cardK105) {
        this.cardK105 = cardK105;
    }

    public String getTeamOwner() {
        return teamOwner;
    }

    public void setTeamOwner(String teamOwner) {
        this.teamOwner = teamOwner;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getRoundscore() {
        return roundscore;
    }

    public void setRoundscore(Integer roundscore) {
        this.roundscore = roundscore;
    }

    public Integer getNonums() {
        return nonums;
    }

    public void setNonums(Integer nonums) {
        this.nonums = nonums;
    }

    public Integer getCurrentnum() {
        return currentnum;
    }

    public void setCurrentnum(Integer currentnum) {
        this.currentnum = currentnum;
    }

    public String getDissuserid() {
        return dissuserid;
    }

    public void setDissuserid(String dissuserid) {
        this.dissuserid = dissuserid;
    }

    public Date getDissbegindate() {
        return dissbegindate;
    }

    public void setDissbegindate(Date dissbegindate) {
        this.dissbegindate = dissbegindate;
    }

    public Integer getDisssecond() {
        return disssecond;
    }

    public void setDisssecond(Integer disssecond) {
        this.disssecond = disssecond;
    }

    public int getCardorder() {
        return cardorder;
    }

    public void setCardorder(int cardorder) {
        this.cardorder = cardorder;
    }

    public String getGiveupuserid() {
        return giveupuserid;
    }

    public void setGiveupuserid(String giveupuserid) {
        this.giveupuserid = giveupuserid;
    }

    public Date getGiveupbegindate() {
        return giveupbegindate;
    }

    public void setGiveupbegindate(Date giveupbegindate) {
        this.giveupbegindate = giveupbegindate;
    }

    public Integer getGiveupsecond() {
        return giveupsecond;
    }

    public void setGiveupsecond(Integer giveupsecond) {
        this.giveupsecond = giveupsecond;
    }

    public int getGiveupteamflag() {
        return giveupteamflag;
    }

    public void setGiveupteamflag(int giveupteamflag) {
        this.giveupteamflag = giveupteamflag;
    }
}


