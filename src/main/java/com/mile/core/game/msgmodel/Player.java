package com.mile.core.game.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.DataConstants;
import com.mile.util.UKTools;
import com.mile.util.rules.model.Action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;




public class Player extends BaseEvent implements Serializable,  Cloneable {

    /**
     *
     */
    protected static final long serialVersionUID = 1L;



    public Player() {
    }

    /**
     *
     */
    public Player(String id) {
        this.playuser = id;

    }

    public Player(String id, int index) {
        this.playuser = id;
        this.index = index;

    }

    protected String id = UKTools.getUUID();
    protected String playuser;    //userid对应


    protected String gender;//性别  0.未知，1.男，2.女

    protected byte[] cards;    //玩家手牌，顺序存储 ， 快速排序（4个Bit描述一张牌，玩家手牌 麻将 13+1/2 = 7 byte~=long）
    protected byte[] history = new byte[]{};//出牌历史 ， 特权可看
    protected byte info;        //复合信息存储，用于存储玩家位置（2^4,占用4个Bit，最大支持16个玩家）（是否在线1个Bit），是否庄家/地主（1个Bit），是否当前出牌玩家（1个Bit）（是否机器人1个Bit）
    protected boolean randomcard;    //起到地主牌的人
    protected boolean docatch;    //抢过庄（地主）
    protected boolean recatch;    //补抢
    protected Integer cardsnum;    //剩下多少张牌

    protected boolean hu;    //已经胡过牌了
    protected boolean end;    //血战的时候，标记 结束
    protected String command;

    protected boolean selected;    //已经选择 花色
    protected int color;        //定缺 花色   0  : wan , 1:tong , 2 :tiao

    protected boolean accept;    //抢地主 : 过地主
    protected boolean banker;    //庄家
    protected byte[] played;    //杠碰吃胡

    protected boolean collocation = false; //是否托管

    //码子专用字段
    protected boolean overflag;//完成标志

    protected int lvl;//1.一游，2.二游 3.三游 4.四游

    protected int score = 0;//本局抓分

    protected int index;//位置

    protected int teamflag = 0;//1.表示 庄和对家  2.表示 庄的上手，和下手  摸对后的结果

    protected boolean newsflag = false;//报喜标志

    protected List<List<Byte>> newsCards;//报喜的牌

    protected List<Action> actions = new ArrayList<Action>();

    protected int stepscore = 0;//本局输赢的分数

    protected Integer totalscore=0;//总共输赢的分数

    protected int newsscore=0;//本次报喜所得分数

    protected boolean winflag;//是否赢了

    protected boolean seeflag;//明牌标志

    protected String ip;//IP

    protected Integer dissflag = 0;//解散标志 0.默认 无操作。 1.同意解散  2.拒绝解散

    protected String gamestatus;//玩家状态 与client中一致

    protected boolean online;

    protected boolean lasthands = false;//最后一手牌是否已完成

    protected Integer giveupflag=0;//投降标志 0.默认 无操作。 1.同意投降  2.拒绝投降







    protected String gameroomid;//房间ID

    protected String roomid;//房间号

    protected String teanum;//茶楼号

    protected Integer tablenum;//桌号

    protected String userid;

    protected String username;

    protected String gameid; //游戏ID

    protected String userurl = DataConstants.GAME_USER_DEFAULT_URL;


    protected boolean settleflag =true;//理牌标志



    // 纬度
    public  Double latitude = 0.0;
    // 经度
    public  Double longitude = 0.0;

    public  String locationdesc = "";


    public TakeCards takecards;







    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public byte getInfo() {
        return info;
    }

    public void setInfo(byte info) {
        this.info = info;
    }

    public byte[] getPlayed() {
        return played;
    }

    public void setPlayed(byte[] played) {
        this.played = played;
    }

    public byte[] getHistory() {
        return history;
    }

    public void setHistory(byte[] history) {
        this.history = history;
    }

    public String getPlayuser() {
        return playuser;
    }

    public void setPlayuser(String playuser) {
        this.playuser = playuser;
    }

    public boolean isRandomcard() {
        return randomcard;
    }

    public void setRandomcard(boolean randomcard) {
        this.randomcard = randomcard;
    }

    public boolean isDocatch() {
        return docatch;
    }

    public void setDocatch(boolean docatch) {
        this.docatch = docatch;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public boolean isCollocation() {
        return collocation;
    }

    public void setCollocation(boolean collocation) {
        this.collocation = collocation;
    }

    @Override
    public Player clone() {
        try {
            Player p=(Player) super.clone();
            byte[] cards = new byte[5];
            System.arraycopy(this.getCards(),0,cards,0,5);
            p.setCards(cards);
            return p;


        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isRecatch() {
        return recatch;
    }

    public void setRecatch(boolean recatch) {
        this.recatch = recatch;
    }

    public boolean isBanker() {
        return banker;
    }

    public void setBanker(boolean banker) {
        this.banker = banker;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public boolean isHu() {
        return hu;
    }

    public void setHu(boolean hu) {
        this.hu = hu;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public boolean isOverflag() {
        return overflag;
    }

    public void setOverflag(boolean overflag) {
        this.overflag = overflag;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTeamflag() {
        return teamflag;
    }

    public void setTeamflag(int teamflag) {
        this.teamflag = teamflag;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUserurl() {
//        if (StringUtils.isEmpty(this.getUserurl()))
//            return DataConstants.GAME_USER_DEFAULT_URL;
//        else
            return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isNewsflag() {
        return newsflag;
    }

    public void setNewsflag(boolean newsflag) {
        this.newsflag = newsflag;
    }



    public Integer getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(Integer totalscore) {
        this.totalscore = totalscore;
    }

    public int getNewsscore() {
        return newsscore;
    }

    public void setNewsscore(int newsscore) {
        this.newsscore = newsscore;
    }

    public int getStepscore() {
        return stepscore;
    }

    public void setStepscore(int stepscore) {
        this.stepscore = stepscore;
    }

    public boolean isWinflag() {
        return winflag;
    }

    public void setWinflag(boolean winflag) {
        this.winflag = winflag;
    }

    public boolean isSeeflag() {
        return seeflag;
    }

    public void setSeeflag(boolean seeflag) {
        this.seeflag = seeflag;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<List<Byte>> getNewsCards() {
        return newsCards;
    }

    public void setNewsCards(List<List<Byte>> newsCards) {
        this.newsCards = newsCards;
    }

    public Integer getDissflag() {
        return dissflag;
    }

    public void setDissflag(Integer dissflag) {
        this.dissflag = dissflag;
    }

    public Integer getGiveupflag() {
        return giveupflag;
    }

    public void setGiveupflag(Integer giveupflag) {
        this.giveupflag = giveupflag;
    }

    public String getGamestatus() {
        return gamestatus;
    }

    public void setGamestatus(String gamestatus) {
        this.gamestatus = gamestatus;
    }

    public boolean isLasthands() {
        return lasthands;
    }

    public void setLasthands(boolean lasthands) {
        this.lasthands = lasthands;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public Integer getTablenum() {
        return tablenum;
    }

    public void setTablenum(Integer tablenum) {
        this.tablenum = tablenum;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public boolean isSettleflag() {
        return settleflag;
    }

    public void setSettleflag(boolean settleflag) {
        this.settleflag = settleflag;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLocationdesc() {
        return locationdesc;
    }

    public void setLocationdesc(String locationdesc) {
        this.locationdesc = locationdesc;
    }

    public TakeCards getTakecards() {
        return takecards;
    }

    public void setTakecards(TakeCards takecards) {
        this.takecards = takecards;
    }

    public Integer getCardsnum() {
        return cardsnum;
    }

    public void setCardsnum(Integer cardsnum) {
        this.cardsnum = cardsnum;
    }
}
