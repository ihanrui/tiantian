package com.mile.core.game.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.GameBoard;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.Message;
import com.mile.core.engine.game.impl.Banker;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiUserBoard;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.jpa.TeahouseRepository;

import java.util.ArrayList;
import java.util.List;

public class RecoveryData implements Message{
	private String command ;
	private String userid ;
	private Player player ;
	private byte[] lasthands;
	private TakeCards last ;
	private Banker banker ;
	private String nextplayer ;//正在出牌的玩家
	private CardsInfo[] cardsnum ;
	private int time ;		//计时器剩余时间
	private boolean automic ;	//本轮第一个出牌，不允许出现不出按钮
	private GameBoard data ;
	private int ratio ;
	private byte[] hiscards ;

	private String status ;

	private SelectColor selectcolor ;

	private UserBoard userboard ;


	private Teahouse teahouse;//茶楼信息

	private Integer tablenum;//桌号

	private String roomtype;




	public RecoveryData(Player player , byte[] lasthands , String nextplayer , int time , boolean automic , Board board,String status){
		this.player = player ;


		this.userid = player.getPlayuser() ;
		this.lasthands = lasthands ;
		this.nextplayer = nextplayer ;
		this.banker = new Banker(board.getBanker()) ;
		this.time = time ;
		this.automic = automic;
		this.data = new GameBoard(board.getBanker(), board.getRatio()) ;

		this.hiscards = player.getHistory() ;

		this.ratio = board.getRatio() ;



		if(board instanceof PinshiBoard){
			PinshiUserBoard pinshiUserBoard = new PinshiUserBoard(board, player.getPlayuser(), "play") ;
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(board.getGameroomid(), BMDataContext.SYSTEM_ORGI);
			pinshiUserBoard.setStartflag(gameRoom.isStartflag());
			this.userboard = pinshiUserBoard;

		}else {
			this.userboard = new UserBoard(board, player.getPlayuser(), "play");
		}

		this.selectcolor =  new SelectColor(board.getBanker(), player.getPlayuser()) ;
		this.selectcolor.setColor(player.getColor());


		this.last = board.getLast() ;
		this.cardsnum = new CardsInfo[board.getPlayers().length - 1];
		List<CardsInfo> tempList = new ArrayList<CardsInfo>();
		for(Player temp : board.getPlayers()){
			if(!temp.getPlayuser().equals(player.getPlayuser())){
				tempList.add(new CardsInfo(temp.getPlayuser() , temp.getCards().length , temp.getHistory() , temp.getActions() , board , temp)) ;
			}

		}
		cardsnum = tempList.toArray(this.cardsnum) ;

		this.status = status;
	}


	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public byte[] getLasthands() {
		return lasthands;
	}
	public void setLasthands(byte[] lasthands) {
		this.lasthands = lasthands;
	}
	public String getNextplayer() {
		return nextplayer;
	}

	public void setNextplayer(String nextplayer) {
		this.nextplayer = nextplayer;
	}

	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public boolean isAutomic() {
		return automic;
	}
	public void setAutomic(boolean automic) {
		this.automic = automic;
	}


	public CardsInfo[] getCardsnum() {
		return cardsnum;
	}


	public void setCardsnum(CardsInfo[] cardsnum) {
		this.cardsnum = cardsnum;
	}
	public Banker getBanker() {
		return banker;
	}


	public void setBanker(Banker banker) {
		this.banker = banker;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public TakeCards getLast() {
		return last;
	}


	public void setLast(TakeCards last) {
		this.last = last;
	}


	public GameBoard getData() {
		return data;
	}


	public void setData(GameBoard data) {
		this.data = data;
	}


	public int getRatio() {
		return ratio;
	}


	public void setRatio(int ratio) {
		this.ratio = ratio;
	}


	public UserBoard getUserboard() {
		return userboard;
	}


	public void setUserboard(UserBoard userboard) {
		this.userboard = userboard;
	}


	public SelectColor getSelectcolor() {
		return selectcolor;
	}


	public void setSelectcolor(SelectColor selectcolor) {
		this.selectcolor = selectcolor;
	}


	public byte[] getHiscards() {
		return hiscards;
	}


	public void setHiscards(byte[] hiscards) {
		this.hiscards = hiscards;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Teahouse getTeahouse() {
		return teahouse;
	}

	public void setTeahouse(Teahouse teahouse) {
		this.teahouse = teahouse;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}


	public void initTeahouseinfo(GameRoom gameRoom){
		if(null == gameRoom){
			return ;
		}
		this.teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());
		this.tablenum = gameRoom.getTablenum();
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
}
