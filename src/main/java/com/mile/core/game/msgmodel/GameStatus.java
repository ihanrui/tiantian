package com.mile.core.game.msgmodel;

import com.mile.core.BMDataContext;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.Message;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.jpa.TeahouseRepository;

import java.util.Map;

public class GameStatus implements Message{
	private String command ;
	private String gamestatus ;

	private String userid ;
	private String orgi ;
	private String gametype ;
	private String playway ;
	private boolean cardroom ;

	private Teahouse teahouse;

	private Integer tablenum;

	private String roomtype;



	private Map<String,String> extparams;

	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getGamestatus() {
		return gamestatus;
	}
	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getGametype() {
		return gametype;
	}

	public void setGametype(String gametype) {
		this.gametype = gametype;
	}

	public String getPlayway() {
		return playway;
	}

	public void setPlayway(String playway) {
		this.playway = playway;
	}
	public boolean isCardroom() {
		return cardroom;
	}
	public void setCardroom(boolean cardroom) {
		this.cardroom = cardroom;
	}

	public Map<String, String> getExtparams() {
		return extparams;
	}

	public void setExtparams(Map<String, String> extparams) {
		this.extparams = extparams;
	}

	public Teahouse getTeahouse() {
		return teahouse;
	}

	public void setTeahouse(Teahouse teahouse) {
		this.teahouse = teahouse;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}


	public void initTeahouseinfo(GameRoom gameRoom){
		if(null == gameRoom){
			return ;
		}
		this.teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());
		this.tablenum = gameRoom.getTablenum();
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
}
