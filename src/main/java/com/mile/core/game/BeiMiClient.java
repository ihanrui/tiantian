package com.mile.core.game;

import com.corundumstudio.socketio.SocketIOClient;

import java.util.Map;

/**
 * 当前Session的客户端信息
 */
public class BeiMiClient{
	private String token ;
	private String playway ;
	private String orgi ;
	private String room ;

	private String gameid;

	private long time ;

	private String userid ;

	private String session ;

	private String teanum;//茶楼号

	private Integer tablenum;//桌号
	/**
	 CARD,    //房卡
	 TEACOIN,   //茶楼金币
	 COIN;//金币
	 */
	private String roomtype; //房间类型

	private String roomlinestatus;//玩家在房间里面的状态

	private SocketIOClient client;

	private String changeroom;//1为换桌

	private String hogflag;//抢庄标志

	private int hogtimes = 0 ;//抢庄倍数

	private int bettimes =0 ;//下注倍数

	private String pushflag;//推注标注

	private Map<String,  String> extparams ;

	public BeiMiClient(){

	}

	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}


	public String getPlayway() {
		return playway;
	}

	public void setPlayway(String playway) {
		this.playway = playway;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public SocketIOClient getClient() {
		return client;
	}

	public void setClient(SocketIOClient client) {
		this.client = client;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Map<String, String> getExtparams() {
		return extparams;
	}

	public void setExtparams(Map<String, String> extparams) {
		this.extparams = extparams;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public String getTeanum() {
		return teanum;
	}

	public void setTeanum(String teanum) {
		this.teanum = teanum;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}

	public String getRoomlinestatus() {
		return roomlinestatus;
	}

	public void setRoomlinestatus(String roomlinestatus) {
		this.roomlinestatus = roomlinestatus;
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}

	public String getChangeroom() {
		return changeroom;
	}

	public void setChangeroom(String changeroom) {
		this.changeroom = changeroom;
	}

	public int getHogtimes() {
		return hogtimes;
	}

	public void setHogtimes(int hogtimes) {
		this.hogtimes = hogtimes;
	}

	public int getBettimes() {
		return bettimes;
	}

	public void setBettimes(int bettimes) {
		this.bettimes = bettimes;
	}

	public String getPushflag() {
		return pushflag;
	}

	public void setPushflag(String pushflag) {
		this.pushflag = pushflag;
	}

	public String getHogflag() {
		return hogflag;
	}

	public void setHogflag(String hogflag) {
		this.hogflag = hogflag;
	}
}
