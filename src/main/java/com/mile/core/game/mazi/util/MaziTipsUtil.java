package com.mile.core.game.mazi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.CardType;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.game.msgmodel.JoinRoom;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.entity.GameRoom;
import com.mile.core.teahouse.eventmodel.TableInfo;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.*;

/**
 * Created by michael on 3/18/18.
 */
public class MaziTipsUtil {

    private static final Logger logger = LoggerFactory.getLogger(MaziTipsUtil.class);

    /**
     * 搜索符合条件的当前最小 牌型 , 机器人出牌 ， 只处理到 三带一，其他以后在扩展
     *
     * @param player
     * @param last
     * @return
     */
    public static List<List<Byte>> searchTips(Player player, TakeCards lastTakeCards) {
        List<List<Byte>> retValue = null;

        Map retMap = new LinkedHashMap();

        MaziCardtypeUtil.settleCards(player.getCards(), retMap);

        if (lastTakeCards != null && lastTakeCards.getCardType() != null) {

//
//            if(lastTakeCards.getMazicardtype()<10){
//                logger.info("非板子提示处理");
//                //dealCardNoBorns(player);
//            }else if(lastTakeCards.getMazicardtype()>10){
//                logger.info("板子提示处理");
//                //dealCardWithBorns()
//            }
//

            switch (lastTakeCards.getMazicardtype()) {
                case 1: //单张
                case 2: //对子
                case 3: //三张
                case 4: //四张
                    retValue = MaziTipsUtil.getCardNoBorns(lastTakeCards.getCards(), retMap, lastTakeCards.getCardType());
                    break;
                case 5:
                case 6:
                case 7:
                    retValue = MaziTipsUtil.getCardLinks(retMap, lastTakeCards.getCardType());
                    break;
                default: //炸弹处理。
                    retValue = getCardWithBorns(retMap, lastTakeCards.getCardType());
                    break;

            }
        }
        return retValue;
    }

    public static List<List<Byte>> searchTips(Player player) {
        Map retMap = new HashMap();
        MaziCardtypeUtil.settleCards(player.getCards(), retMap);

        List<List<Byte>> llb = new LinkedList<List<Byte>>();

        int mazicardtype = MaziCardtypeUtil.getMaziCardtype(player.getCards());

        if (BMDataContext.MaziCardsTypeEnum.NONE.getType() != mazicardtype) {
            List<Byte> lb = new LinkedList<Byte>();
            for (byte b : player.getCards()) {
                lb.add(b);
            }
            llb.add(lb);
            return llb;

        }

        for (int i = 0; i < 13; i++) {
            List<Byte> lb = (List<Byte>) retMap.get(i);
            if (null != lb && !lb.isEmpty())
                llb.add(lb);
        }

        return llb;
    }

    /**
     * 找到num对子
     *
     * @param num
     * @return
     */
    public byte[] getPair(byte[] cards, Map<Integer, Integer> types, int num, int mincard) {
        byte[] retCards = null;
        List<Integer> retValue = new ArrayList<Integer>();
        for (int i = 0; i < 14; i++) {
            if (types.get(i) != null && types.get(i) == 2 && retValue.size() < num && (i < 0 || i > mincard)) {
                retValue.add(i);
            }
            if (retValue.size() >= num) {
                break;
            }
        }
        if (retValue.size() == num) {
            retCards = new byte[num * 2];
            int inx = 0;
            for (int temp : retValue) {
                int times = 0;
                for (byte card : cards) {
                    if (card / 4 == temp) {
                        retCards[inx++] = card;
                        times++;
                    }
                    if (times == 2) {
                        break;
                    }
                }
            }
        }
        return retCards;
    }

    public static List<List<Byte>> getCardNoBorns(byte[] cards, Map types, CardType cardType) {
        logger.info("单个牌 获取 提示牌");
        logger.info("当前用户的所有牌  cards [" + cards + "]");
        logger.info("types " + types);

        List<List<Byte>> retListCards = new LinkedList<List<Byte>>();

        for (int j = cardType.getMazicardtype(); j < 5; j++) {
            for (int i = 0; i < 14; i++) {
                //if (i == 2 || i == 7 || i == 10)//分数不提示
                //    continue;
                if (types.get(i) != null && ((List) types.get(i)).size() == j && (byte) ((List) types.get(i)).get(0) > cards[0]) {
                    List<Byte> ret = new LinkedList<Byte>();
                    for (int index = 0; index < cardType.getMazicardtype(); index++) {
                        ret.add((byte) ((List) types.get(i)).get(index));
                    }

                    if (ActionTaskUtils.maziAllow(ActionTaskUtils.identificationMazi(ret), cardType)) {
                        retListCards.add(ret);
                    } else {
                        logger.info("获取错误，再去反思");
                    }
                }

            }
        }

        retListCards.addAll((List) types.get("born"));

        return retListCards;
    }

    public static List<List<Byte>> getCardLinks(Map types, CardType cardType) {

        List<List<Byte>> retListCards = new LinkedList<List<Byte>>();

        for (int i = cardType.getMincard() + 1; i < 12; i++) {

            LinkedList<Byte> ll = new LinkedList<Byte>();

            for (int j = i; j < i + cardType.getTypesize(); j++) {
                List<Byte> jtmpData = (List<Byte>) types.get(j);
                if (null != types.get(j) && ((List) types.get(j)).size() >= cardType.getCardnum()) {
                    for (int index = 0; index < cardType.getCardnum(); index++) {
                        ll.addFirst(jtmpData.get(index));
                    }
                } else {
                    ll.clear();
                    break;
                }
            }
            if (null != ll && !ll.isEmpty()) {
                if (ActionTaskUtils.maziAllow(ActionTaskUtils.identificationMazi(ll), cardType))
                    retListCards.add(ll);
                else
                    logger.info("获取错误，再去反思");
            }

        }

        retListCards.addAll((List) types.get("born"));

        return retListCards;
    }

    public static List<List<Byte>> getCardWithBorns(Map types, CardType cardType) {

        List<List<Byte>> retListCards = new LinkedList<List<Byte>>();

        for (List<Byte> ll : ((List<List<Byte>>) types.get("born"))) {
            if (null != ll && !ll.isEmpty()) {
                byte[] b = new byte[ll.size()];
                for (int a = 0; a < ll.size(); a++) {
                    b[a] = ll.get(a);
                }
                CardType tc = ActionTaskUtils.identification(b);
                if (ActionTaskUtils.maziAllow(tc, cardType)) {
                    retListCards.add(ll);
                }

            }
        }

        return retListCards;
    }

    /**
     * 找到num对子
     *
     * @param num
     * @return
     */
    public byte[] getThree(byte[] cards, Map<Integer, Integer> types, int mincard, int num) {
        byte[] retCards = null;
        List<Integer> retValue = new ArrayList<Integer>();
        for (int i = 0; i < 14; i++) {
            if (types.get(i) != null && types.get(i) == 3 && retValue.size() < num && (i < 0 || i > mincard)) {
                retValue.add(i);
            }
            if (retValue.size() >= num) {
                break;
            }
        }
        if (retValue.size() == num) {
            retCards = new byte[num * 3];
            int inx = 0;
            for (int temp : retValue) {
                int times = 0;
                for (byte card : cards) {
                    if (card / 4 == temp) {
                        retCards[inx++] = card;
                        times++;
                    }
                    if (times == 3) {
                        break;
                    }
                }
            }
        }
        return retCards;
    }

    /**
     * @param testStr 自己的牌
     * @param param   最后一手牌
     */
    public static void test(String testStr, byte[] param) {

//        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
//                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
//                + ",90,91,92,93,94,95,96,97";

        byte[] ret = MaziCardtypeUtil.settleCards(MaziCardtypeUtil.str2byte(testStr), new LinkedHashMap());

        for (byte b : ret) {

            String s = "";
            if (b < 104) {
                int y = b % 4;
                if (y == 0)
                    s = "黑桃";
                if (y == 1)
                    s = "红桃";
                if (y == 2)
                    s = "梅花";
                if (y == 3)
                    s = "方片";

                if (b / 8 == 8)
                    s += "J";
                else if (b / 8 == 9)
                    s += "Q";
                else if (b / 8 == 10)
                    s += "K";
                else if (b / 8 == 11)
                    s += "A";
                else if (b / 8 == 12)
                    s += "2";
                else
                    s += (b / 8 + 3);
            }

            if (b > 105)
                s = "大鬼";
            else if (b > 103)
                s = "小鬼";

            System.out.print(s + ",");
        }

        Map retMap = new LinkedHashMap();

        Player player = new Player("id");
        player.setCards(MaziCardtypeUtil.str2byte(testStr));

        TakeCards lastTakeCards = new MaziTakeCards();

        //lastTakeCards.setCards(new byte[]{(byte)9,8,7,6});
        // byte[] param = new byte[]{(byte)41,40,38,37}
        lastTakeCards.setCards(param);

        CardType c = ActionTaskUtils.identificationMazi(lastTakeCards.getCards());
        lastTakeCards.setCardType(c);
        lastTakeCards.setMazicardtype(c.getMazicardtype());

        List<List<Byte>> retList = MaziTipsUtil.searchTips(player, lastTakeCards);

        if (retList != null)
            for (List<Byte> retxxx : retList) {
                for (byte b : retxxx) {
                    String s = "";
                    if (b < 104) {
                        int y = b % 4;
                        if (y == 0)
                            s = "黑桃";
                        if (y == 1)
                            s = "红桃";
                        if (y == 2)
                            s = "梅花";
                        if (y == 3)
                            s = "方片";
                        if (b / 8 == 8)
                            s += "J";
                        else if (b / 8 == 9)
                            s += "Q";
                        else if (b / 8 == 10)
                            s += "K";
                        else if (b / 8 == 11)
                            s += "A";
                        else if (b / 8 == 12)
                            s += "2";
                        else
                            s += (b / 8 + 3);
                    }
                    if (b > 105)
                        s = "大鬼";
                    else if (b > 103)
                        s = "小鬼";
                    System.out.print(s + ",");
                }
                System.out.println();
            }

    }

    public static String getPlaywaydesc(Map<String, String> map) {
        if (null == map)
            return null;
        String playwaydesc = "";
        String otherstr = map.get("other");
        if (!StringUtils.isEmpty(otherstr) && otherstr.indexOf("newsflag")>-1 ) {
            playwaydesc += "可报喜";
        } else {
            playwaydesc += "无报喜";
        }

        if (!StringUtils.isEmpty(otherstr) && otherstr.indexOf("teamflag")>-1) {
            playwaydesc += ",摸队";
        } else {
            playwaydesc += ",铁对";
        }

        if (!StringUtils.isEmpty(otherstr) && otherstr.indexOf("giveupflag")>-1) {
            playwaydesc += ",可投降";
        } else {
            playwaydesc += ",不可投降";
        }

        if (!StringUtils.isEmpty(otherstr) && otherstr.indexOf("noscore")>-1) {
            playwaydesc += ",打无分";
        } else {
            playwaydesc += "";
        }



        return playwaydesc;

    }

    public static String getNowgamenum(GameRoom gameRoom) {
        if (null == gameRoom)
            return null;

        String retstr = "";

        if(null == gameRoom.getNumofgames()){
            gameRoom.setNumofgames(1);
        }

        if (gameRoom.getNumofgames() >= 999999) {
            retstr = gameRoom.getCurrentnum() + "一局制";
        } else {
            retstr = gameRoom.getCurrentnum() + "/" + gameRoom.getNumofgames() + "局";
        }
        if (DataConstants.Roomtype.CARD.toString().equals(gameRoom.getRoomtype())) {
            retstr += ",经典玩法";
        } else if (DataConstants.Roomtype.TEACOIN.toString().equals(gameRoom.getRoomtype())) {
            retstr += ",积分玩法";
        } else if (DataConstants.Roomtype.COIN.toString().equals(gameRoom.getRoomtype())) {
            retstr += ",金币玩法";
        }

        return retstr;
    }

    public static String getRoomdesc(GameRoom gameRoom) {
        if (null == gameRoom)
            return null;
        String roomdesc = "";
        if (StringUtils.isEmpty(gameRoom.getTeanum())) {
            roomdesc += "房间: " + gameRoom.getRoomid()+"";
            if(DataConstants.Roomtype.COIN.toString().equals(gameRoom.getRoomtype())){
                roomdesc+=",底分["+gameRoom.getScoreunit()+"]";
            }
        } else {
            roomdesc = "茶楼:" + gameRoom.getTeanum() + ",第" + gameRoom.getTablenum() + "桌,房间:" + gameRoom.getRoomid();
        }

        return roomdesc;

    }

    public static JoinRoom formatRoomdesc(GameRoom gameRoom, JoinRoom joinRoom) {
        if (null == gameRoom || null == joinRoom)
            return joinRoom;
        joinRoom.setNowgamenum(getNowgamenum(gameRoom));
        joinRoom.setPlaywaydesc(getPlaywaydesc(gameRoom.getExtparams()));
        joinRoom.setRoomdesc(getRoomdesc(gameRoom));
        joinRoom.setGamestatus(gameRoom.getStatus());
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if(null !=  playerList && !playerList.isEmpty()) {
            joinRoom.setCurpalyers(playerList.size());
        }else{
            joinRoom.setCurpalyers(0);
        }

        joinRoom.setPlaywaycode(gameRoom.getPlaywaycode());
        return joinRoom;


    }

    public static void formatRoomdesc(GameRoom gameRoom, UserBoard userBoard) {
        if (null == gameRoom || null == userBoard)
            return;
        userBoard.setNowgamenum(getNowgamenum(gameRoom));
        userBoard.setPlaywaydesc(getPlaywaydesc(gameRoom.getExtparams()));
        userBoard.setRoomdesc(getRoomdesc(gameRoom));
        userBoard.setGamestatus(gameRoom.getStatus());


        gameRoom.setNowgamenum(getNowgamenum(gameRoom));
        gameRoom.setPlaywaydesc(getPlaywaydesc(gameRoom.getExtparams()));
        gameRoom.setRoomdesc(getRoomdesc(gameRoom));



    }



    public static void sendTablechange(GameRoom gameRoom) {


        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if (!org.apache.commons.lang3.StringUtils.isEmpty(gameRoom.getTeanum())) {
            TableInfo tableInfo = new TableInfo();
            UKTools.copyProperties(gameRoom, tableInfo);

            tableInfo.setUserInfoList(TeahouseUtil.formatPlayer2Userinfo(playerList));
            tableInfo.setNum(playerList.size());

            TeahouseActionTaskUtils.sendTeahouseEvent(DataConstants.TEAHOUSE_EVENT_TABLECHANGE, gameRoom.getTeanum(), tableInfo);
        } else {
            logger.info("未在茶楼，不用发送事件");
        }
    }



    public static Player getPlayerforindex(Player[] players, int index) {
       for(int i=0;i<players.length;i++){
           if(index == players[i].getIndex()){
               return players[i];
           }
       }

       return null;


    }


}