package com.mile.core.game.mazi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiClient;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.CardType;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.mazi.msgmodel.SeecardsEvent;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.NextPlayer;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.util.PinshiTipUtil;
import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.util.GameUtils;
import com.mile.util.UKTools;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

public class MaziActionTaskUtils {
    private static final Logger logger = LoggerFactory.getLogger(MaziActionTaskUtils.class);

    public static void formatTakkeCard(MaziTakeCards takeCards, Board board, Player player, GameRoom gameRoom) {

        if (ArrayUtils.isEmpty(player.getCards())) {
            logger.info("玩家出牌已完成,定义他的几游");
            player.setLvl(MaziScoreUtil.getLvlnum(board.getPlayers()) + 1);
            player.setOverflag(true);
            player.setLasthands(true);
            board.player(player.getUserid()).setOverflag(true);
            board.player(player.getUserid()).setLasthands(true);
            takeCards.setLvl(player.getLvl());
        } else if (player.getCards().length <= 5) {//通知出牌  小于等于5张 报警
            logger.info("报警处理");
            takeCards.setCardsnum(player.getCards().length);
        } else {
            takeCards.setCardsnum(-1);
        }

        takeCards.setAllow(true);

        if (takeCards.getCards() != null) {
            Arrays.sort(takeCards.getCards());
            logger.info("设置本手牌分数和本轮的分数");
            takeCards.setScore(MaziScoreUtil.getScore(takeCards.getCards()));

            board.setScore(takeCards.getScore());

            board.setRoundscore(takeCards.getScore() + board.getRoundscore());

            takeCards.setRoundscore(board.getRoundscore());
        }

        if (takeCards.getCards() != null) {
            board.setLast(takeCards);
            takeCards.setDonot(false);    //出牌
        } else {
            takeCards.setDonot(true);    //不出牌
        }

        //找到下一个玩家
        logger.info("本次玩家  玩家[" + player.getUsername() + "] ID player.getPlayuser()[" + player.getPlayuser() + "][" + player.getIndex() + "] ");
        Player next = board.nextPlayer(player.getIndex());
        if (next != null) {
            logger.info("下一个玩家ID 玩家[" + next.getUsername() + "] next.getPlayuser()[" + next.getPlayuser() + "][" + next.getIndex() + "]");
            takeCards.setNextplayer(next.getPlayuser());
            board.setNextplayer(new NextPlayer(next.getPlayuser(), false));

            if (board.getLast() != null && board.getLast().getUserid().equals(next.getPlayuser())) {
                logger.info("如果下一个出牌的玩家是自己， 那么就允许过");
                takeCards.setAutomic(true);//如果最后一手牌为空，或者手一手牌为自己 不能不出
            }

        }

        if (MaziScoreUtil.countOver(board, player)) {
            logger.info("用户[" + player.getUsername() + "] GAMEID[" + player.getGameid() + "] 已出完最后一手");
            player.setLasthands(true);
        }

        logger.info("房间号  [" + gameRoom.getRoomid() + "] 进入分数结算");
        MaziScoreUtil.countScore(board, next, takeCards, player);

        if (MaziScoreUtil.countOver(board, player) && MaziScoreUtil.isLasthands(board.getPlayers())) {//出完了
            logger.info("房间号  [" + gameRoom.getRoomid() + "] 设置结束标志");
            board.setWinner(MaziScoreUtil.getPlayerwithLvl(board.getPlayers(), 1).getPlayuser());
            takeCards.setOver(true);

            if (MaziScoreUtil.getExistPlayer(board.getPlayers()) > 0) {
                int num = MaziScoreUtil.getLvlnum(board.getPlayers());
                for (Player p : board.getPlayers()) {
                    logger.info("房间号  [" + gameRoom.getRoomid() + "] 设置前玩家[" + p.getUsername() + "] 游数 [" + p.getLvl() + "]");
                    if (p.getLvl() < 1) {
                        p.setLvl(++num);
                        p.setOverflag(true);
                        logger.info("房间号  [" + gameRoom.getRoomid() + "] 设置后 玩家[" + p.getUsername() + "] 游数 [" + p.getLvl() + "]");
                    }
                }
            }

        }
        /**
         * 放到 Board的列表里去，如果是不洗牌玩法，则直接将出牌结果 重新发牌
         */
        if (takeCards.getCards() != null && takeCards.getCards().length > 0) {
            for (byte temp : takeCards.getCards()) {
                board.getHistory().add(temp);
            }
        }

        if (MaziScoreUtil.countOver(board, player) && MaziScoreUtil.isLasthands(board.getPlayers())) {//出完了

            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
            GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.STEPCLEARING.toString(), 1.5);    //赢了，通知结算
            takeCards.setNextplayer(null);
        }
    }

    public static SummaryPlayer formatStepSum(SummaryPlayer summaryPlayer, Player player, GameRoom gameRoom) {
        if (null == summaryPlayer) {
            summaryPlayer = new SummaryPlayer();
            summaryPlayer.setUserid(player.getPlayuser());
            summaryPlayer.setUsername(player.getUsername());
            summaryPlayer.setUserurl(player.getUserurl());
            summaryPlayer.setGameid(player.getGameid());
            summaryPlayer.setRoomid(gameRoom.getRoomid());
            summaryPlayer.setGameroomid(gameRoom.getId());
            summaryPlayer.setTablenum(gameRoom.getTablenum());
            summaryPlayer.setTeanum(gameRoom.getTeanum());
            summaryPlayer.setBegindate(gameRoom.getCreatetime());
            summaryPlayer.setEnddate(new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        }

        summaryPlayer.setBankersum(summaryPlayer.getBankersum() + (player.isBanker() ? 1 : 0));
        summaryPlayer.setWinsum(summaryPlayer.getWinsum() + (player.isWinflag() ? 1 : 0));
        summaryPlayer.setTotalscore(player.getTotalscore());
        summaryPlayer.setScoreunit(gameRoom.getScoreunit());
        if (null != gameRoom.getScoreunit())
            summaryPlayer.setCoins(summaryPlayer.getTotalscore() * gameRoom.getScoreunit());

        summaryPlayer.setLvlsum(summaryPlayer.getLvlsum() + (player.getLvl() == 1 ? 1 : 0));
        summaryPlayer.setNewssum(summaryPlayer.getNewssum() + (player.getNewsscore() > 0 ? 1 : 0));
        summaryPlayer.setMaxscore(summaryPlayer.getMaxscore() < player.getScore() ? player.getScore() : summaryPlayer.getMaxscore());





        return summaryPlayer;

    }

    public static SummaryPlayer formatNULLStep(GameRoom gameRoom, Summary summary, SummaryPlayer summaryPlayer, PlayUserClient playUserClient, Player player) {
        if (null == summaryPlayer) {
            summaryPlayer = new SummaryPlayer();
            summaryPlayer = new SummaryPlayer();
            summaryPlayer.setUserid(player.getPlayuser());
            summaryPlayer.setUsername(player.getUsername());
            summaryPlayer.setUserurl(player.getUserurl());
            summaryPlayer.setGameid(player.getGameid());
            summaryPlayer.setRoomid(gameRoom.getRoomid());
            summaryPlayer.setGameroomid(gameRoom.getId());
            summaryPlayer.setTablenum(gameRoom.getTablenum());
            summaryPlayer.setTeanum(gameRoom.getTeanum());
            summaryPlayer.setBegindate(gameRoom.getCreatetime());
            summaryPlayer.setEnddate(new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        }

        summaryPlayer.setBankersum(0);
        summaryPlayer.setWinsum(0);
        summaryPlayer.setTotalscore(0);
        summaryPlayer.setLvlsum(0);
        summaryPlayer.setNewssum(0);
        summaryPlayer.setMaxscore(0);
        return summaryPlayer;

    }

    /**
     * 找到玩家
     *
     * @param board
     * @param userid
     * @return
     */
    public static Player player(Board board, String userid) {
        Player target = null;
        for (Player temp : board.getPlayers()) {
            if (temp.getPlayuser().equals(userid)) {
                target = temp;
                break;
            }
        }
        return target;
    }

    /**
     * 处理牌局开始时的发牌，摸对等处理。
     *
     * @param gameRoom
     * @param banker
     * @param players
     * @param cards
     * @param board
     */
    public static void dealBeginCards(GameRoom gameRoom, String banker, Player[] players, List<Byte> cards, Board board, List<PlayUserClient> playUsers) {
        int winindex = gameRoom.getExtparams().get(banker + "_index") == null ? 0 : (Integer.parseInt(gameRoom.getExtparams().get(banker + "_index")));
        logger.info("庄家的座位号  winindex [" + winindex + "]");
        byte teamCard = -1, otherTeamCard = -1;
        boolean alwaysTeam = false; //铁对的人。

        Map innermap = MaziInnertestUtil.getInnercards(gameRoom.getExtparams());

        gameRoom.getExtparams().remove(DataConstants.GAME_EVENT_INNERTEST);

        List<Byte> innerlist = (List<Byte>) innermap.get("cards");
        if (null != innerlist) {
            for (Byte b : innerlist)
                cards.remove(b);
        }

        //码子发牌
        for (int i = 0, j = 0, k = 0; i < gameRoom.getCardsnum() * players.length; i++) {
            int pos = (i + winindex) % players.length;

            Player ptemp = MaziTipsUtil.getPlayerforindex(players,pos);//players[pos];

            byte cardtemp;
            if (null != innerlist && !innerlist.isEmpty() && ptemp.getPlayuser().equals(innermap.get("userid")) && k < innerlist.size()) {
                cardtemp = innerlist.get(k++);
            } else {
                cardtemp = cards.get(j++);
            }

            ptemp.getCards()[i / players.length] = cardtemp;

            if ("Y".equals(gameRoom.getTeamflag())) {
                if (i == 0) {
                    logger.info("摸对的牌为 cards[0] ----------" + cardtemp + "  --------");
                    players[pos].setTeamflag(1);//第一队
                    players[pos].setBanker(true);
                    teamCard = cardtemp;
                    board.setTeamCard(teamCard);
                    if (teamCard < 104) {
                        otherTeamCard = (teamCard % 8 > 3) ? (byte) (teamCard - 4) : (byte) (teamCard + 4);
                    } else {
                        otherTeamCard = (teamCard % 2 == 1) ? (byte) (teamCard - 1) : (byte) (teamCard + 1);
                    }
                    board.setNextplayer(new NextPlayer(players[pos].getPlayuser(), false));
                    logger.info("摸对的牌为 otherTeamCard ----------" + otherTeamCard + "  -----players[pos]" + players[pos].getUsername() + "-" +
                            "-----players[pos].getTeamflag-----------------------" + players[pos].getTeamflag() + "------------");
                } else if (otherTeamCard == cardtemp) {
                    players[pos].setTeamflag(1);
                    logger.info("第一队玩家产生  cards[i] [" + cardtemp + "] players[" + players[pos].getUsername() + "]"
                            + "---players[pos].getTeamflag-------" + players[pos].getTeamflag() + "----位置[" + players[pos].getIndex() + "]--庄家位置[" + winindex + "]--");
                    board.setTeamOwner(players[pos].getPlayuser());

                    alwaysTeam = winindex % 2 == players[pos].getIndex() % 2;//铁对
                }
            } else {

                logger.info("1铁对无需处理");
                if (i == 0) {
                    alwaysTeam = true;
                    players[pos].setTeamflag(1);//第一队
                    players[pos].setBanker(true);
                    board.setTeamCard(teamCard);
                    board.setNextplayer(new NextPlayer(players[pos].getPlayuser(), false));
                    players[pos].setIndex(0);
                } else if (i < 4) {
                    players[pos].setTeamflag(i % 2 + 1);
                    players[pos].setIndex(i % 4);
                    if (i == 2) {
                        board.setTeamOwner(players[pos].getPlayuser());
                    }

                }
            }

            if (i == board.getPosition()) {
                players[pos].setRandomcard(true);        //起到地主牌的人  码子， 第一次当庄的人
            }

        }

        //理牌 加 处理第二队情况
        for (Player tempPlayer : players) {
            if ("Y".equals(gameRoom.getTeamflag())) {
                logger.info("成员 信息 tempPlayer.getUsername()[" + tempPlayer.getUsername()
                        + "] alwaysTeam[" + alwaysTeam + "] tempPlayer.getTeamflag() ["
                        + tempPlayer.getTeamflag() + "] tempPlayer.getIndex()["+tempPlayer.getIndex()+"] " +
                        "  board.getTeamOwner()["+ board.getTeamOwner()+"]");


                if(alwaysTeam){
                    logger.info("自己抓了两个庄的牌    处理");
                    if(tempPlayer.getIndex() % 2 == winindex % 2) {
                        tempPlayer.setTeamflag(1);
                    }else{
                        tempPlayer.setTeamflag(2);
                    }
                }else{
                    logger.info("自己只抓一个庄的牌    处理");
                    if(tempPlayer.getIndex() == winindex ||
                            (null != board.getTeamOwner() && board.getTeamOwner().equals(tempPlayer.getPlayuser()))) {
                        tempPlayer.setTeamflag(1);
                    }else{
                        tempPlayer.setTeamflag(2);
                    }
                }

                logger.info("成员 信息 tempPlayer.getUsername()[" + tempPlayer.getUsername()
                        + " tempPlayer.getTeamflag() ["
                        + tempPlayer.getTeamflag() + "] ");



            } else {
                logger.info("2铁对无需处理");
            }

            Map cardsmap = new LinkedHashMap();
            tempPlayer.setCards(MaziCardtypeUtil.settleCards(tempPlayer.getCards(), cardsmap));

            logger.info("处理报喜开始 gameRoom.getRoomid() -------------------" + gameRoom.getRoomid());

            if ("Y".equals(gameRoom.getNewsflag())) {
                List<List<Byte>> bornList = (List<List<Byte>>) cardsmap.get("born");
                if (null != bornList && !bornList.isEmpty()) {
                    List<List<Byte>> listCardtype = new LinkedList<List<Byte>>();
                    for (List<Byte> list : bornList) {
                        CardType c = ActionTaskUtils.identificationMazi(list);
                        if (c.getMazicardtype() > 20)
                            listCardtype.add(list);
                    }
                    tempPlayer.setNewsCards(listCardtype);
                    if (!listCardtype.isEmpty()) {
                        logger.info("本局有喜 " + tempPlayer);
                        tempPlayer.setNewsflag(true);
                    } else {
                        tempPlayer.setNewsflag(false);
                    }

                }
            } else {
                logger.info("1不报喜,无需处理");
            }
        }

        board.setGameroomid(gameRoom.getId());
        board.setRoomid(gameRoom.getRoomid());

        logger.info("摸对位置调换  是否庄家自己抓了摸对的牌 alwaysTeam【" + alwaysTeam + "】 庄家banker[" + banker + "]");

        if (!alwaysTeam) {
            if ("Y".equals(gameRoom.getTeamflag())) {

                for (int i = 0; i < players.length; i++) {
                    Player player = players[i];
                    logger.info("房间[" + gameRoom.getRoomid() + "]玩家[" + player.getUsername() + "] 座位号[" + player.getIndex() + "] 编队[" + player.getTeamflag() + "]");
                    if (!player.getPlayuser().equals(banker) && 1 == player.getTeamflag()) {
                        int oIndex = player.getIndex();
                        logger.info("庄家现在一对的另外一方的座位号 index[" + oIndex + "]");

                        for (int j = 0; j < players.length; j++) {
                            Player playerj = players[j];
                            if ((winindex + 2) % 4 == playerj.getIndex()) {
                                playerj.setIndex(oIndex);
                                logger.info("调换位置房间[" + gameRoom.getRoomid() + "]玩家[" + player.getUsername() + "] 座位号[" + player.getIndex() + "] 编队[" + player.getTeamflag() + "]");
                            }
                        }
                        player.setIndex((winindex + 2) % 4);
                        break;
                    }
                }
            } else {
                logger.info("3铁对无需处理");
            }

        } else {
            logger.info("房间[" + gameRoom.getRoomid() + "]庄家自己抓了摸对的牌，或者对方抓了摸对的牌， 都无需处理");
        }

        for (Player p : players) {
            gameRoom.putExtparams(p.getPlayuser() + "_index", "" + p.getIndex());

            for (PlayUserClient c : playUsers) {
                if (c.getId().equals(p.getPlayuser())) {
                    c.setIndex(p.getIndex());


                    CacheHelper.getGamePlayerCacheBean().put(c.getId(), c, c.getOrgi());
                    CacheHelper.getApiUserCacheBean().put(c.getId(), c, c.getOrgi());
                }
            }

            logger.info("发牌位置 玩家 [" + p.getUsername() + "][" + p.getGameid() + "] 位置[" + p.getIndex() + "]");

            MaziSummaryUtil.dealHisplayer(p, gameRoom);


        }
    }


    public static void sendTakeCardEvent(MaziTakeCards maziTakeCards, Board board, GameRoom gameRoom, Player player) {
        maziTakeCards.setCommand(DataConstants.GAME_EVENT_TAKECARDS);

        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if (null != players && !players.isEmpty()) {
            for (PlayUserClient user : players) {
                Player p = board.player(user.getId());

                logger.info("玩家[" + p.getUsername() + "] [" + p.getGameid() + "]" +
                        " 是否明牌[" + p.isSeeflag() + "] 是否已出牌完成[" + p.isOverflag() + "]" +
                        " 编队[" + p.getTeamflag() + "] 本次出牌张数[" + (null == maziTakeCards.getCards() ? 0 : maziTakeCards.getCards().length) + "]" +
                        " 牌型[" + maziTakeCards.getMazicardtype() + "]");



                if (player.isSeeflag()) { //自己明牌
                    maziTakeCards.setLeftuserid(player.getPlayuser());
                    maziTakeCards.setLeftcards(MaziCardtypeUtil.settleCards(player.getCards(), null));
                } else if (p.isOverflag() || p.isSeeflag()) {//玩家出完或者明牌，需要看到对家的牌
                    logger.info("用户 [" + user.getUsername() + "] 明牌或者已经出牌完成，展现牌给对家");

                    for (Player u : board.getPlayers()) {
                        if (p.getTeamflag() == u.getTeamflag() && !p.getGameid().equals(u.getGameid())) {
                            maziTakeCards.setLeftuserid(u.getPlayuser());
                            maziTakeCards.setLeftcards(MaziCardtypeUtil.settleCards(u.getCards(), null));
                        }
                    }

                } else{
                    maziTakeCards.setLeftcards(null);
                    maziTakeCards.setLeftuserid(null);
                }


                byte[] bytecards = null;
                if (p.isSettleflag()) {
                    bytecards = MaziCardtypeUtil.settleCards(player.getCards(), null);
                } else {
                    Arrays.sort(player.getCards());
                    bytecards = GameUtils.reverseCards(player.getCards());
                }

                if (player.getPlayuser().equals(p.getPlayuser())) {
                    logger.info("用户 [" + user.getUsername() + "]  自己要看自己的牌， 不管是明牌与明牌");
                    maziTakeCards.setMycards(bytecards);
                    p.setCards(bytecards);
                } else {
                    maziTakeCards.setMycards(null);
                }

                BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
                if (client != null && ActionTaskUtils.online(user.getId(), user.getOrgi())) {
                    logger.info("发送打牌事件  user[" + user.getUsername() + "] 是否明牌[" + p.isSeeflag() + "] 是否出完牌[" + p.isOverflag() + "]    maziTakeCards[" + maziTakeCards + "]");
                    MaziTakeCards sendobj = new MaziTakeCards();
                    UKTools.copyProperties(maziTakeCards, sendobj);
                    client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, sendobj);
                }
            }
            MaziSummaryUtil.dealHiscard(board, maziTakeCards, player);
        }

    }

    private static void setSeecardsInfo(Board board, Player player, MaziTakeCards maziTakeCards) {

        Player team = getTeamuser(board, player);
        if (null != team) {
            maziTakeCards.setLeftuserid(team.getPlayuser());
            maziTakeCards.setLeftcards(team.getCards());
        }

    }

    public static void setSeecardsInfo(Board board, Player player, SeecardsEvent seecardsEvent) {

        Player team = getTeamuser(board, player);
        if (null != team) {
            seecardsEvent.setLeftuserid(team.getPlayuser());
            seecardsEvent.setLeftcards(MaziCardtypeUtil.settleCards(team.getCards(), null));
        }

    }

    private static Player getTeamuser(Board board, Player player) {
        for (Player p : board.getPlayers()) {
            if (p.getTeamflag() == player.getTeamflag() && !p.getPlayuser().equals(player.getPlayuser())) {
                return p;
            }
        }
        logger.info("玩家 player[" + player.getUsername() + "] 查找队友失败");
        return null;

    }

    public static void sendSeecardEvent(Player player, Board board, SeecardsEvent seecardsEvent, GameRoom gameRoom) {

        seecardsEvent.setCommand(DataConstants.GAME_EVENT_SEECARDS);

        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if (null != players && !players.isEmpty()) {
            for (PlayUserClient user : players) {
                Player p = board.player(user.getId());

                if ((p.isSeeflag() || p.isOverflag())) {
                    logger.info("用户 [" + user.getUsername() + "] 明牌或者已经出牌完成，展现牌给对家");
                    setSeecardsInfo(board, p, seecardsEvent);
                } else {
                    seecardsEvent.setLeftcards(null);
                    seecardsEvent.setLeftuserid(null);
                }

                BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
                if (client != null && ActionTaskUtils.online(user.getId(), user.getOrgi())) {
                    logger.info("发送明牌事件  user[" + user.getUsername() + "]  是否明牌[" + p.isSeeflag() + "] 是否出完牌[" + p.isOverflag() + "]  seecardsEvent[" + seecardsEvent + "]");

                    SeecardsEvent sendobj = new SeecardsEvent();
                    UKTools.copyProperties(seecardsEvent, sendobj);
                    client.getClient().sendEvent(BMDataContext.BEIMI_MESSAGE_EVENT, sendobj);
                }
            }
        }
    }

}


