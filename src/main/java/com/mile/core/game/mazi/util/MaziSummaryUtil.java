package com.mile.core.game.mazi.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.record.entity.Hiscard;
import com.mile.core.record.entity.Hisplayer;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.jpa.HiscardRepository;
import com.mile.core.record.jpa.HisplayerRepository;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import com.mile.web.model.BeiMiDic;
import com.mile.web.model.SysDic;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by michael on 3/21/18.
 */
public class MaziSummaryUtil {

    private static final Logger logger = LoggerFactory.getLogger(MaziSummaryUtil.class);

    public static String getObjWithName(List<SysDic> sysDicList, String name) {
        if (null == sysDicList || sysDicList.isEmpty()) {
            logger.info("err:数据字典未配置，获取的内容为空");
            return "err:数据字典未配置，获取的内容为空";
        }

        if (StringUtils.isEmpty(name)) {
            logger.info("err:name为空");
            return "err:name为空";
        }

        for (SysDic sysDic : sysDicList) {
            if (name.equals(sysDic.getName()))
                return sysDic.getCode();
        }

        logger.info("err:未找到对应的name [" + name + "]");
        return "err:未找到对应的name [" + name + "]";

    }

    public static Date getRecordDate() {
        String recorddaynum = TeahouseUtil.getConstantsByName("recorddaynum");
        logger.info("后台设置战绩显示天数为 recorddaynum[" + recorddaynum + "]");
        int num = Integer.parseInt(recorddaynum);
        Calendar cal = Calendar.getInstance();//使用默认时区和语言环境获得一个日历。
        cal.add(Calendar.DAY_OF_MONTH, -1 * num);//取当前日期的前一天.  å
        return cal.getTime();
    }

    public static int  ducuteCoins(GameRoom gameRoom) {

        try {
            logger.info("gameroom [" + gameRoom.getRoomid() + "] 扣卡处理开始 ");

            if (!"Y".equals(gameRoom.getDeductflag())) {
                logger.info("gameroom [" + gameRoom.getRoomid() + "] 首局未完成，无需扣卡");
                return 0;
            }

            int mumofgames = gameRoom.getNumofgames();//盘数
            GamePlayway gamePlayway = gameRoom.getGamePlayway();

            String sysdickey = gamePlayway.getCode() + "_num_" + mumofgames;
            List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(DataConstants.SYSTEM_GAME_COINSDECUTE);
            int coinnum = Integer.parseInt(MaziSummaryUtil.getObjWithName(sysDicList, sysdickey));

            Map<String, String> extparams = gameRoom.getExtparams();

            String coinexpend = extparams.get("coinexpend");//扣我的为Y ，扣大赢家的为 N
            logger.info("房间钻石消耗类型 coinexpend[" + coinexpend + "]");

            PlayUser playUser = null;

            if ("Y".equals(coinexpend)) {
                logger.info("扣房主的钻石");
                playUser = BMDataContext.getContext().getBean(PlayUserRepository.class).findByGameid(gameRoom.getCreater());
                logger.info("玩家 [" + playUser.getUsername() + "] 网豆[" + playUser.getDiamonds() + "] 扣除数量 [" + coinnum + "]");
            } else if ("N".equals(coinexpend)) {
                playUser =  BMDataContext.getContext().getBean(PlayUserRepository.class).findByGameid(getMaxWinner(gameRoom));
            } else {
                logger.info("房间钻石消耗类型 coinexpend[" + coinexpend + "] 错误");
            }

            playUser.setDiamonds(playUser.getDiamonds() - coinnum);
            BMDataContext.getContext().getBean(PlayUserRepository.class).save(playUser);


            MaziScoreUtil.synchronizationUsercache(playUser);

            return coinnum;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 房间号[" + gameRoom.getRoomid() + "] 扣卡处理失败", e);

            return 0;

        }

    }

    public static String getMaxWinner(GameRoom gameroom) {

        for (SummaryPlayer player : gameroom.getSummary().getSummaryPlayer()) {
            if (1 == player.getWinflag())
                return player.getGameid();
        }
        logger.info("扣卡过程 未找到大赢家 gameroom[" + gameroom.getRoomid() + "]");
        return null;

    }

    public static void dealHiscard(Board board, MaziTakeCards maziTakeCards, Player player) {
        board.setCardorder(board.getCardorder() + 1);

        Hiscard hiscard = new Hiscard();
        UKTools.copyProperties(maziTakeCards, hiscard);
        UKTools.copyProperties(player, hiscard);
        hiscard.setOrdercards(board.getCardorder());

        if (!ArrayUtils.isEmpty(maziTakeCards.getCards())) {
            String cardstr = "";
            for (byte b : maziTakeCards.getCards()) {
                cardstr += b + ",";
            }

            hiscard.setCards(cardstr);
        }

        hiscard.setCurrentnum(board.getCurrentnum());

        BMDataContext.getContext().getBean(HiscardRepository.class).save(hiscard);

    }

    public static void dealHisplayer(Player player, GameRoom gameRoom) {

        Hisplayer hisplayer = new Hisplayer();
        UKTools.copyProperties(player, hisplayer);
        hisplayer.setSitindex(player.getIndex());

        if (!ArrayUtils.isEmpty(player.getCards())) {
            String cardstr = "";
            for (byte b : player.getCards()) {
                cardstr += b + ",";
            }

            hisplayer.setCardstr(cardstr);
        }
        hisplayer.setCurrentnum(gameRoom.getCurrentnum());

        BMDataContext.getContext().getBean(HisplayerRepository.class).save(hisplayer);

    }





}
