package com.mile.core.game.mazi.msgmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;

public class SeecardsEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String userid;

	private byte[] cards;

	private String command;


	private byte[] leftcards;//剩下的牌

	private String leftuserid;//剩下牌的 玩家ID



	public SeecardsEvent(String userid,byte[] cards){
		this.setUserid(userid);
		this.setCards(MaziCardtypeUtil.settleCards(cards,null));

	}

	public SeecardsEvent(){
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public byte[] getCards() {
		return cards;
	}

	public void setCards(byte[] cards) {
		this.cards = cards;
	}

	public byte[] getLeftcards() {
		return leftcards;
	}

	public void setLeftcards(byte[] leftcards) {
		this.leftcards = leftcards;
	}

	public String getLeftuserid() {
		return leftuserid;
	}

	public void setLeftuserid(String leftuserid) {
		this.leftuserid = leftuserid;
	}
}
