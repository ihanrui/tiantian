package com.mile.core.game.mazi.task;

import com.mile.common.cache.CacheHelper;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.GiveupEvent;
import com.mile.core.engine.game.eventmodel.GiveupInfo;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import org.apache.commons.lang.ArrayUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * 解散消息
 */
public class CreateMaziGiveupTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(CreateMaziGiveupTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateMaziGiveupTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {
        try {
            if (null != gameRoom) {
                List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                Player[] players = board.getPlayers();

                int num = 0;
                if (!ArrayUtils.isEmpty(players)) {
                    for (Player p : players) {
                        if (1 == p.getGiveupflag()) {
                            num++;
                        }
                    }

                }

                List<GiveupInfo> giveupInfoList = new LinkedList<GiveupInfo>();

                for (PlayUserClient playUserClient : playerUserList) {
                    Player player = board.player(playUserClient.getId());
                    giveupInfoList.add(new GiveupInfo(playUserClient.getId(), playUserClient.getUsername(),
                            playUserClient.getUserurl(), player.getGiveupflag()));

                }

                GiveupEvent giveupEvent = new GiveupEvent();
                giveupEvent.setGiveupInfoList(giveupInfoList);

                giveupEvent.setUserid(board.getGiveupuserid());
                giveupEvent.setGiveupbegindate(board.getDissbegindate());
                giveupEvent.setGiveupsecond(board.getDisssecond());
                giveupEvent.setDealflag(1);//超时处理

                logger.info("投降的人共有 num【" + num + "】gameRoom.getPlayers() - 1 [" + (gameRoom.getPlayers() - 1) + "]，为执行解散， 继续打牌");

                if (gameRoom != null && num >= gameRoom.getPlayers() - 1) {
                    giveupEvent.setGiveupflag(1);
                    CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateMaziStepCleanTask(0.0, gameRoom, gameRoom.getOrgi()));
                } else {
                    giveupEvent.setGiveupflag(2);////投降标志 0.默认 无操作。 1.同意投降  2.拒绝投降
                    for (Player player : board.getPlayers()) {
                        logger.info("恢复玩家 player[" + player.getPlayuser() + "] [" + player.getUsername() + "]的状态");
                        player.setGiveupflag(0);

                    }
                    board.setGiveupteamflag(0);
                    board.setGiveupsecond(0);
                    board.setGiveupuserid(null);
                    board.setGiveupbegindate(null);
                }
                CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_MAZIGIVEUP, giveupEvent, gameRoom);
            }
        } catch (Exception e) {
            logger.error("玩家投降失败", e);
        }

    }
}
