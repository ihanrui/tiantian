package com.mile.core.game.mazi.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by michael on 3/12/18.
 */
public class MaziInnertestUtil {

    private static final Logger logger = LoggerFactory.getLogger(MaziInnertestUtil.class);

    //牌型  1,单张，2，对子，3，三张，4，四张，5，连对，6，连三张，7，连四张
    // 炸从小到大排序
    // 11，副510，12，方正510K 13，梅花510K  14，红桃510K  15，黑桃510K
    // 16，两个小王，17，一个大王一个小王 18，两个大王，19，五炸 20 ，三个大小王
    // 21 六炸  22，四个大小王，23 七炸  24 八炸
    //  牌从24开始， 一直到130

    public static Map getInnercards(Map<String,String > param) {

        Map map = new HashMap();

        String usernumtype = param.get(DataConstants.GAME_EVENT_INNERTEST);
        logger.info("MaziInnertestUtil 内部测试["+usernumtype+"]");

        if (null != usernumtype) {
            String[] strs = usernumtype.split(":");
            if(!isGiveuptestUserid(strs[0])){
               return new HashMap();
            }
            map.put("userid",strs[0]);
            map.put("cards",getInnercards(Integer.parseInt(strs[1]),Integer.parseInt(strs[2])));
        }


        return map;

    }

    public static List  getInnercards(int num,int type){
        List<Byte> bytes = new LinkedList<Byte>();
        switch (type){
            case 15:bytes=getK105(num);break;
            case 18:bytes.add((byte)106);bytes.add((byte)107);break;
            case 19:bytes=getBorn(num,5);break;
            case 20:bytes.add((byte)105);bytes.add((byte)106);bytes.add((byte)107);break;
            case 21:bytes=getBorn(num,6);break;
            case 22:bytes.add((byte)104);bytes.add((byte)105);bytes.add((byte)106);bytes.add((byte)107);break;
            case 23:bytes=getBorn(num,7);break;
            case 24:bytes=getBorn(num,8);break;
            default:break;
        }

        return bytes;
    }

    public static List getK105(int num){
        List<Byte> bytes = new LinkedList<Byte>();

        for(int i=16,j=0;i<16+num;i++,j++) {
            bytes.add((byte)i);
        }

        for(int i=56,j=num;i<56+num;i++,j++) {
            bytes.add((byte)i);
        }

        for(int i=80,j=num*2;i<80+num;i++,j++) {
            bytes.add((byte)i);
        }

        return bytes;
    }


    public static List<Byte> getBorn(int num,int bornnum){

        if(bornnum*num > 27)
            num = 1;

        List<Byte> bytes = new LinkedList<Byte>();

        List<Integer> list = new ArrayList();
        for(int i=0;i<num;i++){
            for(int j=0;j<50;j++){
                int n = new Random().nextInt(12);
                if(list.indexOf(n) >= 0 )
                    continue;
                else{
                    list.add(n);
                    break;
                }

            }
        }


        if(null != list){
            for(int i=0;i<list.size();i++){
                 for(int j=0;j<bornnum;j++){
                     bytes.add(Byte.parseByte((list.get(i)*8+j)+""));
                 }
            }
        }



        return bytes;
    }



    public static boolean  isGiveuptestGameid(String gameid){
        Useragentmap useragentmap  = BMDataContext.getContext().getBean(UseragentmapRepository.class).findByGameid(gameid);
        return (null != useragentmap && DataConstants.COMPANY_DATE.equals(useragentmap.getAgent5()));
    }


    public static boolean  isGiveuptestUserid(String userid){
        PlayUser playUser =  BMDataContext.getContext().getBean(PlayUserRepository.class).findById(userid);
        return (null !=  playUser && isGiveuptestGameid(playUser.getGameid()) );
    }







}
