package com.mile.core.game.mazi.msgmodel;

import com.mile.common.entity.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsInfo extends BaseEntity{
	private static final Logger logger = LoggerFactory.getLogger(NewsInfo.class);


	private String userid;

	private int newscore;

	private int totalscore;

	public NewsInfo(String userid, int newscore, int totalscore) {
		this.userid = userid;
		this.newscore = newscore;
		this.totalscore = totalscore;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getNewscore() {
		return newscore;
	}

	public void setNewscore(int newscore) {
		this.newscore = newscore;
	}

	public int getTotalscore() {
		return totalscore;
	}

	public void setTotalscore(int totalscore) {
		this.totalscore = totalscore;
	}
}
