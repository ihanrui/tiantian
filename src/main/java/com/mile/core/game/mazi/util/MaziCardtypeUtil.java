package com.mile.core.game.mazi.util;

import com.mile.core.BMDataContext;
import com.mile.util.GameUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by michael on 3/12/18.
 */
public class MaziCardtypeUtil {

    private static final Logger logger = LoggerFactory.getLogger(MaziCardtypeUtil.class);

    //牌型  1,单张，2，对子，3，三张，4，四张，5，连对，6，连三张，7，连四张
    // 炸从小到大排序
    // 11，副510，12，方正510K 13，梅花510K  14，红桃510K  15，黑桃510K
    // 16，两个小王，17，一个大王一个小王 18，两个大王，19，五炸 20 ，三个大小王
    // 21 六炸  22，四个大小王，23 七炸  24 八炸
    //  牌从24开始， 一直到130

    public static int getMaziCardtype(List<Byte> cards) {
        if(null != cards && !cards.isEmpty()){
            byte[] bytes = new byte[cards.size()];
            for(int i=0;i<cards.size();i++){
                bytes[i] = cards.get(i) ;
            }
            return getMaziCardtype(bytes);
        }else
            logger.info("getMaziCardtype cards is null");
        return 0;

    }

    public static int getMaziCardtype(byte[] cards) {
        if (null == cards) {
            return BMDataContext.MaziCardsTypeEnum.NONE.getType();
        }

        Arrays.sort(cards);

        cards = GameUtils.reverseCards(cards);

        int mazicardtype = 0;
        switch (cards.length) {
            case 1:
                mazicardtype = BMDataContext.MaziCardsTypeEnum.ONE.getType();
                break;
            case 2:
                mazicardtype = getMaziCardtype2(cards);
                break;
            case 3:
                mazicardtype = getMaziCardtype3(cards);
                break;
            case 4:
                mazicardtype = getMaziCardtype4(cards);
                break;
            case 5:
                mazicardtype = getMaziCardtype5(cards);
                break;
            case 6:
                mazicardtype = getMaziCardtype6(cards);
                break;
            case 7:
                mazicardtype = getMaziCardtype7(cards);
                break;
            case 8:
                mazicardtype = getMaziCardtype8(cards);
                break;
            default:
                mazicardtype = getMaziCardtypeDefault(cards);
                break;

        }
        return mazicardtype;
    }

    public static int getMaziCardtype2(byte[] cards) {
        if (isBigGhost2(cards)) {
            return BMDataContext.MaziCardsTypeEnum.GHOST2DW.getType();
        } else if (isGhost2(cards)) {
            return BMDataContext.MaziCardsTypeEnum.GHOST2DX.getType();
        } else if (isPair(cards)) {
            return BMDataContext.MaziCardsTypeEnum.TWO.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype3(byte[] cards) {
        if (isF510K(cards)) {
            return BMDataContext.MaziCardsTypeEnum.F510K.getType();
        } else if (isZ510K(cards)) {
            return BMDataContext.MaziCardsTypeEnum.HEIT510K.getType() - cards[0] % 4;
        } else if (isGhost3(cards)) {
            return BMDataContext.MaziCardsTypeEnum.GHOST3.getType();
        } else if (isThree(cards)) {
            return BMDataContext.MaziCardsTypeEnum.THREE.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype4(byte[] cards) {
        if (isGhost4(cards)) {
            return BMDataContext.MaziCardsTypeEnum.GHOST4.getType();
        } else if (isPair(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK2.getType();
        } else if (isFour(cards)) {
            return BMDataContext.MaziCardsTypeEnum.FOUR.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype5(byte[] cards) {
        if (isBomn5(cards)) {
            return BMDataContext.MaziCardsTypeEnum.BOMB5.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype6(byte[] cards) {
        if (isBomn6(cards)) {
            return BMDataContext.MaziCardsTypeEnum.BOMB6.getType();
        } else if (isThree(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK3.getType();
        } else if (isPair(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK2.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype7(byte[] cards) {
        if (isBomn7(cards)) {
            return BMDataContext.MaziCardsTypeEnum.BOMB7.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtype8(byte[] cards) {
        if (isBomn8(cards)) {
            return BMDataContext.MaziCardsTypeEnum.BOMB8.getType();
        } else if (isFour(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK4.getType();
        } else if (isPair(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK2.getType();
        }

        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    public static int getMaziCardtypeDefault(byte[] cards) {
        if (isPair(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK2.getType();
        } else if (isThree(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK3.getType();
        } else if (isFour(cards)) {
            return BMDataContext.MaziCardsTypeEnum.LINK4.getType();
        }
        return BMDataContext.MaziCardsTypeEnum.NONE.getType();
    }

    /**
     * 判断是否为8炸
     *
     * @param cards
     * @return
     */
    public static boolean isBomn8(byte[] cards) {
        if (null == cards || cards.length != 8) {
            return false;
        }
        if ((cards[0]) / 8 == (cards[7]) / 8 &&
                (cards[1]) / 8 == (cards[7]) / 8 &&
                (cards[2]) / 8 == (cards[7]) / 8 &&
                (cards[3]) / 8 == (cards[7]) / 8 &&
                (cards[4]) / 8 == (cards[7]) / 8 &&
                (cards[5]) / 8 == (cards[7]) / 8 &&
                (cards[6]) / 8 == (cards[7]) / 8)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为7炸
     *
     * @param cards
     * @return
     */
    public static boolean isBomn7(byte[] cards) {
        if (null == cards || cards.length != 7) {
            return false;
        }
        if ((cards[0]) / 8 == (cards[6]) / 8 &&
                (cards[1]) / 8 == (cards[6]) / 8 &&
                (cards[2]) / 8 == (cards[6]) / 8 &&
                (cards[3]) / 8 == (cards[6]) / 8 &&
                (cards[4]) / 8 == (cards[6]) / 8 &&
                (cards[5]) / 8 == (cards[6]) / 8)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为6炸
     *
     * @param cards
     * @return
     */
    public static boolean isBomn6(byte[] cards) {
        if (null == cards || cards.length != 6) {
            return false;
        }
        if ((cards[0]) / 8 == (cards[5]) / 8 &&
                (cards[1]) / 8 == (cards[5]) / 8 &&
                (cards[2]) / 8 == (cards[5]) / 8 &&
                (cards[3]) / 8 == (cards[5]) / 8 &&
                (cards[4]) / 8 == (cards[5]) / 8)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为5炸
     *
     * @param cards
     * @return
     */
    public static boolean isBomn5(byte[] cards) {
        if (null == cards || cards.length != 5) {
            return false;
        }

        if ((cards[0]) / 8 == (cards[4]) / 8 &&
                (cards[1]) / 8 == (cards[4]) / 8 &&
                (cards[2]) / 8 == (cards[4]) / 8 &&
                (cards[3]) / 8 == (cards[4]) / 8)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为4鬼
     *
     * @param cards
     * @return
     */
    public static boolean isGhost4(byte[] cards) {
        if (null == cards || cards.length != 4) {
            return false;
        }

        if ((cards[0]) > 103
                && (cards[1]) > 103
                && (cards[2]) > 103
                && (cards[3]) > 103)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为3鬼
     *
     * @param cards
     * @return
     */
    public static boolean isGhost3(byte[] cards) {
        if (null == cards || cards.length != 3) {
            return false;
        }

        if ((cards[0]) > 103
                && (cards[1]) > 103
                && (cards[2]) > 103)
            return true;
        else
            return false;

    }

    /**
     * 判断是否为2鬼
     *
     * @param cards
     * @return
     */
    public static boolean isGhost2(byte[] cards) {
        if (null == cards || cards.length != 2) {
            return false;
        }

        if ((cards[0]) > 103
                && (cards[1]) > 103
                && (cards[0] < 106 || cards[1] < 106))
            return true;
        else
            return false;

    }

    /**
     * 判断是否为2个大鬼
     *
     * @param cards
     * @return
     */
    public static boolean isBigGhost2(byte[] cards) {
        if (null == cards || cards.length != 2) {
            return false;
        }

        if ((cards[0]) > 105
                && (cards[1]) > 105)
            return true;
        else
            return false;

    }

    /**
     * 判断副510K
     *
     * @param cards
     * @return
     */
    public static boolean isF510K(byte[] cards) {
        if (null == cards || cards.length != 3) {
            return false;
        }

        if ((cards[0]) / 8 == 10
                && (cards[1]) / 8 == 7
                && (cards[2]) / 8 == 2
                && (cards[0] % 4 != cards[1] % 4 || cards[0] % 4 != cards[2] % 4))
            return true;
        else
            return false;

    }

    /**
     * 判断是否正510
     *
     * @param cards
     * @return
     */
    public static boolean isZ510K(byte[] cards) {
        if (null == cards || cards.length != 3) {
            return false;
        }

        if ((cards[0]) / 8 == 10
                && (cards[1]) / 8 == 7
                && (cards[2]) / 8 == 2
                && (cards[0] % 4 == cards[1] % 4 && cards[0] % 4 == cards[2] % 4))
            return true;
        else
            return false;

    }

    public static boolean isPair(byte[] cards) {
        if (null == cards || cards.length % 2 != 0) {
            return false;
        }

        for (int card : cards)
            if (card > 103)
                return false;

        for (int i = 0; i < cards.length; i = i + 2) {
            if (cards[i] / 8 != cards[i + 1] / 8)
                return false;
            if (i + 2 < cards.length && (cards[i] / 8 - cards[i + 2] / 8 != 1))
                return false;
        }
        return true;

    }

    public static boolean isThree(byte[] cards) {
        if (null == cards || cards.length % 3 != 0) {
            return false;
        }

        for (int i = 0; i < cards.length; i = i + 3) {
            if (cards[i] / 8 != cards[i + 1] / 8 || cards[i] / 8 != cards[i + 2] / 8)
                return false;
            if (i + 3 < cards.length && (cards[i] / 8 - cards[i + 3] / 8 != 1))
                return false;
        }
        return true;

    }

    public static boolean isFour(byte[] cards) {

        if (null == cards || cards.length % 4 != 0) {
            return false;
        }

        for (int i = 0; i < cards.length; i = i + 4) {
            if (cards[i] / 8 != cards[i + 1] / 8 || cards[i] / 8 != cards[i + 2] / 8 || cards[i] / 8 != cards[i + 3] / 8)
                return false;
            if (i + 4 < cards.length && (cards[i] / 8 - cards[i + 4] / 8 != 1))
                return false;
        }
        return true;

    }

    /**
     * 必须从大到小排序 b
     *
     * @param cards
     * @return
     */
    public static byte[] settleCards(byte[] cards, Map retmap) {
        if (null == cards) {
            logger.info("待理牌的数据为空");
            return null;
        }

        if(null == retmap)
            retmap = new HashedMap();

        Arrays.sort(cards);

        cards = GameUtils.reverseCards(cards);

        //排序时都用linkedList
        List<Byte> retlist = new LinkedList<Byte>();

        byte[] retCards = new byte[cards.length];

        Map<Integer, Integer> types = new HashMap<Integer, Integer>();

        List<Integer> born = new LinkedList<Integer>();

        List<List<Byte>> bornlist = new LinkedList<List<Byte>>();

        //记录每个牌的值
        Map<Integer, List<Byte>> mapValue = new HashMap<Integer, List<Byte>>();

        int max = -1, maxcard = -1, cardtype = 0, mincard = -1, min = 200;
        for (int i = 0; i < cards.length; i++) {
            int card = cards[i] / 8;
            Integer num = 0;
            if (types.get(card) == null) {
                num++;
            } else {
                num = types.get(card) + 1;
            }

            types.put(card, num);
            if (types.get(card) > max) {
                max = types.get(card);
                maxcard = card;
            }
            if (types.get(card) == max) {
                if (mincard < 0 || mincard > card) {
                    mincard = card;
                }
            }

            //记录板子
            if (num == 5) { //板子
                born.add(card);
            }

            //记录每个牌的值
            List<Byte> lb = mapValue.get(card);
            if (null == lb) {
                lb = new LinkedList<Byte>();
            }

            lb.add(cards[i]);
            mapValue.put(card, lb);

        }

        //logger.info("处理板子");
        //先放炸弹 版子
        if (born.size() > 0) {
            for (int i = born.size() - 1; i >= 0; i--) {
                retlist.addAll(mapValue.get(born.get(i)));
                bornlist.add((mapValue.get(born.get(i))));//放板子放到list中
                mapValue.remove(born.get(i));

            }
        }

        //logger.info("处理5，10，");
        dealK105(mapValue, retlist, bornlist);

        //logger.info("处理ghost");

        List<Byte> ghost = mapValue.get(13);
        if (null != ghost && ghost.size() > 1) {
            bornlist.add(ghost);
            retlist.addAll(ghost);
            mapValue.remove(13);

        }

        retmap.put("born", bornlist);

        //logger.info("处理剩下的");
        for (int i = 13; i > -1; i--) {
            if (null != mapValue.get(i)) {
                retlist.addAll(mapValue.get(i));

            }

        }

        retmap.putAll(mapValue);

        for (int i = 0; i < retlist.size(); i++)
            retCards[i] = retlist.get(i);

        return retCards;
    }

    public static boolean dealK105(Map<Integer, List<Byte>> mapValue, List<Byte> retlist, List<List<Byte>> bornlist) {
        //再放黑红梅芳副5，10，K
        List<Byte> l10 = mapValue.get(10);
        List<Byte> l7 = mapValue.get(7);
        List<Byte> l2 = mapValue.get(2);

        if (null != l10 && null != l7 && null != l2
                && !l10.isEmpty() && !l7.isEmpty() && !l2.isEmpty()
                ) {

            int minSize = l10.size() > l7.size() ? l7.size() : l10.size();
            minSize = l2.size() > minSize ? minSize : l2.size();

            for (int i = minSize-1; i >= 0; i--) {//最多允许3套副K105,4套正k105
                Byte ck = l10.get(i);
                Byte c10 = getValueByNumColor(l7, 7, ck % 4);
                Byte c5 = getValueByNumColor(l2, 2, ck % 4);

                if (null != c10 && null != c5) {
                    retlist.add(ck);
                    l10.remove(ck);
                    retlist.add(c10);
                    l7.remove(c10);
                    retlist.add(c5);
                    l2.remove(c5);

                    List<Byte> list = new LinkedList<Byte>();
                    list.add(ck);
                    list.add(c10);
                    list.add(c5);
                    bornlist.add(list);

                } else {
                    Byte c102 = l7.get(i);
                    Byte ck2 = getValueByNumColor(l10, 10, c102 % 4);
                    Byte c52 = getValueByNumColor(l2, 2, c102 % 4);
                    if (null != ck2 && null != c52) {
                        retlist.add(ck2);
                        l10.remove(ck2);
                        retlist.add(c102);
                        l7.remove(c102);
                        retlist.add(c52);
                        l2.remove(c52);

                        List<Byte> list = new LinkedList<Byte>();
                        list.add(ck2);
                        list.add(c102);
                        list.add(c52);
                        bornlist.add(list);

                    }else {
                        Byte c53 = l2.get(i);
                        Byte ck3 = getValueByNumColor(l10, 10, c53 % 4);
                        Byte c103 = getValueByNumColor(l7, 7, c53 % 4);;
                        if (null != ck3 && null != c103) {
                            retlist.add(ck3);
                            l10.remove(ck3);
                            retlist.add(c103);
                            l7.remove(c103);
                            retlist.add(c53);
                            l2.remove(c53);

                            List<Byte> list = new LinkedList<Byte>();
                            list.add(ck3);
                            list.add(c103);
                            list.add(c53);
                            bornlist.add(list);
                        }else {
                            retlist.add(ck);
                            l10.remove(ck);
                            retlist.add(c102);
                            l7.remove(c102);
                            retlist.add(c53);
                            l2.remove(c53);

                            List<Byte> list = new LinkedList<Byte>();
                            list.add(ck);
                            list.add(c102);
                            list.add(c53);
                            bornlist.add(list);
                        }
                    }
                }

            }

        }
        return true;
    }


    public static Byte getValueByNumColor(List<Byte> list, int num, int color) {
        if (null == list || list.isEmpty()) {
            return null;
        }
        for (Byte b : list) {
            if (b / 8 == num && b % 4 == color)
                return b;
        }
        return null;
    }

    public static void main(String[] args) {

    }

    public static byte[] str2byte(String str) {
        if (null == str)
            return null;
        String[] strs = str.split(",");

        byte[] ret = new byte[strs.length];
        for (int i = 0; i < strs.length; i++) {
            if (null != strs[i]) {
                ret[i] = Byte.parseByte(strs[i].trim());
            } else {
                logger.info("有空值，请注意异常!!!!");
            }
        }

        return ret;
    }







}
