package com.mile.core.game.mazi.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.engine.game.eventmodel.StepSumEvent;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;




@Entity
@Table(name = "wd_r_takecardhistory")
@org.hibernate.annotations.Proxy(lazy = false)
public class Takecardshistory extends BaseEntity{

	private static final long serialVersionUID = -1L;



	private String id = UUID.randomUUID().toString();

	private String gameroomid;//房间UUID

	private String roomid;//房间号

	private String boardid;//board uuid

	private List<Player> playerList = new ArrayList<Player>();

	private List<TakeCards> takeCardsList = new LinkedList<TakeCards>();

	private StepSumEvent stepSumEvent;//小结事件

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getGameroomid() {
		return gameroomid;
	}

	public void setGameroomid(String gameroomid) {
		this.gameroomid = gameroomid;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getBoardid() {
		return boardid;
	}

	public void setBoardid(String boardid) {
		this.boardid = boardid;
	}

	@Transient
	public List<Player> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(List<Player> playerList) {
		this.playerList = playerList;
	}

	@Transient
	public List<TakeCards> getTakeCardsList() {
		return takeCardsList;
	}

	public void setTakeCardsList(List<TakeCards> takeCardsList) {
		this.takeCardsList = takeCardsList;
	}

	@Transient
	public StepSumEvent getStepSumEvent() {
		return stepSumEvent;
	}

	public void setStepSumEvent(StepSumEvent stepSumEvent) {
		this.stepSumEvent = stepSumEvent;
	}
}
