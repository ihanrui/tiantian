package com.mile.core.game.mazi.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.Message;

public class SettleCardEvent extends BaseEntity implements Message{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;


	private byte[] cards;

	private String command;


	public SettleCardEvent(byte[] cards,String command) {
		this.setCards(cards);
		this.setCommand(command);
	}


	public byte[] getCards() {
		return cards;
	}

	public void setCards(byte[] cards) {
		this.cards = cards;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}
}
