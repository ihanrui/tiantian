package com.mile.core.game.mazi.task;

import com.mile.core.DataConstants;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.engine.game.eventmodel.DissroomEvent;
import com.mile.core.engine.game.eventmodel.DissroomInfo;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.common.cache.CacheHelper;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import org.apache.commons.lang.ArrayUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 解散消息
 */
public class CreateMaziDissTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(CreateMaziDissTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateMaziDissTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {
        try {
            if (null != gameRoom) {
                List<PlayUserClient> playerUserList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                Player[] players = board.getPlayers();

                int num = 0;
                if (!ArrayUtils.isEmpty(players)) {
                    for (Player p : players) {
                        if (1 == p.getDissflag()) {
                            num++;
                        }
                    }

                }

                List<DissroomInfo> dissroomInfoList = new LinkedList<DissroomInfo>();

                for (PlayUserClient playUserClient : playerUserList) {
                    Player player = board.player(playUserClient.getId());
                    dissroomInfoList.add(new DissroomInfo(playUserClient.getId(), playUserClient.getUsername(),
                            playUserClient.getUserurl(), player.getDissflag()));

                }

                DissroomEvent dissroomEvent = new DissroomEvent();
                dissroomEvent.setDissroomInfoList(dissroomInfoList);

                dissroomEvent.setUserid(board.getDissuserid());
                dissroomEvent.setDissbegindate(board.getDissbegindate());
                dissroomEvent.setDisssecond(board.getDisssecond());
                dissroomEvent.setDealflag(1);//超时处理

                logger.info("解散的人共有 num【" + num + "】playerUserList.size() - 1 [" + (playerUserList.size() - 1) + "]，为执行解散， 继续打牌");

                if (playerUserList != null && num >= playerUserList.size() - 1) {
                    dissroomEvent.setDissflag(1);
                    CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateMaziCleanTask(0.0, gameRoom, gameRoom.getOrgi()));
                } else {
                    dissroomEvent.setDissflag(2);
                    board.setDissbegindate(null);
                    board.setDissuserid(null);
                    board.setDisssecond(0);
                }
                CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_DISSROOM, dissroomEvent, gameRoom);
            }
        } catch (Exception e) {
            logger.error("玩家解散房间失败", e);
        }

    }
}
