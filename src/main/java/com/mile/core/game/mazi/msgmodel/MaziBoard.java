package com.mile.core.game.mazi.msgmodel;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.CardType;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.BaseBoard;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.web.model.BeiMiDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael
 */

public class MaziBoard extends BaseBoard {

    private static final Logger logger = LoggerFactory.getLogger(MaziBoard.class);

    /**
     *
     */
    private static final long serialVersionUID = 6143646772231515350L;

    public TakeCards takeCardsRequest(GameRoom gameRoom, Board board, Player player,
                                      String orgi, boolean auto, byte[] playCards) {

        MaziTakeCards takeCards = null;

        boolean automic = false;

        if ((playCards != null)) {
            CardType playCardType = null;
            if (playCards != null && playCards.length > 0) {
                playCardType = ActionTaskUtils.identificationMazi(playCards);
            }
            logger.info("牌型  playCardType[" + playCardType + "]");
            if (playCardType.getMazicardtype() > 0) {
                logger.info("当前无出牌信息，刚开始出牌，或者出牌无玩家 压 ");
                if (board.getLast() == null || board.getLast().getUserid().equals(player.getPlayuser())) {    //当前无出牌信息，刚开始出牌，或者出牌无玩家 压
                    takeCards = (MaziTakeCards) board.takecard(player, playCards);
                    ((MaziBoard) board).recordScore(board, playCards);

                } else {
                    CardType lastCardType = ActionTaskUtils.identificationMazi(board.getLast().getCards());
                    if (playCardType.getMazicardtype() > 0 && ActionTaskUtils.maziAllow(playCardType, lastCardType)) {//合规，允许出牌
                        //board.takecard(player , true , playCards) ;
                        takeCards = (MaziTakeCards) board.takecard(player, playCards);
                        ((MaziBoard) board).recordScore(board, playCards);

                        logger.info("有压牌，不过重置为0");
                        board.setNonums(0);

                    } else {//不合规的牌 ， 需要通知客户端 出牌不符合规则 ， 此处放在服务端判断，防外挂
                        logger.info("要不起牌，takeCards为空");
                    }
                }
            } else {
                logger.info("未解析出正确的牌型");
            }
        } else {
            logger.info("出牌信息为空");
            takeCards = new MaziTakeCards();
            takeCards.setUserid(player.getPlayuser());
            logger.info("增加不出者的人数");
            board.setNonums(board.getNonums() + 1);
            takeCards.setRoundscore(board.getRoundscore());//点不出的时候，分数赋值

        }
        if (takeCards != null) {
            MaziActionTaskUtils.formatTakkeCard(takeCards, board, player, gameRoom);
            /**
             * 移除定时器，然后重新设置
             */
            CacheHelper.getExpireCache().remove(gameRoom.getRoomid());

            player.setTakecards(takeCards.clone());

            if(MaziScoreUtil.countOver(board, player) && MaziScoreUtil.isLasthands(board.getPlayers())) {
                logger.info("房间号["+gameRoom.getRoomid()+"]最后一手牌，tableclean为false");
                takeCards.setTableclean(false);
            }
            MaziActionTaskUtils.sendTakeCardEvent(takeCards, board,gameRoom,player);

        } else {
            logger.info("未符合出牌规则");
            takeCards = new MaziTakeCards();
            takeCards.setAllow(false);
            takeCards.setTipstr(BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_ROOM_TAKECARD_ERRTIP).get(0).getCode());
            logger.info("出牌不符合规则的， 只有自己才能收到");
            //MaziActionTaskUtils.sendTakeCardEvent(takeCards, gameRoom,player);
            player.setTakecards(takeCards.clone());
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_TAKECARDS,player.getPlayuser(),takeCards);

        }

        CacheHelper.getBoardCacheBean().put(gameRoom.getId(),board,gameRoom.getOrgi());

        return takeCards;
    }

    @Override
    public Summary stepSummary(Board board, GameRoom gameRoom, GamePlayway playway) {
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());


        Summary summary = gameRoom.getSummary();

        if (null == summary) {
            summary = new Summary();
            summary.setRoomid(gameRoom.getRoomid());
            summary.setGameplaywayid(gameRoom.getGamePlayway().getId());
            summary.setGameplaywaycode(gameRoom.getGamePlayway().getCode());
            summary.setGameroomid(gameRoom.getId());
            summary.setTeanum(gameRoom.getTeanum());
            summary.setTablenum(gameRoom.getTablenum());
            summary.setBegindate(gameRoom.getCreatetime());//开始时间

        }

        List<SummaryPlayer> summaryPlayerList = summary.getSummaryPlayer();
        //summary.setSummaryPlayer(new ArrayList<SummaryPlayer>());
        List<SummaryPlayer> summaryPlayers  =  new ArrayList<SummaryPlayer>();

        logger.info("房间号["+gameRoom.getRoomid()+"]一盘格式化信息");
        for (Player player : board.getPlayers()) {

            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(player.getPlayuser(), gameRoom.getOrgi());

            SummaryPlayer summaryPlayer = MaziScoreUtil.findSumByUserid(summary.getSummaryPlayer(), player.getPlayuser());

            //格式化小结结算信息
            summaryPlayer = MaziActionTaskUtils.formatStepSum(summaryPlayer, player,gameRoom);



            summaryPlayers.add(summaryPlayer);
        }
        summary.setSummaryPlayer(summaryPlayers);

        logger.info("房间号["+gameRoom.getRoomid()+"]设置大赢家和土豪");
        int winflag = 0;
        int richflag = 0;
        for (SummaryPlayer summaryPlayer : summary.getSummaryPlayer()) {
            winflag = summaryPlayer.getTotalscore() > winflag ? summaryPlayer.getTotalscore() : winflag;
            richflag = summaryPlayer.getTotalscore() < richflag ? summaryPlayer.getTotalscore() : richflag;

        }

        for (SummaryPlayer summaryPlayer : summary.getSummaryPlayer()) {
            summaryPlayer.setRichflag(richflag == summaryPlayer.getTotalscore() ? 1 : 0);
            summaryPlayer.setWinflag(winflag == summaryPlayer.getTotalscore() ? 1 : 0);

        }

        logger.info("房间号["+gameRoom.getRoomid()+"]码子单局结算信息  summary [" + summary + "]");
        gameRoom.setSummary(summary);

        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(),gameRoom,gameRoom.getOrgi());


        return summary;
    }

    /**
     * @param player
     * @param current
     * @return
     */
    public TakeCards takecard(Player player, byte[] playCards) {
        MaziTakeCards takeCards = new MaziTakeCards();
        takeCards.init(player, playCards);

        return takeCards;
    }

    public void recordScore(Board board, byte[] playCards) {
        for (byte p : playCards) {
            int s = p / 8;
            if (s == 2 || s == 7 || s == 10) {
                logger.info("添加出分信息  p[" + p + "]");
                board.getCardK105().add(p);
            }
        }

    }

    /**
     * 当前玩家随机出牌，能管住当前出牌的 最小牌
     *
     * @param player
     * @param current
     * @return
     */
    public TakeCards cardtip(Player player, TakeCards last) {
        MaziTakeCards maziTakeCards = new MaziTakeCards();
        maziTakeCards.cardtip(player, last);
        return maziTakeCards;
    }

}
