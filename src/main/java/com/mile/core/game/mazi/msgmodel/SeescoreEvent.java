package com.mile.core.game.mazi.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.Message;

import java.util.List;

public class SeescoreEvent extends BaseEntity implements Message{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String roomid;

	private List<Byte> cards;

	private String command;

	public SeescoreEvent(String roomid, List<Byte> cards) {
		this.roomid = roomid;
		this.cards = cards;
	}



	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public List<Byte> getCards() {
		return cards;
	}

	public void setCards(List<Byte> cards) {
		this.cards = cards;
	}
}
