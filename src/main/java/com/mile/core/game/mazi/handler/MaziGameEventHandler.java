package com.mile.core.game.mazi.handler;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.Token;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.BeiMiClient;
import com.mile.core.game.mazi.util.MaziInnertestUtil;
import com.mile.core.game.msgmodel.TestEvent;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class MaziGameEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(MaziGameEventHandler.class);

    protected SocketIOServer server;

    @Autowired
    public MaziGameEventHandler(SocketIOServer server) {
        this.server = server;
    }

    //报喜
    @OnEvent(value = DataConstants.GAME_EVENT_REPORTNEWS)
    public void onReportNews(SocketIOClient client, String data) {

        try {

            logger.info("报喜参数 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

            String newsflag = JSON.parseObject(data).getString("newsflag");

            BMDataContext.getMaziGameEngine().reportnews(beiMiClient, newsflag);
        } catch (Exception e) {
            logger.error("玩家报喜失败", e);
        }

    }

    //明牌
    @OnEvent(value = DataConstants.GAME_EVENT_SEECARDS)
    public void onSeecards(SocketIOClient client, String data) {
        try {
            logger.info("明牌事件消息 传入  data[" + data + "]");
            BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

            BMDataContext.getMaziGameEngine().seecards(beiMiClient);
        } catch (Exception e) {
            logger.error("玩家明牌失败", e);
        }

    }

    //理牌
    @OnEvent(value = DataConstants.GAME_EVENT_SETTLECARDS)
    public void onSettlecards(SocketIOClient client, String data) {

        logger.info("发送消息 传入  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

        String settleFlag = JSON.parseObject(data).getString("settleFlag");
        try {

            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                BMDataContext.getMaziGameEngine().settlecards(beiMiClient, token, settleFlag);

            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("玩家理牌异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家理牌异常", false));
        }

    }

    @OnEvent(value = DataConstants.GAME_EVENT_INNERTEST)
    public void onInnertest(SocketIOClient client, String data) {

        logger.info("内部测试  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);
        String usernumtype = JSON.parseObject(data).getString("usernumtype");

        try {
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

                GameRoom gameRoom = null;
                if (!StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
                    gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, userToken.getOrgi());
                    gameRoom.putExtparams(DataConstants.GAME_EVENT_INNERTEST, usernumtype);
                    logger.info("设置 ROOM 参数  " + gameRoom.getExtparams().get(DataConstants.GAME_EVENT_INNERTEST));
                    CacheHelper.getGameRoomCacheBean().put(roomid, gameRoom, gameRoom.getOrgi());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统测试异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统测试异常", false));
        }
    }

    @OnEvent(value = DataConstants.GAME_EVENT_SEESCORE)
    public void onSeescore(SocketIOClient client, String data) {
        logger.info("看分处理");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

        try {
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                BMDataContext.getMaziGameEngine().seescore(token);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("玩家看分异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统测试异常", false));

        }
    }

    @OnEvent(value = DataConstants.GAME_EVENT_GVIEUPTEST)
    public void giveuptest(SocketIOClient client, String data) {

        logger.info("内部测试  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client, data);

        try {
            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_GVIEUPTEST, userToken.getUserid(), new TestEvent(MaziInnertestUtil.isGiveuptestUserid(beiMiClient.getUserid())));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统测试异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统测试异常", false));
        }
    }

}