package com.mile.core.game.mazi.task;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.jpa.SummaryPlayerRepository;
import com.mile.core.record.jpa.SummaryRepository;
import com.mile.core.record.util.GamerecordUtil;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 一小局牌局结算
 */
public class CreateMaziCleanTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(CreateMaziCleanTask.class);

    private Double timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateMaziCleanTask(Double timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + new Double(timer * 1000).longValue();    //5秒后执行
    }

    public void execute() {

        try {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            board.setFinished(true);

            for (Player player : board.getPlayers()) {
                PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(player.getPlayuser(), this.orgi);
                if (playUserClient != null) {
                    if (!playUserClient.getPlayertype().equals(BMDataContext.PlayerTypeEnum.AI.toString())) {
                        playUserClient.setGamestatus(BMDataContext.GameStatusEnum.NOTREADY.toString());
                        playUserClient.setRoomready(false);
                        CacheHelper.getGamePlayerCacheBean().put(playUserClient.getId(), playUserClient, gameRoom.getOrgi());
                        CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, gameRoom.getOrgi());
                    }
                }
            }
            if (null == gameRoom.getSummary()) {
                logger.info("房间号[" + gameRoom.getRoomid() + "]1盘未打完的结算，不扣房卡");
                nullSummary(board, gameRoom);
            }

            Summary summary = gameRoom.getSummary();
            summary.setBegindate(gameRoom.getCreatetime());
            summary.setOverdate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
            summary.setTablenum(gameRoom.getTablenum());
            summary.setTeanum(gameRoom.getTeanum());
            summary.setRoomtype(gameRoom.getRoomtype());

            SummaryPlayerRepository summaryPlayerRepository = BMDataContext.getContext().getBean(SummaryPlayerRepository.class);

            GamerecordUtil.dealcoinrecord(gameRoom);
            logger.info("经营状况分析");
            GamerecordUtil.dealTeareport(gameRoom);

            for (SummaryPlayer summaryPlayer : gameRoom.getSummary().getSummaryPlayer()) {
                summaryPlayerRepository.save(summaryPlayer);
            }

            logger.info("保存总结算信息 roomid[" + gameRoom.getRoomid() + "]");
            BMDataContext.getContext().getBean(SummaryRepository.class).save(gameRoom.getSummary());

            gameRoom.setStatus(MileGameEnum.END.toString());
            sendEvent(DataConstants.GAME_EVENT_CLEAN, gameRoom.getSummary(), gameRoom);

            if (gameRoom.getNumofgames() >= 999999) {
                logger.info("金币场 不结束房间 roomid[" + gameRoom.getRoomid() + "]   前面的结算信息设置为空，重新开始");

                gameRoom.setStatus(MileGameEnum.WAITTING.toString());
                gameRoom.setSummary(null);

                CacheHelper.getBoardCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());

                CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

                GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.JOIN.toString(), Double.parseDouble(TeahouseUtil.getConstantsByName(DataConstants.CONST_CLEANSLEEP)));    //通知状态机 , 继续执行

                return;

            }

            for (Player player : board.getPlayers()) {
                CacheHelper.getGameRoomCacheBean().delete(player.getPlayuser(), gameRoom.getOrgi());
                CacheHelper.getRoomMappingCacheBean().delete(player.getPlayuser(), this.orgi);
            }

            CacheHelper.getGamePlayerCacheBean().clean(gameRoom.getId(), orgi);

            logger.info("保存房间信息 roomid[" + gameRoom.getRoomid() + "]");
            BMDataContext.getContext().getBean(GameRoomRepository.class).save(gameRoom);

            logger.info("清理房间中的缓存信息  roomid[" + gameRoom.getRoomid() + "]");
            BMDataContext.getGameEngine().finished(gameRoom, orgi);
        } catch (Exception e) {
            logger.error("牌局结算失败", e);
        }

    }

    public Summary nullSummary(Board board, GameRoom gameRoom) {
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        Summary summary = gameRoom.getSummary();

        if (null == summary) {
            summary = new Summary();
            summary.setGameroomid(gameRoom.getId());
            summary.setRoomid(gameRoom.getRoomid());
            summary.setGameplaywayid(gameRoom.getGamePlayway().getId());
            summary.setGameplaywaycode(gameRoom.getGamePlayway().getCode());
        }

        List<SummaryPlayer> summaryPlayerList = summary.getSummaryPlayer();

        summary.setSummaryPlayer(new ArrayList<SummaryPlayer>());

        logger.info("房间号[" + gameRoom.getRoomid() + "]NULL一盘格式化信息");
        for (Player player : board.getPlayers()) {

            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(player.getPlayuser(), gameRoom.getOrgi());

            SummaryPlayer summaryPlayer = MaziScoreUtil.findSumByUserid(summary.getSummaryPlayer(), player.getPlayuser());

            //格式化小结结算信息
            summaryPlayer = MaziActionTaskUtils.formatNULLStep(gameRoom, summary, summaryPlayer, playUserClient, player);

            summary.getSummaryPlayer().add(summaryPlayer);
        }

        logger.info("房间号[" + gameRoom.getRoomid() + "]NULL码子单局结算信息  summary [" + summary + "]");
        gameRoom.setSummary(summary);

        return summary;
    }
}
