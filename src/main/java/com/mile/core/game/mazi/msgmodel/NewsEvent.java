package com.mile.core.game.mazi.msgmodel;

import com.mile.core.game.Message;
import com.mile.core.game.msgmodel.Player;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class NewsEvent implements Message{
	private static final Logger logger = LoggerFactory.getLogger(NewsEvent.class);

	private String command ;


	private List<NewsInfo> ls ;

	private String userid;


	private boolean flag;//true:报喜成功，false报喜失败


	private String tipstr;//提示语句

	private List<Byte> listcard;


	private boolean newsflag;


	public NewsEvent(List<Player> playerList){//报喜成功
		if(null == ls)
			ls = new LinkedList<NewsInfo>();

		if(null != playerList &&  !playerList.isEmpty()) {
			for (Player p : playerList) {
				ls.add(new NewsInfo(p.getPlayuser(), p.getNewsscore(), p.getTotalscore()));
			}
		}else{
			logger.info("报喜消息构建失败，列表为空");
		}

		flag = true;

	}


	public NewsEvent(String tipstr){//报喜失败
		this.tipstr = tipstr;
		flag = false;

	}

	public NewsEvent(String tipstr,String userid){//报喜失败
		this.tipstr = tipstr;
		this.userid = userid;
		flag = false;

	}


	public NewsEvent(Player[] arrList){
		if(null == ls)
			ls = new LinkedList<NewsInfo>();

		if(!ArrayUtils.isEmpty(arrList)) {
			for (Player p : arrList) {
				ls.add(new NewsInfo(p.getPlayuser(), p.getNewsscore(), p.getTotalscore()));
			}
		}else{
			logger.info("报喜消息构建失败，数组为空");
		}

	}


	public NewsEvent(Player[] arrList,String  userid){
		this(arrList);
		this.userid = userid;
    }


	public NewsEvent(Player[] arrList,String userid,List<Byte> listcard){
		this(arrList);
		this.userid = userid;
		this.setListcard(listcard);


	}


	public NewsEvent(Player[] arrList,String userid,List<Byte> listcard,boolean newsflag){
		this(arrList);
		this.setListcard(listcard);
		this.setNewsflag(newsflag);
		this.userid = userid;


	}


	public NewsEvent(boolean newsflag){
		this.setNewsflag(newsflag);


	}






	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}


	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getTipstr() {
		return tipstr;
	}

	public void setTipstr(String tipstr) {
		this.tipstr = tipstr;
	}

	public List<Byte> getListcard() {
		return listcard;
	}

	public void setListcard(List<Byte> listcard) {
		this.listcard = listcard;
	}

	public boolean isNewsflag() {
		return newsflag;
	}

	public void setNewsflag(boolean newsflag) {
		this.newsflag = newsflag;
	}

	public List<NewsInfo> getLs() {
		return ls;
	}

	public void setLs(List<NewsInfo> ls) {
		this.ls = ls;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
