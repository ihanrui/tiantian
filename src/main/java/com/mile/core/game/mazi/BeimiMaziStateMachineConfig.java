package com.mile.core.game.mazi;

import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.engine.game.action.EnoughAction;
import com.mile.core.engine.game.action.EnterAction;
import com.mile.core.engine.game.action.JoinAction;
import com.mile.core.game.majiang.action.PlayCardsAction;
import com.mile.core.game.mazi.action.MaziClearAction;
import com.mile.core.game.mazi.action.MaziDissAction;
import com.mile.core.game.mazi.action.MaziGiveupAction;
import com.mile.core.game.mazi.action.MaziStepClearAction;
import com.mile.core.statemachine.BeiMiStateMachine;
import com.mile.core.statemachine.config.StateConfigurer;
import com.mile.core.statemachine.config.StateMachineTransitionConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeimiMaziStateMachineConfig<T, S>  {

	@Bean("mazi")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}

    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(MileGameEnum.NONE.toString())
                    .state(MileGameEnum.CRERATED.toString())
                    .state(MileGameEnum.WAITTING.toString())
                    .state(MileGameEnum.READY.toString())
                    .state(MileGameEnum.BEGIN.toString())
                    .state(MileGameEnum.PLAY.toString())
                    .state(MileGameEnum.END.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
		/**
		 * 状态切换：BEGIN->WAITTING->READY->PLAY->END
		 */
        transitions
	        .withExternal()
		    	.source(MileGameEnum.NONE.toString()).target(MileGameEnum.CRERATED.toString())
		    	.event(MileGameEvent.ENTER.toString()).action(new EnterAction<String,String>())
		    	.and()
		    .withExternal()
	        	.source(MileGameEnum.CRERATED.toString()).target(MileGameEnum.WAITTING.toString())
	        	.event(MileGameEvent.JOIN.toString()).action(new JoinAction<String,String>())
	        	.and()
            .withExternal()
                .source(MileGameEnum.WAITTING.toString()).target(MileGameEnum.READY.toString())
                .event(MileGameEvent.ENOUGH.toString()).action(new EnoughAction<String, String>())
                .and()


            .withExternal()
                .source(MileGameEnum.READY.toString()).target(MileGameEnum.PLAY.toString())
                .event(MileGameEvent.PLAYCARDS.toString()).action(new PlayCardsAction<String,String>())
                .and()
            .withExternal()
                .source(MileGameEnum.PLAY.toString()).target(MileGameEnum.STEPEND.toString())   //单局结算
                .event(MileGameEvent.STEPCLEARING.toString()).action(new MaziStepClearAction<String,String>())
                .and()
			.withExternal()
				.source(MileGameEnum.DISSROOM.toString()).target(MileGameEnum.END.toString())
				.event(MileGameEvent.DISSROOM.toString()).action(new MaziDissAction<String,String>())
				.and()
			.withExternal()
				.source(MileGameEnum.GIVEUP.toString()).target(MileGameEnum.END.toString())
				.event(MileGameEvent.GIVEUP.toString()).action(new MaziGiveupAction<String,String>())
				.and()
			.withExternal()
				.source(MileGameEnum.STEPEND.toString()).target(MileGameEnum.END.toString())
				.event(MileGameEvent.CLEARING.toString()).action(new MaziClearAction<String,String>())
				.and()
            ;
    }
}
