package com.mile.core.game.mazi.action;

import com.mile.core.BMDataContext;
import com.mile.core.game.mazi.task.CreateMaziDissTask;
import com.mile.core.statemachine.action.Action;
import com.mile.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.mile.core.statemachine.message.Message;
import com.mile.common.cache.CacheHelper;
import com.mile.core.entity.GameRoom;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 房间解散
 *
 * @param <T>
 * @param <S>
 */
public class MaziDissAction<T, S> implements Action<T, S> {

    private static final Logger logger = LoggerFactory.getLogger(MaziDissAction.class);

    @Override
    public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
        String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
        if(!StringUtils.isBlank(room)){
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ;
            if(gameRoom!=null){
                Double interval = new Double( message.getMessageHeaders().getHeaders().get("interval").toString() ) ;
                CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateMaziDissTask(interval , gameRoom , gameRoom.getOrgi()));
            }
        }
    }
}
