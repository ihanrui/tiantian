package com.mile.core.game.mazi.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.UserinfoEvent;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.record.entity.Stepsum;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.jpa.StepsumRepository;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.util.UKTools;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by michael on 3/18/18.
 */
public class MaziScoreUtil {

    private static final Logger logger = LoggerFactory.getLogger(MaziScoreUtil.class);

    /**
     * 计算本手牌的分数
     *
     * @param bytes
     * @return
     */
    public static int getScore(byte[] bytes) {
        int score = 0;
        if (ArrayUtils.isEmpty(bytes)) {
            logger.info("本次传入进来计算分数的牌为空");
            return 0;
        }
        for (byte b : bytes) {
            switch (b / 8) {
                case 2:
                    score += 5;
                    break;
                case 7:
                case 10:
                    score += 10;
                    break;
                default:
                    break;
            }
        }

        logger.info("本次计算的分数为 score[" + score + "]");

        return score;
    }

    /**
     * 计算玩家现在是几游
     *
     * @param playerList
     * @return
     */
    public static int getLvlnum(Player[] playerList) {
        int lvlnum = 0;

        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("本次传入进来计算上游的玩家为空");
            return 0;
        }
        for (Player p : playerList) {
            if (p.isOverflag()) {
                ++lvlnum;
            }
        }
        logger.info("本次计算的上游个数 lvlnum[" + lvlnum + "]");

        return lvlnum;
    }

    /**
     * 计算现在剩下的玩家
     *
     * @param playerList
     * @return
     */
    public static int getExistPlayer(Player[] playerList) {
        int lvlnum = 0;

        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("本次传入进来计算玩家的参数为空");
            return 0;
        }
        return playerList.length - getLvlnum(playerList);
    }

    /**
     * 计算玩家分数
     *
     * @param cardflag 是否有出牌信息
     * @return
     */
    public static boolean countScore(Board board, Player next, MaziTakeCards takeCards, Player player) {
        try {

            if (null != board.getNonums() && board.getNonums() >= getExistPlayer(board.getPlayers())) {
                logger.info(" 要不起(过)的数量，大于等于 现存活的玩家，那么分数就归最大游");
                Player p = getMaxLvl(board.getPlayers());
                if (null == p) {
                    logger.info("计算分数未找到对象 [" + next.getUsername() + " ]");
                    return false;
                }
                p.setScore(p.getScore() + board.getRoundscore());

                takeCards.setScoreuserid(p.getPlayuser());
                takeCards.setChangescore(p.getScore());
                board.setScore(0);
                board.setRoundscore(0);
                board.setNonums(0);
                takeCards.setAutomic(true);
                board.setLast(null);//得分后， 最后一手牌应为NULL
                takeCards.setTableclean(true);
                cleanPlayerTakecard(board);
                takeCards.setRoundscore(0);
                return true;

            }

            if (null != next && null != board &&
                    null != next.getPlayuser() && null != board.getLast() && null != board.getNonums()
                    && board.getNonums() == getExistPlayer(board.getPlayers()) - 1
                    && next.getPlayuser().equals(board.getLast().getUserid())) {
                logger.info(" 要不起(过)的数量，等于 现存活的玩家-1，那么分数归自己，谁出的牌就归谁");
                next.setScore(next.getScore() + board.getRoundscore());

                takeCards.setScoreuserid(next.getPlayuser());
                takeCards.setChangescore(next.getScore());
                board.setRoundscore(0);
                board.setScore(0);
                board.setNonums(0);
                board.setLast(null);//得分后， 最后一手牌应为NULL

                takeCards.setAutomic(true);
                takeCards.setTableclean(true);
                cleanPlayerTakecard(board);
                takeCards.setRoundscore(0);
                return true;
            }

            if (MaziScoreUtil.countOver(board, player) && MaziScoreUtil.isLasthands(board.getPlayers())) {
                player.setScore(player.getScore() + board.getRoundscore());

                takeCards.setScoreuserid(player.getPlayuser());
                takeCards.setChangescore(player.getScore());
                board.setScore(0);
                board.setRoundscore(0);
                board.setNonums(0);
                takeCards.setAutomic(true);
                board.setLast(null);//得分后， 最后一手牌应为NULL
                takeCards.setTableclean(true);
                cleanPlayerTakecard(board);
                takeCards.setRoundscore(0);
                return true;
            }
            return false;
        }catch(Exception e){
            logger.error("计算玩家分数异常",e);
            return false;
        }

    }

    /**
     * 通过游数 找到对应玩家
     *
     * @param playerList
     * @param lvl
     * @return
     */
    public static Player getPlayerwithLvl(Player[] playerList, int lvl) {

        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("通过游数 找到对应玩家 传入参数为空");

        }
        for (Player p : playerList) {
            if (lvl == p.getLvl())
                return p;
        }

        logger.info("通过游数 未找到对应玩家 ");
        return null;

    }

    /**
     * 通过游数 找到对应玩家
     *
     * @param playerList
     * @param lvl
     * @return
     */
    public static Player getMaxLvl(Player[] playerList) {

        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("通过游数 找到对应玩家 传入参数为空");
        }

        int lvl = 0;
        Player retP = null;
        for (Player p : playerList) {
            if (p.getLvl() > lvl) {
                retP = p;
                lvl = p.getLvl();
            }
        }

        logger.info("通过游数 找到最大游数玩家 retP[" + retP + "]");
        return retP;

    }

    public static boolean countOver(Board board, Player player) {
        boolean overflag = false;

        if (0 == MaziScoreUtil.getExistPlayer(board.getPlayers())) {
            logger.info(" 房间号  [" + board.getRoomid() + "] 1.4个人都出牌完成，牌局结束 ");
            overflag = true;
        } else if (1 == MaziScoreUtil.getExistPlayer(board.getPlayers()) && !player.isOverflag()) {
            logger.info(" 房间号  [" + board.getRoomid() + "] 2.3个人都出牌完成，轮到最后这个人来了，牌局结束 ");
            overflag = true;
        } else {

            Player p1 = getPlayerwithLvl(board.getPlayers(), 1);

            Player p2 = getPlayerwithLvl(board.getPlayers(), 2);

            if (null != p1 && null != p2) {

                if (p1.getTeamflag() == p2.getTeamflag()) {
                    logger.info("  房间号  [" + board.getRoomid() + "] 3.同一边的都走了，单局结束");
                    overflag = true;
                } else {
                    if (getScoreByTeamflag(board.getPlayers(), p1.getTeamflag()) >= 100 && getScoreByTeamflag(board.getPlayers(), p2.getTeamflag()) >= 50) {
                        logger.info(" 房间号  [" + board.getRoomid() + "] 4.一游走了 带了100 分，二游走了带了50分， 一游赢1分");
                        overflag = true;
                    }

                    if (getScoreByTeamflag(board.getPlayers(), p2.getTeamflag()) > 150) {
                        logger.info(" 房间号  [" + board.getRoomid() + "] 5.二游走了 带了150 分以上, 二游赢1分");
                        overflag = true;
                    }
                }
            }

        }
        return overflag;
    }

    public static boolean isLasthands(Player[] players) {
        boolean overflag = true;

        for (Player p : players) {
            if (!p.isLasthands() && !p.isOverflag()) {
                overflag = false;
                break;
            }
        }
        return overflag;
    }

    /**
     * 计算一边的分数
     *
     * @param teamflag
     * @return
     */
    public static int getScoreByTeamflag(Player[] playerList, int teamflag) {
        int score = 0;

        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("本次传入进来计算分数的参数为空");
            return 0;
        }
        for (Player p : playerList) {
            if (teamflag == p.getTeamflag())
                score += p.getScore();
        }

        return score;
    }

    public static SummaryPlayer findSumByUserid(List<SummaryPlayer> summaryPlayers, String userid) {
        if (null == summaryPlayers || summaryPlayers.isEmpty() || StringUtils.isEmpty(userid) || null == userid) {
            logger.info("查找结算对象方法， 传入参数未空");
            return null;
        }
        for (SummaryPlayer sp : summaryPlayers) {
            if (null != sp && null != sp.getUserid() && userid.equals(sp.getUserid()))
                return sp;
        }
        logger.info("查找结算对象，为找到对应的结算对象");
        return null;
    }

    public static void analyzeScore(Board board,GameRoom gameRoom) {
        if(board.getGiveupteamflag()>0){
            logger.info("投降处理 board.getGiveupteamflag()["+board.getGiveupteamflag()+"] roomid["+board.getRoomid()+"] userid["+board.getGiveupuserid()+"]");
            for (Player p : board.getPlayers()) {
                p.setStepscore(p.getTeamflag() == board.getGiveupteamflag()? -1 : 1);
                p.setTotalscore(p.getTotalscore() + p.getStepscore());
                p.setWinflag(p.getStepscore() > 0);
                p.setGiveupflag(0);
            }


            board.setGiveupteamflag(0);
            board.setGiveupsecond(0);
            board.setGiveupuserid(null);
            board.setGiveupbegindate(null);
            return;
        }


        String winner = board.getWinner();//一游
        int t1 = 0,//1对游数
                t2 = 0,//2对游数
                ts1 = 0,//1对分数
                ts2 = 0,//2对分数
                tw = 0; //上游的对数
        for (Player p : board.getPlayers()) {
            logger.info("房间号[" + board.getRoomid() + "]  玩家 [" + p.getPlayuser() + "] [" + p.getUsername() + "] 对数[" + p.getTeamflag() + "] 游数[" + p.getLvl() + "]");
            if (1 == p.getTeamflag()) {
                t1 += p.getLvl();
                ts1 += p.getScore();
            } else if (2 == p.getTeamflag()) {
                t2 += p.getLvl();
                ts2 += p.getScore();
            } else {
                logger.info("小结结算分数，玩家的对数未确定");
            }

            if (null != winner && winner.equals(p.getPlayuser())) {
                tw = p.getTeamflag();
            }
        }

        int ta1 = ts1 + (5 - t1) * 50;//1队总分数
        int ta2 = ts2 + (5 - t2) * 50;//2队总分数

        int r1 = 0;//1队的分数

        if (ta1 < 0) {
            r1 = -2;
        } else if (ta2 < 0) {
            r1 = 2;
        } else if (ta1 == ta2) {
            r1 = tw == 1 ? 1 : -1;
        } else {
            r1 = ta1 > ta2 ? 1 : -1;
        }

        logger.info("无分处理 [" + gameRoom.getNoscore() + "]");
        if ("Y".equals(gameRoom.getNoscore())) {
            logger.info("房间[" + gameRoom.getNoscore() + "]打无分 ");
            if ((ts1 <= 0 && r1 < 0 && t1 == 7 )) {
                r1 = -4;
            } else if (ts2 <= 0 && r1 > 0 && t2 == 7) {
                r1 = 4;
            }
        } else {
            logger.info("房间[" + gameRoom.getNoscore() + "]不打无分 ");
        }

        logger.info("房间号[" + board.getRoomid() + "]  计算分数变量  玩家游数t1[" + t1 + "] t2[" + t2 + "]  总分数 ta1[" + ta1 + "] " +
                "ta2[" + ta2 + "] 总抓分数ts1[" + ts1 + "] ts2[" + ts2 + "] 上游的对数tw[" + tw + "] 一队输赢 倍数  r1[" + r1 + "]");

        for (Player p : board.getPlayers()) {
            p.setStepscore(p.getTeamflag() == 1 ? r1 : -1 * r1);
            p.setTotalscore(p.getTotalscore() + p.getStepscore());
            p.setWinflag(p.getStepscore() > 0);
        }

    }

    public static void saveStepsum(GameRoom gameRoom, Player[] players) {
        StepsumRepository stepsumRepository = BMDataContext.getContext().getBean(StepsumRepository.class);
        for (Player player : players) {
            Stepsum stepsum = new Stepsum();
            UKTools.copyProperties(player, stepsum);
            stepsum.setCurnum(gameRoom.getCurrentnum());
            stepsum.setStepscore(stepsum.getStepscore()+player.getNewsscore());//加上报喜的分数
            stepsumRepository.save(stepsum);

        }

    }


    public static void cleanPlayerTakecard(Board board){
        for(Player p:board.getPlayers()){
                p.setTakecards(null);
        }
    }




    public static void  synchronizationUsercache(PlayUser playUser){

        PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());

        if (null != playUserClient) {
            playUserClient.setDiamonds(playUser.getDiamonds());
            playUserClient.setGoldcoins(playUser.getGoldcoins());

            CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, playUserClient.getOrgi());
            if (null != CacheHelper.getGamePlayerCacheBean().getPlayer(playUserClient.getId(), playUserClient.getOrgi())) {
                CacheHelper.getGamePlayerCacheBean().put(playUserClient.getId(), playUserClient, playUserClient.getOrgi());
            }
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_UPDATEUSER, playUser.getId(), new UserinfoEvent(playUserClient));

        }
    }

}