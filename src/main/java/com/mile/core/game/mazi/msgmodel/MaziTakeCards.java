package com.mile.core.game.mazi.msgmodel;

import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.Message;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * 当前出牌信息
 *
 */
public class MaziTakeCards extends TakeCards implements Message,Cloneable {



	private static final Logger logger = LoggerFactory.getLogger(MaziTakeCards.class);
	/**
	 *
	 */
	private static final long serialVersionUID = 8718778983090104033L;


	private int score;//本次牌的分数

	private int roundscore;//本轮分数总和

	private boolean seeflag=false;//明牌标志

	private byte[] mycards;

	private byte[] leftcards;//剩下的牌

	private String leftuserid;//剩下牌的 玩家ID

	private int cardnum;//小于等于5时才报警

	private boolean tableclean=false;//是否清理牌


	private String scoreuserid;//分数变化者ID

	private int changescore;//变化分数


	private int lvl=0;//几游





	public MaziTakeCards(){}

	/**
	 * 最小出牌 ， 管住 last
	 * @param player
	 * @param last
	 */
	public void cardtip(Player player , TakeCards last ){
		this.userid = player.getPlayuser() ;
		logger.info("last  ["+last+"]");
		if(last != null){
			this.tipcards = MaziTipsUtil.searchTips(player, last) ;
		}else{
			this.tipcards = MaziTipsUtil.searchTips(player) ;
		}
		if(cards!=null){
			this.allow = true ;
			this.cardType =  ActionTaskUtils.identificationMazi(cards);
			this.type = cardType.getCardtype() ;
		}
		this.cardsnum = player.getCards().length ;
	}


	public void init(Player player , byte[] tipcards ){
		this.userid = player.getPlayuser() ;
		this.cards = tipcards ;
		if(this.cards!=null){
			player.setCards(this.removeCards(player.getCards() , cards));
			this.cardType =  ActionTaskUtils.identificationMazi(cards);
			this.type = cardType.getCardtype() ;
			this.mazicardtype = MaziCardtypeUtil.getMaziCardtype(cards);

			this.allow = true ;

		}
		this.cardsnum = player.getCards().length ;
		this.allow = true;

	}











	/**
	 * 移除出牌
	 * @param cards
	 * @param start
	 * @param end
	 * @return
	 */
	public byte[] removeCards(byte[] cards , int start , int end){
		byte[] retCards = new byte[cards.length - (end - start)] ;
		int inx = 0 ;
		for(int i=0; i<cards.length ; i++){
			if(i<start || i >= end){
				retCards[inx++] = cards[i] ;
			}
		}
		return retCards ;
	}

	/**
	 * 移除出牌
	 * @param cards
	 * @param start
	 * @param end
	 * @return
	 */
	public byte[] removeCards(byte[] cards , byte[] playcards){
		List<Byte> tempArray = new ArrayList<Byte>();
		for(int i=0; i<cards.length ; i++){
			boolean found = false ;
			for(int inx = 0 ;inx<playcards.length ; inx++){
				if(cards[i] == playcards[inx]){
					found = true ; break ;
				}
			}
			if(found == false){
				tempArray.add(cards[i]);
			}
		}
		byte[] retCards = new byte[tempArray.size()] ;
		for(int i=0 ; i<tempArray.size() ; i++){
			retCards[i] = tempArray.get(i) ;
		}
		return retCards ;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getRoundscore() {
		return roundscore;
	}

	public void setRoundscore(int roundscore) {
		this.roundscore = roundscore;
	}

	public boolean isSeeflag() {
		return seeflag;
	}

	public void setSeeflag(boolean seeflag) {
		this.seeflag = seeflag;
	}

	@Transient
	public byte[] getLeftcards() {
		return leftcards;
	}

	public void setLeftcards(byte[] leftcards) {
		this.leftcards = leftcards;
	}

	@Transient
	public int getCardnum() {
		return cardnum;
	}

	public void setCardnum(int cardnum) {
		this.cardnum = cardnum;
	}

	public String getScoreuserid() {
		return scoreuserid;
	}

	public void setScoreuserid(String scoreuserid) {
		this.scoreuserid = scoreuserid;
	}

	public int getChangescore() {
		return changescore;
	}

	public void setChangescore(int changescore) {
		this.changescore = changescore;
	}

	public int getLvl() {
		return lvl;
	}

	public void setLvl(int lvl) {
		this.lvl = lvl;
	}

	public String getLeftuserid() {
		return leftuserid;
	}

	public void setLeftuserid(String leftuserid) {
		this.leftuserid = leftuserid;
	}

	@Transient
	public byte[] getMycards() {
		return mycards;
	}

	public void setMycards(byte[] mycards) {
		this.mycards = mycards;
	}

	public boolean isTableclean() {
		return tableclean;
	}

	public void setTableclean(boolean tableclean) {
		this.tableclean = tableclean;
	}


	@Override
	public MaziTakeCards clone() {
		try {
			return (MaziTakeCards) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
