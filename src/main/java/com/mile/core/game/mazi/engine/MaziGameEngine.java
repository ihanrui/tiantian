package com.mile.core.game.mazi.engine;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.DissroomEvent;
import com.mile.core.engine.game.eventmodel.DissroomInfo;
import com.mile.core.engine.game.eventmodel.GiveupEvent;
import com.mile.core.engine.game.eventmodel.GiveupInfo;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.game.*;
import com.mile.core.game.mazi.msgmodel.NewsEvent;
import com.mile.core.game.mazi.msgmodel.SeecardsEvent;
import com.mile.core.game.mazi.msgmodel.SeescoreEvent;
import com.mile.core.game.mazi.msgmodel.SettleCardEvent;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service(value = "maziGameEngine")
public class MaziGameEngine {
    private static final Logger logger = LoggerFactory.getLogger(GameEngine.class);

    /**
     * //牌型  1,单张，2，对子，3，三张，4，四张，5，连对，6，连三张，7，连四张
     * // 炸从小到大排序
     * // 11，副510，12，方正510K 13，梅花510K  14，红桃510K  15，黑桃510K
     * // 16，两个小王，17，一个大王一个小王 18，两个大王，19，五炸 20 ，三个大小王
     * // 21 六炸  22，四个大小王，23 七炸  24 八炸
     * //  牌从24开始， 一直到130
     *
     * @param roomid
     * @param userid
     * @param orgi
     * @param cards
     */
    public void reportnews(String roomid, String userid, String orgi, String newsflag) {

        try {
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(roomid, orgi);

            logger.info(" roomid [" + roomid + "] userid[" + userid + "] orgi[" + orgi + "]");

            Player player = board.player(userid);

            List<List<Byte>> listCardType = player.getNewsCards();

            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, orgi);

            List<Byte> lb = null;
            if (!"false".equals(newsflag)) {

                if (null != listCardType && !listCardType.isEmpty()) {
                    lb = listCardType.get(0);
                    logger.info("房间号  [" + board.getRoomid() + "] 恭喜你,报喜成功 ");
                    CardType c = ActionTaskUtils.identificationMazi(lb);
                    listCardType.remove(lb);
                    dealnews(board, userid, c.getMazicardtype());
                }

            }

            player.setNewsflag(!"false".equals(newsflag));
            player.setNewsCards(listCardType);
            if (null == listCardType || listCardType.isEmpty()) {
                player.setNewsflag(false);
                logger.info("房间号  [" + board.getRoomid() + "] 报喜完成，没有牌了， 强制设置为FALSE");
            }

            if (!"false".equals(newsflag)) {
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_REPORTNEWS, new NewsEvent(board.getPlayers(), player.getPlayuser(), lb, player.isNewsflag()), gameRoom);
            } else {
                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_REPORTNEWS, new NewsEvent(false), gameRoom);
            }

            CacheHelper.getBoardCacheBean().put(roomid, board, orgi);    //更新缓存数据
        } catch (Exception e) {
            logger.error("玩家报喜失败", e);
        }
    }

    private void dealnews(Board board, String userid, int mazicardtype) {
        Player[] lp = board.getPlayers();

        int newsscore = (mazicardtype - 22 > 0) ? (mazicardtype - 22) : 0;
        for (Player p : board.getPlayers()) {
            if (userid.equals(p.getPlayuser())) {
                p.setNewsscore(new Double(3 * Math.pow(2,newsscore)).intValue());
            } else {
                p.setNewsscore(new Double(-1 * Math.pow(2,newsscore)).intValue());
            }
            p.setTotalscore(p.getNewsscore() + p.getTotalscore());

        }

    }



    /**
     * 看分处理
     * @param token
     */
    public void seescore(String  token){
        try{
        Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

        String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

        GameRoom gameRoom = null;
        if (!StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
            gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, userToken.getOrgi());
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
            ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_SEESCORE, userToken.getUserid(), new SeescoreEvent(gameRoom.getRoomid(), board.getCardK105()));

        }
        } catch (Exception e) {
            logger.error("玩家看分失败", e);
        }
    }

    /**
     * 理牌处理
     * @param beiMiClient
     * @param token
     * @param settleFlag
     */
    public void settlecards(BeiMiClient beiMiClient,String token,String settleFlag){

        try {
            byte[] retcard = null;
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
                Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(roomid, gameRoom.getOrgi());

                Player player = board.player(userToken.getUserid());

                if ("true".equals(settleFlag)) {
                    player.setSettleflag(true);
                    retcard = MaziCardtypeUtil.settleCards(player.getCards(), null);
                } else {
                    Arrays.sort(player.getCards());
                    retcard = GameUtils.reverseCards(player.getCards());
                    player.setSettleflag(false);
                }

                ActionTaskUtils.sendEvent(playUser, new SettleCardEvent(retcard, DataConstants.GAME_EVENT_SETTLECARDS));
                CacheHelper.getBoardCacheBean().put(roomid, board, gameRoom.getOrgi());
            }
        } catch (Exception e) {
            logger.error("玩家理牌失败", e);
        }
    }

    /**
     * 明牌
     * @param beiMiClient
     */
    public void seecards(BeiMiClient beiMiClient){
        try {

            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {

                    synchronized (this) {
                        PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                        String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
                        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(roomid, gameRoom.getOrgi());

                        Player player = board.player(userToken.getUserid());

                        for (Player p : board.getPlayers()) {
                            if (!p.getPlayuser().equals(player.getPlayuser()) && player.getTeamflag() == p.getTeamflag() && p.isSeeflag()) {
                                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(),
                                        new NoticeEvent("对家已明牌", false));

                                return;
                            }

                        }

                        player.setSeeflag(true);

                        SeecardsEvent seecardsEvent = new SeecardsEvent(player.getPlayuser(), MaziCardtypeUtil.settleCards(player.getCards(), null));

                        MaziActionTaskUtils.sendSeecardEvent(player, board, seecardsEvent, gameRoom);

                        CacheHelper.getBoardCacheBean().put(roomid, board, gameRoom.getOrgi());
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("明牌失败",e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("玩家明牌异常", false));
        }
    }

    /**
     * 报喜处理
     * @param beiMiClient
     * @param newsflag
     */
    public void reportnews(BeiMiClient beiMiClient,String newsflag){
        try {

            String token = beiMiClient.getToken();
            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
                if (userToken != null) {
                    PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                    GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, beiMiClient.getOrgi());
                    Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                    Player player = board.player(userToken.getUserid());

                    BMDataContext.getMaziGameEngine().reportnews(roomid, playUser.getId(), playUser.getOrgi(), newsflag);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统报喜异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("系统报喜异常", false));
        }
    }



}
