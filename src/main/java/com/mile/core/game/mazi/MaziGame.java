package com.mile.core.game.mazi;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.engine.game.iface.ChessGame;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.msgmodel.MaziBoard;
import com.mile.core.game.mazi.util.MaziActionTaskUtils;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.NextPlayer;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MaziGame implements ChessGame {

    private static final Logger logger = LoggerFactory.getLogger(MaziGame.class);

    /**
     * 开始码子游戏
     *
     * @return
     */
    public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker, int cardsnum) {
        try {
            gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
            Board board = new MaziBoard();
            board.setCards(null);
            board.setCurrentnum(gameRoom.getCurrentnum());

            //设置第一个出牌人信息， banker是在createbegintask中随机抽取或者是上一局的1游
            board.setNextplayer(new NextPlayer(banker, false));

            List<Byte> temp = new ArrayList<Byte>();
            for (int i = 0; i < 108; i++) { //码子为两付牌， 108张
                temp.add((byte) i);
            }

            int random = playUsers.size() * gameRoom.getCardsnum();

            board.setPosition((byte) new Random().nextInt(random));    //随机庄家用

            logger.info("process begin 房间中 extparam参数  " + gameRoom.getExtparams());

            Player[] players = new Player[playUsers.size()];
            byte[] cards = new byte[108];

            if (RoomUtil.isShuffle()) {
                for (int i = 0; i < playway.getShuffletimes() + 1; i++) {
                    Collections.shuffle(temp);
                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, j, new Random().nextInt(108));
                        ;
                    }

                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, 107 - j, new Random().nextInt(108));
                        ;
                    }

                    for (int j = 0; j < temp.size(); j++) {//增加一次洗牌
                        Collections.swap(temp, new Random().nextInt(108), new Random().nextInt(108));
                        ;
                    }
                }

            }

            for (int i = 0; i < temp.size(); i++) {
                cards[i] = temp.get(i);
            }
            board.setCards(cards);

            int inx = 0;
            logger.info("playUsers size [" + playUsers.size() + "]");
            for (int i = 0; i < playUsers.size(); i++) {

                PlayUserClient client = playUsers.get(i);

                int index = gameRoom.getExtparams().get(client.getId() + "_index") == null ? client.getIndex()
                        : Integer.parseInt(gameRoom.getExtparams().get(client.getId() + "_index"));

                logger.info("玩家client " + client.getUsername() + "][" + client.getGameid() + "] 位置为 index[" + index + "]");

                if (index < 0 || index > 3) {
                    index = RoomUtil.getIndex(playUsers, client);
                    client.setIndex(index);
                    CacheHelper.getGamePlayerCacheBean().put(client.getId(), client, client.getOrgi());
                }

                Player player = new Player(client.getId(), index);
                UKTools.copyProperties(client, player);
                player.setCards(new byte[cardsnum]);
                player.setGameroomid(gameRoom.getId());
                player.setRoomid(gameRoom.getRoomid());
                player.setTablenum(gameRoom.getTablenum());
                player.setTeanum(gameRoom.getTeanum());
                player.setUserid(client.getId());

                SummaryPlayer summaryPlayer = getSummaryPlayerByUserid(gameRoom, player.getPlayuser());

                if (null != summaryPlayer) {
                    player.setTotalscore(summaryPlayer.getTotalscore());
                }
                logger.info("玩家 " + player.getUsername() + "][" + player.getGameid() + "] 位置为 index[" + index + "]");
                players[index] = player;//数组根据座位号来  数组下标从0开始，位置号从1开始。

            }

            //处理发牌摸对
            MaziActionTaskUtils.dealBeginCards(gameRoom, banker, players, temp, board, playUsers);

            board.setPlayers(players);
            board.setBanker(banker);

            logger.info("process finish 房间中 extparam参数  " + gameRoom.getExtparams());

            return board;
        }catch (Exception e){
            logger.error("开始码子游戏 失败",e);
            return null;
        }
    }

    private SummaryPlayer getSummaryPlayerByUserid(GameRoom gameRoom, String userid) {

        if (null != gameRoom.getSummary() && null != gameRoom.getSummary().getSummaryPlayer()) {
            List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();

            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                if (summaryPlayer.getUserid().equals(userid)) {
                    return summaryPlayer;
                }

            }
            return null;
        } else
            return null;

    }


    private boolean isShuffle(){
        String suffleName  = TeahouseUtil.getConstantsByName(BMDataContext.BEIMI_SYSTEM_GAME_SHUFFLEFLAG);

        if(null == suffleName || "".equals(suffleName))
            return true;

        return !"noshuffle".equals(suffleName);
    }

}
