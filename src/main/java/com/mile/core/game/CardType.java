package com.mile.core.game;

import com.mile.common.entity.BaseEntity;

public class CardType extends BaseEntity{
	/**
	 *
	 */
	private  static final long serialVersionUID = 1L;
	protected int maxcard ;	//最大的牌

	protected int cardtype ;	//牌型

	protected int typesize ;	//不同牌数量
	protected boolean king ;	//王炸
	protected boolean bomb;	//炸弹
	protected int mincard ; 	//最小的牌
	protected int cardnum ;	//最大牌张数  ， JJJQ，= 3
	protected byte maxcardvalue ;


	protected int score;//分数






	protected int mazicardtype;//码子牌型

	public CardType(){}

	public CardType(int maxcard, int cardtype) {
		this.maxcard = maxcard;
		this.cardtype = cardtype;
	}

	public CardType(int maxcard , int cardtype , boolean king , boolean bomb){
		this.maxcard = maxcard ;
		this.cardtype = cardtype ;
		this.king = king ;
		this.bomb = bomb ;
	}

	public int getMaxcard() {
		return maxcard;
	}
	public void setMaxcard(int maxcard) {
		this.maxcard = maxcard;
	}
	public int getCardtype() {
		return cardtype;
	}
	public void setCardtype(int cardtype) {
		this.cardtype = cardtype;
	}
	public boolean isKing() {
		return king;
	}
	public void setKing(boolean king) {
		this.king = king;
	}
	public boolean isBomb() {
		return bomb;
	}
	public void setBomb(boolean bomb) {
		this.bomb = bomb;
	}
	public int getTypesize() {
		return typesize;
	}
	public void setTypesize(int typesize) {
		this.typesize = typesize;
	}
	public int getMincard() {
		return mincard;
	}
	public void setMincard(int mincard) {
		this.mincard = mincard;
	}
	public int getCardnum() {
		return cardnum;
	}
	public void setCardnum(int cardnum) {
		this.cardnum = cardnum;
	}
	public byte getMaxcardvalue() {
		return maxcardvalue;
	}
	public void setMaxcardvalue(byte maxcardvalue) {
		this.maxcardvalue = maxcardvalue;
	}

	public int getMazicardtype() {
		return mazicardtype;
	}

	public void setMazicardtype(int mazicardtype) {
		this.mazicardtype = mazicardtype;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
