package com.mile.core.game.majiang.task;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.mile.core.game.BeiMiGameTask;
import com.mile.core.engine.game.task.AbstractTask;
import com.mile.common.cache.CacheHelper;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.entity.GameRoom;

/**
 * 出牌计时器，默认25秒，超时执行
 * @author zhangtianyi
 *
 */
public class CreateDealMJCardTask extends AbstractTask implements ValueWithExpiryTime  , BeiMiGameTask{

	private long timer  ;
	private GameRoom gameRoom = null ;
	private String orgi ;

	public CreateDealMJCardTask(long timer , GameRoom gameRoom, String orgi){
		super();
		this.timer = timer ;
		this.gameRoom = gameRoom ;
		this.orgi = orgi ;
	}
	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis()+timer*1000;	//5秒后执行
	}
	/**
	 * 杠碰吃胡 时间到 了，执行发牌动作
	 */
	public void execute(){
		Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
		if(board!=null){
			board.dealRequest(gameRoom, board, orgi , false,  null);
		}
	}
}
