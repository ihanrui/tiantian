package com.mile.core.game.majiang;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.engine.game.action.AllCardsAction;
import com.mile.core.engine.game.action.EnoughAction;
import com.mile.core.engine.game.action.EnterAction;
import com.mile.core.engine.game.action.JoinAction;
import com.mile.core.game.dizhu.action.PlayMJCardsAction;
import com.mile.core.game.majiang.action.DealMJCardAction;
import com.mile.core.game.majiang.action.MJRaiseHandsAction;
import com.mile.core.game.majiang.action.SelectAction;
import com.mile.core.statemachine.BeiMiStateMachine;
import com.mile.core.statemachine.config.StateConfigurer;
import com.mile.core.statemachine.config.StateMachineTransitionConfigurer;

@Configuration
public class BeiMiMaJiangStateMachineConfig<T, S>  {

	@Bean("majiang")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}

    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(MileGameEnum.NONE.toString())
                    .state(MileGameEnum.CRERATED.toString())
                    .state(MileGameEnum.WAITTING.toString())
                    .state(MileGameEnum.READY.toString())
                    .state(MileGameEnum.BEGIN.toString())
                    .state(MileGameEnum.PLAY.toString())
                    .state(MileGameEnum.END.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
		/**
		 * 状态切换：BEGIN->WAITTING->READY->PLAY->END
		 */
        transitions
	        .withExternal()
		    	.source(MileGameEnum.NONE.toString()).target(MileGameEnum.CRERATED.toString())
		    	.event(MileGameEvent.ENTER.toString()).action(new EnterAction<String,String>())
		    	.and()
		    .withExternal()
	        	.source(MileGameEnum.CRERATED.toString()).target(MileGameEnum.WAITTING.toString())
	        	.event(MileGameEvent.JOIN.toString()).action(new JoinAction<String,String>())
	        	.and()
            .withExternal()
                .source(MileGameEnum.WAITTING.toString()).target(MileGameEnum.READY.toString())
                .event(MileGameEvent.ENOUGH.toString()).action(new EnoughAction<String, String>())
                .and()
            .withExternal()
                .source(MileGameEnum.READY.toString()).target(MileGameEnum.BEGIN.toString())
                .event(MileGameEvent.AUTO.toString()).action(new SelectAction<String,String>())	//抢地主
                .and()
            .withExternal()
                .source(MileGameEnum.BEGIN.toString()).target(MileGameEnum.LASTHANDS.toString())
                .event(MileGameEvent.RAISEHANDS.toString()).action(new MJRaiseHandsAction<String,String>())
                .and()
            .withExternal()
                .source(MileGameEnum.LASTHANDS.toString()).target(MileGameEnum.LASTHANDS.toString())
                .event(MileGameEvent.DEAL.toString()).action(new DealMJCardAction<String,String>())
                .and()
             .withExternal()
                .source(MileGameEnum.LASTHANDS.toString()).target(MileGameEnum.PLAY.toString())
                .event(MileGameEvent.PLAYCARDS.toString()).action(new PlayMJCardsAction<String,String>())
                .and()
            .withExternal()
                .source(MileGameEnum.PLAY.toString()).target(MileGameEnum.END.toString())
                .event(MileGameEvent.ALLCARDS.toString()).action(new AllCardsAction<String,String>())
                .and()
            ;
    }
}
