package com.mile.core.game.majiang.action;

import org.apache.commons.lang3.StringUtils;

import com.mile.core.BMDataContext;
import com.mile.core.game.majiang.task.CreateDealMJCardTask;
import com.mile.core.statemachine.action.Action;
import com.mile.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.mile.core.statemachine.message.Message;
import com.mile.common.cache.CacheHelper;
import com.mile.core.entity.GameRoom;

/**
 * 抓牌
 * @author iceworld
 *
 * @param <T>
 * @param <S>
 */
public class DealMJCardAction<T,S> implements Action<T, S>{

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T,S> configurer) {
		String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
		if(!StringUtils.isBlank(room)){
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ;
			if(gameRoom!=null){
				CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateDealMJCardTask(5 , gameRoom , gameRoom.getOrgi()));
			}
		}
	}
}
