package com.mile.core.game;

public enum MileGameEnum {
	/**
	 * 游戏的基本状态，开局->等待玩家（AI）->凑齐一桌子->打牌->结束
	 */
	NONE,		//无状态
	CRERATED,	//进入房间
	BEGIN ,	//开局
	WAITTING,	//等待玩家
	READY,		//凑齐一桌子
	LASTHANDS,	//翻底牌
	PLAY,		//打牌
	STEPEND,    //一盘结束
	DISSROOM,    //解散
	GIVEUP,    //投降
	END,
	//牛牛状态
	//准备 -> 发牌 -> 抢庄 -> 定庄 ->闲家下注 -> 发牌 -> 看牌-> 亮牌 ->比牌->结算
	//NONE->创建房间CRERATED->等待(WAITTING)->准备(READY)->发牌4(DEAL4)->定庄(BIND)
	// ->发牌1(DEAL1)->结算(STEPCLEAN)->准备(READY)......结算(CLEAN)->结束(END)
	BIND,//定庄
	DEAL1,//发牌;
	DEAL4, //发牌;
	STEPCLEARING,//小结
	CLEARING;//结算

	public String toString(){
		return super.toString().toLowerCase() ;
	}
}
