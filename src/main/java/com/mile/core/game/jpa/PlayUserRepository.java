package com.mile.core.game.jpa;

import com.mile.core.entity.PlayUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public abstract interface PlayUserRepository extends JpaRepository<PlayUser, String>
{
	public abstract PlayUser findById(String paramString);

	public abstract PlayUser findByUsername(String username);

	public abstract PlayUser findByGameid(String gameid);

	public abstract PlayUser findByEmail(String email);


	public abstract PlayUser findByUsernameAndPassword(String username, String password);

	public abstract Page<PlayUser> findByDatastatus(boolean datastatus , String orgi, Pageable paramPageable);

	public abstract Page<PlayUser> findByDatastatusAndUsername(boolean datastatus , String orgi ,String username ,Pageable paramPageable);



	public abstract PlayUser findByOpenid(String openid);

	public abstract int countByOpenid(String Openid);


	public abstract Page<PlayUser> findByGameidLike(String gameid,Pageable paramPageable);


	public abstract int countByUsername(String username);


	public abstract PlayUser findByMobileAndPassword(String username, String password);

	public abstract Page<PlayUser> findByOrgi(String orgi, Pageable page);

	public abstract Page<PlayUser> findByOrgiAndOnline(String orgi ,boolean online, Pageable page);


	@Query(value="select t.* from bm_playuser t where t.gamid in (select t1.gameid from wd_a_useragentinfo t1 " +
			"where t1.agent1 = :agentid or t1.agent2 = :agentid or t1.agent3 = :agentid  or t1.agent4 = :agentid)"
	, nativeQuery = true)
	public  List<PlayUser> queryByAgentid(@Param("agentid") String agentid);



	@Query(value="select count(t.*) from bm_playuser t where t.gamid in (select t1.gameid from wd_a_useragentinfo t1 " +
			" where t1.lastmodify like %?1%  )"
			, nativeQuery = true)
	public int queryByLastmodify(@Param("lastmodify") String lastmodify);




	@Query(value=" select * from bm_playuser t where t.gameid in ( " +
			"    select t1.gameid from wd_a_useragentmap t1 where t1.agent1 = :gameid or t1.agent2 = :gameid or " +
			" t1.agent3 = :gameid or t1.agent4 = :gameid  " +
			" ) " +
			" and t.gameid not in (select t2.gameid from wd_a_gameagent t2)"
			, nativeQuery = true)
	public List<PlayUser> getMyuser(@Param("gameid") String gameid);






	/*@Query(value=" select * from bm_playuser t where t.gameid in ( " +
			"    select t1.gameid from wd_a_useragentmap t1 where t1.agent1 = :gameid or t1.agent2 = :gameid or " +
			" t1.agent3 = :gameid or t1.agent4 = :gameid  " +
			" ) " +
			" and t.gameid not in (select t2.gameid from wd_a_gameagent t2)"
			, nativeQuery = true)
	public Page<PlayUser> getMyuser(@Param("gameid") String gameid,PageRequest pageRequest);  */

	@Query(value=" select * from bm_playuser t where t.gameid in ( " +
			"    select t1.gameid from wd_a_useragentmap t1 where t1.agent1 = :gameid or t1.agent2 = :gameid or " +
			" t1.agent3 = :gameid or t1.agent4 = :gameid  " +
			" ) " +
			" and t.gameid not in (select t2.gameid from wd_a_gameagent t2)  and t.gameid = :selfid"
			, nativeQuery = true)
	public List<PlayUser> getMyuser(@Param("gameid") String gameid,@Param("selfid") String selfid);


	/*@Query(value=" select * from bm_playuser t where t.gameid in ( " +
			"    select t1.gameid from wd_a_useragentmap t1 where t1.agent1 = :gameid or t1.agent2 = :gameid or " +
			" t1.agent3 = :gameid or t1.agent4 = :gameid  " +
			" ) " +
			" and t.gameid not in (select t2.gameid from wd_a_gameagent t2)  and t.gameid = :selfid"
			, nativeQuery = true)
	public Page<PlayUser> getMyuser(@Param("gameid") String gameid,@Param("selfid") String selfid,PageRequest pageRequest);    */


	@Query(value=" select * from bm_playuser t where t.gameid in ( " +
			"    select t1.gameid from wd_a_useragentmap t1 where t1.agent1 = :gameid or t1.agent2 = :gameid or " +
			" t1.agent3 = :gameid or t1.agent4 = :gameid  " +
			" ) " +
			" and t.gameid  in (select t2.gameid from wd_a_gameagent t2)"
			, nativeQuery = true)
	public List<PlayUser> getMyagent(@Param("gameid") String gameid);


}
