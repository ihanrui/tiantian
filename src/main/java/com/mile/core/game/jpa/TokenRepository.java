package com.mile.core.game.jpa;

import com.mile.core.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface TokenRepository extends JpaRepository<Token, String>
{
	public abstract Token findById(String id);

	public abstract List<Token> findByUserid(String userid);

	public abstract void deleteByUserid(String userid);
}
