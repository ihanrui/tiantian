package com.mile.core.game.jpa;

import com.mile.core.entity.GameModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface GameModelRepository  extends JpaRepository<GameModel, String>{

  public abstract GameModel findByIdAndOrgi(String id, String orgi);

  public abstract List<GameModel> findByOrgiAndGame(String orgi , String game);
}
