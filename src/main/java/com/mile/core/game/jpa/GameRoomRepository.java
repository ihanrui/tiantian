package com.mile.core.game.jpa;

import com.mile.core.entity.GameRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface GameRoomRepository  extends JpaRepository<GameRoom, String>{

  public abstract GameRoom findById(String id);


  public abstract GameRoom findByIdAndOrgi(String id, String orgi);

  public abstract Page<GameRoom> findByOrgi(String orgi , Pageable page);

  public abstract List<GameRoom> findByRoomidAndOrgi(String roomid, String orgi);

  public abstract List<GameRoom> findByRoomid(String roomid);

  public abstract GameRoom findByTeanumAndTablenum(String teanum,int tablenum);

  public abstract  Page<GameRoom> findByRoomid(String roomid,Pageable page);
}
