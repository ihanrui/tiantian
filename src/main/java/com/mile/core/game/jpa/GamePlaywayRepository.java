package com.mile.core.game.jpa;

import com.mile.core.entity.GamePlayway;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface GamePlaywayRepository  extends JpaRepository<GamePlayway, String>{

  public abstract GamePlayway findByIdAndOrgi(String id, String orgi);

  public abstract GamePlayway findById(String id);

  public abstract List<GamePlayway> findByOrgi(String orgi, Sort sort);

  public abstract List<GamePlayway> findByOrgiAndTypeid(String orgi , String typeid , Sort sort);
}
