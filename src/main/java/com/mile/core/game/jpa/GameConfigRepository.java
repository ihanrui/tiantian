package com.mile.core.game.jpa;

import com.mile.web.model.GameConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface GameConfigRepository extends JpaRepository<GameConfig, String>
{
  public abstract List<GameConfig> findByOrgi(String orgi);
}
