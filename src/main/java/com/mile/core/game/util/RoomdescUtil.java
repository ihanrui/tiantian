package com.mile.core.game.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.JoinRoom;
import com.mile.core.game.pinshi.msgmodel.PinshiJoinRoom;
import com.mile.core.game.pinshi.msgmodel.PinshiUserBoard;
import com.mile.core.game.pinshi.util.PinshiRoomDescUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by michael on 2018/6/11.
 */
public class RoomdescUtil {

    private static final Logger logger = LoggerFactory.getLogger(RoomdescUtil.class);



    public static JoinRoom  formatRoomdesc(GameRoom gameRoom, JoinRoom joinRoom){
        if(DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())){
            return MaziTipsUtil.formatRoomdesc(gameRoom, joinRoom);

        }else if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
            return PinshiRoomDescUtil.formatRoomdesc(gameRoom, joinRoom);

        }

        return null;

    }



    public static void  formatRoomdesc(GameRoom gameRoom, UserBoard userboard){
        if(DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())){
             MaziTipsUtil.formatRoomdesc(gameRoom, userboard);

        }else if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
             PinshiRoomDescUtil.formatRoomdesc(gameRoom, (PinshiUserBoard) userboard);

        }


    }




}
