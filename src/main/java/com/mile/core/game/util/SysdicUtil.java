package com.mile.core.game.util;

import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.web.model.BeiMiDic;
import com.mile.web.model.SysDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by michael on 2018/6/12.
 */
public class SysdicUtil {

    private static final Logger logger = LoggerFactory.getLogger(SysdicUtil.class);



    public static Integer getIntTimesByName(String name) {
        if (null == getTimesByName(name))
            return 0;

        return Integer.parseInt(getTimesByName(name));
    }

    public static String getTimesByName(String name) {
        try {
            List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(PinshiDataConstants.COM_PINSHI_TIMES);
            if (null == sysDicList)
                return null;

            for (SysDic sysDic : sysDicList) {
                if (name.equals(sysDic.getName()))
                    return sysDic.getCode();
            }

            logger.info("未找到拼十时间间隔游戏常量[" + name + "]");
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取拼十时间间隔游戏常量[" + name + "]失败", e);
            return null;
        }
    }
}
