package com.mile.core.game.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.CardType;
import com.mile.core.game.mazi.msgmodel.MaziTakeCards;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.JoinRoom;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.msgmodel.TakeCards;
import com.mile.core.teahouse.util.TeahouseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.*;

/**
 * Created by michael on 6/3/18.
 */
public class RoomUtil {

    private static final Logger logger = LoggerFactory.getLogger(RoomUtil.class);


    public static String getCreatevalue(GameRoom gameRoom,String key){

        if(StringUtils.isEmpty(key)){
            logger.info("getCreatevalue key 为空 ");
            return null;
        }

        if(DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())){
            return getMaziCreatevalue(gameRoom.getExtparams(),key);
        }

        if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())){
            return getPinshiCreatevalue(gameRoom.getExtparams(),key);
        }
        return null;
    }



    public static int getCreateIntvalue(GameRoom gameRoom,String key){

        return Integer.parseInt(getCreatevalue(gameRoom,key));
    }


    public static String getMaziCreatevalue(Map<String,String> param,String key){

        if(key.indexOf(":") > -1 ){
            String[] keys = key.split(":");
            String v1 = param.get(keys[0]);
            if(StringUtils.isEmpty(v1) || v1.indexOf(keys[1])<0){
                return "";
            }
            return ""+v1.indexOf(keys[1]);
        }else{
            return param.get(key);
        }

    }



    public static String getPinshiCreatevalue(Map<String,String> param,String key){

        if(key.indexOf(":") > -1 ){
            String[] keys = key.split(":");
            String v1 = param.get(keys[0]);
            if(StringUtils.isEmpty(v1) || v1.indexOf(keys[1])<0){
                return "";
            }
            return ""+v1.indexOf(keys[1]);
        }else{
            return param.get(key);
        }

    }


    public static boolean isShuffle(){
        String suffleName  = TeahouseUtil.getConstantsByName(BMDataContext.BEIMI_SYSTEM_GAME_SHUFFLEFLAG);

        if(null == suffleName || "".equals(suffleName))
            return true;

        return !"noshuffle".equals(suffleName);
    }



    public static String getJoinStatus(GameRoom gameRoom){
        if(DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getPlaywaycode())) {
            return BMDataContext.GameStatusEnum.WATCH.toString();
        }else if(DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getPlaywaycode())){
            return BMDataContext.GameStatusEnum.WAITTING.toString();
        }
        return BMDataContext.GameStatusEnum.WAITTING.toString();
    }



    public static int getIndex(GameRoom gameRoom,PlayUserClient playUserClient){
        if(null == gameRoom || null == playUserClient){
            logger.info("获取座位号信息为空");
            return  -1;
        }
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        int index = -1;

        if (null != board && null != board.player(playUserClient.getId())) {
            index =  board.player(playUserClient.getId()).getIndex();

        } else {
            index = playUserClient.getIndex();
        }

        logger.info("getIndex 获取的座位号为 index["+index+"]");
        return index;
    }


    /**
     * 得到座位号
     *
     * @param playUserClients
     * @return
     */
    public static int getIndex(List<PlayUserClient> playUserClients, PlayUserClient playUser) {
        int index = 0;

        if (null != playUserClients && !playUserClients.isEmpty()) {
            index = playUserClients.size();

            int i = 0;
            for (; i < playUserClients.size(); i++) {
                int j = 0;
                for (; j < playUserClients.size(); j++) {
                    if (i == playUserClients.get(j).getIndex())
                        break;
                }
                if (j == playUserClients.size()) {
                    index = i;
                    break;
                }

            }

        }
        logger.info("本次------" + playUser.getUsername() + "-----获取的 座位号为【" + index + "】");
        return index;

    }


}