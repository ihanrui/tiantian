package com.mile.core.record.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.record.entity.Expenddetail;
import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.jpa.ExpenddetailRepository;
import com.mile.core.record.jpa.SummaryPlayerRepository;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.entity.Teamem;
import com.mile.core.teahouse.entity.Teareport;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.core.teahouse.jpa.TeamemRepository;
import com.mile.core.teahouse.jpa.TeareportRepository;
import com.mile.web.model.BeiMiDic;
import com.mile.web.model.SysDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by michael on 3/18/18.
 */
public class GamerecordUtil {

    private static final Logger logger = LoggerFactory.getLogger(GamerecordUtil.class);

    public static void dealcoinrecord(GameRoom gameroom) {

        logger.info("记录金币和钻石 roomid[" + gameroom.getRoomid()
                + "] gameid[" + gameroom.getRoomid() + "] roomtype[" + gameroom.getRoomtype()
                + "] gameroom.getCoinexpend()[" + gameroom.getCoinexpend() + "]");

        if (DataConstants.Roomtype.CARD.toString().equals(gameroom.getRoomtype())) {
            dealcard(gameroom);
        } else if (DataConstants.Roomtype.COIN.toString().equals(gameroom.getRoomtype())) {
            dealcoin(gameroom);

        } else if (DataConstants.Roomtype.TEACOIN.toString().equals(gameroom.getRoomtype())) {
            dealcard(gameroom);
            dealtea(gameroom);
            dealticket(gameroom);
        }
    }

    /**
     * 房卡处理
     *
     * @param gameRoom
     */
    private static void dealcard(GameRoom gameRoom) {

        logger.info("gameroom [" + gameRoom.getRoomid() + "] 扣卡处理开始 ");

        if (!"Y".equals(gameRoom.getDeductflag())) {
            logger.info("gameroom [" + gameRoom.getRoomid() + "] 首局未完成，无需扣卡");
            return;
        }

        int mumofgames = gameRoom.getNumofgames();//盘数
        GamePlayway gamePlayway = gameRoom.getGamePlayway();

        String sysdickey = gamePlayway.getCode() + "_" + gameRoom.getCoinexpend() + "_" + mumofgames;
        List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(DataConstants.SYSTEM_GAME_COINSDECUTE);


        if(!StringUtils.isEmpty(MaziSummaryUtil.getObjWithName(sysDicList, sysdickey))) {
            int num = Integer.parseInt(MaziSummaryUtil.getObjWithName(sysDicList, sysdickey));

            if (DataConstants.Coinexpend.CREATER.toString().equals(gameRoom.getCoinexpend())) {//扣房主的
                recordCard(gameRoom, num, gameRoom.getCreater());
            } else if (DataConstants.Coinexpend.AA.toString().equals(gameRoom.getCoinexpend())) {
                List<PlayUserClient> playUserClients = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
                if (null != playUserClients && !playUserClients.isEmpty())
                    for (PlayUserClient playUserClient : playUserClients) {
                        recordCard(gameRoom, num, playUserClient.getGameid());
                    }

            }
        }else {
            logger.info( "gameroom [" + gameRoom.getRoomid() + "] 扣卡处理未扣卡");
        }

    }

    /**
     * 金币处理
     *
     * @param gameRoom
     */
    private static void dealcoin(GameRoom gameRoom) {
        List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();
        if (null != summaryPlayers && !summaryPlayers.isEmpty()) {

            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                logger.info("金币结算信息  summaryPlayer["+summaryPlayer+"]");
                if(null == summaryPlayer.getTotalscore()) {
                    summaryPlayer.setTotalscore(0);
                }

                if(null == gameRoom.getScoreunit()) {
                    gameRoom.setScoreunit(0);
                }

                recordCoin(summaryPlayer.getGameid(), gameRoom.getRoomtype(),
                        summaryPlayer.getTotalscore() * gameRoom.getScoreunit(),
                        gameRoom.getRoomid(),"牌局金币结算",DataConstants.Roomtype.COIN.toString());
            }
        }
    }

    /**
     * 茶楼分处理
     *
     * @param gameRoom
     */
    private static void dealtea(GameRoom gameRoom) {
        List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();
        if (null != summaryPlayers && !summaryPlayers.isEmpty()) {
            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                recordTeaCoin(summaryPlayer.getGameid(), gameRoom.getRoomtype(), summaryPlayer.getTotalscore() * gameRoom.getScoreunit(),
                        gameRoom.getTeanum(), gameRoom.getRoomid(), "N",DataConstants.Roomtype.TEACOIN.toString());
            }
        }
    }

    /**
     * 茶楼分处理
     *
     * @param gameRoom
     */
    private static void dealticket(GameRoom gameRoom) {
        Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());

        if (DataConstants.Tickettype.PERSENT1.toString().equals(teahouse.getTicketflag())) {
            logger.info("茶楼抽水方式为比例 【" + teahouse.getTeanum() + "】[" + teahouse.getTicketvalue() + "]");
        } else if (DataConstants.Tickettype.FIX1.toString().equals(teahouse.getTicketflag())) {
            logger.info("茶楼抽水方式为固定值 【" + teahouse.getTeanum() + "】[" + teahouse.getTicketvalue() + "]");
        }

        List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();

        Summary summary =  gameRoom.getSummary();
        if (null != summaryPlayers && !summaryPlayers.isEmpty()) {

            int winplayernums =  getWinplayernums(summaryPlayers);

            for (SummaryPlayer summaryPlayer : summaryPlayers) {

                int num = 0;
                if (DataConstants.Tickettype.PERSENT1.toString().equals(teahouse.getTicketflag())
                        && teahouse.getTicketvalue() <= 20 //门票比例不能高于20%
                        && summaryPlayer.getCoins()>0) {//抽赢家的
                    num = new Double(summaryPlayer.getCoins() * teahouse.getTicketvalue()/100).intValue();//百分比为整数，除以100
                }

                if (DataConstants.Tickettype.FIX1.toString().equals(teahouse.getTicketflag()) && summaryPlayer.getWinflag() == 1
                        && summaryPlayer.getCoins() > 0) {
                    num = new Double(teahouse.getTicketvalue()/winplayernums).intValue();
                }
                if(num > 0) { //只有是赢家 才抽成
                    recordTeaCoin(summaryPlayer.getGameid(), gameRoom.getRoomtype(), -1 * num, gameRoom.getTeanum(), gameRoom.getRoomid(), "Y",
                            DataConstants.Roomtype.TICKET.toString());
                    summaryPlayer.setTicket(num);
                    summaryPlayer.setCoins(summaryPlayer.getCoins()-num);
                    if(null == summary.getTicket()){
                        summary.setTicket(0);
                    }
                    summary.setTicket(summary.getTicket()+num);
                }
            }
        }
    }


    private static int getWinplayernums(List<SummaryPlayer> summaryPlayers){
        if(null == summaryPlayers || summaryPlayers.isEmpty()){
            return 0;
        }

        int winplayernums = 0;

        for(SummaryPlayer summaryPlayer : summaryPlayers){
            winplayernums = summaryPlayer.getWinflag() ==1?winplayernums:winplayernums+1;
        }

        return winplayernums;

    }

    /**
     * 记录用户钻石消耗
     *
     * @param gameid
     * @param roomtype
     * @param num
     * @param roomid
     */
    private static void recordCard(GameRoom gameRoom, int num, String gameid) {
        PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);

        if (!StringUtils.isEmpty(gameRoom.getTeanum())) {
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());
            Expenddetail expenddetail = new Expenddetail();
            expenddetail.setExpend(num);


            if(null == teahouse.getFund() ){
                teahouse.setFund(0);
            }

            expenddetail.setExpendbegin(teahouse.getFund());
            teahouse.setFund(teahouse.getFund() - num);
            expenddetail.setExpendend(teahouse.getFund());
            expenddetail.setRoomtype(gameRoom.getRoomtype());
            expenddetail.setExpendtype(DataConstants.Roomtype.CARD.toString());
            expenddetail.setUsername(teahouse.getName());
            expenddetail.setUserid(teahouse.getUserid());
            expenddetail.setUserurl(teahouse.getUserurl());
            expenddetail.setRoomid(gameRoom.getRoomid());
            expenddetail.setTicketflag("N");
            expenddetail.setGameid(teahouse.getTeanum());
            expenddetail.setMemo("房卡消耗");
            BMDataContext.getContext().getBean(TeahouseRepository.class).save(teahouse);
            BMDataContext.getContext().getBean(ExpenddetailRepository.class).save(expenddetail);
        } else {
            Expenddetail expenddetail = new Expenddetail();
            expenddetail.setExpend(num);
            PlayUser p = playUserRepository.findByGameid(gameid);
            expenddetail.setExpendbegin(p.getDiamonds());
            p.setDiamonds(p.getDiamonds() - num);
            expenddetail.setExpendend(p.getDiamonds());
            expenddetail.setRoomtype(gameRoom.getRoomtype());
            expenddetail.setExpendtype(DataConstants.Roomtype.CARD.toString());
            expenddetail.setUsername(p.getUsername());
            expenddetail.setUserid(p.getId());
            expenddetail.setGameid(p.getGameid());
            expenddetail.setUserurl(p.getUserurl());
            expenddetail.setRoomid(gameRoom.getRoomid());
            expenddetail.setTicketflag("N");
            playUserRepository.save(p);
            expenddetail.setMemo("房卡消耗");
            BMDataContext.getContext().getBean(ExpenddetailRepository.class).save(expenddetail);

            MaziScoreUtil.synchronizationUsercache(p);

        }

    }

    /**
     * 记录用户金币消耗
     *
     * @param gameid
     * @param roomtype
     * @param num
     * @param roomid
     */
    public static PlayUser  recordCoin(String gameid, String roomtype, int num, String roomid,String memo,String expendtype) {
        logger.info("记录用户金币消耗  gameid["+gameid+"]roomtype["+roomtype+"]num["+num+"]roomid["+roomid+"]");
        PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);

        Expenddetail expenddetail = new Expenddetail();
        expenddetail.setExpend(num);
        PlayUser p = playUserRepository.findByGameid(gameid);
        expenddetail.setExpendbegin(p.getGoldcoins());
        p.setGoldcoins(p.getGoldcoins() + num);
        expenddetail.setExpendend(p.getGoldcoins());
        expenddetail.setRoomtype(roomtype);
        expenddetail.setExpendtype(expendtype);
        expenddetail.setUsername(p.getUsername());
        expenddetail.setUserid(p.getId());
        expenddetail.setUserurl(p.getUserurl());
        expenddetail.setRoomid(roomid);

        expenddetail.setGameid(p.getGameid());
        expenddetail.setTicketflag("N");
        playUserRepository.save(p);
        expenddetail.setMemo(memo);
        BMDataContext.getContext().getBean(ExpenddetailRepository.class).save(expenddetail);

        MaziScoreUtil.synchronizationUsercache(p);
        return p;

    }

    /**
     * 记录用户茶楼金币消耗
     *
     * @param gameid
     * @param roomtype
     * @param num
     * @param roomid
     */
    private static void recordTeaCoin(String gameid, String roomtype, int num, String teanum, String roomid, String ticketflag,String expendtype) {
        TeamemRepository teamemRepository = BMDataContext.getContext().getBean(TeamemRepository.class);

        Expenddetail expenddetail = new Expenddetail();
        expenddetail.setExpend(num);
        Teamem teamem = teamemRepository.findByGameidAndTeanum(gameid, teanum);
        expenddetail.setExpendbegin(teamem.getScore());
        teamem.setScore(teamem.getScore() + num);
        expenddetail.setExpendend(teamem.getScore());
        expenddetail.setRoomtype(roomtype);
        expenddetail.setExpendtype(expendtype);
        expenddetail.setUsername(teamem.getUsername());
        expenddetail.setUserid(teamem.getId());
        expenddetail.setUserurl(teamem.getUserurl());
        expenddetail.setRoomid(roomid);
        expenddetail.setTicketflag(ticketflag);
        expenddetail.setGameid(teamem.getGameid());
        teamemRepository.save(teamem);
        expenddetail.setMemo("茶楼金币消耗");
        BMDataContext.getContext().getBean(ExpenddetailRepository.class).save(expenddetail);

    }








    public static void dealTeareport(GameRoom gameRoom) {
        if(StringUtils.isEmpty(gameRoom.getTeanum())) {
            logger.info("非茶楼不统计");
            return;
        }

        String rq = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis()));

        TeareportRepository teareportRepository = BMDataContext.getContext().getBean(TeareportRepository.class);
        Teareport teareport = teareportRepository.findByTeanumAndReportdate(gameRoom.getTeanum(), rq);

        if (null == teareport) {
            teareport = new Teareport();
            teareport.setReportdate(rq);
            teareport.setTeanum(gameRoom.getTeanum());
            teareport.setNum(0);
            teareport.setTeahouseid(BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum()).getId());
            teareport.setTicket(0);
        }
        teareport.setNum(teareport.getNum() + 1);



        for (SummaryPlayer summaryPlayer : gameRoom.getSummary().getSummaryPlayer()) {
            teareport.setTicket(teareport.getTicket()+summaryPlayer.getTicket());
        }


        teareportRepository.saveAndFlush(teareport);

    }

}