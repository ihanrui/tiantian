package com.mile.core.record.controler;

import com.mile.common.cache.CacheHelper;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.game.msgmodel.GpsEvent;
import com.mile.core.game.msgmodel.GpsInfo;
import com.mile.core.record.entity.*;
import com.mile.core.record.jpa.*;
import com.mile.core.teahouse.eventmodel.UserInfo;
import com.mile.util.UKTools;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = {"/record"})
public class RecordQueryController extends Handler {
    private static final Logger logger = LoggerFactory.getLogger(RecordQueryController.class);

    @Autowired
    private TokenRepository tokenRes;

    @Autowired
    private GameRoomRepository gameRoomRepository;

    @Autowired
    private SummaryPlayerRepository summaryPlayerRepository;

    @Autowired
    private SummaryRepository summaryRepository;

    @Autowired
    private StepsumRepository stepsumRepository;

    @Autowired
    private HisplayerRepository hisplayerRepository;

    @Autowired
    private HiscardRepository hiscardRepository;

    @Autowired
    private PinshihiscardRepository pinshihiscardRepository;


    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private ExpenddetailRepository expenddetailRepository;


    private class Queryback {
        public List<Hisplayer> hisplayers;

        public List<Hiscard> hiscards;

        public GameRoom gameRoom;

        public List<Pinshihiscard> pinshihiscards;
    }

    /**
     * 用户 点击大厅战绩查询的第一个接口
     *
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/query"}, method = RequestMethod.POST)
    public ResponseEntity query(HttpServletRequest request, HttpServletResponse response,
                                String token, String teanum, String gameid) {

        Token userToken = null;

        List<Summary> summaryList = new LinkedList<Summary>();

        if (!StringUtils.isBlank(token)) {
            userToken = tokenRes.findById(token);
            if (userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime() != null && userToken.getExptime().after(new Date())) {

                String rdate = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(MaziSummaryUtil.getRecordDate());

                logger.info("获取的最晚查看战绩日期为rdate [" + rdate + "]  茶楼ID teanum[" + teanum + "] gameid[" + gameid + "]");

                List<SummaryPlayer> summaryPlayerList = null;
                if (StringUtils.isEmpty(teanum)) {
                    logger.info("查看大厅战绩");
                    summaryPlayerList = summaryPlayerRepository.findByBegindateAfterAndGameidOrderByBegindateDesc(rdate, gameid);
                } else if (!StringUtils.isEmpty(gameid)) {
                    logger.info("查看茶楼中我的战绩");
                    summaryPlayerList = summaryPlayerRepository.findByBegindateAfterAndGameidAndTeanumOrderByBegindateDesc(rdate, gameid, teanum);
                } else if (StringUtils.isEmpty(gameid)) {
                    logger.info("查看茶楼战绩");
                    summaryList = summaryRepository.findByBegindateAfterAndTeanumOrderByBegindateDesc(rdate, teanum);
                }

                if (!StringUtils.isEmpty(gameid) && null != summaryPlayerList && !summaryPlayerList.isEmpty()) {
                    logger.info("非茶楼战绩格式化");
                    for (SummaryPlayer sp : summaryPlayerList) {
                        Summary summary = summaryRepository.findByGameroomid(sp.getGameroomid());
                        if (null != summary) {
                            List<SummaryPlayer> summaryPlayers = summaryPlayerRepository.findByGameroomid(summary.getGameroomid());
                            for(SummaryPlayer summaryPlayer:summaryPlayers){
                                Expenddetail expenddetail = expenddetailRepository.findByRoomidAndGameidAndExpendtype(summaryPlayer.getRoomid(),summaryPlayer.getGameid(),DataConstants.Roomtype.TEACOIN.toString());
                                if(null == expenddetail) {
                                    expenddetail = expenddetailRepository.findByRoomidAndGameidAndExpendtype(summaryPlayer.getRoomid(), summaryPlayer.getGameid(), DataConstants.Roomtype.COIN.toString());
                                }


                                summaryPlayer.setExpenddetail(expenddetail);
                            }
                            summary.setSummaryPlayer(summaryPlayers);
                            summaryList.add(summary);
                        }
                    }
                } else if (null != summaryList && !summaryList.isEmpty()) {
                    logger.info("茶楼战绩格式化");
                    for (Summary summary : summaryList) {
                        List<SummaryPlayer> summaryPlayers = summaryPlayerRepository.findByGameroomid(summary.getGameroomid());
                        for(SummaryPlayer summaryPlayer:summaryPlayers){
                            Expenddetail expenddetail = expenddetailRepository.findByRoomidAndGameidAndExpendtype(summaryPlayer.getRoomid(),summaryPlayer.getGameid(),DataConstants.Roomtype.TEACOIN.toString());
                            if(null == expenddetail)
                                expenddetailRepository.findByRoomidAndGameidAndExpendtype(summaryPlayer.getRoomid(),summaryPlayer.getGameid(),DataConstants.Roomtype.COIN.toString());


                            summaryPlayer.setExpenddetail(expenddetail);
                        }
                        summary.setSummaryPlayer(summaryPlayers);
                    }

                } else {
                    return new ResponseEntity<>("战绩信息为空", HttpStatus.OK);
                }

            } else {
                if (userToken != null) {
                    tokenRes.delete(userToken);
                    userToken = null;
                }
                return new ResponseEntity<>("token 错误", HttpStatus.OK);
            }
        }

        return new ResponseEntity<List<Summary>>(summaryList, HttpStatus.OK);
    }

    /**
     * 具体战绩查询中，点击详细接口，传入茶楼号和房间UUID 或者 房间六位数字编号
     *
     * @param request
     * @param response
     * @param token
     * @param
     * @param
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/queryDetails"}, method = RequestMethod.POST)
    public ResponseEntity queryDetails(HttpServletRequest request, HttpServletResponse response,
                                       String token, String roomid) {
        PlayUser playUser = null;
        Token userToken = null;

        if (!StringUtils.isBlank(token)) {
            userToken = tokenRes.findById(token);
            if (userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime() != null && userToken.getExptime().after(new Date())) {

                Date rdate = MaziSummaryUtil.getRecordDate();

                logger.info("获取的最晚查看战绩日期为rdate [" + rdate + "]");

                GameRoom gameRoom = new GameRoom();

                List<GameRoom> gameRooms = gameRoomRepository.findByRoomid(roomid);
                if (null != gameRooms && !gameRooms.isEmpty()) {
                    gameRoom = gameRooms.get(0);
                }

                if (null == gameRoom) {
                    gameRoom = gameRoomRepository.findById(roomid);
                }
                if (null == gameRoom) {
                    return new ResponseEntity<>("房间号[" + roomid + "]错误", HttpStatus.OK);
                }

                List<List<Stepsum>> summaryList = new LinkedList<List<Stepsum>>();

                for (int i = 1; i <= gameRoom.getNumofgames() &&
                        null != stepsumRepository.findByRoomidAndCurnum(gameRoom.getRoomid(), i); i++) {
                    summaryList.add(stepsumRepository.findByRoomidAndCurnum(gameRoom.getRoomid(), i));
                }

                //summaryList.add(stepsumRepository.findByRoomid(gameRoom.getRoomid()));

                return new ResponseEntity<List<List<Stepsum>>>(summaryList, HttpStatus.OK);

            } else {

                return new ResponseEntity<>("token 错误", HttpStatus.OK);
            }
        }

        return new ResponseEntity<>("token为空", HttpStatus.OK);
    }

    /**
     * 具体战绩查询中，点击详细接口，传入茶楼号和房间UUID 或者 房间六位数字编号
     *
     * @param request
     * @param response
     * @param token
     * @param
     * @param
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/queryPlayback"}, method = RequestMethod.POST)
    public ResponseEntity queryPlayback(HttpServletRequest request, HttpServletResponse response,
                                        String token, String roomid, Integer currentnum) {

        try {
            PlayUser playUser = null;
            Token userToken = null;

            if (!StringUtils.isBlank(token)) {
                userToken = tokenRes.findById(token);
                if (userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime() != null && userToken.getExptime().after(new Date())) {

                    Date rdate = MaziSummaryUtil.getRecordDate();

                    logger.info("获取的最晚查看战绩日期为rdate [" + rdate + "]");

                    Queryback queryback = new Queryback();
                    queryback.hisplayers = hisplayerRepository.findByRoomidAndCurrentnum(roomid, currentnum);
                    queryback.hiscards = hiscardRepository.findByRoomidAndCurrentnum(roomid, currentnum);

                    queryback.pinshihiscards = pinshihiscardRepository.findByRoomidAndCurrentnum(roomid, currentnum);

                    queryback.gameRoom = gameRoomRepository.findByRoomid(roomid).get(0);

                    return new ResponseEntity<Queryback>(queryback, HttpStatus.OK);

                } else {
                    if (userToken != null) {
                        tokenRes.delete(userToken);
                        userToken = null;
                    }
                    return new ResponseEntity<>("token 错误", HttpStatus.OK);
                }
            }

            return new ResponseEntity<>("token为空", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取回放信息异常", e);
            return new ResponseEntity<>("获取回放信息异常", HttpStatus.OK);

        }
    }

    /**
     * 具体战绩查询中，点击详细接口，传入茶楼号和房间UUID 或者 房间六位数字编号
     *
     * @param request
     * @param response
     * @param token
     * @param
     * @param
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/querygps"}, method = RequestMethod.POST)
    public ResponseEntity querygps(HttpServletRequest request, HttpServletResponse response,
                                   String token) {

        try {
            PlayUser playUser = null;
            Token userToken = null;

            if (!StringUtils.isBlank(token)) {
                userToken = tokenRes.findById(token);
                if (userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime() != null && userToken.getExptime().after(new Date())) {

                    String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), BMDataContext.SYSTEM_ORGI);

                    List<PlayUserClient> playUserClientList = CacheHelper.getGamePlayerCacheBean().getCacheObject(roomid, BMDataContext.SYSTEM_ORGI);
                    ;
                    if (null != playUserClientList && !playUserClientList.isEmpty()) {
                        GpsEvent gpsEvent = new GpsEvent();
                        gpsEvent.setCommand(DataConstants.GAME_EVENT_GPS);
                        for (int i = 0; i < playUserClientList.size(); i++) {
                            UserInfo u = new UserInfo();
                            u.setUserid(playUserClientList.get(i).getId());
                            UKTools.copyProperties(playUserClientList.get(i), u);
                            gpsEvent.addUserInfo(u);
                            int j1 = 0;
                            for (int j2 = j1; j2 < playUserClientList.size(); j2++) {

                                if (i != j2) {
                                    UserInfo u1 = new UserInfo();
                                    UserInfo u2 = new UserInfo();
                                    UKTools.copyProperties(playUserClientList.get(i), u1);
                                    UKTools.copyProperties(playUserClientList.get(j2), u2);
                                    Double dis = 0.0;
                                    if(playUserClientList.get(i).getLatitude() > 0
                                            && playUserClientList.get(j2).getLatitude() > 0){

                                        dis = UKTools.GetDistance(playUserClientList.get(i).getLatitude(), playUserClientList.get(i).getLongitude(),
                                                playUserClientList.get(j2).getLatitude(), playUserClientList.get(j2).getLongitude());

                                    }
                                    logger.info("距离 信息 [" + new GpsInfo(u1.getGameid(), u2.getGameid(), dis) + "]");

                                    gpsEvent.addGpsInfo(new GpsInfo(u1.getGameid(), u2.getGameid(), dis));
                                }
                            }
                            j1++;
                        }
                        return new ResponseEntity<GpsEvent>(gpsEvent, HttpStatus.OK);
                    } else {
                        return new ResponseEntity<String>("未找到房间信息", HttpStatus.OK);
                    }

                } else {
                    if (userToken != null) {
                        tokenRes.delete(userToken);
                        userToken = null;
                    }
                    return new ResponseEntity<>("token 错误", HttpStatus.OK);
                }
            }

            return new ResponseEntity<>("token为空", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取回放信息失败", e);
            return new ResponseEntity<>("获取回放信息失败", HttpStatus.OK);

        }
    }

    /**
     * 通过gameid获取用户信息
     * @param request
     * @param response
     * @param gameid
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/querygameuser"}, method = RequestMethod.GET)
    public ResponseEntity querygameuser(HttpServletRequest request, HttpServletResponse response,
                                        String gameid) {
        logger.info("通过gameid获取用户信息gameid["+gameid+"]");
        try {
            if(StringUtils.isEmpty(gameid))
                return new ResponseEntity<>("querygameuser GAMEID为空", HttpStatus.OK);

            return new ResponseEntity<>(playUserRepository.findByGameid(gameid), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取回放信息异常", e);
            return new ResponseEntity<>("获取回放信息异常", HttpStatus.OK);

        }
    }


}