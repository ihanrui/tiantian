package com.mile.core.record.controler;

import com.mile.core.record.entity.Teascorerecord;
import com.mile.core.record.jpa.TeascorerecordRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/apps/maziplatform")
public class TeascorerecordController extends Handler{


	@Autowired
	private TeascorerecordRepository teascorerecordRepository ;


	@RequestMapping({"/teascorerecord"})
	@Menu(type="platform", subtype="teascorerecord")
	public ModelAndView gameusers(ModelMap map , HttpServletRequest request){
		Page<Teascorerecord> teascorerecordlist = teascorerecordRepository.findAll(new PageRequest(super.getP(request), super.getPs(request) , new Sort(Sort.Direction.DESC, "lastmodify"))) ;
		map.addAttribute("teascorerecordlist", teascorerecordlist) ;
		return request(super.createAppsTempletResponse("/apps/business/maziplatform/teascorerecord/teascorerecordlist"));
	}



}
