package com.mile.core.record.controler;

import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.record.entity.Expenddetail;
import com.mile.core.record.jpa.ExpenddetailRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/apps/maziplatform")
public class ExpenddetailController extends Handler{


	@Autowired
	private ExpenddetailRepository expenddetailRepository ;


	@Autowired
	private PlayUserRepository playUserRes;

	@Autowired
	private GamePlaywayRepository playwayRes;

	@RequestMapping({"/expenddetail"})
	@Menu(type="platform", subtype="expenddetail")
	public ModelAndView gameusers(ModelMap map , HttpServletRequest request){
		Page<Expenddetail> expenddetaillist = expenddetailRepository.findAll(new PageRequest(super.getP(request), super.getPs(request) , new Sort(Sort.Direction.DESC, "lastmodify"))) ;

		map.addAttribute("expenddetaillist", expenddetaillist) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/expenddetail/expenddetaillist"));
	}



}
