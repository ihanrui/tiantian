package com.mile.core.record.jpa;

import com.mile.core.record.entity.Hiscard;
import com.mile.core.record.entity.Pinshihiscard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface PinshihiscardRepository extends JpaRepository<Pinshihiscard, String>
{
	public abstract Hiscard findById(String id);

	public abstract List<Pinshihiscard> findByRoomid(String roomid);

	public abstract List<Pinshihiscard> findByRoomidAndCurrentnum(String roomid, int currentnum);


}
