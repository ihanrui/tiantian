package com.mile.core.record.jpa;

import com.mile.core.record.entity.Summary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface SummaryRepository extends JpaRepository<Summary, String>
{
	public abstract Summary findById(String id);


	public abstract Summary findByGameroomidOrRoomid(String gameroomid, String roomid);



	public abstract Summary findByGameroomid(String gameroomid);


	public abstract List<Summary> findByBegindateAfterAndTeanum(String begindate,String teanum);

	public abstract List<Summary> findByBegindateAfterAndTeanumOrderByBegindateDesc(String begindate,String teanum);




}
