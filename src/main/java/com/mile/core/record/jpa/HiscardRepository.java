package com.mile.core.record.jpa;

import com.mile.core.record.entity.Hiscard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface HiscardRepository extends JpaRepository<Hiscard, String>
{
	public abstract Hiscard findById(String id);

	public abstract List<Hiscard> findByRoomid(String roomid);

	public abstract List<Hiscard> findByRoomidAndCurrentnum(String roomid,int currentnum);


}
