package com.mile.core.record.jpa;

import com.mile.core.record.entity.Stepsum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface StepsumRepository extends JpaRepository<Stepsum, String>
{
	public abstract Stepsum findById(String id);


	public abstract List<Stepsum> findByGameroomidOrRoomid(String gameroomid, String roomid);



	public abstract List<Stepsum> findByGameroomid(String gameroomid);



	public abstract List<Stepsum> findByRoomidAndCurnum(String roomid,int curnum);

	public abstract List<Stepsum> findByRoomid(String roomid);



}
