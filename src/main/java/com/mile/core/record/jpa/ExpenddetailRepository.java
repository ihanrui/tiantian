package com.mile.core.record.jpa;

import com.mile.core.record.entity.Expenddetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface ExpenddetailRepository extends JpaRepository<Expenddetail, String>
{
	public abstract Expenddetail findById(String id);

	public abstract List<Expenddetail> findByRoomid(String roomid);

	public abstract Expenddetail findByRoomidAndGameid(String roomid,String gameid);



	public abstract Expenddetail findByRoomidAndGameidAndExpendtype(String roomid,String gameid,String extendtype);


}
