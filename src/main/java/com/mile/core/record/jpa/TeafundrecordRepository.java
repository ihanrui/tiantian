package com.mile.core.record.jpa;

import com.mile.core.record.entity.Teafundrecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface TeafundrecordRepository extends JpaRepository<Teafundrecord, String>
{
	public abstract Teafundrecord findById(String id);

	public abstract Teafundrecord findByGameid(String gameid);

	public abstract List<Teafundrecord> findByTeanumAndGameid(String teanum,String gameid);


	public abstract List<Teafundrecord> findByTeanum(String teanum);


	public abstract Page<Teafundrecord> findByTeanumAndGameid(String teanum,String gameid,Pageable pageable);

	public abstract Page<Teafundrecord> findByTeanum(String teanum,Pageable pageable);



}
