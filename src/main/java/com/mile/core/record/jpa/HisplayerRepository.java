package com.mile.core.record.jpa;

import com.mile.core.record.entity.Hisplayer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface HisplayerRepository extends JpaRepository<Hisplayer, String>
{
	public abstract Hisplayer findById(String id);

	public abstract List<Hisplayer> findByRoomid(String roomid);

	public abstract List<Hisplayer> findByRoomidAndCurrentnum(String roomid, int currentnum);

}
