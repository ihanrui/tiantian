package com.mile.core.record.jpa;

import com.mile.core.record.entity.SummaryPlayer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface SummaryPlayerRepository extends JpaRepository<SummaryPlayer, String>
{
	public abstract SummaryPlayer findById(String id);

	public abstract List<SummaryPlayer> findByBegindateAfterAndUserid(String begindate, String userid);

	public abstract Page<SummaryPlayer> findByBegindateAfterAndUserid(String begindate, String userid, Pageable pageable);


	public abstract List<SummaryPlayer> findByBegindateAfterAndUseridOrderByBegindateDesc(String begindate, String userid);

	public abstract List<SummaryPlayer> findByBegindateAfterAndGameidOrderByBegindateDesc(String begindate, String gameid);





	public abstract List<SummaryPlayer> findByGameroomid(String gameroomid);


	public abstract Page<SummaryPlayer> findByGameroomidIn(String[] gameroomid, Pageable pageable);



	public abstract List<SummaryPlayer> findByBegindateAfterAndUseridAndTeanum(String begindate, String userid, String teanum);

	public abstract List<SummaryPlayer> findByBegindateAfterAndGameidAndTeanum(String begindate, String gameid, String teanum);


	public abstract List<SummaryPlayer> findByBegindateAfterAndTeanumOrderByBegindateDesc(String begindate, String teanum);

	public abstract List<SummaryPlayer> findByBegindateAfterAndUseridAndTeanumOrderByBegindateDesc(String begindate, String userid, String teanum);

	public abstract List<SummaryPlayer> findByBegindateAfterAndGameidAndTeanumOrderByBegindateDesc(String begindate, String gameid, String teanum);


	public abstract Page<SummaryPlayer> findByBegindateAfterAndUseridAndTeanum(String begindate, String userid, String teanum,Pageable pageable);


}
