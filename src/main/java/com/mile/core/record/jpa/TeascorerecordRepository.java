package com.mile.core.record.jpa;

import com.mile.core.record.entity.Teascorerecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface TeascorerecordRepository extends JpaRepository<Teascorerecord, String>
{
	public abstract Teascorerecord findById(String id);


	public abstract Teascorerecord findByGameid(String gameid);

	public abstract List<Teascorerecord> findByTeanumAndGameid(String teanum, String gameid);


	public abstract List<Teascorerecord> findByTeanum(String teanum);


	public abstract Page<Teascorerecord> findByTeanumAndGameid(String teanum, String gameid, Pageable pageable);

	public abstract Page<Teascorerecord> findByTeanum(String teanum,Pageable pageable);



}
