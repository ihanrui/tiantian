package com.mile.core.record.jpa;

import com.mile.core.game.mazi.msgmodel.Takecardshistory;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface TakecardshistoryRepository extends JpaRepository<Takecardshistory, String>
{
	public abstract Takecardshistory findById(String id);

	public abstract Takecardshistory findByBoardid(String boardid);




}
