package com.mile.core.record.entity;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 茶楼上下分记录
 */
@Entity
@Table(name = "wd_r_teascorerecord")
@org.hibernate.annotations.Proxy(lazy = false)
public class Teascorerecord extends BaseEvent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String id = UKTools.getUUID();

    private String teanum;        //茶楼编号

    private String optid;//操作者GAMEID

    private String gameid;

    private String userid;

    private String username;

    private String userurl;

    private Integer topupbegin;//充值前

    private Integer topupend;//充值后

    private Integer topup;//本次消耗

    private String memo;//备注

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间









    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public Integer getTopupbegin() {
        return topupbegin;
    }

    public void setTopupbegin(Integer topupbegin) {
        this.topupbegin = topupbegin;
    }

    public Integer getTopupend() {
        return topupend;
    }

    public void setTopupend(Integer topupend) {
        this.topupend = topupend;
    }

    public Integer getTopup() {
        return topup;
    }

    public void setTopup(Integer topup) {
        this.topup = topup;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getOptid() {
        return optid;
    }

    public void setOptid(String optid) {
        this.optid = optid;
    }
}



