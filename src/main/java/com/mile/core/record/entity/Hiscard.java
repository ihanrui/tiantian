package com.mile.core.record.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michael
 * 小结结算
 */
@Entity
@Table(name = "wd_his_card")
@org.hibernate.annotations.Proxy(lazy = false)
public class Hiscard extends BaseIdEntity {


    private static final long serialVersionUID = 1L;

    private String gameroomid;//房间ID

    private String roomid;//房间号

    private String teanum;//茶楼号

    private Integer tablenum;//桌号

    private Integer ordercards;

    private String scoreuserid;//变化着分数

    private Integer  changescore;//变化分数

    private Integer lvl;//几游

    private Boolean tableclean;//是否清理桌面

    private String userid ;

    private  Integer mazicardtype;//码子牌型

    private String cards;

    private Integer currentnum;//第几盘


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间







    @Id
    @Column(length = 36)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public Integer getTablenum() {
        return tablenum;
    }

    public void setTablenum(Integer tablenum) {
        this.tablenum = tablenum;
    }

    public Integer getOrdercards() {
        return ordercards;
    }

    public void setOrdercards(Integer ordercards) {
        this.ordercards = ordercards;
    }

    public String getScoreuserid() {
        return scoreuserid;
    }

    public void setScoreuserid(String scoreuserid) {
        this.scoreuserid = scoreuserid;
    }

    public Integer getChangescore() {
        return changescore;
    }

    public void setChangescore(Integer changescore) {
        this.changescore = changescore;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public Boolean getTableclean() {
        return tableclean;
    }

    public void setTableclean(Boolean tableclean) {
        this.tableclean = tableclean;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getMazicardtype() {
        return mazicardtype;
    }

    public void setMazicardtype(Integer mazicardtype) {
        this.mazicardtype = mazicardtype;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    public Integer getCurrentnum() {
        return currentnum;
    }

    public void setCurrentnum(Integer currentnum) {
        this.currentnum = currentnum;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }


}
