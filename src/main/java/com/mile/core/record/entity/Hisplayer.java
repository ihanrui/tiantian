package com.mile.core.record.entity;

import com.mile.common.entity.BaseIdEntity;
import com.mile.core.DataConstants;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michael
 * 小结结算
 */
@Entity
@Table(name = "wd_his_player")
@org.hibernate.annotations.Proxy(lazy = false)
public class Hisplayer extends BaseIdEntity {

    private static final long serialVersionUID = 1L;

    private String gameroomid;//房间ID

    private String roomid;//房间号

    private String teanum;//茶楼号

    private Integer tablenum;//桌号

    private String userid;

    private String username;

    private String gameid; //游戏ID

    private String userurl = DataConstants.GAME_USER_DEFAULT_URL;

    private int teamflag;

    private String cardstr;

    private Integer currentnum;//第几盘

    private Integer sitindex;

    private String lastmodify = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间

    private Integer totalscore = 0;//总共输赢的分数













    @Id
    @Column(length = 36)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public Integer getTablenum() {
        return tablenum;
    }

    public void setTablenum(Integer tablenum) {
        this.tablenum = tablenum;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getCardstr() {
        return cardstr;
    }

    public void setCardstr(String cardstr) {
        this.cardstr = cardstr;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public Integer getCurrentnum() {
        return currentnum;
    }

    public void setCurrentnum(Integer currentnum) {
        this.currentnum = currentnum;
    }

    public Integer getSitindex() {
        return sitindex;
    }

    public void setSitindex(Integer sitindex) {
        this.sitindex = sitindex;
    }

    public Integer getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(Integer totalscore) {
        this.totalscore = totalscore;
    }

    public int getTeamflag() {
        return teamflag;
    }

    public void setTeamflag(int teamflag) {
        this.teamflag = teamflag;
    }
}

