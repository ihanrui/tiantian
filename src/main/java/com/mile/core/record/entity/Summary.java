package com.mile.core.record.entity;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "wd_r_summary")
@org.hibernate.annotations.Proxy(lazy = false)
public class Summary extends BaseEvent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String id = UKTools.getUUID();

    private String gameroomid;        //房间ID

    private String roomid;        //房间编号

    private List<SummaryPlayer> summaryPlayer = new LinkedList<SummaryPlayer>();

    private String begindate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));//开始时间

    private String overdate;//结束时间

    private String gameplaywayid;

    private String gameplaywaycode;

    private String teanum;//茶馆编号

    private Integer tablenum;//桌号

    private String roomtype;//房间类型

    private String scoreunit;//底分

    private Integer ticket;//门值


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间



    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Transient
    public List<SummaryPlayer> getSummaryPlayer() {
        if (null == summaryPlayer)
            return new LinkedList<SummaryPlayer>();
        return summaryPlayer;
    }

    public void setSummaryPlayer(List<SummaryPlayer> summaryPlayer) {
        this.summaryPlayer = summaryPlayer;
    }

    public String getBegindate() {
        return begindate;
    }

    public void setBegindate(String begindate) {
        this.begindate = begindate;
    }

    public String getOverdate() {
        return overdate;
    }

    public void setOverdate(String overdate) {
        this.overdate = overdate;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public Integer getTablenum() {
        return tablenum;
    }

    public void setTablenum(Integer tablenum) {
        this.tablenum = tablenum;
    }

    public String getGameplaywayid() {
        return gameplaywayid;
    }

    public void setGameplaywayid(String gameplaywayid) {
        this.gameplaywayid = gameplaywayid;
    }

    public String getGameplaywaycode() {
        return gameplaywaycode;
    }

    public void setGameplaywaycode(String gameplaywaycode) {
        this.gameplaywaycode = gameplaywaycode;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype;
    }

    public String getScoreunit() {
        return scoreunit;
    }

    public void setScoreunit(String scoreunit) {
        this.scoreunit = scoreunit;
    }

    public Integer getTicket() {
        return ticket;
    }

    public void setTicket(Integer ticket) {
        this.ticket = ticket;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}
