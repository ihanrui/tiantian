package com.mile.core.record.entity;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "wd_r_expenddetail")
@org.hibernate.annotations.Proxy(lazy = false)
public class Expenddetail extends BaseEvent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String id = UKTools.getUUID();

    private String roomid;        //房间编号

    private String roomtype;//房间类型

    private String expendtype;//  房卡，金币，茶楼金币 DataConstants.Roomtype

    private String userid;

    private String gameid;


    private String username;

    private String userurl;

    private Integer expendbegin;//消耗前

    private Integer expendend;//消耗后

    private Integer expend;//本次消耗

    private String ticketflag="N";//是否为门票

    private String memo;//备注

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间









    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getExpendtype() {
        return expendtype;
    }

    public void setExpendtype(String expendtype) {
        this.expendtype = expendtype;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public Integer getExpendbegin() {
        return expendbegin;
    }

    public void setExpendbegin(Integer expendbegin) {
        this.expendbegin = expendbegin;
    }

    public Integer getExpendend() {
        return expendend;
    }

    public void setExpendend(Integer expendend) {
        this.expendend = expendend;
    }

    public Integer getExpend() {
        return expend;
    }

    public void setExpend(Integer expend) {
        this.expend = expend;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getTicketflag() {
        return ticketflag;
    }

    public void setTicketflag(String ticketflag) {
        this.ticketflag = ticketflag;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype;
    }
}



