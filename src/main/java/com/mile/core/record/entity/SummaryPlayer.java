package com.mile.core.record.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "wd_r_summaryplayer")
@org.hibernate.annotations.Proxy(lazy = false)

public class SummaryPlayer extends BaseIdEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private String gameroomid;//房间ID

	private String roomid;//房间号

	private String teanum;//茶楼号

	private Integer tablenum;//桌号

	private String userid;

	private String username;

	private String gameid; //游戏ID

	private String userurl;


	private Integer totalscore=0;//输赢分数

	private Integer coins = 0;//积分



	private Integer bankersum=0;//庄家次数

	private Integer lvlsum=0;//一游次数

	private Integer newssum=0;//报喜次数

	private Integer winsum=0;//赢牌次数

	private Integer maxscore=0;//赢牌次数

	private Integer winflag=0;//是否为大赢家  1为大赢家

	private Integer richflag=0;//土豪标志  1位土豪

	private String begindate;//开始日期

	private String enddate;//结束日期

	private Integer scoreunit;

	private Integer ticket=0;//门票


	private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间

	private Expenddetail expenddetail;

	private String bolusflag;

	private int bolustimes=0;//推注倍数



	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGameroomid() {
		return gameroomid;
	}

	public void setGameroomid(String gameroomid) {
		this.gameroomid = gameroomid;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getTeanum() {
		return teanum;
	}

	public void setTeanum(String teanum) {
		this.teanum = teanum;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public String getUserurl() {
		return userurl;
	}

	public void setUserurl(String userurl) {
		this.userurl = userurl;
	}

	public Integer getTotalscore() {
		return totalscore;
	}

	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}

	public Integer getBankersum() {
		return bankersum;
	}

	public void setBankersum(Integer bankersum) {
		this.bankersum = bankersum;
	}

	public Integer getLvlsum() {
		return lvlsum;
	}

	public void setLvlsum(Integer lvlsum) {
		this.lvlsum = lvlsum;
	}

	public Integer getNewssum() {
		return newssum;
	}

	public void setNewssum(Integer newssum) {
		this.newssum = newssum;
	}

	public Integer getWinsum() {
		return winsum;
	}

	public void setWinsum(Integer winsum) {
		this.winsum = winsum;
	}

	public Integer getMaxscore() {
		return maxscore;
	}

	public void setMaxscore(Integer maxscore) {
		this.maxscore = maxscore;
	}

	public Integer getWinflag() {
		return winflag;
	}

	public void setWinflag(Integer winflag) {
		this.winflag = winflag;
	}

	public Integer getRichflag() {
		return richflag;
	}

	public void setRichflag(Integer richflag) {
		this.richflag = richflag;
	}

	public String getBegindate() {
		return begindate;
	}

	public void setBegindate(String begindate) {
		this.begindate = begindate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public Integer getCoins() {
		return coins;
	}

	public void setCoins(Integer coins) {
		this.coins = coins;
	}

	public Integer getScoreunit() {
		return scoreunit;
	}

	public void setScoreunit(Integer scoreunit) {
		this.scoreunit = scoreunit;
	}

	public Integer getTicket() {
		return ticket;
	}

	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}

	public String getLastmodify() {
		return lastmodify;
	}

	public void setLastmodify(String lastmodify) {
		this.lastmodify = lastmodify;
	}


	@Transient
	public Expenddetail getExpenddetail() {
		return expenddetail;
	}

	public void setExpenddetail(Expenddetail expenddetail) {
		this.expenddetail = expenddetail;
	}


	@Transient
	public String getBolusflag() {
		return bolusflag;
	}

	public void setBolusflag(String bolusflag) {
		this.bolusflag = bolusflag;
	}

	@Transient
	public int getBolustimes() {
		return bolustimes;
	}

	public void setBolustimes(int bolustimes) {
		this.bolustimes = bolustimes;
	}
}
