package com.mile.core.record.entity;

import com.mile.common.entity.BaseIdEntity;
import com.mile.core.game.pinshi.msgmodel.Pinshicardtype;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michael
 * 小结结算
 */
@Entity
@Table(name = "wd_his_pinshicard")
@org.hibernate.annotations.Proxy(lazy = false)
public class Pinshihiscard extends BaseIdEntity {


    private static final long serialVersionUID = 1L;

    private String gameroomid;//房间ID

    private String roomid;//房间号

    private String teanum;//茶楼号

    private Integer tablenum;//桌号

    private Integer currentnum;//第几盘


    private String userid ;



    protected Integer stepscore = 0;//本局输赢的分数

    private Integer hogtimes=0;//抢庄倍数

    private Integer bankertimes=0;//庄家倍数

    private Integer bettimes=0;//闲家下注倍数

    private Byte lastcard;//最后一张牌


    private String scorecard;//算点数的牌

    protected Integer cardtype ;	//牌型


    protected Boolean banker;    //是否庄家


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间







    @Id
    @Column(length = 36)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGameroomid() {
        return gameroomid;
    }

    public void setGameroomid(String gameroomid) {
        this.gameroomid = gameroomid;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public Integer getTablenum() {
        return tablenum;
    }

    public void setTablenum(Integer tablenum) {
        this.tablenum = tablenum;
    }

    public Integer getCurrentnum() {
        return currentnum;
    }

    public void setCurrentnum(Integer currentnum) {
        this.currentnum = currentnum;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getStepscore() {
        return stepscore;
    }

    public void setStepscore(Integer stepscore) {
        this.stepscore = stepscore;
    }

    public Integer getHogtimes() {
        return hogtimes;
    }

    public void setHogtimes(Integer hogtimes) {
        this.hogtimes = hogtimes;
    }

    public Integer getBankertimes() {
        return bankertimes;
    }

    public void setBankertimes(Integer bankertimes) {
        this.bankertimes = bankertimes;
    }

    public Integer getBettimes() {
        return bettimes;
    }

    public void setBettimes(Integer bettimes) {
        this.bettimes = bettimes;
    }

    public Byte getLastcard() {
        return lastcard;
    }

    public void setLastcard(Byte lastcard) {
        this.lastcard = lastcard;
    }

    public String getScorecard() {
        return scorecard;
    }

    public void setScorecard(String scorecard) {
        this.scorecard = scorecard;
    }

    public Integer getCardtype() {
        return cardtype;
    }

    public void setCardtype(Integer cardtype) {
        this.cardtype = cardtype;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public Boolean getBanker() {
        return banker;
    }

    public void setBanker(Boolean banker) {
        this.banker = banker;
    }
}
