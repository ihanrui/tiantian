package com.mile.core.teahouse.jpa;

import com.mile.core.teahouse.entity.Teamem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface TeamemRepository extends JpaRepository<Teamem, String>
{
	public abstract Teamem findById(String id);

	public abstract Page<Teamem> findAll(Pageable page);

	public abstract List<Teamem> findByTeanumAndRole(String teanum, String role);

	public abstract List<Teamem> findByTeanumAndRoleIn(String teanum, String[] roles);

	public abstract Page<Teamem> findByTeanumAndRole(String teanum, String role,  Pageable page);

	public abstract Page<Teamem> findByTeanumAndRoleAndGameid(String teanum, String role,String gameid,  Pageable page);

	public abstract List<Teamem> findByTeanumAndRoleAndPassflag(String teanum, String role,String passflag);

	public abstract Page<Teamem> findByTeanumAndRoleAndPassflag(String teanum, String role, String passflag,  Pageable page);

	public abstract Page<Teamem> findByTeanumAndRoleAndPassflagAndGameid(String teanum, String role, String passflag,String gameid,  Pageable page);



	public abstract List<Teamem> findByTeanumAndPassflag(String teanum, String passflag);

	public abstract Page<Teamem> findByTeanumAndPassflag(String teanum, String passflag,  Pageable page);


	public abstract Page<Teamem> findByTeanumAndPassflagAndGameid(String teanum, String passflag,String gameid,  Pageable page);



	public abstract List<Teamem> findByTeanum(String teanum);

	public abstract Page<Teamem> findByTeanum(String teanum,  Pageable page);


	public abstract Page<Teamem> findByTeanumAndGameid(String teanum,String gameid,  Pageable page);

	public abstract Teamem findByGameidAndTeanum(String gameid, String teanum);

	public abstract List<Teamem> findByGameid(String gameid);

	public abstract List<Teamem> findByUserid(String userid);


	public abstract Teamem findByUseridAndTeanum(String userid,String teanum);




	public abstract void deleteByTeanum(String teanum);


	public abstract void deleteByGameid(String gameid);


}
