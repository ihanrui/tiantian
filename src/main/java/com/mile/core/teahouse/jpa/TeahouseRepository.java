package com.mile.core.teahouse.jpa;

import com.mile.core.teahouse.entity.Teahouse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface TeahouseRepository extends JpaRepository<Teahouse, String>
{
	public abstract Teahouse findById(String id);

	public abstract Page<Teahouse> findAll(Pageable page);

	public abstract Teahouse findByTeanum(String teanum);

	public abstract List<Teahouse> findByGameid(String gameid);




}
