package com.mile.core.teahouse.jpa;

import com.mile.core.teahouse.entity.Teareport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface TeareportRepository extends JpaRepository<Teareport, String>
{
	public abstract Teareport findById(String id);

	public abstract Teareport findByTeanumAndReportdate(String teanum,String reportdate);


	public abstract Page<Teareport> findByTeanum(String  teanum,Pageable pageable);



}
