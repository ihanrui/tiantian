package com.mile.core.teahouse.service;

import com.alibaba.fastjson.JSON;
import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.engine.game.eventmodel.LeaveEvent;
import com.mile.core.entity.*;
import com.mile.core.game.*;
import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.record.entity.Teafundrecord;
import com.mile.core.record.entity.Teascorerecord;
import com.mile.core.record.jpa.TeafundrecordRepository;
import com.mile.core.record.jpa.TeascorerecordRepository;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.entity.Teamem;
import com.mile.core.teahouse.eventmodel.*;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.core.teahouse.jpa.TeamemRepository;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import com.mile.util.UKTools;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(value = "teahouseService")
public class TeahouseService {
    private static final Logger logger = LoggerFactory.getLogger(TeahouseService.class);

    /**
     * 创建茶楼
     *
     * @return
     */
    public Teahouse creatTeahouse(TeahouseClient client) {

        try {

            logger.info("creatTeahouse 客户端信息  client[" + client + "]");

            List<Teahouse> teahouseList = BMDataContext.getContext().getBean(TeahouseRepository.class).findByGameid(client.getGameid());

            if (null != teahouseList) {
                int teaownernum = Integer.parseInt(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_TEAOWNER));
                if (teahouseList.size() >= teaownernum) {
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("创建茶楼个数不能超过[" + teaownernum + "]个", false));
                    return null;
                }
            }

            Teahouse teahouse = new Teahouse();

            PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);

            PlayUser playUser = playUserRepository.findByGameid(client.getGameid());

            logger.info("创建茶楼 用户 [" + playUser.getUsername() + "] [" + playUser.getGameid() + "]");
            teahouse.setGameid(playUser.getGameid());

            GamePlaywayRepository gamePlaywayRepository = BMDataContext.getContext().getBean(GamePlaywayRepository.class);

            GamePlayway gamePlayway = gamePlaywayRepository.findById(client.getPlaywayid());

            logger.info("玩法  [" + playUser.getUsername() + "] [" + playUser.getId() + "]");

            teahouse.setName(gamePlayway.getName());

            teahouse.setGameplaywaycode(gamePlayway.getCode());
            teahouse.setGameplaywayid(gamePlayway.getId());
            teahouse.setGameid(playUser.getGameid());
            teahouse.setState(DataConstants.Teastatus.OPEN.toString());
            teahouse.setCreatedate(new Date(System.currentTimeMillis()));
            teahouse.setExtparams(client.getExtparams());
            teahouse.setRoominfo(TeahouseUtil.getRoominfo(teahouse.getExtparams(),teahouse.getGameplaywaycode()));
            teahouse.setTeanum(TeahouseUtil.getTeanum());//设置茶楼号
            teahouse.setUsername(playUser.getUsername());
            teahouse.setUserurl(StringUtils.isEmpty(playUser.getUserurl()) ? DataConstants.GAME_USER_DEFAULT_URL : playUser.getUserurl());
            teahouse.setPassflag("Y");
            teahouse.setIpflag("N");
            teahouse.setShareflag("Y");
            teahouse.setUserid(playUser.getId());
            teahouse.setTeanotice(teahouse.getRoominfo() + ( null == teahouse.getUsernotice()?"":( " 楼主留言:" + teahouse.getUsernotice()) ));

            teahouse.setLastmodify(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));

            teahouse.setNums(TeahouseUtil.getNums(client.getExtparams(),teahouse.getGameplaywaycode()));


            BMDataContext.getContext().getBean(TeahouseRepository.class).save(teahouse);

            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_CREATE, client.getUserid(), new TeahouseinfoEvent(teahouse));

            Teamem teamem = new Teamem();
            teamem.setTeanum(teahouse.getTeanum());
            teamem.setGameid(playUser.getGameid());
            teamem.setPassflag(DataConstants.Teapassflag.PASS.toString());
            teamem.setLimitscore(teahouse.getLimitscore());
            teamem.setScore(0);//默认开始为0
            teamem.setRole(DataConstants.Tearole.OWNER.toString());//普通成员
            teamem.setUserid(playUser.getId());
            teamem.setUserurl(StringUtils.isEmpty(playUser.getUserurl()) ? DataConstants.GAME_USER_DEFAULT_URL : playUser.getUserurl());
            teamem.setUsername(playUser.getUsername());
            teamem.setLastmodify(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));

            BMDataContext.getContext().getBean(TeamemRepository.class).save(teamem);

            CacheHelper.getTeahouseCacheBean().put(teahouse.getTeanum(), new TeahouseinfoEvent(teahouse), "");

            return teahouse;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("创建茶楼异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("创建茶楼异常", false));

            return null;
        }
    }

    /**
     * 修改茶楼信息
     *
     * @∂
     */
    public void formateClient2Entity(TeahouseClient client) {

        try {

            logger.info("formateClient2Entity客户端信息  client[" + client + "]");
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());

            TeahouseinfoEvent teahouseinfoEvent = new TeahouseinfoEvent(teahouse);

            logger.info("formateClient2Entity 系统茶楼信息 teahouse[" + teahouse + "]");

            Teamem teamem = BMDataContext.getContext().getBean(TeamemRepository.class).findByUseridAndTeanum(client.getUserid(), client.getTeanum());

            if (null == teahouse) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("茶楼信息为空", false));
                return;
            }

            if (null == teamem) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("用户未登录", false));
                return;
            }

            if (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                    !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole())) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("非管理者不能修改茶楼信息", false));
                return;
            }

            if (!StringUtils.isEmpty(client.getName())) {
                logger.info("修改茶楼名称");
                teahouse.setName(client.getName());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_NOTICE.toString());
            }


            if (!StringUtils.isEmpty(client.getClosenotice())) {
                logger.info("修改茶楼歇业公告");
                teahouse.setClosenotice(client.getClosenotice());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_NOTICE.toString());
            }


            if (!StringUtils.isEmpty(client.getUsernotice())) {
                logger.info("修改茶楼公告");
                teahouse.setUsernotice(client.getUsernotice());
                teahouse.setTeanotice(teahouse.getRoominfo() +   ( null == teahouse.getUsernotice()?"":( " 楼主留言:" + teahouse.getUsernotice()) ) );
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_NOTICE.toString());

            }

            if (null != client.getExtparams()) {
                logger.info("修改茶楼玩法");
                teahouse.setExtparams(client.getExtparams());
                teahouse.setRoominfo(TeahouseUtil.getRoominfo(teahouse.getExtparams(),teahouse.getGameplaywaycode()));
                teahouse.setTeanotice(teahouse.getRoominfo() + ( null == teahouse.getUsernotice()?"":( " 楼主留言:" + teahouse.getUsernotice()) ) );
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_PLAYWAY.toString());

            }

            if (!StringUtils.isEmpty(client.getState())) {
                logger.info("销毁茶楼 开业 打烊");
                teahouse.setState(client.getState());
                if (DataConstants.Teastatus.DESTORY.toString().equals(client.getState()))
                    teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.DESTORY.toString());
                else
                    teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.TEASTATUS.toString());

            }

            if (!StringUtils.isEmpty(client.getIpflag())) {
                logger.info("修改同IP禁止加入标志");
                teahouse.setIpflag(client.getIpflag());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_SETTING.toString());
            }

            if (!StringUtils.isEmpty(client.getPassflag())) {
                logger.info("修改 加入茶楼是否审核");
                teahouse.setPassflag(client.getPassflag());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_SETTING.toString());
            }

            if (!StringUtils.isEmpty(client.getShareflag())) {
                logger.info("修改 是否允许茶楼分享  ");
                teahouse.setShareflag(client.getShareflag());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_SETTING.toString());
            }

            if (!StringUtils.isEmpty(client.getTicketflag())) {
                logger.info("修改 门票收取方式");
                teahouse.setTicketflag(client.getTicketflag());
                teahouse.setTicketlimit(client.getTicketlimit());
                teahouse.setTicketvalue(client.getTicketvalue());
                teahouse.setLimitscore(client.getLimitscore());
                teahouseinfoEvent.setTeaflag(DataConstants.Teaflag.UPDATE_SETTING.toString());
            }

            teahouse.setLastmodify(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));

            BMDataContext.getContext().getBean(TeahouseRepository.class).delete(teahouse.getId());
            BMDataContext.getContext().getBean(TeahouseRepository.class).save(teahouse);

            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_UPDATE, client.getUserid(), teahouseinfoEvent);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("修改茶楼信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("修改茶楼信息异常", false));

        }
    }

    /**
     * 获取茶楼信息
     *
     * @return
     */
    public void getTeahouseInfo(TeahouseClient client) {

        try {

            logger.info("getTeahouseInfo 客户端信息  client[" + client + "]");
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());
            if (null == teahouse) {
                teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());
            }

            logger.info("getTeahouseInfo系统茶楼信息 teahouse[" + teahouse + "]");

            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(client.getUserid(), "");
            if (null != playUserClient) {
                logger.info("用户 [" + playUserClient.getUsername() + "] [" + playUserClient.getGameid() + "] 进入茶楼");
                playUserClient.setGamestatus(BMDataContext.GameStatusEnum.TEAHOUSE.toString());
                playUserClient.setTeanum(client.getTeanum());
                CacheHelper.getApiUserCacheBean().put(client.getUserid(), playUserClient, playUserClient.getOrgi());
            }
            TeahouseinfoEvent teahouseinfoEvent = new TeahouseinfoEvent(teahouse);
            teahouseinfoEvent.setTableInfoList(TeahouseUtil.getAllTableinfo(teahouse.getTeanum(),client.getRoomtype()));
            teahouseinfoEvent.setRoomtype(client.getRoomtype());
            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_GET, client.getUserid(), teahouseinfoEvent);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("获取茶楼信息异常", false));
        }
    }

    /**
     * 申请加入茶楼
     *
     * @return
     */
    public void applyjoin(TeahouseClient client) {

        try {

            logger.info("applyjoin 客户端信息  client[" + client + "] ");

            //获取茶楼不包括已经销毁的茶楼
            List<Teamem> teamems = BMDataContext.getContext().getBean(TeamemRepository.class).findByGameid(client.getGameid());

            if (null != teamems) {
                int teajoinnum = Integer.parseInt(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_JOINNUM));
                if (teamems.size() >= teajoinnum ) {
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN, client.getUserid(), new NoticeEvent("加入茶楼个数不能超过[" + teajoinnum + "]个", false));
                    return;
                }
            }

            PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);

            PlayUser playUser = playUserRepository.findByGameid(client.getGameid());

            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());
            if (null == teahouse) {

                TeamemInfoEvent teamemInfoEvent = new TeamemInfoEvent();
                teamemInfoEvent.setFlag("N");
                teamemInfoEvent.setTipstr("茶楼号[" + client.getTeanum() + "]不存在!!!!");
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN, client.getUserid(), teamemInfoEvent);

                return;
            }

            TeamemRepository teamemRepository = BMDataContext.getContext().getBean(TeamemRepository.class);

            Teamem t = teamemRepository.findByGameidAndTeanum(playUser.getGameid(), teahouse.getTeanum());

            if (null != t && (DataConstants.Teapassflag.PASS.toString().equals(t.getPassflag()) || DataConstants.Teapassflag.APPLY.toString().equals(t.getPassflag()))) {
                TeamemInfoEvent teamemInfoEvent = new TeamemInfoEvent();
                teamemInfoEvent.setFlag("N");
                teamemInfoEvent.setTipstr("请勿重复加入 茶楼[" + client.getTeanum() + "]!!!!");
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN, client.getUserid(), teamemInfoEvent);
                return;

            }else if(null != t){
                teamemRepository.delete(t);
            }

            Teamem teamem = new Teamem();
            teamem.setTeanum(teahouse.getTeanum());
            teamem.setGameid(playUser.getGameid());
            teamem.setPassflag("N".equals(teahouse.getPassflag()) ? DataConstants.Teapassflag.PASS.toString() : DataConstants.Teapassflag.APPLY.toString());
            teamem.setLimitscore(teahouse.getLimitscore());
            teamem.setScore(0);//默认开始为0
            teamem.setRole(DataConstants.Tearole.NORMAL.toString());//普通成员
            teamem.setUserid(playUser.getId());
            teamem.setUserurl(StringUtils.isEmpty(playUser.getUserurl()) ? DataConstants.GAME_USER_DEFAULT_URL : playUser.getUserurl());
            teamem.setUsername(playUser.getUsername());
            teamem.setLastmodify(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));

            teamemRepository.save(teamem);

            logger.info("给管理者发送信息，自己申请无返回消息");
            for (Teamem pt : teamemRepository.findByTeanumAndRoleIn(teahouse.getTeanum(), new String[]{DataConstants.Tearole.MANAGER.toString(),DataConstants.Tearole.OWNER.toString()})) {
                TeamemInfoEvent infoEvent = new TeamemInfoEvent(teamem);
                infoEvent.setFlag("Y");
                PlayUser temp = BMDataContext.getContext().getBean(PlayUserRepository.class).findByGameid(pt.getGameid());
                if(null != temp) {
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_NOTICE, temp.getId(), new NoticeEvent("[" + playUser.getUsername() + "]申请加入茶楼", true));
                }

            }

            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN, client.getUserid(), new TeamemInfoEvent(teamem));//发给自己，告诉自己已经申请加入茶楼

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("申请加入茶楼信息异常", e);
            TeamemInfoEvent teamemInfoEvent = new TeamemInfoEvent();
            teamemInfoEvent.setFlag("N");
            teamemInfoEvent.setTipstr("申请加入茶楼号[" + client.getTeanum() + "]信息异常!!!!");
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN, client.getUserid(), teamemInfoEvent);

        }
    }

    /**
     * 茶楼成员信息修改   店小二添加/删除
     *
     * @return
     */
    public void memupdate(TeahouseClient client) {

        try {

            logger.info("memupdate 客户端信息  client[" + client + "] ");

            TeamemRepository TeamemRepository = BMDataContext.getContext().getBean(TeamemRepository.class);
            Teamem teamem = TeamemRepository.findByGameidAndTeanum(client.getGameid(), client.getTeanum());

            if (!StringUtils.isEmpty(client.getPassflag())) {

                if (DataConstants.Tearole.OWNER.toString().equals(teamem.getRole())
                        && DataConstants.Teapassflag.OUT.toString().equals(client.getPassflag())) {

                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("自己创建的茶楼无法退出", false));
                    return;
                }

                logger.info("修改申请，审批，禁止，离开茶楼 选项");
                teamem.setPassflag(client.getPassflag());
            }

            if (!StringUtils.isEmpty(client.getRole())) {
                logger.info("修改角色 设置店小二，取消店小二 选项");
                teamem.setRole(client.getRole());
            }

            teamem.setLastmodify(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));

            TeamemRepository.delete(teamem.getId());
            TeamemRepository.save(teamem);

            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_MEMUPDATE, client.getUserid(), new TeamemInfoEvent(teamem));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("茶楼成员信息修改   店小二添加/删除异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("茶楼成员信息修改   店小二添加/删除异常申请加入茶楼信息异常", false));
        }
    }

    /**
     * 获取茶楼成员信息
     *
     * @return
     */
    public void getmeminfo(TeahouseClient client) {

        try {

            logger.info("getMemInfo 客户端信息  client[" + client + "] ");

            TeamemRepository TeamemRepository = BMDataContext.getContext().getBean(TeamemRepository.class);

            Teamem teamem = TeamemRepository.findByGameidAndTeanum(client.getGameid(), client.getTeanum());

            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_GETMEMINFO, client.getUserid(), new TeamemInfoEvent(teamem));

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼成员信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("获取茶楼成员信息异常", false));

        }
    }

    /**
     * 处理茶楼上下分  scoreflag:U(up)上分，D(down)下分
     *
     * @return
     */
    public void dealscore(TeahouseClient client) {

        try {

            logger.info("dealscore 客户端信息  client[" + client + "] ");

            TeamemRepository teamemRepository = BMDataContext.getContext().getBean(TeamemRepository.class);

            Teascorerecord teascorerecord = new Teascorerecord();

            Teamem teamem = teamemRepository.findByGameidAndTeanum(client.getGameid(), client.getTeanum());

            UKTools.copyProperties(teamem, teascorerecord);

            teascorerecord.setTopup(client.getScore());

            teascorerecord.setTopupbegin(teamem.getScore());

            teamem.setScore(teamem.getScore() + client.getScore());//上下分

            teascorerecord.setTopupend(teamem.getScore());

            teascorerecord.setOptid(BMDataContext.getContext().getBean(PlayUserRepository.class).findById(client.getUserid()).getGameid());

            teamem.setLimitscore(client.getLimitscore());//设置最低信誉分

            BMDataContext.getContext().getBean(TeascorerecordRepository.class).save(teascorerecord);
            teamemRepository.save(teamem);

            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_DEALSCORE, client.getUserid(), new NoticeEvent("处理茶楼上下分成功", true));

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("处理茶楼上下分异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("处理茶楼上下分异常", false));

        }
    }

    /**
     * 获取茶楼信息
     *
     * @return
     */
    public void getTableInfo(TeahouseClient client) {

        try {

            logger.info("getTableInfo 客户端信息  client[" + client + "]");
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());
            if (null == teahouse) {
                teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());
            }

            logger.info("getTeahouseInfo系统茶楼信息 teahouse[" + teahouse + "]");

            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(client.getUserid(), "");
            if (null != playUserClient) {
                logger.info("用户 [" + playUserClient.getUsername() + "] [" + playUserClient.getGameid() + "] 进入茶楼");
                playUserClient.setGamestatus(BMDataContext.GameStatusEnum.TEAHOUSE.toString());
                playUserClient.setTeanum(client.getTeanum());
                CacheHelper.getApiUserCacheBean().put(client.getUserid(), playUserClient, playUserClient.getOrgi());
            }
            TeahouseinfoEvent teahouseinfoEvent = new TeahouseinfoEvent(teahouse);
            List<TableInfo> tableInfos = new ArrayList<TableInfo>();
            tableInfos.add(TeahouseUtil.getTableinfo(client.getTeanum(), client.getTablenum(),client.getRoomtype()));
            teahouseinfoEvent.setTableInfoList(tableInfos);
            teahouseinfoEvent.setRoomtype(client.getRoomtype());
            logger.info("获取桌子信息返回  teahouseinfoEvent[" + teahouseinfoEvent + "]");
            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_GETTABLEINFO, client.getUserid(), teahouseinfoEvent);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("获取茶楼信息异常", false));
        }
    }

    /**
     * 强制解散茶楼里面的房间
     *
     * @return
     */
    public void teadissroom(TeahouseClient client) {

        try {
            logger.info("getTableInfo 客户端信息  TeahouseClient[" + client + "]");
            GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();

            Teamem teamem = BMDataContext.getContext().getBean(TeamemRepository.class).findByUseridAndTeanum(client.getUserid(), client.getTeanum());

            if (null == teamem) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("用户未登录", false));
                return;
            }
            if (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                    !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole())) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("非管理者不能解散茶楼中的房间", false));
                return;
            }
            GameRoom gameRoom = gameRoomCache.getGameroomByTea(client.getTeanum(), client.getTablenum(),client.getRoomtype());
            if (null != gameRoom) {

                GameUtils.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom, MileGameEvent.CLEARING.toString(), 0);

                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_SUCCESS, client.getUserid(), new NoticeEvent("解散成功", true));

            } else {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("未找对对应房间", false));
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("解散茶楼中的房间异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("解散茶楼中的房间异常", false));
        }
    }

    /**
     * 基金转入
     *
     * @return
     */
    public void shiftfund(TeahouseClient client) {

        try {
            logger.info("shiftfund 客户端信息  TeahouseClient[" + client + "]");
            GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();

            Teamem teamem = BMDataContext.getContext().getBean(TeamemRepository.class).findByUseridAndTeanum(client.getUserid(), client.getTeanum());

            if (null == teamem) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("用户未登录", false));
                return;
            }

            if (null == client.getFund() || client.getFund() < 0) {
                TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("基金不能为负数", false));
                return;
            }

            Teafundrecord teafundrecord = new Teafundrecord();

            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(client.getTeanum());

            teafundrecord.setTeatopupbegin(teahouse.getFund());

            if (null == teahouse.getFund()) {
                teahouse.setFund(0);
            }

            if (null == client.getFund()) {
                client.setFund(0);
            }

            teahouse.setFund(teahouse.getFund() + client.getFund());

            teafundrecord.setTeatopupend(teahouse.getFund());

            teafundrecord.setTeanum(teahouse.getTeanum());

            BMDataContext.getContext().getBean(TeahouseRepository.class).save(teahouse);

            PlayUser playUser = BMDataContext.getContext().getBean(PlayUserRepository.class).findById(client.getUserid());

            if (playUser.getDiamonds() < client.getFund()) {
                TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_SHIFTFUND, client.getUserid(), new NoticeEvent("转入失败:转入数量不能大于自身的钻石", false));
                return;
            }

            teafundrecord.setTopupbegin(playUser.getDiamonds());
            teafundrecord.setTopup(client.getFund());

            playUser.setDiamonds(playUser.getDiamonds() - client.getFund());

            teafundrecord.setTopupend(playUser.getDiamonds());

            teafundrecord.setGameid(playUser.getGameid());
            teafundrecord.setUsername(playUser.getUsername());
            teafundrecord.setUserid(playUser.getId());
            teafundrecord.setUserurl(playUser.getUserurl());

            BMDataContext.getContext().getBean(PlayUserRepository.class).save(playUser);

            MaziScoreUtil.synchronizationUsercache(playUser);

            BMDataContext.getContext().getBean(TeafundrecordRepository.class).save(teafundrecord);

            TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_SHIFTFUND, client.getUserid(), new NoticeEvent("转入成功", true));

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("基金转入 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, client.getUserid(), new NoticeEvent("基金转入 异常", false));
        }
    }

    /**
     * 强制解散茶楼里面的房间
     *
     * @return
     */
    public void kickplayer(BeiMiClient beiMiClient, String data) {

        try {

            String token = beiMiClient.getToken();

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);

            if (!StringUtils.isBlank(token)) {
                Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

                Teamem teamem = BMDataContext.getContext().getBean(TeamemRepository.class).findByUseridAndTeanum(userToken.getUserid(), teahouseClient.getTeanum());

                if (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                        !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole())) {
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("非管理者不能踢人", false));
                    return;
                }

                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(teahouseClient.getKickplayer(), userToken.getOrgi());

                GameRoom gameRoom = null;
                if (!org.apache.commons.lang.StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
                    gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomid, userToken.getOrgi());

                    if (MileGameEnum.PLAY.toString().equals(gameRoom.getStatus())) {
                        TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GAME_EVENT_KICKPLAYER, beiMiClient.getUserid(), new NoticeEvent("玩家已在牌局，无法踢出", false));
                        return;
                    }
                }
                if (userToken != null) {
                    //GameUtils.updatePlayerClientStatus(beiMiClient.getUserid(), beiMiClient.getOrgi(), BMDataContext.PlayerTypeEnum.LEAVE.toString());
                    PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(teahouseClient.getKickplayer(), BMDataContext.SYSTEM_ORGI);
                    if(null == playUserClient){
                        playUserClient = BMDataContext.getContext().getBean(PlayUserClientRepository.class).findById(teahouseClient.getKickplayer());

                    }

                    if(null != playUserClient) {
                        playUserClient.setPlayertype(BMDataContext.PlayerTypeEnum.LEAVE.toString());
                        CacheHelper.getApiUserCacheBean().put(playUserClient.getId(),playUserClient,playUserClient.getOrgi());
                        BMDataContext.getRoomEngine().leaveRoom(playUserClient, playUserClient.getOrgi());

                    }
                }

                if (null != gameRoom) {
                    ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_LEAVE, new LeaveEvent(teahouseClient.getKickplayer()), gameRoom);
                }

                ActionTaskUtils.sendEvent(DataConstants.GAME_EVENT_LEAVE, teahouseClient.getKickplayer(), new LeaveEvent(teahouseClient.getKickplayer()));

                TeahouseActionTaskUtils.sendEventByUserid(DataConstants.GAME_EVENT_KICKPLAYER, userToken.getUserid(), new NoticeEvent("剔除成功", true));
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("剔出玩家异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GAME_EVENT_KICKPLAYER, beiMiClient.getUserid(), new NoticeEvent("剔出玩家异常", false));
        }
    }

    public void sitdown(Token userToken, TeahouseClient teahouseClient) {
        boolean joinRoom = false;
        GamePlayway gamePlayway = null;
        PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());

        TableInfo tableInfo = new TableInfo();

        GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();

        if (playUser != null) {
            GameRoom gameRoom = null;

            //得到他之前加入房间的ID。非本次加入的ID
            String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());

            if (!StringUtils.isBlank(roomid)) {//恢复上次进入时的ID
                gameRoom = (GameRoom) gameRoomCache.getCacheObject(roomid, playUser.getOrgi());
                tableInfo.setSitflag(BMDataContext.SearchRoomResultType.EXIST.toString());
                if (null != gameRoom) {
                    if (null == gameRoom.getTeanum() || !gameRoom.getTeanum().equals(teahouseClient.getTeanum())) {
                        logger.info("非茶楼人员不能进入房间");
                        tableInfo.setSitflag(BMDataContext.SearchRoomResultType.NOTEA.toString());
                    }
                }
            } else {
                gameRoom = gameRoomCache.getGameroomByTea(teahouseClient.getTeanum(), teahouseClient.getTablenum(),teahouseClient.getRoomtype());
                //tableInfo.setSitflag(BMDataContext.SearchRoomResultType.OK.toString());
            }

            Teamem teamem = BMDataContext.getContext().getBean(TeamemRepository.class).findByUseridAndTeanum(userToken.getUserid(), teahouseClient.getTeanum());



            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(teahouseClient.getTeanum());

            if(null == teahouse || teahouse.getFund()<=0){
                tableInfo.setSitflag(BMDataContext.SearchRoomResultType.NOFUND.toString());

            }

            if(null == teahouse.getLimitscore()){
                teahouse.setLimitscore(0);
            }

            if (null == teamem) {
                tableInfo.setSitflag(BMDataContext.SearchRoomResultType.NOTEA.toString());//非茶楼人员
            } else if (!DataConstants.Teapassflag.PASS.toString().equals(teamem.getPassflag())) {
                tableInfo.setSitflag(BMDataContext.SearchRoomResultType.NOTPASS.toString());//未审批通过人员
            } else if (DataConstants.Roomtype.TEACOIN.toString().equals(
                    teahouseClient.getRoomtype() ) && teahouse.getLimitscore() > teamem.getScore() ) {
                tableInfo.setSitflag(BMDataContext.SearchRoomResultType.NOSCORE.toString());//茶楼分不够
            }


            if (gameRoom != null && StringUtils.isEmpty(tableInfo.getSitflag()) ) {
                /**
                 * 将玩家加入到 房间 中来 ， 加入的时候需要处理当前的 房间 已满员或未满员，如果满员，需要检查是否允许围观
                 */
                UKTools.copyProperties(gameRoom,tableInfo);
                gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(gameRoom.getPlayway(), gameRoom.getOrgi());
                List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

                String ipflag = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum()).getIpflag();

                if ("Y".equals(ipflag)) {
                    for (PlayUserClient playUserClient : playerList) {
                        logger.info("同IP的人禁止加入 playUser.getIp()[" + playUser.getIp() + "] ["
                                + playUser.getGameid() + "] playUserClient.getIp()[" + playUserClient.getIp() + "] [" + playUserClient.getGameid() + "]");
                        if (null != playUser.getIp() && playUser.getIp().equals(playUserClient.getIp())) {
                            tableInfo.setSitflag(BMDataContext.SearchRoomResultType.IPFORBID.toString());
                            logger.info("IP地址相同，直接退出");
                            break;
                        }
                    }
                } else {
                    logger.info("茶楼为开启同IP禁止加入 ipflag[" + ipflag + "]");
                }

                if(StringUtils.isEmpty(tableInfo.getSitflag())) {
                    if (playerList.size() < gamePlayway.getPlayers()) {
                        BMDataContext.getRoomEngine().joinRoom(gameRoom, playUser, playerList);
                        joinRoom = true;
                        tableInfo.setSitflag(BMDataContext.SearchRoomResultType.OK.toString());
                    } else if (isExsit(playerList, playUser)) {
                        joinRoom = true;
                        tableInfo.setSitflag(BMDataContext.SearchRoomResultType.OK.toString());
                    } else if (playerList.size() == gamePlayway.getPlayers()) {
                        tableInfo.setSitflag(BMDataContext.SearchRoomResultType.FULL.toString());
                    }
                }


                tableInfo.setRoomtype(teahouseClient.getRoomtype());

                /**
                 * 获取的玩法，将玩法数据发送给当前请求的玩家
                 */
            } else {
                UKTools.copyProperties(teahouseClient,tableInfo);
                if (StringUtils.isEmpty(tableInfo.getSitflag())) {
                    tableInfo.setSitflag(BMDataContext.SearchRoomResultType.CREATE.toString());//新建
                }
            }

        }
        logger.info("坐下信息TableInfo ["+tableInfo+"]");
        TeahouseActionTaskUtils.sendEventByUserid(DataConstants.TEAHOUSE_EVENT_SITDOWN, teahouseClient.getUserid(), tableInfo);
    }

    private boolean isExsit(List<PlayUserClient> playUserClients, PlayUserClient playUserClient) {
        if (null == playUserClients || playUserClients.isEmpty())
            return false;
        for (PlayUserClient p : playUserClients) {
            if (p.getId().equals(playUserClient.getId())) {
                return true;
            }
        }
        return false;
    }

}
