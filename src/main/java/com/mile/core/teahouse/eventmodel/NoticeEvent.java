package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

public class NoticeEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String context;//内容

	private boolean flag;//true , false


	public NoticeEvent(String context) {
		this.context = context;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public NoticeEvent(String context, boolean flag) {
		this.context = context;
		this.flag = flag;
	}
}



