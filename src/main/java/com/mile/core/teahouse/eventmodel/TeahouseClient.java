package com.mile.core.teahouse.eventmodel;

import com.mile.common.entity.BaseEntity;

import java.util.Map;

/**
 * 当前Session的客户端信息
 */
public class TeahouseClient extends BaseEntity {


    private static final long serialVersionUID = -1L;


    private String id;

    private String userid;

    private String token;

    private String gameid;

    private String name;//茶馆名称

    private String teanum;//茶楼号

    private String ipflag;

    private String shareflag;

    private String  state;//状态  0.正常运营 1.打烊， 2.销毁

    private String usernotice;//公告

    private Integer fund=0;


    private Map<String, String> extparams;//房卡模式下的自定义参数


    private String playwayid;//玩法ID


    private String closenotice;//歇业公告

    private String roomtype;






    private String passflag;//是否需要审核  Y/N   状态

    private String role;//角色

    private int tablenum;


    private int limitscore;//茶楼要求的最低信誉分

    //门票费用类型  P1:大赢家百分比  P2:所有赢家百分比  N1:扣除固定大赢家分数 N2:扣除所有赢家固定分数
    private String ticketflag;

    private double ticketvalue;//门票的值  百分比 或者 值

    private int ticketlimit;//门票下线

    private int score;//分数  上下信誉分

    private String scoreflag;//上下分标志  上分U(up)  下分D(down)


    private Integer page;

    private Integer size;


    private String kickplayer;//被踢者ID






    public TeahouseClient() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpflag() {
        return ipflag;
    }

    public void setIpflag(String ipflag) {
        this.ipflag = ipflag;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUsernotice() {
        return usernotice;
    }

    public void setUsernotice(String usernotice) {
        this.usernotice = usernotice;
    }


    public Map<String, String> getExtparams() {
        return extparams;
    }

    public void setExtparams(Map<String, String> extparams) {
        this.extparams = extparams;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPlaywayid() {
        return playwayid;
    }

    public void setPlaywayid(String playwayid) {
        this.playwayid = playwayid;
    }

    public String getPassflag() {
        return passflag;
    }

    public void setPassflag(String passflag) {
        this.passflag = passflag;
    }

    public int getLimitscore() {
        return limitscore;
    }

    public void setLimitscore(int limitscore) {
        this.limitscore = limitscore;
    }

    public String getTicketflag() {
        return ticketflag;
    }

    public void setTicketflag(String ticketflag) {
        this.ticketflag = ticketflag;
    }

    public double getTicketvalue() {
        return ticketvalue;
    }

    public void setTicketvalue(double ticketvalue) {
        this.ticketvalue = ticketvalue;
    }

    public int getTicketlimit() {
        return ticketlimit;
    }

    public void setTicketlimit(int ticketlimit) {
        this.ticketlimit = ticketlimit;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getScoreflag() {
        return scoreflag;
    }

    public void setScoreflag(String scoreflag) {
        this.scoreflag = scoreflag;
    }

    public String getShareflag() {
        return shareflag;
    }

    public void setShareflag(String shareflag) {
        this.shareflag = shareflag;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getTablenum() {
        return tablenum;
    }

    public void setTablenum(int tablenum) {
        this.tablenum = tablenum;
    }

    public Integer getFund() {
        return fund;
    }

    public void setFund(Integer fund) {
        this.fund = fund;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getKickplayer() {
        return kickplayer;
    }

    public void setKickplayer(String kickplayer) {
        this.kickplayer = kickplayer;
    }

    public String getClosenotice() {
        return closenotice;
    }

    public void setClosenotice(String closenotice) {
        this.closenotice = closenotice;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype;
    }
}
