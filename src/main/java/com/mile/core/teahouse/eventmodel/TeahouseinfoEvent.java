package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.teahouse.entity.Teahouse;

import java.util.List;

public class TeahouseinfoEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private Teahouse teahouse;

	private List<TableInfo> tableInfoList;

	private String roomtype;

	private String teaflag;


	public TeahouseinfoEvent() {}

	public TeahouseinfoEvent(Teahouse teahouse) {
		this.teahouse = teahouse;
	}

	public Teahouse getTeahouse() {
		return teahouse;
	}

	public void setTeahouse(Teahouse teahouse) {
		this.teahouse = teahouse;
	}

	public List<TableInfo> getTableInfoList() {
		return tableInfoList;
	}

	public void setTableInfoList(List<TableInfo> tableInfoList) {
		this.tableInfoList = tableInfoList;
	}

	public String getTeaflag() {
		return teaflag;
	}

	public void setTeaflag(String teaflag) {
		this.teaflag = teaflag;
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
}
