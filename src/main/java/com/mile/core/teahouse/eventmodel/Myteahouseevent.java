package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.entity.Teamem;

public class Myteahouseevent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private Teahouse teahouse;

	private Teamem teamem;

	public Myteahouseevent(Teahouse teahouse, Teamem teamem) {
		this.teahouse = teahouse;
		this.teamem = teamem;
	}

	public Teahouse getTeahouse() {
		return teahouse;
	}

	public void setTeahouse(Teahouse teahouse) {
		this.teahouse = teahouse;
	}

	public Teamem getTeamem() {
		return teamem;
	}

	public void setTeamem(Teamem teamem) {
		this.teamem = teamem;
	}
}
