package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.teahouse.entity.Teamem;

import java.util.List;

public class TeamemInfoEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String flag;//Y:加入成功， N:加入失败

	private String tipstr;//提示语句

	private Teamem teamem;

	private List<Teamem> teamemList;

	public TeamemInfoEvent() {
	}

	public TeamemInfoEvent(Teamem teamem) {
		this.teamem = teamem;
	}

	public TeamemInfoEvent(List<Teamem> teamemList) {
		this.teamemList = teamemList;
	}

	public Teamem getTeamem() {
		return teamem;
	}

	public void setTeamem(Teamem teamem) {
		this.teamem = teamem;
	}

	public List<Teamem> getTeamemList() {
		return teamemList;
	}

	public void setTeamemList(List<Teamem> teamemList) {
		this.teamemList = teamemList;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getTipstr() {
		return tipstr;
	}

	public void setTipstr(String tipstr) {
		this.tipstr = tipstr;
	}
}
