package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

public class UserInfo extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String userid;

	private String gameid;

	private String username;

	private String userurl;

	private String ip;

	// 纬度
	public  Double latitude = 0.0;
	// 经度
	public  Double longitude = 0.0;

	public  String locationdesc = "";



	public UserInfo() {

	}

	public UserInfo(String userid, String gameid, String username, String userurl, String ip) {
		this.userid = userid;
		this.gameid = gameid;
		this.username = username;
		this.userurl = userurl;
		this.ip = ip;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserurl() {
		return userurl;
	}

	public void setUserurl(String userurl) {
		this.userurl = userurl;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getLocationdesc() {
		return locationdesc;
	}

	public void setLocationdesc(String locationdesc) {
		this.locationdesc = locationdesc;
	}
}
