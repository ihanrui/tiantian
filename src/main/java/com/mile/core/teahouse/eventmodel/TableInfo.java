package com.mile.core.teahouse.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

import java.util.List;

public class TableInfo extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private Integer num;//人数

	private Integer tablenum;//桌号

	private List<UserInfo> userInfoList;//人员信息

	private String roomid;//房间号

	private String teanum;//茶楼号

	private String roomtype;

	private String sitflag;// BMDataContext.SearchRoomResultType


	private Integer currentnum;

	private String begindate ;



	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getTablenum() {
		return tablenum;
	}

	public void setTablenum(Integer tablenum) {
		this.tablenum = tablenum;
	}

	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	public void setUserInfoList(List<UserInfo> userInfoList) {
		this.userInfoList = userInfoList;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	public String getTeanum() {
		return teanum;
	}

	public void setTeanum(String teanum) {
		this.teanum = teanum;
	}

	public String getSitflag() {
		return sitflag;
	}

	public void setSitflag(String sitflag) {
		this.sitflag = sitflag;
	}

	public Integer getCurrentnum() {
		return currentnum;
	}

	public void setCurrentnum(Integer currentnum) {
		this.currentnum = currentnum;
	}

	public String getBegindate() {
		return begindate;
	}

	public void setBegindate(String begindate) {
		this.begindate = begindate;
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
}
