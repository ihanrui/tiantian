package com.mile.core.teahouse.util;

import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.GameRoomCache;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.eventmodel.TableInfo;
import com.mile.core.teahouse.eventmodel.TeahouseinfoEvent;
import com.mile.core.teahouse.eventmodel.UserInfo;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.util.RandomCharUtil;
import com.mile.util.UKTools;
import com.mile.web.model.BeiMiDic;
import com.mile.web.model.SysDic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by michael on 3/18/18.
 */
public class TeahouseUtil {

    private static final Logger logger = LoggerFactory.getLogger(TeahouseUtil.class);

    /**
     * 计算本手牌的分数
     *
     * @param bytes
     * @return
     */
    public static String getRoominfo(Map extparams,String playwaycode) {

        try {

            StringBuffer s = new StringBuffer();
            s.append("信息：");

            if(DataConstants.GAME_PLAYWAY_MAZI.equals(playwaycode)) {
                s.append(DataConstants.Coinexpend.CREATER.toString().equals(extparams.get("coinexpend")) ? "只扣老板钻石" : "AA支付钻石");

                s.append("," + MaziTipsUtil.getPlaywaydesc(extparams));

                if ("999999".equals(extparams.get("numofgames"))) {
                    s.append(",一局制。");
                } else {
                    s.append(",每局").append(extparams.get("numofgames")).append("盘。");
                }
            }else if(DataConstants.GAME_PLAYWAY_PINSHI.equals(playwaycode)) {
                s.append(Integer.parseInt(RoomUtil.getPinshiCreatevalue(extparams, PinshiDataConstants.CONT_ROOM_PLAYERNUMBER))+"人场");
                s.append(","+((DataConstants.Coinexpend.CREATER.toString().equals(extparams.get(PinshiDataConstants.CONT_ROOM_TIPS))) ? "只扣老板钻石" : "AA支付钻石"));

                s.append(","+Integer.parseInt(RoomUtil.getPinshiCreatevalue(extparams, PinshiDataConstants.CONT_ROOM_GAMENUMBERS))+"局制");
            }
            logger.info("公告信息房间信息为:[" + s.toString() + "]");
            return s.toString();
        }catch (Exception e){
            logger.error("获取茶楼房间信息失败",e);
            return  null;
        }
    }

    /**
     * 计算本手牌的分数
     *
     * @param bytes
     * @return
     */
    public static String getTeanum() {

        String teanum = RandomCharUtil.getRandomNumberChar(7);

        //茶楼号
        for (int i = 0; i < 50; i++) {
            if (null == BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(teanum)) {
                break;
            } else {
                logger.info("茶楼号重复 teanum[" + teanum + "]");
                teanum = RandomCharUtil.getRandomNumberChar(7);
            }

        }

        return teanum;
    }

    public static Integer getIntConstantsByName(String name) {
        if (null == getConstantsByName(name))
            return 0;

        return Integer.parseInt(getConstantsByName(name));
    }

    public static String getConstantsByName(String name) {
        try {

            return getConststr(name,DataConstants.BEIMI_SYSTEM_GAME_CONST);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取游戏常量[" + name + "]失败", e);
            return null;
        }
    }








    public static String getConststr(String name,String conststr) {
        try {
            List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(conststr);
            if (null == sysDicList)
                return null;

            for (SysDic sysDic : sysDicList) {
                if (name.equals(sysDic.getName()))
                    return sysDic.getCode();
            }

            logger.info("未找到常量[" + name + "]");
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取常量[" + name + "]失败", e);
            return null;
        }
    }






    public static TeahouseinfoEvent putTeahouseinfo(GameRoom gameRoom) {
        TeahouseinfoEvent teahouseinfoEvent = (TeahouseinfoEvent) CacheHelper.getTeahouseCacheBean().getCacheObject(gameRoom.getTeanum(), gameRoom.getOrgi());
        if (null == teahouseinfoEvent) {
            teahouseinfoEvent = new TeahouseinfoEvent();
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());
            teahouseinfoEvent.setTeahouse(teahouse);
        }

        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        List<TableInfo> tableInfos = teahouseinfoEvent.getTableInfoList();

        TableInfo tableInfo = null;
        if (null == tableInfos) {
            tableInfo = new TableInfo();
            UKTools.copyProperties(gameRoom, tableInfo);
        } else {
            boolean exsit = false;
            for (TableInfo t : tableInfos) {
                if (t.getTeanum().equals(gameRoom.getTeanum()) && t.getTablenum() == gameRoom.getTablenum()) {
                    tableInfo = t;
                    tableInfos.remove(t);
                }
            }
        }

        tableInfos.add(tableInfo);
        tableInfo.setNum(null == playerList ? 0 : playerList.size());
        tableInfo.setUserInfoList(formatPlayer2Userinfo(board.getPlayers()));

        CacheHelper.getTeahouseCacheBean().put(gameRoom.getTeanum(), teahouseinfoEvent, gameRoom.getOrgi());
        return teahouseinfoEvent;

    }

    public static List<UserInfo> formatPlayer2Userinfo(Player[] players) {
        if (ArrayUtils.isEmpty(players)) {
            logger.info("Player[] players formatPlayer2Userinfo player 为 null");
            return null;
        }

        List<UserInfo> infoList = new LinkedList<UserInfo>();

        for (Player p : players) {
            UserInfo info = new UserInfo();
            UKTools.copyProperties(p, info);
            infoList.add(info);
        }

        return infoList;
    }

    public static List<UserInfo> formatPlayer2Userinfo(List<PlayUserClient> players) {
        if (null == players || players.isEmpty()) {
            logger.info("List<PlayUserClient> players formatPlayer2Userinfo player 为 null");
            return null;
        }

        List<UserInfo> infoList = new LinkedList<UserInfo>();

        for (PlayUserClient p : players) {
            UserInfo info = new UserInfo();
            UKTools.copyProperties(p, info);
            info.setUserid(p.getId());
            infoList.add(info);
        }

        return infoList;
    }

    public static TeahouseinfoEvent delTeahouseinfo(GameRoom gameRoom) {
        TeahouseinfoEvent teahouseinfoEvent = (TeahouseinfoEvent) CacheHelper.getTeahouseCacheBean().getCacheObject(gameRoom.getTeanum(), gameRoom.getOrgi());
        if (null == teahouseinfoEvent) {
            teahouseinfoEvent = new TeahouseinfoEvent();
            Teahouse teahouse = BMDataContext.getContext().getBean(TeahouseRepository.class).findByTeanum(gameRoom.getTeanum());
            teahouseinfoEvent.setTeahouse(teahouse);
        }

        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        List<TableInfo> tableInfos = teahouseinfoEvent.getTableInfoList();

        TableInfo tableInfo = null;
        if (null == tableInfos) {
            tableInfo = new TableInfo();
            UKTools.copyProperties(gameRoom, tableInfo);

        } else {
            boolean exsit = false;
            for (TableInfo t : tableInfos) {
                if (t.getTeanum().equals(gameRoom.getTeanum()) && t.getTablenum() == gameRoom.getTablenum()) {
                    tableInfo = t;
                    tableInfos.remove(t);
                }
            }
        }

        tableInfos.add(tableInfo);
        tableInfo.setNum(null == playerList ? 0 : playerList.size());
        tableInfo.setUserInfoList(formatPlayer2Userinfo(board.getPlayers()));

        CacheHelper.getTeahouseCacheBean().put(gameRoom.getTeanum(), teahouseinfoEvent, gameRoom.getOrgi());
        return teahouseinfoEvent;

    }

    public static List<TableInfo> getAllTableinfo(String teanum, String roomtype) {
        List<TableInfo> tableInfos = new ArrayList<TableInfo>();
        try {
            GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();
            List<GameRoom> gameRooms = gameRoomCache.getGameroomObject(teanum, roomtype);
            if (null != gameRooms && !gameRooms.isEmpty()) {
                for (GameRoom gameRoom : gameRooms) {
                    tableInfos.add(getTableinfo(gameRoom));
                }
            }

        } catch (Exception e) {
            logger.error("获取桌子信息失败[" + teanum + "]", e);
        }
        return tableInfos;
    }

    public static TableInfo getTableinfo(String teanum, int tablenum, String roomtype) {
        TableInfo tableInfo = new TableInfo();
        try {
            GameRoomCache gameRoomCache = (GameRoomCache) CacheHelper.getGameRoomCacheBean();
            GameRoom gameRoom = gameRoomCache.getGameroomObject(teanum, tablenum, roomtype);
            if (null == gameRoom) {
                return null;
            }
            return getTableinfo(gameRoom);

        } catch (Exception e) {
            logger.error("获取桌子信息失败[" + teanum + "]", e);
        }
        return tableInfo;
    }

    public static TableInfo getTableinfo(GameRoom gameRoom) {

        if (null == gameRoom)
            return null;

        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        TableInfo tableInfo = new TableInfo();
        tableInfo.setUserInfoList(TeahouseUtil.formatPlayer2Userinfo(playerList));
        UKTools.copyProperties(gameRoom, tableInfo);

        tableInfo.setNum(playerList.size());
        tableInfo.setBegindate(gameRoom.getCreatetime());

        return tableInfo;

    }



    public static int getNums(Map extparams,String playwaycode) {
        try {

            if (null == extparams || StringUtils.isEmpty(playwaycode))
                return 0;

            if (DataConstants.GAME_PLAYWAY_MAZI.equals(playwaycode)) {
                return 4;
            } else if (DataConstants.GAME_PLAYWAY_PINSHI.equals(playwaycode)) {
                return Integer.parseInt(RoomUtil.getPinshiCreatevalue(extparams, PinshiDataConstants.CONT_ROOM_PLAYERNUMBER));
            }

            return 0;
        }catch (Exception e){
            logger.error("获取玩法人数失败",e);
            return 0;
        }

    }

}