package com.mile.core.teahouse.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.Message;
import com.mile.core.teahouse.entity.Teamem;
import com.mile.core.teahouse.jpa.TeamemRepository;
import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.game.BeiMiClient;
import com.mile.core.entity.PlayUserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

public class TeahouseActionTaskUtils {
    private static final Logger logger = LoggerFactory.getLogger(TeahouseActionTaskUtils.class);

    public static void sendCommandEvent(String event, String userid, Message message) {
        message.setCommand(event);
        BeiMiClient client = NettyClients.getInstance().getClient(userid);
        if (client != null) {
            if (ActionTaskUtils.online(userid, client.getOrgi())) {
                client.getClient().sendEvent(event, message);
            }
        }
    }

    public static void sendTeahouseEvent(String event, String teanum, Message message) {
        try {
            message.setCommand(event);

            List<Teamem> teamems = BMDataContext.getContext().getBean(TeamemRepository.class).findByTeanum(teanum);

            if (null != teamems && !teamems.isEmpty()) {
                for (Teamem teamem : teamems) {
                    if (null != teamem && !StringUtils.isEmpty(teamem.getUserid())) {
                        if (ActionTaskUtils.online(teamem.getUserid(), null)) {
                            PlayUserClient playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(teamem.getUserid(), "");
                            logger.info("发送茶楼事件 用户 [" + playUserClient.getUsername() + "] [" + playUserClient.getGameid() + "] 状态[" + playUserClient.getGamestatus() + "] teanum[" + teanum + "] playUserClient.getTeanum()[" + playUserClient.getTeanum() + "]");
                            if (!StringUtils.isEmpty(playUserClient.getGamestatus()) &&
                                    playUserClient.getGamestatus().equals(BMDataContext.GameStatusEnum.TEAHOUSE.toString())
                                    && null != teanum && teanum.equals(playUserClient.getTeanum())) {
                                sendEventByUserid(event, playUserClient.getId(), message);
                            }
                        } else {
                            logger.info("用户 [" + teamem.getUsername() + "] [" + teamem.getGameid() + "] 未上线");
                        }
                    } else {
                        logger.info("发送茶楼事件 userid为空  [" + teamem.getUsername() + "][" + teamem.getGameid() + "]");
                    }

                }
            } else {
                logger.info("发送茶楼事件 event[" + event + "]   teanum[" + teanum + "]  为空!!!!!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("发送茶楼消息异常 ", e);

        }
    }

    public static void sendEventByUserid(String event, String userid, Message message) {
        message.setCommand(event);
        BeiMiClient client = NettyClients.getInstance().getClient(userid);
        if (client != null) {
            if (ActionTaskUtils.online(userid, client.getOrgi())) {
                client.getClient().sendEvent(DataConstants.TEAHOUSE_MESSAGE_EVENT, message);
            }
        }
    }

}


