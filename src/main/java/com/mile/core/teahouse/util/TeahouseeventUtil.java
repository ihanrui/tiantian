package com.mile.core.teahouse.util;

import com.mile.common.cache.CacheHelper;
import com.mile.core.DataConstants;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.teahouse.eventmodel.TableInfo;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by michael on 2018/6/11.
 */
public class TeahouseeventUtil {
    private static final Logger logger = LoggerFactory.getLogger(TeahouseeventUtil.class);


    public static void sendTablechange(GameRoom gameRoom) {



        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());

        if (!org.apache.commons.lang3.StringUtils.isEmpty(gameRoom.getTeanum())) {
            TableInfo tableInfo = new TableInfo();
            UKTools.copyProperties(gameRoom, tableInfo);

            tableInfo.setUserInfoList(TeahouseUtil.formatPlayer2Userinfo(playerList));
            tableInfo.setNum(playerList.size());

            TeahouseActionTaskUtils.sendTeahouseEvent(DataConstants.TEAHOUSE_EVENT_TABLECHANGE, gameRoom.getTeanum(), tableInfo);
        } else {
            logger.info("未在茶楼，不用发送事件");
        }
    }
}
