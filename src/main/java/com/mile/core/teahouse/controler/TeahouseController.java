package com.mile.core.teahouse.controler;

import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/apps/maziplatform")
public class TeahouseController extends Handler{


	@Autowired
	private TeahouseRepository teahouseRes ;


	@Autowired
	private PlayUserRepository playUserRes;

	@Autowired
	private GamePlaywayRepository playwayRes;

	@RequestMapping({"/teahouse"})
	@Menu(type="platform", subtype="teahouse")
	public ModelAndView gameusers(ModelMap map , HttpServletRequest request , @Valid String id){
		Page<Teahouse> teahouseList = teahouseRes.findAll(new PageRequest(super.getP(request), super.getPs(request) , new Sort(Sort.Direction.DESC, "lastmodify"))) ;

		map.addAttribute("teahouseList", teahouseList) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/teahouse/index"));
	}



}
