package com.mile.core.teahouse.controler;

import com.mile.core.DataConstants;
import com.mile.core.entity.Token;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.core.record.entity.Teafundrecord;
import com.mile.core.record.entity.Teascorerecord;
import com.mile.core.record.jpa.TeafundrecordRepository;
import com.mile.core.record.jpa.TeascorerecordRepository;
import com.mile.core.teahouse.entity.Teahouse;
import com.mile.core.teahouse.entity.Teamem;
import com.mile.core.teahouse.entity.Teareport;
import com.mile.core.teahouse.eventmodel.Myteahouseevent;
import com.mile.core.teahouse.jpa.TeahouseRepository;
import com.mile.core.teahouse.jpa.TeamemRepository;
import com.mile.core.teahouse.jpa.TeareportRepository;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = {"/teahouse"})
public class TeahouseGameController extends Handler {
    private static final Logger logger = LoggerFactory.getLogger(TeahouseGameController.class);

    @Autowired
    private TeamemRepository teamemRepository;

    @Autowired
    private TeahouseRepository teahouseRepository;

    @Autowired
    private TeafundrecordRepository teafundrecordRepository;

    @Autowired
    private TeascorerecordRepository teascorerecordRepository;

    @Autowired
    private TeareportRepository teareportRepository;

    @Autowired
    private TokenRepository tokenRes;

    /**
     * 获取茶楼列表
     *
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/getmyteahouses"}, method = RequestMethod.POST)
    public ResponseEntity getmyteahouses(HttpServletRequest request, HttpServletResponse response,
                                         String gameid) {

        try {

            List<Myteahouseevent> teahouses = new ArrayList<Myteahouseevent>();

            List<Teamem> teamems = teamemRepository.findByGameid(gameid);

            for (Teamem teamem : teamems) {

                if (DataConstants.Teapassflag.APPLY.toString().equals(teamem.getPassflag()) || DataConstants.Teapassflag.PASS.toString().equals(teamem.getPassflag())) {
                    Teahouse teahouse = teahouseRepository.findByTeanum(teamem.getTeanum());
                    if (null != teahouse && !DataConstants.Teastatus.DESTORY.toString().equals(teahouse.getState())) {//剔除销毁的茶楼
                        teahouses.add(new Myteahouseevent(teahouse, teamem));
                    } else {
                        logger.info("销毁的茶楼不能显示  [" + teamem.getTeanum() + "] [" + teamem.getState() + "]");
                    }
                } else {
                    logger.info("非 申请 和 通过状态 不能显示茶楼  [" + teamem.getTeanum() + "] [" + teamem.getPassflag() + "]");
                }

            }
            return new ResponseEntity<List<Myteahouseevent>>(teahouses, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取我的茶楼列表信息异常", e);
            return new ResponseEntity<String>("获取我的茶楼列表信息异常", HttpStatus.OK);
        }
    }

    /**
     * 获取茶楼成员列表
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/getmeminfoList"}, method = RequestMethod.POST)
    public ResponseEntity getmeminfoList(HttpServletRequest request, HttpServletResponse response,
                                         String token, String teanum, String role, String passflag, Integer page, Integer size, String gameid) {

        Token userToken = null;

        try {
            Page<Teamem> teamemList = null;

            if (!StringUtils.isBlank(token)) {

                userToken = tokenRes.findById(token);

                Teamem teamem = teamemRepository.findByUseridAndTeanum(userToken.getUserid(), teanum);

                if (null == teamem || StringUtils.isEmpty(teamem.getRole())) {
                    return new ResponseEntity<String>("成员信息为空", HttpStatus.OK);
                }

                if (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                        !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole())) {
                    return new ResponseEntity<String>("非管理者不能获取成员列表", HttpStatus.OK);
                }

                if (null == size) {
                    size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
                }

                if (null == page) {
                    page = 0;
                }

                PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "lastmodify"));

                if (null == gameid) {

                    if (!StringUtils.isEmpty(role) && !StringUtils.isEmpty(passflag)) {
                        teamemList = teamemRepository.findByTeanumAndRoleAndPassflag(teanum, role, passflag, pageRequest);

                    } else if (!StringUtils.isEmpty(role)) {

                        teamemList = teamemRepository.findByTeanumAndRole(teanum, role, pageRequest);

                    } else if (!StringUtils.isEmpty(passflag)) {

                        teamemList = teamemRepository.findByTeanumAndPassflag(teanum, passflag, pageRequest);

                    } else {
                        teamemList = teamemRepository.findByTeanum(teanum, pageRequest);
                    }
                } else {
                    if (!StringUtils.isEmpty(role) && !StringUtils.isEmpty(passflag)) {
                        teamemList = teamemRepository.findByTeanumAndRoleAndPassflagAndGameid(teanum, role, passflag, gameid, pageRequest);

                    } else if (!StringUtils.isEmpty(role)) {

                        teamemList = teamemRepository.findByTeanumAndRoleAndGameid(teanum, role, gameid, pageRequest);

                    } else if (!StringUtils.isEmpty(passflag)) {

                        teamemList = teamemRepository.findByTeanumAndPassflagAndGameid(teanum, passflag, gameid, pageRequest);

                    } else {
                        teamemList = teamemRepository.findByTeanumAndGameid(teanum, gameid, pageRequest);
                    }
                }

            }

            if (null == userToken) {
                return new ResponseEntity<String>("Token信息为空", HttpStatus.OK);
            }

            return new ResponseEntity<Page<Teamem>>(teamemList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼成员列表信息异常", e);
            return new ResponseEntity<String>("获取茶楼成员列表信息异常", HttpStatus.OK);
        }
    }

    /**
     * 获取基金充值记录
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/getteafundrecordlist"}, method = RequestMethod.POST)
    public ResponseEntity getteafundrecordlist(HttpServletRequest request, HttpServletResponse response,
                                               String token, String teanum, String gameid, Integer page, Integer size) {

        Token userToken = null;

        try {
            Page<Teafundrecord> teafundrecordPage = null;

            if (!StringUtils.isBlank(token)) {

                userToken = tokenRes.findById(token);

                if (null == userToken || StringUtils.isEmpty(userToken.getId())) {
                    return new ResponseEntity<String>("TOKEN 信息为空", HttpStatus.OK);
                }

                if (null == size) {
                    size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
                }

                if (null == page) {
                    page = 0;
                }

                PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "lastmodify"));

                if (!StringUtils.isEmpty(gameid)) {
                    teafundrecordPage = teafundrecordRepository.findByTeanumAndGameid(teanum, gameid, pageRequest);

                } else {
                    teafundrecordPage = teafundrecordRepository.findByTeanum(teanum, pageRequest);

                }

            }

            return new ResponseEntity(teafundrecordPage, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取基金充值信息异常", e);
            return new ResponseEntity<String>("获取基金充值信息异常", HttpStatus.OK);
        }
    }

    /**
     * 获取上下分记录
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/getteascorerecordlist"}, method = RequestMethod.POST)
    public ResponseEntity getteascorerecordlist(HttpServletRequest request, HttpServletResponse response,
                                                String token, String teanum, String gameid, Integer page, Integer size) {

        Token userToken = null;

        try {
            Page<Teascorerecord> teafundrecordPage = null;

            if (!StringUtils.isBlank(token)) {

                userToken = tokenRes.findById(token);

                if (null == userToken || StringUtils.isEmpty(userToken.getId())) {
                    return new ResponseEntity<String>("TOKEN 信息为空", HttpStatus.OK);
                }

                Teamem teamem = teamemRepository.findByUseridAndTeanum(userToken.getUserid(), teanum);

                if (teamem == null || (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                        !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole()))) {
                    return new ResponseEntity<String>("非管理者不能获取成员列表", HttpStatus.OK);
                }

                if (null == size) {
                    size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
                }

                if (null == page) {
                    page = 0;
                }

                PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "lastmodify"));

                if (!StringUtils.isEmpty(gameid)) {
                    teafundrecordPage = teascorerecordRepository.findByTeanumAndGameid(teanum, gameid, pageRequest);

                } else {
                    teafundrecordPage = teascorerecordRepository.findByTeanum(teanum, pageRequest);

                }

            }

            return new ResponseEntity(teafundrecordPage, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼上下分信息异常", e);
            return new ResponseEntity<String>("获取茶楼上下分信息异常", HttpStatus.OK);
        }
    }

    /**
     * 获取经营状况记录
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/getteareport"}, method = RequestMethod.POST)
    public ResponseEntity getteareport(HttpServletRequest request, HttpServletResponse response,
                                       String token, String teanum, Integer page, Integer size) {

        Token userToken = null;

        try {
            Page<Teareport> teafundrecordPage = null;

            if (!StringUtils.isBlank(token)) {

                userToken = tokenRes.findById(token);

                if (null == userToken || StringUtils.isEmpty(userToken.getId())) {
                    return new ResponseEntity<String>("TOKEN 信息为空", HttpStatus.OK);
                }

                Teamem teamem = teamemRepository.findByUseridAndTeanum(userToken.getUserid(), teanum);

                if (teamem == null || (!DataConstants.Tearole.MANAGER.toString().equals(teamem.getRole()) &&
                        !DataConstants.Tearole.OWNER.toString().equals(teamem.getRole()))) {
                    return new ResponseEntity<String>("非管理者不能经营状况", HttpStatus.OK);
                }

                if (null == size) {
                    size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
                }

                if (null == page) {
                    page = 0;
                }

                PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "reportdate"));

                teafundrecordPage = teareportRepository.findByTeanum(teanum, pageRequest);

            }

            return new ResponseEntity(teafundrecordPage, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取茶楼经营状况信息异常", e);
            return new ResponseEntity<String>("获取茶楼经营状况信息异常", HttpStatus.OK);
        }
    }

}