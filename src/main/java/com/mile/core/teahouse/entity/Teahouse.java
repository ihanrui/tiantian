package com.mile.core.teahouse.entity;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSONObject;
import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 茶楼描述信息
 * Created by michael on 2018/4/8.
 */

@Entity
@Table(name = "wd_t_teahouse")
@org.hibernate.annotations.Proxy(lazy = false)
public class Teahouse extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String name;//茶馆名称

    private String teanum;//茶馆编号 7位

    private String state;//状态

    private String usernotice;//公告

    private String roominfo;//房间信息

    private String gameid;//创建者

    private String username;

    private String userurl;

    private String userid;

    private Date createdate;//创建时间

    private String gameplaywayid;

    private String gameplaywaycode;

    private Date enddate;//销毁时间

    private Map extparams ;//房卡模式下的自定义参数

    private String extparamsstr;

    private String teanotice;

    private String ipflag;//相同IP禁止登陆

    private String passflag;//是否需要审核  Y/N


    private Integer limitscore;//茶楼要求的最低信誉分


    private String ticketflag;// DataConstants.Tickettype

    private Double ticketvalue;//门票的值  百分比 或者 值

    private Integer ticketlimit;//门票下线

    private Integer fund=0;//基金 任何人都可以往里面充值  用于房费

    private String shareflag;//是否允许分享 Y/N

    private String memo;//备注

    private String closenotice;//歇业公告


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间

    private Integer nums;//人数





    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Transient
    public Map getExtparams() {
        return extparams;
    }

    public void setExtparams(Map extparams) {
        this.extparams = extparams;
        this.extparamsstr = JSONUtils.toJSONString(extparams);
    }

    public String getExtparamsstr() {
        return extparamsstr;
    }

    public void setExtparamsstr(String extparamsstr) {
        extparams = JSONObject.parseObject(extparamsstr);
        this.extparamsstr = extparamsstr;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUsernotice() {
        return usernotice;
    }

    public void setUsernotice(String usernotice) {
        this.usernotice = usernotice;
    }

    public String getRoominfo() {
        return roominfo;
    }

    public void setRoominfo(String roominfo) {
        this.roominfo = roominfo;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getGameplaywayid() {
        return gameplaywayid;
    }

    public void setGameplaywayid(String gameplaywayid) {
        this.gameplaywayid = gameplaywayid;
    }

    public String getGameplaywaycode() {
        return gameplaywaycode;
    }

    public void setGameplaywaycode(String gameplaywaycode) {
        this.gameplaywaycode = gameplaywaycode;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getTeanotice() {
        return teanotice;
    }

    public void setTeanotice(String teanotice) {
        this.teanotice = teanotice;
    }

    public String getIpflag() {
        return ipflag;
    }

    public void setIpflag(String ipflag) {
        this.ipflag = ipflag;
    }

    public String getPassflag() {
        return passflag;
    }

    public void setPassflag(String passflag) {
        this.passflag = passflag;
    }

    public Integer getLimitscore() {
        return limitscore;
    }

    public void setLimitscore(Integer limitscore) {
        this.limitscore = limitscore;
    }

    public String getTicketflag() {
        return ticketflag;
    }

    public void setTicketflag(String ticketflag) {
        this.ticketflag = ticketflag;
    }

    public Double getTicketvalue() {
        return ticketvalue;
    }

    public void setTicketvalue(Double ticketvalue) {
        this.ticketvalue = ticketvalue;
    }

    public Integer getTicketlimit() {
        return ticketlimit;
    }

    public void setTicketlimit(Integer ticketlimit) {
        this.ticketlimit = ticketlimit;
    }

    public Integer getFund() {
        return fund;
    }

    public void setFund(Integer fund) {
        this.fund = fund;
    }

    public String getShareflag() {
        return shareflag;
    }

    public void setShareflag(String shareflag) {
        this.shareflag = shareflag;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getClosenotice() {
        return closenotice;
    }

    public void setClosenotice(String closenotice) {
        this.closenotice = closenotice;
    }

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }
}
