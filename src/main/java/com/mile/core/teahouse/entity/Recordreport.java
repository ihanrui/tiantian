package com.mile.core.teahouse.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**战绩统计
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_t_recordreport")
@org.hibernate.annotations.Proxy(lazy = false)
public class Recordreport extends BaseIdEntity {

    private static final long serialVersionUID = 1L;




    private String teanum;

    private String gameid;


    private int  record1;//昨日战绩

    private int record7;//7日战绩

    private int record30;//30日战绩

    private int recordall;//所有战绩


    private String memo;//备注



    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间




    public int getRecord1() {
        return record1;
    }

    public void setRecord1(int record1) {
        this.record1 = record1;
    }

    public int getRecord7() {
        return record7;
    }

    public void setRecord7(int record7) {
        this.record7 = record7;
    }

    public int getRecord30() {
        return record30;
    }

    public void setRecord30(int record30) {
        this.record30 = record30;
    }

    public int getRecordall() {
        return recordall;
    }

    public void setRecordall(int recordall) {
        this.recordall = recordall;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }



    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}
