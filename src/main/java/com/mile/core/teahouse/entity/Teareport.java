package com.mile.core.teahouse.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**茶楼记录
 * Created by michael on 2018/4/8.
 */


@Entity
@Table(name = "wd_t_teareport")
@org.hibernate.annotations.Proxy(lazy = false)
public class Teareport extends BaseIdEntity {

    private static final long serialVersionUID = 1L;



    private String teahouseid;

    private String teanum;

    private String reportdate;//日期

    private Integer num=0;//局数

    private Integer ticket=0;//门票







    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间


    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }


    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeahouseid() {
        return teahouseid;
    }

    public void setTeahouseid(String teahouseid) {
        this.teahouseid = teahouseid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getTicket() {
        return ticket;
    }

    public void setTicket(Integer ticket) {
        this.ticket = ticket;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}
