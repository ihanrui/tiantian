package com.mile.core.teahouse.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**信誉分变化明细
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_t_teascoredetail")
@org.hibernate.annotations.Proxy(lazy = false)
public class Teascoredetail extends BaseIdEntity {

    private static final long serialVersionUID = 1L;



    private String gameid;//成员

    private String teanum;//茶楼信息

    private int bscore;//修改前信誉分

    private int escore;//修改后信誉分

    private String roomid;//房间信息

    private int score;//本次修改分数

    private int percent;//门票


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间




    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public int getBscore() {
        return bscore;
    }

    public void setBscore(int bscore) {
        this.bscore = bscore;
    }

    public int getEscore() {
        return escore;
    }

    public void setEscore(int escore) {
        this.escore = escore;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }


    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}







