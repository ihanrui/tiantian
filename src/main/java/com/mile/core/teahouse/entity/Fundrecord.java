package com.mile.core.teahouse.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**战绩明细
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_t_fundrecord")
@org.hibernate.annotations.Proxy(lazy = false)
public class Fundrecord extends BaseIdEntity {

    private static final long serialVersionUID = 1L;



    private String teanum;

    private String gameid;

    private Date createdate;//充值时间

    private int bfund;//充值前基金

    private int efund;//充值后基金

    private int fund;//充值基金

    private String memo;



    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间




    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public int getBfund() {
        return bfund;
    }

    public void setBfund(int bfund) {
        this.bfund = bfund;
    }

    public int getEfund() {
        return efund;
    }

    public void setEfund(int efund) {
        this.efund = efund;
    }

    public int getFund() {
        return fund;
    }

    public void setFund(int fund) {
        this.fund = fund;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }


    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }
}
