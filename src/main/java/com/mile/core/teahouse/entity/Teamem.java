package com.mile.core.teahouse.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**茶楼成员map
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_t_teamem")
@org.hibernate.annotations.Proxy(lazy = false)
public class Teamem extends BaseIdEntity {

    private static final long serialVersionUID = 1L;



    private String gameid;//成员

    private String userid;//用户ID

    private String username;

    private String userurl;

    private String teanum;



    private Integer score;//信誉分

    private Integer limitscore;//最低信誉分



    private String state;//状态 0.在线 1.离线

    private String passflag;//DataConstants.Teapassflag

    private String role;//DataConstants.Tearole

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间



    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getTeanum() {
        return teanum;
    }

    public void setTeanum(String teanum) {
        this.teanum = teanum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPassflag() {
        return passflag;
    }

    public void setPassflag(String passflag) {
        this.passflag = passflag;
    }

    public String getRole() {
        return role;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }



    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getLimitscore() {
        return limitscore;
    }

    public void setLimitscore(Integer limitscore) {
        this.limitscore = limitscore;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
