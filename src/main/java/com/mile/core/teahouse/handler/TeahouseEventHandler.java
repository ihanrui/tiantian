package com.mile.core.teahouse.handler;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.mile.common.cache.CacheHelper;
import com.mile.common.client.NettyClients;
import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.Token;
import com.mile.core.game.BeiMiClient;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.eventmodel.TeahouseClient;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TeahouseEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(TeahouseEventHandler.class);

    protected SocketIOServer server;

    @Autowired
    public TeahouseEventHandler(SocketIOServer server) {
        this.server = server;
    }

    //创建茶楼

    /**
     * 传入参数  玩法ID(playwayid),游戏ID(gameid),玩法(extparams)
     *
     * @param client
     * @param request
     * @param data
     */
    @OnEvent(value = DataConstants.GLOBAL_TEAHOUSE_EVENT_CREATE)
    public void createteahouse(SocketIOClient client, AckRequest request, String data) {

        logger.info("创建茶楼 参数 data["+data+"]");

        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().creatTeahouse(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("创建茶楼异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent("创建茶楼异常",false));

        }

    }

    /*
    获取茶楼信息
    传入参数  teanum
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_GET)
    public void getteahouse(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().getTeahouseInfo(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 获取茶楼信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 获取茶楼信息异常",false));

        }
    }

    /*
    获取茶楼中桌子信息信息
    传入参数  teanum
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_GETTABLEINFO)
    public void gettableinfo(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().getTableInfo(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 获取桌子信息信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 获取桌子信息异常",false));

        }
    }

    /**
     * 修改茶楼信息 茶楼名称(name)，公告(usernotice)，玩法(extparams)，
     * 茶楼状态(正常，打烊，销毁)(state)，IP禁止加入(ipflag),
     * 门票设置(ticketflag，ticketvalue,ticketlimit)，审核标志(passflag)
     *
     * @param client
     * @param request
     * @param data
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_UPDATE)
    public void updateteahouse(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().formateClient2Entity(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 获取茶楼信息异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 获取茶楼信息异常",false));

        }
    }

    /**
     * 加入茶楼  传入参数applyjoin,teanum
     *
     * @param client
     * @param request
     * @param data
     */
    @OnEvent(value = DataConstants.GLOBAL_TEAHOUSE_EVENT_APPLYJOIN)
    public void applyjoin(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {
            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().applyjoin(teahouseClient);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 加入茶楼 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 加入茶楼 异常",false));
        }

    }

    //
    /*
    修改茶楼成员 gameid,teanum,passflag,role
    passflag  N:申请  Y:申请通过  O:离开茶楼  X:拒绝加入 F:禁止加入
    role 角色  N.成员 M.店小二
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_MEMUPDATE)
    public void memupdate(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());

            BMDataContext.getTeahouseService().memupdate(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 加入茶楼 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 加入茶楼 异常",false));

        }
    }


    /*
     获取单个成员信息  主要用于上下分
     传入参数 teanum,gameid
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_GETMEMINFO)
    public void getmeminfo(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().getmeminfo(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 获取单个成员信息 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 获取单个成员信息 异常",false));

        }
    }

    /*
     成员上下分
     传入参数 gameid,teanum,scoreflag,score,limitscore
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_DEALSCORE)
    public void dealscore(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().dealscore(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 处理上下分信息 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 处理上下分信息 异常",false));

        }
    }

    /*

     传入参数 teanum,tablenum,token
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_SITDOWN)
    public void sitdown(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);

        TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            teahouseClient.setUserid(userToken.getUserid());
            BMDataContext.getTeahouseService().sitdown(userToken,teahouseClient);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 茶楼坐下异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 茶楼坐下异常",false));
        }
    }



    /*
     解散房间
     传入参数 gameid,teanum,scoreflag,score,limitscore
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_DISSROOM)
    public void teadissroom(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());

            BMDataContext.getTeahouseService().teadissroom(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 处理上下分信息 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 处理上下分信息 异常",false));

        }
    }

    /*
     转入基金
     传入参数 gameid,teanum,scoreflag,score,limitscore
     */
    @OnEvent(value = DataConstants.TEAHOUSE_EVENT_SHIFTFUND)
    public void shiftfund(SocketIOClient client, AckRequest request, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);
        try {

            String token = beiMiClient.getToken();
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);

            TeahouseClient teahouseClient = JSON.parseObject(data, TeahouseClient.class);
            teahouseClient.setUserid(userToken.getUserid());

            BMDataContext.getTeahouseService().shiftfund(teahouseClient);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" 处理上下分信息 异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_ERROR, beiMiClient.getUserid(), new NoticeEvent(" 处理上下分信息 异常",false));

        }
    }

    /**
     * token,kickplayer,teanum
     * @param client
     * @param data
     */
    //踢人
    @OnEvent(value = DataConstants.GAME_EVENT_KICKPLAYER)
    public void kickplayer(SocketIOClient client, String data) {

        logger.info("发送消息 传入  data[" + data + "]");
        BeiMiClient beiMiClient = NettyClients.getInstance().getClientByData(client,data);

        try {

            BMDataContext.getTeahouseService().kickplayer(beiMiClient,data);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("踢出玩家异常", e);
            TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GAME_EVENT_KICKPLAYER, beiMiClient.getUserid(), new NoticeEvent("踢出玩家异常",false));
        }
    }


}