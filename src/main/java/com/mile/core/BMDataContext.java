package com.mile.core;

import com.mile.core.game.GameEngine;
import com.mile.core.game.RoomEngine;
import com.mile.core.game.mazi.engine.MaziGameEngine;
import com.mile.core.game.pinshi.engine.PinshiGameEngine;
import com.mile.core.teahouse.service.TeahouseService;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

public class BMDataContext {
	public static final String USER_SESSION_NAME = "user";
	public static final String GUEST_USER = "guest";
	public static final String IM_USER_SESSION_NAME = "im_user";
	public static final String GUEST_USER_ID_CODE = "BEIMIGUESTUSEKEY" ;
	public static final String SERVICE_QUENE_NULL_STR = "service_quene_null" ;
	public static final String DEFAULT_TYPE = "default"	;		//默认分类代码
	public static final String BEIMI_SYSTEM_DIC = "com.dic.system.template";
	public static final String BEIMI_SYSTEM_GAME_TYPE_DIC = "com.dic.game.type";
	public static final String BEIMI_SYSTEM_GAME_SCENE_DIC = "com.dic.scene.item";

	public static final String BEIMI_SYSTEM_GAME_CARDTYPE_DIC = "com.dic.game.dizhu.cardtype";

	public static final String BEIMI_SYSTEM_GAME_ROOMTITLE_DIC = "com.dic.game.room.title";

	public static final String BEIMI_COM_DIC_HALL_MARQUEE = "com.dic.hall.marquee";

	public static final String BEIMI_MESSAGE_EVENT = "command_room" ;

	public static final String BEIMI_GAMESTATUS_EVENT = "gamestatus" ;


	public static final String BEIMI_GAME_PLAYWAY = "game_playway";

	public static final String BEIMI_SYSTEM_AUTH_DIC = "com.dic.auth.resource";

	public static final String BEIMI_SYSTEM_ROOM = "room" ;





	public static final String BEIMI_SYSTEM_GAME_ROOM_TAKECARD_ERRTIP = "com.dic.game.room.takecard_errtip";


	public static final String BEIMI_SYSTEM_GAME_ROOM_NEWSREPORT_ERRTIP = "com.dic.game.room.newsreport_errtip";

	public static final String BEIMI_SYSTEM_GAME_JSONSTR = "com.dic.game.jsonstr";



	public static final String BEIMI_SYSTEM_GAME_SHUFFLEFLAG = "shuffleflag";

	public static String SYSTEM_ORGI = "beimi" ;

	private static int WebIMPort = 9081 ;

	private static boolean imServerRunning = false ;			//IM服务状态

	private static ApplicationContext applicationContext ;

	public static Map<String , Boolean> model = new HashMap<String,Boolean>();


	private static GameEngine gameEngine ;

	private static MaziGameEngine maziGameEngine;

	private static RoomEngine roomEngine;

	private static PinshiGameEngine pinshiGameEngine;


	private static TeahouseService teahouseService;

	public static int getWebIMPort() {
		return WebIMPort;
	}

	public static void setWebIMPort(int webIMPort) {
		WebIMPort = webIMPort;
	}

	public static void setApplicationContext(ApplicationContext context){
		applicationContext = context ;
	}

	public static void setGameEngine(GameEngine engine){
		gameEngine = engine ;
	}

	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameAccountConfig(String orgi){
		return BMDataContext.ConfigNames.ACCOUNTCONFIG.toString()+"_"+orgi ;
	}

	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameConfig(String orgi){
		return BMDataContext.ConfigNames.GAMECONFIG.toString()+"_"+orgi ;
	}

	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameAiConfig(String orgi){
		return BMDataContext.ConfigNames.AICONFIG.toString()+"_"+orgi ;
	}


	public static ApplicationContext getContext(){
		return applicationContext ;
	}

	public static MaziGameEngine getMaziGameEngine() {
		return maziGameEngine;
	}

	public static void setMaziGameEngine(MaziGameEngine maziGameEngine) {
		BMDataContext.maziGameEngine = maziGameEngine;
	}

	public static GameEngine getGameEngine(){
		return gameEngine;
	}
	/**
	 * 系统级的加密密码 ， 从CA获取
	 * @return
	 */
	public static String getSystemSecrityPassword(){
		return "BEIMI" ;
	}

	public enum NameSpaceEnum{

		SYSTEM("/bm/system") ,
		GAME("/bm/game");

		private String namespace ;

		public String getNamespace() {
			return namespace;
		}

		public void setNamespace(String namespace) {
			this.namespace = namespace;
		}

		NameSpaceEnum(String namespace){
			this.namespace = namespace ;
		}

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum ModelType{
		ROOM,
		HALL;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum ConfigNames{
		GAMECONFIG,
		AICONFIG,
		ACCOUNTCONFIG,
		PLAYWAYCONFIG,
		PLAYWAYGROUP,
		PLAYWAYGROUPITEM;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum UserDataEventType{
		SAVE,UPDATE,DELETE;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum PlayerAction{
		GANG,
		PENG,
		HU,
		CHI,
		GUO;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum PlayerGangAction{
		MING,		//明杠
		AN,			//暗杠
		WAN;		//弯杠
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum GameTypeEnum{
		MAJIANG,
		DIZHU,
		DEZHOU;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum PlayerTypeEnum{
		AI,			//AI
		NORMAL,		//普通玩家
		OFFLINE,	//托管玩家
		LEAVE;		//离开房间的玩家
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum GameStatusEnum{
		READY,
		NOTREADY,
		MANAGED,
		PLAY,
		TIMEOUT,		//登录会话过期
		TEAHOUSE,//在茶楼
		WATCH,//观看状态  pinshi

		WAITTING;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum CardsTypeEnum{
		ONE(1),		//单张      3~K,A,2
		TWO(2),		//一对	 3~K,A,2
		THREE(3),	//三张	 3~K,A,2
		FOUR(4),	//三带一	 AAA+K
		FORMTWO(41),	//三带对	 AAA+K
		FIVE(5),	//单顺	连子		10JQKA
		SIX(6),		//双顺	连对		JJQQKK
		SEVEN(7),	//三顺	飞机		JJJQQQ
		EIGHT(8),	//飞机	带翅膀	JJJ+QQQ+K+A
		EIGHTONE(81),	//飞机	带翅膀	JJJ+QQQ+KK+AA
		NINE(9),	//四带二			JJJJ+Q+K
		NINEONE(91),	//四带二对			JJJJ+QQ+KK
		TEN(10),	//炸弹			JJJJ
		ELEVEN(11);	//王炸			0+0

		private int type ;

		CardsTypeEnum(int type){
			this.type = type ;
		}


		public int getType() {
			return type;
		}


		public void setType(int type) {
			this.type = type;
		}
	}

	public enum MessageTypeEnum{
		JOINROOM,
		MESSAGE,
		END,
		TRANS, STATUS , AGENTSTATUS , SERVICE, WRITING;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum SearchRoomResultType{
		NOTEXIST,  //房间不存在
		FULL, 		//房间已满员
		OK,			//加入成功
		DISABLE,	//房间启用了 禁止非邀请加入
		INVALID,	//房主已离开房间
		EXIST,  //已在房间
		CREATE,//需要新建房间
		IPFORBID,//同IP禁止加入
		NOTPASS,//非审核通过状态
		NOFUND,//无茶楼基金
		NOSCORE,//茶楼分小于0
		NOTEA;	//非茶楼人员

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum MaJiangWinType{
		TUI,
		RIVER,
		END,
		LOST;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum PVActionEnum{
		INCOME,	//
		CONSUME,
		EXCHANGE,
		VERIFY;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum PVAStatusEnum{
		OK,
		NOTENOUGH,
		FAILD,
		NOTEXIST,
		INVALID;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	/**
	 * 收入类型 ， 1、充值，2、兑换、3、赢了，4、赠送，5、抽奖，6、接受赠与
	 */
	public enum PVAInComeActionEnum{
		RECHARGE,
		EXCHANGE,
		WIN,
		WELFARE,
		PRIZE,
		GIFT;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	/**
	 *  支出 1、输了，2、逃跑扣除、3、兑换扣除，4、送好友
	 */
	public enum PVAConsumeActionEnum{
		LOST,
		ESCAPE,
		DEDUCTION,
		SEND;

		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public static void setIMServerStatus(boolean running){
		imServerRunning = running ;
	}
	public static boolean getIMServerStatus(){
		return imServerRunning;
	}


	//牌型  1,单张，2，对子，3，三张，4，四张，5，连对，6，连三张，7，连四张
	// 炸从小到大排序
	// 11，副510，12，方块510K 13，梅花510K  14，红桃510K  15，黑桃510K
	// 16，两个小王，17，一个大王一个小王 18，两个大王，19，五炸 20 ，三个大小王
	// 21 六炸  22，四个大小王，23 七炸  24 八炸

	public enum MaziCardsTypeEnum{
		NONE(0),    //无牌型
		ONE(1),		//单张
		TWO(2),		//对子
		THREE(3),	//三张
		FOUR(4),	//四带
		LINK2(5),	//连对
		LINK3(6),   //连三张
		LINK4(7),	//连四张


		F510K(11),//副510
		FK510K(12),//方块510K
		MH510K(13),//梅花510K
		HONGT510K(14),//红桃510
		HEIT510K(15),//黑桃510
		GHOST2XW(16),//（两个）小王
		GHOST2DX(17),//大小王
		GHOST2DW(18),//(两个)大王
		BOMB5(19),//5炸
		GHOST3(20),//3个大小王
		BOMB6(21),//6炸
		GHOST4(22),//4个大小王
		BOMB7(23),//7炸
		BOMB8(24);//8炸




		private int type ;

		MaziCardsTypeEnum(int type){
			this.type = type ;
		}


		public int getType() {
			return type;
		}


		public void setType(int type) {
			this.type = type;
		}
	}

	public static TeahouseService getTeahouseService() {
		return teahouseService;
	}

	public static void setTeahouseService(TeahouseService teahouseService) {
		BMDataContext.teahouseService = teahouseService;
	}

	public static RoomEngine getRoomEngine() {
		return roomEngine;
	}

	public static void setRoomEngine(RoomEngine roomEngine) {
		BMDataContext.roomEngine = roomEngine;
	}

	public static PinshiGameEngine getPinshiGameEngine() {
		return pinshiGameEngine;
	}

	public static void setPinshiGameEngine(PinshiGameEngine pinshiGameEngine) {
		BMDataContext.pinshiGameEngine = pinshiGameEngine;
	}
}
