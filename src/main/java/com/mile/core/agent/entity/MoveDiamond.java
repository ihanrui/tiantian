package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**代理充值
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_movediamond")
@org.hibernate.annotations.Proxy(lazy = false)
public class MoveDiamond extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String sourceid;//代理游戏ID

    private String sourceusername;//代理名称

    private String sourceuserurl;

    private String targetid;//代理/玩家游戏ID

    private String targetusername;//玩家名称

    private String targetuserurl;

    private int movenum;//拨钻数量

    private int sourcemovebegin;//拨钻前数量

    private int sourcemoveend;//拨钻后数量


    private int targetmovebegin;//拨钻前数量

    private int targetmoveend;//拨钻后数量


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间



    public String getSourceid() {
        return sourceid;
    }

    public void setSourceid(String sourceid) {
        this.sourceid = sourceid;
    }

    public String getTargetid() {
        return targetid;
    }

    public void setTargetid(String targetid) {
        this.targetid = targetid;
    }



    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getSourceusername() {
        return sourceusername;
    }

    public void setSourceusername(String sourceusername) {
        this.sourceusername = sourceusername;
    }

    public String getSourceuserurl() {
        return sourceuserurl;
    }

    public void setSourceuserurl(String sourceuserurl) {
        this.sourceuserurl = sourceuserurl;
    }

    public String getTargetusername() {
        return targetusername;
    }

    public void setTargetusername(String targetusername) {
        this.targetusername = targetusername;
    }

    public String getTargetuserurl() {
        return targetuserurl;
    }

    public void setTargetuserurl(String targetuserurl) {
        this.targetuserurl = targetuserurl;
    }


    public int getSourcemovebegin() {
        return sourcemovebegin;
    }

    public void setSourcemovebegin(int sourcemovebegin) {
        this.sourcemovebegin = sourcemovebegin;
    }

    public int getSourcemoveend() {
        return sourcemoveend;
    }

    public void setSourcemoveend(int sourcemoveend) {
        this.sourcemoveend = sourcemoveend;
    }



    public int getTargetmovebegin() {
        return targetmovebegin;
    }

    public void setTargetmovebegin(int targetmovebegin) {
        this.targetmovebegin = targetmovebegin;
    }

    public int getTargetmoveend() {
        return targetmoveend;
    }

    public void setTargetmoveend(int targetmoveend) {
        this.targetmoveend = targetmoveend;
    }

    public int getMovenum() {
        return movenum;
    }

    public void setMovenum(int movenum) {
        this.movenum = movenum;
    }
}
