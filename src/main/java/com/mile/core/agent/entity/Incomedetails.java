package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**收益明细
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_incomedetails")
@org.hibernate.annotations.Proxy(lazy = false)
public class Incomedetails extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String chargeid;//充值ID

    private String gameid;//游戏ID

    private String userurl;//头像路径

    private String username;//名称

    private String agentid;//代理ID 同GAMEID

    private String agentuserurl;//头像路径

    private String agentusername;//名称

    private double incomeamt;//返利金额


    private double chargeamt;//充值金额

    private double balbegin;//返现收益前余额

    private double balend;//返现收益后余额

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间

    private double incomepersent;//返利比例

    private String chargetype;//goldcoin 豆子,diamond  钻石


    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChargeid() {
        return chargeid;
    }

    public void setChargeid(String chargeid) {
        this.chargeid = chargeid;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getAgentuserurl() {
        return agentuserurl;
    }

    public void setAgentuserurl(String agentuserurl) {
        this.agentuserurl = agentuserurl;
    }

    public String getAgentusername() {
        return agentusername;
    }

    public void setAgentusername(String agentusername) {
        this.agentusername = agentusername;
    }



    public double getBalbegin() {
        return balbegin;
    }

    public void setBalbegin(double balbegin) {
        this.balbegin = balbegin;
    }

    public double getBalend() {
        return balend;
    }

    public void setBalend(double balend) {
        this.balend = balend;
    }


    public double getIncomepersent() {
        return incomepersent;
    }

    public void setIncomepersent(double incomepersent) {
        this.incomepersent = incomepersent;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public double getIncomeamt() {
        return incomeamt;
    }

    public void setIncomeamt(double incomeamt) {
        this.incomeamt = incomeamt;
    }

    public double getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(double chargeamt) {
        this.chargeamt = chargeamt;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }
}
