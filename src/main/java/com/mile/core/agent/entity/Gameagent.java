package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**代理信息
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_gameagent")
@org.hibernate.annotations.Proxy(lazy = false)
public class Gameagent extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String gameid;//游戏ID

    private String userurl;//头像路径

    private String username;//名称

    private String daystr;//当日日期

    private Double daycharge;//当日充值

    private Double dayearn;//当日收益

    private String agentlvl;//代理级别 com.dic.gameagent.lvl:  lvl0,lvl1,lvl2

    private Double hischarge;//历史总充值

    private Double hisearn;//历史总收益

    private Double balamt;//余额(可提现金额)

    private Double withdrawamt;//已提现金额

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间

    private String usercode;//登录名

    private String passwd;//登录密码

    private String phone;//手机号码


    private String webopenid;





    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDaystr() {
        return daystr;
    }

    public void setDaystr(String daystr) {
        this.daystr = daystr;
    }

    public Double getDaycharge() {
        return daycharge;
    }

    public void setDaycharge(Double daycharge) {
        this.daycharge = daycharge;
    }

    public Double getDayearn() {
        return dayearn;
    }

    public void setDayearn(Double dayearn) {
        this.dayearn = dayearn;
    }

    public String getAgentlvl() {
        return agentlvl;
    }

    public void setAgentlvl(String agentlvl) {
        this.agentlvl = agentlvl;
    }

    public Double getHischarge() {
        return hischarge;
    }

    public void setHischarge(Double hischarge) {
        this.hischarge = hischarge;
    }

    public Double getHisearn() {
        return hisearn;
    }

    public void setHisearn(Double hisearn) {
        this.hisearn = hisearn;
    }

    public Double getBalamt() {
        return balamt;
    }

    public void setBalamt(Double balamt) {
        this.balamt = balamt;
    }

    public Double getWithdrawamt() {
        return withdrawamt;
    }

    public void setWithdrawamt(Double withdrawamt) {
        this.withdrawamt = withdrawamt;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebopenid() {
        return webopenid;
    }

    public void setWebopenid(String webopenid) {
        this.webopenid = webopenid;
    }
}
