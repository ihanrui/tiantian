package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**用户-代理关系表
 * Created by michael on 2018/4/8.
 */

@Entity
@Table(name = "wd_a_useragentmap")
@org.hibernate.annotations.Proxy(lazy = false)
public class Useragentmap extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String gameid;

    private String username;

    private String userurl;



    private String  referid;//推荐者ID，推荐者和用户本身都可送卡

    private String agent1;//直接代理

    private String agent2;//间接代理

    private String agent3;//间接代理

    private String agent4;//间接代理

    private String agent5;//5级代理

    private Double chargeamt;//充值总金额

    private Double chargecoin;//豆子


    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间


    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }


    public String getAgent1() {
        return agent1;
    }

    public void setAgent1(String agent1) {
        this.agent1 = agent1;
    }

    public String getAgent2() {
        return agent2;
    }

    public void setAgent2(String agent2) {
        this.agent2 = agent2;
    }

    public String getAgent3() {
        return agent3;
    }

    public void setAgent3(String agent3) {
        this.agent3 = agent3;
    }

    public String getAgent4() {
        return agent4;
    }

    public void setAgent4(String agent4) {
        this.agent4 = agent4;
    }

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getReferid() {
        return referid;
    }

    public void setReferid(String referid) {
        this.referid = referid;
    }

    public String getAgent5() {
        return agent5;
    }

    public void setAgent5(String agent5) {
        this.agent5 = agent5;
    }

    public Double getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(Double chargeamt) {
        this.chargeamt = chargeamt;
    }

    public Double getChargecoin() {
        return chargecoin;
    }

    public void setChargecoin(Double chargecoin) {
        this.chargecoin = chargecoin;
    }
}
