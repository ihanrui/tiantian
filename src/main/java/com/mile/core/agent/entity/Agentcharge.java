package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**代理充值
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_agentcharge")
@org.hibernate.annotations.Proxy(lazy = false)
public class Agentcharge extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String agentid;//代理游戏ID

    private String agentname;//代理游戏名称

    private String agenturl;//代理头像URL

    private String gameid;//玩家游戏ID

    private String username;//玩家名称

    private String userurl;//玩家URL

    private double chargeamt;//充值金额

    private int chargenum;//充值数量

    private int chargebegin;//充值前数量

    private int chargeend;//充值后数量

    private String chargeid;//充值ID

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间



    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(double chargeamt) {
        this.chargeamt = chargeamt;
    }

    public int getChargenum() {
        return chargenum;
    }

    public void setChargenum(int chargenum) {
        this.chargenum = chargenum;
    }

    public int getChargebegin() {
        return chargebegin;
    }

    public void setChargebegin(int chargebegin) {
        this.chargebegin = chargebegin;
    }

    public int getChargeend() {
        return chargeend;
    }

    public void setChargeend(int chargeend) {
        this.chargeend = chargeend;
    }



    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public String getAgenturl() {
        return agenturl;
    }

    public void setAgenturl(String agenturl) {
        this.agenturl = agenturl;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getChargeid() {
        return chargeid;
    }

    public void setChargeid(String chargeid) {
        this.chargeid = chargeid;
    }
}
