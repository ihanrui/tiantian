package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**weixininfojoin 与 OPENID关联
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_mapinfo")
@org.hibernate.annotations.Proxy(lazy = false)
public class Mapinfo extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String gameid;//代理游戏ID

    private String webopenid;//WEB OPENID

    private String appopenid;//app OPENID

    private String weixininfojoin;//组合字段

    private String referid;//推荐者ID

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间



    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getWebopenid() {
        return webopenid;
    }

    public void setWebopenid(String webopenid) {
        this.webopenid = webopenid;
    }

    public String getAppopenid() {
        return appopenid;
    }

    public void setAppopenid(String appopenid) {
        this.appopenid = appopenid;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public String getWeixininfojoin() {
        return weixininfojoin;
    }

    public void setWeixininfojoin(String weixininfojoin) {
        this.weixininfojoin = weixininfojoin;
    }

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferid() {
        return referid;
    }

    public void setReferid(String referid) {
        this.referid = referid;
    }
}
