package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**充值明细
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_chargedetails")
@org.hibernate.annotations.Proxy(lazy = false)
public class Chargedetails extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String gameid;//游戏ID

    private String userurl;//头像路径

    private String username;//名称

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间


    private int  chargebegin;//充值前数量

    private int  chargeend;//充值后数量

    private double  chargeamt;//充值金额

    private int  chargenum;//充值数量

    private String giveflag;//赠送标志  Y：赠送，N:充值

    private String memo ;//备注

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    public int getChargebegin() {
        return chargebegin;
    }

    public void setChargebegin(int chargebegin) {
        this.chargebegin = chargebegin;
    }

    public int getChargeend() {
        return chargeend;
    }

    public void setChargeend(int chargeend) {
        this.chargeend = chargeend;
    }

    public double getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(double chargeamt) {
        this.chargeamt = chargeamt;
    }

    public int getChargenum() {
        return chargenum;
    }

    public void setChargenum(int chargenum) {
        this.chargenum = chargenum;
    }


    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGiveflag() {
        return giveflag;
    }

    public void setGiveflag(String giveflag) {
        this.giveflag = giveflag;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
