package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**提现记录
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_withdrawinfo")
@org.hibernate.annotations.Proxy(lazy = false)
public class Withdrawinfo extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String applyid;//申请者游戏ID

    private String username;

    private String userurl;


    private Integer withdrawamt;//提现金额

    private double balbegin;//提现前余额

    private double balend;//提现后余额

    private String lastmodify= new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date(System.currentTimeMillis()));//最后修改时间


    private String applyflag;//A:已申请未到账 P:审批通过已提现

    private String chargetype;//豆子/amt




    public String getApplyid() {
        return applyid;
    }

    public void setApplyid(String applyid) {
        this.applyid = applyid;
    }



    public double getBalbegin() {
        return balbegin;
    }

    public void setBalbegin(double balbegin) {
        this.balbegin = balbegin;
    }

    public double getBalend() {
        return balend;
    }

    public void setBalend(double balend) {
        this.balend = balend;
    }

    public String getLastmodify() {
        return lastmodify;
    }

    public void setLastmodify(String lastmodify) {
        this.lastmodify = lastmodify;
    }

    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApplyflag() {
        return applyflag;
    }

    public void setApplyflag(String applyflag) {
        this.applyflag = applyflag;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserurl() {
        return userurl;
    }

    public void setUserurl(String userurl) {
        this.userurl = userurl;
    }

    public Integer getWithdrawamt() {
        return withdrawamt;
    }

    public void setWithdrawamt(Integer withdrawamt) {
        this.withdrawamt = withdrawamt;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }
}
