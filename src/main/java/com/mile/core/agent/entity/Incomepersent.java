package com.mile.core.agent.entity;

import com.mile.common.entity.BaseIdEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**提现比例
 * Created by michael on 2018/4/8.
 */
@Entity
@Table(name = "wd_a_incomepersent")
@org.hibernate.annotations.Proxy(lazy = false)
public class Incomepersent extends BaseIdEntity {

    private static final long serialVersionUID = 1L;


    private String name;//名称

    private int weight;//权重

    private int distance;//距离

    private double persent;//提现比例





    @Id
    @Column(length = 32)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "assigned")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getPersent() {
        return persent;
    }

    public void setPersent(double persent) {
        this.persent = persent;
    }
}
