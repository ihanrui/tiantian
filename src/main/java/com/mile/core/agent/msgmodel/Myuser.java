package com.mile.core.agent.msgmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.entity.PlayUser;

import java.util.List;

public class Myuser extends BaseEntity{

	private List<Useragentmap> useragentmaps;

	private Integer totalsize;

	private Double persent;

	public Myuser(List<Useragentmap> playUserList, Integer totalsize, Double persent) {
		this.useragentmaps = playUserList;
		this.totalsize = totalsize;
		this.persent = persent;
	}

	public List<Useragentmap> getUseragentmaps() {
		return useragentmaps;
	}

	public void setUseragentmaps(List<Useragentmap> useragentmaps) {
		this.useragentmaps = useragentmaps;
	}

	public Integer getTotalsize() {
		return totalsize;
	}

	public void setTotalsize(Integer totalsize) {
		this.totalsize = totalsize;
	}

	public Double getPersent() {
		return persent;
	}

	public void setPersent(Double persent) {
		this.persent = persent;
	}
}



