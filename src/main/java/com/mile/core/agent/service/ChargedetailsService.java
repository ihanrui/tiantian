package com.mile.core.agent.service;

import com.mile.core.BMDataContext;
import com.mile.core.agent.entity.Chargedetails;
import com.mile.core.agent.entity.Incomepersent;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.IncomepersentRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service(value = "chargedetailsService")
public class ChargedetailsService {
    private static final Logger logger = LoggerFactory.getLogger(ChargedetailsService.class);



    /**
     * 充值明细
     *
     * @return
     */
    public void  dealIncome(Chargedetails chargedetails) {

        try {

            logger.info("充值 信息  [" + chargedetails + "]");

            UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);

            Useragentmap useragentmap = useragentmapRepository.findByGameid(chargedetails.getGameid());

            if(null == useragentmap){
                logger.info("没有绑定代理关系["+chargedetails.getGameid()+"]");
            }

            String gameid1 = useragentmap.getAgent1();
            IncomepersentRepository incomepersentRepository = BMDataContext.getContext().getBean(IncomepersentRepository.class);

            Incomepersent incomepersent = incomepersentRepository.findByDistance(1);

            if(null != incomepersent){

            }





        } catch (Exception e) {
            e.printStackTrace();
            logger.error("充值明细", e);

        }
    }
}
