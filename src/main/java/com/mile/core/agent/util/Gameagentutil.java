package com.mile.core.agent.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.agent.entity.*;
import com.mile.core.agent.jpa.*;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.hall.entity.RechargeRecord;
import com.mile.core.hall.entity.Shop;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michael on 2018/4/23.
 */
public class Gameagentutil {
    private static final Logger logger = LoggerFactory.getLogger(Gameagentutil.class);

    /**
     * 房卡的返佣
     *
     * @param chargedetails
     */
    public static void dealCardIncome(Chargedetails chargedetails) {
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);
        UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);

        Useragentmap useragentmap = useragentmapRepository.findByGameid(chargedetails.getGameid());

        if (null == useragentmap) {
            logger.info("用户未绑定邀请码 [" + chargedetails.getGameid() + "]");
            return ;
        }

        Gameagent gameagent1 = gameagentRepository.findByGameid(useragentmap.getAgent1());
        if (null != gameagent1) {
            dealOneCardIncome(chargedetails, gameagent1, 1);
        } else {
            logger.info("通过 useragentmap.getAgent1() [" + useragentmap.getAgent1() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent2 = gameagentRepository.findByGameid(useragentmap.getAgent2());
        if (null != gameagent2) {
            dealOneCardIncome(chargedetails, gameagent2, 2);
        } else {
            logger.info("通过 useragentmap.getAgent2() [" + useragentmap.getAgent2() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent3 = gameagentRepository.findByGameid(useragentmap.getAgent3());
        if (null != gameagent3) {
            dealOneCardIncome(chargedetails, gameagent3, 3);
        } else {
            logger.info("通过 useragentmap.getAgent3() [" + useragentmap.getAgent3() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent4 = gameagentRepository.findByGameid(useragentmap.getAgent4());
        if (false && null != gameagent4) {
            dealOneCardIncome(chargedetails, gameagent4, 4);
        } else {

            logger.info("目前只做3级返利  [" + useragentmap.getAgent4() + "] ");
        }

        if(null == useragentmap.getChargeamt()) {
            useragentmap.setChargeamt(0.0);
        }

        useragentmap.setChargeamt(useragentmap.getChargeamt()+chargedetails.getChargeamt());
        useragentmapRepository.save(useragentmap);

    }

    public static Gameagent dealOneCardIncome(Chargedetails chargedetails, Gameagent gameagent, int distance) {

        IncomepersentRepository incomepersentRepository = BMDataContext.getContext().getBean(IncomepersentRepository.class);
        IncomedetailsRepository incomedetailsRepository = BMDataContext.getContext().getBean(IncomedetailsRepository.class);

        //直接代理返利
        Incomepersent incomepersent = incomepersentRepository.findByDistance(distance);



        if (null == incomepersent) {
            logger.info("通过 distance[" + distance + "] 获取代理分成信息为空");
            return gameagent;
        }

        logger.info(incomepersent+"级返利，比例为["+incomepersent.getPersent()+"] ");



        Incomedetails incomedetails = new Incomedetails();
        incomedetails.setAgentid(gameagent.getGameid());
        incomedetails.setAgentusername(gameagent.getUsername());
        incomedetails.setAgentuserurl(gameagent.getUserurl());

        incomedetails.setGameid(chargedetails.getGameid());
        incomedetails.setUsername(chargedetails.getUsername());
        incomedetails.setUserurl(chargedetails.getUserurl());

        incomedetails.setChargetype(DataConstants.Chargetype.AMT.toString());

        if( null == gameagent.getBalamt()){
            gameagent.setBalamt(0.0);
        }


        incomedetails.setBalbegin(gameagent.getBalamt());
        incomedetails.setChargeid(chargedetails.getId());
        incomedetails.setIncomepersent(incomepersent.getPersent());

        incomedetails.setChargeamt(chargedetails.getChargeamt());
        if (gameagent.getGameid().equals(chargedetails.getGameid())) {
            logger.info("代理为自身，无返利");
            incomedetails.setIncomeamt(0);
        }else
            incomedetails.setIncomeamt(incomedetails.getChargeamt() * incomedetails.getIncomepersent());

        incomedetails.setBalend(incomedetails.getBalbegin() + incomedetails.getIncomeamt());

        incomedetailsRepository.saveAndFlush(incomedetails);

        gameagent.setBalamt(incomedetails.getBalend());

        String datestr = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis()));

        if (!datestr.equals(gameagent.getDaystr())) {
            gameagent.setDaystr(datestr);
            gameagent.setDayearn(0.0);
            gameagent.setDaycharge(0.0);
        }

        gameagent.setDaycharge(gameagent.getDaycharge()+incomedetails.getChargeamt());
        gameagent.setDayearn(gameagent.getDayearn() + incomedetails.getIncomeamt());



        if(null == gameagent.getHischarge())
            gameagent.setHischarge(0.0);
        if(null == gameagent.getHisearn())
            gameagent.setHisearn(0.0);

        gameagent.setHischarge(gameagent.getHischarge() + chargedetails.getChargeamt());
        gameagent.setHisearn(gameagent.getHisearn() + incomedetails.getIncomeamt());

        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);

        gameagentRepository.saveAndFlush(gameagent);

        return gameagent;

    }

    public static Chargedetails charge(Shop shop, RechargeRecord record, String gameid) {

        logger.info("充值信息 gameid["+gameid+"] record["+record+"] shop["+shop+"] ");

        PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);
        ChargedetailsRepository chargedetailsRepository = BMDataContext.getContext().getBean(ChargedetailsRepository.class);

        PlayUser playUser = playUserRepository.findByGameid(gameid);

        Chargedetails chargedetails = new Chargedetails();

        chargedetails.setChargeamt(record.getActualamount());
        chargedetails.setChargebegin(playUser.getDiamonds());
        playUser.setDiamonds(playUser.getDiamonds() + record.getRechargenumbers());
        chargedetails.setChargeend(playUser.getDiamonds());
        chargedetails.setGameid(gameid);
        chargedetails.setChargenum(record.getRechargenumbers());
        chargedetails.setUsername(playUser.getUsername());
        chargedetails.setUserurl(playUser.getUserurl());
        chargedetails.setGiveflag("N");
        chargedetails.setMemo("正常充值");

        chargedetailsRepository.save(chargedetails);

        MaziScoreUtil.synchronizationUsercache(playUser);
        if ("Y".equals(shop.getAgentflag())) {
            becomeAgent(playUser, gameid);

        } else {
            logger.info(" gameid[" + gameid + "] 充值未达到成为代理要求");
        }

        dealCardIncome(chargedetails);

        return    chargedetails;

    }



    public static void charge(Shop shop, RechargeRecord record, String gameid,String agentid) {
        logger.info("代充充值信息 gameid["+gameid+"] agentid["+agentid+"] record["+record+"] shop["+shop+"] ");

        Chargedetails chargedetails= charge(shop,record,gameid);

        Agentcharge agentcharge = new Agentcharge();
        PlayUserRepository playUserRepository =BMDataContext.getContext().getBean(PlayUserRepository.class);
        PlayUser agent =    playUserRepository.findByGameid(agentid);
        PlayUser playUser  =  playUserRepository.findByGameid(gameid);

        agentcharge.setAgentid(agent.getGameid());
        agentcharge.setAgentname(agent.getUsername());
        agentcharge.setAgenturl(agent.getUserurl());
        agentcharge.setChargeamt(chargedetails.getChargeamt());
        agentcharge.setChargebegin(chargedetails.getChargebegin());
        agentcharge.setChargeend(chargedetails.getChargeend());
        agentcharge.setChargenum(chargedetails.getChargenum());
        agentcharge.setGameid(playUser.getGameid());
        agentcharge.setUsername(playUser.getUsername());
        agentcharge.setUserurl(playUser.getUserurl());
        agentcharge.setChargeid(chargedetails.getId());

        BMDataContext.getContext().getBean(AgentchargeRepository.class).save(agentcharge);


    }

    /**
     * 修改代理关系， 自己成为代理的时候，自己是自己的直接上级
     *
     * @param gameid
     */
    public static void changeAgent(String gameid) {
        UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);
        Useragentmap useragentmap = useragentmapRepository.findByGameid(gameid);

        if (null == useragentmap) {
            logger.info("通过 gameid[" + useragentmap + "] 为查找到代理关系");
            return;
        }

        useragentmap.setAgent4(useragentmap.getAgent3());
        useragentmap.setAgent3(useragentmap.getAgent2());
        useragentmap.setAgent2(useragentmap.getAgent1());
        useragentmap.setAgent1(gameid);

        useragentmapRepository.delete(useragentmap);
        useragentmapRepository.saveAndFlush(useragentmap);

    }

    /**
     * 成为代理
     *
     * @param playUser
     * @param gameid
     */
    public static void becomeAgent(PlayUser playUser, String gameid) {
        logger.info("充值数量已经达到成为代理要求");
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);
        Gameagent gameagent = gameagentRepository.findByGameid(gameid);
        if (null == gameagent) {
            logger.info("恭喜 gameid[" + gameid + "] 成为代理");
            gameagent = new Gameagent();
            gameagent.setGameid(playUser.getGameid());
            gameagent.setPhone(playUser.getMobile());
            gameagent.setUsercode(playUser.getGameid());
            gameagent.setUsername(playUser.getUsername());
            gameagent.setUserurl(playUser.getUserurl());
            gameagent.setPasswd(UKTools.md5(playUser.getGameid()));
            gameagentRepository.saveAndFlush(gameagent);

            changeAgent(gameid);

        } else {
            logger.info(" gameid[" + gameid + "] 已经为代理， 无需重新增加");
        }
    }

    /**
     * 微信登录
     *
     * @param playUser
     * @param gameid
     */
    public static boolean applogin(String weixinjoininfo, String gameid, String appopenid) {

        logger.info("微信登录处理   weixinjoininfo[" + weixinjoininfo + "] gameid[" + gameid + "] appopenid[" + appopenid + "]");
        if(StringUtils.isEmpty(gameid) || StringUtils.isEmpty(appopenid)){
            logger.info("参数为空，直接返回");
            return false;
        }
        MapinfoRepository mapinfoRepository = BMDataContext.getContext().getBean(MapinfoRepository.class);
        boolean agentmapflag = isagentmap(gameid);//是否已建立处理代理关系

        logger.info("是否已建立处理代理关系 agentmapflag[" + agentmapflag + "]  gameid[" + gameid + "]");

        //1.如果通过appopenid能查询到，如果referid 不为空， 处理referid。

        Mapinfo mapinfo = mapinfoRepository.findByAppopenid(appopenid);
        if(null == mapinfo) {
            mapinfo = mapinfoRepository.findByWeixininfojoin(weixinjoininfo);
        }


        boolean flag = false;
        if (agentmapflag) {
            flag = true;
        } else if (!agentmapflag && null != mapinfo && !StringUtils.isEmpty(mapinfo.getReferid())) {
            logger.info(" 通过appopenid ,weixinjoininfo 能查询到 referid[" + mapinfo.getReferid() + "]  gameid[" + gameid + "]");
            createagentmap(gameid, mapinfo.getReferid());
            mapinfoRepository.delete(mapinfo);
            flag = true;
        } else {
            logger.info("如果都未 能查询到，存储gameid,appopenid,weixinjoinfinfo到mapinfo , gameid[" + gameid + "]");
            flag = false;
        }
        if (null != mapinfo && !StringUtils.isEmpty(mapinfo.getGameid())) {
            logger.info("mapinfo  gameid 不为空 ,无需存储gameid,appopenid,weixinjoinfinfo到mapinfo , gameid[" + gameid + "]");
            return flag;
        }
        if (null == mapinfo) {
            mapinfo = new Mapinfo();
        }
        mapinfo.setAppopenid(appopenid);
        mapinfo.setGameid(gameid);
        mapinfo.setWeixininfojoin(weixinjoininfo);
        mapinfoRepository.delete(mapinfo);
        mapinfoRepository.saveAndFlush(mapinfo);
        return flag;
    }

    public static boolean isagentmap(String gameid) {
        UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);
        if(StringUtils.isEmpty(gameid))
            return false;
        return null == useragentmapRepository.findByGameid(gameid) ? false : true;

    }

    public static boolean createagentmap(String gameid, String referid) {

        UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);
        Useragentmap useragentmapgame = useragentmapRepository.findByGameid(gameid);

        if (null != useragentmapgame) {
            logger.info("已绑定代理关系gameid[" + gameid + "],无需再次绑定");
            return false;
        } else {
            logger.info("未绑定代理关系gameid[" + gameid + "],双方赠卡");
        }

        useragentmapgame = new Useragentmap();
        useragentmapgame.setGameid(gameid);
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);
        Gameagent gameagent = gameagentRepository.findByGameid(referid);
        Gameagent self = gameagentRepository.findByGameid(gameid);


        if (null == gameagent) {
            logger.info("推荐者referid[" + referid + "] 非代理 双方赠卡");
            useragentmapgame.setReferid(referid);

            Useragentmap useragentmaprefer = useragentmapRepository.findByGameid(referid);
            if (null != useragentmaprefer && null == self) {
                logger.info("推荐者referid[" + referid + "] 已绑定代理关系");
                useragentmapgame.setAgent1(useragentmaprefer.getAgent1());
                useragentmapgame.setAgent2(useragentmaprefer.getAgent2());
                useragentmapgame.setAgent3(useragentmaprefer.getAgent3());
                useragentmapgame.setAgent4(useragentmaprefer.getAgent4());

            } else if (null != useragentmaprefer && null != self) {
                logger.info("self 推荐者referid[" + referid + "] 已绑定代理关系");
                useragentmapgame.setAgent1(gameid);
                useragentmapgame.setAgent2(useragentmaprefer.getAgent1());
                useragentmapgame.setAgent3(useragentmaprefer.getAgent2());
                useragentmapgame.setAgent4(useragentmaprefer.getAgent3());
            } else {
                logger.info("推荐者referid[" + referid + "] 未绑定代理关系，无需处理");
            }
        } else {

            Useragentmap useragentmaprefer = useragentmapRepository.findByGameid(referid);
            if (null == self) {
                useragentmapgame.setAgent1(referid);
                if (null != useragentmaprefer) {
                    logger.info("推荐者为代理  referid[" + referid + "] 已绑定代理关系");
                    useragentmapgame.setAgent2(useragentmaprefer.getAgent2());
                    useragentmapgame.setAgent3(useragentmaprefer.getAgent3());
                    useragentmapgame.setAgent4(useragentmaprefer.getAgent4());
                } else {
                    logger.info("推荐者为代理 referid[" + referid + "] 未绑定代理关系，无需处理");
                }
            } else {
                useragentmapgame.setAgent1(gameid);
                useragentmapgame.setAgent2(referid);
                if (null != useragentmaprefer) {
                    logger.info("self 推荐者为代理  referid[" + referid + "] 已绑定代理关系");
                    useragentmapgame.setAgent3(useragentmaprefer.getAgent1());
                    useragentmapgame.setAgent4(useragentmaprefer.getAgent2());
                } else {
                    logger.info("self 推荐者为代理 referid[" + referid + "] 未绑定代理关系，无需处理");
                }
            }
        }


        formatUseragentmap(useragentmapgame);

        useragentmapRepository.save(useragentmapgame);


        giveDiamond(gameid, TeahouseUtil.getIntConstantsByName(DataConstants.SYS_GIVE_DIAMOND));
        giveDiamond(referid,TeahouseUtil.getIntConstantsByName(DataConstants.SYS_GIVE_DIAMOND));

        return true;

    }

    public static void formatUseragentmap(Useragentmap useragentmap) {
        if (StringUtils.isEmpty(useragentmap.getGameid())) {
            return;
        }

        PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);
        PlayUser playUser = playUserRepository.findByGameid(useragentmap.getGameid());
        if (null == playUser) {
            return;
        }
        useragentmap.setUsername(playUser.getUsername());
        useragentmap.setUserurl(playUser.getUserurl());

    }

    /**
     * 代理登录
     *
     * @param playUser
     * @param gameid   返回值说明：
     */
    public static boolean agentlogin(String weixinjoininfo, String webopenid) {
        logger.info("代理登录处理   weixinjoininfo[" + weixinjoininfo + "]  webopenid[" + webopenid + "]");
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);

        Gameagent gameagent = gameagentRepository.findByWebopenid(webopenid);

        if (null != gameagent) {
            logger.info("webopenid[" + webopenid + "] 已关联代理  成功关联  true");
            return true;
        } else {
            MapinfoRepository mapinfoRepository = BMDataContext.getContext().getBean(MapinfoRepository.class);
            Mapinfo mapinfo = mapinfoRepository.findByWebopenid(webopenid);//mapinfoRepository.findByWeixininfojoinOrWebopenid(weixinjoininfo, webopenid);


            if(null == mapinfo){
                mapinfo = mapinfoRepository.findByWeixininfojoin(weixinjoininfo);
            }



            boolean flag = false;

            if (null != mapinfo && !StringUtils.isEmpty(mapinfo.getGameid())) {
                gameagent = gameagentRepository.findByGameid(mapinfo.getGameid());
                if (null == gameagent) {
                    logger.info("webopenid[" + webopenid + "] 为非代理 直接返回 false ");
                    flag = false;
                } else {
                    logger.info("webopenid[" + webopenid + "] 为代理 成功关联  直接返回 true ");
                    gameagent.setWebopenid(webopenid);
                    gameagentRepository.delete(gameagent);
                    gameagentRepository.saveAndFlush(gameagent);

                    mapinfo.setWeixininfojoin(weixinjoininfo);
                    mapinfo.setWebopenid(webopenid);
                    mapinfoRepository.delete(mapinfo);//删除mapinfo信息， 现在已经没有用了。
                    //mapinfoRepository.saveAndFlush(mapinfo);

                    flag = true;
                }

            } else if (null != mapinfo) {

                mapinfo.setWeixininfojoin(weixinjoininfo);
                mapinfo.setWebopenid(webopenid);
                mapinfoRepository.delete(mapinfo);
                mapinfoRepository.saveAndFlush(mapinfo);

                logger.info("webopenid[" + webopenid + "] 找到关联关系 但未有GAMEID    直接返回 false ");
                flag = false;
            } else {

                mapinfo = new Mapinfo();

                logger.info("webopenid[" + webopenid + "] 未找到关联关系   直接返回 false ");

                flag = false;
                mapinfo.setWeixininfojoin(weixinjoininfo);
                mapinfo.setWebopenid(webopenid);
                mapinfoRepository.saveAndFlush(mapinfo);
            }




            return flag;

        }

    }

    public static boolean agentlogin(String webopenid) {
        logger.info("代理登录处理    webopenid[" + webopenid + "]");
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);

        Gameagent gameagent = gameagentRepository.findByWebopenid(webopenid);

        if (null != gameagent) {
            logger.info("webopenid[" + webopenid + "] 已关联代理  成功关联  true");
            return true;
        } else {
            return false;
        }
    }

    /**
     * 软件包下载
     *
     * @param playUser
     * @param gameid   返回值说明：
     */
    public static void webdownload(String weixinjoininfo, String webopenid, String referid) {
        logger.info("软件包下载 处理   weixinjoininfo[" + weixinjoininfo + "]  webopenid[" + webopenid + "] referid[" + referid + "]");
        MapinfoRepository mapinfoRepository = BMDataContext.getContext().getBean(MapinfoRepository.class);

        Mapinfo mapinfo = mapinfoRepository.findByWebopenid(webopenid);
        if(null == mapinfo) {
            mapinfo = mapinfoRepository.findByWeixininfojoin(weixinjoininfo);
        }


        if (null == mapinfo) {
            logger.info("未到 mapinfo对象  weixinjoininfo[" + weixinjoininfo + "]  webopenid[" + webopenid + "] referid[" + referid + "] ");
            mapinfo = new Mapinfo();
            mapinfo.setWebopenid(webopenid);
            mapinfo.setWeixininfojoin(weixinjoininfo);
            mapinfo.setReferid(referid);
            mapinfoRepository.saveAndFlush(mapinfo);
        } else if (StringUtils.isEmpty(mapinfo.getReferid())) {
            logger.info("找到 mapinfo对象  weixinjoininfo[" + weixinjoininfo + "]  webopenid[" + webopenid + "] referid[" + referid + "] ");
            mapinfo.setWebopenid(webopenid);
            mapinfo.setWeixininfojoin(weixinjoininfo);
            mapinfo.setReferid(referid);
            mapinfoRepository.delete(mapinfo);
            mapinfoRepository.saveAndFlush(mapinfo);

        } else {
            logger.info("找到 mapinfo对象  weixinjoininfo[" + weixinjoininfo + "]  webopenid[" + webopenid + "] referid[" + referid + "]  无需处理");

        }

    }


    public static void giveDiamond(String gameid,Integer givenum){
        PlayUser playUser = BMDataContext.getContext().getBean(PlayUserRepository.class).findByGameid(gameid);

        Chargedetails chargedetails = new Chargedetails();

        chargedetails.setChargeamt(givenum);
        chargedetails.setChargebegin(playUser.getDiamonds());
        playUser.setDiamonds(playUser.getDiamonds() + givenum);
        chargedetails.setChargeend(playUser.getDiamonds());
        chargedetails.setGameid(gameid);
        chargedetails.setUsername(playUser.getUsername());
        chargedetails.setUserurl(playUser.getUserurl());
        chargedetails.setGiveflag("Y");
        chargedetails.setMemo("赠送钻石");

        BMDataContext.getContext().getBean(ChargedetailsRepository.class).save(chargedetails);
        BMDataContext.getContext().getBean(PlayUserRepository.class).save(playUser);

        MaziScoreUtil.synchronizationUsercache(playUser);
    }

}
