package com.mile.core.agent.util;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.agent.entity.*;
import com.mile.core.agent.jpa.*;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.hall.entity.RechargeRecord;
import com.mile.core.hall.entity.Shop;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.UKTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michael on 2018/4/23.
 */
public class Gamecoinutil {
    private static final Logger logger = LoggerFactory.getLogger(Gamecoinutil.class);

    /**
     * 房卡的返佣
     *
     * @param chargedetails
     */
    public static void dealCoinIncome(String gameid) {
        GameagentRepository gameagentRepository = BMDataContext.getContext().getBean(GameagentRepository.class);
        UseragentmapRepository useragentmapRepository = BMDataContext.getContext().getBean(UseragentmapRepository.class);

        int num = TeahouseUtil.getIntConstantsByName("coinexpend");
        Useragentmap    useragentmap = useragentmapRepository.findByGameid(gameid);

        if (null == useragentmap) {
            logger.info("用户未绑定邀请码 [" + gameid + "]");
            return ;
        }

        Gameagent gameagent1 = gameagentRepository.findByGameid(useragentmap.getAgent1());


        if (null != gameagent1) {
            dealGoldconinIncome(num, useragentmap.getAgent1(), 1,gameid);
        } else {
            logger.info("通过 useragentmap.getAgent1() [" + useragentmap.getAgent1() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent2 = gameagentRepository.findByGameid(useragentmap.getAgent2());
        if (null != gameagent2) {
            dealGoldconinIncome(num, useragentmap.getAgent2(), 2,gameid);
        } else {
            logger.info("通过 useragentmap.getAgent2() [" + useragentmap.getAgent2() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent3 = gameagentRepository.findByGameid(useragentmap.getAgent3());
        if (null != gameagent3) {
            dealGoldconinIncome(num, useragentmap.getAgent3(), 3,gameid);
        } else {
            logger.info("通过 useragentmap.getAgent3() [" + useragentmap.getAgent3() + "] 查询 Gameagent 为空");
        }

        Gameagent gameagent4 = gameagentRepository.findByGameid(useragentmap.getAgent4());
        if (false && null != gameagent4) {
            dealGoldconinIncome(num, useragentmap.getAgent4(), 4,gameid);
        } else {

            logger.info("目前只做3级返利  [" + useragentmap.getAgent4() + "] ");
        }

        if(null == useragentmap.getChargecoin()) {
            useragentmap.setChargecoin(0.0);
        }

        useragentmapRepository.save(useragentmap);

    }

    public static void dealGoldconinIncome(int num, String agentid, int distance,String gameid) {

        IncomepersentRepository incomepersentRepository = BMDataContext.getContext().getBean(IncomepersentRepository.class);
        IncomedetailsRepository incomedetailsRepository = BMDataContext.getContext().getBean(IncomedetailsRepository.class);

        //直接代理返利
        Incomepersent incomepersent = incomepersentRepository.findByDistance(distance);

        PlayUserRepository playUserRepository = BMDataContext.getContext().getBean(PlayUserRepository.class);

        PlayUser playUser = playUserRepository.findByGameid(gameid);

        if (null == incomepersent) {
            logger.info("通过 distance[" + distance + "] 获取代理分成信息为空");
            return ;
        }

        logger.info(incomepersent+"级返利，比例为["+incomepersent.getPersent()+"] ");


        PlayUser gameagent = BMDataContext.getContext().getBean(PlayUserRepository.class).findByGameid(agentid);
        Incomedetails incomedetails = new Incomedetails();
        incomedetails.setAgentid(gameagent.getGameid());
        incomedetails.setAgentusername(gameagent.getUsername());
        incomedetails.setAgentuserurl(gameagent.getUserurl());

        incomedetails.setGameid(playUser.getGameid());
        incomedetails.setUsername(playUser.getUsername());
        incomedetails.setUserurl(playUser.getUserurl());



        incomedetails.setBalbegin(gameagent.getGoldcoins());
        incomedetails.setChargeid("");
        incomedetails.setIncomepersent(incomepersent.getPersent());

        incomedetails.setChargeamt(num);

        incomedetails.setIncomeamt(incomedetails.getChargeamt() * incomedetails.getIncomepersent());

        incomedetails.setBalend(incomedetails.getBalbegin() + incomedetails.getIncomeamt());

        incomedetails.setChargetype(DataConstants.Chargetype.GOLDCOIN.toString());

        incomedetailsRepository.saveAndFlush(incomedetails);


        gameagent.setGoldcoins(new Double(incomedetails.getBalend()).intValue());

        playUserRepository.save(gameagent);

        MaziScoreUtil.synchronizationUsercache(gameagent);


    }

}
