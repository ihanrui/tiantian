package com.mile.core.agent.controler;

import com.mile.core.agent.entity.Incomedetails;
import com.mile.core.agent.jpa.IncomedetailsRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/apps/maziplatform")
public class IncomedetailsController extends Handler{


	@Autowired
	private IncomedetailsRepository incomedetailsRepository ;



	@RequestMapping({"/incomedetails"})
	@Menu(type="platform", subtype="incomedetails")
	public ModelAndView list(ModelMap map , HttpServletRequest request ){
		Page<Incomedetails> incomedetailslist = incomedetailsRepository.findAll(new PageRequest(super.getP(request), super.getPs(request),new Sort(Sort.Direction.DESC, "lastmodify"))) ;

		map.addAttribute("incomedetailslist", incomedetailslist) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/incomedetails/incomedetailslist"));
	}



}
