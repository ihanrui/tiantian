package com.mile.core.agent.controler;

import com.mile.core.agent.entity.Incomepersent;
import com.mile.core.agent.jpa.IncomepersentRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/apps/maziplatform")
public class IncomepersentController extends Handler{


	@Autowired
	private IncomepersentRepository incomepersentRepository ;



	@RequestMapping({"/incomepersent"})
	@Menu(type="platform", subtype="incomepersent")
	public ModelAndView list(ModelMap map , HttpServletRequest request ){
		Page<Incomepersent> incomepersentlist = incomepersentRepository.findAll(new PageRequest(super.getP(request), super.getPs(request) )) ;

		map.addAttribute("incomepersentlist", incomepersentlist) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/incomepersent/incomepersentlist"));
	}



}
