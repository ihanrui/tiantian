package com.mile.core.agent.controler;

import com.mile.common.cache.CacheHelper;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.entity.Withdrawinfo;
import com.mile.core.agent.handler.MovediamondHandler;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.agent.jpa.WithdrawinfoRepository;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import com.mile.util.GameUtils;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/apps/maziplatform")
public class WithdrawController extends Handler {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawController.class);

    @Autowired
    private WithdrawinfoRepository withdrawinfoRepository;

    @Autowired
    private GameagentRepository gameagentRepository;

    @RequestMapping({"/withdraw"})
    @Menu(type = "platform", subtype = "withdraw")
    public ModelAndView list(ModelMap map, HttpServletRequest request, String applyflag) {

        try {

            logger.info("查询提现记录 applyflag[" + applyflag + "]");

            Page<Withdrawinfo> withdrawinfolist = null;
            if (StringUtils.isEmpty(applyflag)) {
                withdrawinfolist = withdrawinfoRepository.findAll(new PageRequest(super.getP(request), super.getPs(request), new Sort(Sort.Direction.DESC, "lastmodify")));
            } else {
                withdrawinfolist = withdrawinfoRepository.findByApplyflag(applyflag, new PageRequest(super.getP(request), super.getPs(request), new Sort(Sort.Direction.DESC, "lastmodify")));
            }
            map.addAttribute("withdrawinfolist", withdrawinfolist);

            return request(super.createAppsTempletResponse("/apps/business/maziplatform/withdraw/withdrawlist"));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("查询提现记录失败", e);
            return null;
        }
    }

    @RequestMapping({"/withdraw/approve"})
    @Menu(type = "platform", subtype = "gameroom")
    public ModelAndView delete(ModelMap map, HttpServletRequest request, String withdrawid, String applyflag) {
        logger.info("提现审批 applyflag[" + applyflag + "]  withdrawid[" + withdrawid + "]");

        Withdrawinfo withdrawinfo = withdrawinfoRepository.findById(withdrawid);

        Gameagent gameagent = gameagentRepository.findByGameid(withdrawinfo.getApplyid());

        gameagent.setWithdrawamt((null == gameagent.getWithdrawamt() ? 0 : gameagent.getWithdrawamt()) + withdrawinfo.getWithdrawamt());

        if (!"A".equals(withdrawinfo.getApplyflag())) {
            return request(super.createRequestPageTempletResponse("redirect:/apps/maziplatform/withdraw.html"));
        }

        withdrawinfo.setApplyflag(applyflag);

        withdrawinfoRepository.save(withdrawinfo);
        gameagentRepository.save(gameagent);
        return request(super.createRequestPageTempletResponse("redirect:/apps/maziplatform/withdraw.html"));
    }

}
