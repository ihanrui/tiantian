package com.mile.core.agent.controler;

import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/apps/maziplatform")
public class GameagentController extends Handler{


	@Autowired
	private GameagentRepository gameagentRepository ;



	@RequestMapping({"/gameagent"})
	@Menu(type="platform", subtype="gameagent")
	public ModelAndView list(ModelMap map , HttpServletRequest request){
		Page<Gameagent> gameagentlist = gameagentRepository.findAll(new PageRequest(super.getP(request), super.getPs(request) )) ;

		map.addAttribute("gameagentlist", gameagentlist) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/gameagent/gameagentlist"));
	}



}
