package com.mile.core.agent.controler;

import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import com.mile.web.handler.apps.business.platform.GameUsersController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/apps/maziplatform")
public class UseragentmapController extends Handler{


	private static final Logger logger = LoggerFactory.getLogger(UseragentmapController.class);


	@Autowired
	private UseragentmapRepository useragentmapRepository ;



	@RequestMapping({"/useragentmap"})
	@Menu(type="platform", subtype="useragentmap")
	public ModelAndView list(ModelMap map , HttpServletRequest request, @Valid String gameid ){


		logger.info("代理关系查询 gameid["+gameid+"] ");
		if(!StringUtils.isEmpty(gameid)){
			map.addAttribute("useragentmaplist", useragentmapRepository.findByGameidLike(gameid, new PageRequest(super.getP(request), super.getPs(request),
					new Sort(Sort.Direction.DESC, "lastmodify"))));
		}else {

			map.addAttribute("useragentmaplist", useragentmapRepository.findAll(new PageRequest(super.getP(request), super.getPs(request),
					new Sort(Sort.Direction.DESC, "lastmodify"))));
		}
		return request(super.createAppsTempletResponse("/apps/business/maziplatform/useragentmap/useragentmaplist"));
	}

	@RequestMapping({"/useragentmap/delete"})
	@Menu(type="platform", subtype="useragentmap")
	public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String gameid){
		if(!StringUtils.isBlank(gameid)){
			Useragentmap useragentmap = useragentmapRepository.findByGameid(gameid) ;
			if(useragentmap!=null){
				useragentmapRepository.delete(useragentmap);
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/maziplatform/useragentmap.html"));
	}



	@RequestMapping({"/useragentmap/edit"})
	@Menu(type="platform", subtype="useragentmap")
	public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id){

		map.addAttribute("useragentmap", useragentmapRepository.findById(id)) ;

		return request(super.createRequestPageTempletResponse("/apps/business/maziplatform/useragentmap/useragentmapedit"));
	}



	@RequestMapping("/useragentmap/update")
	@Menu(type = "admin" , subtype = "useragentmap")
	public ModelAndView update(HttpServletRequest request ,@Valid Useragentmap useragentmapp) {

		Useragentmap useragentmap = useragentmapRepository.findById(useragentmapp.getId()) ;

		if(useragentmap!=null){
			useragentmap.setAgent1(useragentmapp.getAgent1());
			useragentmap.setAgent2(useragentmapp.getAgent2());
			useragentmap.setAgent3(useragentmapp.getAgent3());
			useragentmap.setAgent4(useragentmapp.getAgent4());
			useragentmap.setAgent5(useragentmapp.getAgent5());
			useragentmap.setReferid(useragentmapp.getReferid());


			useragentmapRepository.save(useragentmap) ;

		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/maziplatform/useragentmap.html"));
	}



}
