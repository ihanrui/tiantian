package com.mile.core.agent.handler;

import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.entity.MoveDiamond;
import com.mile.core.agent.entity.Withdrawinfo;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.MoveDiamondRepository;
import com.mile.core.agent.msgmodel.Myuser;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拨钻controler
 */
@Controller
@RequestMapping("/agent/movediamond")
public class MovediamondHandler extends Handler {

    private static final Logger logger = LoggerFactory.getLogger(MovediamondHandler.class);

    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private MoveDiamondRepository moveDiamondRepository;

    /**
     * 我的玩家
     *
     * @param request
     * @param response
     * @param gameid
     * @param size
     * @param page
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/querymyrecord"}, method = RequestMethod.GET)
    public ResponseEntity querymyrecord(HttpServletRequest request, HttpServletResponse response
            , Integer size, Integer page) {

        try {

            logger.info("查询我的提现记录 ");

            if (null == size) {
                size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
            }

            if (null == page) {
                page = 0;
            }

            PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "createtime"));

            List<MoveDiamond> moveDiamondList = moveDiamondRepository.findBySourceidOrderByLastmodifyDesc(this.getGameagent(request).getGameid());

            Map map = new HashMap();
            map.put("totalsize",moveDiamondList.size());
            map.put("data",moveDiamondList);

            return new ResponseEntity(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("查询我的拨钻记录", e);
            return new ResponseEntity("查询我的拨钻记录失败", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping({"/move"})
    public ResponseEntity movediamond(ModelMap map, HttpServletRequest request, String targetid, Integer movenum) {

        try {


            MoveDiamond movediamond = new MoveDiamond();
            movediamond.setTargetid(targetid);
            movediamond.setMovenum(movenum);


            Gameagent gameagent = this.getGameagent(request);
            movediamond.setSourceid(gameagent.getGameid());
            movediamond.setSourceusername(gameagent.getUsername());
            movediamond.setSourceuserurl(gameagent.getUserurl());



            PlayUser source = playUserRepository.findByGameid(movediamond.getSourceid());

            if(source.getDiamonds() < movenum){
                return new ResponseEntity("拨钻数量["+movenum+"]应小于自己的钻石数量["+source.getDiamonds()+"]", HttpStatus.OK);
            }

            if (null != source) {
                movediamond.setSourcemovebegin(source.getDiamonds());
                source.setDiamonds(source.getDiamonds() - movediamond.getMovenum());
                if (source.getDiamonds() < 0) {
                    throw new Exception("拨钻数量应大于0");
                }
                movediamond.setSourcemoveend(source.getDiamonds());
                playUserRepository.save(source);

                MaziScoreUtil.synchronizationUsercache(source);
            }

            PlayUser target = playUserRepository.findByGameid(movediamond.getTargetid());
            if (null != target) {
                movediamond.setTargetusername(target.getUsername());
                movediamond.setTargetuserurl(target.getUserurl());
                movediamond.setTargetmovebegin(target.getDiamonds());
                target.setDiamonds(source.getDiamonds() + movediamond.getMovenum());
                movediamond.setTargetmoveend(target.getDiamonds());
                playUserRepository.save(source);

                MaziScoreUtil.synchronizationUsercache(target);

            }

            moveDiamondRepository.saveAndFlush(movediamond);

            map.addAttribute("moveDiamond", movediamond);

            return new ResponseEntity(movediamond, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("拨钻失败" + e.getMessage(), e);
            return new ResponseEntity("拨钻失败", HttpStatus.OK);
        }
    }

}
