package com.mile.core.agent.handler;

import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Agentcharge;
import com.mile.core.agent.entity.Incomedetails;
import com.mile.core.agent.entity.Withdrawinfo;
import com.mile.core.agent.jpa.AgentchargeRepository;
import com.mile.core.agent.jpa.IncomedetailsRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 充值查询
 */
@Controller
@RequestMapping("/agent/agentcharge")
public class AgentchargeHandler extends Handler{

	private static final Logger logger = LoggerFactory.getLogger(AgentchargeHandler.class);

	@Autowired
	private PlayUserRepository playUserRepository ;


	@Autowired
	private AgentchargeRepository agentchargeRepository ;











	@SuppressWarnings("rawtypes")
	@RequestMapping(value = {"/querybygameid"}, method = RequestMethod.GET)
	public ResponseEntity querybygameid(HttpServletRequest request, HttpServletResponse response, String gameid
			, Integer size, Integer page) {

		try {

			logger.info("查询玩家充值返利  gameid[" + gameid + "] this.getGameagent(request).getGameid() ["+this.getGameagent(request).getGameid()+"]");

			if (null == size) {
				size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
			}

			if (null == page) {
				page = 0;
			}

			PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "createtime"));

			List<Agentcharge> agentchargeList = null;



			if (StringUtils.isEmpty(gameid)) {
				agentchargeList = agentchargeRepository.findByAgentidOrderByLastmodifyDesc(this.getGameagent(request).getGameid());
			}else {
				agentchargeList = agentchargeRepository.findByAgentidAndGameidOrderByLastmodifyDesc(this.getGameagent(request).getGameid(), gameid);
			}

			if(null == agentchargeList) {
				agentchargeList = new ArrayList<Agentcharge>();
			}



			Map map = new HashMap();
			map.put("totalsize",agentchargeList.size());
			map.put("data",agentchargeList);
			return new ResponseEntity(map, HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			logger.error("查询玩家充值返利信息失败",e);
			return new ResponseEntity("查询玩家充值返利信息失", HttpStatus.BAD_REQUEST);
		}
	}









}
