package com.mile.core.agent.handler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GameagentHelp {


    private static Map<String, String> phoneCodes = new HashMap<>();
    private static List<String> phones = new ArrayList();

    private static String WECHAT_REDIRECT_BASEINFO = null;
    private static String WECHAT_REDIRECT_USERINFO = null;

    static {
        try {
            WECHAT_REDIRECT_BASEINFO = URLEncoder.encode("http://app.chnmile.com/wxgame/wxGameLoginController.do?wechatBaseInfo", "UTF-8");
            WECHAT_REDIRECT_USERINFO = URLEncoder.encode("http://app.chnmile.com/wxgame/wxGameLoginController.do?wechatUserInfo", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void putPhoneCode(String phone, String smsCode) {
        if (phoneCodes == null) {
            phoneCodes = new HashMap<>();
            phones = new ArrayList();
        } else {
            if (phones.size() > 100) {
                for (int i = 50; i >= 0; i--) {
                    String p = phones.get(i);
                    phoneCodes.remove(p);
                    phones.remove(i);
                }
            }
        }
        phones.add(phone);
        phoneCodes.put(phone, smsCode);
    }

    public static String getMSMCode(String phone) {
        if (phoneCodes != null) {
            return phoneCodes.get(phone);
        }
        return "";
    }

    /**
     * @param str
     * @return
     */
    public static boolean isMobile(final String str) {
        Pattern p = null;
        Matcher m = null;
        boolean b = false;
        p = Pattern.compile("^[1][3,4,5,6,7,8][0-9]{9}$"); // 验证手机号
        m = p.matcher(str);
        b = m.matches();
        return b;
    }


    public static String getWechatBaseUrl(String appid, String baseInfoUrl) {
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=game#wechat_redirect";
        return String.format(url, appid, baseInfoUrl);
    }

    public static String getWechatAuthUrl(String appid, String userInfoUrl) {
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo&state=game#wechat_redirect";
        return String.format(url, appid, userInfoUrl);
    }

    public static void main(String[] args) {
        System.out.println(isMobile("203030222"));
        System.out.println(isMobile("123456789"));
        System.out.println(isMobile("133456789"));
    }


}
