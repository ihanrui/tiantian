package com.mile.core.agent.handler;

import com.mile.common.constants.ContextConstants;
import com.mile.common.constants.SystemDicKeyConstants;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.entity.Mapinfo;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.MapinfoRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.agent.util.Gameagentutil;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.hall.entity.Shop;
import com.mile.core.hall.jpa.ShopRepository;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.DicUtil;
import com.mile.util.JSONUtil;
import com.mile.util.tool.WechatUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/agent/gameagent")
public class GameagentHandler extends Handler {

    @Autowired
    private GameagentRepository gameagentRepository;

    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private MapinfoRepository mapinfoRepository;


    @Autowired
    private UseragentmapRepository useragentmapRepository;

    private static final Logger logger = LoggerFactory.getLogger(GameagentHandler.class);


    private final static String LOGIN_TYPE_KEY = "loginType";

    private final static String GAMEID_KEY = "gameid";

    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/wechatAgentLogin"})
    public ModelAndView wechatValidate(HttpServletRequest request) {
        System.out.println("-----loginValidate--");
        request.getSession().setAttribute(LOGIN_TYPE_KEY, request.getParameter(LOGIN_TYPE_KEY));
        request.getSession().setAttribute(GAMEID_KEY, request.getParameter(GAMEID_KEY));

        String wechatUtl = GameagentHelp.getWechatBaseUrl(
                DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_APPID),
                DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.AGENT_BASEINFO_URL));

        return new ModelAndView("redirect:" + wechatUtl);
    }

    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/wechatBaseInfo"})
    public ModelAndView wechatBaseInfo(HttpServletRequest request) {
        logger.error("-----wechatBaseInfo-----");
        String code = request.getParameter("code");
        logger.error("code----:" + code);


        if (!StringUtils.isEmpty(code)) {

            try {
                JSONObject tokenJson = WechatUtil.getAccessToken(code,
                        DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_APPID),
                        DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_SECRET));
                logger.error("tokenJson----:" + tokenJson.toString());

                return this.getBaseInfoOptionsModelView(tokenJson, request);

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("获取微信信息出错：" + e.getMessage());
            }

        } else {
            return this.getRefreshPageView(request);
        }
        return new ModelAndView("/agent/error");

    }

    private ModelAndView getBaseInfoOptionsModelView(JSONObject tokenJson, HttpServletRequest request) {
        String openId = JSONUtil.toString(tokenJson, "opneid");

        String loginType = (String) request.getSession().getAttribute(LOGIN_TYPE_KEY);

        logger.info("---获取微信类型----" + loginType);
        if (ContextConstants.WECHAT_LOGIN_TYPE_DOWNLOAD.equals(loginType)) {

            String wechatUtl = GameagentHelp.getWechatAuthUrl(
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_APPID),
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.AGENT_USERINFO_URL));
            return new ModelAndView("redirect:" + wechatUtl);

        } else {
            Gameagent gameagent = this.gameagentRepository.findByWebopenid(openId);


            if (gameagent != null && !StringUtils.isEmpty(gameagent.getId())) {

                HttpSession session = request.getSession();

                String datestr = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis()));

                if (!datestr.equals(gameagent.getDaystr())) {
                    gameagent.setDaystr(datestr);
                    gameagent.setDayearn(0.0);
                    gameagent.setDaycharge(0.0);
                }

                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT, gameagent);
                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_OPEN_ID, openId);
                PlayUser playUser = this.playUserRepository.findByGameid(gameagent.getGameid());

                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT, playUser);

                if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT.equals(loginType)) {

                    ModelAndView view = new ModelAndView("/agent/home");
                    view.addObject("gameagent", request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT));

                    return view;
                } else if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT_RECHARGE.equals(loginType)) {

                    ModelAndView view = new ModelAndView("/agent/money/recharge");
                    view.addObject("playUser", playUser);
                    view.addObject("shopItems", this.getAgentRechargeShopItem());

                    return view;
                }
            } else {
                logger.info("-----not find agent-----" + openId);

                String wechatUtl = GameagentHelp.getWechatAuthUrl(
                        DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_APPID),
                        DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.AGENT_USERINFO_URL));
                return new ModelAndView("redirect:" + wechatUtl);
            }

        }
        return new ModelAndView("/agent/error");
    }

    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/wechatUserInfo"})
    public ModelAndView wechatUserInfo(HttpServletRequest request) {
        String code = request.getParameter("code");
        System.out.println("-----wechatUserInfo-----" + code);
        try {


            JSONObject tokenJson = WechatUtil.getAccessToken(code,
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_APPID),
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_AGENT_SECRET));

            logger.info("---Get tokenjson userinfo  ---:" + tokenJson.toString());
            if (tokenJson != null && tokenJson.has("access_token")) {

                String access_token = JSONUtil.toString(tokenJson, "access_token");
                String openid = JSONUtil.toString(tokenJson, "openid");
                JSONObject userData = WechatUtil.getWeUserInfo(access_token, openid);

                logger.info("---Get weixin userinfo  ---:" + userData.toString());

                return this.getWexinUserInfoView(userData, request);
            } else {

                logger.info("---refresh page  ---");


                return this.getRefreshPageView(request);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("---Get weixin userinfo error ---" + e.getMessage());
        }
        return new ModelAndView("/agent/error");

    }


    private ModelAndView getRefreshPageView(HttpServletRequest request) {


        String loginType = (String) request.getSession().getAttribute(LOGIN_TYPE_KEY);

        logger.info("----reflush page----" + loginType);

        if (ContextConstants.WECHAT_LOGIN_TYPE_DOWNLOAD.equals(loginType)) {

            String downloadUrl = TeahouseUtil.getConstantsByName("app_download_url");
            if (StringUtils.isNotBlank(downloadUrl)) {
                return new ModelAndView("redirect:" + downloadUrl);
            }

        } else if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT.equals(loginType)) {

            if (request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT) != null) {

                ModelAndView view = new ModelAndView("/agent/home");
                view.addObject("gameagent", request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT));

                return view;

            } else {
                return new ModelAndView("/agent/login");
            }


        } else if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT_RECHARGE.equals(loginType)) {

            if (request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT) != null) {
                ModelAndView view = new ModelAndView("/agent/money/recharge");
                view.addObject("playUser",
                        request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT));
                view.addObject("shopItems", this.getAgentRechargeShopItem());

                return view;
            }
        }
        return new ModelAndView("/agent/error");
    }


    private ModelAndView getWexinUserInfoView(JSONObject weixinJson, HttpServletRequest request) {
        String openId = JSONUtil.toString(weixinJson, "openid");

        String loginType = (String) request.getSession().getAttribute(LOGIN_TYPE_KEY);
        String gameid = (String) request.getSession().getAttribute(GAMEID_KEY);
        String weixinjoinInfo = WechatUtil.weixinJoinInfo(weixinJson);

        if (ContextConstants.WECHAT_LOGIN_TYPE_DOWNLOAD.equals(loginType)) {

            logger.info("----下载 download redirect----" + loginType);
            Gameagentutil.webdownload(weixinjoinInfo, openId, gameid);

            String downloadUrl = TeahouseUtil.getConstantsByName("app_download_url");
            if (StringUtils.isNotBlank(downloadUrl)) {
                return new ModelAndView("redirect:" + downloadUrl);
            }

        } else {


            logger.info("----后台登录或者充值 download redirect----" + loginType);

            if (Gameagentutil.agentlogin(weixinjoinInfo, openId)) {

                logger.info("----init  agent  success ----");
                Gameagent gameagent = this.gameagentRepository.findByWebopenid(openId);
                HttpSession session = request.getSession();
                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT, gameagent);
                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_OPEN_ID, openId);
                session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT,
                        this.playUserRepository.findByGameid(gameagent.getGameid()));

                if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT.equals(loginType)) {

                    ModelAndView view = new ModelAndView("/agent/home");
                    view.addObject("gameagent", gameagent);

                    return view;
                } else {
                    ModelAndView view = new ModelAndView("/agent/money/recharge");
                    view.addObject("playUser",
                            request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT));
                    view.addObject("shopItems", this.getAgentRechargeShopItem());
                    return view;
                }


            } else {
                logger.info("----不是代理充值----");
                if (ContextConstants.WECHAT_LOGIN_TYPE_AGENT_RECHARGE.equals(loginType)) {

                    Mapinfo mapinfo = mapinfoRepository.findByWebopenid(openId);
                    if (null == mapinfo) {
                        mapinfo = mapinfoRepository.findByWeixininfojoin(weixinjoinInfo);
                    }

                    logger.info("----查询用户关联信息----：[" + weixinjoinInfo + "]-----：" + openId);
                    if (mapinfo != null) {

                        logger.info("----不是代理充值----" + mapinfo.getGameid());
                        PlayUser playUser = this.playUserRepository.findByGameid(mapinfo.getGameid());
                        HttpSession session = request.getSession();
                        session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT, playUser);
                        session.setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_OPEN_ID, openId);

                        ModelAndView view = new ModelAndView("/agent/money/recharge");
                        view.addObject("playUser", playUser);
                        view.addObject("shopItems", this.getAgentRechargeShopItem());


                        return view;
                    }

                }
                return new ModelAndView("/agent/invalid");
            }

        }
        return new ModelAndView("/agent/error");
    }


    private List<Shop> getAgentRechargeShopItem() {
        return this.shopRepository.findByShoptype(ContextConstants.SHOP_ITEM_WEB);
    }


    @RequestMapping(value = "/login")
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response, @Valid Gameagent gameagent) {
        /*ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/"));
        if (request.getSession(true).getAttribute(BMDataContext.USER_SESSION_NAME) == null) {
            if (gameagent != null && gameagent.getUsercode() != null) {
                final Gameagent loginagent = gameagentRepository.findByUsercodeAndPasswd(gameagent.getUsercode(), UKTools.md5(gameagent.getPasswd()));
                if (loginagent != null && !StringUtils.isBlank(loginagent.getId())) {
                    //loginagent.setLogin(true);
                    this.setAgent(request, loginagent);
                    view.addObject("referer", "/agent/login");//跳转的对应页面
                } else {
                    view = request(super.createRequestPageTempletResponse("/agent/login"));

                    view.addObject("msg", "0");
                }
            }
        }*/
        return request(super.createRequestPageTempletResponse("/agent/login"));
    }


    @RequestMapping({"/info"})
    public ModelAndView gameagentinfo(ModelMap map, HttpServletRequest request) {

        map.addAttribute("gameagent", this.getGameagent(request));

        return new ModelAndView("/agent/login");
    }

    @RequestMapping({"/myplayer"})
    public ModelAndView myplayer(ModelMap map, HttpServletRequest request) {

        return new ModelAndView("/agent/player/myplayer");
    }



    @RequestMapping({"/myagent"})
    public ModelAndView myagent(ModelMap map, HttpServletRequest request) {

        return new ModelAndView("/agent/player/myagent");
    }


    @RequestMapping({"/change_pwd"})
    public ModelAndView change_pwd(ModelMap map, HttpServletRequest request) {

        return new ModelAndView("/agent/setting/change_pwd");
    }

    @RequestMapping({"/agentAgreement"})
    public ModelAndView agentAgreement(ModelMap map, HttpServletRequest request) {

        return new ModelAndView("/agent/agentAgreement");
    }

    @RequestMapping({"/baseinfo"})
    public ModelAndView gameagentBaseinfo(ModelMap map, HttpServletRequest request) {

        map.addAttribute("gameagent", this.getGameagent(request));

        return new ModelAndView("/agent/info/baseinfo");
    }

    @RequestMapping({"/logout"})
    public ModelAndView logout(ModelMap map, HttpServletRequest request) {
        request.getSession().removeAttribute(ContextConstants.SESSESION_KEY_WEIXIN_PLAY_ACCOUNT);
        request.getSession().removeAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);

        return new ModelAndView("/agent/login");
    }

    @RequestMapping({"/testAgentView"})
    public ModelAndView testAgentView(ModelMap map, HttpServletRequest request) {

        String gameid = request.getParameter("gameid");
        if (StringUtils.isNotBlank(gameid)) {

            Gameagent gameagent = this.gameagentRepository.findByGameid(gameid);
            if (gameagent != null) {

                request.getSession().setAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT, gameagent);
                ModelAndView view = new ModelAndView("/agent/home");
                view.addObject("gameagent",
                        request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT));

                map.addAttribute("gameagent", this.getGameagent(request));
                return view;
            }
        }
        return new ModelAndView("/agent/invalid");
    }



    @RequestMapping({"/toMain"})
    public ModelAndView toMain(ModelMap map, HttpServletRequest request) {

        Gameagent gameagent = (Gameagent) request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);
        if (gameagent != null) {
            ModelAndView view = new ModelAndView("/agent/home");
            view.addObject("gameagent",
                    request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT));

            map.addAttribute("gameagent", this.getGameagent(request));
            return view;
        }

        return new ModelAndView("/agent/invalid");
    }



    @RequestMapping({"/myplayuser"})
    public ModelAndView myplayuser(ModelMap map, HttpServletRequest request, @Valid String gameid) {
        List<Useragentmap> useragentmaps = useragentmapRepository.queryUserByAgentid(this.getGameagent(request).getGameid());
        map.addAttribute("useragentmaps", useragentmaps);
        map.addAttribute("allnum", useragentmaps.size());
        map.addAttribute("daynum",
                useragentmapRepository.queryByLastmodify(new SimpleDateFormat("yyyyMMdd").format(
                        new Date(System.currentTimeMillis()))));

        return new ModelAndView("/apps/business/maziplatform/historyroom/index");
    }


    @RequestMapping({"/querybygameid"})
    public ModelAndView querybygameid(ModelMap map, HttpServletRequest request, @Valid String gameid) {
        List<Useragentmap> useragentmaps = useragentmapRepository.queryUserByAgentid(this.getGameagent(request).getGameid());

        Useragentmap useragentmap = useragentmapRepository.findByGameid(gameid);


        map.addAttribute("useragentmap", useragentmap);

        map.addAttribute("useragentmaps", useragentmaps);
        map.addAttribute("allnum", useragentmaps.size());
        map.addAttribute("daynum",
                useragentmapRepository.queryByLastmodify(new SimpleDateFormat("yyyyMMdd").format(
                        new Date(System.currentTimeMillis()))));

        return new ModelAndView("/apps/business/maziplatform/historyroom/index");
    }


}
