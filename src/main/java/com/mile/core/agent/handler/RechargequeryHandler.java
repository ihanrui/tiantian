package com.mile.core.agent.handler;

import com.mile.common.constants.ContextConstants;
import com.mile.common.constants.SystemDicKeyConstants;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.MapinfoRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.hall.entity.Shop;
import com.mile.core.hall.jpa.ShopRepository;
import com.mile.util.DicUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/agent/gameagent")
public class RechargequeryHandler extends Handler {

    @Autowired
    private GameagentRepository gameagentRepository;

    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private MapinfoRepository mapinfoRepository;

    @Autowired
    private UseragentmapRepository useragentmapRepository;

    private static final Logger logger = LoggerFactory.getLogger(RechargequeryHandler.class);

    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/rechargelist"})
    public ModelAndView recchargelist(HttpServletRequest request) {
        return new ModelAndView("/agent/money/recharge_query");

    }
    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/goldcoinQuery"})
    public ModelAndView goldcoinQuery(HttpServletRequest request) {
        return new ModelAndView("/agent/money/goldcoin_query");

    }

    /**
     * 微信游戏配置列表 页面跳转
     *
     * @return
     */
    @RequestMapping({"/rechargequery"})
    public ModelAndView recchargequery(HttpServletRequest request, String userid, Date begindate, Date enddate) {
        logger.info("查询条件 userid[" + userid + "] begindate[" + begindate + "] enddate[" + enddate + "]");
        return new ModelAndView("/agent/money/recharge_query");

    }

    @RequestMapping({"/recharge_settlement"})
    public ModelAndView recharge_settlement(ModelMap map, HttpServletRequest request) {

        Gameagent gameagent = (Gameagent) request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);

        if(gameagent != null){
            gameagent = this.gameagentRepository.findById(gameagent.getId());
        }
        ModelAndView view = new ModelAndView("/agent/money/recharge_settlement");
        String max  = DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.SETTLEMENT_MAX);
        String min = DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.SETTLEMENT_MIN);
        double balamt = 0;
        double withdrawamt = 0;
        if (gameagent != null && gameagent.getBalamt() != null) {
            balamt = gameagent.getBalamt()/100;
        }
        if (gameagent != null && gameagent.getWithdrawamt() != null) {
            withdrawamt = gameagent.getWithdrawamt()/100;
        }
        if(StringUtils.isBlank(max)){
            max = "0";
        }
        if(StringUtils.isBlank(min)){
            min = "0";
        }
        map.addAttribute("balamt", String.valueOf(balamt));
        map.addAttribute("withdrawamt", String.valueOf(withdrawamt));
        map.addAttribute("maxsettlement", max);
        map.addAttribute("minsettlement", min);

        view.addObject("balamt", String.valueOf(balamt));
        view.addObject("withdrawamt", String.valueOf(withdrawamt));
        view.addObject("maxsettlement", max);
        view.addObject("minsettlement", min);

        return view;
    }

    @RequestMapping({"/goldCoinlist"})
    public ModelAndView goldcoinFromUser(ModelMap map, HttpServletRequest request) {

        Gameagent gameagent = (Gameagent) request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);
        PlayUser playUser = null;
        if(gameagent != null){
            playUser = this.playUserRepository.findByGameid(gameagent.getGameid());

        }

        ModelAndView view = new ModelAndView("/agent/money/goldcoin_fromuser");
        String max  = DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.GOLDCOIN_MAX);
        String min = DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.GOLDCOIN_MIN);
        int goldCoins = 0;

        if (playUser != null && playUser.getGoldcoins() != null) {
            goldCoins = playUser.getGoldcoins();
        }

        if(StringUtils.isBlank(max)){
            max = "0";
        }
        if(StringUtils.isBlank(min)){
            min = "0";
        }

        view.addObject("goldCoins", String.valueOf(goldCoins));
        view.addObject("goldcoin_max", max);
        view.addObject("goldcoin_min", min);

        return view;
    }





    @RequestMapping({"/recharge_touser"})
    public ModelAndView recharge_touser(ModelMap map, HttpServletRequest request) {
        ModelAndView view = new ModelAndView("/agent/money/recharge_touser");
        Gameagent gameagent = (Gameagent) request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);

        PlayUser playUser = this.playUserRepository.findByGameid(gameagent.getGameid());
        view.addObject("playUser", playUser);
        view.addObject("shopItems", this.getAgentRechargeShopItem());

        return view;
    }

    private List<Shop> getAgentRechargeShopItem() {
        return this.shopRepository.findByShoptype(ContextConstants.SHOP_ITEM_WEB);
    }

    @RequestMapping({"/recharge_transfer"})
    public ModelAndView recharge_transfer(ModelMap map, HttpServletRequest request) {

        Gameagent gameagent = (Gameagent) request.getSession().getAttribute(ContextConstants.SESSESION_KEY_WEIXIN_GAME_ACCOUNT);
        PlayUser playUser = this.playUserRepository.findByGameid(gameagent.getGameid());
        ModelAndView view = new ModelAndView("/agent/money/recharge_transfer");

        double balamt = 0;
        double diamonds = 0;
        if (playUser != null && playUser.getDiamonds() != null) {
            diamonds = playUser.getDiamonds();
        }

        map.addAttribute("diamonds", diamonds);
        view.addObject("diamonds", diamonds);

        return view;

    }

}