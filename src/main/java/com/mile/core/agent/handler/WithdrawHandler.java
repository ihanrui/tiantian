package com.mile.core.agent.handler;

import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.entity.MoveDiamond;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.entity.Withdrawinfo;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.IncomepersentRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.agent.jpa.WithdrawinfoRepository;
import com.mile.core.agent.msgmodel.Myuser;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代理系统，  我的玩家请求信息
 */
@Controller
@RequestMapping("/agent/withdraw")
public class WithdrawHandler extends Handler {

    private static final Logger logger = LoggerFactory.getLogger(WithdrawHandler.class);

    @Autowired
    private WithdrawinfoRepository withdrawinfoRepository;

    @Autowired
    private GameagentRepository gameagentRepository;


    @Autowired
    private PlayUserRepository playUserRepository;

    /**
     * 我的玩家
     *
     * @param request
     * @param response
     * @param gameid
     * @param size
     * @param page
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/querymyrecord"}, method = RequestMethod.GET)
    public ResponseEntity querymyrecord(HttpServletRequest request, HttpServletResponse response
            , Integer size, Integer page,String chargetype) {

        try {

            logger.info("查询我的提现记录 chargetype["+chargetype+"]");

            if (null == size) {
                size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
            }

            if (null == page) {
                page = 0;
            }

            PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "createtime"));

            List<Withdrawinfo> withdrawinfoList =  null;
            if(StringUtils.isEmpty(chargetype)){
                withdrawinfoList = withdrawinfoRepository.findByApplyidOrderByLastmodifyDesc(this.getGameagent(request).getGameid());
            }else{
                withdrawinfoList = withdrawinfoRepository.findByApplyidAndChargetypeOrderByLastmodifyDesc(this.getGameagent(request).getGameid(),chargetype);

            }



            if(null ==  withdrawinfoList)
                withdrawinfoList= new ArrayList<Withdrawinfo>();

            Map map = new HashMap();
            map.put("totalsize",withdrawinfoList.size());
            map.put("data",withdrawinfoList);

            return new ResponseEntity(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("查询我的提现记录", e);
            return new ResponseEntity("查询我的提现记录失败", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping({"/apply"})
    public ResponseEntity apply(HttpServletRequest request, Integer withdrawamt,String chargetype) {
        try {
            Gameagent gameagent = this.getGameagent(request);

            PlayUser playUser  = playUserRepository.findByGameid(gameagent.getGameid());


            Withdrawinfo withdrawinfo = new Withdrawinfo();
            withdrawinfo.setApplyflag("A");
            withdrawinfo.setWithdrawamt(withdrawamt);
            withdrawinfo.setApplyid(gameagent.getGameid());

            if(DataConstants.Chargetype.AMT.toString().equals(chargetype)) {
                if (withdrawamt > gameagent.getBalamt()) {
                    return new ResponseEntity("申请金额[" + withdrawamt + "]不能大于余额[" + gameagent.getBalamt() + "]", HttpStatus.OK);
                }

                withdrawinfo.setBalbegin(gameagent.getBalamt());
                gameagent.setBalamt(gameagent.getBalamt() - withdrawamt);
                withdrawinfo.setBalend(gameagent.getBalamt());
            }else{
                if(withdrawamt > playUser.getGoldcoins() ){
                    return new ResponseEntity("申请豆子[" + withdrawamt + "]不能大于豆子[" + playUser.getGoldcoins()  + "]", HttpStatus.OK);
                }

                withdrawinfo.setBalbegin(playUser.getGoldcoins());
                playUser.setGoldcoins(playUser.getGoldcoins() - withdrawamt);
                withdrawinfo.setBalend(playUser.getGoldcoins());
            }


            withdrawinfo.setUsername(gameagent.getUsername());
            withdrawinfo.setUserurl(gameagent.getUserurl());
            withdrawinfo.setChargetype(chargetype);
            withdrawinfoRepository.save(withdrawinfo);

            if(DataConstants.Chargetype.AMT.toString().equals(chargetype)) {
                gameagentRepository.save(gameagent);
            }else{
                playUserRepository.save(playUser);

                this.setAgent(request, gameagent);
                MaziScoreUtil.synchronizationUsercache(playUser);
            }
            return new ResponseEntity("提现申请成功", HttpStatus.OK);
        } catch (Exception e) {
            logger.error("提现申请失败" + e.getMessage(), e);
            return new ResponseEntity("提现申请失败", HttpStatus.OK);
        }
    }


    @RequestMapping({"/approve"})
    public ResponseEntity approve(HttpServletRequest request, String  withdrawid,String applyflag) {
        try {

            logger.info("提现申请 applyflag["+applyflag+"]  withdrawid["+withdrawid+"]");
            Withdrawinfo withdrawinfo = withdrawinfoRepository.findById(withdrawid);

            if(null == withdrawinfo){
                return new ResponseEntity("提现记录获取为空", HttpStatus.OK);
            }
            withdrawinfo.setApplyflag(applyflag);

            withdrawinfoRepository.save(withdrawinfo);
            return new ResponseEntity("提现状态修改成功", HttpStatus.OK);
        } catch (Exception e) {
            logger.error("提现申请失败" + e.getMessage(), e);
            return new ResponseEntity("提现申请失败", HttpStatus.OK);
        }
    }
}
