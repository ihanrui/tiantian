package com.mile.core.agent.handler;

import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.IncomepersentRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.agent.msgmodel.Myuser;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import com.mile.core.game.jpa.PlayUserRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * 代理系统，  我的玩家请求信息
 */
@Controller
@RequestMapping("/agent/myuser")
public class MyuserHandler extends Handler{

	private static final Logger logger = LoggerFactory.getLogger(MyuserHandler.class);

	@Autowired
	private GameagentRepository gameagentRepository ;

	@Autowired
	private PlayUserRepository playUserRepository ;


	@Autowired
	private UseragentmapRepository useragentmapRepository ;


	@Autowired
	private IncomepersentRepository incomepersentRepository;

	/**
	 * 我的玩家
	 * @param request
	 * @param response
	 * @param gameid
	 * @param size
	 * @param page
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = {"/querybygameid"}, method = RequestMethod.GET)
	public ResponseEntity querybygameid(HttpServletRequest request, HttpServletResponse response, String gameid
	,Integer size,Integer page) {

		try {

			logger.info("查询我的玩家  gameid[" + gameid + "] this.getGameagent(request).getGameid() ["+this.getGameagent(request).getGameid()+"]");

			if (null == size) {
				size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
			}

			if (null == page) {
				page = 0;
			}

			PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "createtime"));

			List<Useragentmap> useragentmaps = null;
			if (StringUtils.isEmpty(gameid))
				useragentmaps = useragentmapRepository.queryUserByAgentid(this.getGameagent(request).getGameid());
			else
				useragentmaps = useragentmapRepository.queryUserByAgentid(this.getGameagent(request).getGameid(), gameid);

			if(null == useragentmaps) {
				useragentmaps = new ArrayList<Useragentmap>();
			}

			return new ResponseEntity(new Myuser(useragentmaps,useragentmaps.size(),incomepersentRepository.findByDistance(1).getPersent()), HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			logger.error("查询我的玩家失败",e);
			return new ResponseEntity("查询我的玩家失败", HttpStatus.BAD_REQUEST);
		}
	}


}
