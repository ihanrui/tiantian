package com.mile.core.agent.handler;

import com.mile.core.DataConstants;
import com.mile.core.agent.entity.Agentcharge;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.entity.Incomedetails;
import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.jpa.AgentchargeRepository;
import com.mile.core.agent.jpa.IncomedetailsRepository;
import com.mile.core.agent.msgmodel.Myuser;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.web.handler.Handler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 充值查询
 */
@Controller
@RequestMapping("/agent/chargequery")
public class ChargequeryHandler extends Handler{

	private static final Logger logger = LoggerFactory.getLogger(ChargequeryHandler.class);

	@Autowired
	private PlayUserRepository playUserRepository ;


	@Autowired
	private IncomedetailsRepository incomedetailsRepository ;











	@SuppressWarnings("rawtypes")
	@RequestMapping(value = {"/querybygameid"}, method = RequestMethod.GET)
	public ResponseEntity querybygameid(HttpServletRequest request, HttpServletResponse response, String gameid
			, Integer size, Integer page,String date_begin,String date_end,String chargetype) {

		try {

			logger.info("查询玩家充值返利  gameid[" + gameid + "] this.getGameagent(request).getGameid() ["+this.getGameagent(request).getGameid()+"] chargetype["+chargetype+"]");

			if (null == size) {
				size = new Integer(TeahouseUtil.getConstantsByName(DataConstants.TEAHOUSE_CONST_PAGESIZE));
			}

			if (null == page) {
				page = 0;
			}

			PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "createtime"));

			List<Incomedetails> incomedetailsList = null;


			if(StringUtils.isEmpty(date_begin))
				date_begin = "19900000";

			if(StringUtils.isEmpty(date_end))
				date_end = "99900000";

			if(StringUtils.isEmpty(chargetype)) {
			if (StringUtils.isEmpty(gameid)) {
				incomedetailsList = incomedetailsRepository.findByAgentidAndLastmodifyBetweenOrderByLastmodifyDesc(this.getGameagent(request).getGameid(),date_begin,date_end);
			}else {
				incomedetailsList = incomedetailsRepository.findByAgentidAndGameidAndLastmodifyBetweenOrderByLastmodifyDesc(this.getGameagent(request).getGameid(), gameid,date_begin,date_end);
			}
			}else{
				if (StringUtils.isEmpty(gameid)) {
					incomedetailsList = incomedetailsRepository.findByAgentidAndChargetypeAndLastmodifyBetweenOrderByLastmodifyDesc(this.getGameagent(request).getGameid(),chargetype,date_begin,date_end);
				}else {
					incomedetailsList = incomedetailsRepository.findByAgentidAndGameidAndChargetypeAndLastmodifyBetweenOrderByLastmodifyDesc(this.getGameagent(request).getGameid(), gameid,chargetype,date_begin,date_end);
				}
			}

			if(null == incomedetailsList) {
				incomedetailsList = new ArrayList<Incomedetails>();
			}


			Map map = new HashMap();
			map.put("totalsize",incomedetailsList.size());
			map.put("data",incomedetailsList);
			return new ResponseEntity(map, HttpStatus.OK);

		}catch (Exception e){
			e.printStackTrace();
			logger.error("查询玩家充值返利信息失败",e);
			return new ResponseEntity("查询玩家充值返利信息失", HttpStatus.BAD_REQUEST);
		}
	}









}
