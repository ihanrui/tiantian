package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Chargedetails;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface ChargedetailsRepository extends JpaRepository<Chargedetails, String>
{
  public abstract Chargedetails findById(String id);
}
