package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Gameagent;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface GameagentRepository extends JpaRepository<Gameagent, String>
{
  public abstract Gameagent findById(String id);

  public abstract Gameagent findByGameid(String gameid);


  public abstract Gameagent findByUsercodeAndPasswd(String usercode,String passwd);


  public abstract Gameagent findByWebopenid(String webopenid);




  public abstract void deleteByGameid(String gameid);
}
