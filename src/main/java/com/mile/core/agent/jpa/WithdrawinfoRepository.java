package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.agent.entity.Withdrawinfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface WithdrawinfoRepository extends JpaRepository<Withdrawinfo, String>
{
  public abstract Withdrawinfo findById(String id);


  public abstract List<Withdrawinfo> findByApplyid(String applyid);

  public abstract List<Withdrawinfo> findByApplyidOrderByLastmodifyDesc(String applyid);



  public abstract List<Withdrawinfo> findByApplyidAndApplyflag(String applyid,String applyflag);



  public abstract Page<Withdrawinfo> findByApplyflag(String applyflag, Pageable pageable);



  public abstract List<Withdrawinfo> findByApplyidAndChargetypeOrderByLastmodifyDesc(String applyid,String chargetype);


}
