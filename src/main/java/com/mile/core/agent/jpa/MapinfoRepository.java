package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Mapinfo;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface MapinfoRepository extends JpaRepository<Mapinfo, String>
{
  public abstract Mapinfo findById(String id);

  public abstract Mapinfo findByWebopenid(String webopenid);

  public abstract Mapinfo findByWeixininfojoin(String joinInfo);

  public abstract Mapinfo findByAppopenid(String appopenid);


  public abstract void deleteByGameid(String gameid);






}
