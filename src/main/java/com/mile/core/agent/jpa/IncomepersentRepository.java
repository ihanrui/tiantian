package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Incomepersent;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface IncomepersentRepository extends JpaRepository<Incomepersent, String>
{
  public abstract Incomepersent findById(String id);

  public abstract  Incomepersent findByDistance(int distance);
}
