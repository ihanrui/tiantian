package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Useragentmap;
import com.mile.core.entity.PlayUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public abstract interface UseragentmapRepository extends JpaRepository<Useragentmap, String>
{
  public abstract Useragentmap findById(String id);

  public abstract Useragentmap findByGameid(String gameid);


  @Query(value="select t1.*  from wd_a_useragentmap t1 " +
          "where (t1.agent1 = :agentid or t1.agent2 = :agentid or t1.agent3 = :agentid  or t1.agent4 = :agentid )" +
          " and  t1.gameid not in (select t2.gameid from wd_a_gameagent t2 )"
          , nativeQuery = true)
  public List<Useragentmap> queryUserByAgentid(@Param("agentid") String agentid);


  @Query(value="select t1.*  from wd_a_useragentmap t1 " +
          "where (t1.agent1 = :agentid or t1.agent2 = :agentid or t1.agent3 = :agentid  or t1.agent4 = :agentid )" +
          " and  t1.gameid not in (select t2.gameid from wd_a_gameagent t2 ) and t1.gameid =:selfid "
          , nativeQuery = true)
  public List<Useragentmap> queryUserByAgentid(@Param("agentid") String agentid,@Param("selfid") String selfid);



  @Query(value="select count(t1.*)  from wd_a_useragentmap t1 " +
          " where t1.lastmodify like %?1%  )"
          , nativeQuery = true)
  public int queryByLastmodify(@Param("lastmodify") String lastmodify);



  @Query(value="select t1.*  from wd_a_useragentmap t1 " +
          "where (t1.agent1 = :agentid or t1.agent2 = :agentid or t1.agent3 = :agentid  or t1.agent4 = :agentid )" +
          " and  t1.gameid  in (select t2.gameid from wd_a_gameagent t2 )", nativeQuery = true)
  public List<Useragentmap> queryAgentByAgentid(@Param("agentid") String agentid);


  @Query(value="select t1.*  from wd_a_useragentmap t1 " +
          "where (t1.agent1 = :agentid or t1.agent2 = :agentid or t1.agent3 = :agentid  or t1.agent4 = :agentid )" +
          " and  t1.gameid  in (select t2.gameid from wd_a_gameagent t2 )  and t1.gameid =:selfid ", nativeQuery = true)
  public List<Useragentmap> queryAgentByAgentid(@Param("agentid") String agentid,@Param("selfid")  String selfid);






  @Query(value=" select t1.*  from wd_a_useragentmap t1 " +
          " where t1.gameid = :gameid " +
          " and  t1.gameid  in (select t2.gameid from wd_a_gameagent t2 )"
          , nativeQuery = true )
  public abstract Useragentmap queryAgentByGameid(String gameid);



  public abstract void deleteByGameid(String gameid);


  public abstract Page<Useragentmap> findByGameidLike(String gameid, Pageable paramPageable);

}
