package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Agentcharge;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface AgentchargeRepository extends JpaRepository<Agentcharge, String>
{
  public abstract Agentcharge findById(String id);


  public abstract List<Agentcharge> findByAgentid(String id);


  public abstract List<Agentcharge> findByAgentidOrderByLastmodifyDesc(String agentid);

  public abstract List<Agentcharge> findByAgentidAndGameidOrderByLastmodifyDesc(String agentid,String gameid);
}
