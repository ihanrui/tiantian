package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.Incomedetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface IncomedetailsRepository extends JpaRepository<Incomedetails, String>
{
  public abstract Incomedetails findById(String id);

  public abstract List<Incomedetails> findByGameidOrderByLastmodifyDesc(String gameid);

  public abstract List<Incomedetails> findByAgentidOrderByLastmodifyDesc(String agentid);


  public abstract List<Incomedetails> findByAgentidAndLastmodifyBetweenOrderByLastmodifyDesc(String agentid,String lastmodifys,String lastmodifye);


  public abstract List<Incomedetails> findByAgentidAndChargetypeAndLastmodifyBetweenOrderByLastmodifyDesc(String agentid,String chargetype,String lastmodifys,String lastmodifye);


  public abstract List<Incomedetails> findByAgentidAndGameidOrderByLastmodifyDesc(String agentid,String gameid);



  public abstract List<Incomedetails> findByAgentidAndGameidAndLastmodifyBetweenOrderByLastmodifyDesc(String agentid,String gameid,String lastmodifys,String lastmodifye);



  public abstract List<Incomedetails> findByAgentidAndGameidAndChargetypeAndLastmodifyBetweenOrderByLastmodifyDesc(String agentid,String gameid,String chargetype,String lastmodifys,String lastmodifye);






}
