package com.mile.core.agent.jpa;

import com.mile.core.agent.entity.MoveDiamond;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface MoveDiamondRepository extends JpaRepository<MoveDiamond, String>
{
  public abstract MoveDiamond findById(String id);


  public abstract List<MoveDiamond> findBySourceidOrderByLastmodifyDesc(String sourceid);
}
