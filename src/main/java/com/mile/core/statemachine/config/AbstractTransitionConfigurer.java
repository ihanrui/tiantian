package com.mile.core.statemachine.config;

public interface AbstractTransitionConfigurer<I> {
	I and() ;
}
