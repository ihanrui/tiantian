package com.mile.core.engine.game.eventmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.Message;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.util.UKTools;
import com.mile.core.game.msgmodel.Player;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by michael on 3/28/18.
 * 小结结算
 */


public class StepSumEvent extends BaseEntity implements Message {

    private static final Logger logger = LoggerFactory.getLogger(StepSumEvent.class);

    private static final long serialVersionUID = 1L;

    private String id = UKTools.getUUID();

    private String command;

    private List<StepEvent> stepEvents;

    private String banker;//庄家userid

    private int readysnd;//准备秒数

    public StepSumEvent(Player[] playerList) {
        List<StepEvent> stepEventList = new LinkedList<StepEvent>();
        if (ArrayUtils.isEmpty(playerList)) {
            logger.info("StepSumEvent 构造方法参数为空");
            return;
        }
        for (Player player : playerList) {
            StepEvent stepEvent = new StepEvent(player.getPlayuser(), player.getTotalscore(), player.getStepscore(),
                    MaziCardtypeUtil.settleCards(player.getCards(), null));
            if(player instanceof PinshiPlayer){
                PinshiPlayer p = (PinshiPlayer)player;
                stepEvent.setCardType(p.getPinshicardtype());
            }
            stepEventList.add(stepEvent);
        }
        this.setStepEvents(stepEventList);

    }

    public StepSumEvent(Player[] playerList, String banker) {
        this(playerList);
        this.banker = banker;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public void setCommand(String command) {
        this.command = command;
    }

    public List<StepEvent> getStepEvents() {
        return stepEvents;
    }

    public void setStepEvents(List<StepEvent> stepEvents) {
        this.stepEvents = stepEvents;
    }

    public int getReadysnd() {
        return readysnd;
    }

    public void setReadysnd(int readysnd) {
        this.readysnd = readysnd;
    }
}
