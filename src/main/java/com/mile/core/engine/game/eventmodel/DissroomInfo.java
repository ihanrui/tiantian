package com.mile.core.engine.game.eventmodel;

import com.mile.common.entity.BaseEntity;

public class DissroomInfo extends BaseEntity {
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String userid;

	private String username;

	private String userurl;

	private int dissflag;

	public DissroomInfo(String userid, String username, String userurl, int dissflag) {
		this.userid = userid;
		this.username = username;
		this.userurl = userurl;
		this.dissflag = dissflag;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserurl() {
		return userurl;
	}

	public void setUserurl(String userurl) {
		this.userurl = userurl;
	}

	public int getDissflag() {
		return dissflag;
	}

	public void setDissflag(int dissflag) {
		this.dissflag = dissflag;
	}
}
