package com.mile.core.engine.game.eventmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.Message;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DissroomEvent extends BaseEntity implements Message{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

    private List<DissroomInfo> dissroomInfoList = new LinkedList<DissroomInfo>();


    private String userid;//发起解散的人

    private int dissflag;

    private int dealflag=0;//0.主动处理 1.超时处理

    private Date dissbegindate;

    private int disssecond;


	private String command;


	public DissroomEvent(){

	}


	public  DissroomEvent(List<DissroomInfo> dissroomInfoList,String userid,int dissflag){
		this.dissroomInfoList = dissroomInfoList;
		this.userid = userid;
	}




	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public List<DissroomInfo> getDissroomInfoList() {
		return dissroomInfoList;
	}

	public void setDissroomInfoList(List<DissroomInfo> dissroomInfoList) {
		this.dissroomInfoList = dissroomInfoList;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getDissflag() {
		return dissflag;
	}

	public void setDissflag(int dissflag) {
		this.dissflag = dissflag;
	}

	public Date getDissbegindate() {
		return dissbegindate;
	}

	public void setDissbegindate(Date dissbegindate) {
		this.dissbegindate = dissbegindate;
	}

	public int getDisssecond() {
		return disssecond;
	}

	public void setDisssecond(int disssecond) {
		this.disssecond = disssecond;
	}

	public int getDealflag() {
		return dealflag;
	}

	public void setDealflag(int dealflag) {
		this.dealflag = dealflag;
	}
}
