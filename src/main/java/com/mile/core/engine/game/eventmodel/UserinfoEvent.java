package com.mile.core.engine.game.eventmodel;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.entity.PlayUserClient;

public class UserinfoEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	public UserinfoEvent(PlayUserClient playUserClient) {
		this.playUserClient = playUserClient;
	}

	public UserinfoEvent() {
	}

	private PlayUserClient playUserClient;

	public PlayUserClient getPlayUserClient() {
		return playUserClient;
	}

	public void setPlayUserClient(PlayUserClient playUserClient) {
		this.playUserClient = playUserClient;
	}
}
