package com.mile.core.engine.game.task;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.PinshiDataConstants;
import com.mile.core.agent.util.Gamecoinutil;
import com.mile.core.entity.PlayUser;
import com.mile.core.game.ActionTaskUtils;
import com.mile.core.game.MileGameEnum;
import com.mile.core.game.MileGameEvent;
import com.mile.core.game.BeiMiGameTask;
import com.mile.core.engine.game.impl.Banker;
import com.mile.core.engine.game.impl.UserBoard;
import com.mile.core.game.mazi.util.MaziTipsUtil;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import com.mile.core.game.pinshi.msgmodel.PinshiUserBoard;
import com.mile.core.game.pinshi.util.PinshiRoomDescUtil;
import com.mile.core.game.util.RoomUtil;
import com.mile.core.game.util.SysdicUtil;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.core.record.util.GamerecordUtil;
import com.mile.core.teahouse.eventmodel.NoticeEvent;
import com.mile.core.teahouse.util.TeahouseActionTaskUtils;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.GameUtils;
import com.mile.common.cache.CacheHelper;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;
import org.apache.commons.lang3.StringUtils;
import org.cache2k.expiry.ValueWithExpiryTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;

public class CreateBeginTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

    private static final Logger logger = LoggerFactory.getLogger(CreateBeginTask.class);

    private long timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateBeginTask(long timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * 1000;    //5秒后执行
    }

    public void execute() {

        try {

            if (gameRoom.getNumofgames() <= gameRoom.getCurrentnum() || gameRoom.getNumofgames() >= 999999) {//金币场 也要结算

                CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());

                GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.CLEARING.toString(), 0.5);//通知状态机 , 继续执行

                logger.info("局数已经满了，不能再开始。 [" + gameRoom.getRoomid() + "]");
                return;
            }
            List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi);

            if (MileGameEnum.PLAY.toString().equals(gameRoom.getStatus())) {
                logger.info("游戏[" + gameRoom.getRoomid() + "] 已开始， 请勿重复开始!!!!!!!!!!!!!!!!");
                return;
            }

            /**
             *
             * 顺手 把牌发了，注：此处应根据 GameRoom的类型获取 发牌方式
             */
            boolean inroom = false;

            if (!StringUtils.isBlank(gameRoom.getLastwinner())) {
                for (PlayUserClient player : playerList) {
                    if (player.getId().equals(gameRoom.getLastwinner())) {
                        inroom = true;
                    }
                }
            }
            if (inroom == false || StringUtils.isEmpty(gameRoom.getLastwinner()) || gameRoom.getNumofgames() >= 999999) {//随机一位为庄家，金币场每场随机
                logger.info("第一局，随机一个庄家");
                int winindex = new Random().nextInt(playerList.size() - 1);
                PlayUserClient p = playerList.get(winindex);

                gameRoom.putExtparams(p.getId() + "_index", "" + p.getIndex());
                gameRoom.setLastwinner(p.getId());
            }
            /**
             * 通知所有玩家 新的庄
             */
            if (DataConstants.GAME_PLAYWAY_DIZHU.equals(gameRoom.getGamePlayway().getCode())) {
                ActionTaskUtils.sendEvent("banker", new Banker(gameRoom.getLastwinner()), gameRoom);
            }

            Board board = GameUtils.playGame(playerList, gameRoom, gameRoom.getLastwinner(), gameRoom.getCardsnum());

            gameRoom.setStatus(MileGameEnum.PLAY.toString());

            for (PlayUserClient playerUser : CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi)) {
                playerUser.setGamestatus(BMDataContext.GameStatusEnum.PLAY.toString());
                playerUser.setOnline(true);
                /**
                 * 更新状态到 PLAYING
                 */

                /**
                 * 每个人收到的 牌面不同，所以不用 ROOM发送广播消息，而是用 遍历房间里所有成员发送消息的方式
                 */

                CacheHelper.getGamePlayerCacheBean().put(playerUser.getId(), playerUser, playerUser.getOrgi());
                CacheHelper.getApiUserCacheBean().put(playerUser.getId(), playerUser, orgi);

            }

            for (PlayUserClient playerUser : CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi)) {

                if (DataConstants.GAME_PLAYWAY_MAZI.equals(gameRoom.getGamePlayway().getCode())) {

                    UserBoard userBoard = new UserBoard(board, playerUser.getId(),
                            DataConstants.GAME_EVENT_PLAY, board.getTeamCard(), board.getTeamOwner());
                    logger.info("码子房间信息格式化");
                    MaziTipsUtil.formatRoomdesc(gameRoom, userBoard);
                    ActionTaskUtils.sendEvent(playerUser, userBoard);
                } else if (DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getGamePlayway().getCode())) {
                    logger.info("拼十房间信息格式化");

                    PinshiUserBoard pinshiUserBoard = new PinshiUserBoard(board, playerUser.getId(),
                            DataConstants.GAME_EVENT_PLAY, board.getTeamCard(), board.getTeamOwner());

                    PinshiPlayer myplayer = (PinshiPlayer) pinshiUserBoard.getPlayer().clone();


                    logger.info("开始就发送5张牌，客户端只显示4张");
                    myplayer.getCards()[4]=-1;

                    SummaryPlayer mysum = getSummaryPlayerByUserid(gameRoom, myplayer.getUserid());
                    if (null != mysum && "Y".equals(mysum.getBolusflag())) {//设置是否可以推注
                        logger.info("玩家 ["+myplayer.getUsername()+"] ["+myplayer.getGameid()+"] 可以推注");
                        myplayer.setPushscore(mysum.getBolustimes());
                    }else {
                        logger.info("玩家 ["+myplayer.getUsername()+"] ["+myplayer.getGameid()+"] 不可推注");
                    }

                    pinshiUserBoard.setPlayer(myplayer);

                    for (Player player : pinshiUserBoard.getPlayers()) {
                        PinshiPlayer pinshiPlayer = (PinshiPlayer) player;
                        SummaryPlayer summaryPlayer = getSummaryPlayerByUserid(gameRoom, pinshiPlayer.getUserid());
                        if (null != summaryPlayer && "Y".equals(summaryPlayer.getBolusflag())) {
                            pinshiPlayer.setPushscore(summaryPlayer.getBolustimes());
                        }
                    }

                    PinshiRoomDescUtil.formatRoomdesc(gameRoom, pinshiUserBoard);
                    ActionTaskUtils.sendEvent(playerUser, pinshiUserBoard);
                }

                logger.info("发牌更新缓存 玩家 [" + playerUser.getUsername() + "][" + playerUser.getGameid() + "] 位置[" + playerUser.getIndex() + "]");
            }

            for (PlayUserClient playerUser : CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi)) {
                logger.info("当前分配的playuser信息  " + playerUser.getUsername() + "-------" + playerUser.getIndex());
                if (DataConstants.Roomtype.COIN.toString().equals(gameRoom.getRoomtype())) {
                    logger.info("房间[" + gameRoom.getRoomid() + "]消耗 玩家[" + playerUser.getUsername() + "] [" + playerUser.getGameid() + "]");
                    PlayUser playUser = GamerecordUtil.recordCoin(playerUser.getGameid(), gameRoom.getRoomtype(),
                            -1 * TeahouseUtil.getIntConstantsByName("coinexpend"), gameRoom.getRoomid()
                            , "金币门票消耗", DataConstants.Roomtype.TICKET.toString());
                    TeahouseActionTaskUtils.sendCommandEvent(DataConstants.GLOBAL_GAME_EVENT_NOTICE, playUser.getId(),
                            new NoticeEvent("消耗金币[" + TeahouseUtil.getIntConstantsByName("coinexpend") + "]个.", true));
                    Gamecoinutil.dealCoinIncome(playerUser.getGameid());
                }

            }

            /**
             * 发送一个 Begin 事件
             */
            if (DataConstants.GAME_PLAYWAY_DIZHU.equals(gameRoom.getGamePlayway().getCode())) {
                super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.AUTO.toString(), 2);//通知状态机 , 此处应由状态机处理异步执行
                logger.info("直接进入 抢地主等事件");
            } else if (DataConstants.GAME_PLAYWAY_PINSHI.equals(gameRoom.getGamePlayway().getCode())) {

                for (Player player : board.getPlayers()) {
                    PinshiPlayer pinshiPlayer = (PinshiPlayer) player;
                    pinshiPlayer.setGamestep(PinshiDataConstants.Gamestep.HOG.toString());
                }
                //GameUtils.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, MileGameEvent.BIND.toString(), SysdicUtil.getIntTimesByName(PinshiDataConstants.Gamestep.HOG.toString()));
                logger.info("等待进入抢庄的事件");
            }

            //更新信息到缓存
            CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());

        }catch(Exception e){
            e.printStackTrace();
            logger.error("开始游戏失败",e);
        }


    }





    private SummaryPlayer getSummaryPlayerByUserid(GameRoom gameRoom, String userid) {

        if (null != gameRoom.getSummary() && null != gameRoom.getSummary().getSummaryPlayer()) {
            List<SummaryPlayer> summaryPlayers = gameRoom.getSummary().getSummaryPlayer();

            for (SummaryPlayer summaryPlayer : summaryPlayers) {
                if (summaryPlayer.getUserid().equals(userid)) {
                    return summaryPlayer;
                }

            }
            return null;
        } else
            return null;

    }
}
