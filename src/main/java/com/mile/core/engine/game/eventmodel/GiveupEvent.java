package com.mile.core.engine.game.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class GiveupEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

    private List<GiveupInfo> giveupInfoList = new LinkedList<GiveupInfo>();


    private String userid;//发起解散的人

    private int giveupflag; ////投降标志 0.默认 无操作。 1.同意投降  2.拒绝投降

    private int dealflag=0;//0.主动处理 1.超时处理

    private Date giveupbegindate;

    private int giveupsecond;


	private String command;


	public GiveupEvent(){

	}


	public GiveupEvent(List<GiveupInfo> giveupInfoList, String userid, int giveupflag){
		this.giveupInfoList = giveupInfoList;
		this.userid = userid;
	}




	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public List<GiveupInfo> getGiveupInfoList() {
		return giveupInfoList;
	}

	public void setGiveupInfoList(List<GiveupInfo> giveupInfoList) {
		this.giveupInfoList = giveupInfoList;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getGiveupflag() {
		return giveupflag;
	}

	public void setGiveupflag(int giveupflag) {
		this.giveupflag = giveupflag;
	}

	public int getDealflag() {
		return dealflag;
	}

	public void setDealflag(int dealflag) {
		this.dealflag = dealflag;
	}

	public Date getGiveupbegindate() {
		return giveupbegindate;
	}

	public void setGiveupbegindate(Date giveupbegindate) {
		this.giveupbegindate = giveupbegindate;
	}

	public int getGiveupsecond() {
		return giveupsecond;
	}

	public void setGiveupsecond(int giveupsecond) {
		this.giveupsecond = giveupsecond;
	}
}
