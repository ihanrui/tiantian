package com.mile.core.engine.game.iface;

import java.util.List;

import com.mile.core.game.msgmodel.Board;
import com.mile.core.entity.GamePlayway;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUserClient;

/**
 * 棋牌游戏接口API
 * @author iceworld
 *
 */
public interface ChessGame {
	/**
	 * 创建一局新游戏
	 * @return
	 */
	public Board process(List<PlayUserClient> playUsers , GameRoom gameRoom ,GamePlayway playway ,  String banker , int cardsnum) ;
}
