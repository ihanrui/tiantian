package com.mile.core.engine.game.impl;

import com.mile.common.eventmodel.BaseEvent;
import com.mile.core.game.mazi.msgmodel.MaziBoard;
import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import com.mile.common.cache.CacheHelper;
import com.mile.core.game.msgmodel.Board;
import com.mile.core.game.msgmodel.Player;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.pinshi.msgmodel.PinshiBoard;
import com.mile.core.game.pinshi.msgmodel.PinshiPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

public class UserBoard extends BaseEvent {

    protected static final Logger logger = LoggerFactory.getLogger(UserBoard.class);
    /**
     *
     */
    protected static final long serialVersionUID = -1224310911110772375L;
    protected Player player;
    protected Player[] players;
    protected int deskcards;    //剩下多少张牌

    protected String teamOwner;//摸对的人

    protected byte teamCard;//摸对的牌

    protected String playwaydesc;//玩法描述

    protected String nowgamenum;//当前局数

    protected String roomdesc;//房间 描述

    protected String command;

    protected String gamestatus;


    protected String dissuserid;//发起解散人

    protected Date dissbegindate;//解散开始时间


    protected Integer disssecond=0;//默认解散秒数

    protected Integer dissleftsecond;//剩余秒数




    protected String giveupuserid;//投降发起人

    protected Date giveupbegindate;//解散开始时间


    protected Integer giveupsecond=0;//默认解散秒数

    protected Integer giveupleftsecond;//剩余秒数

    protected Integer giveupteamflag;








    /**
     * 发给玩家的牌，开启特权后可以将某个其他玩家的牌 显示出来
     *
     * @param board
     * @param curruser
     */

    public UserBoard(Board board, String curruser, String command, byte teamCard, String teamOwner) {
        this(board, curruser, command);
        this.setTeamCard(teamCard);
        this.setTeamOwner(teamOwner);

    }

    public UserBoard() {

    }

    public UserBoard(Board board, String curruser, String command) {
        players = new Player[board.getPlayers().length - 1];
        this.command = command;
        if (board.getDeskcards() != null) {
            this.deskcards = board.getDeskcards().size();
        }


        if(board instanceof MaziBoard) {
            maziRecovery(board, curruser);
        }else if(board instanceof PinshiBoard){
            pinshiRecovery(board,curruser);
        }




        this.setDissbegindate(board.getDissbegindate());
        this.setDisssecond(board.getDisssecond());
        this.setDissuserid(board.getDissuserid());
        this.initDissleftsecond(this.getDissbegindate(),new Date());
        this.setGiveupbegindate(board.getGiveupbegindate());
        this.setGiveupsecond(board.getGiveupsecond());
        this.setGiveupuserid(board.getGiveupuserid());
        this.setGiveupteamflag(board.getGiveupteamflag());
        this.initGiveupleftsecond(this.getGiveupbegindate(),new Date());

    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getDeskcards() {
        return deskcards;
    }

    public void setDeskcards(int deskcards) {
        this.deskcards = deskcards;
    }

    public String getTeamOwner() {
        return teamOwner;
    }

    public void setTeamOwner(String teamOwner) {
        this.teamOwner = teamOwner;
    }

    public byte getTeamCard() {
        return teamCard;
    }

    public void setTeamCard(byte teamCard) {
        this.teamCard = teamCard;
    }

    public String getPlaywaydesc() {
        return playwaydesc;
    }

    public void setPlaywaydesc(String playwaydesc) {
        this.playwaydesc = playwaydesc;
    }

    public String getNowgamenum() {
        return nowgamenum;
    }

    public void setNowgamenum(String nowgamenum) {
        this.nowgamenum = nowgamenum;
    }

    public String getRoomdesc() {
        return roomdesc;
    }

    public void setRoomdesc(String roomdesc) {
        this.roomdesc = roomdesc;
    }

    public String getGamestatus() {
        return gamestatus;
    }

    public void setGamestatus(String gamestatus) {
        this.gamestatus = gamestatus;
    }

    public String getDissuserid() {
        return dissuserid;
    }

    public void setDissuserid(String dissuserid) {
        this.dissuserid = dissuserid;
    }

    public Date getDissbegindate() {
        return dissbegindate;
    }

    public void setDissbegindate(Date dissbegindate) {
        this.dissbegindate = dissbegindate;
    }

    public Integer getDisssecond() {
        return disssecond;
    }

    public void setDisssecond(Integer disssecond) {
        this.disssecond = disssecond;
    }

    public Integer getDissleftsecond() {
        return dissleftsecond;
    }

    public void setDissleftsecond(Integer dissleftsecond) {
        this.dissleftsecond = dissleftsecond;
    }


    protected void initDissleftsecond(Date date1,Date date2){
        if(null == date1)
            return ;
        Calendar calendar1= Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2= Calendar.getInstance();
        calendar2.setTime(date2);

        this.setDissleftsecond( calendar1.get(Calendar.SECOND)
                +this.getDisssecond()
                - calendar2.get(Calendar.SECOND));

    }

    public String getGiveupuserid() {
        return giveupuserid;
    }

    public void setGiveupuserid(String giveupuserid) {
        this.giveupuserid = giveupuserid;
    }

    public Date getGiveupbegindate() {
        return giveupbegindate;
    }

    public void setGiveupbegindate(Date giveupbegindate) {
        this.giveupbegindate = giveupbegindate;
    }

    public Integer getGiveupsecond() {
        return giveupsecond;
    }

    public void setGiveupsecond(Integer giveupsecond) {
        this.giveupsecond = giveupsecond;
    }

    public Integer getGiveupleftsecond() {
        return giveupleftsecond;
    }

    public void setGiveupleftsecond(Integer giveupleftsecond) {
        this.giveupleftsecond = giveupleftsecond;
    }


    protected void initGiveupleftsecond(Date date1,Date date2){
        if(null == date1)
            return ;
        Calendar calendar1= Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2= Calendar.getInstance();
        calendar2.setTime(date2);

        this.setGiveupleftsecond( calendar1.get(Calendar.SECOND)
                +this.getGiveupsecond()
                - calendar2.get(Calendar.SECOND));

    }

    public Integer getGiveupteamflag() {
        return giveupteamflag;
    }

    public void setGiveupteamflag(Integer giveupteamflag) {
        this.giveupteamflag = giveupteamflag;
    }

    protected void maziRecovery(Board board, String curruser){
        int inx = 0;
        for (Player temp : board.getPlayers()) {

            PlayUserClient client = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(temp.getPlayuser(), null);
            temp.setGamestatus(client.getGamestatus());
            temp.setOnline(client.isOnline());
            if (temp.getPlayuser().equals(curruser)) {
                player = temp;
                player.setCards(MaziCardtypeUtil.settleCards(player.getCards(), null));

            } else {
                Player clonePlayer = temp.clone();

                if (null != clonePlayer.getCards() && clonePlayer.getCards().length <= 5) {//通知出牌  小于等于5张 报警
                    logger.info("报警处理");
                    clonePlayer.setCardsnum(clonePlayer.getCards().length);
                }

                logger.info("玩家 curruser["+board.player(curruser).getUsername()+"] 是否完成["+board.player(curruser).isOverflag()+"]，" +
                        "编队["+board.player(curruser).getTeamflag()+"] 明牌["+board.player(curruser).isSeeflag()+"]" +
                        " 对家["+clonePlayer.getUsername()+"] 编队["+clonePlayer.getTeamflag()
                        +"] 明牌["+clonePlayer.isSeeflag()+"]");
                if (board.player(curruser).isOverflag()
                        && board.player(curruser).getTeamflag() == clonePlayer.getTeamflag()
                        && !board.player(curruser).getGameid().equals(clonePlayer.getGameid())) {
                    logger.info("打牌完成，设置牌的值");
                    clonePlayer.setCards(MaziCardtypeUtil.settleCards(clonePlayer.getCards(), null));
                }else if (clonePlayer.isSeeflag() ||  (clonePlayer.getTeamflag() == board.player(curruser).getTeamflag()
                        && board.player(curruser).isSeeflag())) {//非名牌才会设置cards为空
                    logger.info("明牌了， 设置牌的值");
                    clonePlayer.setCards(MaziCardtypeUtil.settleCards(clonePlayer.getCards(), null));
                } else {
                    clonePlayer.setCards(null);    //克隆对象，然后将 其他玩家手里的牌清空
                    logger.info("已明牌 或者 自己已名牌，需要看到对家的牌 [" + clonePlayer.getUsername() + "] ");
                }




                players[inx++] = clonePlayer;
            }
        }

    }



    protected void pinshiRecovery(Board board,String curruser){
        int inx = 0;
        for (Player temp : board.getPlayers()) {

            PlayUserClient client = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(temp.getPlayuser(), null);
            temp.setGamestatus(client.getGamestatus());
            temp.setOnline(client.isOnline());
            if (temp.getPlayuser().equals(curruser)) {
                player = temp;

            } else {
                Player clonePlayer = temp.clone();
                PinshiPlayer pinshiPlayer = (PinshiPlayer)clonePlayer;
                if(!"Y".equals(pinshiPlayer.getShowflag())){
                    pinshiPlayer.setPinshicardtype(null);
                    pinshiPlayer.setCards(null);
                }

                if(!"Y".equals(pinshiPlayer.getHogflag())){
                    pinshiPlayer.setHogtimes(-1);
                }

                if(!"Y".equals(pinshiPlayer.getBetflag())){
                    pinshiPlayer.setBettimes(-1);
                }
                players[inx++] = pinshiPlayer;
            }
        }

    }
}
