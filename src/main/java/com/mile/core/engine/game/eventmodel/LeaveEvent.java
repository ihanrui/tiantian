package com.mile.core.engine.game.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

public class LeaveEvent extends BaseEvent {
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;


	private  String userid;

	private  boolean online;//在线：true 离线：false


	private String gamestatus;//玩家游戏中的状态


	private String changeroom;//换桌标志

	public LeaveEvent(String userid, String changeroom) {
		this.userid = userid;
		this.changeroom = changeroom;
	}

	public LeaveEvent(String userid) {
		this.setUserid(userid);
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public LeaveEvent(String userid, boolean online) {
		this.userid = userid;
		this.online = online;
	}

	public String getGamestatus() {
		return gamestatus;
	}

	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public String getChangeroom() {
		return changeroom;
	}

	public void setChangeroom(String changeroom) {
		this.changeroom = changeroom;
	}
}
