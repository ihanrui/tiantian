package com.mile.core.engine.game.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

import java.util.ArrayList;
import java.util.List;

public class CardEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;


	protected List<Byte> history = new ArrayList<Byte>();

	public CardEvent(List<Byte> history) {
		this.history = history;
	}

	public List<Byte> getHistory() {
		return history;
	}

	public void setHistory(List<Byte> history) {
		this.history = history;
	}
}
