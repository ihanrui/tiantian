package com.mile.core.engine.game.eventmodel;

import com.mile.common.entity.BaseEntity;
import com.mile.core.game.CardType;
import com.mile.util.UKTools;

/**
 * Created by michael on 3/28/18.
 * 小结结算
 */
public class StepEvent extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String id = UKTools.getUUID();

    private String userid;

    private int totalscore;

    private int stepscore;

    private byte[] cards;

    private CardType cardType;

    public StepEvent(String userid, int totalscore, int stepscore,byte[] cards) {
        this.setUserid(userid);
        this.setTotalscore(totalscore);
        this.setStepscore(stepscore);
        this.setCards(cards);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(int totalscore) {
        this.totalscore = totalscore;
    }

    public int getStepscore() {
        return stepscore;
    }

    public void setStepscore(int stepscore) {
        this.stepscore = stepscore;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }
}

