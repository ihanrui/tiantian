package com.mile.core.engine.game.eventmodel;

import com.mile.common.eventmodel.BaseEvent;

public class MsgEvent extends BaseEvent{
	/**
	 *
	 */
	private static final long serialVersionUID = -1L;

	private String token;

	private String source;//源用户ID

	private String target;//目标用户ID

	private String msgtype;//消息类型 0，快捷语，1.语音，2.表情，4.文字消息

	private String sendtype;//发送类型 0.单个发送，1.群发

	private String msgid;//消息ID

	private String msgname;//消息名称

	private String msgcontent;//消息内容

	private String memo;//消息说明

	private String nextplayer;//正在出牌的玩家





	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getSendtype() {
		return sendtype;
	}

	public void setSendtype(String sendtype) {
		this.sendtype = sendtype;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getMsgname() {
		return msgname;
	}

	public void setMsgname(String msgname) {
		this.msgname = msgname;
	}

	public String getMsgcontent() {
		return msgcontent;
	}

	public void setMsgcontent(String msgcontent) {
		this.msgcontent = msgcontent;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNextplayer() {
		return nextplayer;
	}

	public void setNextplayer(String nextplayer) {
		this.nextplayer = nextplayer;
	}
}
