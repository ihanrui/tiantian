package com.mile.core;

public class DataConstants {
	public static final int CARD_3  = 0;
	public static final int CARD_4  = 1;
	public static final int CARD_5  = 2;
	public static final int CARD_6  = 3;
	public static final int CARD_7  = 4;
	public static final int CARD_8  = 5;
	public static final int CARD_9  = 6;
	public static final int CARD_10 = 7;
	public static final int CARD_J  = 8;
	public static final int CARD_Q  = 9;
	public static final int CARD_K  = 10;
	public static final int CARD_1  = 11;
	public static final int CARD_2  = 12;
	public static final int CARD_sg = 13;
	public static final int CARD_mg = 14;



	public static final String GAME_EVENT_TAKECARDS = "takecards" ;//出牌事件
	public static final String GAME_EVENT_STEPCLEAN = "stepclean" ;//小局结算事件
	public static final String GAME_EVENT_PLAY= "play" ;//发牌事件
	public static final String GAME_EVENT_CLEAN= "clean" ;//一局总结算事件
	public static final String GAME_EVENT_SEECARDS= "seecards" ;//明牌接口
	public static final String GAME_EVENT_SENDMSG= "sendmsg" ;//小局结算事件
	public static final String GAME_EVENT_SETTLECARDS= "settlecards" ;//理牌事件
	public static final String GAME_EVENT_REPORTNEWS= "reportnews" ;//报喜事件
	public static final String GAME_EVENT_CARDTIPS="cardtips";//提示事件
	public static final String GAME_EVENT_JOINROOM="joinroom";//加入事件
	public static final String GAME_EVENT_GAMESTATUS="gamestatus";//游戏状态事件
	public static final String GAME_EVENT_SEARCHROOM="searchroom";//查找房间事件
	public static final String GAME_EVENT_NOCARDS="nocards";//不出，过事件
	public static final String GAME_EVENT_LEAVE="leave";//未开始游戏前的离开
	public static final String GAME_EVENT_KICKPLAYER="kickplayer";//踢人
	public static final String GAME_EVENT_REMEMBERCARD="remembercard";//记牌



	public static final String GAME_EVENT_CREATEROOM="createroom";//创建房间

	public static final String GAME_EVENT_PLAYERS = "players" ;//接受玩家列表
	public static final String GAME_EVENT_START = "start" ;//准备
	public static final String GAME_EVENT_INNERTEST = "innertest" ;//准备
	public static final String GAME_EVENT_GVIEUPTEST = "giveuptest" ;//准备

	public static final String COMPANY_DATE = "20171213" ;//准备

	public static final String GAME_EVENT_SEESCORE = "seescore" ;//看分


	public static final String GAME_EVENT_RECOVERY  = "recovery";//恢复牌局


	public static final String GAME_EVENT_GPS  = "gps";//GPS信息

	public static final String GAME_EVENT_ROOMLINESTATUS = "roomlinestatus" ;//玩家在房间状态

	public static final String GAME_EVENT_DISSROOM = "dissroom" ;//解散



	public static final String GAME_EVENT_ROOMREADY = "roomready" ;//房间准备


	public static final String GAME_EVENT_PLAYERREADY = "playerready" ;//玩家准备


	public static final String GAME_EVENT_MAZIGIVEUP = "mazigiveup" ;//投降

	public static final String GLOBAL_GAME_EVENT_ERROR= "error" ;//系统错误事件

	public static final String GLOBAL_GAME_EVENT_LINESTATUS = "linestatus" ;//在线状态

	public static final String GLOBAL_GAME_EVENT_SUCCESS= "success" ;//系统成功事件

	public static final String GLOBAL_GAME_EVENT_NOTICE= "notice" ;//通知

	public static final String GAME_USER_DEFAULT_URL=""; // "http://thirdwx.qlogo.cn/mmopen/vi_32/twibiaV0K4L2eEaGvSlhgpnjYUwEZcAv220dSic5cTK9bHoMJiambJt67ThQqU0Wssd3UVM2YaiaicxyekGTM4Wm12Nw/132";



	public static final String GLOBAL_TEAHOUSE_EVENT_CREATE="createteahouse";//创建茶楼
	public static final String TEAHOUSE_EVENT_GET="getteahouse";//获取茶楼
	public static final String TEAHOUSE_EVENT_GETTABLEINFO="gettableinfo";//获取茶楼
	public static final String TEAHOUSE_EVENT_UPDATE="updateteahouse";//修改茶楼 (包括销毁)

	public static final String GLOBAL_TEAHOUSE_EVENT_APPLYJOIN="applyjoin";//申请加入茶楼



	public static final String TEAHOUSE_EVENT_MEMUPDATE="memupdate";//茶楼成员更新

	public static final String TEAHOUSE_EVENT_GETMEMINFO="getmeminfo";//获取茶楼成员信息


	public static final String TEAHOUSE_EVENT_GETMEMINFOLIST="getmeminfolist";//获取茶楼成员信息

	public static final String TEAHOUSE_EVENT_DEALSCORE="dealscore";//上下分

	public static final String TEAHOUSE_EVENT_SITDOWN="sitdown";//坐下茶楼


	public static final String TEAHOUSE_EVENT_TABLECHANGE="tablechange";//茶楼桌号变化


	public static final String GLOBAL_GAME_EVENT_UPDATEUSER="updateuser";//修改用户信息


	public static final String TEAHOUSE_EVENT_DISSROOM="teadissroom";//茶楼解散

	public static final String TEAHOUSE_EVENT_SHIFTFUND="shiftfund";//转入基金


	//public static final String TEAHOUSE_EVENT_OUTTEAHOUSE="outteahouse";//退出茶楼


	//public static final String TEAHOUSE_EVENT_DELTEAHOUSE="delTeahouse";//删除茶楼信息





	public static final String GAME_PLAYWAY_MAZI="mazi";//
	public static final String GAME_PLAYWAY_DIZHU="dizhu";//
	public static final String GAME_PLAYWAY_MAJIANG="majiang";//
	public static final String GAME_PLAYWAY_PINSHI="pinshi";//


	public static final String TEAHOUSE_CONST_TEAOWNER="teaownernum";//创建茶楼个数

	public static final String TEAHOUSE_CONST_JOINNUM="teajoinnum";//加入茶楼个数

	public static final String CONST_CLEANSLEEP="cleansleep";//结算停顿秒数

	public static final String TEAHOUSE_CONST_PAGESIZE="pagesize";//加入茶楼个数


	public static final String SYSTEM_GAME_COINSDECUTE = "com.dic.game.coinsdecute";


	public static final String TEAHOUSE_MESSAGE_EVENT = "command_teahouse" ;


	public static final String BEIMI_SYSTEM_GAME_CONST = "com.dic.game.const";

	public static final String SYS_GIVE_DIAMOND = "sys.give.diamond";



	public enum Teaflag{
		UPDATE_NOTICE,  //名称 公告
		DESTORY, //销毁
		TEASTATUS,//打烊/开业
		UPDATE_PLAYWAY,//修改玩法
		UPDATE_SETTING;//修改 同IP禁止，是否分享，是否审核  门票收取方式
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum Teastatus{
		CLOSE,    //打烊
		OPEN,     //开业
		DESTORY;//销毁
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public enum Tickettype{
		PERSENT1,    //大赢家百分比
		PERSENT2,//两个赢家百分比
		PERSENTALL,//所有赢家百分比
		FIX1,//大赢家固定值
		FIX2,//两个赢家固定值
		FIXALL;//所有赢家固定值
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public enum Tearole{
		NORMAL,    //普通成员
		MANAGER,   //管理者
		OWNER;     //茶楼主
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}



	public enum Teapassflag{
		APPLY,    //申请
		PASS,    //审核通过
		OUT,     //离开茶楼
		REFUSE,  //拒绝
		FORBID;//禁止加入
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public enum Shoptype{
		APP,    //APP
		OPEN;   //公众号
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}





	public enum Roomlinestatus{
		OFFLINE,    //离线
		ONLINE;   //在线
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}



	public enum Roomtype{
		CARD,    //房卡
		TEACOIN,   //茶楼金币
		TICKET,//门票
		COIN;//金币
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public enum Coinexpend{
		CREATER,    //房主支付
		AA;   //AA支付
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public enum Chargetype{
		GOLDCOIN,    //豆子
		AMT,//
		DIAMOND;   //钻石
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


}
