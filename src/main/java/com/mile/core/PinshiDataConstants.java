package com.mile.core;

public class PinshiDataConstants {
	/**
	 * a、底分   basescore
	 b、局数   gamenumbers
	 c、房费    tips
	 d、自动开桌  autobeginflag  0.手动，2.自动 4.满4 5.满5，6.满6 7.满7 8.满8
	 e、闲家推注  pushflag
	 f、最大抢庄   maxbanker
	 g、翻倍规则  pinshirule
	 h、特殊牌型  specialcard
	 i、中途禁入forbidjoin、禁止搓牌forbidrubpoker、下注限制betlimit，    additional
	 j 、暗抢庄家hidebanker、王牌癞子 laizi、下注加倍betdouble，  senior
	 j、六人、八人场   playernumber
	 */



	public static final String CONT_ROOM_PUSHFLAG = "pushflag" ;

	public static final String CONT_ROOM_AUTOBEGINFLAG = "autobeginflag" ;
	public static final String CONT_ROOM_BASESCORE = "basescore" ;
	public static final String CONT_ROOM_ADDITIONAL = "additional" ;
	public static final String CONT_ROOM_GAMENUMBERS = "gamenumbers" ;
	public static final String CONT_ROOM_PINSHIRULE = "pinshirule" ;
	public static final String CONT_ROOM_SENIOR = "senior" ;
	public static final String CONT_ROOM_SPECIALCARD = "specialcard";
	public static final String CONT_ROOM_PLAYERNUMBER = "playernumber";
	public static final String CONT_ROOM_TIPS = "tips";//房费





	public static final String GAME_EVENT_ROOMSIT= "roomsit" ;//坐下
	public static final String GAME_EVENT_GAMESTART= "gamestart" ;//开始游戏

	public static final String GAME_EVENT_HOG= "hog" ;//抢庄

	public static final String CONST_HOGTIMES= "hogtimes" ;//抢庄

	public static final String GAME_EVENT_BIND= "bind" ;//定庄

	public static final String GAME_EVENT_BET= "bet" ;//下注


	public static final String GAME_EVENT_SHOW= "show" ;//下注

	public static final String GAME_EVENT_DEALING= "dealing" ;//发牌 - 发最后一张

	public static final String COM_PINSHI_TIMES = "com.pinshi.times";//时间间隔字段


	public static final String COM_PINSHI_RULETO = "com.pinshi.ruleto";//通用赔率


	public static final String RULETO = "ruleto";//通用赔率









	public enum Gamestep{
		SHOW,    //亮牌时间
		BEGIN,//准备时间
		BET,//下注时间
		HOG;//抢庄
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}



	public enum PinshiCardsTypeEnum{
		NONE(0), //无点
		S1(1),	 //score 1 1点
		S2(2),	 //score 2 2点
		S3(3),	 //score 3 3点
		S4(4),	 //score 4 4点
		S5(5),	 //score 5 5点
		S6(6),   //score 6 6点
		S7(7),	 //score 7 7点
		S8(8),	 //score 8 8点
		S9(9),   //score 9 点
		S10(10), //10点
		SZ(11),//顺子
		TH(12),//同花
		WH(13),//五花
		HL(14),//葫芦
		ZD(15),//炸弹
		WX(16);//五小

		private int type ;
		PinshiCardsTypeEnum(int type){
			this.type = type ;
		}
		public int getType() {
			return type;
		}
		public void setType(int type) {
			this.type = type;
		}
	}





}
