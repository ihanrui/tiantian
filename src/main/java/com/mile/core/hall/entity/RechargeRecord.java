package com.mile.core.hall.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * 充值记录表
 *
 */
@Entity
@Table(name = "wd_h_recharge_record")
@org.hibernate.annotations.Proxy(lazy = false)
public class RechargeRecord extends BaseEntity{


	private String id = UKTools.getUUID();;
	private int  orderindex=0;//序号
	private String  orderid;//订单号
	private String  shopid;//商店充值新
	private Date createdate  = new Date();//创建日期
	private String userid; //用户的游戏id
	private String gameid; //用户的游戏id
	private String username;//用户名称  同微信名称
	private String userurl;//用户头像地址
	private double amount; // 金额
	private double actualamount;// 实际付款金额
	private String rechargesource;
	private int rechargetype;//充值类型
	private int currentnumbers;//当前数量
	private int rechargenumbers;//购买数量
	private int rechargednumbers;// 充卡后数量
	private int status;		//状态
	private String memo;//备注




	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getShopid() {
		return shopid;
	}

	public void setShopid(String shopid) {
		this.shopid = shopid;
	}

	public int getOrderindex() {
		return orderindex;
	}

	public void setOrderindex(int orderindex) {
		this.orderindex = orderindex;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserurl() {
		return userurl;
	}

	public void setUserurl(String userurl) {
		this.userurl = userurl;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getActualamount() {
		return actualamount;
	}

	public void setActualamount(double actualamount) {
		this.actualamount = actualamount;
	}

	public int getRechargetype() {
		return rechargetype;
	}

	public void setRechargetype(int rechargetype) {
		this.rechargetype = rechargetype;
	}

	public int getCurrentnumbers() {
		return currentnumbers;
	}

	public void setCurrentnumbers(int currentnumbers) {
		this.currentnumbers = currentnumbers;
	}

	public int getRechargenumbers() {
		return rechargenumbers;
	}

	public void setRechargenumbers(int rechargenumbers) {
		this.rechargenumbers = rechargenumbers;
	}

	public int getRechargednumbers() {
		return rechargednumbers;
	}

	public void setRechargednumbers(int rechargednumbers) {
		this.rechargednumbers = rechargednumbers;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}



	public String getRechargesource() {
		return rechargesource;
	}

	public void setRechargesource(String rechargesource) {
		this.rechargesource = rechargesource;
	}
}
