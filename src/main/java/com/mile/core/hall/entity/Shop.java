package com.mile.core.hall.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "wd_h_shop")
@org.hibernate.annotations.Proxy(lazy = false)
public class Shop extends BaseEntity{


	private String id = UKTools.getUUID();;
	private int order;//序号
	private String shopname;//充值名称
	private String cointype;//类型  房卡/金币/钻石   1 为钻石，2 网豆
	private int num;//数量
	private int amt;//金额
	private int givenum;//赠送数量

	private String shoptype;//DataConstants.Shoptype

	private String agentflag;//是否可以成为代理  Y/N

	private String memo;//备注


	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getCointype() {
		return cointype;
	}

	public void setCointype(String cointype) {
		this.cointype = cointype;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getAmt() {
		return amt;
	}

	public void setAmt(int amt) {
		this.amt = amt;
	}

	public int getGivenum() {
		return givenum;
	}

	public void setGivenum(int givenum) {
		this.givenum = givenum;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getShoptype() {
		return shoptype;
	}

	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}

	public String getAgentflag() {
		return agentflag;
	}

	public void setAgentflag(String agentflag) {
		this.agentflag = agentflag;
	}
}
