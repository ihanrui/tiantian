package com.mile.core.hall.entity;

import com.mile.common.entity.BaseEntity;
import com.mile.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "wd_h_userfeedback")
@org.hibernate.annotations.Proxy(lazy = false)
public class Userfeedback extends BaseEntity{


	private String id = UKTools.getUUID();;
	private int  orderindex=0;//序号
	private String gameid;//游戏ID
	private Date createdate  = new Date();;//创建日期
	private String title;//标题
	private String question;//问题描述
	private String responseid;//回复人
	private Date responsedate  = new Date();;//回复时间
	private String responsecontent;//回复内容
	private String memo;//备注
	private String wx;//微信
	private String token;


	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getGameid() {
		return gameid;
	}

	public void setGameid(String gameid) {
		this.gameid = gameid;
	}

	public int getOrderindex() {
		return orderindex;
	}

	public void setOrderindex(int orderindex) {
		this.orderindex = orderindex;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getResponseid() {
		return responseid;
	}

	public void setResponseid(String responseid) {
		this.responseid = responseid;
	}

	public Date getResponsedate() {
		return responsedate;
	}

	public void setResponsedate(Date responsedate) {
		this.responsedate = responsedate;
	}

	public String getResponsecontent() {
		return responsecontent;
	}

	public void setResponsecontent(String responsecontent) {
		this.responsecontent = responsecontent;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getWx() {
		return wx;
	}

	public void setWx(String wx) {
		this.wx = wx;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
