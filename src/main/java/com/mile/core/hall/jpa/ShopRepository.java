package com.mile.core.hall.jpa;

import com.mile.core.hall.entity.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface ShopRepository extends JpaRepository<Shop, String> {
    public abstract Shop findById(String id);
    public abstract List<Shop> findByShoptype(String shoptype);
}
