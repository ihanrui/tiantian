package com.mile.core.hall.jpa;

import com.mile.core.hall.entity.Userfeedback;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface UserfeedbackRepository extends JpaRepository<Userfeedback, String>
{
  public abstract Userfeedback findById(String id);
}
