package com.mile.core.hall.jpa;

import com.mile.core.hall.entity.RechargeRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface RechargeRecordRepository extends JpaRepository<RechargeRecord, String> {
    public abstract RechargeRecord findById(String id);
}
