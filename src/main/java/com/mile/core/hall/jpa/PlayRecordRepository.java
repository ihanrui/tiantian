package com.mile.core.hall.jpa;

import com.mile.core.entity.PlayRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface PlayRecordRepository extends JpaRepository<PlayRecord, String> {
    public abstract PlayRecord findById(String id);
}
