package com.mile.core.hall.jpa;

import com.mile.core.hall.entity.AgentApply;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface AgentApplyRepository extends JpaRepository<AgentApply, String>
{
  public abstract AgentApply findById(String id);
}
