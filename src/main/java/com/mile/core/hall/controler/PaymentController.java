package com.mile.core.hall.controler;

import com.mile.common.constants.ContextConstants;
import com.mile.common.constants.SystemDicKeyConstants;
import com.mile.core.agent.util.Gameagentutil;
import com.mile.core.hall.entity.Shop;
import com.mile.core.hall.jpa.ShopRepository;
import com.mile.util.DicUtil;
import com.mile.util.MessageEnum;
import com.mile.util.UKTools;
import com.mile.util.payment.ParamModel;
import com.mile.util.payment.wechat.PayReqParam;
import com.mile.util.payment.wechat.WeixinUtil;
import com.mile.web.constants.SystemConstants;
import com.mile.web.handler.Handler;
import com.mile.core.entity.PlayUserClient;
import com.mile.web.model.ResultData;
import com.mile.core.entity.Token;
import com.mile.core.hall.entity.RechargeRecord;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.core.hall.jpa.RechargeRecordRepository;
import com.mile.web.utils.StringTool;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.*;

@RestController
@RequestMapping(value = {"/payment"})
public class PaymentController extends Handler {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PlayUserClientRepository playUserClientRes;

    @Autowired
    private PlayUserRepository playUserRes;

    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private RechargeRecordRepository rechargeRecordRepository;

    @Autowired
    private TokenRepository tokenRes;


    /**
     * 创建交易
     *
     * @param request
     * @param response
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/createtrade"}, method = RequestMethod.POST)
    public ResponseEntity gameCertification(HttpServletRequest request, HttpServletResponse response, String token) {

        Token userToken = null;
        PlayUserClient playUserClient = null;
        ResultData playerResultData = null;
        //String token = request.getParameter("token");
        logger.info("token-----  :" + token);
        try {

            if (!StringUtils.isBlank(token)) {
                userToken = tokenRes.findById(token);
                logger.info(
                        userToken.getUserid() + userToken.getExptime()
                );
                if (userToken != null && !StringUtils.isBlank(userToken.getUserid())) {

                    playUserClient = playUserClientRes.findById(userToken.getUserid());
                    if (playUserClient != null) {
                        logger.info("playUserClient:" + playUserClient.getId());
                        playerResultData = this.startTrade(request, playUserClient, userToken.getUserid());
                    } else {
                        logger.info("playUserClient  没找到 ！！！！");
                        playerResultData = new ResultData(false, MessageEnum.USER_TOKEN_INVALID, new PayReqParam());
                    }
                } else {
                    playerResultData = new ResultData(false, MessageEnum.USER_NOT_FOUND, new PayReqParam());
                }
            } else {
                playerResultData = new ResultData(false, MessageEnum.USER_TOKEN_INVALID, new PayReqParam());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            playerResultData = new ResultData(false, MessageEnum.INTERNAL_SERVER_ERROR, new PayReqParam());
        }
        //response.setCharacterEncoding("utf-8");//
        return new ResponseEntity<>(playerResultData, HttpStatus.OK);
    }


    private ResultData startTrade(HttpServletRequest request,
                                  PlayUserClient playUserClient,
                                  String userId) throws UnsupportedEncodingException {

        String resultDataDesc = "";
        boolean resultStatus = false;
        String shopId = request.getParameter("shopId");
        if (StringUtils.isNotBlank(shopId)) {

            Shop shop = this.shopRepository.findById(shopId);

            if (shop != null) {
                String rechargeType = request.getParameter("rechargeType");
                String rechargeSource = request.getParameter("rechargeSource");
                RechargeRecord r = getClientRechargeRecord(shop, playUserClient, userId, rechargeSource);
                if (SystemConstants.RECHARGE_TYPE_WEIXIN.equals(rechargeType)) {
                    r = rechargeRecordRepository.save(r);
                    //String shopDesc = String.format("%s-%s", appName.trim(), shop.getShopname());
                    //String shopDesc = DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.PAYMENT_APPNAME);
                    String ip = UKTools.getIpAddr(request);

                    ParamModel paramModel = getWeixinRechargeParamModel(ip, shop.getShopname(), r.getId(), (int) r.getActualamount());


                    String args = WeixinUtil.getProductArgs(paramModel);

                    logger.info("-------\n" + args + "\n-------");

                    PayReqParam payReqParam = new PayReqParam();
                    payReqParam.setAppId(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APPID));
                    payReqParam.setMchId(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_MCHID));
                    //payReqParam.setApiKey(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APIKEY));

                    Map params = getWeixinPrepareId(args);

                    if (params.containsKey("prepay_id")) {

                        payReqParam.setPrepareId((String) params.get("prepay_id"));
                        payReqParam.setNonceStr(WeixinUtil.genNonceStr().toUpperCase());
                        payReqParam.setTimeStamp(String.valueOf(Calendar.getInstance().getTimeInMillis() / 1000));

                        List<NameValuePair> pairs = getChargeParis(payReqParam);

                        String sign = WeixinUtil.genPackageSign(pairs,DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APIKEY));
                        payReqParam.setPaySign(sign);


                        return new ResultData(true, resultDataDesc, payReqParam);
                    } else {
                        return new ResultData(false, MessageEnum.RECHARGE_WEIXIN_ERROR, payReqParam);
                    }

                } else if (SystemConstants.RECHARGE_TYPE_ZHIFUBAO.equals(rechargeType)) {
                    r = rechargeRecordRepository.save(r);
                    resultDataDesc = MessageEnum.RECHARGE_TYPE_ERROR;
                } else {
                    resultDataDesc = MessageEnum.RECHARGE_TYPE_ERROR;
                }
            } else {
                resultDataDesc = MessageEnum.SHOP_NOT_FOUND;
            }
        } else {
            resultDataDesc = MessageEnum.SHOP_NOT_FOUND;
        }
        return new ResultData(resultStatus, resultDataDesc, new PayReqParam());
    }

    private List<NameValuePair> getChargeParis(PayReqParam payReqParam ){
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("appid", payReqParam.getAppId()));
        pairs.add(new BasicNameValuePair("noncestr", payReqParam.getNonceStr()));
        pairs.add(new BasicNameValuePair("package", "Sign=WXPay"));//"prepay_id="+payReqParam.getPrepareId()));
        pairs.add(new BasicNameValuePair("partnerid", payReqParam.getMchId()));
        pairs.add(new BasicNameValuePair("prepayid", payReqParam.getPrepareId()));
        pairs.add(new BasicNameValuePair("timestamp", payReqParam.getTimeStamp()));

        return pairs;
    }

    private Map<String, String> getWeixinPrepareId(String xml) {
        //https://api.mch.weixin.qq.com/pay/unifiedorder
        String urldd = String.format("https://api.mch.weixin.qq.com/pay/unifiedorder");
        //String urldd = String.format(QCConstant.PAYMENT_NOTIFY_WEXIN);

        byte[] buf = WeixinUtil.httpPost(urldd, xml);
        try {
            String content = new String(buf);
            logger.info("--获取初始化单据getWeixinPrepareId:--", content);
            //Log.e("doInBackgrou--orion22--", content2);
            Map<String, String> backInfoParam = WeixinUtil.decodeXml(content);
            //Map<String, String> xml = new HashMap<>();
            return backInfoParam;
        } catch (Exception e) {
            logger.error("--PaymentFragment--", e.getMessage());
            return new HashMap<String, String>();
        }
    }

    /**
     * 获取微信充值参数
     *
     * @param shopDesc
     * @param tradeId
     * @param totalFee
     * @return
     */
    private ParamModel getWeixinRechargeParamModel(String ip, String shopDesc, String tradeId, int totalFee) {
        ParamModel m = new ParamModel();
        String nonceStr = WeixinUtil.genNonceStr().toUpperCase();
        m.setAppId(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APPID));
        m.setAppSecret(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_SECRET));
        m.setApiKey(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APIKEY));
        m.setMchId(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_MCHID));
        m.setNotifyUrl(DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.PAYMENT_NOTIFY_URL));
        m.setGoodDesc(shopDesc);
        m.setOutTreadId(tradeId);
        m.setTotalFee(String.valueOf(totalFee));
        m.setSpbillCreateIp(ip);
        m.setNonceStr(nonceStr);
        m.setTradeType(ContextConstants.WECHAT_TRADE_TYPE_APP);
        return m;
    }

    /**
     * 获取用户充值的对象
     *
     * @param shop
     * @param playUserClient
     * @param userId
     * @param rechargeSource
     * @return
     */
    private RechargeRecord getClientRechargeRecord(Shop shop, PlayUserClient playUserClient,
                                                   String userId, String rechargeSource) {
        RechargeRecord r = new RechargeRecord();
        r.setActualamount(shop.getAmt());
        r.setAmount(shop.getAmt());
        r.setUserid(userId);
        r.setShopid(shop.getId());
        r.setGameid(StringTool.getDefaultNotNullValue(playUserClient.getGameid(), "0"));
        //r.setRechargetype(shop.getCointype());
        r.setRechargesource(StringTool.getDefaultNotNullValue(rechargeSource, "empty"));
        r.setRechargenumbers(shop.getNum());
        r.setUsername(StringTool.getDefaultNotNullValue(playUserClient.getNickname(), playUserClient.getUsername()));
        r.setUserurl(StringTool.getDefaultNotNullValue(playUserClient.getUserurl(), ""));
        r.setStatus(SystemConstants.RECHARGE_STATUS_INIT);
        r.setMemo("");
        r.setOrderid("");

        return r;
    }

    /**
     * 手机号码保存
     *
     * @param request
     * @param response
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/notifyback"}, method = RequestMethod.POST)
    public ResponseEntity gameMobile(HttpServletRequest request,
                                     HttpServletResponse response) {

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            StringBuilder stb = new StringBuilder();
            logger.info("-------回调验证------");
            String line = null;
            while ((line = in.readLine()) != null) {
                stb.append(line);
            }
            logger.info("-------回调信息------");
            logger.info("-------\n" + stb.toString() + "\n-------");

            Map<String, String> params = WeixinUtil.decodeXml(stb.toString());
            return this.validateNotifyCallback(params, stb.toString());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.error("关闭回调read 流出错", e);
                }
            }
        }
        String errorXml = String.format(SystemConstants.RECHCAREGE_BACK_XML, SystemConstants.RECHARGE_VALIDATE_FAILURE_CODE, SystemConstants.RECHARGE_VALIDATE_FAILURE);
        return new ResponseEntity<>(errorXml, HttpStatus.OK);
    }

    private ResponseEntity validateNotifyCallback(Map<String, String> params, String content) {
        String out_trade_no = params.get("out_trade_no");
        String validateDesc = SystemConstants.RECHARGE_VALIDATE_SUCCESS;
        String validateCode = SystemConstants.RECHARGE_VALIDATE_SUCCESS_CODE;
        if (validateSign(params)) {
            logger.info("签名验证通过-----");
            if (StringUtils.isNotBlank(out_trade_no)) {
                logger.info("订单验证通过-----");
                RechargeRecord rechargeRecord = rechargeRecordRepository.findById(out_trade_no);
                if (rechargeRecord != null) {

                    if (SystemConstants.RECHARGE_STATUS_INIT == rechargeRecord.getStatus()) {

                        rechargeRecord.setOrderid(params.get("transaction_id"));
                        rechargeRecord.setMemo(content);
                        rechargeRecord.setStatus(SystemConstants.RECHARGE_STATUS_SUCCESS);
                        rechargeRecordRepository.save(rechargeRecord);
                        Shop shop = this.shopRepository.findById(rechargeRecord.getShopid());

                        Gameagentutil.charge(shop, rechargeRecord, rechargeRecord.getGameid());//返利处理

                    } else {

                        validateDesc = SystemConstants.RECHARGE_VALIDATE_TRADE_NO_EXPIRE;

                        validateCode = SystemConstants.RECHARGE_VALIDATE_FAILURE_CODE;
                    }

                } else {
                    validateDesc = SystemConstants.RECHARGE_VALIDATE_TRADE_NO_ERROR;

                    validateCode = SystemConstants.RECHARGE_VALIDATE_FAILURE_CODE;
                }

            } else {
                validateDesc = SystemConstants.RECHARGE_VALIDATE_TRADE_NO_ERROR;
                validateCode = SystemConstants.RECHARGE_VALIDATE_FAILURE_CODE;
            }
        } else {
            validateDesc = SystemConstants.RECHARGE_VALIDATE_TRADE_SIGN_ERROR;
            validateCode = SystemConstants.RECHARGE_VALIDATE_FAILURE_CODE;
        }

        return new ResponseEntity(String.format(SystemConstants.RECHCAREGE_BACK_XML, validateCode, validateDesc), HttpStatus.OK);
    }

    private boolean validateSign(Map<String, String> params) {
        String sign = params.get("sign");
        String checkSign = WeixinUtil.genPackageSign(WeixinUtil.getPackageParams(params),
                DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_APIKEY));
        if (sign == null) {
            return false;
        }
        return sign.equals(checkSign);
    }


}