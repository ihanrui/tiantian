package com.mile.core.hall.controler;

import com.mile.core.hall.entity.Shop;
import com.mile.core.hall.jpa.ShopRepository;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import com.mile.core.game.jpa.GamePlaywayRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/apps/maziplatform")
public class ShopController extends Handler{


	@Autowired
	private ShopRepository shopRepository ;


	@Autowired
	private PlayUserRepository playUserRes;

	@Autowired
	private GamePlaywayRepository playwayRes;

	@RequestMapping({"/shop"})
	@Menu(type="platform", subtype="shop")
	public ModelAndView gameusers(ModelMap map , HttpServletRequest request , @Valid String id){
		Page<Shop> shoplist = shopRepository.findAll(new PageRequest(super.getP(request), super.getPs(request) )) ;

		map.addAttribute("shoplist", shoplist) ;


		return request(super.createAppsTempletResponse("/apps/business/maziplatform/shop/shoplist"));
	}



}
