package com.mile.core.hall.controler;

import com.mile.common.cache.CacheHelper;
import com.mile.common.cache.hazelcast.impl.QueneCache;
import com.mile.core.BMDataContext;
import com.mile.core.game.BeiMiGame;
import com.mile.core.game.mazi.util.MaziSummaryUtil;
import com.mile.core.hall.jpa.ShopRepository;
import com.mile.util.GameUtils;
import com.mile.web.handler.Handler;
import com.mile.web.model.BeiMiDic;
import com.mile.core.entity.PlayUser;
import com.mile.web.model.SysDic;
import com.mile.core.entity.Token;
import com.mile.core.hall.entity.AgentApply;
import com.mile.core.hall.entity.Userfeedback;
import com.mile.core.hall.jpa.AgentApplyRepository;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.core.hall.jpa.UserfeedbackRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value={"/hall"})
public class HallController extends Handler{
    private static final Logger logger = LoggerFactory.getLogger(HallController.class);



    @Autowired
    private PlayUserClientRepository playUserClientRes ;

    @Autowired
    private PlayUserRepository playUserRes ;

    @Autowired
    private AgentApplyRepository agentApplyRepository ;

    @Autowired
    private TokenRepository tokenRes ;

    @Autowired
    private UserfeedbackRepository userchatRes ;

    @Autowired
    private ShopRepository shopRepository ;

    /**
     * 实名认证
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value={"/gameCertification"},method = RequestMethod.POST)
    public ResponseEntity gameCertification(HttpServletRequest request , HttpServletResponse response,
                                            String token,String uname,String idcard,String mobile) {
        PlayUser playUser  = null ;
        Token userToken = null ;

        if(!StringUtils.isBlank(token)){
            userToken = tokenRes.findById(token) ;
            if(userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime()!=null && userToken.getExptime().after(new Date())){
                //返回token， 并返回游客数据给游客
                playUser  = playUserRes.findById(userToken.getUserid()) ;
                playUser.setUname(uname);
                playUser.setIdcard(idcard);
                playUser.setMobile(mobile);
                playUserRes.save(playUser);
                return new ResponseEntity<>(true, HttpStatus.OK) ;
            }else{
                if(userToken!=null){
                    tokenRes.delete(userToken);
                    userToken = null ;
                }
                return new ResponseEntity<>("token 错误", HttpStatus.OK) ;
            }
        }

        return new ResponseEntity<>("token为空", HttpStatus.OK) ;
    }

    /**
     * 手机号码保存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/gameMobile"},method = RequestMethod.POST)
    public ResponseEntity gameMobile(HttpServletRequest request , HttpServletResponse response, String token,String mobile) {
        PlayUser playUser  = null ;
        Token userToken = null ;

        if(!StringUtils.isBlank(token)){
            userToken = tokenRes.findById(token) ;
            if(userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime()!=null && userToken.getExptime().after(new Date())){
                //返回token， 并返回游客数据给游客
                playUser  = playUserRes.findById(userToken.getUserid()) ;
                playUser.setMobile(mobile);
                playUserRes.save(playUser);
                return new ResponseEntity<>(true, HttpStatus.OK) ;
            }else{
                if(userToken!=null){
                    tokenRes.delete(userToken);
                    userToken = null ;
                }
            }
        }

        return new ResponseEntity<>(false, HttpStatus.OK) ;
    }




    /**
     * 代理申请
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/gameAgent"},method = RequestMethod.POST)
    public ResponseEntity gameAgent(HttpServletRequest request , HttpServletResponse response, String token,
                                    String mobile,String wx,String qq) {

        try {
            AgentApply aa = new AgentApply();
            aa.setToken(token);
            aa.setMobile(mobile);
            aa.setQq(qq);
            aa.setWx(wx);
            agentApplyRepository.save(aa);

            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("保存代理信息失败",e);
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }



    /**
     * 代理申请
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/getMarquee"},method = RequestMethod.POST)
    public ResponseEntity getMarquee(HttpServletRequest request , HttpServletResponse response) {

        try {
            return new ResponseEntity<>(BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_COM_DIC_HALL_MARQUEE).get(0).getCode(), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("获取跑马灯信息失败",e);
            return new ResponseEntity<>("获取跑马灯信息失败", HttpStatus.OK);
        }
    }







    /**
     * 建议保存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/gameSuggest"},method = RequestMethod.POST)
    public ResponseEntity gameSuggest(HttpServletRequest request , HttpServletResponse response,
                                          String token,String wx,String question) {
        try{
            Userfeedback userchat = new Userfeedback();
            userchat.setToken(token);
            userchat.setQuestion(question);
            userchat.setWx(wx);
            userchatRes.save(userchat);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("建议信息保存失败",e);
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }




    /**
     * 建议保存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/getJsonstr"},method = RequestMethod.POST)
    public ResponseEntity getJsonstr(HttpServletRequest request , HttpServletResponse response,
                                      String code) {
        try{
            logger.info("获取公告或者分享链接的接口 code ["+code+"]");
            List<SysDic> sysDicList = BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_JSONSTR);
            return new ResponseEntity<>(MaziSummaryUtil.getObjWithName(sysDicList,code), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("获取公告或者分享链接的接口 失败",e);
            return new ResponseEntity<>("err", HttpStatus.OK);
        }
    }


    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/getallshop"},method = RequestMethod.POST)
    public ResponseEntity getallshop(HttpServletRequest request , HttpServletResponse response) {
        try{
            return new ResponseEntity<>(shopRepository.findAll(), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("获取所有充值信息异常",e);
            return new ResponseEntity<>("获取所有充值信息异常", HttpStatus.OK);
        }
    }










    /**
     * 获取游戏玩法
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/getgames"},method = RequestMethod.POST)
    public ResponseEntity getgames(HttpServletRequest request , HttpServletResponse response,
                                     String code) {
        try{
            logger.info("获取游戏玩法接口 code ["+code+"]");
            List<BeiMiGame> beiMiGames = GameUtils.gameswithcode(code);
            return new ResponseEntity(beiMiGames, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("获取游戏玩法接口 失败",e);
            return new ResponseEntity<>("err", HttpStatus.OK);
        }
    }


    /**
     * 获取积分场的人数
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/getusernum"},method = RequestMethod.POST)
    public ResponseEntity getusernum(HttpServletRequest request , HttpServletResponse response,
                                   String playway) {
        try{
            logger.info("获取积分场的人数 playway ["+playway+"]");
            QueneCache queneCache = (QueneCache)CacheHelper.getQueneCache();
            int nums = queneCache.getUsernums(playway,BMDataContext.SYSTEM_ORGI);
            return new ResponseEntity(nums, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("获取积分场的人数 失败",e);
            return new ResponseEntity<>("err", HttpStatus.OK);
        }
    }








}