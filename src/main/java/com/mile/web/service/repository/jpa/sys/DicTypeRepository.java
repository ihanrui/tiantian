package com.mile.web.service.repository.jpa.sys;

import com.mile.core.entity.DicType;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface DicTypeRepository extends JpaRepository<DicType, String>
{
  public abstract DicType findById(String id);
}
