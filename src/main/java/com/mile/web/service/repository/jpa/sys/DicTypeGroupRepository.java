package com.mile.web.service.repository.jpa.sys;

import com.mile.core.entity.DicTypeGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface DicTypeGroupRepository extends JpaRepository<DicTypeGroup, String>
{
  public abstract DicTypeGroup findById(String id);
}
