package com.mile.web.constants;

public class SystemConstants {

    //充值类型
    public static final String RECHARGE_TYPE_WEIXIN = "weixin_pay";
    public static final String RECHARGE_TYPE_ZHIFUBAO = "zhifubao_pay";

    //充值状态
    public static final int RECHARGE_STATUS_INIT = 1;
    public static final int RECHARGE_STATUS_SUCCESS = 5;
    public static final int RECHARGE_STATUS_FAILURE = 9;

    // 微信充值验证

    public static final String RECHARGE_VALIDATE_FAILURE = "FAIL";
    public static final String RECHARGE_VALIDATE_SUCCESS = "OK";
    public static final String RECHARGE_VALIDATE_FAILURE_CODE = "FAIL";
    public static final String RECHARGE_VALIDATE_SUCCESS_CODE = "SUCCESS";
    public static final String RECHARGE_VALIDATE_TRADE_NO_ERROR = "交易码有误";
    public static final String RECHARGE_VALIDATE_TRADE_NO_EXPIRE = "交易已过期";
    public static final String RECHARGE_VALIDATE_TRADE_SIGN_ERROR = "签名错误";

    public static final String RECHCAREGE_BACK_XML = "<xml> <return_code><![CDATA[%s]]></return_code>\n" +
            "  <return_msg><![CDATA[%s]]></return_msg>\n" +
            "</xml>";


}
