package com.mile.web.handler.apps.business.platform;

import com.mile.core.entity.PlayUser;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/apps/platform/online")
public class OnlineGameUsersController extends Handler{


	@Autowired
	private PlayUserRepository playUserRes ;

	@RequestMapping({"/gameusers"})
	@Menu(type="platform", subtype="onlinegameusers")
	public ModelAndView online(ModelMap map , HttpServletRequest request , @Valid String id){
		map.addAttribute("playersList", playUserRes.findByOrgiAndOnline(super.getOrgi(request), true,new PageRequest(super.getP(request), super.getPs(request)))) ;
		return request(super.createAppsTempletResponse("/apps/business/platform/game/online/index"));
	}

	@RequestMapping({"/gameusers/edit"})
	@Menu(type="platform", subtype="onlinegameusers")
	public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id){

		map.addAttribute("playUser", playUserRes.findById(id)) ;

		return request(super.createRequestPageTempletResponse("/apps/business/platform/game/online/edit"));
	}

	@RequestMapping("/gameusers/update")
    @Menu(type = "admin" , subtype = "gameusers")
    public ModelAndView update(HttpServletRequest request ,@Valid PlayUser players) {
		PlayUser playUser = playUserRes.findById(players.getId()) ;
		if(playUser!=null){
			playUser.setDisabled(players.getDisabled());
			playUser.setGoldcoins(players.getGoldcoins());
			playUser.setCards(players.getCards());
			playUser.setDiamonds(players.getDiamonds());
			playUser.setUpdatetime(new Date());
			playUserRes.save(playUser) ;


			MaziScoreUtil.synchronizationUsercache(playUser);
		}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/platform/online/gameusers.html"));
    }
}
