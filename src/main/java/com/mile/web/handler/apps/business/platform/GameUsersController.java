package com.mile.web.handler.apps.business.platform;

import com.mile.common.cache.CacheHelper;
import com.mile.core.agent.entity.Gameagent;
import com.mile.core.agent.jpa.GameagentRepository;
import com.mile.core.agent.jpa.MapinfoRepository;
import com.mile.core.agent.jpa.UseragentmapRepository;
import com.mile.core.entity.GameRoom;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.game.GameEventHandler;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import com.mile.core.game.mazi.util.MaziScoreUtil;
import com.mile.core.teahouse.jpa.TeamemRepository;
import com.mile.util.GameUtils;
import com.mile.util.Menu;
import com.mile.web.handler.Handler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/apps/platform")
public class GameUsersController extends Handler{
	private static final Logger logger = LoggerFactory.getLogger(GameUsersController.class);

	@Autowired
	private PlayUserRepository playUserRes ;

	@Autowired
	private TokenRepository tokenRepository ;

	@Autowired
	private TeamemRepository teamemRepository ;


	@Autowired
	private MapinfoRepository mapinfoRepository ;


	@Autowired
	private UseragentmapRepository useragentmapRepository ;


	@Autowired
	private GameagentRepository gameagentRepository ;






	@RequestMapping({"/gameusers"})
	@Menu(type="platform", subtype="gameusers")
	public ModelAndView gameusers(ModelMap map , HttpServletRequest request , @Valid String gameid){
		logger.info("用户信息查询 gameid["+gameid+"] ");
		if(!StringUtils.isEmpty(gameid)){
			map.addAttribute("playersList", playUserRes.findByGameidLike(gameid, new PageRequest(super.getP(request), super.getPs(request),
					new Sort(Sort.Direction.DESC, "createtime"))));
		}else {

			map.addAttribute("playersList", playUserRes.findByOrgi(super.getOrgi(request), new PageRequest(super.getP(request), super.getPs(request),
					new Sort(Sort.Direction.DESC, "createtime"))));
		}
		return request(super.createAppsTempletResponse("/apps/business/platform/game/data/index"));
	}

	@RequestMapping({"/gameusers/online"})
	@Menu(type="platform", subtype="onlinegameusers")
	public ModelAndView online(ModelMap map , HttpServletRequest request , @Valid String id){
		map.addAttribute("playersList", playUserRes.findByOrgiAndOnline(super.getOrgi(request), true,new PageRequest(super.getP(request), super.getPs(request)))) ;
		return request(super.createAppsTempletResponse("/apps/business/platform/game/data/online"));
	}

	@RequestMapping({"/gameusers/edit"})
	@Menu(type="platform", subtype="gameusers")
	public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id){

		map.addAttribute("playUser", playUserRes.findById(id)) ;

		return request(super.createRequestPageTempletResponse("/apps/business/platform/game/data/edit"));
	}

	@RequestMapping("/gameusers/update")
    @Menu(type = "admin" , subtype = "gameusers")
	public ModelAndView update(HttpServletRequest request ,@Valid PlayUser players) {
		PlayUser playUser = playUserRes.findById(players.getId()) ;
		if(playUser!=null){
			playUser.setDisabled(players.getDisabled());
			playUser.setGoldcoins(players.getGoldcoins());
			playUser.setCards(players.getCards());
			playUser.setDiamonds(players.getDiamonds());
			playUser.setUpdatetime(new Date());
			playUserRes.save(playUser) ;

			MaziScoreUtil.synchronizationUsercache(playUser);

		}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/platform/gameusers.html"));
    }

	@RequestMapping({"/gameusers/delete"})
	@Menu(type="platform", subtype="gameusers")
	@Transactional
	public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String gameid){
		if(!StringUtils.isBlank(gameid)){
			PlayUser playUser = playUserRes.findByGameid(gameid) ;
			if(playUser!=null){
				playUserRes.delete(playUser);
				tokenRepository.deleteByUserid(playUser.getId());
				teamemRepository.deleteByGameid(gameid);
				mapinfoRepository.deleteByGameid(gameid);
				useragentmapRepository.deleteByGameid(gameid);
				gameagentRepository.deleteByGameid(gameid);
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/platform/gameusers.html"));
	}











}
