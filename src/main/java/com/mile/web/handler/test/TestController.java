package com.mile.web.handler.test;

import com.mile.core.record.entity.Summary;
import com.mile.core.record.entity.SummaryPlayer;
import com.mile.web.handler.Handler;
import com.mile.core.entity.GameRoom;
import com.mile.core.game.jpa.GameRoomRepository;
import com.mile.core.record.jpa.SummaryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value={"/test"})
public class TestController extends Handler{
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);


    @Autowired
    private SummaryRepository summaryRes;
    @Autowired
    private GameRoomRepository gameRoomRes;



    /**
     * 测试存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value={"/testSave"},method = RequestMethod.POST)
    public ResponseEntity testSave(HttpServletRequest request , HttpServletResponse response) {

        Summary summary = new Summary();
        summary.getSummaryPlayer().add(new SummaryPlayer());
        summary.getSummaryPlayer().add(new SummaryPlayer());
        summary.getSummaryPlayer().add(new SummaryPlayer());
        summary.getSummaryPlayer().add(new SummaryPlayer());

        System.out.println("summary ID  ======"+summary.getId());

        GameRoom  gameRoom = new GameRoom();

        Map<String,String> extparams = new HashMap<String,String>();
        extparams.put("test","测试");
        extparams.put("gameid","房间号");
        gameRoom.setExtparams(extparams);



        summaryRes.save(summary);

        System.out.println("gameRoom ID  ======"+gameRoom.getId());

        gameRoomRes.save(gameRoom);

        return new ResponseEntity<>("保存成功", HttpStatus.OK) ;
    }





    /**
     * 测试存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/testGet"},method = RequestMethod.POST)
    public ResponseEntity testGet(HttpServletRequest request , HttpServletResponse response,
    String id) {


        System.out.println(gameRoomRes.findById(id)) ;




        return new ResponseEntity<>("获取成功"+gameRoomRes.findById(id), HttpStatus.OK) ;
    }


    /**
     * 测试存
     * @param request
     * @param response
     * @param token
     * @return
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value={"/testDel"},method = RequestMethod.POST)
    public ResponseEntity testDel(HttpServletRequest request , HttpServletResponse response,
                                  String id) {


        summaryRes.delete(id);
        gameRoomRes.delete(id);


        return new ResponseEntity<>("删除成功"+ id, HttpStatus.OK) ;
    }



}