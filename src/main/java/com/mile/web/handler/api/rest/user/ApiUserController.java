package com.mile.web.handler.api.rest.user;

import com.mile.util.MessageEnum;
import com.mile.web.handler.Handler;
import com.mile.core.entity.PlayUser;
import com.mile.web.model.ResultData;
import com.mile.core.game.jpa.PlayUserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/player")
public class ApiUserController extends Handler{


	@Autowired
	private PlayUserRepository playUserRes ;

	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ResultData> get(HttpServletRequest request , @RequestParam String id) {
    	PlayUser player = null ;
    	if(!StringUtils.isBlank(id) && playUserRes.exists(id)){
    		player = playUserRes.findOne(id) ;
    	}
    	return new ResponseEntity<>(new ResultData( player!=null , player != null ? MessageEnum.USER_GET_SUCCESS : MessageEnum.USER_NOT_EXIST, player), HttpStatus.OK);
    }

}