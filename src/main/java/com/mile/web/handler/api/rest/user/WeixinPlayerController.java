package com.mile.web.handler.api.rest.user;

import com.mile.common.constants.SystemDicKeyConstants;
import com.mile.core.BMDataContext;
import com.mile.core.agent.util.Gameagentutil;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.core.teahouse.util.TeahouseUtil;
import com.mile.util.*;
import com.mile.common.cache.CacheHelper;
import com.mile.util.payment.wechat.MD5;
import com.mile.util.tool.WechatUtil;
import com.mile.web.handler.Handler;
import com.mile.web.model.*;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@RestController
@RequestMapping("/api2")
public class WeixinPlayerController extends Handler {


    @Autowired
    private PlayUserClientRepository playUserClientRes;

    @Autowired
    private PlayUserRepository playUserRes;

    @Autowired
    private TokenRepository tokenRes;


    private static final Logger logger = LoggerFactory.getLogger(WeixinPlayerController.class);

    /**
     * 微信登录
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/weixinlogin"}, method = RequestMethod.GET)
    public ResponseEntity<ResultData> weixinlogin(HttpServletRequest request) {
        PlayUserClient playUserClient = null;
        Token userToken = null;

        String token = request.getParameter("token");
        String openid = request.getParameter("openid");
        String weixinCode = request.getParameter("code");
        logger.info("----weixin login-----openid:[" + openid + "]---是否为空-：" + (openid == null));
        logger.info("----weixin login-----token:[" + token + "]---是否为空-：" + (token == null));
        logger.info("----weixin login-----weixinCode:[" + weixinCode + "]---是否为空-：" + (weixinCode == null));
        if (!StringUtils.isBlank(token)) {
            userToken = tokenRes.findById(token);
            if (userToken != null && !StringUtils.isBlank(userToken.getUserid())
                    && userToken.getExptime() != null && userToken.getExptime().after(new Date())) {
                //返回token， 并返回游客数据给游客
                playUserClient = playUserClientRes.findById(userToken.getUserid());

                PlayUserClient tmp = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(playUserClient.getId(), playUserClient.getOrgi());

                if (playUserClient != null && tmp != null) {
                    playUserClient = tmp;
                }

                if (playUserClient != null) {
                    playUserClient.setToken(userToken.getId());
                }


                if (playUserClient != null) {
                    playUserClient.setToken(userToken.getId());
                }
            } else {
                if (userToken != null) {
                    tokenRes.delete(userToken);
                    userToken = null;
                }
            }

        }

        logger.info("---Get play user client---:" + openid);
        String ip = UKTools.getIpAddr(request);
        IP ipdata = IPTools.getInstance().findGeography(ip);
        if (playUserClient == null) {
            try {
                playUserClient = register(new PlayUser(), ipdata, request);
                playUserClient.setIp(ip);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        if (userToken == null) {
            userToken = new Token();
            userToken.setIp(ip);
            userToken.setRegion(ipdata.getProvince() + ipdata.getCity());
            userToken.setId(UKTools.getUUID());
            userToken.setUserid(playUserClient.getId());
            userToken.setCreatetime(new Date());
            userToken.setOrgi(playUserClient.getOrgi());
            AccountConfig config = CacheConfigTools.getGameAccountConfig(BMDataContext.SYSTEM_ORGI);
            if (config != null && config.getExpdays() > 0) {
                userToken.setExptime(new Date(System.currentTimeMillis() + 60 * 60 * 24 * config.getExpdays() * 1000));//默认有效期 ， 7天
            } else {
                userToken.setExptime(new Date(System.currentTimeMillis() + 60 * 60 * 24 * 7 * 1000));//默认有效期 ， 7天
            }
            userToken.setLastlogintime(new Date());
            userToken.setUpdatetime(new Date(0));

            tokenRes.save(userToken);
        }

        logger.info("---Save  token ---:" + userToken.getId());


        playUserClient.setToken(userToken.getId());
        playUserClient.setIp(ip);

        UKTools.intiGPSInfo(request, playUserClient);
        ResultData playerResultData = new ResultData(playUserClient != null, playUserClient != null ? MessageEnum.USER_REGISTER_SUCCESS : MessageEnum.USER_REGISTER_FAILD_USERNAME, playUserClient, userToken);
        GameConfig gameConfig = CacheConfigTools.getGameConfig(userToken.getOrgi());
        if (gameConfig != null) {
            playerResultData.setGametype(gameConfig.getGamemodel());
            playerResultData.setNoaiwaitime(gameConfig.getTimeout());    //无AI的时候 等待时长
            playerResultData.setNoaimsg(gameConfig.getTimeoutmsg());    //无AI的时候，到达最大时长以后的 提示消息，提示完毕后，解散房间
            /**
             * 封装 游戏对象，发送到客户端
             */
            /**
             * 找到游戏配置的 模式 和玩法，如果多选，则默认进入的是 大厅模式，如果是单选，则进入的是选场模式
             */
            playerResultData.setGames(GameUtils.games(gameConfig.getGametype()));
        }
        logger.info("---Set ai  ---:" + playUserClient.getGameid());
        AiConfig aiConfig = CacheConfigTools.getAiConfig(userToken.getOrgi());
        if (aiConfig != null) {
            playerResultData.setEnableai(aiConfig.isEnableai());
            playerResultData.setWaittime(aiConfig.getWaittime());
        }

        // gameid 不为空，则进行保存
        if (StringUtils.isNotBlank(playUserClient.getGameid())) {

            logger.info("---save client   ---:" + playUserClient.getGameid());
            playUserClientRes.saveAndFlush(playUserClient);


            logger.info("---Set to cache  ---:" + playUserClient.getId());

            CacheHelper.getApiUserCacheBean().put(userToken.getId(), userToken, userToken.getOrgi());
            CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, userToken.getOrgi());


        }
        logger.info("---Finish login  ---:" + playUserClient.getId());


        return new ResponseEntity<>(playerResultData, HttpStatus.OK);
    }

    /**
     * 注册用户
     *
     * @param player
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public PlayUserClient register(PlayUser player, IP ipdata, HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {


        String code = request.getParameter("code");

        try {


            JSONObject tokenJson = WechatUtil.getAccessToken(code,
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_LOGIN_APPID),
                    DicUtil.getSystemKeyDicNameByName(SystemDicKeyConstants.WECHAT_LOGIN_SECRET));
            if (tokenJson != null && tokenJson.has("access_token")) {
                String access_token = JSONUtil.toString(tokenJson, "access_token");
                String openid = JSONUtil.toString(tokenJson, "openid");

                JSONObject userData = WechatUtil.getWeUserInfo(access_token, openid);

                logger.info("----weixin login-----");
                logger.info(userData.toString());

                if (userData != null && userData.has("openid")) {

                    boolean hasUserData = false;
                    PlayUser sysPlayuser = playUserRes.findByOpenid(openid);
                    if (sysPlayuser != null && StringUtils.isNotBlank(sysPlayuser.getId())) {
                        logger.info("----存在 exists-----:" + sysPlayuser.getId());
                        player = sysPlayuser;
                        hasUserData = true;
                    }
                    player.setOpenid(openid);
                    player.setUserurl(JSONUtil.toString(userData, "headimgurl"));
                    player.setNickname(JSONUtil.toString(userData, "nickname"));
                    player.setUsername(JSONUtil.toString(userData, "nickname"));
                    player.setHeadimg(true);
                    player.setProvince(JSONUtil.toString(userData, "province"));
                    player.setCity(JSONUtil.toString(userData, "city"));


                    PlayUserClient playUserClient = new PlayUserClient();
                    logger.info("----save login-----");
                    if (hasUserData) {
                        BeanUtils.copyProperties(playUserClient, player);
                    } else {
                        playUserClient = GameUtils.create(player, ipdata, request);
                    }


                    playUserRes.saveAndFlush(player);
                    CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, playUserClient.getOrgi());
                    logger.info("----Gameagentutil applogin login-----");
                    Gameagentutil.applogin(WechatUtil.weixinJoinInfo(userData), playUserClient.getGameid(), openid);

                    logger.info("绑定代理关系前 玩家[" + playUserClient.getUsername() + "][" + playUserClient.getGameid() + "] 钻石数量[" + playUserClient.getDiamonds() + "]");

                    playUserClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(playUserClient.getId(), playUserClient.getOrgi());

                    logger.info("绑定代理关系后 玩家[" + playUserClient.getUsername() + "][" + playUserClient.getGameid() + "] 钻石数量[" + playUserClient.getDiamonds() + "]");

                    return playUserClient;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            logger.error("注册用户出错", e);
        }

        return new PlayUserClient();
    }


    /**
     * 微信登录
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/loadingImage"}, method = RequestMethod.GET)
    public void loginImage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String imageUrl = request.getParameter("imageUrl");
        //String username = request.getParameter("username");
        String fileName = MD5.getMessageDigest(imageUrl.toString().getBytes()).toUpperCase();
        //String.format("%s%s", username, imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.length()));
        logger.info("用户头像地址请求：" + imageUrl);
        File cachePath = new File("cacheImg");
        if (!cachePath.isDirectory()) {
            cachePath.mkdirs();
        }
        File cacheFile = new File(new File("cacheImg"), fileName);
        if (imageUrl == null || imageUrl.indexOf("http") != 0) {
            cacheFile = new File(new File("src/main/resources/static/app/img/"), "empty_head.jpg");
        }
        if (cacheFile == null || !cacheFile.exists()) {

            //得到图片的二进制数据，以二进制封装得到数据，具有通用性
            InputStream inStream = WeixinPlayerControllerHelp.getConnectionInputStream(imageUrl);
            if (inStream != null) {

                byte[] data = WeixinPlayerControllerHelp.readInputStream(inStream);
                //new一个文件对象用来保存图片，默认保存当前工程根目录
                //创建输出流
                FileOutputStream outStream = new FileOutputStream(cacheFile);
                //写入数据
                outStream.write(data);
                //关闭输出流
                outStream.close();
                cacheFile = new File(new File("cacheImg"), fileName);
            }
        }


        if (cacheFile != null && cacheFile.exists()) {
            response.setContentType("image/jpeg");
            BufferedImage image = ImageIO.read(cacheFile);
            ImageIO.write(image, "jpg", response.getOutputStream());

        }

    }

    /**
     * 微信登录
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/loadingInviteImage"}, method = RequestMethod.GET)
    public void loadingInviteImage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String imageUrl = request.getParameter("imageUrl");
        String downloadUrl = TeahouseUtil.getConstantsByName("app_share_download_url");
        String gameid = request.getParameter("gameid");
        try {
            logger.info("生成二维码--gameid:" + gameid);
            logger.info("生成二维码--imageUrl:" + imageUrl);
            logger.info("生成二维码--downloadUrl:" + downloadUrl);
            downloadUrl = downloadUrl.replace("$gameid", gameid + "");
            logger.info("生成二维码--downloadUrl:" + downloadUrl);
            BufferedImage image = QRCodeUtil.encodeUrlInsertImage(downloadUrl, imageUrl, true);
            logger.info("生成二维码--完成");
            response.setContentType("image/jpeg");
            ImageIO.write(image, "jpg", response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("生成二维码出错--loadingInviteImage");
        }

    }


    /**
     * 微信登录
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = {"/catchErrorInfo"}, method = RequestMethod.POST)
    public void catchErrorInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {


        String infoType = request.getParameter("infoType");
        String content = request.getParameter("content");
        if("info".equals(infoType)){
            if (StringUtils.isNotBlank(content)) {
                logger.info("前端信息记录：" + content);
            }
        }else {

            if (StringUtils.isNotBlank(content)) {
                logger.error("前端错误收集：" + content);
            }
        }

    }


}