package com.mile.web.handler.api.rest.user;

import com.mile.core.BMDataContext;
import com.mile.core.DataConstants;
import com.mile.core.entity.PlayUser;
import com.mile.core.entity.PlayUserClient;
import com.mile.core.entity.Token;
import com.mile.util.*;
import com.mile.common.cache.CacheHelper;
import com.mile.web.handler.Handler;
import com.mile.web.model.*;
import com.mile.core.game.jpa.PlayUserClientRepository;
import com.mile.core.game.jpa.PlayUserRepository;
import com.mile.core.game.jpa.TokenRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@RestController
@RequestMapping("/api/guest")
public class GuestPlayerController extends Handler{




	@Autowired
	private PlayUserClientRepository   playUserClientRes;

	@Autowired
	private PlayUserRepository playUserRes ;

	@Autowired
	private TokenRepository tokenRes ;


	private static final Logger logger = LoggerFactory.getLogger(GuestPlayerController.class);

	@RequestMapping
    public ResponseEntity<ResultData> guest(HttpServletRequest request , @Valid String token) {
		PlayUserClient playUserClient = null ;
		Token userToken = null ;
		if(!StringUtils.isBlank(token)){
			userToken = tokenRes.findById(token) ;
			if(userToken != null && !StringUtils.isBlank(userToken.getUserid()) && userToken.getExptime()!=null && userToken.getExptime().after(new Date())){
				//返回token， 并返回游客数据给游客
				playUserClient = playUserClientRes.findById(userToken.getUserid()) ;
				if(null != playUserClient) {
					PlayUserClient tmp = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(playUserClient.getId(), playUserClient.getOrgi());

					if (playUserClient != null && tmp != null) {
						playUserClient = tmp;
					}

					if (playUserClient != null) {
						playUserClient.setToken(userToken.getId());
					}
				}
			}else{
				if(userToken!=null){
					tokenRes.delete(userToken);
					userToken = null ;
				}
			}
		}


		String ip = UKTools.getIpAddr(request);
		IP ipdata = IPTools.getInstance().findGeography(ip);
		if(playUserClient == null){
			try {
				playUserClient = register(new PlayUser() , ipdata , request) ;
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		logger.info("------register user ---:"+playUserClient.getId());


		if(userToken == null){
			userToken = new Token();
			userToken.setIp(ip);
			userToken.setRegion(ipdata.getProvince()+ipdata.getCity());
			userToken.setId(UKTools.getUUID());
			userToken.setUserid(playUserClient.getId());
			userToken.setCreatetime(new Date());
			userToken.setOrgi(playUserClient.getOrgi());
			AccountConfig config = CacheConfigTools.getGameAccountConfig(BMDataContext.SYSTEM_ORGI) ;
    		if(config!=null && config.getExpdays() > 0){
    			userToken.setExptime(new Date(System.currentTimeMillis()+60*60*24*config.getExpdays()*1000));//默认有效期 ， 7天
    		}else{
    			userToken.setExptime(new Date(System.currentTimeMillis()+60*60*24*7*1000));//默认有效期 ， 7天
    		}
			userToken.setLastlogintime(new Date());
			userToken.setUpdatetime(new Date(0));

			tokenRes.save(userToken) ;
		}

		logger.info("------init token---:"+userToken.getId());

		playUserClient.setToken(userToken.getId());
		playUserClient.setIp(ip);
		playUserClient.setOnline(true);

		if(StringUtils.isEmpty(playUserClient.getUserurl())) {
			playUserClient.setUserurl(DataConstants.GAME_USER_DEFAULT_URL);
		}

		UKTools.intiGPSInfo(request,playUserClient);


		ResultData playerResultData = new ResultData( playUserClient!=null , playUserClient != null ? MessageEnum.USER_REGISTER_SUCCESS: MessageEnum.USER_REGISTER_FAILD_USERNAME , playUserClient , userToken) ;
		GameConfig gameConfig = CacheConfigTools.getGameConfig(userToken.getOrgi()) ;
		if(gameConfig!=null){
			playerResultData.setGametype(gameConfig.getGamemodel());
			playerResultData.setNoaiwaitime(gameConfig.getTimeout());	//无AI的时候 等待时长
			playerResultData.setNoaimsg(gameConfig.getTimeoutmsg());    //无AI的时候，到达最大时长以后的 提示消息，提示完毕后，解散房间
			/**
			 * 封装 游戏对象，发送到客户端
			 */
			/**
			 * 找到游戏配置的 模式 和玩法，如果多选，则默认进入的是 大厅模式，如果是单选，则进入的是选场模式
			 */
			playerResultData.setGames(GameUtils.games(gameConfig.getGametype()));
		}
		AiConfig aiConfig = CacheConfigTools.getAiConfig(userToken.getOrgi()) ;
		if(aiConfig!=null){
			playerResultData.setEnableai(aiConfig.isEnableai());
			playerResultData.setWaittime(aiConfig.getWaittime());
		}

		logger.info("------init  ai ---:"+ gameConfig.getId());



		logger.info("------save  client ---:"+ playUserClient.getId());
		playUserClientRes.save(playUserClient);


		logger.info("------save  cache ---:"+ playUserClient.getId());
		CacheHelper.getApiUserCacheBean().put(userToken.getId(),userToken, userToken.getOrgi());
		CacheHelper.getApiUserCacheBean().put(playUserClient.getId(),playUserClient, userToken.getOrgi());

		logger.info("------finish login---:"+ playUserClient.getId());
		/**
		 * 根据游戏配置 ， 选择 返回的 玩法列表
		 */
        return new ResponseEntity<>(playerResultData, HttpStatus.OK);
    }


	/**
	 * 注册用户
	 * @param player
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public PlayUserClient register(PlayUser player , IP ipdata , HttpServletRequest request ) throws IllegalAccessException, InvocationTargetException{
		PlayUserClient playUserClient = GameUtils.create(player, ipdata, request) ;
		return playUserClient ;
	}

}