var pageSize = 0;
var lastIndex = 0;
var loadingFlag = false;

function refreshPlayerGameData(isInit) {

    if (isInit) {
        pageSize = 0;
        lastIndex = 0;
    }
    if (loadingFlag) {
        return;
    }

    loadingFlag = true;
    getRequestPlayerData("/agent/movediamond/querymyrecord?pagesize=" + pageSize,
        {});

}

function queryPlayer() {
    var playerId = $("#search").val();
    if (playerId) {

        getRequestSinglePlayerData("/record/querygameuser?gameid=" + playerId,
            {});

    } else {
        $.toast('请输入玩家ID');
    }
}

function applyTransfer() {

    var username = $('#targetUserGamename').val();
    var userid = $('#targetUserGameid').val();
    var myCoin = parseInt($('#myCoin').val());
    var transferNums = parseInt($('#transferNums').val());
    if (username && userid) {

        if (myCoin > 0 && transferNums < myCoin) {

            $.confirm("将转入" + transferNums + "钻石到" + username + "(" + userid + ")的账户"
                , "转钻确认"
                , function () {
                    getRequestTransfertData("/agent/movediamond/move?targetid=" + userid + "&movenum=" + transferNums,
                        {});
                });
        } else {
            $.toast('转入的钻石数量不能大于' + myCoin);
        }
    } else {
        $.toast('没有要转入的玩家');

    }

}

function getPlayerDataCallback(itemDatas) {

    loadingFlag = false;

    if (itemDatas && itemDatas.hasOwnProperty("totalsize")) {
        if (lastIndex == 0) {
            $('#player_container').empty();
            $('#playerCountView').text(itemDatas.totalsize);
        }

        var contents = itemDatas.data;

        for (var i = 0; i < contents.length; i++) {

            lastIndex++;
            addGamePlayerItems(contents[i]);
        }

        if (lastIndex >= itemDatas.totalsize) {
            $('#player_loadingmore_container').hide();
        } else {
            if (lastIndex > 0) {
                $('#player_loadingmore_container').show();
            } else {
                $('#player_loadingmore_container').hide();
            }
        }

        if (lastIndex > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();
        }

        pageSize++;
    }


}

function addGamePlayerItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.amount);
    console.log(item);

    var html = '<li>\n' +
        '                <div class="row player-table-content">\n' +

        '                    <div class="col-20">\n' +
        item.targetid +
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        '                        <image class="player-game-user-img" src="' + item.targetuserurl + '"></image>\n' +

        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        item.targetusername +
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        item.lastmodify +
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        (item.movenum ? item.movenum : 0) +
        '                    </div>\n' +
        '                </div>\n' +
        '            </li>';


    // 添加新条目
    $('#player_container').append(html);

}


function getRequestPlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getPlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getPlayerDataCallback(result)
        }
    };
    $.ajax(options);

}


function getRequestTransfertData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getRequestTransfertDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getRequestTransfertDataCallback(result)
        }
    };
    $.ajax(options);

}


function getRequestSinglePlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getRequestSinglePlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getRequestSinglePlayerDataCallback(result)
        }
    };
    $.ajax(options);

}


function getRequestSinglePlayerDataCallback(itemDatas) {

    if (itemDatas && itemDatas.hasOwnProperty("id")) {
        $('#playUserName').text(itemDatas.username);
        $('#targetUserGamename').val(itemDatas.username);
        $('#targetUserGameid').val(itemDatas.gameid);

    } else {
        $.toast('没有查到对应的玩家');
    }
}

function getRequestTransfertDataCallback(data) {
    if (data.hasOwnProperty("sourceid")) {
        $.toast('转入成功');

        var myCoin = parseInt($('#myCoin').val());
        var transferNums = parseInt($('#transferNums').val());
        $('#myCoin').val(myCoin - transferNums);

        $('#leftDiamondsSpan').text(myCoin - transferNums);
    } else {
        $.toast(data.responseText);
    }

}