var pageSize = 0;
var lastIndex = 0;
var loadingFlag = false;

function refreshPlayerGameData(isInit) {

    if (isInit) {
        pageSize = 0;
        lastIndex = 0;
    }
    if (loadingFlag) {
        return;
    }

    loadingFlag = true;
    getRequestPlayerData("/agent/withdraw/querymyrecord?chargetype=goldcoin&pagesize="+pageSize,
        {});

}


function applyRebate() {
    var balamt = parseInt($("#balamt").val());
    var minsettlement = parseInt($("#minsettlement").val());
    var maxsettlement = parseInt($("#maxsettlement").val());
    var applyRebate = parseInt($("#applyRebateValue").val());
    if(applyRebate > balamt){
        $.toast('申请网豆不能大于可提现网豆');
        return;
    }
    if(applyRebate < minsettlement){
        $.toast('申请提现网豆不能小于'+minsettlement);
        return;
    }

    if(applyRebate > maxsettlement){
        $.toast('申请提现网豆不能大于'+maxsettlement);
        return;
    }

    getRequestSettlementData("/agent/withdraw/apply?chargetype=goldcoin&withdrawamt="+applyRebate,
        {});
}

function getPlayerDataCallback(itemDatas) {

    loadingFlag = false;

    if (itemDatas && itemDatas.hasOwnProperty("totalsize")) {
        if (lastIndex == 0) {
            $('#player_container').empty();
            $('#settlementCount').text(itemDatas.totalsize);

        }

        var contents = itemDatas.data;

        for (var i = 0; i < contents.length; i++) {

            lastIndex++;
            addGamePlayerItems(contents[i]);
        }

        if (lastIndex >= itemDatas.totalsize) {

            $('#player_loadingmore_container').hide();
        } else {
            if (lastIndex > 0) {

                $('#player_loadingmore_container').show();
            }else {
                $('#player_loadingmore_container').hide();
            }
        }

        if (lastIndex > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();


        }

        pageSize++;
    }else {
        $('#settlementCount').text('0');
    }


}

function addGamePlayerItems(item) {
    // 生成新条目的HTML

    console.log(item);
    var status = item.applyflag;
    if(status == 'A'){
        status = '申请中';
    }else if(status == 'P'){
        status = '已审核';
    }
    var html = '<li>\n' +
        '                <div class="row player-table-content">\n' +
        '                    <div class="col-20">\n' +
        item.lastmodify    +
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        (item.balbegin ? item.balbegin : 0) +

        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        (item.balend ? item.balend : 0) +

        '                    </div>\n' +

        '                    <div class="col-20" style="color: red">\n' +
        status      +
        '                    </div>\n' +

        '                    <div class="col-20">\n' +
        (item.withdrawamt ? item.withdrawamt : 0) +
        '                    </div>\n' +

        '                </div>\n' +
        '            </li>';


    // 添加新条目
    $('#player_container').append(html);

}


function getRequestPlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getPlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getPlayerDataCallback(result)
        }
    };
    $.ajax(options);

}


function getRequestSettlementData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getPlayerSettlementDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getPlayerSettlementDataCallback(result)
        }
    };
    $.ajax(options);

}

function getPlayerSettlementDataCallback(data) {

    var text = data.responseText;
    if(text && text.indexOf("成功") > 0 ){
        var balamt = parseInt($("#balamt").val());
        var applyRebate = parseInt($("#applyRebateValue").val());
        $("#balamtSpan").text(balamt- applyRebate);
        $("#balamt").val(balamt- applyRebate);
        $("#applyRebateValue").val('');
    }
    $.toast(data.responseText);
}