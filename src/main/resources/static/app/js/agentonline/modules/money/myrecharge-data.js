var pageSize = 0;
var lastIndex = 0;
var loadingFlag = false;

function refreshPlayerGameData(isInit) {

    if (isInit) {
        pageSize = 0;
        lastIndex = 0;
    }
    if (loadingFlag) {
        return;
    }
    var begindate = $("#begindate").val();
    var enddate = $("#enddate").val();
    if(begindate){
        begindate = begindate.replace("-","");
        begindate = begindate.replace("-","");
        begindate = begindate.replace("/","");
        begindate = begindate.replace("/","");
    }
    if(enddate){
        enddate = enddate.replace("-","");
        enddate = enddate.replace("-","");
        enddate = enddate.replace("/","");
        enddate = enddate.replace("/","");
    }
    loadingFlag = true;
    getRequestPlayerData("/agent/chargequery/querybygameid?chargetype=amt&pagesize="+pageSize+"&begindate="+begindate+"&enddate="+enddate,
        {});

}

function getPlayerDataCallback(itemDatas) {

    loadingFlag = false;

    if (itemDatas && itemDatas.hasOwnProperty("totalsize")) {
        if (lastIndex == 0) {
            $('#player_container').empty();
            $('#playerCountView').text(itemDatas.totalsize);

        }

        var contents = itemDatas.data;

        for (var i = 0; i < contents.length; i++) {

            lastIndex++;
            addGamePlayerItems(contents[i]);
        }
        if (lastIndex >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载

        }
        if (lastIndex >= itemDatas.totalsize) {

            $('#player_loadingmore_container').hide();
        } else {
            if (lastIndex > 0) {

                $('#player_loadingmore_container').show();
            }else {
                $('#player_loadingmore_container').hide();
            }
        }

        if (lastIndex > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();


        }

        pageSize++;
    }else {
        $("#game-empty-all").show();
        $('#player_loadingmore_container').hide();
    }


}

function addGamePlayerItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.amount);
    console.log(item);

    var html = '<li>\n' +
        '                <div class="row player-table-content">\n' +
        '                    <div class="col-20">\n' +
        item.username    + "/"+
        item.gameid+
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        '                        <image class="player-game-user-img" src="'+item.userurl+'"></image>\n' +

        '                    </div>\n' +

        '                    <div class="col-20">\n' +
        item.lastmodify+

        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        (item.chargeamt ? parseFloat(item.chargeamt)/100 : 0) +

        '                    </div>\n' +
        '                    <div class="col-20">\n' +

        (item.incomeamt ? parseFloat(item.incomeamt)/100 : 0) +
        '                    </div>\n' +
        '                </div>\n' +
        '            </li>';


    // 添加新条目
    $('#player_container').append(html);

}


function getRequestPlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getPlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getPlayerDataCallback(result)
        }
    };
    $.ajax(options);

}