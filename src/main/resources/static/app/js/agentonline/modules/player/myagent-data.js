var pageSize = 0;
var lastIndex = 0;
var loadingFlag = false;

function refreshPlayerGameData(isInit) {

    if (isInit) {
        pageSize = 0;
        lastIndex = 0;
    }
    if (loadingFlag) {
        return;
    }
    var filterId = $("#search").val();
    loadingFlag = true;
    getRequestPlayerData("/agent/myagent/querybygameid?pagesize="+pageSize+"&gameid="+filterId,
        {});

}

function getPlayerDataCallback(itemDatas) {

    loadingFlag = false;
    console.log(itemDatas);
    if (itemDatas && itemDatas.hasOwnProperty("totalsize")) {
        if (lastIndex == 0) {
            $('#player_container').empty();
            $('#playerCountView').text(itemDatas.totalsize);
            $('#pesentView').text(itemDatas.persent * 100);

        }

        var contents = itemDatas.useragentmaps;

        for (var i = 0; i < contents.length; i++) {

            lastIndex++;
            addGamePlayerItems(contents[i]);
        }


        if (lastIndex >= itemDatas.totalsize) {

            $('#player_loadingmore_container').hide();
        } else {
            if (lastIndex > 0) {
                $('#player_loadingmore_container').show();
            }else {
                $('#player_loadingmore_container').hide();
            }
        }

        if (lastIndex > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();


        }

        pageSize++;
    }


}

function addGamePlayerItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.amount);
    console.log(item);

    var html = '<li>\n' +
        '                <div class="row player-table-content">\n' +
        '                    <div class="col-20">\n' +
        lastIndex    +
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        item.gameid+
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        '                        <image class="player-game-user-img" src="'+item.userurl+'"></image>\n' +

        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        item.username+
        '                    </div>\n' +
        '                    <div class="col-20">\n' +
        (item.chargeamt ? item.chargeamt:0)+
        '                    </div>\n' +
        '                </div>\n' +
        '            </li>';


    // 添加新条目
    $('#player_container').append(html);

}


function getRequestPlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getPlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getPlayerDataCallback(result)
        }
    };
    $.ajax(options);

}