/**
 * Created by michael on 10/31/17.
 */


$(document).on('click','#sysid', function () {
    var buttons1 = [
        {
            text: '请选择',
            label: true
        },
        {
            text: '代理海报',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goPoster";
            }
        },
        {
            text: '充值统计',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goRechargestat";
            }
        },
        {
            text: '发红包统计',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goPackagestat";
            }
        },
        {
            text: '我的下级',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goSubordinatelist";
            }
        },
        {
            text: '收益统计',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goIncome";
            }
        },
        {
            text: '游戏统计',
            bold: true,
            color: 'danger',
            onClick: function() {
                window.location.href="gameAgentController.do?goGamestat";
            }
        },

    ];
    var buttons2 = [
        {
            text: '取消',
            bg: 'danger'
        }
    ];
    var groups = [buttons1, buttons2];
    $.actions(groups);
});









$(document).on('click','#sendid', function () {
    window.location.href="gamePacketsendController.do?goSend";
});
$(document).on('click','#topupid', function () {
    window.location.href="gameTopupController.do?goTopup";
});

$(document).on('click','#homeid', function () {
    window.location.href="gameUserController.do?goHome";
});

