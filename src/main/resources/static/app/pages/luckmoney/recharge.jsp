<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>购买金币</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/luckmoney.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external active"  id="homeid">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external" id="topupid">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" id="sendid" href="#">
                <span class="icon icon-star"></span>
                <span class="tab-label">发红包</span>
            </a>
            <a class="tab-item external"  href="#" id="sysid" name="sysname">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="content-padded grid-demo">
                        <div class="row home-title">
                            <div class="col-33">
                                <div class="home-user-info-container">
                                    <div class="home-user-info-item">
                                        ID : 3448884
                                    </div>
                                    <div class="home-user-info-item">
                                        ID : 3448884
                                    </div>
                                </div>
                            </div>
                            <div class="col-33">
                                <div class="home-user-head-container">
                                    <image class="home-user-img" src="webpage/mobile/pages/images/head.jpg"></image>
                                </div>
                            </div>
                            <div class="col-33">
                                <div class="home-user-head-refresh">刷新</div>
                                <div class="home-user-info-container">
                                    <div class="home-user-info-item">
                                        ID : 3448884
                                    </div>
                                    <div class="home-user-info-item">
                                        ID : 3448884
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row home-game-container">
                            <div class="row recharge-btn-desc">
                                <div class="col-25">充值账号：</div>
                                <div class="col-75">3448884</div>
                            </div>
                            <div class="row">
                                <div class="col-25 recharge-btn-desc">微信支付：</div>
                                <div class="col-75 recharge-btn-group">
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">20 金币</a>
                                    </div>
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">50 金币</a>
                                    </div>
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">100 金币</a>
                                    </div>
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">200 金币</a>
                                    </div>
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">500 金币</a>
                                    </div>
                                    <div class="recharge-btn-item">
                                        <a href="#" class="button button-warning">1000 金币</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-block">
                            <p><a href="recharg-form.jsp" class="button button-round">立刻充值</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/js/wxgame_foot.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>
</html>
