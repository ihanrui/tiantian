<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>创建红包</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/luckmoney.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external active"  id="homeid">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external" id="topupid">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" id="sendid" href="#">
                <span class="icon icon-star"></span>
                <span class="tab-label">发红包</span>
            </a>
            <a class="tab-item external"  href="#" id="sysid" name="sysname">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content">
            <div class="row home-title">
                <div class="col-33">
                    <div class="home-user-info-container">
                        <div class="home-user-info-item">
                            ID : 3448884
                        </div>
                        <div class="home-user-info-item">
                            ID : 3448884
                        </div>
                    </div>
                </div>
                <div class="col-33">
                    <div class="home-user-head-container">
                        <image class="home-user-img" src="webpage/mobile/pages/images/head.jpg"></image>
                    </div>
                </div>
                <div class="col-33">
                    <div class="home-user-head-refresh">刷新</div>
                    <div class="home-user-info-container">
                        <div class="home-user-info-item">
                            ID : 3448884
                        </div>
                        <div class="home-user-info-item">
                            ID : 3448884
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-block-title">红包信息</div>
            <div class="list-block">
                <ul>
                    <!-- Text inputs -->
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">金额</div>
                                <div class="item-input">
                                    <input type="text" id='picker-amount' value="5" placeholder="请选择金额"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">红包数量</div>
                                <div class="item-input">
                                    <input type="text" id='picker-count' value="5" placeholder="请选择红包数"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">尾号</div>
                                <div class="item-input">
                                    <input type="text" id='picker-number' value="8" placeholder="请选择尾号数"/>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="content-block">
                <div class="row">
                    <div class="col-50"><a href="game-list.jsp"
                                           class="button button-big button-fill button-danger">取消</a></div>
                    <div class="col-50"><a href="package-detail.jsp"
                                           class="button button-big button-fill button-success">发红包</a></div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/js/wxgame_foot.js' charset='utf-8'></script>
<script>
    $.init();
</script>
<script>

    $("#picker-amount").picker({
        toolbarTemplate: '<header class="bar bar-nav">\
                                          <button class="button button-link pull-left"></button>\
                                          <button class="button button-link pull-right close-picker">确定</button>\
                                          <h1 class="title">请选择金额</h1>\
                                          </header>',
        cols: [
            {
                textAlign: 'center',
                values: ['5', '10', '20', '30', '50', '100', '200', '500', '1000']
            }
        ]
    });

    $("#picker-count").picker({
        toolbarTemplate: '<header class="bar bar-nav">\
                                          <button class="button button-link pull-left"></button>\
                                          <button class="button button-link pull-right close-picker">确定</button>\
                                          <h1 class="title">请选择红包数量</h1>\
                                          </header>',
        cols: [
            {
                textAlign: 'center',
                values: ['5', '7', '10']
            }
        ]
    });
    $("#picker-number").picker({
        toolbarTemplate: '<header class="bar bar-nav">\
                                          <button class="button button-link pull-left"></button>\
                                          <button class="button button-link pull-right close-picker">确定</button>\
                                          <h1 class="title">请选择中雷尾号</h1>\
                                          </header>',
        cols: [
            {
                textAlign: 'center',
                values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            }
        ]
    });
</script>
</body>
</html>
