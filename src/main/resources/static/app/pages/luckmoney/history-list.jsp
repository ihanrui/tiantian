<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>历史记录</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/luckmoney.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">

        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="content-padded grid-demo">
                        <div class="row home-title">
                            <div class="history-head-title">游戏记录</div>
                            <div class="home-user-head-refresh">关闭</div>
                        </div>

                        <div class="list-block media-list history-item-container">

                            <ul class="detail-ul-container detail-ul-container-radius">
                                <li>
                                    <a href="#" class="item-link item-content">
                                        <div class="item-media">
                                            <img class="detail-user-img"
                                                 src="http://gqianniu.alicdn.com/bao/uploaded/i4//tfscom/i3/TB10LfcHFXXXXXKXpXXXXXXXXXX_!!0-item_pic.jpg_250x250q60.jpg">
                                        </div>
                                        <div class="item-inner" style='background-image: none'>
                                            <div class="row">
                                                <div class="col-33">
                                                    <div class="item-text" style="height: 1rem;font-weight: bold;">期号：2783921
                                                    </div>
                                                </div>
                                                <div class="col-33 text-center">
                                                    <div class="item-text " style="height: 1rem"></div>
                                                </div>
                                                <div class="col-33 text-right">
                                                    <div class="item-text " style="height: 1rem">500金币</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-33">
                                                    <div class="item-text " style="height: 1rem">2017-10-19 19:22:33</div>
                                                </div>
                                                <div class="col-33 text-center">
                                                    <div class="item-text " style="height: 1rem"></div>
                                                </div>
                                                <div class="col-33 text-right">
                                                    <div class="item-text " style="height: 1rem">-4.90</div>
                                                </div>

                                            </div>
                                        </div>

                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>
</html>
