<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>收益统计</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/luckmoney.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external active"  id="homeid">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external" id="topupid">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" id="sendid" href="#">
                <span class="icon icon-star"></span>
                <span class="tab-label">发红包</span>
            </a>
            <a class="tab-item external"  href="#" id="sysid" name="sysname">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>

        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="content-padded ">
                        <div class="row home-title">
                            <div class="history-head-title">收益统计</div>
                            <div class="home-user-head-refresh" id="closeid">关闭</div>
                        </div>

                        <div class="list-block  agent-poster-container">
                            <div class="item-title-row agent-poster-stat-item-container">
                                <div class="agent-poster-stat-item-text" >2017-10-26</div>
                                <div class="agent-poster-stat-item-text" >1002</div>
                            </div>
                            <div class="item-title-row agent-poster-stat-item-container">
                                <div class="agent-poster-stat-item-text" >2017-10-25</div>
                                <div class="agent-poster-stat-item-text" >120</div>
                            </div>
                            <div class="item-title-row agent-poster-stat-item-container">
                                <div class="agent-poster-stat-item-text" >2017-10-24</div>
                                <div class="agent-poster-stat-item-text" >1010</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/js/wxgame_foot.js' charset='utf-8'></script>
<script>
    $.init();
    $(document).on('click','#closeid', function () {
        window.location.href="gameUserController.do?goHome";
    });
</script>
</body>
</html>
