<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>历史记录</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/luckmoney.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">

        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="content-padded grid-demo">
                        <div class="row home-title">
                            <div class="history-head-title">牛牛</div>
                            <div class="home-user-head-refresh">关闭</div>
                        </div>

                        <div class="content-block">
                            游戏规则：
                        </div>
                        <div class="content-block introduce-content">
                            <p>1、曾经打卡打卡为了减肥啦就打算到了圣诞节</p>
                            <p>2、记得看过一个研究报告，发现成功与智商等等关系都不大，但与儿时就展现的自我控制力有极大的关系。实验中几个小朋友每人分得一个糖果，
                            并被告知如果现在不吃，等到几个小时后大人回来，可以拿到更多的糖果。结果有的忍不住，就先吃了眼前的一个，后来再也没有糖果吃。
                            而能够忍住眼前诱惑，等到最后的，则得到了几倍的收获。跟踪研究发现，那些儿时就展现出自我约束力的小朋友后期成功的可能性大大提高。在多数人都醉心于“即时满足”（instant
                            gratification） 的世界里，懂得用“滞后满足”（ delayed gratification） 去作交易的人，已先胜一筹了。</p>
                            <p>3、显卡多久记得看过一个研究报告，发现成功与智商等等关系都不大，但与儿时就展现的自我控制力有极大的关系。实验中几个小朋友每人分得一个糖果，并被告知如果现在不吃，等到几个小时后大人回来，可以拿到更多的糖果。结果有的忍不住，就先吃了眼前的一个，后来再也没有糖果吃。而能够忍住眼前诱惑，等到最后的，则得到了几倍的收获。跟踪研究发现，那些儿时就展现出自我约束力的小朋友后期成功的可能性大大提高。在多数人都醉心于“即时满足”（instant
                            gratification） 的世界里，懂得用“滞后满足”（ delayed gratification） 去作交易的人，已先胜一筹了。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>
</html>
