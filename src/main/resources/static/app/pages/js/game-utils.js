

function checkPhone(number){
    if(!number || !(/^1(3|4|5|6|7|8|9)\d{9}$/.test(number))){
        return false;
    }
    return true;
}



function doRequestSend(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {

            if (data.callback) {

                data.callback.call(data.callback, result);
            }
        },
        dataType: "json",
        error: function (result) {

            if (data.callback) {
                data.callback.call(data.callback, result);
            }
        }
    };
    $.ajax(options);


}
