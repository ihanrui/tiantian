
function sendSMSCode() {
    var phoneNumber = $("#phoneNumber").val();
    if (!checkPhone(phoneNumber)) {
        $.toast("请输入正确的手机号！");
        return;
    }
    $('#registerBtn').attr("disabled", "disabled");//将input元素设置为disabled
    if (doRequestSend("wxgame/wxGameLoginController.do?sendMobileCode", {"phoneNumber": phoneNumber})) {
        updateBtnDesc();
        $.toast("已发送");
    } else {
        //$('#registerBtn').attr("enabled", "enabled");//将input元素设置为disabled
    }
}

var sendSec = 59;

function updateBtnDesc() {
    setTimeout(function () {
        if (sendSec > 0) {
            sendSec = sendSec - 1;
            $("#registerBtn").text(sendSec + "s 重新发送");
            updateBtnDesc();
        } else {
            $("#registerBtn").text("获取注册码");
            $('#registerBtn').attr("enabled", "enabled");//将input元素设置为disabled
        }
    }, 1000);
}

function toLogin() {
    var phoneNumber = $("#phoneNumber").val();
    var smsCode = $("#smsCode").val();
    if (!checkPhone(phoneNumber)) {
        $.toast("请输入正确的手机号！");
        return;
    }
    if (smsCode.length < 4) {
        $.toast("请输入4位验证码");
        return;
    }

    $.showPreloader('登陆中');

    if(doRequestSend("wxGameLoginController.do?validteMobileCode", {phoneNumber: phoneNumber, code: smsCode})){
        $.hidePreloader();
        $.router.load("wxGameLoginController.do?loginValidate", true)
    }
}


function doRequestSend(requestUrl, data) {
    var returnvalue = false;
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers:{
            'Content-Type':'text/html;charset=UTF-8',
            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: false,
        success: function (result){

            if (result && result.jsonStr) {
                var resObj = eval('[' + result.jsonStr + ']')[0];
                $.toast(resObj.msg);
                returnvalue = resObj.success;

            } else {
                $.toast("数据验证出错！");
            }
        },
        dataType: "json",
        error: function (result) {

            if (result && result.jsonStr) {
                var resObj = eval('[' + result.jsonStr + ']')[0];
                $.toast(resObj.msg);
                returnvalue = resObj.success;

            } else {
                $.toast("数据验证出错！");
            }
        }
    };
    $.ajax(options);
    return returnvalue;

}
