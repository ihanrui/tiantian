var pageIndexOther = 0;
var lastIndexOther = 0;
var otherLoadingFlag = false;

function refreshOtherBetGameData(isInit, gameType,filterUserId, winFlag) {

    if (isInit) {
        pageIndexOther = 0;
        lastIndexOther = 0;
    }

    if(!otherLoadingFlag){
        otherLoadingFlag = true;
        getRequestOpenGameData("sscMembermessageController.do?datagridBetOpenQueryList",
            {   pageIndex: pageIndexOther,
                gameType: gameType,
                filterUserId:filterUserId,
                winFlag:winFlag
            });
    }

}

function getGameDataOtherCallback(itemDatas) {

    otherLoadingFlag = false;
    if (!itemDatas.success) {
        $.toast(itemDatas.msg);
    } else {
        if (lastIndexOther == 0) {
            $('.bet-all-other').empty();
        }
        var contents = itemDatas.content;
        for (var i = 0; i < contents.length; i++) {
            addBetGameOtherItems(contents[i]);
            lastIndexOther++;
        }
        if (lastIndexOther >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            if($('#bet-all-preloader').length > 0){

                $.detachInfiniteScroll($('#tab_communication'));
                // 删除加载提示符
                $('#bet-all-preloader').remove();
            }
        }
        if (lastIndexOther > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();
            $("#game-empty-all").css("display", "flex");
        }
        pageIndexOther++;
    }

}

function addBetGameOtherItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.orderamt);
    console.log(item);

    var htmlTeam = '<div class="row lottery-m-qishu-history-show">\n' +
        '                                <div class="item red">' + item.a1 + '</div>\n' +
        '                                <div class="item red">' + item.a2 + '</div>\n' +
        '                                <div class="item red">' + item.a3 + '</div>\n' +
        '                                <div class="item red">' + item.a4 + '</div>\n' +
        '                                <div class="item red">' + item.a5 + '</div>\n' +
        '                                <div class="item blue">' + item.team_total + '</div>\n' +
        '                                <div class="item red-deep">' + item.lh + '</div>\n' +
        '                                <div class="item blue">' + item.dx + '</div>\n' +
        '                                <div class="item red">' + item.ds + '</div>\n' +
        '                                <div class="niuniu-item green">' + item.nn + '</div>\n' +
        '                            </div>';

    var jquerySelector = 'select option[value=\''+item.ordertype+'\']';

    var openDesc = "<div class=\"item-text bet-unwin-red-text\"  style=\"height: 1rem;\">未中</div>";
    if(item.winflag == 'Y'){
        var winamt = new Number(item.winamt);
        openDesc = "<div class=\"item-text bet-win-red-text\"  style=\"height: 1rem;\">赢"+winamt.toFixed(2)+"</div>";
    }


    var html = '<li>' +
        '<a href="#" class="item-content">' +
        '<div class="item-media">' +
        '<img class="detail-user-img"' +
        'src="' + item.wx_head + '">' +
        '</div>' +
        '<div class="item-inner" style="padding-top: 1.2rem">' +
        '<div class="row">' +
        '   <div class="col-33">' +
        '       <div class="item-text"' +
        '         style="height: 1rem;font-weight: bold;">' + item.wx_name +
        '       </div>' +
        '   </div>' +
        '   <div class="col-33 text-center">' +
        openDesc +
        '   </div>' +
        '   <div class="col-33 text-right">' +
        '       <div class="item-text " style="height: 1rem">' + $(jquerySelector).text() + item.ordercontent + '</div>' +
        '   </div>' +
        '    </div>' +
        ' <div class="row">' +
        '   <div class="col-60">' +
        '   <div class="item-text " style="height: 1rem">' + item.term +
        '   </div>' +
        '   </div>' +

        '   <div class="col-40 text-right">' +
        '   <div class="bet-in-text ">' + orderamt.toFixed(2) + ' 元</div>' +
        '   </div>' +

        '</div>' +
        htmlTeam +
        ' <div class="row">' +

        '   <div class="bet-text-create-time">' + item.create_date + '</div>' +


        '</div>' +
        '</div>' +
        '</a>' +
        '</li>';


    // 添加新条目
    $('.bet-all-other').append(html);

}



function getRequestOpenGameData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            getGameDataOtherCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getGameDataOtherCallback(result)
        }
    };
    $.ajax(options);


}
