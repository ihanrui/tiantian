var pageIndexUnopen = 0;
var lastIndexUnopen = 0;

function refreshUnopenBetGameData(isInit, gameType,filterUserId, winFlag) {

    if (isInit) {
        pageIndexUnopen = 0;
        lastIndexUnopen = 0;
    }

    getRequestUnpenGameData("sscMembermessageController.do?datagridBetQueryList",
        {   pageIndex: pageIndexUnopen,
            gameType: gameType,
            filterUserId:filterUserId,
            winFlag:winFlag
        });

}

function getGameDataUnopenCallback(itemDatas) {


    if (!itemDatas.success) {
        $.toast(itemDatas.msg);
    } else {
        if (lastIndexUnopen == 0) {
            $('.bet-all-unopen').empty();
        }
        var contents = itemDatas.content;
        for (var i = 0; i < contents.length; i++) {
            addBetGameUnopenItems(contents[i]);
            lastIndexUnopen++;
        }
        if (lastIndexUnopen >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            //$.detachInfiniteScroll($('.infinite-scroll'));
            // 删除加载提示符
            //$('.infinite-scroll-preloader').remove();
        }
        if (lastIndexUnopen > 0) {
            $("#game-empty-container").hide();
        } else {
            $("#game-empty-container").show();
            $("#game-empty-container").css("display", "flex");
        }
        pageIndexUnopen++;
    }

}

function addBetGameUnopenItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.orderamt);
    console.log(item);
    var jquerySelector = 'select option[value=\''+item.ordertype+'\']';
    var html = '<li>' +
        '<a href="#" class="item-content">' +
        '<div class="item-media">' +
        '<img class="detail-user-img"' +
        'src="' + item.wx_head + '">' +
        '</div>' +
        '<div class="item-inner" style="padding-top: 1.2rem">' +
        '<div class="row">' +
        '<div class="col-40">' +
        '<div class="item-text"' +
        'style="height: 1rem;font-weight: bold;">' + item.wx_name +
        '</div>' +
        '</div>' +
        '   <div class="col-60 text-right">' +
        '   <div class="item-text " style="height: 1rem">' + $(jquerySelector).text() + item.ordercontent + '</div>' +
        '   </div>' +
        '    </div>' +
        ' <div class="row">' +
        '   <div class="col-60">' +
        '   <div class="item-text " style="height: 1rem">' + item.term +
        '   </div>' +
        '   </div>' +

        '   <div class="col-40 text-right">' +
        '   <div class="bet-in-text ">' + orderamt.toFixed(2) + ' 元</div>' +
        '   </div>' +

        '</div>' +
        ' <div class="row">' +

        '   <div class="bet-text-create-time">' + item.create_date + '</div>' +


        '</div>' +
        '</div>' +
        '</a>' +
        '</li>';


    // 添加新条目
    $('.bet-all-unopen').append(html);

}

function getRequestUnpenGameData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            getGameDataUnopenCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getGameDataUnopenCallback(result)
        }
    };
    $.ajax(options);


}

