

function initRechargeEvent() {

    console.log("---here--");
    //初始化 投注按钮事件
    $('.recharge-btn-item').on('click', function () {

        var url = $(this).attr('url')
        if(url){
            window.location = url;
        }
    });
    //初始化 投注按钮事件
    $('.user-head-refresh').on('click', function () {
        var url = $(this).attr('url')
        if(url){
            window.location = url;
        }
    });

    $('.discharge-line-yellow').on('click', function () {
        showAddCaiwuModal();
    });

}



function initDischargeEvent() {

//初始化 投注按钮事件
    console.log("---here22--");

    $('#doDisRechargeBtn').on('click', function () {
        var disAmount = $('#betDischargeInput').val();
        var disRechargeNumber = new Number(disAmount);
        $('#betDischargeInput').val(disRechargeNumber.toFixed(2));

        if(disRechargeNumber > 0){
            var userTotalLeft = $('#userTotalLeft').val();
            var fTotal = parseFloat(userTotalLeft);

            if(fTotal < disRechargeNumber){
                $.toast("不能超过账户余额:"+userTotalLeft);
            }else {

                $.confirm('确定是否提现'+disRechargeNumber.toFixed(2)+'元?', '提现确认',
                    function () {
                        confirmDisCharge();
                    },
                    function () {

                    }
                );

            }

        }else {
            $.toast("提现金额需大于0")
        }
    });
    //初始化 投注按钮事件
    $('.discharge-line-yellow').on('click', function () {
        showAddCaiwuModal();
    });



}

function confirmDisCharge() {
    $.showPreloader('提现处理中');
    var disAmount = $('#betDischargeInput').val();
    getRequestDischargeData("sscMemberRechargeController.do?onlineDownRecharge",{
        amount:disAmount
    });
}

function showAddCaiwuModal() {
    $.modal({
        title: '长按二维码添加财务微信',
        text: '<div class="lottery-m-check-history-container" style="height: 10rem; ">' +
        '<img src="webpage/mobile/pages/images/recharge/caiwu.jpeg" width="100%" height="100%"  style="display:block">' +
        '</div>',
        buttons: [
            {
                text: '关闭',
                bold: true
            },
        ]
    });
}

function onlineDownRechargeCallback(result) {
    //console.log(result);
    if(result && result.msg){
        $.toast(result.msg);
    }
    $.hidePreloader();
    setTimeout(function () {
        location.reload();
    },2000);
}



function getRequestDischargeData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            onlineDownRechargeCallback(result);
        },
        dataType: "json",
        error: function (result) {

            onlineDownRechargeCallback(result)
        }
    };
    $.ajax(options);


}
