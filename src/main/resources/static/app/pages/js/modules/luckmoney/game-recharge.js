function getTradeId() {
    var tradeAmount = $("#tradeAmount").val();
    if (!$.isNumeric(tradeAmount)) {
        $.toast("请输入正确的金额！");
        return;
    }
    var tradeId = getTradeIdRequestSend({rechargeAmount: tradeAmount})
    if (tradeId) {
        $.toast("已发送");
    }
}


function getTradeIdRequestSend(data) {
    var returnvalue = '';
    var options = {
        type: 'POST',
        url: "gameTopupController.do?getTradeId",
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: false,
        success: function (result) {

            if (result && result.jsonStr) {
                var resObj = eval('[' + result.jsonStr + ']')[0];
                if (resObj.success) {
                    returnvalue = resObj.msg;
                } else {
                    $.toast(resObj.msg);
                }
            } else {
                $.toast("数据验证出错！");
            }
        },
        dataType: "json",
        error: function (result) {

            if (result && result.jsonStr) {
                var resObj = eval('[' + result.jsonStr + ']')[0];
                $.toast(resObj.msg);
            } else {
                $.toast("数据验证出错！");
            }
        }
    };
    $.ajax(options);
    return returnvalue;

}
