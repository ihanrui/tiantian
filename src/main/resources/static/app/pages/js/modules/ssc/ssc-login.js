
var smsFlag = false;

function sendSMSCode() {
    var phoneNumber = $("#phoneNumber").val();
    if (!checkPhone(phoneNumber)) {
        $.toast("请输入正确的手机号！");
        return;
    }
    if(!smsFlag){
        doRequestSend("sscLoginController.do?sendMobileCode", {
            "phoneNumber": phoneNumber,
            callback:sendMobileCodeCallback
        });
    }
}

function sendMobileCodeCallback(result) {
    console.log(result);
    if(result && result.msg){
        $.toast(result.msg);
    }
    if(result && result.success){
        $('#recoverFlag').val(result.attributes.recoverFlag);
        smsFlag = true;
        $('#registerBtn').attr("disabled", "disabled");//将input元素设置为disabled
        updateBtnDesc();
    }
}

var sendSec = 59;

function updateBtnDesc() {
    setTimeout(function () {
        if (sendSec > 0) {
            sendSec = sendSec - 1;
            $("#registerBtn").text(sendSec + "s 重新发送");
            updateBtnDesc();
        } else {
            smsFlag = false;
            sendSec = 59;
            $("#registerBtn").text("获取注册码");
            $('#registerBtn').attr("enabled", "enabled");//将input元素设置为disabled
        }
    }, 1000);
}

function toLogin() {
    var phoneNumber = $("#phoneNumber").val();
    var smsCode = $("#smsCode").val();
    if (!checkPhone(phoneNumber)) {
        $.toast("请输入正确的手机号！");
        return;
    }
    if (smsCode.length < 6) {
        $.toast("请输入6位验证码");
        return;
    }
    var recoverFlag = $('#recoverFlag').val();
    if('true' == recoverFlag){
        $.confirm('手机号已存在，原来的账号将迁移到当前账号，请确认是否继续?', '账号迁移确认',
            function () {
                toValidateMobileCode();
            },
            function () {

            }
        );
    }else {
        toValidateMobileCode()
    }
}



function toValidateMobileCode() {

    $.showPreloader('登陆中');
    var phoneNumber = $("#phoneNumber").val();
    var smsCode = $("#smsCode").val();
    doRequestSend("sscLoginController.do?validteMobileCode", {
        phoneNumber: phoneNumber,
        code: smsCode,
        callback:toLoginCallback
    });
}

function toLoginCallback(result) {
    if(result && result.msg){
        $.toast(result.msg);
    }
    if(result && result.success){
        $.hidePreloader();
        window.location = 'sscLoginController.do?toHome';
        //$.router.load("sscLoginController.do?toHome", true)
    }else {

        $.hidePreloader();
    }
}
