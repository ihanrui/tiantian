var pageIndex = 1;
var lastIndex = 0;

function refreshBetGameData(isInit, gameType, winFlag) {

    if (isInit) {
        pageIndex = 1;
        lastIndex = 0;
    }

    getRequestGameData("sscMembermessageController.do?datagridBetQueryList",
        {pageIndex: pageIndex, gameType: gameType});

}

function getGameDataCallback(itemDatas) {


    if (!itemDatas.success) {
        $.toast(itemDatas.msg);
    } else {
        if (lastIndex == 0) {
            $('.bet-myself').empty();
        }
        var contents = itemDatas.content;
        for (var i = 0; i < contents.length; i++) {
            addBetGameItems(contents[i]);
            lastIndex++;
        }
        if (lastIndex >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            //$.detachInfiniteScroll($('.infinite-scroll'));
            // 删除加载提示符
            //$('.infinite-scroll-preloader').remove();
        }
        if (lastIndex > 0) {
            $("#game-empty-container").hide();
        } else {
            $("#game-empty-container").show();
            $("#game-empty-container").css("display", "flex");
        }
        pageIndex++;
    }
    console.log(itemDatas)
}

function addBetGameItems(item) {
    // 生成新条目的HTML
    var html = '<li>' +
        '<a href="#" class="item-content">' +
        '<div class="item-media">' +
        '<img class="detail-user-img"' +
        'src="' + item.wx_head + '">' +
        '</div>' +
        '<div class="item-inner" style="padding-top: 1.2rem">' +
        '<div class="row">' +
        '<div class="col-40">' +
        '<div class="item-text"' +
        'style="height: 1rem;font-weight: bold;">' + item.wx_name +
        '</div>' +
        '</div>' +
        '   <div class="col-60 text-right">' +
        '   <div class="item-text " style="height: 1rem">' + item.ordertype + item.ordercontent + '</div>' +
        '   </div>' +
        '    </div>' +
        '   <div class="row">' +
        '   <div class="col-60">' +
        '   <div class="item-text " style="height: 1rem">' + item.term +
        '</div>' +
        '</div>' +

        '<div class="col-40 text-right">' +
        '   <div class="item-text ">' + item.orderamt + ' 元</div>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</a>' +
        '</li>';


    // 添加新条目
    $('.bet-myself').append(html);

}


function getRequestGameData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            getGameDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getGameDataCallback(result)
        }
    };
    $.ajax(options);


}
