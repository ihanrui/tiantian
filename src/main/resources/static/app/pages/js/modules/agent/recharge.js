var _preSign = {};
var shopId='';
function initPageEvent() {

    $('[name="rechargeItems"]' ).on( "click", function() {
        $('[name="checkImg"]' ).css('display','none');
        $(this).find('[name="checkImg"]').css('display',"block");
        shopId = $(this).find('[name="checkImg"]').attr('itemId');
        console.log(shopId);
    });
    $('#rechargeBtn' ).on( "click", function() {


        doRecharge();
    });

}

function doRecharge() {
    debugger
    var gameid = $("#gameid").val();

    var username = $('#targetUserGamename').val();
    var userid = $('#targetUserGameid').val();

    if (username && userid) {

        $.confirm("将给" + username + "(" + userid + ")的账户充值"
            , "确认代冲"
            , function () {

                if(shopId && shopId.length > 0){

                    if(gameid && gameid.length > 0){


                        var rechargeType = "weixin_pay";
                        var rechargeSource = "web";
                        var params = "gameid=" + gameid + "&shopId=" + shopId + "&rechargeType=" + rechargeType + "&rechargeSource=" + rechargeSource;


                        var preSign = getRequestData(
                            "/webpayment/createtrade?1=1&" + params, {
                                gameid: gameid,
                                shopId: shopId,
                                rechargeType: rechargeType,
                                rechargeSource: rechargeSource,
                            },
                            'GET');
                        console.log(preSign);
                        var resultXml = jQuery.parseXML(preSign);
                        var status = $(resultXml).find("status").text();
                        if(status == 'true'){
                            _preSign = {
                                "mchId": $(resultXml).find("mchId").text(),
                                "appId": $(resultXml).find("appId").text(),
                                "apiKey": $(resultXml).find("apiKey").text(),
                                "nonceStr": $(resultXml).find("nonceStr").text(),
                                "prepareId": $(resultXml).find("prepareId").text(),
                                "timeStamp": $(resultXml).find("timeStamp").text(),
                                "paySign": $(resultXml).find("paySign").text()
                            }
                            callpay();
                        }else {
                            alert($(resultXml).find("msg").text());
                        }
                    }else {
                        alert('请输入玩家id');
                    }
                }else {
                    alert('请选择充值商品');

                }
            });
    } else {
        $.toast('没有要充值的玩家');

    }

}


//调用微信JS api 支付
function jsApiCall(params) {
    debugger

    WeixinJSBridge.invoke(
        'getBrandWCPayRequest', {
            "appId": _preSign.appId,     //公众号名称，由商户传入
            "timeStamp": _preSign.timeStamp,         //时间戳，自1970年以来的秒数
            "nonceStr": _preSign.nonceStr, //随机串
            "package": "prepay_id="+_preSign.prepareId,
            "signType": "MD5",         //微信签名方式：
            "paySign": _preSign.paySign //微信签名
        },
        function (res) {

            //alert(JSON.stringify(res));
            //alert(res.err_msg);
            if (res.err_msg == "get_brand_wcpay_request:ok") {
                alert('充值成功!');
            }
            // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
        }
    );

}

function callpay() {

    if (typeof WeixinJSBridge == "undefined") {
        if (document.addEventListener) {
            document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
        } else if (document.attachEvent) {
            document.attachEvent('WeixinJSBridgeReady', jsApiCall);
            document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
        }
    } else {
        jsApiCall();
    }
}


function getRequestData(requestUrl, data, methodType) {
    var returnvalue = false;
    var options = {
        type: methodType,
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: false,
        success: function (result) {
            returnvalue = result.responseText;
        },
        dataType: "json",
        error: function (result) {
            returnvalue = result.responseText;
        }
    };
    $.ajax(options);
    return returnvalue;
}



function queryPlayer() {
    var playerId = $("#gameid").val();
    if (playerId) {

        getRequestSinglePlayerData("/record/querygameuser?gameid=" + playerId,
            {});

    } else {
        $.toast('请输入玩家ID');
    }
}



function getRequestSinglePlayerData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'application/json'
        },
        async: true,
        success: function (result) {
            getRequestSinglePlayerDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getRequestSinglePlayerDataCallback(result)
        }
    };
    $.ajax(options);

}


function getRequestSinglePlayerDataCallback(itemDatas) {

    if (itemDatas && itemDatas.hasOwnProperty("id")) {
        $('#playUserName').text(itemDatas.username);
        $('#playUserName').val(itemDatas.username);
        $('#targetUserGamename').val(itemDatas.username);
        $('#targetUserGameid').val(itemDatas.gameid);

    } else {
        $.toast('没有查到对应的玩家');

        $('#playUserName').text("");
        $('#targetUserGamename').val("");
        $('#targetUserGameid').val("");

    }
}
