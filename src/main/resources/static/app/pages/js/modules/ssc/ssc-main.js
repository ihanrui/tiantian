function initNumberEvent() {

    //初始化 投注按钮事件
    $('.lottery-m-qishu-xiazhu .item').on('click', function () {
        var count = 0;
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
        }
        $('.lottery-m-qishu-xiazhu .item').each(function () {
            if ($(this).hasClass("active")) {
                count++;
            }
        });
        $('#bet-desc').text("已投" + count + '注');

    });
    //初始化连号按钮
    $('.lottery-m-qishu-xiazhu .item-connect').on('click', function () {
        var textvalue = $(this).text();
        var vals = $('#connectNumbers').val();
        $('#connectNumbers').val(vals + textvalue);
    });

    $('#clearConnectNumber').on('click', function () {
        $('#connectNumbers').val('');
    });

    // 初始化 tab类型切换
    $('.bet-tab-items').on('click', function () {
        clearSelectItem();
    });
    // 初始化 tab类型切换
    $('#tab_communication_all').on('click', function () {

        refreshUnopenBetGameData(true,'ssc','','');
    });

    // 初始化投注
    $('#doBetBtn').on('click', function () {
        doBet();
    });
    // 初始化投注
    $('#check_history').on('click', function () {
        showLastestTerms();
    });

    //初始化 投注按钮事件
    $('#sscMembermessageAll').on('click', function () {
        var url = $(this).attr('url')
        if(url){
            window.location = url;
        }
    });

}

function clearSelectItem() {
    $('.lottery-m-qishu-xiazhu .item').removeClass("active");
    $('#bet-desc').text("");

}


function doBet() {

    var amount = $('#betAmountInput').val();
    var betNumber = new Number(amount);
    $('#betAmountInput').val(betNumber.toFixed(2));
    if (betNumber < 1) {
        $('#betAmountInput').focus();
    } else {
        if (betNumber > 2000) {
            $.toast("下注金额不能超过2000");
        } else {

            var ordertype = '';//$('.bet-tab-items').attr('flag');
            var desc = '';
            $('.bet-tab-items').each(function () {
                if ($(this).hasClass("active")) {
                    ordertype = $(this).attr('flag');
                    desc = $(this).text();
                }
            });

            var lstItems = getBetNumbers(ordertype);
            if (lstItems.length < 1) {
                $.toast("请选择投注项");
                return;
            } else {
                if ('lh' == ordertype) {

                    var connectNumbers = $('#connectNumbers').val();
                    if (connectNumbers != '' && connectNumbers.length > 0) {
                        if (connectNumbers.length > 4) {
                            $.toast("连号不能大于4位");
                            return;
                        }
                    }
                }
            }

            var team = $('#betTeamNext').val();

            confirmToBet(lstItems, betNumber, team, desc, ordertype);
        }
    }

}

function getBetNumbers(ordertype) {
    var lstItems = [];
    if ('lh' == ordertype) {

        var connectNumbers = $('#connectNumbers').val();
        if (connectNumbers != '' && connectNumbers.length > 0) {
            lstItems.push(connectNumbers)
        }

    } else if ('dw' == ordertype) {

        $('.lottery-m-qishu-xiazhu .item').each(function () {
            if ($(this).hasClass("active")) {
                lstItems.push($(this).attr('tag') + '/' + $(this).text());
            }
        });
    } else {

        $('.lottery-m-qishu-xiazhu .item').each(function () {
            if ($(this).hasClass("active")) {
                lstItems.push($(this).text());
            }
        });
    }
    return lstItems;
}

function confirmToBet(lstItems, betAmount, teamCode, typeDesc, ordertype) {

    var lstViews = [];
    var betDesc = '';
    for (var i = 0; i < lstItems.length; i++) {

        betDesc = getOrdertypeDesc(i, ordertype, typeDesc);
        var htmls = '<div class="bet-confirm-item">\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-40 confirm-item-left">投注期号:</div>\n' +
            '                                <div class="col-60 confirm-item-right">第' + teamCode + '期</div>\n' +
            '                            </div>\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-40 confirm-item-left">玩法:</div>\n' +
            '                                <div class="col-60 confirm-item-right">' + betDesc + '</div>\n' +
            '                            </div>\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-40 confirm-item-left">投注号:</div>\n' +
            '                                <div class="col-60 confirm-item-right">\n' +
            '                                    <span class="yellow">' + lstItems[i] + '</span>号\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-40 confirm-item-left">投注金额:</div>\n' +
            '                                <div class="col-60 confirm-item-right"><span class="yellow">' + betAmount + '</span>元</div>\n' +
            '                            </div>\n' +
            '                        </div>';
        lstViews.push(htmls);
    }

    var heightItem = 10;
    if (lstViews > 3) {
        heightItem += (lstViews.length - 3) * 2;
    }
    if (heightItem > 18) {
        heightItem = 18;
    }
    $.modal({
        title: '下注确认',
        text: '<div class="lottery-m-check-history-container" style="height: ' + heightItem + 'rem; ">' +
        '<div style="overflow:scroll">' +
        lstViews.join('') +
        '</div></div>',
        buttons: [
            {
                text: '关闭',
                bold: false
            }, {
                text: '确认',
                bold: true,
                onClick: function () {
                    var ordertype = '';//$('.bet-tab-items').attr('flag');
                    $('.bet-tab-items').each(function () {
                        if ($(this).hasClass("active")) {
                            ordertype = $(this).attr('flag');
                        }
                    });

                    var amount = $('#betAmountInput').val();
                    var team = $('#betTeamNext').val();
                    doRequestSend('sscMembermessageController.do?doBet', {
                        ordertype: ordertype,
                        term: team,
                        gameType: 'ssc',
                        ordercontent: lstItems.join('-'),
                        orderamt: amount,
                        callback: doBetCallback
                    });
                }
            },
        ]
    })
}

function pageDataTimer() {

    window.setInterval(function () {
        doRequestSend("sscLoginController.do?getSSCStatueInfo", {callback: pageStatusCallback});
    }, 3000);

}

function pageStatusCallback(result) {
    console.log(result);

    if (result) {
        var userAmount = new Number(result.statusInfo.userAmount);
        $('#userAmountAccount').text(userAmount.toFixed(2));
        $('#lastTeamNumber').text(result.statusInfo.teamNumber);
        $('#betTeamNumber').text(result.statusInfo.newTeamNumber);
        $('#betTeamNext').val(result.statusInfo.newTeamNumber);
        if (result.statusInfo.teamStatus) {

            if (result.teamInfo && result.teamInfo.hasOwnProperty('a1')) {

                var html = '<div class="item red">' + result.teamInfo.a1 + '</div>\n' +
                    '                            <div class="item red">' + result.teamInfo.a2 + '</div>\n' +
                    '                            <div class="item red">' + result.teamInfo.a3 + '</div>\n' +
                    '                            <div class="item red">' + result.teamInfo.a4 + '</div>\n' +
                    '                            <div class="item red">' + result.teamInfo.a5 + '</div>\n' +
                    '                            <div class="item blue">' + result.teamInfo.total + '</div>\n' +
                    '                            <div class="item red-deep">' + result.teamInfo.lh + '</div>\n' +
                    '                            <div class="item blue">' + result.teamInfo.dx + '</div>\n' +
                    '                            <div class="item red">' + result.teamInfo.ds + '</div>\n' +
                    '                            <div class="niuniu-item green">' + result.teamInfo.nn + '</div>';
                $('#sscTeamDetail').html(html);
            } else {
                var html = '<div class="closeblue">老板请稍等，正在获取开奖信息</div>';
                $('#sscTeamDetail').html(html);
            }
            if (teamCloseFlag) {
                teamCloseFlag = false;
                totalCloseTime = result.statusInfo.fpsj;
                updateCloseDesc();
            }
        } else {
            teamCloseFlag = true;
            $('#sscTeamDetail').html('<div class="closeblue">开奖中，预祝老板中奖</div>')
        }
    }

}

var totalCloseTime = 0;
var teamCloseFlag = true;

function updateCloseDesc() {

    if (!teamCloseFlag && totalCloseTime > 0) {
        totalCloseTime--;
        var min = parseInt(totalCloseTime / 60);
        var secend = totalCloseTime % 60;
        if (min < 10) {
            min = '0' + min;
        }
        if (secend < 10) {
            secend = '0' + secend;
        }
        $('#stopTime').html('<div class="closeblue">投注截止：' + min + ':' + secend + '</div>');
        setTimeout(function () {
            updateCloseDesc();
        }, 1000);
    }

}

function getOrdertypeDesc(index, orderType, orderDesc) {
    if (orderType == 'dw') {

        var wWeiCount = getBetSelectedCount("groupWan");
        if (wWeiCount > 0) {
            if (index + 1 - wWeiCount <= 0) {
                return orderDesc + '- 万位';
            }
        }
        var qWeiCount = getBetSelectedCount('groupQian');
        if (qWeiCount > 0) {
            if (index + 1 - wWeiCount - qWeiCount <= 0) {
                return orderDesc + '- 千位';
            }
        }
        var bWeiCount = getBetSelectedCount('groupBai');
        if (bWeiCount > 0) {
            if (index + 1 - wWeiCount - qWeiCount - bWeiCount <= 0) {
                return orderDesc + '- 百位';
            }
        }
        var sWeiCount = getBetSelectedCount('groupShi');
        if (sWeiCount > 0) {
            if (index + 1 - wWeiCount - qWeiCount - bWeiCount - sWeiCount <= 0) {
                return orderDesc + '- 十位';
            }
        }

        return orderDesc + '- 个位';
    }
    return orderDesc;
}

function getBetSelectedCount(containerId) {
    var count = 0;
    $('#' + containerId + ' .lottery-m-qishu-xiazhu .item').each(function () {
        if ($(this).hasClass("active")) {
            count++;
        }
    });

    return count;
}


function doBetCallback(result) {

    if (result) {
        $.toast(result.msg);
        if (result.success) {
            $('#betAmountInput').val('');
            refreshBetGameData(true, 'ssc', true);
        }
    }
}


function showLastestTerms() {
    doRequestSend('sscWinningrecordController.do?teamLastItems', {
        pageSize: "30",
        callback: showLastestTermsCallback
    });
}

function showLastestTermsCallback(result) {

    if (result) {
        var htmls = [];
        for (var i = 0; i < result.content.length; i++) {
            var itemData = result.content[i];
            var term = itemData.term;
            var msgdate = itemData.msgdate;
            var termNumber = term.substr(msgdate.length, term.length);
            var html = '<div class="row lottery-m-qishu-history-show">\n' +
                '                                <div class="niuniu-item green">' + termNumber + '</div>\n' +
                '                                <div class="item red">' + itemData.a1 + '</div>\n' +
                '                                <div class="item red">' + itemData.a2 + '</div>\n' +
                '                                <div class="item red">' + itemData.a3 + '</div>\n' +
                '                                <div class="item red">' + itemData.a4 + '</div>\n' +
                '                                <div class="item red">' + itemData.a5 + '</div>\n' +
                '                                <div class="item blue">' + itemData.total + '</div>\n' +
                '                                <div class="item red-deep">' + itemData.lh + '</div>\n' +
                '                                <div class="item blue">' + itemData.dx + '</div>\n' +
                '                                <div class="item red">' + itemData.ds + '</div>\n' +
                '                                <div class="niuniu-item green">' + itemData.nn + '</div>\n' +
                '                            </div>';
            htmls.push(html);
        }
        $.modal({
            title: '查看走势图',
            text: '<div class="lottery-m-check-history-container" style="width:100%;height: 12rem; ">' +
            '<div style="overflow:scroll">' +
            htmls.join("") +
            '</div></div>',
            buttons: [
                {
                    text: '关闭',
                    bold: true
                },
            ]
        });

    }
}
