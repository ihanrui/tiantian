var pageIndex = 0;
var lastIndex = 0;
var loadingFlag = false;

function refreshBetGameData(isInit, rechargeType,filterUserId) {

    if (isInit) {
        pageIndex = 0;
        lastIndex = 0;
    }
    if(loadingFlag){
        return;
    }
    loadingFlag = true;
    getRequestGameData("sscMemberRechargeController.do?rechargeQueryList",
        {pageIndex: pageIndex, rechargeType: rechargeType,filterUserId:filterUserId});

}

function getGameDataCallback(itemDatas) {

    loadingFlag = false;
    if (!itemDatas.success) {
        $.toast(itemDatas.msg);
    } else {
        if (lastIndex == 0) {
            $('.bet-myself').empty();
        }
        var contents = itemDatas.content;
        for (var i = 0; i < contents.length; i++) {

            addBetGameItems(contents[i]);
            lastIndex++;
        }
        if (lastIndex >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            if($('#bet-myself-preloader').length > 0){
                $.detachInfiniteScroll($('#tab_new'));
                // 删除加载提示符
                $('#bet-myself-preloader').remove();
            }
        }
        if (lastIndex > 0) {
            $("#game-empty-myself").hide();
        } else {
            $("#game-empty-myself").show();
            $("#game-empty-myself").css("display", "flex");
        }
        pageIndex++;
    }

}

function addBetGameItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.amount);
    console.log(item);
    var jquerySelector = 'select option[value=\''+item.recharge_type+'\']';
    var jquerySelector2 = 'select option[value=\''+item.recharge_status+'\']';
    var html = '<li>' +
        '<a href="#" class="item-content">' +
        '<div class="item-media">' +
        '<img class="detail-user-img"' +
        'src="webpage/mobile/pages/images/ssc/money.png">' +
        '</div>' +
        '<div class="item-inner" style="padding-top: 1.2rem">' +

        ' <div class="row">' +
        '   <div class="col-33">' +
        '   <div class="item-text " style="height: 1rem">充值' +
        '   </div>' +
        '   </div>' +
        '   <div class="col-33 bet-text-create-time">' +
        '       <div class="item-text " style="height: 1rem">' + $(jquerySelector2).text()  + '</div>' +
        '   </div>' +
        '   <div class="col-33 text-right">' +
        '   <div class="bet-in-text ">' + orderamt.toFixed(2) + ' 元</div>' +
        '   </div>' +

        '</div>' +
        ' <div class="row">' +

        '   <div class="bet-text-create-time">' + item.create_date + '</div>' +

        '</div>' +
        '</div>' +
        '</a>' +
        '</li>';


    // 添加新条目
    $('.bet-myself').append(html);

}


function getRequestGameData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            getGameDataCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getGameDataCallback(result)
        }
    };
    $.ajax(options);


}
