var pageIndexOther = 0;
var lastIndexOther = 0;
var otherLoadingFlag = false;

function refreshOtherBetGameData(isInit, rechargeType,filterUserId, approveFlag) {

    if (isInit) {
        pageIndexOther = 0;
        lastIndexOther = 0;
    }

    if(!otherLoadingFlag){
        otherLoadingFlag = true;
        getRequestOpenGameData("sscMemberRechargeController.do?rechargeQueryList",
            {   pageIndex: pageIndexOther,
                rechargeType: rechargeType,
                filterUserId: filterUserId,
                approveFlag: approveFlag
            });
    }

}

function getGameDataOtherCallback(itemDatas) {

    otherLoadingFlag = false;
    if (!itemDatas.success) {
        $.toast(itemDatas.msg);
    } else {
        if (lastIndexOther == 0) {
            $('.bet-all-other').empty();
        }
        var contents = itemDatas.content;
        for (var i = 0; i < contents.length; i++) {
            addBetGameOtherItems(contents[i]);
            lastIndexOther++;
        }
        if (lastIndexOther >= itemDatas.count) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            if($('#bet-all-preloader').length > 0){

                $.detachInfiniteScroll($('#tab_communication'));
                // 删除加载提示符
                $('#bet-all-preloader').remove();
            }
        }
        if (lastIndexOther > 0) {
            $("#game-empty-all").hide();
        } else {
            $("#game-empty-all").show();
            $("#game-empty-all").css("display", "flex");
        }
        pageIndexOther++;
    }

}

function addBetGameOtherItems(item) {
    // 生成新条目的HTML
    var orderamt = new Number(item.amount);



    var jquerySelector2 = 'select option[value=\''+item.recharge_status+'\']';
    var html = '<li>' +
        '<a href="#" class="item-content">' +
        '<div class="item-media">' +
        '<img class="detail-user-img"' +
        'src="webpage/mobile/pages/images/ssc/money.png">' +
        '</div>' +
        '<div class="item-inner" style="padding-top: 1.2rem">' +

        ' <div class="row">' +
        '   <div class="col-33">' +
        '   <div class="item-text " style="height: 1rem">提现' +
        '   </div>' +
        '   </div>' +
        '   <div class="col-33 bet-text-create-time">' +
        '       <div class="item-text " style="height: 1rem">' + $(jquerySelector2).text()  + '</div>' +
        '   </div>' +
        '   <div class="col-33 text-right">' +
        '   <div class="bet-in-text ">' + orderamt.toFixed(2) + ' 元</div>' +
        '   </div>' +

        '</div>' +
        ' <div class="row">' +

        '   <div class="bet-text-create-time">' + item.create_date + '</div>' +

        '</div>' +
        '</div>' +
        '</a>' +
        '</li>';



    // 添加新条目
    $('.bet-all-other').append(html);

}



function getRequestOpenGameData(requestUrl, data) {
    var options = {
        type: 'GET',
        url: requestUrl,
        data: data,
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        },
        async: true,
        success: function (result) {
            getGameDataOtherCallback(result);
        },
        dataType: "json",
        error: function (result) {

            getGameDataOtherCallback(result)
        }
    };
    $.ajax(options);


}
