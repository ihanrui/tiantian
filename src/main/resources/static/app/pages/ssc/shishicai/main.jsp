<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" uri="/easyui-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>重庆时时彩</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">
    <style type="text/css">

        .modal {
            width: 14rem;
        }

    </style>
</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external active" href="#">
                <span class="icon icon-app"></span>
                <span class="tab-label">时时彩</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?toHome">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external" href="sscMemberRechargeController.do?toUserRecharge">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" href="sscMemberRechargeController.do?toUserDischarge">
                <span class="icon icon-star"></span>
                <span class="tab-label">提现</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?sscSystem">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">

                    <div class="content-padded grid-demo">
                        <div class="row">
                            <div class="col-25">
                                <div href="#" class="button button-fill lottery-m-button"
                                   id="sscMembermessageAll"
                                   url="sscMembermessageController.do?sscMembermessageAll">会员消息</div></div>
                            <input type="hidden" value="${teamNumber}" id="betTeam">
                            <input type="hidden" value="${newTeamNumber}" id="betTeamNext">
                            <span style="display: none">

                            <t:dictSelect field="ssc_game" type="list" extendJson="{style:'display:none'}"
                                          typeGroupCode="ssc_game" defaultVal="${sscMemberRechargePage.rechargeStatus}"
                                          title="状态"></t:dictSelect>
                            </span>

                            <div class="col-50 lottery-m-qishu-title"> 第<span id="lastTeamNumber">${teamNumber}</span>期
                            </div>
                            <div class="col-25">
                                <a href="#" id="check_history"
                                   class="button button-fill lottery-m-button button-warning">走势图</a>
                            </div>


                        </div>

                        <div class="row lottery-m-qishu-show" id="sscTeamDetail">
                            <div class="item red">${lastTeam.a1}</div>
                            <div class="item red">${lastTeam.a2}</div>
                            <div class="item red">${lastTeam.a3}</div>
                            <div class="item red">${lastTeam.a4}</div>
                            <div class="item red">${lastTeam.a5}</div>
                            <div class="item blue">${lastTeam.total}</div>
                            <div class="item red-deep">${lastTeam.lh}</div>
                            <div class="item blue">${lastTeam.dx}</div>
                            <div class="item red">${lastTeam.ds}</div>
                            <div class="niuniu-item green">${lastTeam.nn}</div>
                        </div>

                        <div class="row">
                            <div class="col-50 lottery-m-qishu-number-container">第<span
                                class="lottery-m-qishu-number" id="betTeamNumber">${newTeamNumber}</span>期
                            </div>
                            <div class="col-50 lottery-m-qishu-line-time-container" id="stopTime"> 投注截止：00:00</div>
                        </div>

                        <div style="height: 1rem;"></div>
                        <div>

                            <div class="buttons-row">
                                <a href="#tab1" class="tab-link button bet-tab-items active" flag="dm">单码</a>
                                <a href="#tab2" class="tab-link button bet-tab-items" flag="dw">定位</a>
                                <a href="#tab3" class="tab-link button bet-tab-items" flag="lhh">龙虎</a>
                                <a href="#tab4" class="tab-link button bet-tab-items" flag="lh">连号</a>
                                <a href="#tab5" class="tab-link button bet-tab-items" flag="dxds">大小单双</a>
                                <a href="#tab6" class="tab-link button bet-tab-items" flag="nn">牛牛</a>
                            </div>
                            <div class="content-block">
                                <div class="tabs">
                                    <div id="tab1" class="tab active">
                                        <jsp:include page="single.jsp" flush="true"/>
                                    </div>
                                    <div id="tab2" class="tab">
                                        <jsp:include page="position.jsp" flush="true"/>
                                    </div>
                                    <div id="tab3" class="tab">
                                        <jsp:include page="dragon.jsp" flush="true"/>
                                    </div>
                                    <div id="tab4" class="tab">
                                        <jsp:include page="connect.jsp" flush="true"/>
                                    </div>
                                    <div id="tab5" class="tab">
                                        <jsp:include page="bigSmall.jsp" flush="true"/>
                                    </div>
                                    <div id="tab6" class="tab">
                                        <jsp:include page="niuniu.jsp" flush="true"/>
                                    </div>
                                </div>


                                <div class="lottery-m-game-desc-container">

                                    <div class="row lottery-m-game-xiazhu-desc">
                                        <span>余额：</span>
                                        <span class="yellow" id="userAmountAccount">${userAmount}</span>
                                        <span class="red-deep">元</span>
                                        <span class="bet-desc" id="bet-desc"></span>
                                    </div>
                                    <div class="searchbar row">
                                        <div class="col-10">
                                            <div class="lottery-m-xiazhu-coin"></div>

                                        </div>
                                        <div class="search-input col-60"
                                             style="padding-left: 0.2rem;padding-right: 0.2rem;">
                                            <input type="number" id='betAmountInput' placeholder='输入每注下注金额'/>
                                        </div>
                                        <a id="doBetBtn"
                                           class="button button-fill button-primary col-30 button-warning">投注(元)</a>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div style="height: 1rem;"></div>
                        <div class="buttons-row">
                            <a href="#tab_new" class="tab-link active button">最新投注</a>
                            <a href="#tab_communication_all" class="tab-link button">交流区</a>
                        </div>

                        <div class="tabs">
                            <div id="tab_new" class="tab active">
                                <div class="list-block media-list detail-item-container">
                                    <ul class="bet-myself"></ul>
                                </div>

                            </div>
                            <div id="tab_communication_all" class="tab">
                                <div class="list-block media-list detail-item-container bet-all">
                                    <ul class="bet-all-unopen"></ul>

                                </div>
                            </div>
                        </div>
                        <div class="content-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/game-utils.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-main.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-data.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-unopen-data.js' charset='utf-8'></script>
<script>
    $.init();
    initNumberEvent();

    refreshBetGameData(true, 'ssc',true);

    refreshUnopenBetGameData(true,'ssc','','');
    pageDataTimer();
</script>
</body>
</html>
