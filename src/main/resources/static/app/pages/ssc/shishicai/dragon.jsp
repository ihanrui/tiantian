<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="yellow">玩法说明：</span>开奖号号首位大于尾位为龙，首位小于尾位为虎，首尾号码相同为合。
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            龙虎 <span class="red">1赔2</span>
            合 <span class="red">1赔9.5</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">龙</div>
            <div class="item">虎</div>
            <div class="item ">合</div>
        </div>
    </div>
</div>
