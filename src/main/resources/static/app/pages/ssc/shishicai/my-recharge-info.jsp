<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" uri="/easyui-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>我的上下分</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external active" href="#">
                <span class="icon icon-app"></span>
                <span class="tab-label">上下分信息</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?toHome">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external" href="sscMemberRechargeController.do?toUserRecharge">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" href="sscMemberRechargeController.do?toUserDischarge">
                <span class="icon icon-star"></span>
                <span class="tab-label">提现</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?sscSystem">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content" data-type="">

            <header class="bar bar-nav fixed-tab">
                <button class="button pull-right" id="refreshData">
                    刷新
                </button>

                <div class="buttons-row ">
                    <div href="#tab_new" class="tab-link active button">上分记录</div>
                    <div href="#tab_communication" class="tab-link button">下分记录</div>
                    <span style="display: none">

                    <t:dictSelect field="recharge_type" type="list" extendJson="{style:'display:none'}"
                                  typeGroupCode="recharge_type" defaultVal=""
                                  title="状态"></t:dictSelect>
                    </span>
                </div>
            </header>

            <div class="tabs" style="margin-top: 2.4rem">

                <div id="tab_new" class="tab active infinite-scroll  infinite-scroll-bottom" data-distance="100">
                    <div class="list-block media-list detail-item-container">
                        <ul class="bet-myself"></ul>
                    </div>

                    <div class="game-empty-container" id="game-empty-myself" style="display: none">
                        <img class='card-cover game-empty-img' src="webpage/mobile/pages/images/empty_single_item.png" alt="">
                        <p class="color-gray game-empty-desc">暂时没有记录</p>
                    </div>
                    <!-- 加载提示符 -->
                    <div class="infinite-scroll-preloader" id="bet-myself-preloader">
                        <div class="preloader">
                        </div>
                    </div>
                </div>

                <div id="tab_communication" class="tab infinite-scroll infinite-scroll-bottom" data-distance="100">
                    <div class="list-block media-list detail-item-container">
                        <ul class="bet-all-other"></ul>
                    </div>

                    <div class="game-empty-container" id="game-empty-all" style="display: none">
                        <img class='card-cover game-empty-img' src="webpage/mobile/pages/images/empty_single_item.png" alt="">
                        <p class="color-gray game-empty-desc">暂时没有记录</p>
                    </div>
                    <!-- 加载提示符 -->
                    <div class="infinite-scroll-preloader" id="bet-all-preloader">
                        <div class="preloader">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/game-utils.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-recharge-data.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-disrecharge-data.js' charset='utf-8'></script>
<script>

    $(document).on('infinite', function () {
        // 如果正在加载，则退出
        console.log("---loading---");

        if ($(this).find('.infinite-scroll.active').attr('id') == "tab_new") {

            refreshBetGameData(false, 'rechargeIn',true);
        }
        if ($(this).find('.infinite-scroll.active').attr('id') == "tab_communication") {
            refreshOtherBetGameData(false, 'rechargeOut',true);
        }

    });

    $.init();

    $("#refreshData").on('click',function () {

        refreshBetGameData(true, 'rechargeIn',true);
        refreshOtherBetGameData(true, 'rechargeOut',true);
    });

    refreshBetGameData(true, 'rechargeIn' ,true);


    refreshOtherBetGameData(true, 'rechargeOut' ,true);

</script>
</body>
</html>
