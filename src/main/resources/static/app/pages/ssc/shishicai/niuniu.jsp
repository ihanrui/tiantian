<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="red">开奖号码为重庆时时彩</span>，
            官方网址：
            <span class="red">http://www.shishicai.cn/cqssc/touzhu</span>，
        </div>
        <div class="row">
            <span class="yellow">游戏时间：</span>10:00&nbsp;-&nbsp;20.00；
        </div>
        <div class="row">
            <span class="yellow">玩法说明：</span>以开奖的5个数字为基准，5个数字中若有任意3个数字相加之和为0或10在倍数，其余下2个数字之和的个位数为兑奖结果。
            如00018为牛9；01188为牛8，99228为牛牛，12312为无牛！<br>
            11111,22222,33333等皆视为无点，但00000则为牛牛!
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            [牛1-牛9] <span class="red">1赔10</span>
            [牛牛] <span class="red">1赔11</span>
            [无牛] <span class="red">1赔2</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">牛1</div>
            <div class="item">牛2</div>
            <div class="item">牛3</div>
            <div class="item">牛4</div>
            <div class="item">牛5</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">牛6</div>
            <div class="item">牛7</div>
            <div class="item">牛8</div>
            <div class="item">牛9</div>
            <div class="item">牛牛</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">无牛</div>
        </div>

    </div>
</div>
