<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="yellow">玩法说明：</span>请至少选择2个号码进行投注，开奖号码包含全部投注号码为中奖；
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            [2连] <span class="red">1赔5.2</span>
            [3连] <span class="red">1赔20</span><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            [4连] <span class="red">1赔50</span>
            [5连] <span class="red">1赔188</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item-connect">0</div>
            <div class="item-connect">1</div>
            <div class="item-connect">2</div>
            <div class="item-connect">3</div>
            <div class="item-connect">4</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item-connect">5</div>
            <div class="item-connect">6</div>
            <div class="item-connect">7</div>
            <div class="item-connect">8</div>
            <div class="item-connect">9</div>
        </div>
        <div class="searchbar row">
            <div class="search-input col-60"
                 style="padding-left: 0.2rem;padding-right: 0.2rem;">
                <input type="number" id='connectNumbers' placeholder='输入号码'/>
            </div>
            <a class="button button-fill button-primary col-40 button-warning" id="clearConnectNumber">清空重选</a>
        </div>
    </div>
</div>
