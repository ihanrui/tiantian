<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="yellow">玩法说明：</span>定位玩法可以分别对开奖号的万位、千位、百位、十位、个位进行投注；
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            数字 <span class="red">1赔9.8</span>
            大小单双 <span class="red">1赔1.99</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>
        <div id="groupWan">

            <div class="row yellow center">
                万位
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="万">0</div>
                <div class="item" tag="万">1</div>
                <div class="item" tag="万">2</div>
                <div class="item" tag="万">3</div>
                <div class="item" tag="万">4</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="万">5</div>
                <div class="item" tag="万">6</div>
                <div class="item" tag="万">7</div>
                <div class="item" tag="万">8</div>
                <div class="item" tag="万">9</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="万">大</div>
                <div class="item" tag="万">小</div>
                <div class="item" tag="万">单</div>
                <div class="item" tag="万">双</div>
            </div>
        </div>

        <div id="groupQian">


            <div class="row blue center">
                千位
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="千">0</div>
                <div class="item"  tag="千">1</div>
                <div class="item" tag="千">2</div>
                <div class="item" tag="千">3</div>
                <div class="item" tag="千">4</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="千">5</div>
                <div class="item" tag="千">6</div>
                <div class="item" tag="千">7</div>
                <div class="item" tag="千">8</div>
                <div class="item" tag="千">9</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="千">大</div>
                <div class="item" tag="千">小</div>
                <div class="item" tag="千">单</div>
                <div class="item" tag="千">双</div>
            </div>
        </div>

        <div id="groupBai">
            <div class="row yellow center">
                百位
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="百">0</div>
                <div class="item" tag="百">1</div>
                <div class="item" tag="百">2</div>
                <div class="item" tag="百">3</div>
                <div class="item" tag="百">4</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="百">5</div>
                <div class="item" tag="百">6</div>
                <div class="item" tag="百">7</div>
                <div class="item" tag="百">8</div>
                <div class="item" tag="百">9</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="百">大</div>
                <div class="item" tag="百">小</div>
                <div class="item" tag="百">单</div>
                <div class="item" tag="百">双</div>
            </div>
        </div>

        <div id="groupShi">

            <div class="row blue center">
                十位
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="十">0</div>
                <div class="item" tag="十">1</div>
                <div class="item" tag="十">2</div>
                <div class="item" tag="十">3</div>
                <div class="item" tag="十">4</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="十">5</div>
                <div class="item" tag="十">6</div>
                <div class="item" tag="十">7</div>
                <div class="item" tag="十">8</div>
                <div class="item" tag="十">9</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="十">大</div>
                <div class="item" tag="十">小</div>
                <div class="item" tag="十">单</div>
                <div class="item" tag="十">双</div>
            </div>
        </div>

        <div id="groupGe">


            <div class="row yellow center">
                个位
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="个">0</div>
                <div class="item" tag="个">1</div>
                <div class="item" tag="个">2</div>
                <div class="item" tag="个">3</div>
                <div class="item" tag="个">4</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="个">5</div>
                <div class="item" tag="个">6</div>
                <div class="item" tag="个">7</div>
                <div class="item" tag="个">8</div>
                <div class="item" tag="个">9</div>
            </div>
            <div class="row lottery-m-qishu-xiazhu">
                <div class="item" tag="个">大</div>
                <div class="item" tag="个">小</div>
                <div class="item" tag="个">单</div>
                <div class="item" tag="个">双</div>
            </div>
        </div>
    </div>
</div>
