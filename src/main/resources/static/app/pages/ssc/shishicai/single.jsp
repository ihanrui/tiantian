<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="yellow">玩法说明：</span>请至少选择 1
            个号码进行投注，开奖号码包含投注号码为中奖，比如投注0，开60886为中1；开奖00688为中2；
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            [中1] <span class="red">1赔2</span>
            [中2] <span class="red">1赔3.6</span>
            [中3] <span class="red">1赔7.5</span><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            [中4] <span class="red">1赔15</span>
            [中5] <span class="red">1赔30</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">0</div>
            <div class="item">1</div>
            <div class="item">2</div>
            <div class="item">3</div>
            <div class="item">4</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">5</div>
            <div class="item">6</div>
            <div class="item">7</div>
            <div class="item">8</div>
            <div class="item">9</div>
        </div>
    </div>
</div>
