<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" uri="/easyui-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>会员消息</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <div class="content" data-type="">

            <header class="bar bar-nav fixed-tab">
                <button class="button pull-right" id="refreshData">
                    刷新
                </button>

                <div class="buttons-row ">
                    <div href="#tab_new" class="tab-link active button">未开奖消息</div>
                    <div href="#tab_communication" class="tab-link button">开奖消息</div>
                    <span style="display: none">

                    <t:dictSelect field="ssc_game" type="list" extendJson="{style:'display:none'}"
                                  typeGroupCode="ssc_game" defaultVal="${sscMemberRechargePage.rechargeStatus}"
                                  title="状态"></t:dictSelect>
                    </span>
                </div>
            </header>

            <div class="tabs" style="margin-top: 2.4rem">

                <div id="tab_new" class="tab active infinite-scroll  infinite-scroll-bottom" data-distance="100">
                    <div class="list-block media-list detail-item-container">
                        <ul class="bet-myself"></ul>
                    </div>

                    <div class="game-empty-container" id="game-empty-myself" style="display: none">
                        <img class='card-cover game-empty-img' src="webpage/mobile/pages/images/empty_single_item.png" alt="">
                        <p class="color-gray game-empty-desc">暂时没有记录</p>
                    </div>
                    <!-- 加载提示符 -->
                    <!-- 加载提示符 -->
                    <div  id="bet-myself-preloader">
                        <p><a href="#" class="button button-round">加载更多</a></p>
                    </div>
                </div>

                <div id="tab_communication" class="tab infinite-scroll infinite-scroll-bottom" data-distance="100">
                    <div class="list-block media-list detail-item-container">
                        <ul class="bet-all-other"></ul>
                    </div>

                    <div class="game-empty-container" id="game-empty-all" style="display: none">
                        <img class='card-cover game-empty-img' src="webpage/mobile/pages/images/empty_single_item.png" alt="">
                        <p class="color-gray game-empty-desc">暂时没有记录</p>
                    </div>
                    <!-- 加载提示符 -->
                    <div  id="bet-all-preloader">
                        <p><a href="#" class="button button-round">加载更多</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/game-utils.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-data.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-open-data.js' charset='utf-8'></script>
<script>

    $(document).on('infinite', function () {
        // 如果正在加载，则退出
        console.log("---loading---");

        if ($(this).find('.infinite-scroll.active').attr('id') == "tab_new") {

        }
        if ($(this).find('.infinite-scroll.active').attr('id') == "tab_communication") {
        }

    });

    $.init();

    $("#refreshData").on('click',function () {
        refreshBetGameData(true, 'ssc','','');
        refreshOtherBetGameData(true, 'ssc','','true');
    });
    $("#bet-myself-preloader").on('click',function () {
        refreshBetGameData(false, 'ssc','');
    });
    $("#bet-all-preloader").on('click',function () {
        refreshOtherBetGameData(false, 'ssc','','true');
    });

    refreshBetGameData(true, 'ssc','','');
    refreshOtherBetGameData(true, 'ssc','','true');

</script>
</body>
</html>
