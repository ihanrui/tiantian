<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="content-block">
    <div class="lottery-m-game-desc-container">
        <div class="row">
            <span class="yellow">玩法说明：</span>押注5位开奖号码相加的和值的大小单双，
            0-22为小、23-45为大；
        </div>
        <div class="row">
            <span class="yellow">赔率：</span>
            [大] <span class="red">1赔2</span>
            [小] <span class="red">1赔2</span>
            [单] <span class="red">1赔2</span>
            [双] <span class="red">1赔2</span>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            [组合] <span class="red">2组合：1赔4</span>
            <span class="red">3组合：1赔8</span>；(赔率包含本金)
        </div>
        <div class="row">
            <span class="yellow">限制下注：</span>
            <span class="red">20</span>元&nbsp;-&nbsp;<span class="red">3000</span>元
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item">大</div>
            <div class="item">小</div>
            <div class="item">单</div>
            <div class="item">双</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item doubler">大单</div>
            <div class="item doubler">大双</div>
            <div class="item doubler">小单</div>
            <div class="item doubler">小双</div>
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item doubler">大龙</div>
            <div class="item doubler">大虎</div>
            <div class="item doubler">小龙</div>
            <div class="item doubler">小虎</div>
        </div>

        <div class="row lottery-m-qishu-xiazhu">
            <div class="item doubler">单龙</div>
            <div class="item doubler">单虎</div>
            <div class="item doubler">双龙</div>
            <div class="item doubler">双虎</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item three">大单龙</div>
            <div class="item three">大单虎</div>
            <div class="item three">大双龙</div>
            <div class="item three">大双虎</div>
        </div>
        <div class="row lottery-m-qishu-xiazhu">
            <div class="item three">小单龙</div>
            <div class="item three">小单虎</div>
            <div class="item three">小双龙</div>
            <div class="item three">小双虎</div>
        </div>
    </div>
</div>
