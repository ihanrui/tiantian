<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>充值</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <a class="tab-item external " href="sscLoginController.do?toHome">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external active" href="sscMemberRechargeController.do?toUserRecharge">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external" href="sscMemberRechargeController.do?toUserDischarge">
                <span class="icon icon-star"></span>
                <span class="tab-label">提现</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?sscSystem">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">


                    <div class="home-game-container">
                        充值说明：<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微信在线充值，就是自己可以通过选择金额自行扫码充值；
                        客服充值，输入充值金额后点击确认按钮，会弹出客服二维码，联系客服充值！
                        <div class="recharge-yellow">
                            最小充值金额20元，充值时间9:00-02:00
                        </div>

                        <div class="discharge-line-yellow">
                            点此链接添加值班财务充值
                        </div>

                        <div class="recharge-total-desc">
                            账户余额:&nbsp;&nbsp;<span class="recharge-red" id="userTotalAmount">${userEntity.totalAmount}元</span>
                        </div>
                    </div>


                    <div style="height: 1rem;"></div>
                    <div class="row">
                        <div class="col-25 recharge-blue">微信<br>在线支付：</div>
                        <div class="col-75 recharge-btn-group">
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=20">
                                <a href="#" class="button button-warning">20 金币</a>
                            </div>
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=50">
                                <a href="#" class="button button-warning">50 金币</a>
                            </div>
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=100">
                                <a href="#" class="button button-warning">100 金币</a>
                            </div>
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=200">
                                <a href="#" class="button button-warning">200 金币</a>
                            </div>
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=500">
                                <a href="#" class="button button-warning">500 金币</a>
                            </div>
                            <div class="recharge-btn-item"
                                 url="sscMemberRechargeController.do?onlinePreRecharge&amount=1000">
                                <a href="#" class="button button-warning">1000 金币</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-recharge.js' charset='utf-8'></script>
<script>
    $.init();
    initRechargeEvent();
    var userTotalAmount = '${userEntity.totalAmount}';
    if(userTotalAmount == 'null'){
        userTotalAmount = '0';
    }
    var userTotalAmountNum = new Number(userTotalAmount);
    $('#userTotalAmount').text(userTotalAmountNum.toFixed(2)+'元');
</script>
</body>
</html>
