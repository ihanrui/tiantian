<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>登陆验证</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">

        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="row home-title">
                        <div class="home-user-head-container">
                            <image class="home-user-img" src="${userEntity.wxHead}"></image>
                        </div>
                        <div class="home-user-head-refresh">关闭</div>

                    </div>
                    <div class="row ">
                        <div class="login-detail-head-desc">${userEntity.wxName}欢迎您，<span class="login-name">加入欢乐时时彩</span></div>
                    </div>
                    <div class="list-block">
                        <ul>
                            <!-- Text inputs -->
                            <li>
                                <div class="item-content">
                                    <div class="item-media"><i class="icon icon-form-name"></i></div>
                                    <div class="item-inner">
                                        <div class="item-title label">手机号码</div>
                                        <div class="item-input">
                                            <input type="hidden" id="recoverFlag" value="">
                                            <input type="text" id="phoneNumber" placeholder="请输入手机号">
                                        </div>
                                    </div>
                                </div>
                                <div class="item-content">
                                    <div class="item-media"><i class="icon icon-form-name"></i></div>
                                    <div class="item-inner">
                                        <div class="item-title label">注册码</div>
                                        <div class="item-input">
                                            <input type="text" id="smsCode" placeholder="请输入注册码">
                                        </div>
                                        <div class="item-after">
                                            <a href="#" id="registerBtn" onclick="sendSMSCode()" class="button">获取注册码</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="content-block">
                        <p><a href="#" onclick="toLogin()" class="button button-fill">登陆</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/game-utils.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-login.js' charset='utf-8'></script>
<script>
    $.init();
</script>
</body>
</html>
