<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0">
    <title>微信识别二维码支付操作流程</title>
    <meta name="keywords" content="微信付款操作流程">
    <meta name="description" content="微信付款操作流程">

    <script type="text/javascript" src="/wxgame/plug-in/jquery/jquery-1.8.0.min.js"></script>

    <style type="text/css">
        .contens {
            background-color: #FFF;
            width: 86%;
            max-width: 640px;
            box-shadow: 0px 3px 10px #0070A6;
            margin-right: auto;
            margin-left: auto;
            margin-top: 5px;
            height: auto;
            border-radius: 6px;
            font-family: "微软雅黑";
            margin-bottom: 50px;
            padding-top: 5px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
        }

        p {
            font-size: 12px;
            color: #0B48FF;
            margin-top: 6px;
            margin-bottom: 6px;
        }

        .inputsty {
            font-size: 16px;
            width: 230px;
            height: 28px;
        }

        .sweep {
            background-repeat: no-repeat;
            text-align: center;
        }

        .buttonStyle {
            margin-right: auto;
            margin-left: auto;
            width: 100%;
        }

        .buttonStyle a {
            text-decoration: none;
            font-size: 18px;
            background-color: #0080C0;
            border-radius: 6px;
            color: #FFF;
            padding-top: 6px;
            padding-bottom: 6px;
            display: block;
        }

        .buttonStyle a:hover {
            border-radius: 6px;
            background-color: #2377F5;
        }

        .inputEmail {
            width: 30px;
            padding-left: 36px;
            padding-top: 160px;
        }

    </style>


</head>
<body>
<div class="contens">

    <h1 style="text-align: center;font-size: 18px;"><font color="red" id="alertinfo">请按照以下步骤操作完成付款</font></h1>

    <div class="sweep">

        <p>
            <b>第一步：手指长按二维码不放再点击识别图中二维码</b>
        </p>


        <p><b>指定金额：<a style="color: #F00;font-size: 30px;">${amount}</a></b></p>
        <p><img id="Qrcode" src="/wxgame/webpage/mobile/pages/images/recharge/${amount}.png" width="70%"></p>


        <script type="text/javascript">
            var intDiff = parseInt(78);//倒计时总秒数量
            function timer(intDiff) {
                window.setInterval(function () {
                    var day = 0,
                        hour = 0,
                        minute = 0,
                        second = 0;//时间默认值
                    if (intDiff > 0) {
                        day = Math.floor(intDiff / (60 * 60 * 24));
                        hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
                        minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
                        second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
                    }
                    if (minute <= 9) minute = '0' + minute;
                    if (second <= 9) second = '0' + second;
                    $('#day_show').html(day + "天");
                    $('#hour_show').html('<s id="h"></s>' + hour + '时');
                    $('#minute_show').html('<s></s>' + minute + '分');
                    $('#second_show').html('<s></s>' + second + '秒');
                    intDiff--;
                }, 1000);
            }

            $(function () {
                timer(intDiff);
            });
        </script>
        <style type="text/css">
            h1 {
                font-family: "微软雅黑";
                font-size: 30px;
                border-bottom: solid 1px #ccc;
                padding-bottom: 10px;
                letter-spacing: 2px;
            }

            h2 {
                font-family: "微软雅黑";
                font-size: 20px;
                letter-spacing: 2px;
            }

            .time-item strong {
                background: #C71C60;
                color: #fff;
                line-height: 49px;
                font-size: 24px;
                font-family: Arial;
                padding: 0 10px;
                margin-right: 10px;
                border-radius: 5px;
                margin-top: 10px;
                margin-bottom: 10px;
                box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.2);
            }

            #day_show {
                float: left;
                line-height: 49px;
                color: #c71c60;
                font-size: 32px;
                margin: 0 10px;
                font-family: Arial, Helvetica, sans-serif;
            }

            .item-title .unit {
                background: none;
                line-height: 49px;
                font-size: 24px;
                padding: 0 10px;
                float: left;
            }
        </style>


        <h2 style="margin-top:10px;margin-bottom:1px">距离该订单过期还有</h2>
        <div class="time-item">
            <!--<span id="day_show">0天</span>
            <strong id="hour_show">0时</strong>-->
            <strong id="minute_show"><s></s>00分</strong>
            <strong id="second_show"><s></s>54秒</strong>
        </div>


        <p style="color:#F2230B;margin-top: 5px;">温馨提示：付款成功后1-20秒到账请耐心等候，如果超时未到账请联系网站客服处理。</p>
    </div>

</div>


<script type="text/javascript">


    var timerQuerystatus = window.setInterval("querystatus('${rechargeCode}')", 3000);

    //查询订单状态
    function querystatus(order) {
        $.get("sscMemberRechargeController?checkRecharge", {rechargeCode: order, timestamp: new Date().getTime()},
            function (data) {
                console.log(data)
                if (data.success ) {
                    $("#Qrcode").attr("src", "/wxgame/webpage/mobile/pages/images/recharge/failure.png");
                    $("#alertinfo").html("恭喜您付款成功，3秒后自动返回网站");
                    window.setTimeout("jump()", 3000);
                    window.clearInterval(timerQuerystatus);//停止ajax

                } else if (data.message != '') {
                    //订单不存在提示失效
                    $("#alertinfo").html(data.message );
                    $("#Qrcode").attr("src", "/wxgame/webpage/mobile/pages/images/recharge/failure.png");
                    window.clearInterval(timerQuerystatus);//停止ajax
                }
            }, 'json');

    }

    //跳转
    function jump() {
        window.location.href = '/';
    }

    function alertOverdue() {

        $("#Qrcode").attr("src", "/wxgame/webpage/mobile/pages/images/recharge/failure.png");
        $("#alertinfo").html("该订单已过期如果您正在付款请停止操作");

        window.clearInterval(timerQuerystatus);//停止ajax

    }

    window.setTimeout("alertOverdue()", 1000 * 78);

</script>


</body>
</html>
