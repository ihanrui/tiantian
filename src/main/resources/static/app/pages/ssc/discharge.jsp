<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" uri="/easyui-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>提现</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="webpage/mobile/dist/css/sm.min.css">
    <link rel="stylesheet" href="webpage/mobile/dist/css/sm-extend.min.css">
    <link rel="stylesheet" href="webpage/mobile/pages/css/ssc.css">

</head>
<body>

<div class="page-group">
    <!-- 你的html代码 -->
    <div class="page">
        <nav class="bar bar-tab">
            <span style="display: none">

                    <t:dictSelect field="recharge_type" type="list" extendJson="{style:'display:none'}"
                                  typeGroupCode="recharge_type" defaultVal=""
                                  title="状态"></t:dictSelect>
                    </span>
            <a class="tab-item external " href="sscLoginController.do?toHome">
                <span class="icon icon-home"></span>
                <span class="tab-label">大厅</span>
            </a>
            <a class="tab-item external " href="sscMemberRechargeController.do?toUserRecharge">
                <span class="icon icon-me"></span>
                <span class="tab-label">充值</span>
            </a>
            <a class="tab-item external active" href="sscMemberRechargeController.do?toUserDischarge">
                <span class="icon icon-star"></span>
                <span class="tab-label">提现</span>
            </a>
            <a class="tab-item external" href="sscLoginController.do?sscSystem">
                <span class="icon icon-settings"></span>
                <span class="tab-label">系统</span>
            </a>
        </nav>
        <div class="content">
            <!-- 这里是页面内容区 -->
            <div class="page-index">
                <div class="content">
                    <div class="home-game-container">
                        提现说明：<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;输入提现金额，点击确认提交申请，加值班财务私聊通知。
                        最小提现金额10元，提现时段:9:00-02:00
                        点此链接添加值班财务提现！
                        <div class="discharge-line-yellow">
                            点此链接添加值班财务提现
                        </div>


                        <div class="recharge-total-desc">
                            <input type="hidden" value="${userTotalAmount}" id="userTotalLeft">
                            账户余额:&nbsp;&nbsp;<span class="recharge-red"
                                                   id="userTotalAmount">${userEntity.totalAmount}元</span>
                        </div>
                        <div class="lottery-m-game-desc-container">

                            <div class="searchbar row">
                                <div class="col-10">
                                    <div class="lottery-m-xiazhu-coin"></div>

                                </div>
                                <div class="search-input col-60"
                                     style="padding-left: 0.2rem;padding-right: 0.2rem;">
                                    <input type="number" id='betDischargeInput' placeholder='输入提现金额金额'/>
                                </div>
                                <a id="doDisRechargeBtn"
                                   class="button button-fill button-primary col-30 button-warning">提现(元)</a>
                            </div>
                        </div>
                    </div>


                    <div class="list-block media-list detail-item-container">

                        <div class="disrecharge-blue-split">
                            最近提现:
                        </div>
                    </div>
                    <div id="tab_communication" class="tab infinite-scroll infinite-scroll-bottom" data-distance="100">
                        <div class="list-block media-list detail-item-container">
                            <ul class="bet-all-other"></ul>
                        </div>

                        <div class="game-empty-container" id="game-empty-all" style="display: none">
                            <img class='card-cover game-empty-img'
                                 src="webpage/mobile/pages/images/empty_single_item.png" alt="">
                            <p class="color-gray game-empty-desc">暂时没有记录</p>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/dist/js/sm-extend.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/game-utils.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-recharge.js' charset='utf-8'></script>
<script type='text/javascript' src='webpage/mobile/pages/js/modules/ssc/ssc-disrecharge-data.js'
        charset='utf-8'></script>
<script>
    $.init();
    initDischargeEvent();
    var userTotalAmount = '${userEntity.totalAmount}';
    if (userTotalAmount == 'null') {
        userTotalAmount = '0';
    }
    var userTotalAmountNum = new Number(userTotalAmount);
    $('#userTotalAmount').text(userTotalAmountNum.toFixed(2) + '元');
    refreshOtherBetGameData(true, 'rechargeOut', true);
</script>
</body>
</html>
