package com.wangda.game.util;

import com.mile.util.payment.wechat.MD5;
import com.mile.util.payment.wechat.WeixinUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class WeixinUtilTest {

    @Test
    public void testDecodeXml() {
        String xml = "<xml>\n" +
                "  <appid><![CDATA[wx2421b1c4370ec43b]]></appid>\n" +
                "  <attach><![CDATA[支付测试]]></attach>\n" +
                "  <bank_type><![CDATA[CFT]]></bank_type>\n" +
                "  <fee_type><![CDATA[CNY]]></fee_type>\n" +
                "  <is_subscribe><![CDATA[Y]]></is_subscribe>\n" +
                "  <mch_id><![CDATA[10000100]]></mch_id>\n" +
                "  <nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>\n" +
                "  <openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>\n" +
                "  <out_trade_no><![CDATA[1409811653]]></out_trade_no>\n" +
                "  <result_code><![CDATA[SUCCESS]]></result_code>\n" +
                "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "  <sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>\n" +
                "  <sub_mch_id><![CDATA[10000100]]></sub_mch_id>\n" +
                "  <time_end><![CDATA[20140903131540]]></time_end>\n" +
                "  <total_fee>1</total_fee><coupon_fee><![CDATA[10]]></coupon_fee>\n" +
                "<coupon_count><![CDATA[1]]></coupon_count>\n" +
                "<coupon_type><![CDATA[CASH]]></coupon_type>\n" +
                "<coupon_id><![CDATA[10000]]></coupon_id>\n" +
                "<coupon_fee><![CDATA[100]]></coupon_fee>\n" +
                "  <trade_type><![CDATA[JSAPI]]></trade_type>\n" +
                "  <transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>\n" +
                "</xml>";

        Map m = WeixinUtil.decodeXml(xml);
        Assert.assertTrue(m.size() > 0);
        Assert.assertTrue(m.get("appid") != null);
    }

    /**
     *
     *
     "<xml><appid>wx62bc50f1844e4150</appid><body>å\u0087ºä¼\u0097æ£\u008bç\u0089\u008c\r\n-ç \u0096ç\u009f³</body><mch_id>1493945962</mch_id><nonce_str>F8C1F23D6A8D8D7904FC0EA8E066B3BB</nonce_str><notify_url>http://120.79.154.92:5161/paymeng/notifyback</notify_url><out_trade_no>2c948a82621dc4ef01621dc929460000</out_trade_no><spbill_create_ip>127.0.0.1</spbill_create_ip><total_fee>100.0</total_fee><trade_type>APP</trade_type><sign>12EC95E79547344FF8B6445A4C61DC7B</sign></xml>"

     *
     *
     */
    @Test
    public void testSignDecodeXml() {

        String xml = "<xml><appid>wx62bc50f1844e4150</appid><body>test</body>" +
                "<mch_id>1493945962</mch_id><nonce_str>BD48F59A9F04AEFD7708058B717453AF</nonce_str>" +
                "<notify_url>http://120.79.154.92:5161/paymeng/notifyback</notify_url><out_trade_no>2c948a826224b60f016224b702d30000</out_trade_no><spbill_create_ip>127.0.0.1</spbill_create_ip>" +
                "<total_fee>100</total_fee><trade_type>APP</trade_type><sign>230525F21EE7FC7839283003F1E03553</sign></xml>";

        Map m = WeixinUtil.decodeXml(xml);
        //m.put("body","出众棋牌\r\n-砖石");

        Assert.assertTrue(m.size() > 0);
        Assert.assertTrue(m.get("appid") != null);
        List<org.apache.http.NameValuePair> aaaaa = WeixinUtil.getPackageParams(m);
        String sign = WeixinUtil.genPackageSign(aaaaa,"963a911b4013dd2377f18f9fbe828945");
        System.out.println(sign);
    }

    /**
     *
     *清泉Link😊🏠
     *
     * 😄😄🎁🎁[红包][Pup]
     *
     "<xml><appid>wx62bc50f1844e4150</appid><body>å\u0087ºä¼\u0097æ£\u008bç\u0089\u008c\r\n-ç \u0096ç\u009f³</body><mch_id>1493945962</mch_id><nonce_str>F8C1F23D6A8D8D7904FC0EA8E066B3BB</nonce_str><notify_url>http://120.79.154.92:5161/paymeng/notifyback</notify_url><out_trade_no>2c948a82621dc4ef01621dc929460000</out_trade_no><spbill_create_ip>127.0.0.1</spbill_create_ip><total_fee>100.0</total_fee><trade_type>APP</trade_type><sign>12EC95E79547344FF8B6445A4C61DC7B</sign></xml>"

     *
     *
     */
    @Test
    public void testSignString() {


        String body  ="出众棋牌\r\n";


        String dd = body.trim();
        System.out.println(String.format("--%s---",dd));
        System.out.println(String.format("--%s---",body));
    }

    @Test
    public void testSign() {
        String xml = "<xml><appid>wx3cfec3034ef3c74e</appid><body><![CDATA[test]]></body><mch_id>1493566842</mch_id>" +
                "<nonce_str>4EB82F891B344474E99CAB5955F075D7</nonce_str><notify_url>http://120.79.154.92:5161/payment/notifyback</notify_url>" +
                "<out_trade_no>0000000062f5e9390162f5e9f1340000</out_trade_no><spbill_create_ip>127.0.0.1</spbill_create_ip>" +
                "<total_fee>1</total_fee><trade_type>APP</trade_type>" +
                "<sign><![CDATA[A4139BA9F53DBEBEE1F6F04E44D8C170]]></sign></xml>";

        String sign = "appid=wx3cfec3034ef3c74e&body=test&mch_id=1493566842&nonce_str=4EB82F891B344474E99CAB5955F075D7&" +
                "notify_url=http://120.79.154.92:5161/payment/notifyback" +
                "&out_trade_no=0000000062f5e9390162f5e9f1340000&spbill_create_ip=127.0.0.1&total_fee=1&trade_type=APP" +
                "&key=b61de25f562717923db379761aad8a3e";

        String sign11 = "appid=wx3cfec3034ef3c74e&body=test&mch_id=1493566842&nonce_str=4EB82F891B344474E99CAB5955F075D7&" +
                "notify_url=http://120.79.154.92:5161/payment/notifyback&" +
                "out_trade_no=0000000062f5e9390162f5e9f1340000&spbill_create_ip=127.0.0.1&total_fee=1&trade_type=APP&" +
                "key=Woeidji12di2d89aj8342ddedaDfffff";
        Map<String, String> params = WeixinUtil.decodeXml(xml);

        String checkSign = WeixinUtil.genPackageSign(WeixinUtil.getPackageParams(params), "Woeidji12di2d89aj8342ddedaDfffff");
        System.out.println(checkSign);
        Assert.assertTrue(checkSign.equals(params.get("sign")));

    }


    @Test
    public void testMD5() {
        String testUrl = "http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLJYn0kl8JgNTRnmdNrhEFRwbLwcicdicbAqpsoeqJN2ekOLKRjPOSSTlCYAY0EJxyEtBI9q9PrSmYQ/132";
        System.out.println(MD5.getMessageDigest(testUrl.toString().getBytes()).toUpperCase());

        testUrl += "45678";

        System.out.println(MD5.getMessageDigest(testUrl.toString().getBytes()).toUpperCase());
    }
}
