package com.wangda.game.pinshi;

import com.mile.core.game.pinshi.msgmodel.Pinshicardtype;
import com.mile.core.game.pinshi.util.PinshiCardtypeUtil;
import org.junit.Test;

/**
 * Created by michael on 2018/6/27.
 */
public class CardtypeTest {


    public static void main(String[] args){
        byte[] cards = new byte[]{48,49,50,51,47};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards));


        byte[] cards1 = new byte[]{0,1,2,3,40};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards1));

        byte[] cards2 = new byte[]{5,4,7,40,41};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards2));


        byte[] cards3 = new byte[]{0,1,5,51,47};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards3));


        byte[] cards4 = new byte[]{0,4,8,12,16};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards4));


        System.out.println("--------开始------------------- byte[] cards4s1 = new byte[]{0,4,8,12,48};-------------------------- ");
        byte[] cards4s1 = new byte[]{0,4,8,12,48};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards4s1).toString());
        System.out.println("--------结束------------------- byte[] cards4s1 = new byte[]{0,4,8,12,48};-------------------------- ");




        System.out.println("--------开始------------------- byte[] cards4s1 = new byte[]{28,32,36,40,44};-------------------------- ");
        byte[] cards4s2 = new byte[]{28,32,36,40,44};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards4s2).toString());
        System.out.println("--------结束------------------- byte[] cards4s1 = new byte[]{28,32,36,40,44};-------------------------- ");

        byte[] cards5 = new byte[]{46,7,11,30,36};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards5));


        byte[] cards6 = new byte[]{48,49,50,51,47};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards6));




        System.out.println("byte[] cards8 = new byte[]{12,13,22,32,39};");
        byte[] cards8 = new byte[]{12,13,22,32,39};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards8));
        byte[] ret8 =PinshiCardtypeUtil.getScorecard(cards8);
        System.out.println(ret8[0]+","+ret8[1]);


        byte[] cards9 = new byte[]{8,14,26,34,41};
        System.out.println(PinshiCardtypeUtil.getCardtype(cards9));

        byte[] cards10 = new byte[]{4,6,29,32,37};

        byte[] ret10 =PinshiCardtypeUtil.getScorecard(cards10);

        System.out.println(ret10[0]+","+ret10[1]);


        byte[] cards11 = new byte[]{8,14,25,27,31};
        byte[] ret11 =PinshiCardtypeUtil.getScorecard(cards11);
        System.out.println(ret11[0]+","+ret11[1]);


        byte[] cardsa = new byte[]{ 2,5,20,22,43};
        byte[] cardsb = new byte[]{ 7,10,11,26,42};
        System.out.println(PinshiCardtypeUtil.compare((Pinshicardtype) PinshiCardtypeUtil.getCardtype(cardsa),(Pinshicardtype)PinshiCardtypeUtil.getCardtype(cardsb)));




    }

    @Test
    public void psTest1(){
        System.out.println("--------开始------------------- byte[] cards4s3 = new byte[]{17,29,34,44,51};-------------------------- ");
        byte[] cards4s3 = new byte[]{17,29,34,44,51};
        byte[] ret11 =PinshiCardtypeUtil.getScorecard(cards4s3);
        System.out.println(ret11[0]+","+ret11[1]);
        System.out.println("--------结束------------------- byte[] cards4s3 = new byte[]{17,29,34,44,51};-------------------------- ");

    }

}
