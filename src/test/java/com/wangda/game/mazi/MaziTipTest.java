package com.wangda.game.mazi;

import com.mile.core.game.mazi.util.MaziTipsUtil;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class MaziTipTest {

    @Test
    public  void test2Link2(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)41,40,38,37};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test1(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)41};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test2(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)23,21};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test3(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)19,18,17};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test4(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)19,18,17,16};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test2Link3(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)17,16,10,9,1,0};
        MaziTipsUtil.test(testStr,param);
    }



    @Test
    public  void test3Link2(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)18,17,16,10,9,8};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test3Link3(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)27,26,25,18,17,16,10,9,8};
        MaziTipsUtil.test(testStr,param);
    }


    @Test
    public  void test4Link3(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)28,27,26,25,19,18,17,16,11,10,9,8};
        MaziTipsUtil.test(testStr,param);
    }

    @Test
    public  void printRandom(){
        for(int i=0;i<1000;i++)
        System.out.println((new Random().nextInt(108)));
    }


    @Test
    public  void test4Link2(){
        String testStr = "0,1,8,9,10,16,17,18,19,24,32,33,34,35,40,41,48,49," +
                "56,57,58,70,71,72,73,74,75,76,80,81,103,104,105,106"
                + ",90,91,92,93,94,95,96,97";
        byte[] param = new byte[]{(byte)28,27,26,25,19,18,17,16 };
        MaziTipsUtil.test(testStr,param);
    }

    @Test
    public  void dateFormat(){
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ssSSS").format(new Date(System.currentTimeMillis())));

        String str = UUID.randomUUID().toString();
        System.out.println(str);
        System.out.println(str.length());
        System.out.println(Integer.parseInt("999999"));

        System.out.println(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));


        boolean alwaysTeam = 0 % 2 == 0 % 2;//铁对
        System.out.println("队列测试 alwaysTeam["+alwaysTeam+"]");
    }


}
