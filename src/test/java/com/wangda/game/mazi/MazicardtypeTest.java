package com.wangda.game.mazi;

import com.mile.core.game.mazi.util.MaziCardtypeUtil;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class MazicardtypeTest {

    @Test
    public void testB1(){
        //单张牌
        byte[] b1 = new byte[1];
        b1[0] = 48;
        System.out.println("b1=" + MaziCardtypeUtil.getMaziCardtype(b1));

    }

    @Test
    public void testB21(){
        //两张
        byte[] b21 = new byte[2];
        b21[0] = 48;
        b21[1] = 9;
        System.out.println("b21=" + MaziCardtypeUtil.getMaziCardtype(b21));

    }

    @Test
    public void testB22(){
        //两张
        byte[] b22 = new byte[2];
        b22[0] = 107;
        b22[1] = 106;
        System.out.println("b22 =" + MaziCardtypeUtil.getMaziCardtype(b22));

    }


    @Test
    public void testB23(){

        //两张
        byte[] b23 = new byte[2];
        b23[0] = 107;
        b23[1] = 105;
        System.out.println("b23 = " + MaziCardtypeUtil.getMaziCardtype(b23));

    }


    @Test
    public void testB24(){

        //两张
        byte[] b24 = new byte[2];
        b24[0] = 105;
        b24[1] = 104;
        System.out.println("b24 =" + MaziCardtypeUtil.getMaziCardtype(b24));


    }


    @Test
    public void testB25(){

        //两张
        byte[] b25 = new byte[2];
        b25[0] = 25;
        b25[1] = 24;
        System.out.println("b25=" + MaziCardtypeUtil.getMaziCardtype(b25));

    }


    @Test
    public void testB31(){

        //三张
        byte[] b31 = new byte[3];
        b31[0] = 100;
        b31[1] = 25;
        b31[2] = 26;
        System.out.println("b31=" + MaziCardtypeUtil.getMaziCardtype(b31));

    }




    @Test
    public void testB32(){

        //三张
        byte[] b32 = new byte[3];
        b32[0] = 28;
        b32[1] = 26;
        b32[2] = 24;
        System.out.println("b32=" + MaziCardtypeUtil.getMaziCardtype(b32));

    }


    @Test
    public void testB33(){

        //三张
        byte[] b33 = new byte[3];
        b33[0] = 107;
        b33[1] = 105;
        b33[2] = 104;
        System.out.println("b33=" + MaziCardtypeUtil.getMaziCardtype(b33));

    }


    @Test
    public void testB34(){

        //三张
        byte[] b34 = new byte[3];
        b34[0] = 10 * 8 + 1;
        b34[1] = 7 * 8 + 2;
        b34[2] = 2 * 8 + 3;
        System.out.println("b34=" + MaziCardtypeUtil.getMaziCardtype(b34));


    }


    @Test
    public void testB35(){

        //三张
        byte[] b35 = new byte[3];
        b35[0] = 10 * 8;
        b35[1] = 7 * 8;
        b35[2] = 2 * 8;
        System.out.println("b35=" + MaziCardtypeUtil.getMaziCardtype(b35));

    }


    @Test
    public void testB36(){

        //三张
        byte[] b36 = new byte[3];
        b36[0] = 10 * 8 + 1;
        b36[1] = 7 * 8 + 1;
        b36[2] = 2 * 8 + 1;
        System.out.println("b36=" + MaziCardtypeUtil.getMaziCardtype(b36));

    }


    @Test
    public void testB37(){


        //三张
        byte[] b37 = new byte[3];
        b37[0] = 10 * 8 + 2;
        b37[1] = 7 * 8 + 2;
        b37[2] = 2 * 8 + 2;
        System.out.println("b37" + MaziCardtypeUtil.getMaziCardtype(b37));

    }


    @Test
    public void testB38(){


        //三张
        byte[] b38 = new byte[3];
        b38[0] = 10 * 8 + 3;
        b38[1] = 7 * 8 + 3;
        b38[2] = 2 * 8 + 3;
        System.out.println("b38=" + MaziCardtypeUtil.getMaziCardtype(b38));

    }



    @Test
    public void testB41(){
        //四张
        byte[] b41 = new byte[4];
        b41[0] = 10 * 8 + 3;
        b41[1] = 7 * 8 + 3;
        b41[2] = 2 * 8 + 3;
        b41[3] = 3 * 8 + 1;
        System.out.println("b41=" + MaziCardtypeUtil.getMaziCardtype(b41));
    }



    @Test
    public void testB42(){
        //四张
        byte[] b42 = new byte[4];
        b42[0] = 10 * 8 + 3;
        b42[1] = 10 * 8 + 6;
        b42[2] = 10 * 8 + 7;
        b42[3] = 10 * 8 + 1;
        System.out.println("b42=" + MaziCardtypeUtil.getMaziCardtype(b42));
    }


    @Test
    public void testB43(){
        //四张
        byte[] b43 = new byte[4];
        b43[0] = 10 * 8 + 3;
        b43[1] = 10 * 8 + 6;
        b43[2] = 9 * 8 + 7;
        b43[3] = 9 * 8 + 1;
        System.out.println("b43=" + MaziCardtypeUtil.getMaziCardtype(b43));
    }


    @Test
    public void testB44(){
        //四张
        byte[] b44 = new byte[4];
        b44[0] = 104;
        b44[1] = 105;
        b44[2] = 106;
        b44[3] = 107;
        System.out.println("b44=" + MaziCardtypeUtil.getMaziCardtype(b44));
    }


    @Test
    public void testB51(){
        //五张
        byte[] b51 = new byte[5];
        b51[0] = 9 * 8 + 6;
        b51[1] = 9 * 8 + 5;
        b51[2] = 9 * 8 + 4;
        b51[3] = 9 * 8 + 3;
        b51[4] = 9 * 8 + 2;
        System.out.println("b51=" + MaziCardtypeUtil.getMaziCardtype(b51));
    }




    @Test
    public void testB52(){
        //五张
        byte[] b52 = new byte[5];
        b52[0] = 9 * 8 + 6;
        b52[1] = 9 * 8 + 5;
        b52[2] = 7 * 8 + 4;
        b52[3] = 9 * 8 + 3;
        b52[4] = 9 * 8 + 2;
        System.out.println("b52=" + MaziCardtypeUtil.getMaziCardtype(b52));
    }


    @Test
    public void testB61(){
        //五张
        //六张
        byte[] b61 = new byte[6];
        b61[0] = 9 * 8 + 6;
        b61[1] = 9 * 8 + 5;
        b61[2] = 9 * 8 + 4;
        b61[3] = 9 * 8 + 3;
        b61[4] = 9 * 8 + 2;
        b61[5] = 9 * 8 + 1;
        System.out.println("b61=" + MaziCardtypeUtil.getMaziCardtype(b61));
    }



    @Test
    public void testB62(){
        //六张
        byte[] b62 = new byte[6];
        b62[0] = 9 * 8 + 6;
        b62[1] = 9 * 8 + 5;
        b62[2] = 9 * 8 + 4;
        b62[3] = 9 * 8 + 3;
        b62[4] = 1 * 8 + 2;
        b62[5] = 9 * 8 + 1;
        System.out.println("b62=" + MaziCardtypeUtil.getMaziCardtype(b62));
    }



    @Test
    public void testB63(){
        //六张
        byte[] b63 = new byte[6];
        b63[0] = 9 * 8 + 6;
        b63[1] = 9 * 8 + 5;
        b63[2] = 9 * 8 + 4;
        b63[3] = 8 * 8 + 3;
        b63[4] = 8 * 8 + 2;
        b63[5] = 8 * 8 + 1;
        System.out.println("b63=" + MaziCardtypeUtil.getMaziCardtype(b63));
    }



    @Test
    public void testB64(){
        //六张
        byte[] b64 = new byte[6];
        b64[0] = 9 * 8 + 6;
        b64[1] = 9 * 8 + 5;
        b64[2] = 8 * 8 + 4;
        b64[3] = 8 * 8 + 3;
        b64[4] = 7 * 8 + 2;
        b64[5] = 7 * 8 + 1;
        System.out.println("b64=" + MaziCardtypeUtil.getMaziCardtype(b64));

    }


    @Test
    public void testB71(){
        //七张
        byte[] b71 = new byte[7];
        b71[0] = 9 * 8 + 6;
        b71[1] = 9 * 8 + 5;
        b71[2] = 8 * 8 + 4;
        b71[3] = 8 * 8 + 3;
        b71[4] = 7 * 8 + 2;
        b71[5] = 7 * 8 + 1;
        b71[6] = 7 * 8 + 1;
        System.out.println("b71=" + MaziCardtypeUtil.getMaziCardtype(b71));

    }

    @Test
    public void testB72(){
        //七张
        byte[] b72 = new byte[7];
        b72[0] = 9 * 8 + 6;
        b72[1] = 9 * 8 + 5;
        b72[2] = 9 * 8 + 4;
        b72[3] = 9 * 8 + 3;
        b72[4] = 9 * 8 + 2;
        b72[5] = 9 * 8 + 1;
        b72[6] = 9 * 8 + 0;
        System.out.println("b72=" + MaziCardtypeUtil.getMaziCardtype(b72));

    }


    @Test
    public void testB81(){
        //八张
        byte[] b81 = new byte[8];
        b81[0] = 9 * 8 + 6;
        b81[1] = 9 * 8 + 5;
        b81[2] = 9 * 8 + 4;
        b81[3] = 9 * 8 + 3;
        b81[4] = 9 * 8 + 2;
        b81[5] = 9 * 8 + 1;
        b81[6] = 9 * 8 + 0;
        b81[7] = 9 * 8 + 0;
        System.out.println("b81=" + MaziCardtypeUtil.getMaziCardtype(b81));
    }



    @Test
    public void testB82(){
        //八张
        byte[] b82 = new byte[8];
        b82[0] = 9 * 8 + 6;
        b82[1] = 9 * 8 + 5;
        b82[2] = 8 * 8 + 4;
        b82[3] = 9 * 8 + 3;
        b82[4] = 9 * 8 + 2;
        b82[5] = 9 * 8 + 1;
        b82[6] = 9 * 8 + 0;
        b82[7] = 9 * 8 + 0;
        System.out.println("b82=" + MaziCardtypeUtil.getMaziCardtype(b82));
    }


    @Test
    public void testB83(){
        //八张
        byte[] b83 = new byte[8];
        b83[0] = 9 * 8 + 6;
        b83[1] = 9 * 8 + 5;
        b83[2] = 9 * 8 + 4;
        b83[3] = 9 * 8 + 3;
        b83[4] = 8 * 8 + 2;
        b83[5] = 8 * 8 + 1;
        b83[6] = 8 * 8 + 0;
        b83[7] = 8 * 8 + 7;
        System.out.println("b83=" + MaziCardtypeUtil.getMaziCardtype(b83));
    }


    @Test
    public void testB84(){
        //八张
        byte[] b84 = new byte[8];
        b84[0] = 11 * 8 + 6;
        b84[1] = 11 * 8 + 5;
        b84[2] = 10 * 8 + 4;
        b84[3] = 10 * 8 + 3;
        b84[4] = 9 * 8 + 2;
        b84[5] = 9 * 8 + 1;
        b84[6] = 8 * 8 + 0;
        b84[7] = 8 * 8 + 7;
        System.out.println("b84=" + MaziCardtypeUtil.getMaziCardtype(b84));

    }

    @Test
    public void testsettleCards(){

        String testStr = "0,1,8,9,16,17,24,25,23,32,33, 41,48,49," +
                "56,57,60,61,70,71,72,73,74,75,76,80,81,84,86,103,104,105,106"
                +",90,91,92,93,94,95,96,97,98";
        Map retMap = new LinkedHashMap();
        byte[] ret = MaziCardtypeUtil.settleCards(MaziCardtypeUtil.str2byte(testStr),retMap);

        for (byte b : ret) {


            String s="";
            if(b<104) {
                int y = b % 4;
                if (y == 0)
                    s = "黑桃";
                if (y == 1)
                    s = "红桃";
                if (y == 2)
                    s = "梅花";
                if (y == 3)
                    s = "方片";

                if (b/8 == 8)
                    s += "J";
                else  if (b/8  == 9)
                    s += "Q";
                else if (b/8  == 10)
                    s += "K";
                else if (b/8  == 11)
                    s += "A";
                else if (b/8  == 12)
                    s += "2";
                else
                    s += (b / 8 + 3);
            }

            if(b>105)
                s="大鬼";
            else if(b>103)
                s="小鬼";

            System.out.print(s + ",");
        }

    }

}
