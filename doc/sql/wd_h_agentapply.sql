ALTER TABLE `beimi`.`wd_h_agentapply` ADD COLUMN `token` varchar(32) AFTER `memo`, ADD COLUMN `userid` varchar(32) AFTER `token`;

ALTER TABLE `beimi`.`wd_h_agentapply` CHANGE COLUMN `phone` `mobile` varchar(32) DEFAULT NULL, ADD COLUMN `qq` varchar(32) AFTER `userid`;
