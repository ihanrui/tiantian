

-- ----------------------------
--  Table structure for `wd_h_userfeedback`
-- ----------------------------
DROP TABLE IF EXISTS `wd_h_userfeedback`;
CREATE TABLE `wd_h_userfeedback` (
  `id` varchar(32) NOT NULL,
  `orderindex` int(3) DEFAULT NULL,
  `gameid` varchar(32) DEFAULT NULL,
  `createdate` datetime(6) DEFAULT CURRENT_TIMESTAMP(6),
  `title` varchar(128) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `responseid` varchar(32) DEFAULT NULL,
  `responsedate` datetime(6) DEFAULT CURRENT_TIMESTAMP(6),
  `responsecontent` varchar(255) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `wx` varchar(32) DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `wd_h_userfeedback`
-- ----------------------------

-- ----------------------------
--  Table structure for `wd_r_play_record`
-- ----------------------------
DROP TABLE IF EXISTS `wd_r_play_record`;
CREATE TABLE `wd_r_play_record` (
  `id` varchar(36) DEFAULT NULL,
  `roomid` varchar(16) DEFAULT NULL,
  `teahouseid` varchar(16) DEFAULT NULL,
  `tableid` varchar(16) DEFAULT NULL,
  `userid` varchar(36) DEFAULT NULL,
  `createdate` datetime(6) DEFAULT CURRENT_TIMESTAMP(6),
  `cards` double(12,4) DEFAULT NULL,
  `integral` double(12,4) DEFAULT NULL,
  `goldcoins` double(12,4) DEFAULT NULL,
  `diamonds` double(12,4) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `seat` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `userurl` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




DROP TABLE IF EXISTS `wd_h_recharge_record`;
CREATE TABLE `wd_h_recharge_record` (
			`id` varchar(32) NOT NULL,
			`orderindex` int(8) NOT NULL,
			`orderid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单号',
			`createdate` datetime NOT NULL,
			`userid` varchar(32) NOT NULL,
			`gameid` int(10) NOT NULL,
			`username` varchar(100) NOT NULL,
			`userurl` varchar(200) NOT NULL,
			`amount` double(12,4) NOT NULL,
			`actualamount` double(12,4) NOT NULL,
			`rechargetype` int(1) NOT NULL,
			`currentnumbers` float(12,0) NOT NULL,
			`rechargenumbers` int(10) NOT NULL,
			`rechargednumbers` int(10) NOT NULL,
			`status` int(1) NOT NULL,
			`memo` varchar(200) NOT NULL,
			PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `wd_h_game_sms`;
CREATE TABLE `wd_h_game_sms` (
  `id` varchar(36) NOT NULL,
  `create_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_date` datetime DEFAULT NULL COMMENT '创建日期',
  `update_name` varchar(50) DEFAULT NULL COMMENT '修改人名称',
  `update_date` datetime DEFAULT NULL COMMENT '修改日期',
  `sms_status` varchar(2) DEFAULT NULL COMMENT '状态',
  `phone_number` varchar(32) DEFAULT NULL COMMENT '手机号',
  `smsid` varchar(50) DEFAULT NULL COMMENT '发送平台id',
  `sms_content` longtext COMMENT '发送内容',
  `platform` varchar(32) DEFAULT NULL COMMENT '发送平台',
  `sms_type` varchar(2) DEFAULT NULL COMMENT '发送类型',
  `user_id` varchar(36) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- ----------------------------
--  Table structure for `wd_s_typegroup`
-- ----------------------------
DROP TABLE IF EXISTS `wd_s_typegroup`;
CREATE TABLE `wd_s_typegroup` (
  `ID` varchar(32) NOT NULL,
  `typegroupcode` varchar(50) DEFAULT NULL,
  `typegroupname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- ----------------------------
--  Table structure for `wd_s_type`
-- ----------------------------
DROP TABLE IF EXISTS `wd_s_type`;
CREATE TABLE `wd_s_type` (
  `ID` varchar(32) NOT NULL,
  `typecode` varchar(50) DEFAULT NULL,
  `typename` varchar(50) DEFAULT NULL,
  `typegroupid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

