
ALTER TABLE `beimi`.`wd_h_userchat` ADD COLUMN `wx` varchar(32) AFTER `memo`;

ALTER TABLE `beimi`.`wd_h_userchat` ADD COLUMN `token` varchar(32) AFTER `wx`;

ALTER TABLE `beimi`.`wd_h_userchat` CHANGE COLUMN `responseDate` `responsedate` datetime(6) DEFAULT CURRENT_TIMESTAMP(6);